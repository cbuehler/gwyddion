/*
 *  $Id$
 *  Copyright (C) 2003-2017 David Necas (Yeti), Petr Klapetek.
 *  E-mail: yeti@gwyddion.net, klapetek@gwyddion.net.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA 02110-1301, USA.
 */

#include <string.h>
#include <libgwyddion/gwymacros.h>
#include <libgwyddion/gwymath.h>
#include <libprocess/filters.h>
#include <libprocess/stats.h>
#include <libprocess/linestats.h>
#include <libprocess/inttrans.h>
#include <libprocess/arithmetic.h>
#include "gwyprocessinternal.h"

typedef gdouble (*DoubleArrayFunc)(const gdouble *results);

static void thin_data_field(GwyDataField *data_field);

/**
 * gwy_data_field_normalize:
 * @data_field: A data field.
 *
 * Normalizes data in a data field to range 0.0 to 1.0.
 *
 * It is equivalent to gwy_data_field_renormalize(@data_field, 1.0, 0.0);
 *
 * If @data_field is filled with only one value, it is changed to 0.0.
 **/
void
gwy_data_field_normalize(GwyDataField *data_field)
{
    gdouble min, max;
    gdouble *p;
    gint xres, yres, i;

    g_return_if_fail(GWY_IS_DATA_FIELD(data_field));

    gwy_data_field_get_min_max(data_field, &min, &max);
    if (min == max) {
        gwy_data_field_clear(data_field);
        return;
    }
    if (!min) {
        if (max != 1.0)
            gwy_data_field_multiply(data_field, 1.0/max);
        return;
    }

    /* The general case */
    max -= min;
    xres = data_field->xres;
    yres = data_field->yres;
    for (i = xres*yres, p = data_field->data; i; i--, p++)
        *p = (*p - min)/max;

    /* We can transform stats */
    data_field->cached &= CBIT(MIN) | CBIT(MAX) | CBIT(SUM) | CBIT(RMS)
                          | CBIT(MED) | CBIT(ARF) | CBIT(ART);
    CVAL(data_field, MIN) = 0.0;
    CVAL(data_field, MAX) = 1.0;
    CVAL(data_field, SUM) /= (CVAL(data_field, SUM) - xres*yres*min)/max;
    CVAL(data_field, RMS) /= max;
    CVAL(data_field, MED) = (CVAL(data_field, MED) - min)/max;
    CVAL(data_field, ART) = (CVAL(data_field, ART) - min)/max;
    CVAL(data_field, ARF) = (CVAL(data_field, ARF) - min)/max;
}

/**
 * gwy_data_field_renormalize:
 * @data_field: A data field.
 * @range: New data interval size.
 * @offset: New data interval offset.
 *
 * Transforms data in a data field with linear function to given range.
 *
 * When @range is positive, the new data range is (@offset, @offset+@range);
 * when @range is negative, the new data range is (@offset-@range, @offset).
 * In neither case the data are flipped, negative range only means different
 * selection of boundaries.
 *
 * When @range is zero, this method is equivalent to
 * gwy_data_field_fill(@data_field, @offset).
 **/
void
gwy_data_field_renormalize(GwyDataField *data_field,
                           gdouble range,
                           gdouble offset)
{
    gdouble min, max, v;
    gdouble *p;
    gint xres, yres, i;

    g_return_if_fail(GWY_IS_DATA_FIELD(data_field));

    if (!range) {
        gwy_data_field_fill(data_field, offset);
        return;
    }

    gwy_data_field_get_min_max(data_field, &min, &max);
    if (min == max) {
        gwy_data_field_fill(data_field, offset);
        return;
    }

    if ((range > 0 && min == offset && min + range == max)
        || (range < 0 && max == offset && min - range == max))
        return;

    /* The general case */
    xres = data_field->xres;
    yres = data_field->yres;

    if (range > 0) {
        max -= min;
        for (i = xres*yres, p = data_field->data; i; i--, p++)
            *p = (*p - min)/max*range + offset;

        /* We can transform stats */
        data_field->cached &= CBIT(MIN) | CBIT(MAX) | CBIT(SUM) | CBIT(RMS)
                              | CBIT(MED);
        CVAL(data_field, MIN) = offset;
        CVAL(data_field, MAX) = offset + range;
        v = CVAL(data_field, SUM);
        CVAL(data_field, SUM) = (v - xres*yres*min)/max*range
                                + offset*xres*yres;
        CVAL(data_field, RMS) = CVAL(data_field, RMS)/max*range;
        CVAL(data_field, MED) = (CVAL(data_field, MED) - min)/max*range
                                + offset;
        /* FIXME: we can recompute ARF and ART too */
    }
    else {
        min = max - min;
        for (i = xres*yres, p = data_field->data; i; i--, p++)
            *p = (max - *p)/min*range + offset;

        /* We can transform stats */
        data_field->cached &= CBIT(MIN) | CBIT(MAX) | CBIT(SUM) | CBIT(RMS)
                              | CBIT(MED);
        CVAL(data_field, MIN) = offset + range;
        CVAL(data_field, MAX) = offset;
        v = CVAL(data_field, SUM);
        CVAL(data_field, SUM) = (xres*yres*max - v)/min*range
                                + offset*xres*yres;
        CVAL(data_field, RMS) = CVAL(data_field, RMS)/min*(-range);
        CVAL(data_field, MED) = (max - CVAL(data_field, MED))/min*range
                                + offset;
        /* FIXME: we can recompute ARF and ART too */
    }
}

/**
 * gwy_data_field_area_renormalize:
 * @data_field: A data field.
 * @col: Upper-left column coordinate.
 * @row: Upper-left row coordinate.
 * @width: Area width (number of columns).
 * @height: Area height (number of rows).
 * @range: New data interval size.
 * @offset: New data interval offset.
 *
 * Transforms data in a part of a data field with linear function to given
 * range.
 *
 * When @range is positive, the new data range is (@offset, @offset+@range);
 * when @range is negative, the new data range is (@offset-@range, @offset).
 * In neither case the data are flipped, negative range only means different
 * selection of boundaries.
 *
 * When @range is zero, this method is equivalent to
 * gwy_data_field_fill(@data_field, @offset).
 *
 * Since: 2.45
 **/
void
gwy_data_field_area_renormalize(GwyDataField *dfield,
                                gint col,
                                gint row,
                                gint width,
                                gint height,
                                gdouble range,
                                gdouble offset)
{
    gdouble min, max;
    gdouble *d, *r;
    gint xres, yres, i, j;

    g_return_if_fail(GWY_IS_DATA_FIELD(dfield));
    xres = dfield->xres;
    yres = dfield->yres;
    g_return_if_fail(col >= 0 && row >= 0
                     && width >= 0 && height >= 0
                     && col + width <= xres
                     && row + height <= yres);

    if (col == 0 && row == 0 && width == xres && height == yres) {
        gwy_data_field_renormalize(dfield, range, offset);
        return;
    }

    if (!range) {
        gwy_data_field_area_fill(dfield, col, row, width, height, offset);
        return;
    }

    gwy_data_field_area_get_min_max_mask(dfield, NULL, GWY_MASK_IGNORE,
                                         col, row, width, height,
                                         &min, &max);
    if (min == max) {
        gwy_data_field_area_fill(dfield, col, row, width, height, offset);
        return;
    }

    if ((range > 0 && min == offset && min + range == max)
        || (range < 0 && max == offset && min - range == max))
        return;

    /* The general case */
    d = dfield->data;
    if (range > 0) {
        max -= min;
        for (i = 0; i < height; i++) {
            r = d + (i + row)*xres + col;
            for (j = 0; j < width; j++)
                r[j] = (r[j] - min)/max*range + offset;
        }
    }
    else {
        min = max - min;
        for (i = 0; i < height; i++) {
            r = d + (i + row)*xres + col;
            for (j = 0; j < width; j++)
                r[j] = (max - r[j])/min*range + offset;
        }
    }

    gwy_data_field_invalidate(dfield);
}

/**
 * gwy_data_field_threshold:
 * @data_field: A data field.
 * @threshval: Threshold value.
 * @bottom: Lower replacement value.
 * @top: Upper replacement value.
 *
 * Tresholds values of a data field.
 *
 * Values smaller than @threshold are set to value @bottom, values higher
 * than @threshold or equal to it are set to value @top
 *
 * Returns: The total number of values above threshold.
 **/
gint
gwy_data_field_threshold(GwyDataField *data_field,
                         gdouble threshval, gdouble bottom, gdouble top)
{
    gint i, n, tot = 0;
    gdouble *p = data_field->data;

    g_return_val_if_fail(GWY_IS_DATA_FIELD(data_field), 0);

    n = data_field->xres * data_field->yres;
    for (i = n; i; i--, p++) {
        if (*p < threshval)
            *p = bottom;
        else {
            *p = top;
            tot++;
        }
    }

    /* We can precompute stats */
    data_field->cached = CBIT(MIN) | CBIT(MAX) | CBIT(SUM) | CBIT(RMS)
                         | CBIT(MED);
    if (tot == n)
        CVAL(data_field, MIN) = CVAL(data_field, MAX) = top;
    else if (tot == 0)
        CVAL(data_field, MIN) = CVAL(data_field, MAX) = bottom;
    else {
        CVAL(data_field, MIN) = MIN(top, bottom);
        CVAL(data_field, MAX) = MAX(top, bottom);
    }
    CVAL(data_field, SUM) = tot*top + (n - tot)*bottom;
    CVAL(data_field, RMS) = (top - bottom)*(top - bottom)
                            * tot/(gdouble)n * (n - tot)/(gdouble)n;
    /* FIXME: may be incorrect for tot == n/2(?) */
    CVAL(data_field, MED) = tot > n/2 ? top : bottom;

    return tot;
}


/**
 * gwy_data_field_area_threshold:
 * @data_field: A data field.
 * @col: Upper-left column coordinate.
 * @row: Upper-left row coordinate.
 * @width: Area width (number of columns).
 * @height: Area height (number of rows).
 * @threshval: Threshold value.
 * @bottom: Lower replacement value.
 * @top: Upper replacement value.
 *
 * Tresholds values of a rectangular part of a data field.
 *
 * Values smaller than @threshold are set to value @bottom, values higher
 * than @threshold or equal to it are set to value @top
 *
 * Returns: The total number of values above threshold.
 **/
gint
gwy_data_field_area_threshold(GwyDataField *data_field,
                              gint col, gint row, gint width, gint height,
                              gdouble threshval, gdouble bottom, gdouble top)
{
    gint i, j, tot = 0;
    gdouble *drow;

    g_return_val_if_fail(GWY_IS_DATA_FIELD(data_field), 0);
    g_return_val_if_fail(col >= 0 && row >= 0
                         && width >= 0 && height >= 0
                         && col + width <= data_field->xres
                         && row + height <= data_field->yres,
                         0);

    for (i = 0; i < height; i++) {
        drow = data_field->data + (row + i)*data_field->xres + col;

        for (j = 0; j < width; j++) {
            if (*drow < threshval)
                *drow = bottom;
            else {
                *drow = top;
                tot++;
            }
        }
    }
    gwy_data_field_invalidate(data_field);

    return tot;
}

/**
 * gwy_data_field_clamp:
 * @data_field: A data field.
 * @bottom: Lower limit value.
 * @top: Upper limit value.
 *
 * Limits data field values to a range.
 *
 * Returns: The number of changed values, i.e., values that were outside
 *          [@bottom, @top].
 **/
gint
gwy_data_field_clamp(GwyDataField *data_field,
                     gdouble bottom, gdouble top)
{
    gint i, tot = 0;
    gdouble *p = data_field->data;

    g_return_val_if_fail(GWY_IS_DATA_FIELD(data_field), 0);
    g_return_val_if_fail(bottom <= top, 0);

    for (i = data_field->xres * data_field->yres; i; i--, p++) {
        if (*p < bottom) {
            *p = bottom;
            tot++;
        }
        else if (*p > top) {
            *p = top;
            tot++;
        }
    }
    if (tot) {
        /* We can precompute stats */
        data_field->cached &= CBIT(MIN) | CBIT(MAX) | CBIT(MED);
        CVAL(data_field, MIN) = MAX(bottom, CVAL(data_field, MIN));
        CVAL(data_field, MAX) = MIN(top, CVAL(data_field, MAX));
        if (CTEST(data_field, MED)
            && (CVAL(data_field, MED) < bottom || CVAL(data_field, MED) > top))
            data_field->cached &= ~CBIT(MED);
    }

    return tot;
}

/**
 * gwy_data_field_area_clamp:
 * @data_field: A data field.
 * @col: Upper-left column coordinate.
 * @row: Upper-left row coordinate.
 * @width: Area width (number of columns).
 * @height: Area height (number of rows).
 * @bottom: Lower limit value.
 * @top: Upper limit value.
 *
 * Limits values in a rectangular part of a data field to a range.
 *
 * Returns: The number of changed values, i.e., values that were outside
 *          [@bottom, @top].
 **/
gint
gwy_data_field_area_clamp(GwyDataField *data_field,
                          gint col, gint row,
                          gint width, gint height,
                          gdouble bottom, gdouble top)
{
    gint i, j, tot = 0;
    gdouble *drow;

    g_return_val_if_fail(GWY_IS_DATA_FIELD(data_field), 0);
    g_return_val_if_fail(col >= 0 && row >= 0
                         && width >= 0 && height >= 0
                         && col + width <= data_field->xres
                         && row + height <= data_field->yres,
                         0);

    for (i = 0; i < height; i++) {
        drow = data_field->data + (row + i)*data_field->xres + col;

        for (j = 0; j < width; j++) {
            if (*drow < bottom) {
                *drow = bottom;
                tot++;
            }
            else if (*drow > top) {
                *drow = top;
                tot++;
            }
        }

    }
    if (tot)
        gwy_data_field_invalidate(data_field);

    return tot;
}

/**
 * gwy_data_field_area_gather:
 * @data_field: A data field.
 * @result: A data field to put the result to, it may be @data_field itself.
 * @buffer: A data field to use as a scratch area, its size must be at least
 *          @width*@height.  May be %NULL to allocate a private temporary
 *          buffer.
 * @col: Upper-left column coordinate.
 * @row: Upper-left row coordinate.
 * @width: Area width (number of columns).
 * @height: Area height (number of rows).
 * @hsize: Horizontal size of gathered area.  The area is centered around
 *         each sample if @hsize is odd, it extends one pixel more to the
 *         right if @hsize is even.
 * @vsize: Vertical size of gathered area.  The area is centered around
 *         each sample if @vsize is odd, it extends one pixel more down
 *         if @vsize is even.
 * @average: %TRUE to divide resulting sums by the number of involved samples
 *           to get averages instead of sums.
 *
 * Sums or averages values in reactangular areas around each sample in a data
 * field.
 *
 * When the gathered area extends out of calculation area, only samples from
 * their intersection are taken into the local sum (or average).
 *
 * There are no restrictions on values of @hsize and @vsize with regard to
 * @width and @height, but they have to be positive.
 *
 * The result is calculated by the means of two-dimensional rolling sums.
 * One one hand it means the calculation time depends linearly on
 * (@width + @hsize)*(@height + @vsize) instead of
 * @width*@hsize*@height*@vsize.  On the other hand it means absolute rounding
 * errors of all output values are given by the largest input values, that is
 * relative precision of results small in absolute value may be poor.
 **/
void
gwy_data_field_area_gather(GwyDataField *data_field,
                           GwyDataField *result,
                           GwyDataField *buffer,
                           gint hsize,
                           gint vsize,
                           gboolean average,
                           gint col, gint row,
                           gint width, gint height)
{
    const gdouble *srow, *trow;
    gdouble *drow;
    gint xres, yres, i, j, m;
    gint hs2p, hs2m, vs2p, vs2m;
    gdouble v;

    g_return_if_fail(hsize > 0 && vsize > 0);
    g_return_if_fail(GWY_IS_DATA_FIELD(data_field));
    xres = data_field->xres;
    yres = data_field->yres;
    g_return_if_fail(col >= 0 && row >= 0
                     && width >= 0 && height >= 0
                     && col + width <= xres
                     && row + height <= yres);
    g_return_if_fail(GWY_IS_DATA_FIELD(result));
    g_return_if_fail(result->xres == xres && result->yres == yres);
    if (buffer) {
        g_return_if_fail(GWY_IS_DATA_FIELD(buffer));
        g_return_if_fail(buffer->xres*buffer->yres >= width*height);
        g_object_ref(buffer);
    }
    else
        buffer = gwy_data_field_new(width, height, 1.0, 1.0, FALSE);

    /* Extension to the left and to the right (for asymmetric sizes extend
     * to the right more) */
    hs2m = (hsize - 1)/2;
    hs2p = hsize/2;
    vs2m = (vsize - 1)/2;
    vs2p = vsize/2;

    /* Row-wise sums */
    /* FIXME: This is inefficient, split the inner loops to explicitly
     * according to the conditions inside */
    for (i = 0; i < height; i++) {
        srow = data_field->data + (i + row)*xres + col;
        drow = buffer->data + i*width;

        /* Left half */
        drow[0] = 0.0;
        m = MIN(hs2p, width-1);
        for (j = 0; j <= m; j++)
            drow[0] += srow[j];
        for (j = 1; j < width/2; j++) {
            v = ((j + hs2p < width ? srow[j + hs2p] : 0.0)
                 - (j-1 - hs2m >= 0 ? srow[j-1 - hs2m] : 0.0));
            drow[j] = drow[j-1] + v;
        }

        /* Right half */
        drow[width-1] = 0.0;
        m = width-1 - MIN(hs2m, width-1);
        for (j = width-1; j >= m; j--)
            drow[width-1] += srow[j];
        for (j = width-2; j >= width/2; j--) {
            v = ((j - hs2m >= 0 ? srow[j - hs2m] : 0.0)
                 - (j+1 + hs2p < width ? srow[j+1 + hs2p] : 0.0));
            drow[j] = drow[j+1] + v;
        }
    }

    /* Column-wise sums (but iterate row-wise to access memory linearly) */
    /* Top half */
    drow = result->data + row*xres + col;
    for (j = 0; j < width; j++)
        drow[j] = 0.0;
    m = MIN(vs2p, height-1);
    for (i = 0; i <= m; i++) {
        srow = buffer->data + i*width;
        for (j = 0; j < width; j++)
            drow[j] += srow[j];
    }
    for (i = 1; i < height/2; i++) {
        drow = result->data + (i + row)*xres + col;
        if (i + vs2p < height) {
            srow = buffer->data + (i + vs2p)*width;
            if (i-1 - vs2m >= 0) {
                trow = buffer->data + (i-1 - vs2m)*width;
                for (j = 0; j < width; j++)
                    drow[j] = *(drow + j - xres) + (srow[j] - trow[j]);
            }
            else {
                for (j = 0; j < width; j++)
                    drow[j] = *(drow + j - xres) + srow[j];
            }
        }
        else {
            if (G_UNLIKELY(i-1 - vs2m >= 0)) {
                g_warning("Me thinks pure subtraction cannot occur.");
                trow = buffer->data + (i-1 - vs2m)*width;
                for (j = 0; j < width; j++)
                    drow[j] = *(drow + j - xres) - trow[j];
            }
            else {
                for (j = 0; j < width; j++)
                    drow[j] = *(drow + j - xres);
            }
        }
    }

    /* Bottom half */
    drow = result->data + (height-1 + row)*xres + col;
    for (j = 0; j < width; j++)
        drow[j] = 0.0;
    m = height-1 - MIN(vs2m, height-1);
    for (i = height-1; i >= m; i--) {
        srow = buffer->data + i*width;
        for (j = 0; j < width; j++)
            drow[j] += srow[j];
    }
    for (i = height-2; i >= height/2; i--) {
        drow = result->data + (i + row)*xres + col;
        if (i+1 + vs2p < height) {
            srow = buffer->data + (i+1 + vs2p)*width;
            if (G_LIKELY(i - vs2m >= 0)) {
                trow = buffer->data + (i - vs2m)*width;
                for (j = 0; j < width; j++)
                    drow[j] = drow[j + xres] + (trow[j] - srow[j]);
            }
            else {
                g_warning("Me thinks pure subtraction cannot occur.");
                for (j = 0; j < width; j++)
                    drow[j] = drow[j + xres] - srow[j];
            }
        }
        else {
            if (i - vs2m >= 0) {
                trow = buffer->data + (i - vs2m)*width;
                for (j = 0; j < width; j++)
                    drow[j] = drow[j + xres] + trow[j];
            }
            else {
                for (j = 0; j < width; j++)
                    drow[j] = drow[j + xres];
            }
        }
    }

    gwy_data_field_invalidate(result);
    gwy_data_field_invalidate(buffer);
    g_object_unref(buffer);

    if (!average)
        return;

    /* Divide sums by the numbers of pixels that entered them */
    for (i = 0; i < height; i++) {
        gint iw;

        if (i <= vs2m)
            iw = vs2p + 1 + i;
        else if (i >= height-1 - vs2p)
            iw = vs2m + height - i;
        else
            iw = vsize;
        iw = MIN(iw, height);

        for (j = 0; j < width; j++) {
            gint jw;

            if (j <= hs2m)
                jw = hs2p + 1 + j;
            else if (j >= width-1 - hs2p)
                jw = hs2m + width - j;
            else
                jw = hsize;
            jw = MIN(jw, width);

            result->data[(i + row)*xres + j + col] /= iw*jw;
        }
    }
}

static void
gwy_data_field_area_convolve_3x3(GwyDataField *data_field,
                                 const gdouble *kernel,
                                 gint col, gint row,
                                 gint width, gint height)
{
    gdouble *rm, *rc, *rp;
    gdouble t, v;
    gint xres, i, j;

    xres = data_field->xres;
    rp = data_field->data + row*xres + col;

    /* Special-case width == 1 to avoid complications below.  It's silly but
     * the API guarantees it. */
    if (width == 1) {
        t = rp[0];
        for (i = 0; i < height; i++) {
            rc = rp = data_field->data + (row + i)*xres + col;
            if (i < height-1)
                rp += xres;

            v = (kernel[0] + kernel[1] + kernel[2])*t
                + (kernel[3] + kernel[4] + kernel[5])*rc[0]
                + (kernel[6] + kernel[7] + kernel[8])*rp[0];
            t = rc[0];
            rc[0] = v;
        }
        gwy_data_field_invalidate(data_field);

        return;
    }

    rm = g_new(gdouble, width);
    gwy_assign(rm, rp, width);

    for (i = 0; i < height; i++) {
        rc = rp;
        if (i < height-1)
            rp += xres;
        v = (kernel[0] + kernel[1])*rm[0] + kernel[2]*rm[1]
            + (kernel[3] + kernel[4])*rc[0] + kernel[5]*rc[1]
            + (kernel[6] + kernel[7])*rp[0] + kernel[8]*rp[1];
        t = rc[0];
        rc[0] = v;
        if (i < height-1) {
            for (j = 1; j < width-1; j++) {
                v = kernel[0]*rm[j-1] + kernel[1]*rm[j] + kernel[2]*rm[j+1]
                    + kernel[3]*t + kernel[4]*rc[j] + kernel[5]*rc[j+1]
                    + kernel[6]*rp[j-1] + kernel[7]*rp[j] + kernel[8]*rp[j+1];
                rm[j-1] = t;
                t = rc[j];
                rc[j] = v;
            }
            v = kernel[0]*rm[j-1] + (kernel[1] + kernel[2])*rm[j]
                + kernel[3]*t + (kernel[4] + kernel[5])*rc[j]
                + kernel[6]*rp[j-1] + (kernel[7] + kernel[8])*rp[j];
        }
        else {
            for (j = 1; j < width-1; j++) {
                v = kernel[0]*rm[j-1] + kernel[1]*rm[j] + kernel[2]*rm[j+1]
                    + kernel[3]*t + kernel[4]*rc[j] + kernel[5]*rc[j+1]
                    + kernel[6]*t + kernel[7]*rc[j] + kernel[8]*rc[j+1];
                rm[j-1] = t;
                t = rc[j];
                rc[j] = v;
            }
            v = kernel[0]*rm[j-1] + (kernel[1] + kernel[2])*rm[j]
                + kernel[3]*t + (kernel[4] + kernel[5])*rc[j]
                + kernel[6]*t + (kernel[7] + kernel[8])*rc[j];
        }
        rm[j-1] = t;
        rm[j] = rc[j];
        rc[j] = v;
    }

    g_free(rm);
    gwy_data_field_invalidate(data_field);
}

/**
 * gwy_data_field_area_convolve:
 * @data_field: A data field to convolve.  It must be at least as large as
 *              1/3 of @kernel_field in each dimension.
 * @kernel_field: Kenrel field to convolve @data_field with.
 * @col: Upper-left column coordinate.
 * @row: Upper-left row coordinate.
 * @width: Area width (number of columns).
 * @height: Area height (number of rows).
 *
 * Convolves a rectangular part of a data field with given kernel.
 **/
void
gwy_data_field_area_convolve(GwyDataField *data_field,
                             GwyDataField *kernel_field,
                             gint col, gint row,
                             gint width, gint height)
{
    gint xres, yres, kxres, kyres, i, j, m, n, ii, jj;
    GwyDataField *hlp_df;
    gdouble *d, *h, *k;
    gdouble v;

    g_return_if_fail(GWY_IS_DATA_FIELD(data_field));
    g_return_if_fail(GWY_IS_DATA_FIELD(kernel_field));

    xres = data_field->xres;
    yres = data_field->yres;
    kxres = kernel_field->xres;
    kyres = kernel_field->yres;
    g_return_if_fail(col >= 0 && row >= 0
                     && width >= 0 && height >= 0
                     && col + width <= xres
                     && row + height <= yres);

    if (kxres == 3 && kyres == 3) {
        gwy_data_field_area_convolve_3x3(data_field, kernel_field->data,
                                         col, row, width, height);
        return;
    }

    hlp_df = gwy_data_field_new(width, height, 1.0, 1.0, FALSE);
    d = data_field->data;
    k = kernel_field->data;
    h = hlp_df->data;
    for (i = row; i < row + height; i++) {
        for (j = col; j < col + width; j++) {
            v = 0.0;
            for (m = -kyres/2; m < kyres - kyres/2; m++) {
                ii = i + m;
                if (G_UNLIKELY(ii < 0))
                    ii = -ii-1;
                else if (G_UNLIKELY(ii >= yres))
                    ii = 2*yres-1 - ii;

                for (n = -kxres/2; n < kxres - kxres/2; n++) {
                    jj = j + n;
                    if (G_UNLIKELY(jj < 0))
                        jj = -jj-1;
                    else if (G_UNLIKELY(jj >= xres))
                        jj = 2*xres-1 - jj;

                    v += d[ii*xres + jj] * k[kxres*(m + kyres/2)
                                             + n + kxres/2];
                }
            }
            h[(i - row)*width + (j - col)] = v;
        }
    }
    gwy_data_field_area_copy(hlp_df, data_field, 0, 0, width, height, col, row);
    g_object_unref(hlp_df);

    gwy_data_field_invalidate(data_field);
}

/**
 * gwy_data_field_convolve:
 * @data_field: A data field to convolve.  It must be at least as large as
 *              1/3 of @kernel_field in each dimension.
 * @kernel_field: Kenrel field to convolve @data_field with.
 *
 * Convolves a data field with given kernel.
 **/
void
gwy_data_field_convolve(GwyDataField *data_field,
                        GwyDataField *kernel_field)
{
    g_return_if_fail(GWY_IS_DATA_FIELD(data_field));
    gwy_data_field_area_convolve(data_field, kernel_field,
                                 0, 0, data_field->xres, data_field->yres);
}

static void
gwy_data_field_area_hconvolve(GwyDataField *data_field,
                              GwyDataLine *kernel_line,
                              gint col, gint row,
                              gint width, gint height)
{
    gint kres, mres, k0, i, j, k, pos;
    const gdouble *kernel;
    gdouble *buf, *drow;
    gdouble d;

    kres = kernel_line->res;
    kernel = kernel_line->data;
    mres = 2*width;
    k0 = (kres/2 + 1)*mres;
    buf = g_new(gdouble, kres);

    for (i = 0; i < height; i++) {
        drow = data_field->data + (row + i)*data_field->xres + col;
        /* Initialize with a triangluar sums, mirror-extend */
        gwy_clear(buf, kres);
        for (j = 0; j < kres; j++) {
            k = (j - kres/2 + k0) % mres;
            d = drow[k < width ? k : mres-1 - k];
            for (k = 0; k <= j; k++)
                buf[k] += kernel[j - k]*d;
        }
        pos = 0;
        /* Middle part and tail with mirror extension again, we do some
         * O(1/2*k^2) of useless work here by not separating the tail */
        for (j = 0; j < width; j++) {
            drow[j] = buf[pos];
            buf[pos] = 0.0;
            pos = (pos + 1) % kres;
            k = (j + kres - kres/2 + k0) % mres;
            d = drow[G_LIKELY(k < width) ? k : mres-1 - k];
            for (k = pos; k < kres; k++)
                buf[k] += kernel[kres-1 - (k - pos)]*d;
            for (k = 0; k < pos; k++)
                buf[k] += kernel[pos-1 - k]*d;
        }
    }

    g_free(buf);
}

static void
gwy_data_field_area_vconvolve(GwyDataField *data_field,
                              GwyDataLine *kernel_line,
                              gint col, gint row,
                              gint width, gint height)
{
    gint kres, xres, mres, k0, i, j, k, pos;
    const gdouble *kernel;
    gdouble *buf, *dcol;
    gdouble d;

    kres = kernel_line->res;
    kernel = kernel_line->data;
    xres = data_field->xres;
    mres = 2*height;
    k0 = (kres/2 + 1)*mres;
    buf = g_new(gdouble, kres);

    /* This looks like a bad memory access pattern.  And for small kernels it
     * indeed is (we should iterate row-wise and directly calculate the sums).
     * For large kernels this is mitigated by the maximum possible amount of
     * work done per a data field access. */
    for (j = 0; j < width; j++) {
        dcol = data_field->data + row*xres + (col + j);
        /* Initialize with a triangluar sums, mirror-extend */
        gwy_clear(buf, kres);
        for (i = 0; i < kres; i++) {
            k = (i - kres/2 + k0) % mres;
            d = dcol[k < height ? k*xres : (mres-1 - k)*xres];
            for (k = 0; k <= i; k++)
                buf[k] += kernel[i - k]*d;
        }
        pos = 0;
        /* Middle part and tail with mirror extension again, we do some
         * O(1/2*k^2) of useless work here by not separating the tail */
        for (i = 0; i < height; i++) {
            dcol[i*xres] = buf[pos];
            buf[pos] = 0.0;
            pos = (pos + 1) % kres;
            k = (i + kres - kres/2 + k0) % mres;
            d = dcol[G_LIKELY(k < height) ? k*xres : (mres-1 - k)*xres];
            for (k = pos; k < kres; k++)
                buf[k] += kernel[kres-1 - (k - pos)]*d;
            for (k = 0; k < pos; k++)
                buf[k] += kernel[pos-1 - k]*d;
        }
    }

    g_free(buf);
}

/**
 * gwy_data_field_area_convolve_1d:
 * @data_field: A data field to convolve.  It must be at least as large as
 *              1/3 of @kernel_field in the corresponding dimension.
 * @kernel_line: Kernel line to convolve @data_field with.
 * @orientation: Filter orientation (%GWY_ORIENTATION_HORIZONTAL for
 *               row-wise convolution, %GWY_ORIENTATION_VERTICAL for
 *               column-wise convolution).
 * @col: Upper-left column coordinate.
 * @row: Upper-left row coordinate.
 * @width: Area width (number of columns).
 * @height: Area height (number of rows).
 *
 * Convolves a rectangular part of a data field with given linear kernel.
 *
 * For large separable kernels it can be more efficient to use a sequence of
 * horizontal and vertical convolutions instead one 2D convolution.
 *
 * Since: 2.4
 **/
void
gwy_data_field_area_convolve_1d(GwyDataField *data_field,
                                GwyDataLine *kernel_line,
                                GwyOrientation orientation,
                                gint col, gint row,
                                gint width, gint height)
{
    gint kres;

    g_return_if_fail(GWY_IS_DATA_FIELD(data_field));
    g_return_if_fail(GWY_IS_DATA_LINE(kernel_line));

    g_return_if_fail(col >= 0 && row >= 0
                     && width >= 0 && height >= 0
                     && col + width <= data_field->xres
                     && row + height <= data_field->yres);

    kres = kernel_line->res;
    if (kres == 1) {
        gwy_data_field_area_multiply(data_field, col, row, width, height,
                                     kernel_line->data[0]);
        return;
    }

    switch (orientation) {
        case GWY_ORIENTATION_HORIZONTAL:
        gwy_data_field_area_hconvolve(data_field, kernel_line,
                                      col, row, width, height);
        break;

        case GWY_ORIENTATION_VERTICAL:
        gwy_data_field_area_vconvolve(data_field, kernel_line,
                                      col, row, width, height);
        break;

        default:
        g_return_if_reached();
        break;
    }

    gwy_data_field_invalidate(data_field);
}

/**
 * gwy_data_field_convolve_1d:
 * @data_field: A data field to convolve.  It must be at least as large as
 *              1/3 of @kernel_field in the corresponding dimension.
 * @kernel_line: Kenrel line to convolve @data_field with.
 * @orientation: Filter orientation (see gwy_data_field_area_convolve_1d()).
 *
 * Convolves a data field with given linear kernel.
 *
 * Since: 2.4
 **/
void
gwy_data_field_convolve_1d(GwyDataField *data_field,
                           GwyDataLine *kernel_line,
                           GwyOrientation orientation)
{
    g_return_if_fail(GWY_IS_DATA_FIELD(data_field));
    gwy_data_field_area_convolve_1d(data_field, kernel_line, orientation,
                                    0, 0, data_field->xres, data_field->yres);
}

static void
ensure_defined_exterior(GwyExteriorType *exterior, gdouble *fill_value)
{
    if (*exterior == GWY_EXTERIOR_UNDEFINED) {
        g_warning("Do not use GWY_EXTERIOR_UNDEFINED for convolutions and "
                  "correlations.  Fixing to zero-filled exterior.");
        *exterior = GWY_EXTERIOR_FIXED_VALUE;
        *fill_value = 0.0;
    }
}

static void
row_convolve_direct(const GwyDataField *field,
                    guint col, guint row,
                    guint width, guint height,
                    GwyDataField *target,
                    guint targetcol, guint targetrow,
                    const GwyDataLine *kernel,
                    RowExtendFunc extend_row,
                    gdouble fill_value)
{
    guint xres = field->xres;
    guint kres = kernel->res;
    const gdouble *kdata = kernel->data;
    guint size = width + kres - 1;
    guint extend_left, extend_right, i, j, k;
    gdouble *extdata = g_new(gdouble, size);

    make_symmetrical_extension(width, size, &extend_left, &extend_right);

    /* The direct method is used only if kres ≪ res.  Don't bother optimising
     * the boundaries, just make the inner loop tight. */
    for (i = 0; i < height; i++) {
        gdouble *trow = target->data + (targetrow + i)*target->xres + targetcol;
        extend_row(field->data + (row + i)*xres, extdata,
                   col, width, xres, extend_left, extend_right, fill_value);
        for (j = 0; j < width; j++) {
            const gdouble *d = extdata + extend_left + kres/2 + j;
            gdouble v = 0.0;
            for (k = 0; k < kres; k++, d--)
                v += kdata[k] * *d;
            trow[j] = v;
        }
    }

    g_free(extdata);
}

static inline void
complex_multiply_with(fftw_complex *a, const fftw_complex *b)
{
    gdouble re = (*a)[0]*(*b)[0] - (*a)[1]*(*b)[1];

    (*a)[1] = (*a)[1]*(*b)[0] + (*a)[0]*(*b)[1];
    (*a)[0] = re;
}

static void
row_convolve_fft(GwyDataField *field,
                 guint col, guint row,
                 guint width, guint height,
                 GwyDataField *target,
                 guint targetcol, guint targetrow,
                 GwyDataLine *kernel,
                 RowExtendFunc extend_row,
                 gdouble fill_value)
{
    guint xres = field->xres, kres = kernel->res;
    guint size = gwy_fft_find_nice_size(width + kres - 1);
    // The innermost (contiguous) dimension of R2C the complex output is
    // slightly larger than the real input.  Note @cstride is measured in
    // fftw_complex, multiply it by 2 for doubles.
    guint cstride = size/2 + 1;
    guint extend_left, extend_right, i, j;
    gdouble *extdata = g_new(gdouble, size);
    fftw_complex *datac = g_new(fftw_complex, 2*cstride);
    fftw_complex *kernelc = datac + cstride;
    fftw_plan dplan, cplan;
    gdouble q;

    /* The R2C plan for transforming the extended data row (or kernel). */
    dplan = fftw_plan_dft_r2c_1d(size, extdata, datac,
                                 FFTW_DESTROY_INPUT | _GWY_FFTW_PATIENCE);
    g_assert(dplan);
    /* The C2R plan the backward transform of the convolution of each row. */
    cplan = fftw_plan_dft_c2r_1d(size, datac, extdata, _GWY_FFTW_PATIENCE);
    g_assert(cplan);

    // Transform the kernel.
    extend_kernel_row(kernel->data, kres, extdata, size);
    fftw_execute(dplan);
    gwy_assign(kernelc, datac, cstride);

    // Convolve rows
    make_symmetrical_extension(width, size, &extend_left, &extend_right);
    q = 1.0/size;
    for (i = 0; i < height; i++) {
        extend_row(field->data + (row + i)*xres, extdata,
                   col, width, xres, extend_left, extend_right, fill_value);
        fftw_execute(dplan);
        for (j = 0; j < cstride; j++) {
            complex_multiply_with(datac + j, kernelc + j);
            datac[j][0] *= q;
            datac[j][1] *= q;
        }
        fftw_execute(cplan);
        gwy_assign(target->data + (targetrow + i)*target->xres + targetcol,
                   extdata + extend_left,
                   width);
    }

    fftw_destroy_plan(cplan);
    fftw_destroy_plan(dplan);
    g_free(datac);
    g_free(extdata);
}

/**
 * gwy_data_field_area_ext_row_convolve:
 * @field: A two-dimensional data field.
 * @col: Upper-left column coordinate.
 * @row: Upper-left row coordinate.
 * @width: Area width (number of columns).
 * @height: Area height (number of rows).
 * @target: A two-dimensional data field where the result will be placed.
 *          It may be @field for an in-place modification.
 * @kernel: Kernel to convolve @field with.
 * @exterior: Exterior pixels handling.
 * @fill_value: The value to use with %GWY_EXTERIOR_FIXED_VALUE exterior.
 * @as_integral: %TRUE for normalisation and units as a convolution integral,
 *               %FALSE as a sum.
 *
 * Convolve a field row-wise with a one-dimensional kernel.
 *
 * Pixel dimensions of @target may match either @field or just the rectangular
 * area.  In the former case the result is written in the same rectangular
 * area; in the latter case the result fills the entire @target.
 *
 * The convolution is performed with the kernel centred on the respective field
 * pixels.  For an odd-sized kernel this holds precisely.  For an even-sized
 * kernel this means the kernel centre is placed 0.5 pixel to the left
 * (towards lower column indices) from the respective field pixel.
 *
 * See gwy_data_field_extend() for what constitutes the exterior and how it is
 * handled.
 *
 * If @as_integral is %FALSE the function performs a simple discrete
 * convolution sum and the value units of @target are set to product of @field
 * and @kernel units.
 *
 * If @as_integral is %TRUE the function approximates a convolution integral.
 * In this case @kernel should be a sampled continuous transfer function.
 * The units of value @target are set to product of @field and @kernel value
 * units and @field lateral units.  Furthermore, the discrete sum is multiplied
 * by the pixel size (i.e. d@x in the integral).
 *
 * In either case, the lateral units and pixel size of @kernel are assumed to
 * be the same as for a @field's row (albeit not checked), because
 * the convolution does not make sense otherwise.
 *
 * Since: 2.49
 **/
void
gwy_data_field_area_ext_row_convolve(GwyDataField *field,
                                     guint col, guint row,
                                     guint width, guint height,
                                     GwyDataField *target,
                                     GwyDataLine *kernel,
                                     GwyExteriorType exterior,
                                     gdouble fill_value,
                                     gboolean as_integral)
{
    guint xres, yres;
    guint targetcol, targetrow;
    GwySIUnit *funit, *kunit, *tunit;
    RowExtendFunc extend_row;
    gdouble dx, dy;

    g_return_if_fail(GWY_IS_DATA_FIELD(field));
    g_return_if_fail(GWY_IS_DATA_FIELD(target));
    g_return_if_fail(GWY_IS_DATA_LINE(kernel));
    xres = field->xres;
    yres = field->yres;
    g_return_if_fail(col < xres && row < yres);
    g_return_if_fail(width > 0 && height > 0);
    g_return_if_fail(col + width <= xres && row + height <= yres);
    g_return_if_fail((target->xres == xres && target->yres == yres)
                     || (target->xres == width && target->yres == height));
    targetcol = (target->xres == xres) ? col : 0;
    targetrow = (target->yres == yres) ? row : 0;

    ensure_defined_exterior(&exterior, &fill_value);
    if (!(extend_row = _gwy_get_row_extend_func(exterior)))
        return;

    if (width <= 12 || kernel->res <= 3.0*(log(width) - 1.0)) {
        row_convolve_direct(field, col, row, width, height,
                            target, targetcol, targetrow,
                            kernel, extend_row, fill_value);
    }
    else {
        row_convolve_fft(field, col, row, width, height,
                         target, targetcol, targetrow,
                         kernel, extend_row, fill_value);
    }

    dx = field->xreal/field->xres;
    dy = field->yreal/field->yres;
    if (target != field) {
        _gwy_assign_si_unit(field->si_unit_xy, &target->si_unit_xy);
        target->xreal = dx*target->xres;
        target->yreal = dy*target->yres;
    }

    funit = gwy_data_field_get_si_unit_z(field);
    kunit = gwy_data_line_get_si_unit_y(kernel);
    tunit = gwy_data_field_get_si_unit_z(target);
    gwy_si_unit_multiply(funit, kunit, tunit);
    if (as_integral) {
        funit = gwy_data_field_get_si_unit_xy(field);
        gwy_si_unit_multiply(tunit, funit, tunit);
        gwy_data_field_multiply(target, dx);
    }

    gwy_data_field_invalidate(target);
}

/**
 * multiconvolve_direct:
 * @field: A two-dimensional data field.
 * @col: First ROI column.
 * @row: First RIO row.
 * @width: ROI width.
 * @height: RIO height.
 * @target: A two-dimensional data field where the result will be placed.
 *          It may be @field itself.
 * @targetcol: Column to place the result into @target.
 * @targetrow: Target to place the result into @target.
 * @kernel: Array of @nkernel equally-sized kernel.
 * @nkernel: Number of items in @kernel.
 * @combine_results: Function to combine results of individual convolutions
 *                   to the final result put to @target.  May be %NULL if
 *                   @nkernel is 1.
 * @extend_rect: Rectangle extending method.
 * @fill_value: The value to use with fixed-value exterior.
 *
 * Performs convolution of a field with a number of equally-sized kenrels,
 * combining the results of individual convolutions into a single value.
 */
static void
multiconvolve_direct(GwyDataField *field,
                     guint col, guint row,
                     guint width, guint height,
                     GwyDataField *target,
                     guint targetcol, guint targetrow,
                     GwyDataField **kernel,
                     guint nkernel,
                     DoubleArrayFunc combine_results,
                     RectExtendFunc extend_rect,
                     gdouble fill_value)
{
    guint xres, yres, kxres, kyres, xsize, ysize;
    guint extend_left, extend_right, extend_up, extend_down;
    gdouble *extdata;
    guint kno, i, j, ik, jk;

    g_return_if_fail(nkernel);
    g_return_if_fail(kernel);
    g_return_if_fail(nkernel == 1 || combine_results);

    xres = field->xres;
    yres = field->yres;
    kxres = kernel[0]->xres;
    kyres = kernel[0]->yres;
    for (kno = 1; kno < nkernel; kno++) {
        g_return_if_fail(kernel[kno]->xres == kxres
                         && kernel[kno]->yres == kyres);
    }

    xsize = width + kxres - 1;
    ysize = height + kyres - 1;
    extdata = g_new(gdouble, xsize*ysize);
    make_symmetrical_extension(width, xsize, &extend_left, &extend_right);
    make_symmetrical_extension(height, ysize, &extend_up, &extend_down);

    extend_rect(field->data, xres, extdata, xsize,
                col, row, width, height, xres, yres,
                extend_left, extend_right, extend_up, extend_down, fill_value);

    /* The direct method is used only if kres ≪ res.  Don't bother optimising
     * the boundaries, just make the inner loop tight. */
    if (nkernel == 1) {
        const gdouble *kdata = kernel[0]->data;
        for (i = 0; i < height; i++) {
            gdouble *trow = target->data + ((targetrow + i)*target->xres
                                            + targetcol);
            for (j = 0; j < width; j++) {
                const gdouble *id = extdata + (extend_up + kyres/2 + i)*xsize;
                gdouble v = 0.0;
                for (ik = 0; ik < kyres; ik++, id -= xsize) {
                    const gdouble *jd = id + extend_left + kxres/2 + j;
                    const gdouble *krow = kdata + ik*kxres;
                    for (jk = 0; jk < kxres; jk++, jd--)
                        v += krow[jk] * *jd;
                }
                trow[j] = v;
            }
        }
    }
    else {
        for (i = 0; i < height; i++) {
            gdouble *trow = target->data + ((targetrow + i)*target->xres
                                            + targetcol);
            for (j = 0; j < width; j++) {
                gdouble results[nkernel];
                for (kno = 0; kno < nkernel; kno++) {
                    const gdouble *id = extdata + (extend_up
                                                   + kyres/2 + i)*xsize;
                    const gdouble *kdata = kernel[kno]->data;
                    gdouble v = 0.0;
                    for (ik = 0; ik < kyres; ik++, id -= xsize) {
                        const gdouble *jd = id + extend_left + kxres/2 + j;
                        const gdouble *krow = kdata + ik*kxres;
                        for (jk = 0; jk < kxres; jk++, jd--)
                            v += krow[jk] * *jd;
                    }
                    results[kno] = v;
                }
                trow[j] = combine_results(results);
            }
        }
    }

    g_free(extdata);
}

static void
convolve_fft(GwyDataField *field,
             guint col, guint row,
             guint width, guint height,
             GwyDataField *target,
             guint targetcol, guint targetrow,
             GwyDataField *kernel,
             RectExtendFunc extend_rect,
             gdouble fill_value)
{
    guint xres = field->xres, yres = field->yres,
          kxres = kernel->xres, kyres = kernel->yres;
    guint xsize = gwy_fft_find_nice_size(width + kxres - 1);
    guint ysize = gwy_fft_find_nice_size(height + kyres - 1);
    /* The innermost (contiguous) dimension of R2C the complex output is
     * slightly larger than the real input.  If the transform is in-place the
     * input array needs to be padded.  Note @cstride is measured in
     * fftw_complex, multiply it by 2 for doubles. */
    guint cstride = xsize/2 + 1;
    /* Use in-place transforms.  Let FFTW figure out whether allocating
     * temporary buffers is worth it or not. */
    fftw_complex *datac = g_new(fftw_complex, cstride*ysize);
    gdouble *extdata = (gdouble*)datac;
    fftw_complex *kernelc = g_new(fftw_complex, cstride*ysize);
    guint extend_left, extend_right, extend_up, extend_down;
    guint i, k;
    fftw_plan kplan, dplan, cplan;
    gdouble q;

    make_symmetrical_extension(width, xsize, &extend_left, &extend_right);
    make_symmetrical_extension(height, ysize, &extend_up, &extend_down);

    /* The R2C plan for transforming the extended kernel.  The input is in
     * extdata to make it an out-of-place transform (this also means the input
     * data row stride is just xsize, not 2*cstride). */
    kplan = fftw_plan_dft_r2c_2d(ysize, xsize, extdata, kernelc,
                                 FFTW_DESTROY_INPUT | _GWY_FFTW_PATIENCE);
    g_assert(kplan);
    // The R2C plan for transforming the extended data.  This one is in-place.
    dplan = fftw_plan_dft_r2c_2d(ysize, xsize, extdata, datac,
                                 FFTW_DESTROY_INPUT | _GWY_FFTW_PATIENCE);
    g_assert(dplan);
    // The C2R plan the backward transform of the convolution.  The input
    // is in fact in kernelc to make it an out-of-place transform.  So, again,
    // the output has cstride of only xsize.
    cplan = fftw_plan_dft_c2r_2d(ysize, xsize, kernelc, extdata,
                                 _GWY_FFTW_PATIENCE);
    g_assert(cplan);

    // Transform the kernel.
    extend_kernel_rect(kernel->data, kxres, kyres,
                       extdata, xsize, ysize, xsize);
    fftw_execute(kplan);

    // Convolve
    extend_rect(field->data, xres, extdata, 2*cstride,
                col, row, width, height, xres, yres,
                extend_left, extend_right, extend_up, extend_down, fill_value);
    fftw_execute(dplan);

    q = 1.0/(xsize*ysize);
    for (k = 0; k < cstride*ysize; k++) {
        complex_multiply_with(kernelc + k, datac + k);
        kernelc[k][0] *= q;
        kernelc[k][1] *= q;
    }
    fftw_execute(cplan);

    for (i = 0; i < height; i++) {
        gwy_assign(target->data + (targetrow + i)*target->xres + targetcol,
                   extdata + (extend_up + i)*xsize + extend_left,
                   width);
    }

    fftw_destroy_plan(kplan);
    fftw_destroy_plan(cplan);
    fftw_destroy_plan(dplan);
    g_free(kernelc);
    g_free(datac);
}

/**
 * gwy_data_field_area_ext_convolve:
 * @field: A two-dimensional data field.
 * @col: Upper-left column coordinate.
 * @row: Upper-left row coordinate.
 * @width: Area width (number of columns).
 * @height: Area height (number of rows).
 * @target: A two-dimensional data field where the result will be placed.
 *          It may be @field for an in-place modification.
 * @kernel: Kernel to convolve @field with.
 * @exterior: Exterior pixels handling.
 * @fill_value: The value to use with %GWY_EXTERIOR_FIXED_VALUE exterior.
 * @as_integral: %TRUE for normalisation and units as a convolution integral,
 *               %FALSE as a sum.
 *
 * Convolve a field with a two-dimensional kernel.
 *
 * Pixel dimensions of @target may match either @field or just the rectangular
 * area.  In the former case the result is written in the same rectangular
 * area; in the latter case the result fills the entire @target.
 *
 * The convolution is performed with the kernel centred on the respective field
 * pixels.  For directions in which the kernel has an odd size this holds
 * precisely.  For an even-sized kernel this means the kernel centre is placed
 * 0.5 pixel left or up (towards lower indices) from the respective field
 * pixel.
 *
 * See gwy_data_field_extend() for what constitutes the exterior and how it is
 * handled.
 *
 * If @as_integral is %FALSE the function performs a simple discrete
 * convolution sum and the value units of @target are set to product of @field
 * and @kernel units.
 *
 * If @as_integral is %TRUE the function approximates a convolution integral.
 * In this case @kernel should be a sampled continuous transfer function.
 * The units of value @target are set to product of @field and @kernel value
 * units and @field lateral units squared.  Furthermore, the discrete sum is
 * multiplied by the pixel size (i.e. d@x d@y in the integral).
 *
 * In either case, the lateral units and pixel size of @kernel are assumed to
 * be the same as for @field (albeit not checked), because the convolution does
 * not make sense otherwise.
 *
 * Since: 2.49
 **/
void
gwy_data_field_area_ext_convolve(GwyDataField *field,
                                 guint col, guint row,
                                 guint width, guint height,
                                 GwyDataField *target,
                                 GwyDataField *kernel,
                                 GwyExteriorType exterior,
                                 gdouble fill_value,
                                 gboolean as_integral)
{
    guint xres, yres, size;
    guint targetcol, targetrow;
    GwySIUnit *funit, *kunit, *tunit;
    RectExtendFunc extend_rect;
    gdouble dx, dy;

    g_return_if_fail(GWY_IS_DATA_FIELD(field));
    g_return_if_fail(GWY_IS_DATA_FIELD(target));
    g_return_if_fail(GWY_IS_DATA_FIELD(kernel));
    xres = field->xres;
    yres = field->yres;
    g_return_if_fail(col < xres && row < yres);
    g_return_if_fail(width > 0 && height > 0);
    g_return_if_fail(col + width <= xres && row + height <= yres);
    g_return_if_fail((target->xres == xres && target->yres == yres)
                     || (target->xres == width && target->yres == height));
    targetcol = (target->xres == xres) ? col : 0;
    targetrow = (target->yres == yres) ? row : 0;

    ensure_defined_exterior(&exterior, &fill_value);
    if (!(extend_rect =_gwy_get_rect_extend_func(exterior)))
        return;

    size = height*width;
    if (size <= 25) {
        multiconvolve_direct(field, col, row, width, height,
                             target, targetcol, targetrow,
                             &kernel, 1, NULL, extend_rect, fill_value);
    }
    else {
        convolve_fft(field, col, row, width, height,
                     target, targetcol, targetrow,
                     kernel, extend_rect, fill_value);
    }

    dx = field->xreal/field->xres;
    dy = field->yreal/field->yres;
    if (target != field) {
        funit = gwy_data_field_get_si_unit_xy(field);
        tunit = gwy_data_field_get_si_unit_xy(target);
        gwy_serializable_clone(G_OBJECT(funit), G_OBJECT(tunit));
        target->xreal = dx*target->xres;
        target->yreal = dy*target->yres;
    }

    funit = gwy_data_field_get_si_unit_z(field);
    kunit = gwy_data_field_get_si_unit_z(kernel);
    tunit = gwy_data_field_get_si_unit_z(target);
    gwy_si_unit_multiply(funit, kunit, tunit);
    if (as_integral) {
        funit = gwy_data_field_get_si_unit_xy(field);
        gwy_si_unit_power_multiply(tunit, 1, funit, 2, tunit);
        gwy_data_field_multiply(target, dx*dy);
    }

    gwy_data_field_invalidate(target);
}

/**
 * gwy_data_field_area_filter_mean:
 * @data_field: A data field to apply the filter to.
 * @size: Averaged area size.
 * @col: Upper-left column coordinate.
 * @row: Upper-left row coordinate.
 * @width: Area width (number of columns).
 * @height: Area height (number of rows).
 *
 * Filters a rectangular part of a data field with mean filter of size @size.
 *
 * This method is a simple gwy_data_field_area_gather() wrapper.
 **/
void
gwy_data_field_area_filter_mean(GwyDataField *data_field,
                                gint size,
                                gint col, gint row,
                                gint width, gint height)
{
    gwy_data_field_area_gather(data_field, data_field, NULL,
                               size, size, TRUE,
                               col, row, width, height);
}

/**
 * gwy_data_field_filter_mean:
 * @data_field: A data field to apply the filter to.
 * @size: Averaged area size.
 *
 * Filters a data field with mean filter of size @size.
 **/
void
gwy_data_field_filter_mean(GwyDataField *data_field,
                           gint size)
{
    g_return_if_fail(GWY_IS_DATA_FIELD(data_field));
    gwy_data_field_area_filter_mean(data_field, size, 0, 0,
                                    data_field->xres, data_field->yres);
}

/**
 * gwy_data_field_area_filter_rms:
 * @data_field: A data field to apply RMS filter to.
 * @size: Area size.
 * @col: Upper-left column coordinate.
 * @row: Upper-left row coordinate.
 * @width: Area width (number of columns).
 * @height: Area height (number of rows).
 *
 * Filters a rectangular part of a data field with RMS filter of size @size.
 *
 * RMS filter computes root mean square in given area.
 **/
void
gwy_data_field_area_filter_rms(GwyDataField *data_field,
                               gint size,
                               gint col, gint row,
                               gint width, gint height)
{
    GwyDataField *avg2, *buffer;
    gint i, j;
    const gdouble *arow;
    gdouble *drow;

    g_return_if_fail(GWY_IS_DATA_FIELD(data_field));
    g_return_if_fail(size > 0);
    g_return_if_fail(col >= 0 && row >= 0
                     && width > 0 && height > 0
                     && col + width <= data_field->xres
                     && row + height <= data_field->yres);

    if (size == 1) {
        gwy_data_field_clear(data_field);
        return;
    }

    avg2 = gwy_data_field_area_extract(data_field, col, row, width, height);
    for (i = 0; i < width*height; i++)
        avg2->data[i] *= avg2->data[i];

    buffer = gwy_data_field_new_alike(avg2, FALSE);
    gwy_data_field_area_gather(avg2, avg2, buffer,
                               size, size, TRUE,
                               0, 0, width, height);
    gwy_data_field_area_gather(data_field, data_field, buffer,
                               size, size, TRUE,
                               col, row, width, height);
    g_object_unref(buffer);

    for (i = 0; i < height; i++) {
        arow = avg2->data + i*width;
        drow = data_field->data + (i + row)*data_field->xres + col;
        for (j = 0; j < width; j++) {
            drow[j] = arow[j] - drow[j]*drow[j];
            drow[j] = sqrt(MAX(drow[j], 0.0));
        }
    }
    g_object_unref(avg2);

    gwy_data_field_invalidate(data_field);
}

/**
 * gwy_data_field_filter_rms:
 * @data_field: A data field to apply RMS filter to.
 * @size: Area size.
 *
 * Filters a data field with RMS filter.
 **/
void
gwy_data_field_filter_rms(GwyDataField *data_field,
                          gint size)
{
    g_return_if_fail(GWY_IS_DATA_FIELD(data_field));
    gwy_data_field_area_filter_rms(data_field, size, 0, 0,
                                   data_field->xres, data_field->yres);
}

/**
 * gwy_data_field_filter_canny:
 * @data_field: A data field to apply the filter to.
 * @threshold: Slope detection threshold (range 0..1).
 *
 * Filters a rectangular part of a data field with canny edge detector filter.
 **/
void
gwy_data_field_filter_canny(GwyDataField *data_field,
                            gdouble threshold)
{
    GwyDataField *sobel_horizontal;
    GwyDataField *sobel_vertical;
    gint i, j, k;
    gint xres, yres;
    gdouble angle;
    gint pass;
    gdouble *data;

    g_return_if_fail(GWY_IS_DATA_FIELD(data_field));
    sobel_horizontal = gwy_data_field_duplicate(data_field);
    sobel_vertical = gwy_data_field_duplicate(data_field);

    gwy_data_field_filter_sobel(sobel_horizontal, GWY_ORIENTATION_HORIZONTAL);
    gwy_data_field_filter_sobel(sobel_vertical, GWY_ORIENTATION_VERTICAL);

    data = data_field->data;
    xres = data_field->xres;
    yres = data_field->yres;
    for (k = 0; k < xres*yres; k++) {
        data[k] = fabs(sobel_horizontal->data[k])
                  + fabs(sobel_vertical->data[k]);
    }
    gwy_data_field_invalidate(data_field);

    threshold = gwy_data_field_get_min(data_field)
                + (gwy_data_field_get_max(data_field)
                   - gwy_data_field_get_min(data_field))*threshold;

    /* We do not need sobel array more, so use sobel_horizontal to store data
     * results. */
    for (i = 1; i < yres-1; i++) {
        for (j = 1; j < xres-1; j++) {
            pass = 0;
            if (data[i*xres + j] > threshold) {
                angle = atan2(sobel_vertical->data[i*xres + j],
                              sobel_horizontal->data[i*xres + j]);

                if (angle < 0.3925 || angle > 5.8875
                    || (angle > 2.7475 && angle < 3.5325)) {
                    if (data[j + 1 + xres*i] > threshold)
                        pass = 1;
                }
                else if ((angle > 1.178 && angle < 1.9632)
                         || (angle > 4.318 && angle < 5.1049)) {
                    if (data[j + 1 + xres*(i + 1)] > threshold)
                        pass = 1;
                }
                else {
                    if (data[j + xres*(i + 1)] > threshold)
                        pass = 1;
                }
            }
            sobel_horizontal->data[i*xres + j] = pass;
        }
    }
    /*result is now in sobel_horizontal field*/
    gwy_data_field_copy(sobel_horizontal, data_field, FALSE);

    g_object_unref(sobel_horizontal);
    g_object_unref(sobel_vertical);

    /*thin the lines*/
    thin_data_field(data_field);
}

 /**
 * gwy_data_field_area_filter_laplacian:
 * @data_field: A data field to apply the filter to.
 * @col: Upper-left column coordinate.
 * @row: Upper-left row coordinate.
 * @width: Area width (number of columns).
 * @height: Area height (number of rows).
 *
 * Filters a rectangular part of a data field with Laplacian filter.
 **/
void
gwy_data_field_area_filter_laplacian(GwyDataField *data_field,
                                     gint col, gint row,
                                     gint width, gint height)
{
    const gdouble laplace[] = {
        0,  1, 0,
        1, -4, 1,
        0,  1, 0,
    };

    g_return_if_fail(GWY_IS_DATA_FIELD(data_field));
    gwy_data_field_area_convolve_3x3(data_field, laplace,
                                     col, row, width, height);
}

/**
 * gwy_data_field_filter_laplacian:
 * @data_field: A data field to apply the filter to.
 *
 * Filters a data field with Laplacian filter.
 **/
void
gwy_data_field_filter_laplacian(GwyDataField *data_field)
{
    g_return_if_fail(GWY_IS_DATA_FIELD(data_field));
    gwy_data_field_area_filter_laplacian(data_field, 0, 0,
                                         data_field->xres, data_field->yres);
}

 /**
 * gwy_data_field_area_filter_laplacian_of_gaussians:
 * @data_field: A data field to apply the filter to.
 * @col: Upper-left column coordinate.
 * @row: Upper-left row coordinate.
 * @width: Area width (number of columns).
 * @height: Area height (number of rows).
 *
 * Filters a rectangular part of a data field
 * with Laplacian of Gaussians filter.
 *
 * Since: 2.23
 **/
void
gwy_data_field_area_filter_laplacian_of_gaussians(GwyDataField *data_field,
                                                  gint col, gint row,
                                                  gint width,
                                                  gint height)
{
    /* optimized mexican hat from Scharr's works */
    const gdouble laplacian_of_gaussians_data[] = {
          1, -12,    3, -12,   1,
        -12,  78,  167,  78, -12,
          3, 167, -902, 167,   3,
        -12,  78,  167,  78, -12,
          1, -12,    3, -12,   1,
    };

    GwyDataField *laplacian_of_gaussians;
    gint i, j;

    laplacian_of_gaussians = gwy_data_field_new(5, 5, 5.0, 5.0, TRUE);
    for (i = 0; i < 5; i++)
        for (j = 0; j < 5; j++)
            gwy_data_field_set_val(laplacian_of_gaussians, j, i,
                                   laplacian_of_gaussians_data[i*5+j]);
    g_return_if_fail(GWY_IS_DATA_FIELD(data_field));
    gwy_data_field_area_convolve(data_field, laplacian_of_gaussians,
                                 col, row, width, height);

    g_object_unref(laplacian_of_gaussians);
}

/**
 * gwy_data_field_filter_laplacian_of_gaussians:
 * @data_field: A data field to apply the filter to.
 *
 * Filters a data field with Laplacian of Gaussians filter.
 *
 * Since: 2.23
 **/
void
gwy_data_field_filter_laplacian_of_gaussians(GwyDataField *data_field)
{
    g_return_if_fail(GWY_IS_DATA_FIELD(data_field));
    gwy_data_field_area_filter_laplacian_of_gaussians(data_field, 0, 0,
                                                      data_field->xres,
                                                      data_field->yres);
}

/**
 * gwy_data_field_area_filter_sobel:
 * @data_field: A data field to apply the filter to.
 * @orientation: Filter orientation.
 * @col: Upper-left column coordinate.
 * @row: Upper-left row coordinate.
 * @width: Area width (number of columns).
 * @height: Area height (number of rows).
 *
 * Filters a rectangular part of a data field with a directional Sobel filter.
 **/
void
gwy_data_field_area_filter_sobel(GwyDataField *data_field,
                                 GwyOrientation orientation,
                                 gint col, gint row,
                                 gint width, gint height)
{
    static const gdouble hsobel[] = {
        0.25, 0, -0.25,
        0.5,  0, -0.5,
        0.25, 0, -0.25,
    };
    static const gdouble vsobel[] = {
         0.25,  0.5,  0.25,
         0,     0,    0,
        -0.25, -0.5, -0.25,
    };

    g_return_if_fail(GWY_IS_DATA_FIELD(data_field));
    if (orientation == GWY_ORIENTATION_HORIZONTAL)
        gwy_data_field_area_convolve_3x3(data_field, hsobel,
                                         col, row, width, height);
    else
        gwy_data_field_area_convolve_3x3(data_field, vsobel,
                                         col, row, width, height);
}

/**
 * gwy_data_field_filter_sobel:
 * @data_field: A data field to apply the filter to.
 * @orientation: Filter orientation.
 *
 * Filters a data field with a directional Sobel filter.
 **/
void
gwy_data_field_filter_sobel(GwyDataField *data_field,
                            GwyOrientation orientation)
{
    g_return_if_fail(GWY_IS_DATA_FIELD(data_field));
    gwy_data_field_area_filter_sobel(data_field, orientation, 0, 0,
                                     data_field->xres, data_field->yres);

}

/**
 * gwy_data_field_filter_sobel_total:
 * @data_field: A data field to apply the filter to.
 *
 * Filters a data field with total Sobel filter.
 *
 * Since: 2.31
 **/
void
gwy_data_field_filter_sobel_total(GwyDataField *data_field)
{
    GwyDataField *workspace;
    g_return_if_fail(GWY_IS_DATA_FIELD(data_field));
    workspace = gwy_data_field_duplicate(data_field);
    gwy_data_field_area_filter_sobel(data_field, GWY_ORIENTATION_HORIZONTAL,
                                     0, 0,
                                     data_field->xres, data_field->yres);
    gwy_data_field_area_filter_sobel(workspace, GWY_ORIENTATION_VERTICAL,
                                     0, 0,
                                     data_field->xres, data_field->yres);
    gwy_data_field_hypot_of_fields(data_field, data_field, workspace);
    g_object_unref(workspace);
}

/**
 * gwy_data_field_area_filter_prewitt:
 * @data_field: A data field to apply the filter to.
 * @orientation: Filter orientation.
 * @col: Upper-left column coordinate.
 * @row: Upper-left row coordinate.
 * @width: Area width (number of columns).
 * @height: Area height (number of rows).
 *
 * Filters a rectangular part of a data field with a directional Prewitt
 * filter.
 **/
void
gwy_data_field_area_filter_prewitt(GwyDataField *data_field,
                                   GwyOrientation orientation,
                                   gint col, gint row,
                                   gint width, gint height)
{
    static const gdouble hprewitt[] = {
        1.0/3.0, 0, -1.0/3.0,
        1.0/3.0, 0, -1.0/3.0,
        1.0/3.0, 0, -1.0/3.0,
    };
    static const gdouble vprewitt[] = {
         1.0/3.0,  1.0/3.0,  1.0/3.0,
         0,        0,        0,
        -1.0/3.0, -1.0/3.0, -1.0/3.0,
    };

    g_return_if_fail(GWY_IS_DATA_FIELD(data_field));
    if (orientation == GWY_ORIENTATION_HORIZONTAL)
        gwy_data_field_area_convolve_3x3(data_field, hprewitt,
                                         col, row, width, height);
    else
        gwy_data_field_area_convolve_3x3(data_field, vprewitt,
                                         col, row, width, height);
}

/**
 * gwy_data_field_filter_prewitt:
 * @data_field: A data field to apply the filter to.
 * @orientation: Filter orientation.
 *
 * Filters a data field with Prewitt filter.
 **/
void
gwy_data_field_filter_prewitt(GwyDataField *data_field,
                              GwyOrientation orientation)
{
    g_return_if_fail(GWY_IS_DATA_FIELD(data_field));
    gwy_data_field_area_filter_prewitt(data_field, orientation, 0, 0,
                                       data_field->xres, data_field->yres);
}

/**
 * gwy_data_field_filter_prewitt_total:
 * @data_field: A data field to apply the filter to.
 *
 * Filters a data field with total Prewitt filter.
 *
 * Since: 2.31
 **/
void
gwy_data_field_filter_prewitt_total(GwyDataField *data_field)
{
    GwyDataField *workspace;
    g_return_if_fail(GWY_IS_DATA_FIELD(data_field));
    workspace = gwy_data_field_duplicate(data_field);
    gwy_data_field_area_filter_prewitt(data_field, GWY_ORIENTATION_HORIZONTAL,
                                       0, 0,
                                       data_field->xres, data_field->yres);
    gwy_data_field_area_filter_prewitt(workspace, GWY_ORIENTATION_VERTICAL,
                                       0, 0,
                                       data_field->xres, data_field->yres);
    gwy_data_field_hypot_of_fields(data_field, data_field, workspace);
    g_object_unref(workspace);
}

/**
 * gwy_data_field_filter_slope:
 * @data_field: A data field to apply the filter to.
 * @xder: Data field where the x-derivarive is to be stored, or %NULL if you
 *        are only interested in the y-derivarive.
 * @yder: Data field where the y-derivarive is to be stored, or %NULL if you
 *        are only interested in the x-derivarive.
 *
 * Calculates x and y derivaties for an entire field.
 *
 * The derivatives are calculated as the simple symmetrical differences (in
 * physical units, not pixel-wise), except at the edges where the differences
 * are one-sided.
 *
 * Since: 2.37
 **/
void
gwy_data_field_filter_slope(GwyDataField *data_field,
                            GwyDataField *xder,
                            GwyDataField *yder)
{
    guint xres, yres, i, j;
    gdouble dx, dy;
    const gdouble *d;
    gdouble *bx, *by;

    g_return_if_fail(GWY_IS_DATA_FIELD(data_field));
    g_return_if_fail(!xder || GWY_IS_DATA_FIELD(xder));
    g_return_if_fail(!yder || GWY_IS_DATA_FIELD(yder));
    if (!xder && !yder)
        return;

    xres = data_field->xres;
    yres = data_field->yres;
    gwy_data_field_resample(xder, xres, yres, GWY_INTERPOLATION_NONE);
    gwy_data_field_resample(yder, xres, yres, GWY_INTERPOLATION_NONE);
    dx = gwy_data_field_get_xmeasure(data_field);
    dy = gwy_data_field_get_ymeasure(data_field);
    d = data_field->data;
    bx = xder ? xder->data : NULL;
    by = yder ? yder->data : NULL;

    for (i = 0; i < yres; i++) {
        const gdouble *row = d + i*xres, *prev = row - xres, *next = row + xres;
        gdouble *bxrow = bx ? bx + i*xres : NULL;
        gdouble *byrow = by ? by + i*xres : NULL;

        for (j = 0; j < xres; j++) {
            gdouble xd, yd;

            if (bxrow) {
                if (!j)
                    xd = row[j + 1] - row[j];
                else if (j == xres-1)
                    xd = row[j] - row[j - 1];
                else
                    xd = (row[j + 1] - row[j - 1])/2;

                bxrow[j] = xd/dx;
            }

            if (byrow) {
                if (!i)
                    yd = next[j] - row[j];
                else if (i == yres-1)
                    yd = row[j] - prev[j];
                else
                    yd = (next[j] - prev[j])/2;

                byrow[j] = yd/dy;
            }
        }
    }

    if (xder)
        gwy_data_field_invalidate(xder);
    if (yder)
        gwy_data_field_invalidate(yder);
}

/**
 * gwy_data_field_area_filter_dechecker:
 * @data_field: A data field to apply the filter to.
 * @col: Upper-left column coordinate.
 * @row: Upper-left row coordinate.
 * @width: Area width (number of columns).
 * @height: Area height (number of rows).
 *
 * Filters a rectangular part of a data field with 5x5 checker pattern removal
 * filter.
 *
 * Since: 2.1
 **/
void
gwy_data_field_area_filter_dechecker(GwyDataField *data_field,
                                     gint col, gint row,
                                     gint width, gint height)
{
    enum { size = 5 };
    static const gdouble checker[size*size] = {
         0.0,        1.0/144.0, -1.0/72.0,  1.0/144.0,  0.0,
         1.0/144.0, -1.0/18.0,   1.0/9.0,  -1.0/18.0,   1.0/144.0,
        -1.0/72.0,   1.0/9.0,    7.0/9.0,   1.0/9.0,   -1.0/72.0,
         1.0/144.0, -1.0/18.0,   1.0/9.0,  -1.0/18.0,   1.0/144.0,
         0.0,        1.0/144.0, -1.0/72.0,  1.0/144.0,  0.0,
    };
    GwyDataField *kernel;

    g_return_if_fail(GWY_IS_DATA_FIELD(data_field));
    kernel = gwy_data_field_new(size, size, 1.0, 1.0, FALSE);
    gwy_assign(kernel->data, checker, size*size);
    gwy_data_field_area_convolve(data_field, kernel, col, row, width, height);
    g_object_unref(kernel);
}

/**
 * gwy_data_field_filter_dechecker:
 * @data_field: A data field to apply the filter to.
 *
 * Filters a data field with 5x5 checker pattern removal filter.
 *
 * Since: 2.1
 **/
void
gwy_data_field_filter_dechecker(GwyDataField *data_field)
{
    g_return_if_fail(GWY_IS_DATA_FIELD(data_field));
    gwy_data_field_area_filter_dechecker(data_field, 0, 0,
                                         data_field->xres, data_field->yres);
}

/**
 * gwy_data_field_area_filter_gaussian:
 * @data_field: A data field to apply the filter to.
 * @sigma: The sigma parameter of the Gaussian.
 * @col: Upper-left column coordinate.
 * @row: Upper-left row coordinate.
 * @width: Area width (number of columns).
 * @height: Area height (number of rows).
 *
 * Filters a rectangular part of a data field with a Gaussian filter.
 *
 * The Gausian is normalized, i.e. it is sum-preserving.
 *
 * Since: 2.4
 **/
void
gwy_data_field_area_filter_gaussian(GwyDataField *data_field,
                                    gdouble sigma,
                                    gint col, gint row,
                                    gint width, gint height)
{
    GwyDataLine *kernel;
    gdouble x;
    gint res, i;

    g_return_if_fail(GWY_IS_DATA_FIELD(data_field));
    g_return_if_fail(sigma >= 0.0);
    if (sigma == 0.0)
        return;

    res = (gint)ceil(5.0*sigma);
    res = 2*res + 1;
    /* FIXME */
    i = 3*MIN(data_field->xres, data_field->yres);
    if (res > i) {
        res = i;
        if (res % 2 == 0)
            res--;
    }

    kernel = gwy_data_line_new(res, 1.0, FALSE);
    for (i = 0; i < res; i++) {
        x = i - (res - 1)/2.0;
        x /= sigma;
        kernel->data[i] = exp(-x*x/2.0);
    }
    gwy_data_line_multiply(kernel, 1.0/gwy_data_line_get_sum(kernel));
    gwy_data_field_area_convolve_1d(data_field, kernel,
                                    GWY_ORIENTATION_HORIZONTAL,
                                    col, row, width, height);
    gwy_data_field_area_convolve_1d(data_field, kernel,
                                    GWY_ORIENTATION_VERTICAL,
                                    col, row, width, height);
    g_object_unref(kernel);
}

/**
 * gwy_data_field_filter_gaussian:
 * @data_field: A data field to apply the filter to.
 * @sigma: The sigma parameter of the Gaussian.
 *
 * Filters a data field with a Gaussian filter.
 *
 * Since: 2.4
 **/
void
gwy_data_field_filter_gaussian(GwyDataField *data_field,
                               gdouble sigma)
{
    g_return_if_fail(GWY_IS_DATA_FIELD(data_field));
    gwy_data_field_area_filter_gaussian(data_field, sigma, 0, 0,
                                         data_field->xres, data_field->yres);
}

/**
 * gwy_data_field_area_filter_median:
 * @data_field: A data field to apply the filter to.
 * @size: Size of area to take median of.
 * @col: Upper-left column coordinate.
 * @row: Upper-left row coordinate.
 * @width: Area width (number of columns).
 * @height: Area height (number of rows).
 *
 * Filters a rectangular part of a data field with median filter.
 **/
void
gwy_data_field_area_filter_median(GwyDataField *data_field,
                                  gint size,
                                  gint col, gint row,
                                  gint width, gint height)
{

    gint rowstride;
    gint i, j, k, len;
    gint xfrom, xto, yfrom, yto;
    gdouble *buffer, *data, *kernel;

    g_return_if_fail(GWY_IS_DATA_FIELD(data_field));
    g_return_if_fail(size > 0);
    g_return_if_fail(col >= 0 && row >= 0
                     && width > 0 && height > 0
                     && col + width <= data_field->xres
                     && row + height <= data_field->yres);

    buffer = g_new(gdouble, width*height);
    kernel = g_new(gdouble, size*size);
    rowstride = data_field->xres;
    data = data_field->data + rowstride*row + col;

    for (i = 0; i < height; i++) {
        yfrom = MAX(0, i - (size-1)/2);
        yto = MIN(height-1, i + size/2);
        for (j = 0; j < width; j++) {
            xfrom = MAX(0, j - (size-1)/2);
            xto = MIN(width-1, j + size/2);
            len = xto - xfrom + 1;
            for (k = yfrom; k <= yto; k++)
                gwy_assign(kernel + len*(k - yfrom),
                           data + k*rowstride + xfrom,
                           len);
            buffer[i*width + j] = gwy_math_median(len*(yto - yfrom + 1),
                                                  kernel);
        }
    }

    g_free(kernel);
    for (i = 0; i < height; i++)
        gwy_assign(data + i*rowstride, buffer + i*width, width);
    g_free(buffer);
    gwy_data_field_invalidate(data_field);
}

/**
 * gwy_data_field_filter_median:
 * @data_field: A data field to apply the filter to.
 * @size: Size of area to take median of.
 *
 * Filters a data field with median filter.
 **/
void
gwy_data_field_filter_median(GwyDataField *data_field,
                             gint size)
{
    g_return_if_fail(GWY_IS_DATA_FIELD(data_field));
    gwy_data_field_area_filter_median(data_field, size, 0, 0,
                                      data_field->xres, data_field->yres);
}

/**
 * gwy_data_field_area_filter_conservative:
 * @data_field: A data field to apply the filter to.
 * @size: Filtered area size.
 * @col: Upper-left column coordinate.
 * @row: Upper-left row coordinate.
 * @width: Area width (number of columns).
 * @height: Area height (number of rows).
 *
 * Filters a rectangular part of a data field with conservative denoise filter.
 **/
void
gwy_data_field_area_filter_conservative(GwyDataField *data_field,
                                        gint size,
                                        gint col, gint row,
                                        gint width, gint height)
{
    gint xres, yres, i, j, ii, jj;
    gdouble maxval, minval;
    gdouble *data;
    GwyDataField *hlp_df;

    gwy_debug("");
    g_return_if_fail(GWY_IS_DATA_FIELD(data_field));
    g_return_if_fail(size > 0);
    xres = data_field->xres;
    yres = data_field->yres;
    g_return_if_fail(col >= 0 && row >= 0
                     && width > 0 && height > 0
                     && col + width <= xres
                     && row + height <= yres);
    if (size == 1)
        return;
    if (size > width || size > height) {
        g_warning("Kernel size larger than field area size.");
        return;
    }

    hlp_df = gwy_data_field_new(width, height, 1.0, 1.0, FALSE);

    data = data_field->data;
    for (i = 0; i < height; i++) {
        gint ifrom = MAX(0, i + row - (size-1)/2);
        gint ito = MIN(yres-1, i + row + size/2);

        for (j = 0; j < width; j++) {
            gint jfrom = MAX(0, j + col - (size-1)/2);
            gint jto = MIN(xres-1, j + col + size/2);

            maxval = -G_MAXDOUBLE;
            minval = G_MAXDOUBLE;
            for (ii = 0; ii <= ito - ifrom; ii++) {
                gdouble *drow = data + (ifrom + ii)*xres + jfrom;

                for (jj = 0; jj <= jto - jfrom; jj++) {
                    if (i + row == ii + ifrom && j + col == jj + jfrom)
                        continue;

                    if (drow[jj] < minval)
                        minval = drow[jj];
                    if (drow[jj] > maxval)
                        maxval = drow[jj];
                }
            }

            hlp_df->data[i*width + j] = CLAMP(data[(i + row)*xres + j + col],
                                              minval, maxval);
        }
    }
    /* fix bottom right corner for size == 2 */
    if (size == 2)
        hlp_df->data[height*width - 1] = data[(row + height - 1)*xres
                                              + col + width - 1];

    gwy_data_field_area_copy(hlp_df, data_field, 0, 0, width, height, col, row);
    g_object_unref(hlp_df);
    gwy_data_field_invalidate(data_field);
}

/**
 * gwy_data_field_filter_conservative:
 * @data_field: A data field to apply the filter to.
 * @size: Filtered area size.
 *
 * Filters a data field with conservative denoise filter.
 **/
void
gwy_data_field_filter_conservative(GwyDataField *data_field,
                                   gint size)
{
    g_return_if_fail(GWY_IS_DATA_FIELD(data_field));
    gwy_data_field_area_filter_conservative(data_field, size, 0, 0,
                                            data_field->xres, data_field->yres);
}

/* Return 8*number-of-neighbours + number-of-neighbour-segments.
 * Segments must be separated by unset segments, so it is zero for all set
 * neighbourhood. */
static guint
neighbour_segments(const gdouble *d, gint xres, gint k)
{
    static const guint neightable[0x100] = {
        0,  9,  9,  17, 9,  18, 17, 25, 9,  17, 18, 25, 18, 26, 26, 33,
        9,  18, 18, 26, 17, 26, 25, 33, 18, 26, 27, 34, 26, 34, 34, 41,
        9,  18, 18, 26, 18, 27, 26, 34, 17, 25, 26, 33, 26, 34, 34, 41,
        18, 27, 27, 35, 26, 35, 34, 42, 26, 34, 35, 42, 34, 42, 42, 49,
        9,  18, 18, 26, 18, 27, 26, 34, 18, 26, 27, 34, 27, 35, 35, 42,
        18, 27, 27, 35, 26, 35, 34, 42, 27, 35, 36, 43, 35, 43, 43, 50,
        17, 26, 26, 34, 26, 35, 34, 42, 25, 33, 34, 41, 34, 42, 42, 49,
        26, 35, 35, 43, 34, 43, 42, 50, 34, 42, 43, 50, 42, 50, 50, 57,
        9,  18, 18, 26, 18, 27, 26, 34, 18, 26, 27, 34, 27, 35, 35, 42,
        17, 26, 26, 34, 25, 34, 33, 41, 26, 34, 35, 42, 34, 42, 42, 49,
        18, 27, 27, 35, 27, 36, 35, 43, 26, 34, 35, 42, 35, 43, 43, 50,
        26, 35, 35, 43, 34, 43, 42, 50, 34, 42, 43, 50, 42, 50, 50, 57,
        17, 26, 26, 34, 26, 35, 34, 42, 26, 34, 35, 42, 35, 43, 43, 50,
        25, 34, 34, 42, 33, 42, 41, 49, 34, 42, 43, 50, 42, 50, 50, 57,
        25, 34, 34, 42, 34, 43, 42, 50, 33, 41, 42, 49, 42, 50, 50, 57,
        33, 42, 42, 50, 41, 50, 49, 57, 41, 49, 50, 57, 49, 57, 57, 64,
    };
    guint b = 0;

    if (d[k-xres-1] > 0.0)
        b |= 1;
    if (d[k-xres] > 0.0)
        b |= 2;
    if (d[k-xres+1] > 0.0)
        b |= 4;
    if (d[k-1] > 0.0)
        b |= 8;
    if (d[k+1] > 0.0)
        b |= 16;
    if (d[k+xres-1] > 0.0)
        b |= 32;
    if (d[k+xres] > 0.0)
        b |= 64;
    if (d[k+xres+1] > 0.0)
        b |= 128;

    return neightable[b];
}

static gboolean
pixel_thinnable(const gdouble *data, gint xres, gint k)
{
    guint neighval;

    neighval = neighbour_segments(data, xres, k);
    /* One contiguous neighbour segment. */
    if (neighval % 8 != 1)
        return FALSE;

    /* Two to six neighbours. */
    neighval /= 8;
    if (neighval < 2 || neighval > 6)
        return FALSE;

    /* We could pull the first parts into the table in neighbour_segments(),
     * but I am currently too lazy. */
    if (data[k + 1] > 0.0 && data[k - 1] > 0.0 && data[k + xres] > 0.0
        && neighbour_segments(data, xres, k+xres) % 8 == 1)
        return FALSE;

    if (data[k + 1] > 0.0 && data[k - xres] > 0.0 && data[k + xres] > 0.0
        && neighbour_segments(data, xres, k+1) % 8 == 1)
        return FALSE;

    return TRUE;
}

static gint
thinstep(GwyDataField *data_field,
         GwyDataField *buffer)
{
    gint i, j, k, ch;
    gint xres = data_field->xres, yres = data_field->yres;
    gdouble *data = data_field->data;
    gdouble *bdata = buffer->data;

    gwy_data_field_clear(buffer);
    ch = 0;
    for (i = 2; i < yres-2; i++) {
        for (j = 2; j < xres-2; j++) {
            k = i*xres + j;
            if (data[k] > 0.0 && pixel_thinnable(data, xres, k)) {
                ch++;
                bdata[k] = 1.0;
            }
        }
    }
    for (i = 2; i < yres-1; i++) {
        for (j = 2; j < xres-1; j++) {
            k = i*xres + j;
            if (bdata[k] > 0.0)
                data[k] = 0.0;
        }
    }
    gwy_data_field_invalidate(data_field);

    return ch;
}

/* XXX: Could be an alternative public function to gwy_data_field_thin().  It
 * is more aggressive and loses some branches, but this can be a good thing.
 * But for that it would be nice to implement it using a pixel queue so that
 * we do not have to repeatedly scan the entire data field. */
static void
thin_data_field(GwyDataField *data_field)
{
    GwyDataField *buffer;
    gint xres, yres;

    xres = data_field->xres;
    yres = data_field->yres;
    gwy_data_field_area_clear(data_field, 0, 0, xres, 1);
    gwy_data_field_area_clear(data_field, 0, 0, 1, yres);
    gwy_data_field_area_clear(data_field, xres-1, 0, 1, yres);
    gwy_data_field_area_clear(data_field, 0, yres-1, xres, 1);

    buffer = gwy_data_field_new_alike(data_field, FALSE);
    while (thinstep(data_field, buffer))
        ;
    g_object_unref(buffer);

    gwy_data_field_invalidate(data_field);
}

/**
 * kuwahara_block:
 * @a: points to a 5x5 matrix (array of 25 doubles)
 *
 * Computes a new value of the center pixel according to the Kuwahara filter.
 *
 * Return: Filtered value.
 */
static gdouble
kuwahara_block(const gdouble *a)
{
   static const gint r1[] = { 0, 1, 2, 5, 6, 7, 10, 11, 12 };
   static const gint r2[] = { 2, 3, 4, 7, 8, 9, 12, 13, 14 };
   static const gint r3[] = { 12, 13, 14, 17, 18, 19, 22, 23, 24 };
   static const gint r4[] = { 10, 11, 12, 15, 16, 17, 20, 21, 22 };
   gdouble mean1 = 0.0, mean2 = 0.0, mean3 = 0.0, mean4 = 0.0;
   gdouble var1 = 0.0, var2 = 0.0, var3 = 0.0, var4 = 0.0;
   gint i;

   for (i = 0; i < 9; i++) {
       mean1 += a[r1[i]]/9.0;
       mean2 += a[r2[i]]/9.0;
       mean3 += a[r3[i]]/9.0;
       mean4 += a[r4[i]]/9.0;
       var1 += a[r1[i]]*a[r1[i]]/9.0;
       var2 += a[r2[i]]*a[r2[i]]/9.0;
       var3 += a[r3[i]]*a[r3[i]]/9.0;
       var4 += a[r4[i]]*a[r4[i]]/9.0;
   }

   var1 -= mean1 * mean1;
   var2 -= mean2 * mean2;
   var3 -= mean3 * mean3;
   var4 -= mean4 * mean4;

   if (var1 <= var2 && var1 <= var3 && var1 <= var4)
       return mean1;
   if (var2 <= var3 && var2 <= var4 && var2 <= var1)
       return mean2;
   if (var3 <= var4 && var3 <= var1 && var3 <= var2)
       return mean3;
   if (var4 <= var1 && var4 <= var2 && var4 <= var3)
       return mean4;
   return 0.0;
}

#define gwy_data_field_get_val_closest(d, col, row) \
  ((d)->data[CLAMP((row), 0, (d)->yres-1) * (d)->xres \
  + CLAMP((col), 0, (d)->xres-1)])

/**
 * gwy_data_field_area_filter_kuwahara:
 * @data_field: A data filed to apply Kuwahara filter to.
 * @col: Upper-left column coordinate.
 * @row: Upper-left row coordinate.
 * @width: Area width (number of columns).
 * @height: Area height (number of rows).
 *
 * Filters a rectangular part of a data field with a Kuwahara
 * (edge-preserving smoothing) filter.
 **/
void
gwy_data_field_area_filter_kuwahara(GwyDataField *data_field,
                                    gint col, gint row,
                                    gint width, gint height)
{
    gint i, j, x, y, ctr;
    gdouble *buffer, *kernel;

    g_return_if_fail(GWY_IS_DATA_FIELD(data_field));
    g_return_if_fail(col >= 0 && row >= 0
                     && width > 0 && height > 0
                     && col + width <= data_field->xres
                     && row + height <= data_field->yres);

    buffer = g_new(gdouble, width*height);
    kernel = g_new(gdouble, 25);

    /* TO DO: optimize for speed */
    for (i = 0; i < height; i++) {
        for (j = 0; j < width; j++) {

            ctr = 0;
            for (y = -2; y <= 2; y++) {
                for (x = -2; x <= 2; x++)
                    kernel[ctr++] = gwy_data_field_get_val_closest(data_field,
                                                                   col + j + x,
                                                                   row + i + y);
            }
            buffer[i*width + j] = kuwahara_block(kernel);
        }
    }

    for (i = 0; i < height; i++) {
        for (j = 0; j < width; j++)
            data_field->data[col + j + data_field->xres * (row + i)]
                                                  = buffer[i*width + j];
    }

    g_free(kernel);
    g_free(buffer);
}

/**
 * gwy_data_field_filter_kuwahara:
 * @data_field: A data field to apply Kuwahara filter to.
 *
 * Filters a data field with Kuwahara filter.
 **/
void
gwy_data_field_filter_kuwahara(GwyDataField *data_field)
{
    g_return_if_fail(GWY_IS_DATA_FIELD(data_field));
    gwy_data_field_area_filter_kuwahara(data_field, 0, 0,
                                        data_field->xres, data_field->yres);
}

/**
 * gwy_data_field_shade:
 * @data_field: A data field.
 * @target_field: A data field to put the shade to.  It will be resized to
 *                match @data_field.
 * @theta: Shading angle (in radians, from north pole).
 * @phi: Shade orientation in xy plane (in radians, counterclockwise).
 *
 * Shades a data field.
 **/
void
gwy_data_field_shade(GwyDataField *data_field,
                     GwyDataField *target_field,
                     gdouble theta, gdouble phi)
{
    gint i, j;
    gdouble max, maxval, v;
    gdouble *data;

    gwy_data_field_resample(target_field, data_field->xres, data_field->yres,
                            GWY_INTERPOLATION_NONE);

    max = -G_MAXDOUBLE;
    data = target_field->data;
    for (i = 0; i < data_field->yres; i++) {

        for (j = 0; j < data_field->xres; j++) {
            v = -gwy_data_field_get_angder(data_field, j, i, phi);
            data[j + data_field->xres*i] = v;

            if (max < v)
                max = v;
        }
    }

    maxval = theta/max;
    for (i = 0; i < data_field->xres*data_field->yres; i++)
        data[i] = max - fabs(maxval - data[i]);

    gwy_data_field_invalidate(target_field);
}

/**
 * gwy_data_field_filter_harris:
 * @x_gradient: Data field with pre-calculated horizontal derivative.
 * @y_gradient: Data field with pre-calculated vertical derivative.
 * @result: Data field for the result.
 * @neighbourhood: Neighbourhood size.
 * @alpha: Sensitivity paramter (the squared trace is multiplied by it).
 *
 * Applies Harris corner detection filter to a pair of gradient data fields.
 *
 * All passed data field must have the same size.
 **/
void
gwy_data_field_filter_harris(GwyDataField *x_gradient,
                             GwyDataField *y_gradient,
                             GwyDataField *result,
                             gint neighbourhood,
                             gdouble alpha)
{
    gdouble pxx, pxy, pyy, det, trace, mult;
    gint height, width, i, j, k;
    GwyDataField *xx, *xy, *yy;
    gdouble *xxdata, *xydata, *yydata, *xg, *yg, *r;
    gdouble sigma, vx, vy;

    g_return_if_fail(GWY_IS_DATA_FIELD(x_gradient));
    g_return_if_fail(GWY_IS_DATA_FIELD(y_gradient));
    g_return_if_fail(GWY_IS_DATA_FIELD(result));
    height = result->yres;
    width = result->xres;
    g_return_if_fail(x_gradient->xres == width);
    g_return_if_fail(x_gradient->yres == height);
    g_return_if_fail(y_gradient->xres == width);
    g_return_if_fail(y_gradient->yres == height);
    g_return_if_fail(neighbourhood > 0);

    gwy_data_field_clear(result);

    mult = fabs(gwy_data_field_get_max(x_gradient)
                - gwy_data_field_get_min(x_gradient));
    mult += fabs(gwy_data_field_get_max(y_gradient)
                 - gwy_data_field_get_min(y_gradient));
    mult = 1.0/(mult*mult);

    xx = gwy_data_field_new_alike(result, TRUE);
    xy = gwy_data_field_new_alike(result, TRUE);
    yy = gwy_data_field_new_alike(result, TRUE);
    xxdata = xx->data;
    xydata = xy->data;
    yydata = yy->data;
    xg = x_gradient->data;
    yg = y_gradient->data;

    for (i = neighbourhood; i < height - neighbourhood; i++) {
         for (j = neighbourhood; j < width - neighbourhood; j++) {
             k = i*width + j;
             vx = xg[k];
             vy = yg[k];
             xxdata[k] = vx*vx*mult;
             xydata[k] = vx*vy*mult;
             yydata[k] = vy*vy*mult;
         }
    }

    sigma = neighbourhood/5.0;
    gwy_data_field_filter_gaussian(xx, sigma);
    gwy_data_field_filter_gaussian(xy, sigma);
    gwy_data_field_filter_gaussian(yy, sigma);

    r = result->data;
    for (i = neighbourhood; i < height - neighbourhood; i++) {
         for (j = neighbourhood; j < width - neighbourhood; j++) {
             k = i*width + j;
             pxx = xxdata[k];
             pxy = xydata[k];
             pyy = yydata[k];
             det = pxx*pyy - pxy*pxy;
             trace = pxx + pyy;
             r[k] = det - alpha*trace*trace;
          }
    }

    gwy_data_field_invalidate(result);
    g_object_unref(xx);
    g_object_unref(xy);
    g_object_unref(yy);
}

/************************** Documentation ****************************/

/**
 * SECTION:filters
 * @title: filters
 * @short_description: Convolution and other 2D data filters
 *
 * Filters are point-wise operations, such as thresholding, or generally local
 * operations producing a value based on the data in the vicinity of each
 * point: gradients, step detectors and convolutions.  Some simple common
 * point-wise operations, e.g. value inversion, are also found in base
 * #GwyDataField methods.
 **/

/* vim: set cin et ts=4 sw=4 cino=>1s,e0,n0,f0,{0,}0,^0,\:1s,=0,g1s,h0,t0,+1s,c3,(0,u0 : */
