/*
 *  $Id$
 *  Copyright (C) 2003-2017 David Necas (Yeti), Petr Klapetek.
 *  E-mail: yeti@gwyddion.net, klapetek@gwyddion.net.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA 02110-1301, USA.
 */

#include <string.h>
#include <libgwyddion/gwymacros.h>
#include <libgwyddion/gwymath.h>
#include <libprocess/datafield.h>
#include <libprocess/level.h>
#include <libprocess/stats.h>
#include <libprocess/linestats.h>
#include <libprocess/grains.h>
#include "gwyprocessinternal.h"

typedef gdouble (*LineStatFunc)(GwyDataLine *dline);

/**
 * gwy_data_field_get_max:
 * @data_field: A data field.
 *
 * Finds the maximum value of a data field.
 *
 * This quantity is cached.
 *
 * Returns: The maximum value.
 **/
gdouble
gwy_data_field_get_max(GwyDataField *data_field)
{
    g_return_val_if_fail(GWY_IS_DATA_FIELD(data_field), -G_MAXDOUBLE);
    gwy_debug("%s", CTEST(data_field, MAX) ? "cache" : "lame");

    if (!CTEST(data_field, MAX)) {
        const gdouble *p;
        gdouble max;
        gint i;

        max = data_field->data[0];
        p = data_field->data;
        for (i = data_field->xres * data_field->yres; i; i--, p++) {
            if (G_UNLIKELY(max < *p))
                max = *p;
        }
        CVAL(data_field, MAX) = max;
        data_field->cached |= CBIT(MAX);
    }

    return CVAL(data_field, MAX);
}


/**
 * gwy_data_field_area_get_max:
 * @data_field: A data field.
 * @mask: Mask specifying which values to take into account/exclude, or %NULL.
 * @col: Upper-left column coordinate.
 * @row: Upper-left row coordinate.
 * @width: Area width (number of columns).
 * @height: Area height (number of rows).
 *
 * Finds the maximum value in a rectangular part of a data field.
 *
 * Returns: The maximum value.  When the number of samples to calculate
 *          maximum of is zero, -%G_MAXDOUBLE is returned.
 **/
gdouble
gwy_data_field_area_get_max(GwyDataField *dfield,
                            GwyDataField *mask,
                            gint col, gint row,
                            gint width, gint height)
{
    gint i, j;
    gdouble max = -G_MAXDOUBLE;
    const gdouble *datapos, *mpos;

    g_return_val_if_fail(GWY_IS_DATA_FIELD(dfield), max);
    g_return_val_if_fail(!mask || (GWY_IS_DATA_FIELD(mask)
                                   && mask->xres == dfield->xres
                                   && mask->yres == dfield->yres),
                         max);
    g_return_val_if_fail(col >= 0 && row >= 0
                         && width >= 0 && height >= 0
                         && col + width <= dfield->xres
                         && row + height <= dfield->yres,
                         max);
    if (!width || !height)
        return max;

    if (mask) {
        datapos = dfield->data + row*dfield->xres + col;
        mpos = mask->data + row*mask->xres + col;
        for (i = 0; i < height; i++) {
            const gdouble *drow = datapos + i*dfield->xres;
            const gdouble *mrow = mpos + i*mask->xres;

            for (j = 0; j < width; j++) {
                if (G_UNLIKELY(max < *drow) && *mrow > 0.0)
                    max = *drow;
                drow++;
                mrow++;
            }
        }

        return max;
    }

    if (col == 0 && width == dfield->xres
        && row == 0 && height == dfield->yres)
        return gwy_data_field_get_max(dfield);

    datapos = dfield->data + row*dfield->xres + col;
    for (i = 0; i < height; i++) {
        const gdouble *drow = datapos + i*dfield->xres;

        for (j = 0; j < width; j++) {
            if (G_UNLIKELY(max < *drow))
                max = *drow;
            drow++;
        }
    }

    return max;
}

/**
 * gwy_data_field_get_min:
 * @data_field: A data field.
 *
 * Finds the minimum value of a data field.
 *
 * This quantity is cached.
 *
 * Returns: The minimum value.
 **/
gdouble
gwy_data_field_get_min(GwyDataField *data_field)
{
    g_return_val_if_fail(GWY_IS_DATA_FIELD(data_field), -G_MAXDOUBLE);
    gwy_debug("%s", CTEST(data_field, MIN) ? "cache" : "lame");

    if (!CTEST(data_field, MIN)) {
        gdouble min;
        const gdouble *p;
        gint i;

        min = data_field->data[0];
        p = data_field->data;
        for (i = data_field->xres * data_field->yres; i; i--, p++) {
            if (G_UNLIKELY(min > *p))
                min = *p;
        }
        CVAL(data_field, MIN) = min;
        data_field->cached |= CBIT(MIN);
    }

    return CVAL(data_field, MIN);
}


/**
 * gwy_data_field_area_get_min:
 * @data_field: A data field.
 * @mask: Mask specifying which values to take into account/exclude, or %NULL.
 * @col: Upper-left column coordinate.
 * @row: Upper-left row coordinate.
 * @width: Area width (number of columns).
 * @height: Area height (number of rows).
 *
 * Finds the minimum value in a rectangular part of a data field.
 *
 * Returns: The minimum value.  When the number of samples to calculate
 *          minimum of is zero, -%G_MAXDOUBLE is returned.
 **/
gdouble
gwy_data_field_area_get_min(GwyDataField *dfield,
                            GwyDataField *mask,
                            gint col, gint row,
                            gint width, gint height)
{
    gint i, j;
    gdouble min = G_MAXDOUBLE;
    const gdouble *datapos, *mpos;

    g_return_val_if_fail(GWY_IS_DATA_FIELD(dfield), min);
    g_return_val_if_fail(!mask || (GWY_IS_DATA_FIELD(mask)
                                   && mask->xres == dfield->xres
                                   && mask->yres == dfield->yres),
                         min);
    g_return_val_if_fail(col >= 0 && row >= 0
                         && width >= 0 && height >= 0
                         && col + width <= dfield->xres
                         && row + height <= dfield->yres,
                         min);
    if (!width || !height)
        return min;

    if (mask) {
        datapos = dfield->data + row*dfield->xres + col;
        mpos = mask->data + row*mask->xres + col;
        for (i = 0; i < height; i++) {
            const gdouble *drow = datapos + i*dfield->xres;
            const gdouble *mrow = mpos + i*mask->xres;

            for (j = 0; j < width; j++) {
                if (G_UNLIKELY(min > *drow) && *mrow > 0.0)
                    min = *drow;
                drow++;
                mrow++;
            }
        }

        return min;
    }

    if (col == 0 && width == dfield->xres
        && row == 0 && height == dfield->yres)
        return gwy_data_field_get_min(dfield);

    datapos = dfield->data + row*dfield->xres + col;
    for (i = 0; i < height; i++) {
        const gdouble *drow = datapos + i*dfield->xres;

        for (j = 0; j < width; j++) {
            if (G_UNLIKELY(min > *drow))
                min = *drow;
            drow++;
        }
    }

    return min;
}

/**
 * gwy_data_field_get_min_max:
 * @data_field: A data field.
 * @min: Location to store minimum to.
 * @max: Location to store maximum to.
 *
 * Finds minimum and maximum values of a data field.
 **/
void
gwy_data_field_get_min_max(GwyDataField *data_field,
                           gdouble *min,
                           gdouble *max)
{
    gboolean need_min = FALSE, need_max = FALSE;
    gdouble min1, max1;
    const gdouble *p;
    gint i;

    g_return_if_fail(GWY_IS_DATA_FIELD(data_field));

    if (min) {
        if (CTEST(data_field, MIN))
            *min = CVAL(data_field, MIN);
        else
            need_min = TRUE;
    }
    if (max) {
        if (CTEST(data_field, MAX))
            *max = CVAL(data_field, MAX);
        else
            need_max = TRUE;
    }

    if (!need_min && !need_max)
        return;
    else if (!need_min) {
        *max = gwy_data_field_get_max(data_field);
        return;
    }
    else if (!need_max) {
        *min = gwy_data_field_get_min(data_field);
        return;
    }

    min1 = data_field->data[0];
    max1 = data_field->data[0];
    p = data_field->data;
    for (i = data_field->xres * data_field->yres; i; i--, p++) {
        if (G_UNLIKELY(min1 > *p))
            min1 = *p;
        if (G_UNLIKELY(max1 < *p))
            max1 = *p;
    }

    *min = min1;
    *max = max1;
    CVAL(data_field, MIN) = min1;
    CVAL(data_field, MAX) = max1;
    data_field->cached |= CBIT(MIN) | CBIT(MAX);
}

/**
 * gwy_data_field_area_get_min_max:
 * @data_field: A data field.
 * @mask: Mask specifying which values to take into account/exclude, or %NULL.
 * @col: Upper-left column coordinate.
 * @row: Upper-left row coordinate.
 * @width: Area width (number of columns).
 * @height: Area height (number of rows).
 * @min: Location to store minimum to.
 * @max: Location to store maximum to.
 *
 * Finds minimum and maximum values in a rectangular part of a data field.
 *
 * This function is equivalent to calling
 * @gwy_data_field_area_get_min_max_mask()
 * with masking mode %GWY_MASK_INCLUDE.
 **/
void
gwy_data_field_area_get_min_max(GwyDataField *data_field,
                                GwyDataField *mask,
                                gint col, gint row,
                                gint width, gint height,
                                gdouble *min,
                                gdouble *max)
{
    gwy_data_field_area_get_min_max_mask(data_field, mask, GWY_MASK_INCLUDE,
                                         col, row, width, height,
                                         min, max);
}

/**
 * gwy_data_field_area_get_min_max_mask:
 * @data_field: A data field.
 * @mask: Mask specifying which values to take into account/exclude, or %NULL.
 * @mode: Masking mode to use.  See the introduction for description of
 *        masking modes.
 * @col: Upper-left column coordinate.
 * @row: Upper-left row coordinate.
 * @width: Area width (number of columns).
 * @height: Area height (number of rows).
 * @min: Location to store minimum to.
 * @max: Location to store maximum to.
 *
 * Finds minimum and maximum values in a rectangular part of a data field.
 *
 * Since: 2.18
 **/
void
gwy_data_field_area_get_min_max_mask(GwyDataField *data_field,
                                     GwyDataField *mask,
                                     GwyMaskingType mode,
                                     gint col, gint row,
                                     gint width, gint height,
                                     gdouble *min,
                                     gdouble *max)
{
    gdouble min1 = G_MAXDOUBLE, max1 = -G_MAXDOUBLE;
    const gdouble *datapos, *mpos;
    gint i, j;

    g_return_if_fail(GWY_IS_DATA_FIELD(data_field));
    g_return_if_fail(!mask || (GWY_IS_DATA_FIELD(mask)
                               && mask->xres == data_field->xres
                               && mask->yres == data_field->yres));
    g_return_if_fail(col >= 0 && row >= 0
                     && width >= 0 && height >= 0
                     && col + width <= data_field->xres
                     && row + height <= data_field->yres);
    if (!width || !height) {
        if (min)
            *min = min1;
        if (max)
            *max = max1;
        return;
    }

    if (!min && !max)
        return;

    if (mask && mode != GWY_MASK_IGNORE) {
        datapos = data_field->data + row*data_field->xres + col;
        mpos = mask->data + row*mask->xres + col;
        for (i = 0; i < height; i++) {
            const gdouble *drow = datapos + i*data_field->xres;
            const gdouble *mrow = mpos + i*mask->xres;

            if (mode == GWY_MASK_INCLUDE) {
                for (j = 0; j < width; j++) {
                    if (G_UNLIKELY(min1 > *drow) && *mrow > 0.0)
                        min1 = *drow;
                    if (G_UNLIKELY(max1 < *drow) && *mrow > 0.0)
                        max1 = *drow;
                    drow++;
                    mrow++;
                }
            }
            else {
                for (j = 0; j < width; j++) {
                    if (G_UNLIKELY(min1 > *drow) && *mrow < 1.0)
                        min1 = *drow;
                    if (G_UNLIKELY(max1 < *drow) && *mrow < 1.0)
                        max1 = *drow;
                    drow++;
                    mrow++;
                }
            }
        }

        if (min)
            *min = min1;
        if (max)
            *max = max1;

        return;
    }

    if (col == 0 && width == data_field->xres
        && row == 0 && height == data_field->yres) {
        gwy_data_field_get_min_max(data_field, min, max);
        return;
    }

    /* Static code analysis: sod off.  We ensure just above that at most one
     * pointer is NULL. */
    if (!min) {
        *max = gwy_data_field_area_get_max(data_field, NULL,
                                           col, row, width, height);
        return;
    }
    if (!max) {
        *min = gwy_data_field_area_get_min(data_field, NULL,
                                           col, row, width, height);
        return;
    }

    datapos = data_field->data + row*data_field->xres + col;
    for (i = 0; i < height; i++) {
        const gdouble *drow = datapos + i*data_field->xres;

        for (j = 0; j < width; j++) {
            if (G_UNLIKELY(min1 > *drow))
                min1 = *drow;
            if (G_UNLIKELY(max1 < *drow))
                max1 = *drow;
            drow++;
        }
    }

    *min = min1;
    *max = max1;
}

/**
 * gwy_data_field_get_sum:
 * @data_field: A data field.
 *
 * Sums all values in a data field.
 *
 * This quantity is cached.
 *
 * Returns: The sum of all values.
 **/
gdouble
gwy_data_field_get_sum(GwyDataField *data_field)
{
    gint i;
    gdouble sum = 0;
    const gdouble *p;

    g_return_val_if_fail(GWY_IS_DATA_FIELD(data_field), sum);

    gwy_debug("%s", CTEST(data_field, SUM) ? "cache" : "lame");
    if (CTEST(data_field, SUM))
        return CVAL(data_field, SUM);

    p = data_field->data;
    for (i = data_field->xres * data_field->yres; i; i--, p++)
        sum += *p;

    CVAL(data_field, SUM) = sum;
    data_field->cached |= CBIT(SUM);

    return sum;
}
/**
 * gwy_data_field_area_get_sum:
 * @data_field: A data field.
 * @mask: Mask specifying which values to take into account/exclude, or %NULL.
 * @col: Upper-left column coordinate.
 * @row: Upper-left row coordinate.
 * @width: Area width (number of columns).
 * @height: Area height (number of rows).
 *
 * Sums values of a rectangular part of a data field.
 *
 * This function is equivalent to calling @gwy_data_field_area_get_sum_mask()
 * with masking mode %GWY_MASK_INCLUDE.
 *
 * Returns: The sum of all values inside area.
 **/
gdouble
gwy_data_field_area_get_sum(GwyDataField *dfield,
                            GwyDataField *mask,
                            gint col, gint row,
                            gint width, gint height)
{
    return gwy_data_field_area_get_sum_mask(dfield, mask, GWY_MASK_INCLUDE,
                                            col, row, width, height);
}

/**
 * gwy_data_field_area_get_sum_mask:
 * @data_field: A data field.
 * @mask: Mask specifying which values to take into account/exclude, or %NULL.
 * @mode: Masking mode to use.  See the introduction for description of
 *        masking modes.
 * @col: Upper-left column coordinate.
 * @row: Upper-left row coordinate.
 * @width: Area width (number of columns).
 * @height: Area height (number of rows).
 *
 * Sums values of a rectangular part of a data field.
 *
 * Returns: The sum of all values inside area.
 *
 * Since: 2.18
 **/
gdouble
gwy_data_field_area_get_sum_mask(GwyDataField *dfield,
                                 GwyDataField *mask,
                                 GwyMaskingType mode,
                                 gint col, gint row,
                                 gint width, gint height)
{
    gint i, j;
    gdouble sum = 0;
    const gdouble *datapos, *mpos;

    g_return_val_if_fail(GWY_IS_DATA_FIELD(dfield), sum);
    g_return_val_if_fail(!mask || (GWY_IS_DATA_FIELD(mask)
                                   && mask->xres == dfield->xres
                                   && mask->yres == dfield->yres),
                         sum);
    g_return_val_if_fail(col >= 0 && row >= 0
                         && width >= 0 && height >= 0
                         && col + width <= dfield->xres
                         && row + height <= dfield->yres,
                         sum);

    if (mask && mode != GWY_MASK_IGNORE) {
        datapos = dfield->data + row*dfield->xres + col;
        mpos = mask->data + row*mask->xres + col;
        for (i = 0; i < height; i++) {
            const gdouble *drow = datapos + i*dfield->xres;
            const gdouble *mrow = mpos + i*mask->xres;

            if (mode == GWY_MASK_INCLUDE) {
                for (j = 0; j < width; j++) {
                    if (*mrow > 0.0)
                        sum += *drow;
                    drow++;
                    mrow++;
                }
            }
            else {
                for (j = 0; j < width; j++) {
                    if (*mrow < 1.0)
                        sum += *drow;
                    drow++;
                    mrow++;
                }
            }
        }

        return sum;
    }

    if (col == 0 && width == dfield->xres
        && row == 0 && height == dfield->yres)
        return gwy_data_field_get_sum(dfield);

    datapos = dfield->data + row*dfield->xres + col;
    for (i = 0; i < height; i++) {
        const gdouble *drow = datapos + i*dfield->xres;

        for (j = 0; j < width; j++)
            sum += *(drow++);
    }

    return sum;
}

/**
 * gwy_data_field_get_avg:
 * @data_field: A data field
 *
 * Computes average value of a data field.
 *
 * This quantity is cached.
 *
 * Returns: The average value.
 **/
gdouble
gwy_data_field_get_avg(GwyDataField *data_field)
{
    return gwy_data_field_get_sum(data_field)/((data_field->xres
                                                * data_field->yres));
}
/**
 * gwy_data_field_area_get_avg:
 * @data_field: A data field
 * @mask: Mask specifying which values to take into account/exclude, or %NULL.
 * @col: Upper-left column coordinate.
 * @row: Upper-left row coordinate.
 * @width: Area width (number of columns).
 * @height: Area height (number of rows).
 *
 * Computes average value of a rectangular part of a data field.
 *
 * This function is equivalent to calling @gwy_data_field_area_get_avg_mask()
 * with masking mode %GWY_MASK_INCLUDE.
 *
 * Returns: The average value.
 **/
gdouble
gwy_data_field_area_get_avg(GwyDataField *dfield,
                            GwyDataField *mask,
                            gint col, gint row,
                            gint width, gint height)
{
    return gwy_data_field_area_get_avg_mask(dfield, mask, GWY_MASK_INCLUDE,
                                            col, row, width, height);
}

/**
 * gwy_data_field_area_get_avg_mask:
 * @data_field: A data field
 * @mask: Mask specifying which values to take into account/exclude, or %NULL.
 * @mode: Masking mode to use.  See the introduction for description of
 *        masking modes.
 * @col: Upper-left column coordinate.
 * @row: Upper-left row coordinate.
 * @width: Area width (number of columns).
 * @height: Area height (number of rows).
 *
 * Computes average value of a rectangular part of a data field.
 *
 * Returns: The average value.
 *
 * Since: 2.18
 **/
gdouble
gwy_data_field_area_get_avg_mask(GwyDataField *dfield,
                                 GwyDataField *mask,
                                 GwyMaskingType mode,
                                 gint col, gint row,
                                 gint width, gint height)
{
    const gdouble *datapos, *mpos;
    gdouble sum = 0;
    gint i, j;
    guint nn;

    if (!mask || mode == GWY_MASK_IGNORE) {
        return gwy_data_field_area_get_sum_mask(dfield, NULL, GWY_MASK_IGNORE,
                                                col, row,
                                                width, height)/(width*height);
    }

    g_return_val_if_fail(GWY_IS_DATA_FIELD(dfield), sum);
    g_return_val_if_fail(GWY_IS_DATA_FIELD(mask)
                         && mask->xres == dfield->xres
                         && mask->yres == dfield->yres,
                         sum);
    g_return_val_if_fail(col >= 0 && row >= 0
                         && width >= 0 && height >= 0
                         && col + width <= dfield->xres
                         && row + height <= dfield->yres,
                         sum);

    datapos = dfield->data + row*dfield->xres + col;
    mpos = mask->data + row*mask->xres + col;
    nn = 0;
    for (i = 0; i < height; i++) {
        const gdouble *drow = datapos + i*dfield->xres;
        const gdouble *mrow = mpos + i*mask->xres;

        if (mode == GWY_MASK_INCLUDE) {
            for (j = 0; j < width; j++) {
                if (*mrow > 0.0) {
                    sum += *drow;
                    nn++;
                }
                drow++;
                mrow++;
            }
        }
        else {
            for (j = 0; j < width; j++) {
                if (*mrow < 1.0) {
                    sum += *drow;
                    nn++;
                }
                drow++;
                mrow++;
            }
        }
    }

    return sum/nn;
}

/**
 * gwy_data_field_get_rms:
 * @data_field: A data field.
 *
 * Computes root mean square value of a data field.
 *
 * This quantity is cached.
 *
 * Returns: The root mean square value.
 **/
gdouble
gwy_data_field_get_rms(GwyDataField *data_field)
{
    gint i, n;
    gdouble rms = 0.0, sum, sum2;
    const gdouble *p;

    g_return_val_if_fail(GWY_IS_DATA_FIELD(data_field), rms);

    gwy_debug("%s", CTEST(data_field, RMS) ? "cache" : "lame");
    if (CTEST(data_field, RMS))
        return CVAL(data_field, RMS);

    sum = gwy_data_field_get_sum(data_field);
    sum2 = 0.0;
    p = data_field->data;
    for (i = data_field->xres * data_field->yres; i; i--, p++)
        sum2 += (*p)*(*p);

    n = data_field->xres * data_field->yres;
    rms = sqrt(fabs(sum2 - sum*sum/n)/n);

    CVAL(data_field, RMS) = rms;
    data_field->cached |= CBIT(RMS);

    return rms;
}

/**
 * gwy_data_field_area_get_rms:
 * @data_field: A data field.
 * @mask: Mask specifying which values to take into account/exclude, or %NULL.
 * @col: Upper-left column coordinate.
 * @row: Upper-left row coordinate.
 * @width: Area width (number of columns).
 * @height: Area height (number of rows).
 *
 * Computes root mean square value of a rectangular part of a data field.
 *
 * Returns: The root mean square value.
 *
 * This function is equivalent to calling @gwy_data_field_area_get_rms_mask()
 * with masking mode %GWY_MASK_INCLUDE.
 **/

gdouble
gwy_data_field_area_get_rms(GwyDataField *dfield,
                            GwyDataField *mask,
                            gint col, gint row,
                            gint width, gint height)
{
    return gwy_data_field_area_get_rms_mask(dfield, mask, GWY_MASK_INCLUDE,
                                            col, row, width, height);
}

/**
 * gwy_data_field_area_get_rms_mask:
 * @data_field: A data field.
 * @mask: Mask specifying which values to take into account/exclude, or %NULL.
 * @mode: Masking mode to use.  See the introduction for description of
 *        masking modes.
 * @col: Upper-left column coordinate.
 * @row: Upper-left row coordinate.
 * @width: Area width (number of columns).
 * @height: Area height (number of rows).
 *
 * Computes root mean square value of deviations of a rectangular part of a
 * data field.
 *
 * Returns: The root mean square value of deviations from the mean value.
 *
 * Since: 2.18
 **/
gdouble
gwy_data_field_area_get_rms_mask(GwyDataField *dfield,
                                 GwyDataField *mask,
                                 GwyMaskingType mode,
                                 gint col, gint row,
                                 gint width, gint height)
{
    gint i, j;
    gdouble rms = 0.0, sum2 = 0.0;
    gdouble sum;
    const gdouble *datapos, *mpos;
    guint nn;

    g_return_val_if_fail(GWY_IS_DATA_FIELD(dfield), rms);
    g_return_val_if_fail(!mask || (GWY_IS_DATA_FIELD(mask)
                                   && mask->xres == dfield->xres
                                   && mask->yres == dfield->yres),
                         rms);
    g_return_val_if_fail(col >= 0 && row >= 0
                         && width >= 0 && height >= 0
                         && col + width <= dfield->xres
                         && row + height <= dfield->yres,
                         rms);
    if (!width || !height)
        return rms;

    if (mask && mode != GWY_MASK_INCLUDE) {
        sum = gwy_data_field_area_get_sum_mask(dfield, mask, mode,
                                               col, row, width, height);
        datapos = dfield->data + row*dfield->xres + col;
        mpos = mask->data + row*mask->xres + col;
        nn = 0;
        for (i = 0; i < height; i++) {
            const gdouble *drow = datapos + i*dfield->xres;
            const gdouble *mrow = mpos + i*mask->xres;

            if (mode == GWY_MASK_INCLUDE) {
                for (j = 0; j < width; j++) {
                    if (*mrow > 0.0) {
                        sum2 += (*drow) * (*drow);
                        nn++;
                    }
                    drow++;
                    mrow++;
                }
            }
            else {
                for (j = 0; j < width; j++) {
                    if (*mrow < 1.0) {
                        sum2 += (*drow) * (*drow);
                        nn++;
                    }
                    drow++;
                    mrow++;
                }
            }
        }
        rms = sqrt(fabs(sum2 - sum*sum/nn)/nn);

        return rms;
    }

    if (col == 0 && width == dfield->xres
        && row == 0 && height == dfield->yres)
        return gwy_data_field_get_rms(dfield);

    sum = gwy_data_field_area_get_sum(dfield, NULL, col, row, width, height);
    datapos = dfield->data + row*dfield->xres + col;
    for (i = 0; i < height; i++) {
        const gdouble *drow = datapos + i*dfield->xres;

        for (j = 0; j < width; j++) {
            sum2 += (*drow) * (*drow);
            drow++;
        }
    }

    nn = width*height;
    rms = sqrt(fabs(sum2 - sum*sum/nn)/nn);

    return rms;
}

/**
 * gwy_data_field_area_get_grainwise_rms:
 * @data_field: A data field.
 * @mask: Mask specifying which values to take into account/exclude, or %NULL.
 * @mode: Masking mode to use.  See the introduction for description of
 *        masking modes.
 * @col: Upper-left column coordinate.
 * @row: Upper-left row coordinate.
 * @width: Area width (number of columns).
 * @height: Area height (number of rows).
 *
 * Computes grain-wise root mean square value of deviations of a rectangular
 * part of a data field.
 *
 * Grain-wise means that the mean value is determined for each grain (i.e.
 * cotinguous part of the mask or inverted mask) separately and the deviations
 * are calculated from these mean values.
 *
 * Returns: The root mean square value of deviations from the mean value.
 *
 * Since: 2.29
 **/
gdouble
gwy_data_field_area_get_grainwise_rms(GwyDataField *dfield,
                                      GwyDataField *mask,
                                      GwyMaskingType mode,
                                      gint col,
                                      gint row,
                                      gint width,
                                      gint height)
{
    GwyDataField *grainmask;
    gint *grains, *size, *g;
    gint i, j, n;
    gint xres, yres, ngrains;
    gdouble *m;
    const gdouble *datapos;
    gdouble rms = 0.0;

    g_return_val_if_fail(GWY_IS_DATA_FIELD(dfield), rms);
    xres = dfield->xres;
    yres = dfield->yres;
    g_return_val_if_fail(!mask || (GWY_IS_DATA_FIELD(mask)
                                   && mask->xres == xres
                                   && mask->yres == yres),
                         rms);
    g_return_val_if_fail(col >= 0 && row >= 0
                         && width >= 0 && height >= 0
                         && col + width <= xres
                         && row + height <= yres,
                         rms);
    if (!width || !height)
        return rms;

    if (!mask || mode == GWY_MASK_IGNORE)
        return gwy_data_field_area_get_rms_mask(dfield, NULL,
                                                GWY_MASK_IGNORE,
                                                col, row, width, height);

    if (mode == GWY_MASK_INCLUDE) {
        if (col == 0 && row == 0 && width == xres && height == yres)
            grainmask = (GwyDataField*)g_object_ref(mask);
        else
            grainmask = gwy_data_field_area_extract(mask,
                                                    col, row, width, height);
    }
    else {
        grainmask = gwy_data_field_area_extract(mask, col, row, width, height);
        gwy_data_field_grains_invert(grainmask);
    }

    grains = g_new0(gint, width*height);
    ngrains = gwy_data_field_number_grains(grainmask, grains);
    if (!ngrains) {
        g_free(grains);
        g_object_unref(grainmask);
        return rms;
    }

    m = g_new0(gdouble, ngrains+1);
    size = g_new0(gint, ngrains+1);
    datapos = dfield->data + row*xres + col;
    g = grains;
    for (i = 0; i < height; i++) {
        for (j = 0; j < width; j++, g++) {
            m[*g] += datapos[i*xres + j];
            size[*g]++;
        }
    }

    n = 0;
    for (i = 1; i <= ngrains; i++) {
        m[i] /= size[i];
        n += size[i];
    }

    g = grains;
    rms = 0.0;
    for (i = 0; i < height; i++) {
        for (j = 0; j < width; j++, g++) {
            if (*g) {
                gdouble d = datapos[i*xres + j] - m[*g];
                rms += d*d;
            }
        }
    }
    rms = sqrt(rms/n);

    g_free(size);
    g_free(m);
    g_free(grains);
    g_object_unref(grainmask);

    return rms;
}

/**
 * gwy_data_field_get_autorange:
 * @data_field: A data field.
 * @from: Location to store range start.
 * @to: Location to store range end.
 *
 * Computes value range with outliers cut-off.
 *
 * The purpose of this function is to find a range is suitable for false color
 * mapping.  The precise method how it is calculated is unspecified and may be
 * subject to changes.
 *
 * However, it is guaranteed minimum <= @from <= @to <= maximum.
 *
 * This quantity is cached.
 **/
void
gwy_data_field_get_autorange(GwyDataField *data_field,
                             gdouble *from,
                             gdouble *to)
{
    enum { AR_NDH = 512 };
    guint dh[AR_NDH];
    gdouble min, max, rmin, rmax, q;
    gdouble *p;
    guint i, n, j;

    g_return_if_fail(GWY_IS_DATA_FIELD(data_field));

    gwy_debug("%s", CTEST(data_field, ARF) ? "cache" : "lame");
    if ((!from || CTEST(data_field, ARF))
        && (!to || CTEST(data_field, ART))) {
        if (from)
            *from = CVAL(data_field, ARF);
        if (to)
            *to = CVAL(data_field, ART);
        return;
    }

    gwy_data_field_get_min_max(data_field, &min, &max);
    if (min == max) {
        rmin = min;
        rmax = max;
    }
    else {
        max += 1e-6*(max - min);
        q = AR_NDH/(max - min);

        n = data_field->xres*data_field->yres;
        gwy_clear(dh, AR_NDH);
        for (i = n, p = data_field->data; i; i--, p++) {
            j = (*p - min)*q;
            dh[MIN(j, AR_NDH-1)]++;
        }

        j = 0;
        for (i = j = 0; dh[i] < 5e-2*n/AR_NDH && j < 2e-2*n; i++)
            j += dh[i];
        rmin = min + i/q;

        j = 0;
        for (i = AR_NDH-1, j = 0; dh[i] < 5e-2*n/AR_NDH && j < 2e-2*n; i--)
            j += dh[i];
        rmax = min + (i + 1)/q;
    }

    if (from)
        *from = rmin;
    if (to)
        *to = rmax;

    CVAL(data_field, ARF) = rmin;
    CVAL(data_field, ART) = rmax;
    data_field->cached |= CBIT(ARF) | CBIT(ART);
}

/**
 * gwy_data_field_get_stats:
 * @data_field: A data field.
 * @avg: Where average height value of the surface should be stored, or %NULL.
 * @ra: Where average value of irregularities should be stored, or %NULL.
 * @rms: Where root mean square value of irregularities (Rq) should be stored,
 *       or %NULL.
 * @skew: Where skew (symmetry of height distribution) should be stored, or
 *        %NULL.
 * @kurtosis: Where kurtosis (peakedness of height ditribution) should be
 *            stored, or %NULL.
 *
 * Computes basic statistical quantities of a data field.
 **/
void
gwy_data_field_get_stats(GwyDataField *data_field,
                         gdouble *avg,
                         gdouble *ra,
                         gdouble *rms,
                         gdouble *skew,
                         gdouble *kurtosis)
{
    gint i;
    gdouble c_sz2, c_sz3, c_sz4, c_abs1;
    const gdouble *p = data_field->data;
    guint nn = data_field->xres * data_field->yres;
    gdouble dif, myavg, myrms;

    g_return_if_fail(GWY_IS_DATA_FIELD(data_field));

    c_sz2 = c_sz3 = c_sz4 = c_abs1 = 0;

    myavg = gwy_data_field_get_avg(data_field);
    if (avg)
        *avg = myavg;

    for (i = nn; i; i--, p++) {
        dif = (*p - myavg);
        c_abs1 += fabs(dif);
        c_sz2 += dif*dif;
        c_sz3 += dif*dif*dif;
        c_sz4 += dif*dif*dif*dif;

    }

    myrms = c_sz2/nn;
    if (ra)
        *ra = c_abs1/nn;
    if (skew)
        *skew = c_sz3/pow(myrms, 1.5)/nn;
    if (kurtosis)
        *kurtosis = c_sz4/(myrms)/(myrms)/nn - 3;
    if (rms)
        *rms = sqrt(myrms);

    if (!CTEST(data_field, RMS)) {
        CVAL(data_field, RMS) = sqrt(myrms);
        data_field->cached |= CBIT(RMS);
    }
}

/**
 * gwy_data_field_area_get_stats:
 * @data_field: A data field.
 * @mask: Mask specifying which values to take into account/exclude, or %NULL.
 * @col: Upper-left column coordinate.
 * @row: Upper-left row coordinate.
 * @width: Area width (number of columns).
 * @height: Area height (number of rows).
 * @avg: Where average height value of the surface should be stored, or %NULL.
 * @ra: Where average value of irregularities should be stored, or %NULL.
 * @rms: Where root mean square value of irregularities (Rq) should be stored,
 *       or %NULL.
 * @skew: Where skew (symmetry of height distribution) should be stored, or
 *        %NULL.
 * @kurtosis: Where kurtosis (peakedness of height ditribution) should be
 *            stored, or %NULL.
 *
 * Computes basic statistical quantities of a rectangular part of a data field.
 *
 * This function is equivalent to calling @gwy_data_field_area_get_stats_mask()
 * with masking mode %GWY_MASK_INCLUDE.
 **/
void
gwy_data_field_area_get_stats(GwyDataField *dfield,
                              GwyDataField *mask,
                              gint col, gint row,
                              gint width, gint height,
                              gdouble *avg,
                              gdouble *ra,
                              gdouble *rms,
                              gdouble *skew,
                              gdouble *kurtosis)
{
    gwy_data_field_area_get_stats_mask(dfield, mask, GWY_MASK_INCLUDE,
                                       col, row, width, height,
                                       avg, ra, rms, skew, kurtosis);
}

/**
 * gwy_data_field_area_get_stats_mask:
 * @data_field: A data field.
 * @mask: Mask specifying which values to take into account/exclude, or %NULL.
 * @mode: Masking mode to use.  See the introduction for description of
 *        masking modes.
 * @col: Upper-left column coordinate.
 * @row: Upper-left row coordinate.
 * @width: Area width (number of columns).
 * @height: Area height (number of rows).
 * @avg: Where average height value of the surface should be stored, or %NULL.
 * @ra: Where average value of irregularities should be stored, or %NULL.
 * @rms: Where root mean square value of irregularities (Rq) should be stored,
 *       or %NULL.
 * @skew: Where skew (symmetry of height distribution) should be stored, or
 *        %NULL.
 * @kurtosis: Where kurtosis (peakedness of height ditribution) should be
 *            stored, or %NULL.
 *
 * Computes basic statistical quantities of a rectangular part of a data field.
 *
 * Since: 2.18
 **/
void
gwy_data_field_area_get_stats_mask(GwyDataField *dfield,
                                   GwyDataField *mask,
                                   GwyMaskingType mode,
                                   gint col, gint row,
                                   gint width, gint height,
                                   gdouble *avg,
                                   gdouble *ra,
                                   gdouble *rms,
                                   gdouble *skew,
                                   gdouble *kurtosis)
{
    gdouble c_sz2, c_sz3, c_sz4, c_abs1;
    gdouble dif, myavg, myrms;
    const gdouble *datapos, *mpos;
    gint i, j;
    guint nn;

    g_return_if_fail(GWY_IS_DATA_FIELD(dfield));
    g_return_if_fail(!mask || (GWY_IS_DATA_FIELD(mask)
                               && mask->xres == dfield->xres
                               && mask->yres == dfield->yres));
    g_return_if_fail(col >= 0 && row >= 0
                     && width > 0 && height > 0
                     && col + width <= dfield->xres
                     && row + height <= dfield->yres);

    c_sz2 = c_sz3 = c_sz4 = c_abs1 = 0;

    myavg = gwy_data_field_area_get_avg_mask(dfield, mask, mode,
                                             col, row, width, height);
    if (mask && mode != GWY_MASK_IGNORE) {
        datapos = dfield->data + row*dfield->xres + col;
        mpos = mask->data + row*mask->xres + col;
        nn = 0;
        for (i = 0; i < height; i++) {
            const gdouble *drow = datapos + i*dfield->xres;
            const gdouble *mrow = mpos + i*mask->xres;

            if (mode == GWY_MASK_INCLUDE) {
                for (j = 0; j < width; j++) {
                    if (*mrow > 0.0) {
                        dif = *drow - myavg;
                        c_abs1 += fabs(dif);
                        c_sz2 += dif*dif;
                        c_sz3 += dif*dif*dif;
                        c_sz4 += dif*dif*dif*dif;
                        nn++;
                    }
                    drow++;
                    mrow++;
                }
            }
            else {
                for (j = 0; j < width; j++) {
                    if (*mrow < 1.0) {
                        dif = *drow - myavg;
                        c_abs1 += fabs(dif);
                        c_sz2 += dif*dif;
                        c_sz3 += dif*dif*dif;
                        c_sz4 += dif*dif*dif*dif;
                        nn++;
                    }
                    drow++;
                    mrow++;
                }
            }
        }
    }
    else {
        nn = width*height;
        datapos = dfield->data + row*dfield->xres + col;
        for (i = 0; i < height; i++) {
            const gdouble *drow = datapos + i*dfield->xres;

            for (j = 0; j < width; j++) {
                dif = *(drow++) - myavg;
                c_abs1 += fabs(dif);
                c_sz2 += dif*dif;
                c_sz3 += dif*dif*dif;
                c_sz4 += dif*dif*dif*dif;
            }
        }
    }

    myrms = c_sz2/nn;
    if (avg)
        *avg = myavg;
    if (ra)
        *ra = c_abs1/nn;
    if (skew)
        *skew = c_sz3/pow(myrms, 1.5)/nn;
    if (kurtosis)
        *kurtosis = c_sz4/(myrms)/(myrms)/nn - 3;
    if (rms)
        *rms = sqrt(myrms);
}

/**
 * gwy_data_field_area_count_in_range:
 * @data_field: A data field.
 * @mask: Mask specifying which values to take into account, or %NULL.
 * @col: Upper-left column coordinate.
 * @row: Upper-left row coordinate.
 * @width: Area width (number of columns).
 * @height: Area height (number of rows).
 * @below: Upper bound to compare data to.  The number of samples less
 *         than or equal to @below is stored in @nbelow.
 * @above: Lower bound to compare data to.  The number of samples greater
 *         than or equal to @above is stored in @nabove.
 * @nbelow: Location to store the number of samples less than or equal
 *          to @below, or %NULL.
 * @nabove: Location to store the number of samples greater than or equal
 *          to @above, or %NULL.
 *
 * Counts data samples in given range.
 *
 * No assertion is made about the values of @above and @below, in other words
 * @above may be larger than @below.  To count samples in an open interval
 * instead of a closed interval, exchange @below and @above and then subtract
 * the @nabove and @nbelow from @width*@height to get the complementary counts.
 *
 * With this trick the common task of counting positive values can be
 * realized:
 * <informalexample><programlisting>
 * gwy_data_field_area_count_in_range(data_field, NULL,
 *                                    col, row, width, height,
 *                                    0.0, 0.0, &amp;count, NULL);
 * count = width*height - count;
 * </programlisting></informalexample>
 **/
void
gwy_data_field_area_count_in_range(GwyDataField *data_field,
                                   GwyDataField *mask,
                                   gint col, gint row,
                                   gint width, gint height,
                                   gdouble below,
                                   gdouble above,
                                   gint *nbelow,
                                   gint *nabove)
{
    const gdouble *datapos, *mpos;
    gint i, j, na, nb;

    g_return_if_fail(GWY_IS_DATA_FIELD(data_field));
    g_return_if_fail(!mask || (GWY_IS_DATA_FIELD(mask)
                               && mask->xres == data_field->xres
                               && mask->yres == data_field->yres));
    g_return_if_fail(col >= 0 && row >= 0
                     && width > 0 && height > 0
                     && col + width <= data_field->xres
                     && row + height <= data_field->yres);

    if (!nabove && !nbelow)
        return;

    na = nb = 0;
    if (mask) {
        datapos = data_field->data + row*data_field->xres + col;
        mpos = mask->data + row*mask->xres + col;
        for (i = 0; i < height; i++) {
            const gdouble *drow = datapos + i*data_field->xres;
            const gdouble *mrow = mpos + i*mask->xres;

            for (j = 0; j < width; j++) {
                if (*mrow > 0.0) {
                    if (*drow >= above)
                        na++;
                    if (*drow <= below)
                        nb++;
                }
                drow++;
                mrow++;
            }
        }
    }
    else {
        datapos = data_field->data + row*data_field->xres + col;
        for (i = 0; i < height; i++) {
            const gdouble *drow = datapos + i*data_field->xres;

            for (j = 0; j < width; j++) {
                if (*drow >= above)
                    na++;
                if (*drow <= below)
                    nb++;
                drow++;
            }
        }
    }

    if (nabove)
        *nabove = na;
    if (nbelow)
        *nbelow = nb;
}

/**
 * gwy_data_field_area_dh:
 * @data_field: A data field.
 * @mask: Mask specifying which values to take into account, or %NULL.
 * @target_line: A data line to store the distribution to.  It will be
 *               resampled to requested width.
 * @col: Upper-left column coordinate.
 * @row: Upper-left row coordinate.
 * @width: Area width (number of columns).
 * @height: Area height (number of rows).
 * @nstats: The number of samples to take on the distribution function.  If
 *          nonpositive, a suitable resolution is determined automatically.
 *
 * Calculates distribution of heights in a rectangular part of data field.
 **/
void
gwy_data_field_area_dh(GwyDataField *data_field,
                       GwyDataField *mask,
                       GwyDataLine *target_line,
                       gint col, gint row,
                       gint width, gint height,
                       gint nstats)
{
    GwySIUnit *fieldunit, *lineunit, *rhounit;
    gdouble min, max;
    const gdouble *drow, *mrow;
    gint xres, yres, i, j;
    guint *counts;
    guint nn;

    g_return_if_fail(GWY_IS_DATA_FIELD(data_field));
    xres = data_field->xres;
    yres = data_field->yres;
    g_return_if_fail(!mask || (GWY_IS_DATA_FIELD(mask)
                               && mask->xres == xres
                               && mask->yres == yres));
    g_return_if_fail(GWY_IS_DATA_LINE(target_line));
    g_return_if_fail(col >= 0 && row >= 0
                     && width >= 1 && height >= 1
                     && col + width <= xres
                     && row + height <= yres);

    if (mask) {
        nn = 0;
        for (i = 0; i < height; i++) {
            mrow = mask->data + (i + row)*xres + col;
            for (j = 0; j < width; j++) {
                if (mrow[j] > 0.0)
                    nn++;
            }
        }
    }
    else
        nn = width*height;

    if (nstats < 1) {
        nstats = floor(3.49*cbrt(nn) + 0.5);
        nstats = MAX(nstats, 2);
    }

    gwy_data_line_resample(target_line, nstats, GWY_INTERPOLATION_NONE);
    gwy_data_field_area_get_min_max(data_field, nn ? mask : NULL,
                                    col, row, width, height,
                                    &min, &max);

    /* Set proper units */
    fieldunit = gwy_data_field_get_si_unit_z(data_field);
    lineunit = gwy_data_line_get_si_unit_x(target_line);
    gwy_serializable_clone(G_OBJECT(fieldunit), G_OBJECT(lineunit));
    rhounit = gwy_data_line_get_si_unit_y(target_line);
    gwy_si_unit_power(lineunit, -1, rhounit);

    /* Handle border cases */
    if (!nn) {
        gwy_data_line_clear(target_line);
        gwy_data_line_set_real(target_line, min ? max : 1.0);
        return;
    }
    if (min == max) {
        gwy_data_line_clear(target_line);
        gwy_data_line_set_real(target_line, min ? max : 1.0);
        target_line->data[0] = nstats/gwy_data_line_get_real(target_line);
        return;
    }

    /* Calculate height distribution */
    gwy_data_line_set_real(target_line, max - min);
    gwy_data_line_set_offset(target_line, min);
    counts = g_new(guint, nstats);

    if (mask) {
        gdouble *values = g_new(gdouble, nn);

        nn = 0;
        for (i = 0; i < height; i++) {
            drow = data_field->data + (i + row)*xres + col;
            mrow = mask->data + (i + row)*xres + col;

            for (j = 0; j < width; j++) {
                if (mrow[j] > 0.0)
                    values[nn++] = drow[j];
            }
        }
        nn = gwy_math_histogram(values, nn, min, max, nstats, counts);
        g_free(values);
    }
    else {
        nn = gwy_math_histogram(data_field->data, nn,
                                min, max, nstats, counts);
    }

    for (i = 0; i < nstats; i++)
        target_line->data[i] = counts[i];
    g_free(counts);

    /* Normalize integral to 1 */
    gwy_data_line_multiply(target_line, nstats/(max - min)/MAX(nn, 1));
}

/**
 * gwy_data_field_dh:
 * @data_field: A data field.
 * @target_line: A data line to store the distribution to.  It will be
 *               resampled to requested width.
 * @nstats: The number of samples to take on the distribution function.  If
 *          nonpositive, a suitable resolution is determined automatically.
 *
 * Calculates distribution of heights in a data field.
 **/
void
gwy_data_field_dh(GwyDataField *data_field,
                  GwyDataLine *target_line,
                  gint nstats)
{
    g_return_if_fail(GWY_IS_DATA_FIELD(data_field));
    gwy_data_field_area_dh(data_field, NULL, target_line,
                           0, 0, data_field->xres, data_field->yres,
                           nstats);
}

/**
 * gwy_data_field_area_cdh:
 * @data_field: A data field.
 * @mask: Mask specifying which values to take into account, or %NULL.
 * @target_line: A data line to store the distribution to.  It will be
 *               resampled to requested width.
 * @col: Upper-left column coordinate.
 * @row: Upper-left row coordinate.
 * @width: Area width (number of columns).
 * @height: Area height (number of rows).
 * @nstats: The number of samples to take on the distribution function.  If
 *          nonpositive, a suitable resolution is determined automatically.
 *
 * Calculates cumulative distribution of heights in a rectangular part of data
 * field.
 **/
void
gwy_data_field_area_cdh(GwyDataField *data_field,
                        GwyDataField *mask,
                        GwyDataLine *target_line,
                        gint col, gint row,
                        gint width, gint height,
                        gint nstats)
{
    GwySIUnit *rhounit, *lineunit;

    gwy_data_field_area_dh(data_field, mask, target_line,
                           col, row, width, height,
                           nstats);
    gwy_data_line_cumulate(target_line);
    gwy_data_line_multiply(target_line, gwy_data_line_itor(target_line, 1));
    target_line->data[target_line->res-1] = 1.0;   /* Fix rounding errors. */

    /* Update units after integration */
    lineunit = gwy_data_line_get_si_unit_x(target_line);
    rhounit = gwy_data_line_get_si_unit_y(target_line);
    gwy_si_unit_multiply(rhounit, lineunit, rhounit);
}

/**
 * gwy_data_field_cdh:
 * @data_field: A data field.
 * @target_line: A data line to store the distribution to.  It will be
 *               resampled to requested width.
 * @nstats: The number of samples to take on the distribution function.  If
 *          nonpositive, a suitable resolution is determined automatically.
 *
 * Calculates cumulative distribution of heights in a data field.
 **/
void
gwy_data_field_cdh(GwyDataField *data_field,
                   GwyDataLine *target_line,
                   gint nstats)
{
    g_return_if_fail(GWY_IS_DATA_FIELD(data_field));
    gwy_data_field_area_cdh(data_field, NULL, target_line,
                            0, 0, data_field->xres, data_field->yres,
                            nstats);
}

/**
 * gwy_data_field_area_da_mask:
 * @data_field: A data field.
 * @mask: Mask specifying which values to take into account, or %NULL.
 * @target_line: A data line to store the distribution to.  It will be
 *               resampled to requested width.
 * @col: Upper-left column coordinate.
 * @row: Upper-left row coordinate.
 * @width: Area width (number of columns).
 * @height: Area height (number of rows).
 * @orientation: Orientation to compute the slope distribution in.
 * @nstats: The number of samples to take on the distribution function.  If
 *          nonpositive, a suitable resolution is determined automatically.
 *
 * Calculates distribution of slopes in a rectangular part of data field, with
 * masking.
 *
 * Since: 2.49
 **/
void
gwy_data_field_area_da_mask(GwyDataField *data_field,
                            GwyDataField *mask,
                            GwyDataLine *target_line,
                            gint col, gint row,
                            gint width, gint height,
                            GwyOrientation orientation,
                            gint nstats)
{
    GwySIUnit *lineunit, *rhounit;
    const gdouble *drow, *mrow;
    gdouble *values;
    guint *counts;
    gdouble min, max, q;
    gint xres, yres, i, j, nn;

    g_return_if_fail(GWY_IS_DATA_FIELD(data_field));
    xres = data_field->xres;
    yres = data_field->yres;
    g_return_if_fail(col >= 0 && row >= 0
                     && width >= 1 && height >= 1
                     && col + width <= xres
                     && row + height <= yres);
    g_return_if_fail(!mask || (GWY_IS_DATA_FIELD(mask)
                               && mask->xres == xres
                               && mask->yres == yres));
    g_return_if_fail(GWY_IS_DATA_LINE(target_line));

    if (mask) {
        nn = 0;
        if (orientation == GWY_ORIENTATION_VERTICAL) {
            for (i = 0; i < height-1; i++) {
                mrow = mask->data + (i + row)*xres + col;
                for (j = 0; j < width; j++) {
                    if (mrow[j] > 0.0 && mrow[j+xres] > 0.0)
                        nn++;
                }
            }
        }
        else {
            for (i = 0; i < height; i++) {
                mrow = mask->data + (i + row)*xres + col;
                for (j = 0; j < width-1; j++) {
                    if (mrow[j] > 0.0 && mrow[j+1] > 0.0)
                        nn++;
                }
            }
        }
    }
    else {
        if (orientation == GWY_ORIENTATION_VERTICAL)
            nn = width*(height - 1);
        else
            nn = (width - 1)*height;
    }

    if (nstats < 1) {
        nstats = floor(3.49*cbrt(nn) + 0.5);
        nstats = MAX(nstats, 2);
    }

    gwy_data_line_resample(target_line, nstats, GWY_INTERPOLATION_NONE);

    if (orientation == GWY_ORIENTATION_VERTICAL)
        q = yres/data_field->yreal;
    else
        q = xres/data_field->xreal;

    values = g_new(gdouble, MAX(nn, 1));
    nn = 0;
    if (mask) {
        if (orientation == GWY_ORIENTATION_VERTICAL) {
            for (i = 0; i < height-1; i++) {
                mrow = mask->data + (i + row)*xres + col;
                drow = data_field->data + (i + row)*xres + col;
                for (j = 0; j < width; j++) {
                    if (mrow[j] > 0.0 && mrow[j+xres] > 0.0)
                        values[nn++] = q*(drow[j+xres] - drow[j]);
                }
            }
        }
        else {
            for (i = 0; i < height; i++) {
                mrow = mask->data + (i + row)*xres + col;
                drow = data_field->data + (i + row)*xres + col;
                for (j = 0; j < width-1; j++) {
                    if (mrow[j] > 0.0 && mrow[j+1] > 0.0)
                        values[nn++] = q*(drow[j+1] - drow[j]);
                }
            }
        }
    }
    else {
        if (orientation == GWY_ORIENTATION_VERTICAL) {
            for (i = 0; i < height-1; i++) {
                drow = data_field->data + (i + row)*xres + col;
                for (j = 0; j < width; j++)
                    values[nn++] = q*(drow[j+xres] - drow[j]);
            }
        }
        else {
            for (i = 0; i < height; i++) {
                drow = data_field->data + (i + row)*xres + col;
                for (j = 0; j < width-1; j++)
                    values[nn++] = q*(drow[j+1] - drow[j]);
            }
        }
    }

    min = max = values[0];
    for (i = 1; i < nn; i++) {
        if (values[i] < min)
            min = values[i];
        if (values[i] > max)
            max = values[i];
    }

    /* Handle border cases */
    if (!nn) {
        gwy_data_line_clear(target_line);
        gwy_data_line_set_real(target_line, 1.0);
        gwy_data_line_set_offset(target_line, -0.5);
        return;
    }
    if (min == max) {
        gwy_data_line_clear(target_line);
        gwy_data_line_set_real(target_line, min ? max : 1.0);
        target_line->data[0] = nstats/gwy_data_line_get_real(target_line);
        return;
    }

    counts = g_new(guint, nstats);
    nn = gwy_math_histogram(values, nn, min, max, nstats, counts);
    g_free(values);

    for (i = 0; i < nstats; i++)
        target_line->data[i] = counts[i];
    g_free(counts);

    gwy_data_line_set_real(target_line, max - min);
    gwy_data_line_set_offset(target_line, min);
    gwy_data_line_multiply(target_line, nstats/(max - min)/nn);

    /* Set proper units */
    lineunit = gwy_data_line_get_si_unit_x(target_line);
    gwy_si_unit_divide(gwy_data_field_get_si_unit_z(data_field),
                       gwy_data_field_get_si_unit_xy(data_field),
                       lineunit);
    rhounit = gwy_data_line_get_si_unit_y(target_line);
    gwy_si_unit_power(lineunit, -1, rhounit);
}

/**
 * gwy_data_field_area_da:
 * @data_field: A data field.
 * @target_line: A data line to store the distribution to.  It will be
 *               resampled to requested width.
 * @col: Upper-left column coordinate.
 * @row: Upper-left row coordinate.
 * @width: Area width (number of columns).
 * @height: Area height (number of rows).
 * @orientation: Orientation to compute the slope distribution in.
 * @nstats: The number of samples to take on the distribution function.  If
 *          nonpositive, a suitable resolution is determined automatically.
 *
 * Calculates distribution of slopes in a rectangular part of data field.
 **/
void
gwy_data_field_area_da(GwyDataField *data_field,
                       GwyDataLine *target_line,
                       gint col, gint row,
                       gint width, gint height,
                       GwyOrientation orientation,
                       gint nstats)
{
    gwy_data_field_area_da_mask(data_field, NULL, target_line,
                                col, row, width, height, orientation, nstats);
}

/**
 * gwy_data_field_da:
 * @data_field: A data field.
 * @target_line: A data line to store the distribution to.  It will be
 *               resampled to requested width.
 * @orientation: Orientation to compute the slope distribution in.
 * @nstats: The number of samples to take on the distribution function.  If
 *          nonpositive, a suitable resolution is determined automatically.
 *
 * Calculates distribution of slopes in a data field.
 **/
void
gwy_data_field_da(GwyDataField *data_field,
                  GwyDataLine *target_line,
                  GwyOrientation orientation,
                  gint nstats)
{
    gwy_data_field_area_da(data_field, target_line,
                           0, 0, data_field->xres, data_field->yres,
                           orientation, nstats);
}

/**
 * gwy_data_field_area_cda:
 * @data_field: A data field.
 * @target_line: A data line to store the distribution to.  It will be
 *               resampled to requested width.
 * @col: Upper-left column coordinate.
 * @row: Upper-left row coordinate.
 * @width: Area width (number of columns).
 * @height: Area height (number of rows).
 * @orientation: Orientation to compute the slope distribution in.
 * @nstats: The number of samples to take on the distribution function.  If
 *          nonpositive, a suitable resolution is determined automatically.
 *
 * Calculates cumulative distribution of slopes in a rectangular part of data
 * field.
 **/
void
gwy_data_field_area_cda(GwyDataField *data_field,
                        GwyDataLine *target_line,
                        gint col, gint row,
                        gint width, gint height,
                        GwyOrientation orientation,
                        gint nstats)
{
    gwy_data_field_area_cda_mask(data_field, NULL, target_line,
                                 col, row, width, height, orientation, nstats);
}

/**
 * gwy_data_field_area_cda_mask:
 * @data_field: A data field.
 * @target_line: A data line to store the distribution to.  It will be
 *               resampled to requested width.
 * @col: Upper-left column coordinate.
 * @row: Upper-left row coordinate.
 * @width: Area width (number of columns).
 * @height: Area height (number of rows).
 * @orientation: Orientation to compute the slope distribution in.
 * @nstats: The number of samples to take on the distribution function.  If
 *          nonpositive, a suitable resolution is determined automatically.
 *
 * Calculates cumulative distribution of slopes in a rectangular part of data
 * field, with masking.
 *
 * Since: 2.49
 **/
void
gwy_data_field_area_cda_mask(GwyDataField *data_field,
                             GwyDataField *mask,
                             GwyDataLine *target_line,
                             gint col, gint row,
                             gint width, gint height,
                             GwyOrientation orientation,
                             gint nstats)
{
    GwySIUnit *lineunit, *rhounit;

    gwy_data_field_area_da_mask(data_field, mask, target_line,
                                col, row, width, height,
                                orientation, nstats);
    gwy_data_line_cumulate(target_line);
    gwy_data_line_multiply(target_line, gwy_data_line_itor(target_line, 1));
    target_line->data[target_line->res-1] = 1.0;   /* Fix rounding errors. */

    /* Update units after integration */
    lineunit = gwy_data_line_get_si_unit_x(target_line);
    rhounit = gwy_data_line_get_si_unit_y(target_line);
    gwy_si_unit_multiply(rhounit, lineunit, rhounit);
}

/**
 * gwy_data_field_cda:
 * @data_field: A data field.
 * @target_line: A data line to store the distribution to.  It will be
 *               resampled to requested width.
 * @orientation: Orientation to compute the slope distribution in.
 * @nstats: The number of samples to take on the distribution function.  If
 *          nonpositive, a suitable resolution is determined automatically.
 *
 * Calculates cumulative distribution of slopes in a data field.
 **/
void
gwy_data_field_cda(GwyDataField *data_field,
                   GwyDataLine *target_line,
                   GwyOrientation orientation,
                   gint nstats)
{
    g_return_if_fail(GWY_IS_DATA_FIELD(data_field));
    gwy_data_field_area_cda(data_field, target_line,
                            0, 0, data_field->xres, data_field->yres,
                            orientation, nstats);
}

/**
 * gwy_data_field_area_minkowski_volume:
 * @data_field: A data field.
 * @target_line: A data line to store the distribution to.  It will be
 *               resampled to requested width.
 * @col: Upper-left column coordinate.
 * @row: Upper-left row coordinate.
 * @width: Area width (number of columns).
 * @height: Area height (number of rows).
 * @nstats: The number of samples to take on the distribution function.  If
 *          nonpositive, a suitable resolution is determined automatically.
 *
 * Calculates Minkowski volume functional of a rectangular part of a data
 * field.
 *
 * Volume functional is calculated as the number of values above each
 * threshold value (,white pixels`) divided by the total number of samples
 * in the area.  Is it's equivalent to 1-CDH.
 **/
void
gwy_data_field_area_minkowski_volume(GwyDataField *data_field,
                                     GwyDataLine *target_line,
                                     gint col, gint row,
                                     gint width, gint height,
                                     gint nstats)
{
    gwy_data_field_area_cdh(data_field, NULL, target_line,
                            col, row, width, height,
                            nstats);
    gwy_data_line_multiply(target_line, -1.0);
    gwy_data_line_add(target_line, 1.0);
}

/**
 * gwy_data_field_minkowski_volume:
 * @data_field: A data field.
 * @target_line: A data line to store the distribution to.  It will be
 *               resampled to requested width.
 * @nstats: The number of samples to take on the distribution function.  If
 *          nonpositive, a suitable resolution is determined automatically.
 *
 * Calculates Minkowski volume functional of a data field.
 *
 * See gwy_data_field_area_minkowski_volume() for details.
 **/
void
gwy_data_field_minkowski_volume(GwyDataField *data_field,
                                GwyDataLine *target_line,
                                gint nstats)
{
    gwy_data_field_cdh(data_field, target_line, nstats);
    gwy_data_line_multiply(target_line, -1.0);
    gwy_data_line_add(target_line, 1.0);
}

/**
 * gwy_data_field_area_minkowski_boundary:
 * @data_field: A data field.
 * @target_line: A data line to store the distribution to.  It will be
 *               resampled to requested width.
 * @col: Upper-left column coordinate.
 * @row: Upper-left row coordinate.
 * @width: Area width (number of columns).
 * @height: Area height (number of rows).
 * @nstats: The number of samples to take on the distribution function.  If
 *          nonpositive, a suitable resolution is determined automatically.
 *
 * Calculates Minkowski boundary functional of a rectangular part of a data
 * field.
 *
 * Boundary functional is calculated as the number of boundaries for each
 * threshold value (the number of pixel sides where of neighouring pixels is
 * ,white` and the other ,black`) divided by the total number of samples
 * in the area.
 **/
void
gwy_data_field_area_minkowski_boundary(GwyDataField *data_field,
                                       GwyDataLine *target_line,
                                       gint col, gint row,
                                       gint width, gint height,
                                       gint nstats)
{
    GwySIUnit *fieldunit, *lineunit;
    const gdouble *data;
    gdouble *line;
    gdouble min, max, q;
    gint xres, i, j, k, k0, kr, kd;

    g_return_if_fail(GWY_IS_DATA_FIELD(data_field));
    g_return_if_fail(GWY_IS_DATA_LINE(target_line));
    g_return_if_fail(col >= 0 && row >= 0
                     && width >= 0 && height >= 0
                     && col + width <= data_field->xres
                     && row + height <= data_field->yres);

    if (nstats < 1) {
        nstats = floor(3.49*cbrt(width*height) + 0.5);
        nstats = MAX(nstats, 2);
    }

    gwy_data_line_resample(target_line, nstats, GWY_INTERPOLATION_NONE);
    gwy_data_line_clear(target_line);
    gwy_data_field_area_get_min_max(data_field, NULL,
                                    col, row, width, height,
                                    &min, &max);
    /* There are no boundaries on a totally flat sufrace */
    if (min == max || width == 0 || height == 0)
        return;

    xres = data_field->xres;
    q = nstats/(max - min);
    line = target_line->data;

    for (i = 0; i < height-1; i++) {
        kr = (gint)((data_field->data[i*xres + col] - min)*q);
        for (j = 0; j < width-1; j++) {
            data = data_field->data + (i + row)*xres + (col + j);

            k0 = kr;

            kr = (gint)((data[1] - min)*q);
            for (k = MAX(MIN(k0, kr), 0); k < MIN(MAX(k0, kr), nstats); k++)
                line[k] += 1;

            kd = (gint)((data[xres] - min)*q);
            for (k = MAX(MIN(k0, kd), 0); k < MIN(MAX(k0, kd), nstats); k++)
                line[k] += 1;
        }
    }

    gwy_data_line_multiply(target_line, 1.0/(width*height));
    gwy_data_line_set_real(target_line, max - min);
    gwy_data_line_set_offset(target_line, min);

    /* Set proper units */
    fieldunit = gwy_data_field_get_si_unit_z(data_field);
    lineunit = gwy_data_line_get_si_unit_x(target_line);
    gwy_serializable_clone(G_OBJECT(fieldunit), G_OBJECT(lineunit));
    lineunit = gwy_data_line_get_si_unit_y(target_line);
    gwy_si_unit_set_from_string(lineunit, NULL);
}

/**
 * gwy_data_field_minkowski_boundary:
 * @data_field: A data field.
 * @target_line: A data line to store the distribution to.  It will be
 *               resampled to requested width.
 * @nstats: The number of samples to take on the distribution function.  If
 *          nonpositive, a suitable resolution is determined automatically.
 *
 * Calculates Minkowski boundary functional of a data field.
 *
 * See gwy_data_field_area_minkowski_boundary() for details.
 **/
void
gwy_data_field_minkowski_boundary(GwyDataField *data_field,
                                  GwyDataLine *target_line,
                                  gint nstats)
{
    g_return_if_fail(GWY_IS_DATA_FIELD(data_field));
    gwy_data_field_area_minkowski_boundary(data_field, target_line,
                                           0, 0,
                                           data_field->xres, data_field->yres,
                                           nstats);
}

/**
 * gwy_data_field_area_minkowski_euler:
 * @data_field: A data field.
 * @target_line: A data line to store the distribution to.  It will be
 *               resampled to requested width.
 * @col: Upper-left column coordinate.
 * @row: Upper-left row coordinate.
 * @width: Area width (number of columns).
 * @height: Area height (number of rows).
 * @nstats: The number of samples to take on the distribution function.  If
 *          nonpositive, a suitable resolution is determined automatically.
 *
 * Calculates Minkowski connectivity functional (Euler characteristics) of
 * a rectangular part of a data field.
 *
 * Connectivity functional is calculated as the number connected areas of
 * pixels above threhsold (,white`) minus the number of connected areas of
 * pixels below threhsold (,black`) for each threshold value, divided by the
 * total number of samples in the area.
 **/
void
gwy_data_field_area_minkowski_euler(GwyDataField *data_field,
                                    GwyDataLine *target_line,
                                    gint col, gint row,
                                    gint width, gint height,
                                    gint nstats)
{
    GwySIUnit *fieldunit, *lineunit;
    GwyDataLine *tmp_line;
    gint i;

    g_return_if_fail(GWY_IS_DATA_FIELD(data_field));
    g_return_if_fail(GWY_IS_DATA_LINE(target_line));
    g_return_if_fail(col >= 0 && row >= 0
                     && width >= 0 && height >= 0
                     && col + width <= data_field->xres
                     && row + height <= data_field->yres);

    if (nstats < 1) {
        nstats = floor(3.49*cbrt(width*height) + 0.5);
        nstats = MAX(nstats, 2);
    }

    gwy_data_line_resample(target_line, nstats, GWY_INTERPOLATION_NONE);
    tmp_line = gwy_data_line_new_alike(target_line, FALSE);

    gwy_data_field_area_grains_tgnd(data_field, target_line,
                                    col, row, width, height,
                                    FALSE, nstats);
    gwy_data_field_area_grains_tgnd(data_field, tmp_line,
                                    col, row, width, height,
                                    TRUE, nstats);

    for (i = 0; i < nstats; i++)
        target_line->data[i] -= tmp_line->data[nstats-1 - i];
    g_object_unref(tmp_line);

    gwy_data_line_multiply(target_line, 1.0/(width*height));
    gwy_data_line_invert(target_line, TRUE, FALSE);

    /* Set proper units */
    fieldunit = gwy_data_field_get_si_unit_z(data_field);
    lineunit = gwy_data_line_get_si_unit_x(target_line);
    gwy_serializable_clone(G_OBJECT(fieldunit), G_OBJECT(lineunit));
    lineunit = gwy_data_line_get_si_unit_y(target_line);
    gwy_si_unit_set_from_string(lineunit, NULL);
}

/**
 * gwy_data_field_minkowski_euler:
 * @data_field: A data field.
 * @target_line: A data line to store the distribution to.  It will be
 *               resampled to requested width.
 * @nstats: The number of samples to take on the distribution function.  If
 *          nonpositive, a suitable resolution is determined automatically.
 *
 * Calculates Minkowski connectivity functional (Euler characteristics) of
 * a data field.
 *
 * See gwy_data_field_area_minkowski_euler() for details.
 **/
void
gwy_data_field_minkowski_euler(GwyDataField *data_field,
                               GwyDataLine *target_line,
                               gint nstats)
{
    g_return_if_fail(GWY_IS_DATA_FIELD(data_field));
    gwy_data_field_area_minkowski_euler(data_field, target_line,
                                        0, 0,
                                        data_field->xres, data_field->yres,
                                        nstats);
}

/**
 * gwy_data_field_slope_distribution:
 * @data_field: A data field.
 * @derdist: A data line to fill with angular slope distribution. Its
 *           resolution determines resolution of the distribution.
 * @kernel_size: If positive, local plane fitting will be used for slope
 *               computation; if nonpositive, plain central derivations
 *               will be used.
 *
 * Computes angular slope distribution.
 **/
void
gwy_data_field_slope_distribution(GwyDataField *dfield,
                                  GwyDataLine *derdist,
                                  gint kernel_size)
{
    GwySIUnit *lineunit;
    gdouble *data, *der;
    gdouble bx, by, phi;
    gint xres, yres, nder;
    gint col, row, iphi;

    g_return_if_fail(GWY_IS_DATA_FIELD(dfield));
    g_return_if_fail(GWY_IS_DATA_LINE(derdist));

    nder = gwy_data_line_get_res(derdist);
    der = gwy_data_line_get_data(derdist);
    data = dfield->data;
    xres = dfield->xres;
    yres = dfield->yres;
    gwy_clear(der, nder);
    if (kernel_size > 0) {
        for (row = 0; row + kernel_size < yres; row++) {
            for (col = 0; col + kernel_size < xres; col++) {
                gwy_data_field_area_fit_plane(dfield, NULL, col, row,
                                              kernel_size, kernel_size,
                                              NULL, &bx, &by);
                phi = atan2(by, bx);
                iphi = (gint)floor(nder*(phi + G_PI)/(2.0*G_PI));
                iphi = CLAMP(iphi, 0, nder-1);
                der[iphi] += hypot(bx, by);
            }
        }
    }
    else {
        gdouble qx = xres/gwy_data_field_get_xreal(dfield);
        gdouble qy = yres/gwy_data_field_get_yreal(dfield);

        for (row = 1; row + 1 < yres; row++) {
            for (col = 1; col + 1 < xres; col++) {
                bx = data[row*xres + col + 1] - data[row*xres + col - 1];
                by = data[row*xres + xres + col] - data[row*xres - xres + col];
                phi = atan2(by*qy, bx*qx);
                iphi = (gint)floor(nder*(phi + G_PI)/(2.0*G_PI));
                iphi = CLAMP(iphi, 0, nder-1);
                der[iphi] += hypot(bx, by);
            }
        }
    }

    /* Set proper units */
    lineunit = gwy_data_line_get_si_unit_x(derdist);
    gwy_si_unit_set_from_string(lineunit, NULL);
    lineunit = gwy_data_line_get_si_unit_y(derdist);
    gwy_si_unit_divide(gwy_data_field_get_si_unit_z(dfield),
                       gwy_data_field_get_si_unit_xy(dfield),
                       lineunit);
}

/**
 * gwy_data_field_area_get_median:
 * @data_field: A data field.
 * @mask: Mask specifying which values to take into account/exclude, or %NULL.
 * @col: Upper-left column coordinate.
 * @row: Upper-left row coordinate.
 * @width: Area width (number of columns).
 * @height: Area height (number of rows).
 *
 * Computes median value of a data field area.
 *
 * This function is equivalent to calling
 * @gwy_data_field_area_get_median_mask()
 * with masking mode %GWY_MASK_INCLUDE.
 *
 * Returns: The median value.
 **/
gdouble
gwy_data_field_area_get_median(GwyDataField *dfield,
                               GwyDataField *mask,
                               gint col, gint row,
                               gint width, gint height)
{
    return gwy_data_field_area_get_median_mask(dfield, mask, GWY_MASK_INCLUDE,
                                               col, row, width, height);
}

/**
 * gwy_data_field_area_get_median_mask:
 * @data_field: A data field.
 * @mask: Mask specifying which values to take into account/exclude, or %NULL.
 * @mode: Masking mode to use.  See the introduction for description of
 *        masking modes.
 * @col: Upper-left column coordinate.
 * @row: Upper-left row coordinate.
 * @width: Area width (number of columns).
 * @height: Area height (number of rows).
 *
 * Computes median value of a data field area.
 *
 * Returns: The median value.
 *
 * Since: 2.18
 **/
gdouble
gwy_data_field_area_get_median_mask(GwyDataField *dfield,
                                    GwyDataField *mask,
                                    GwyMaskingType mode,
                                    gint col, gint row,
                                    gint width, gint height)
{
    gdouble median = 0.0;
    const gdouble *datapos, *mpos;
    gdouble *buffer;
    gint i, j;
    guint nn;

    g_return_val_if_fail(GWY_IS_DATA_FIELD(dfield), median);
    g_return_val_if_fail(!mask || (GWY_IS_DATA_FIELD(mask)
                                   && mask->xres == dfield->xres
                                   && mask->yres == dfield->yres),
                         median);
    g_return_val_if_fail(col >= 0 && row >= 0
                         && width >= 0 && height >= 0
                         && col + width <= dfield->xres
                         && row + height <= dfield->yres,
                         median);
    if (!width || !height)
        return median;

    if (mask && mode != GWY_MASK_IGNORE) {
        buffer = g_new(gdouble, width*height);
        datapos = dfield->data + row*dfield->xres + col;
        mpos = mask->data + row*mask->xres + col;
        nn = 0;
        for (i = 0; i < height; i++) {
            const gdouble *drow = datapos + i*dfield->xres;
            const gdouble *mrow = mpos + i*mask->xres;

            if (mode == GWY_MASK_INCLUDE) {
                for (j = 0; j < width; j++) {
                    if (*mrow > 0.0) {
                        buffer[nn] = *drow;
                        nn++;
                    }
                    drow++;
                    mrow++;
                }
            }
            else {
                for (j = 0; j < width; j++) {
                    if (*mrow < 1.0) {
                        buffer[nn] = *drow;
                        nn++;
                    }
                    drow++;
                    mrow++;
                }
            }
        }

        if (nn)
            median = gwy_math_median(nn, buffer);

        g_free(buffer);

        return median;
    }

    if (col == 0 && width == dfield->xres
        && row == 0 && height == dfield->yres)
        return gwy_data_field_get_median(dfield);

    buffer = g_new(gdouble, width*height);
    datapos = dfield->data + row*dfield->xres + col;
    if (height == 1 || (col == 0 && width == dfield->xres))
        gwy_assign(buffer, datapos, width*height);
    else {
        for (i = 0; i < height; i++)
            gwy_assign(buffer + i*width, datapos + i*dfield->xres, width);
    }
    median = gwy_math_median(width*height, buffer);
    g_free(buffer);

    return median;
}

/**
 * gwy_data_field_get_median:
 * @data_field: A data field.
 *
 * Computes median value of a data field.
 *
 * This quantity is cached.
 *
 * Returns: The median value.
 **/
gdouble
gwy_data_field_get_median(GwyDataField *data_field)
{
    gint xres, yres;
    gdouble *buffer;
    gdouble med;

    g_return_val_if_fail(GWY_IS_DATA_FIELD(data_field), 0.0);

    gwy_debug("%s", CTEST(data_field, MED) ? "cache" : "lame");
    if (CTEST(data_field, MED))
        return CVAL(data_field, MED);

    xres = data_field->xres;
    yres = data_field->yres;
    buffer = g_memdup(data_field->data, xres*yres*sizeof(gdouble));
    med = gwy_math_median(xres*yres, buffer);
    g_free(buffer);

    CVAL(data_field, MED) = med;
    data_field->cached |= CBIT(MED);

    return med;
}

/**
 * gwy_data_field_area_get_normal_coeffs:
 * @data_field: A data field.
 * @col: Upper-left column coordinate.
 * @row: Upper-left row coordinate.
 * @width: Area width (number of columns).
 * @height: Area height (number of rows).
 * @nx: Where x-component of average normal vector should be stored, or %NULL.
 * @ny: Where y-component of average normal vector should be stored, or %NULL.
 * @nz: Where z-component of average normal vector should be stored, or %NULL.
 * @normalize1: true to normalize the normal vector to 1, false to normalize
 *              the vector so that z-component is 1.
 *
 * Computes average normal vector of an area of a data field.
 **/
void
gwy_data_field_area_get_normal_coeffs(GwyDataField *data_field,
                                      gint col, gint row,
                                      gint width, gint height,
                                      gdouble *nx, gdouble *ny, gdouble *nz,
                                      gboolean normalize1)
{
    gint i, j;
    int ctr = 0;
    gdouble d1x, d1y, d1z, d2x, d2y, d2z, dcx, dcy, dcz, dd;
    gdouble sumdx = 0.0, sumdy = 0.0, sumdz = 0.0, sumw = 0.0;
    gdouble avgdx, avgdy, avgdz;

    g_return_if_fail(GWY_IS_DATA_FIELD(data_field));
    g_return_if_fail(col >= 0 && row >= 0
                     && width > 0 && height > 0
                     && col + width <= data_field->xres
                     && row + height <= data_field->yres);
    /* This probably should not be enforced */
    /*
    g_return_if_fail(gwy_si_unit_equal(gwy_data_field_get_si_unit_xy(a),
                                       gwy_data_field_get_si_unit_z(a)),
                     FALSE);
                     */

    for (i = col; i < col + width; i++) {
        for (j = row; j < row + height; j++) {
            d1x = 1.0;
            d1y = 0.0;
            d1z = gwy_data_field_get_xder(data_field, i, j);
            d2x = 0.0;
            d2y = 1.0;
            d2z = gwy_data_field_get_yder(data_field, i, j);
            /* Cross product = normal vector */
            dcx = d1y*d2z - d1z*d2y;
            dcy = d1z*d2x - d1x*d2z;
            dcz = d1x*d2y - d1y*d2x; /* Always 1 */
            /* Normalize and add */
            dd = sqrt(dcx*dcx + dcy*dcy + dcz*dcz);
            dcx /= dd;
            sumdx += dcx;
            dcy /= dd;
            sumdy += dcy;
            dcz /= dd;
            sumdz += dcz;
            sumw += 1.0/dd;
            ctr++;
        }
    }
    /* average dimensionless normal vector */
    if (normalize1) {
        /* normalize to 1 */
        avgdx = sumdx/ctr;
        avgdy = sumdy/ctr;
        avgdz = sumdz/ctr;
    }
    else {
        /* normalize for gwy_data_field_plane_level */
        avgdx = sumdx/sumw;
        avgdy = sumdy/sumw;
        avgdz = sumdz/sumw;
    }

    if (nx)
        *nx = avgdx;
    if (ny)
        *ny = avgdy;
    if (nz)
        *nz = avgdz;
}


/**
 * gwy_data_field_get_normal_coeffs:
 * @data_field: A data field.
 * @nx: Where x-component of average normal vector should be stored, or %NULL.
 * @ny: Where y-component of average normal vector should be stored, or %NULL.
 * @nz: Where z-component of average normal vector should be stored, or %NULL.
 * @normalize1: true to normalize the normal vector to 1, false to normalize
 *              the vector so that z-component is 1.
 *
 * Computes average normal vector of a data field.
 **/
void
gwy_data_field_get_normal_coeffs(GwyDataField *data_field,
                                 gdouble *nx, gdouble *ny, gdouble *nz,
                                 gboolean normalize1)
{
    g_return_if_fail(GWY_IS_DATA_FIELD(data_field));
    gwy_data_field_area_get_normal_coeffs(data_field,
                                          0, 0,
                                          data_field->xres, data_field->yres,
                                          nx, ny, nz, normalize1);
}


/**
 * gwy_data_field_area_get_inclination:
 * @data_field: A data field.
 * @col: Upper-left column coordinate.
 * @row: Upper-left row coordinate.
 * @width: Area width (number of columns).
 * @height: Area height (number of rows).
 * @theta: Where theta angle (in radians) should be stored, or %NULL.
 * @phi: Where phi angle (in radians) should be stored, or %NULL.
 *
 * Calculates the inclination of the image (polar and azimuth angle).
 **/
void
gwy_data_field_area_get_inclination(GwyDataField *data_field,
                                    gint col, gint row,
                                    gint width, gint height,
                                    gdouble *theta,
                                    gdouble *phi)
{
    gdouble nx, ny, nz, nr;

    g_return_if_fail(GWY_IS_DATA_FIELD(data_field));
    g_return_if_fail(col >= 0 && row >= 0
                     && width > 0 && height > 0
                     && col + width <= data_field->xres
                     && row + height <= data_field->yres);
    gwy_data_field_area_get_normal_coeffs(data_field,
                                          col, row, width, height,
                                          &nx, &ny, &nz, TRUE);

    nr = hypot(nx, ny);
    if (theta)
        *theta = atan2(nr, nz);
    if (phi)
        *phi = atan2(ny, nx);
}


/**
 * gwy_data_field_get_inclination:
 * @data_field: A data field.
 * @theta: Where theta angle (in radians) should be stored, or %NULL.
 * @phi: Where phi angle (in radians) should be stored, or %NULL.
 *
 * Calculates the inclination of the image (polar and azimuth angle).
 **/
void
gwy_data_field_get_inclination(GwyDataField *data_field,
                               gdouble *theta,
                               gdouble *phi)
{
    g_return_if_fail(GWY_IS_DATA_FIELD(data_field));
    gwy_data_field_area_get_inclination(data_field,
                                        0, 0,
                                        data_field->xres, data_field->yres,
                                        theta,
                                        phi);
}

static gint
extract_field_row_masked(GwyDataField *dfield,
                         GwyDataField *mask,
                         GwyMaskingType masking,
                         gdouble *values,
                         gint col, gint row, gint width)
{
    gint xres = dfield->xres;
    const gdouble *d = dfield->data + row*xres + col, *m;
    gint i, n;

    if (!mask)
        masking = GWY_MASK_IGNORE;

    if (masking == GWY_MASK_INCLUDE) {
        m = mask->data + row*xres + col;
        for (i = n = 0; i < width; i++) {
            if (m[i] > 0.0)
                values[n++] = d[i];
        }
    }
    else if (masking == GWY_MASK_EXCLUDE) {
        m = mask->data + row*xres + col;
        for (i = n = 0; i < width; i++) {
            if (m[i] <= 0.0)
                values[n++] = d[i];
        }
    }
    else {
        n = width;
        gwy_assign(values, d, n);
    }

    return n;
}

static gint
extract_field_column_masked(GwyDataField *dfield,
                            GwyDataField *mask,
                            GwyMaskingType masking,
                            gdouble *values,
                            gint col, gint row, gint height)
{
    gint xres = dfield->xres;
    const gdouble *d = dfield->data + row*xres + col, *m;
    gint i, n;

    if (!mask)
        masking = GWY_MASK_IGNORE;

    if (masking == GWY_MASK_INCLUDE) {
        m = mask->data + row*xres + col;
        for (i = n = 0; i < height; i++) {
            if (m[xres*i] > 0.0)
                values[n++] = d[xres*i];
        }
    }
    else if (masking == GWY_MASK_EXCLUDE) {
        m = mask->data + row*xres + col;
        for (i = n = 0; i < height; i++) {
            if (m[xres*i] <= 0.0)
                values[n++] = d[xres*i];
        }
    }
    else {
        n = height;
        for (i = 0; i < height; i++)
            values[i] = d[xres*i];
    }

    return n;
}

static void
calc_field_row_linestat_masked(GwyDataField *dfield,
                               GwyDataField *mask,
                               GwyMaskingType masking,
                               GwyDataLine *dline,
                               GwyDataLine *weights,
                               LineStatFunc func,
                               gdouble filler_value,
                               gint col, gint row,
                               gint width, gint height)
{
    GwyDataLine *buf;
    gint i, n;
    gdouble *ldata, *wdata, *bufdata;
    gdouble dx = gwy_data_field_get_xmeasure(dfield);

    gwy_data_line_resample(dline, height, GWY_INTERPOLATION_NONE);
    gwy_data_line_set_real(dline, gwy_data_field_itor(dfield, height));
    gwy_data_line_set_offset(dline, gwy_data_field_itor(dfield, row));
    ldata = dline->data;

    if (weights) {
        gwy_data_line_resample(weights, height, GWY_INTERPOLATION_NONE);
        gwy_data_line_set_real(weights, gwy_data_field_itor(dfield, height));
        gwy_data_line_set_offset(weights, gwy_data_field_itor(dfield, row));
        gwy_data_line_clear(weights);
        wdata = weights->data;
    }
    else
        wdata = NULL;

    buf = gwy_data_line_new(width, width*dx, FALSE);
    bufdata = buf->data;

    for (i = 0; i < height; i++) {
        n = extract_field_row_masked(dfield, mask, masking, bufdata,
                                     col, row + i, width);
        if (n) {
            /* Temporarily shorten the dataline to avoid reallocations. */
            buf->res = n;
            buf->real = n*dx;
            ldata[i] = func(buf);
            buf->res = width;
            buf->real = width*dx;
            if (wdata)
                wdata[i] = n;
        }
        else
            ldata[i] = filler_value;
    }

    g_object_unref(buf);
}

static void
calc_field_column_linestat_masked(GwyDataField *dfield,
                                  GwyDataField *mask,
                                  GwyMaskingType masking,
                                  GwyDataLine *dline,
                                  GwyDataLine *weights,
                                  LineStatFunc func,
                                  gdouble filler_value,
                                  gint col, gint row,
                                  gint width, gint height)
{
    GwyDataLine *buf;
    gint i, n;
    gdouble *ldata, *wdata, *bufdata;
    gdouble dy = gwy_data_field_get_ymeasure(dfield);

    gwy_data_line_resample(dline, width, GWY_INTERPOLATION_NONE);
    gwy_data_line_set_real(dline, gwy_data_field_jtor(dfield, width));
    gwy_data_line_set_offset(dline, gwy_data_field_jtor(dfield, col));
    ldata = dline->data;

    if (weights) {
        gwy_data_line_resample(weights, width, GWY_INTERPOLATION_NONE);
        gwy_data_line_set_real(weights, gwy_data_field_jtor(dfield, width));
        gwy_data_line_set_offset(weights, gwy_data_field_jtor(dfield, col));
        gwy_data_line_clear(weights);
        wdata = weights->data;
    }
    else
        wdata = NULL;

    buf = gwy_data_line_new(height, height*dy, FALSE);
    bufdata = buf->data;

    for (i = 0; i < width; i++) {
        n = extract_field_column_masked(dfield, mask, masking, bufdata,
                                        col + i, row, height);
        if (n) {
            /* Temporarily shorten the dataline to avoid reallocations. */
            buf->res = n;
            buf->real = n*dy;
            ldata[i] = func(buf);
            buf->res = height;
            buf->real = height*dy;
            if (wdata)
                wdata[i] = n;
        }
        else
            ldata[i] = filler_value;
    }

    g_object_unref(buf);
}

static gdouble
gwy_data_line_get_slope(GwyDataLine *dline)
{
    gdouble v;

    gwy_data_line_get_line_coeffs(dline, NULL, &v);
    return v*dline->res/dline->real;
}

static gdouble
gwy_data_line_get_range(GwyDataLine *dline)
{
    gdouble min, max;

    gwy_data_line_get_min_max(dline, &min, &max);
    return max - min;
}

static gdouble
gwy_data_line_get_median_destructive(GwyDataLine *dline)
{
    return gwy_math_median(dline->res, dline->data);
}

static gdouble
gwy_data_line_get_Rt_destructive(GwyDataLine *dline)
{
    gwy_data_line_add(dline, -gwy_data_line_get_avg(dline));
    return gwy_data_line_get_xtm(dline, 1, 1);
}

static gdouble
gwy_data_line_get_Rz_destructive(GwyDataLine *dline)
{
    gwy_data_line_add(dline, -gwy_data_line_get_avg(dline));
    return gwy_data_line_get_xtm(dline, 5, 1);
}

/**
 * gwy_data_field_get_line_stats_mask:
 * @data_field: A data field.
 * @mask: Mask of values to take values into account, or %NULL for full
 *        @data_field.
 * @masking: Masking mode to use.  See the introduction for description of
 *           masking modes.
 * @target_line: A data line to store the distribution to.  It will be
 *               resampled to the number of rows (columns).
 * @weights: A data line to store number of data points contributing to each
 *           value in @target_line, or %NULL.  It is useful when masking is
 *           used to possibly exclude values calculated from too few data
 *           points.
 * @col: Upper-left column coordinate.
 * @row: Upper-left row coordinate.
 * @width: Area width (number of columns).
 * @height: Area height (number of rows).
 * @quantity: The line quantity to calulate for each row (column).
 * @orientation: Line orientation.  For %GWY_ORIENTATION_HORIZONTAL each
 *               @target_line point corresponds to a row of the area,
 *               for %GWY_ORIENTATION_VERTICAL each @target_line point
 *               corresponds to a column of the area.
 *
 * Calculates a line quantity for each row or column in a data field area.
 *
 * Since: 2.46
 **/
void
gwy_data_field_get_line_stats_mask(GwyDataField *data_field,
                                   GwyDataField *mask,
                                   GwyMaskingType masking,
                                   GwyDataLine *target_line,
                                   GwyDataLine *weights,
                                   gint col, gint row,
                                   gint width, gint height,
                                   GwyLineStatQuantity quantity,
                                   GwyOrientation orientation)
{
    static const LineStatFunc funcs[] = {
        gwy_data_line_get_avg,
        gwy_data_line_get_median_destructive,
        gwy_data_line_get_min,
        gwy_data_line_get_max,
        gwy_data_line_get_rms,
        gwy_data_line_get_length,
        gwy_data_line_get_slope,
        gwy_data_line_get_tan_beta0,
        gwy_data_line_get_ra,
        gwy_data_line_get_Rz_destructive,
        gwy_data_line_get_Rt_destructive,
        gwy_data_line_get_skew,
        gwy_data_line_get_kurtosis,
        gwy_data_line_get_range,
        gwy_data_line_get_variation,
    };

    LineStatFunc func;
    GwySIUnit *zunit, *xyunit, *lunit;
    gint xres, yres;

    g_return_if_fail(quantity < G_N_ELEMENTS(funcs));
    g_return_if_fail(GWY_IS_DATA_FIELD(data_field));
    g_return_if_fail(GWY_IS_DATA_LINE(target_line));
    g_return_if_fail(!mask || GWY_IS_DATA_FIELD(mask));
    g_return_if_fail(col >= 0 && row >= 0
                     && width > 0 && height > 0
                     && col + width <= data_field->xres
                     && row + height <= data_field->yres);

    func = funcs[quantity];
    xres = data_field->xres;
    yres = data_field->yres;

    if (mask) {
        g_return_if_fail(mask->xres == xres);
        g_return_if_fail(mask->yres == yres);
    }

    if (orientation == GWY_ORIENTATION_VERTICAL) {
        calc_field_column_linestat_masked(data_field, mask, masking,
                                          target_line, weights, func, 0.0,
                                          col, row, width, height);
    }
    else {
        calc_field_row_linestat_masked(data_field, mask, masking,
                                       target_line, weights, func, 0.0,
                                       col, row, width, height);
    }

    xyunit = gwy_data_field_get_si_unit_xy(data_field);
    zunit = gwy_data_field_get_si_unit_z(data_field);

    lunit = gwy_data_line_get_si_unit_x(target_line);
    gwy_serializable_clone(G_OBJECT(xyunit), G_OBJECT(lunit));

    lunit = gwy_data_line_get_si_unit_y(target_line);
    switch (quantity) {
        case GWY_LINE_STAT_LENGTH:
        if (!gwy_si_unit_equal(xyunit, zunit))
            g_warning("Length makes no sense when lateral and value units "
                      "differ");
        case GWY_LINE_STAT_MEAN:
        case GWY_LINE_STAT_MEDIAN:
        case GWY_LINE_STAT_MINIMUM:
        case GWY_LINE_STAT_MAXIMUM:
        case GWY_LINE_STAT_RMS:
        case GWY_LINE_STAT_RA:
        case GWY_LINE_STAT_RT:
        case GWY_LINE_STAT_RZ:
        case GWY_LINE_STAT_RANGE:
        case GWY_LINE_STAT_VARIATION:
        gwy_serializable_clone(G_OBJECT(zunit), G_OBJECT(lunit));
        break;

        case GWY_LINE_STAT_SLOPE:
        case GWY_LINE_STAT_TAN_BETA0:
        case GWY_LINE_STAT_SKEW:
        case GWY_LINE_STAT_KURTOSIS:
        gwy_si_unit_divide(zunit, xyunit, lunit);
        break;

        default:
        g_assert_not_reached();
        break;
    }

    if (weights) {
        lunit = gwy_data_line_get_si_unit_x(weights);
        gwy_serializable_clone(G_OBJECT(xyunit), G_OBJECT(lunit));
        lunit = gwy_data_line_get_si_unit_y(weights);
        gwy_si_unit_set_from_string(lunit, NULL);
    }
}

/**
 * gwy_data_field_area_get_line_stats:
 * @data_field: A data field.
 * @mask: Mask of values to take values into account, or %NULL for full
 *        @data_field.
 * @target_line: A data line to store the distribution to.  It will be
 *               resampled to the number of rows (columns).
 * @col: Upper-left column coordinate.
 * @row: Upper-left row coordinate.
 * @width: Area width (number of columns).
 * @height: Area height (number of rows).
 * @quantity: The line quantity to calulate for each row (column).
 * @orientation: Line orientation.  For %GWY_ORIENTATION_HORIZONTAL each
 *               @target_line point corresponds to a row of the area,
 *               for %GWY_ORIENTATION_VERTICAL each @target_line point
 *               corresponds to a column of the area.
 *
 * Calculates a line quantity for each row or column in a data field area.
 *
 * Use gwy_data_field_get_line_stats_mask() for full masking type options.
 *
 * Since: 2.2
 **/
void
gwy_data_field_area_get_line_stats(GwyDataField *data_field,
                                   GwyDataField *mask,
                                   GwyDataLine *target_line,
                                   gint col, gint row,
                                   gint width, gint height,
                                   GwyLineStatQuantity quantity,
                                   GwyOrientation orientation)
{
    gwy_data_field_get_line_stats_mask(data_field, mask, GWY_MASK_INCLUDE,
                                       target_line, NULL,
                                       col, row, width, height,
                                       quantity, orientation);
}

/**
 * gwy_data_field_get_line_stats:
 * @data_field: A data field.
 * @target_line: A data line to store the distribution to.  It will be
 *               resampled to @data_field height (width).
 * @quantity: The line quantity to calulate for each row (column).
 * @orientation: Line orientation.  See gwy_data_field_area_get_line_stats().
 *
 * Calculates a line quantity for each row or column of a data field.
 *
 * Since: 2.2
 **/
void
gwy_data_field_get_line_stats(GwyDataField *data_field,
                              GwyDataLine *target_line,
                              GwyLineStatQuantity quantity,
                              GwyOrientation orientation)
{
    g_return_if_fail(GWY_IS_DATA_FIELD(data_field));
    gwy_data_field_area_get_line_stats(data_field, NULL, target_line,
                                       0, 0,
                                       data_field->xres, data_field->yres,
                                       quantity, orientation);
}

/**
 * gwy_data_field_count_maxima:
 * @data_field: A data field.
 *
 * Counts the number of regional maxima in a data field.
 *
 * See gwy_data_field_mark_extrema() for the definition of a regional maximum.
 *
 * Returns: The number of regional maxima.
 *
 * Since: 2.38
 **/
guint
gwy_data_field_count_maxima(GwyDataField *data_field)
{
    GwyDataField *mask;
    gint *grains;
    guint ngrains;

    g_return_val_if_fail(GWY_IS_DATA_FIELD(data_field), 0);
    mask = gwy_data_field_new_alike(data_field, FALSE);
    gwy_data_field_mark_extrema(data_field, mask, TRUE);
    grains = g_new0(gint, data_field->xres*data_field->yres);
    ngrains = gwy_data_field_number_grains(mask, grains);
    g_free(grains);
    g_object_unref(mask);
    return ngrains;
}

/**
 * gwy_data_field_count_minima:
 * @data_field: A data field
 *
 * Counts the number of regional minima in a data field.
 *
 * See gwy_data_field_mark_extrema() for the definition of a regional minimum.
 *
 * Returns: The number of regional minima.
 *
 * Since: 2.38
 **/
guint
gwy_data_field_count_minima(GwyDataField *data_field)
{
    GwyDataField *mask;
    gint *grains;
    guint ngrains;

    g_return_val_if_fail(GWY_IS_DATA_FIELD(data_field), 0);
    mask = gwy_data_field_new_alike(data_field, FALSE);
    gwy_data_field_mark_extrema(data_field, mask, FALSE);
    grains = g_new0(gint, data_field->xres*data_field->yres);
    ngrains = gwy_data_field_number_grains(mask, grains);
    g_free(grains);
    g_object_unref(mask);
    return ngrains;
}

/************************** Documentation ****************************/

/**
 * SECTION:stats
 * @title: stats
 * @short_description: Two-dimensional statistical functions
 *
 * Many statistical functions permit to pass masks that determine which values
 * in the data field to take into account or ignore when calculating the
 * statistical characteristics.  Masking mode %GWY_MASK_INCLUDE means that
 * maks values equal to 0.0 and below cause corresponding data field samples
 * to be ignored, values equal to 1.0 and above cause inclusion of
 * corresponding data field samples.  The behaviour for values inside interval
 * (0.0, 1.0) is undefined.  In mode @GWY_MASK_EXCLUDE, the meaning of mask is
 * inverted, as if all mask values x were replaced with 1-x.  The
 * mask field is ignored in mode @GWY_MASK_IGNORE, i.e. the same behaviour
 * occurs as with %NULL mask argument.
 **/

/* vim: set cin et ts=4 sw=4 cino=>1s,e0,n0,f0,{0,}0,^0,\:1s,=0,g1s,h0,t0,+1s,c3,(0,u0 : */
