/*
 *  $Id$
 *  Copyright (C) 2016-2018 David Necas (Yeti), Petr Klapetek.
 *  E-mail: yeti@gwyddion.net, klapetek@gwyddion.net.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA 02110-1301, USA.
 */

/* TODO: Set correct units as appropriate (see individual TODOs). */

#include <string.h>
#include <libgwyddion/gwymacros.h>
#include <libgwyddion/gwymath.h>
#include <libprocess/arithmetic.h>
#include <libprocess/stats.h>
#include <libprocess/filters.h>
#include <libprocess/inttrans.h>
#include <libprocess/mfm.h>

#define MU_0 1.256637061435917295e-6
#define EPSILON_0 8.854187817620389850e-12

static void
mfm_perpendicular_create_wall_mask(GwyDataField *wm, gdouble a, gdouble kn)
{
    gint xres, yres, i, j;
    gdouble xreal, yreal, x, y, s, sum, *data;
    gdouble delta = G_PI*sqrt(a/kn);

    g_return_if_fail(GWY_IS_DATA_FIELD(wm));
    xres = wm->xres;
    yres = wm->yres;
    xreal = wm->xreal;
    yreal = wm->yreal;
    data = wm->data;

    sum = 0.0;
    for (i = 0; i < yres; i++) {
        for (j = 0; j < xres; j++) {
            x = (i - xres/2)*xreal/xres;
            y = (j - yres/2)*yreal/yres;
            s = sqrt(x*x + y*y);
            sum += data[i*xres + j] = 1.0/(cosh(G_PI*s/delta));
        }
    }
    gwy_data_field_invalidate(wm);
    gwy_data_field_multiply(wm, 1.0/sum);
}

static void
mfm_perpendicular_create_ftf(GwyDataField *ftf, gdouble mtip,
                             gdouble bx, gdouble by, gdouble length,
                             GwyMFMProbeType type)
{
    gint xres, yres, i, j;
    gdouble xreal, yreal, c, kx, ky, k;
    gdouble *data;

    g_return_if_fail(GWY_IS_DATA_FIELD(ftf));
    xres = ftf->xres;
    yres = ftf->yres;
    xreal = ftf->xreal;
    yreal = ftf->yreal;

    c = -MU_0*mtip*bx*by;
    if (type == GWY_MFM_PROBE_CHARGE)
        gwy_data_field_fill(ftf, c);
    else {
        data = ftf->data;
        for (i = 0; i < yres; i++) {
            for (j = 0; j < xres; j++) {
                kx = (i - xres/2)/xreal;
                ky = (j - yres/2)/yreal;
                k = sqrt(kx*kx + ky*ky);
                data[i*xres + j] = c * gwy_sinc(kx*bx/2) * gwy_sinc(ky*by/2)
                                   * (1.0 - exp(-k*length));
            }
        }
        gwy_data_field_invalidate(ftf);
    }
}

static void
mfm_create_ztf(GwyDataField *ztf, gdouble zdiff)
{
    gint xres, yres, i, j;
    gdouble xreal, yreal, kx, ky, k;
    gdouble *data;

    g_return_if_fail(GWY_IS_DATA_FIELD(ztf));
    data = ztf->data;
    xres = ztf->xres;
    yres = ztf->yres;
    xreal = ztf->xreal;
    yreal = ztf->yreal;

    for (i = 0; i < yres; i++) {
        for (j = 0; j < xres; j++) {
            kx = (i - xres/2)/xreal;
            ky = (j - yres/2)/yreal;
            k = sqrt(kx*kx + ky*ky);
            data[i*xres + j] = exp(k*zdiff);
        }
    }
    gwy_data_field_invalidate(ztf);
}

static void
mfm_perpendicular_create_field_mask(GwyDataField *fieldmask,
                                    gdouble height,
                                    gdouble thickness)
{
    gint xres, yres, i, j;
    gdouble xreal, yreal, kx, ky, k;
    gdouble *data;

    g_return_if_fail(GWY_IS_DATA_FIELD(fieldmask));
    data = fieldmask->data;
    xres = fieldmask->xres;
    yres = fieldmask->yres;
    xreal = fieldmask->xreal;
    yreal = fieldmask->yreal;

    for (i = 0; i < yres; i++) {
        for (j = 0; j < xres; j++) {
            kx = (i - xres/2)/xreal;
            ky = (j - yres/2)/yreal;
            k = sqrt(kx*kx + ky*ky);
            //removed 0.5
            data[i*xres + j] = exp(-k*height)*(1.0 - exp(-k*thickness));
        }
    }
    gwy_data_field_invalidate(fieldmask);
}

/**
 * gwy_data_field_mfm_perpendicular_stray_field:
 * @mfield: 
 * @out: Target data field to put the result to.  It will be resized to match
 *       @mfield.
 * @height:
 * @thickness:
 * @sigma:
 * @walls:
 * @wall_a:
 * @wall_kn:
 *
 * Calculates stray field for perpendicular media.
 *
 * Since: 2.51
 **/
void
gwy_data_field_mfm_perpendicular_stray_field(GwyDataField *mfield,
                                             GwyDataField *out,
                                             gdouble height,
                                             gdouble thickness,
                                             gdouble sigma,
                                             gboolean walls,
                                             gdouble wall_a,
                                             gdouble wall_kn)
{
    GwyDataField *rea, *ima, *reb, *imb, *fieldmask, *wallmask = NULL;

    g_return_if_fail(GWY_IS_DATA_FIELD(mfield));
    g_return_if_fail(GWY_IS_DATA_FIELD(out));

    rea = gwy_data_field_new_alike(mfield, TRUE);
    reb = gwy_data_field_new_alike(mfield, TRUE);
    ima = gwy_data_field_new_alike(mfield, TRUE);
    imb = gwy_data_field_new_alike(mfield, TRUE);
    fieldmask = gwy_data_field_new_alike(mfield, TRUE);

    gwy_data_field_copy(mfield, rea, FALSE);
    gwy_data_field_multiply(rea, 2*sigma);
    gwy_data_field_add(rea, -sigma);

    if (walls) {
        wallmask = gwy_data_field_new_alike(mfield, TRUE);
        mfm_perpendicular_create_wall_mask(wallmask, wall_a, wall_kn);
        gwy_data_field_area_ext_convolve(rea,
                                         0, 0,
                                         gwy_data_field_get_xres(rea),
                                         gwy_data_field_get_yres(rea),
                                         rea, wallmask,
                                         GWY_EXTERIOR_MIRROR_EXTEND, 0.0,
                                         FALSE);
    }

    gwy_data_field_2dfft_raw(rea, NULL, reb, imb,
                             GWY_TRANSFORM_DIRECTION_FORWARD);

    gwy_data_field_2dfft_humanize(reb);
    gwy_data_field_2dfft_humanize(imb);

    mfm_perpendicular_create_field_mask(fieldmask, height, thickness);

    gwy_data_field_multiply_fields(reb, reb, fieldmask);
    gwy_data_field_multiply_fields(imb, imb, fieldmask);

    gwy_data_field_2dfft_dehumanize(reb);
    gwy_data_field_2dfft_dehumanize(imb);
    gwy_data_field_2dfft_raw(reb, imb, rea, ima,
                             GWY_TRANSFORM_DIRECTION_BACKWARD);

    gwy_data_field_resample(out, mfield->xres, mfield->yres,
                            GWY_INTERPOLATION_NONE);
    gwy_data_field_copy(rea, out, TRUE);
    /* TODO: Set correct value units on out (we just copy lateral and assume
     * they are OK). */

    g_object_unref(rea);
    g_object_unref(reb);
    g_object_unref(ima);
    g_object_unref(imb);
    g_object_unref(fieldmask);
    GWY_OBJECT_UNREF(wallmask);
}

/**
 * gwy_data_field_mfm_perpendicular_medium_force:
 * @hz: Data field contaning the Z-component of the magnetic H field.
 * @fz: Target data field to put the result to.  It will be resized to match
 *       @hz.
 * @type:
 * @mtip:
 * @bx:
 * @by:
 * @length:
 *
 * Calculates ...
 *
 * Since: 2.51
 **/
void
gwy_data_field_mfm_perpendicular_medium_force(GwyDataField *hz,
                                              GwyDataField *fz,
                                              GwyMFMProbeType type,
                                              gdouble mtip,
                                              gdouble bx,
                                              gdouble by,
                                              gdouble length)
{
    GwyDataField *rea, *ima, *reb, *imb, *ftf;

    g_return_if_fail(GWY_IS_DATA_FIELD(hz));
    g_return_if_fail(GWY_IS_DATA_FIELD(fz));

    rea = gwy_data_field_new_alike(hz, TRUE);
    reb = gwy_data_field_new_alike(hz, TRUE);
    ima = gwy_data_field_new_alike(hz, TRUE);
    imb = gwy_data_field_new_alike(hz, TRUE);
    ftf = gwy_data_field_new_alike(hz, TRUE);

    gwy_data_field_copy(hz, rea, FALSE);

    gwy_data_field_2dfft_raw(rea, NULL, reb, imb,
                             GWY_TRANSFORM_DIRECTION_FORWARD);

    gwy_data_field_2dfft_humanize(reb);
    gwy_data_field_2dfft_humanize(imb);

    mfm_perpendicular_create_ftf(ftf, mtip, bx, by, length, type);

    gwy_data_field_multiply_fields(reb, reb, ftf);
    gwy_data_field_multiply_fields(imb, imb, ftf);

    gwy_data_field_2dfft_dehumanize(reb);
    gwy_data_field_2dfft_dehumanize(imb);
    gwy_data_field_2dfft_raw(reb, imb, rea, ima,
                             GWY_TRANSFORM_DIRECTION_BACKWARD);

    gwy_data_field_resample(fz, hz->xres, hz->yres, GWY_INTERPOLATION_NONE);
    gwy_data_field_copy(rea, fz, TRUE);
    /* TODO: Set correct value units on fz (we just copy lateral and assume
     * they are OK). */

    g_object_unref(rea);
    g_object_unref(reb);
    g_object_unref(ima);
    g_object_unref(imb);
    g_object_unref(ftf);

    gwy_si_unit_set_from_string(gwy_data_field_get_si_unit_z(hfield), "N");
}

/**
 * gwy_data_field_mfm_shift_z:
 * @dfield: Data field containing magnetic field component.
 * @out: Target data field to put the result to.
 * @zdiff: TODO which direction is up?
 *
 * Shifts magnetic field to a different height above the surface.
 *
 * Since: 2.51
 **/
void
gwy_data_field_mfm_shift_z(GwyDataField *dfield,
                           GwyDataField *out,
                           gdouble zdiff)
{
    GwyDataField *rea, *ima, *reb, *imb, *ztf;

    g_return_if_fail(GWY_IS_DATA_FIELD(dfield));
    g_return_if_fail(GWY_IS_DATA_FIELD(out));

    rea = gwy_data_field_new_alike(dfield, TRUE);
    reb = gwy_data_field_new_alike(dfield, TRUE);
    ima = gwy_data_field_new_alike(dfield, TRUE);
    imb = gwy_data_field_new_alike(dfield, TRUE);
    ztf = gwy_data_field_new_alike(dfield, TRUE);

    gwy_data_field_copy(dfield, rea, FALSE);

    gwy_data_field_2dfft_raw(rea, NULL, reb, imb,
                             GWY_TRANSFORM_DIRECTION_FORWARD);

    gwy_data_field_2dfft_humanize(reb);
    gwy_data_field_2dfft_humanize(imb);

    mfm_create_ztf(ztf, zdiff);

    gwy_data_field_multiply_fields(reb, reb, ztf);
    gwy_data_field_multiply_fields(imb, imb, ztf);

    gwy_data_field_2dfft_dehumanize(reb);
    gwy_data_field_2dfft_dehumanize(imb);
    gwy_data_field_2dfft_raw(reb, imb, rea, ima,
                             GWY_TRANSFORM_DIRECTION_BACKWARD);

    gwy_data_field_resample(out, dfield->xres, dfield->yres,
                            GWY_INTERPOLATION_NONE);
    gwy_data_field_copy(rea, out, TRUE);

    g_object_unref(rea);
    g_object_unref(reb);
    g_object_unref(ima);
    g_object_unref(imb);
    g_object_unref(ztf);
}

/**
 * gwy_data_field_mfm_parallel_medium:
 * @hfield:
 * @height:
 * @size_a:
 * @size_b:
 * @size_c:
 * @magnetisation:
 * @thickness:
 * @component:
 *
 * Calculates ...
 *
 * TODO: Document that the field is added to existing values in @hfield.
 *
 * Since: 2.51
 **/
void
gwy_data_field_mfm_parallel_medium(GwyDataField *hfield,
                                   gdouble height,
                                   gdouble size_a,
                                   gdouble size_b,
                                   gdouble size_c,
                                   gdouble magnetisation,
                                   gdouble thickness,
                                   GwyMFMComponentType component)
{
    gint xres, yres, i, j, k, n, listsize;
    gdouble d, xreal, x, pos, val, u, v, m;
    gdouble *data, *row;
    gint *xlist, *dirlist;

    g_return_if_fail(GWY_IS_DATA_FIELD(hfield));

    xres = hfield->xres;
    yres = hfield->xres;
    xreal = hfield->xreal;

    listsize = 10*xres;
    xlist = g_new(gint, listsize);
    dirlist = g_new(gint, listsize);

    m = MU_0*magnetisation/G_PI;
    d = 20.0*(size_a + size_b + thickness + height);
    pos = -d;

    n = 0;
    xlist[n] = pos*xres/xreal;
    dirlist[n] = 1;
    n++;

    do {
       pos += size_a + size_c/2;
       xlist[n] = pos*xres/xreal;
       dirlist[n] = -1;
       n++;

       pos += size_b + size_c/2;
       xlist[n] = pos*xres/xreal;
       dirlist[n] = 1;
       n++;
    } while (pos < xreal + d && n < listsize);

    row = g_new0(gdouble, xres);
    for (k = 0; k < n; k++) {
        for (j = 0; j < xres; j++) {
            x = ((gdouble)j - xlist[k])*xreal/xres;
            if (component == GWY_MFM_COMPONENT_HX) {
                u = x*x + size_c*size_c + size_c*(thickness+height);
                v = x*x + size_c*size_c + size_c*height;
                val = -m*dirlist[k]*(atan(x*(thickness+height)/u)
                                     - atan(x*height/v));
            }
            else if (component == GWY_MFM_COMPONENT_HY)
                val = 0;
            else if (component == GWY_MFM_COMPONENT_HZ) {
                u = size_c + height + thickness;
                v = x*x + (size_c + height)*(size_c + height);
                val = m*dirlist[k]/2*log((x*x + u*u)/v);
            }
            else {
                g_return_if_reached();
            }
            row[j] += val;
        }
    }
    g_free(xlist);
    g_free(dirlist);

    data = hfield->data;
    for (i = 0; i < yres; i++) {
        for (j = 0; j < xres; j++)
            data[i*xres + j] += row[j];
    }
    g_free(row);
    gwy_data_field_invalidate(hfield);

    gwy_si_unit_set_from_string(gwy_data_field_get_si_unit_z(hfield), "A/m");
}

/**
 * gwy_data_field_mfm_current_line:
 * @hfield:
 * @height:
 * @width:
 * @position:
 * @current:
 * @component:
 *
 * Calculates ...
 *
 * TODO: Document that the field is added to existing values in @hfield.
 *
 * Since: 2.51
 **/
void
gwy_data_field_mfm_current_line(GwyDataField *hfield,
                                gdouble height,
                                gdouble width,
                                gdouble position,
                                gdouble current,
                                GwyMFMComponentType component)
{
    gint xres, yres, i, j;
    gdouble x, xreal, val, m;
    gdouble *data, *row;

    g_return_if_fail(GWY_IS_DATA_FIELD(hfield));
    xres = hfield->xres;
    yres = hfield->xres;
    xreal = hfield->xreal;

    m = current/(2*G_PI*width);

    row = g_new0(gdouble, xres);
    for (j = 0; j < xres; j++) {
        x = j*xreal/xres - position;
        if (component == GWY_MFM_COMPONENT_HX)
            val = -m*(atan((x - width/2)/height) - atan((x + width/2)/height));
        else if (component == GWY_MFM_COMPONENT_HY)
            val = 0;
        else if (component == GWY_MFM_COMPONENT_HZ) {
            val = m*log(fabs(cos(atan((x + width/2)/height))
                             /cos(atan((x - width/2)/height))));
        }
        else {
            g_return_if_reached();
        }
        row[j] += val;
    }

    data = hfield->data;
    for (i = 0; i < yres; i++) {
        for (j = 0; j < xres; j++)
            data[i*xres + j] += row[j];
    }
    g_free(row);
    gwy_data_field_invalidate(hfield);

    gwy_si_unit_set_from_string(gwy_data_field_get_si_unit_z(hfield), "A/m");
}

/************************** Documentation ****************************/

/**
 * GwyMFMProbeType:
 * @GWY_MFM_PROBE_CHARGE: Magnetic point charge probe.
 * @GWY_MFM_PROBE_BAR: Finite rectangular bar.
 *
 * Type of probe for calculation of force in magnetic field microscopy.
 *
 * Since: 2.51
 **/

/**
 * GwyMFMComponentType:
 * @GWY_MFM_COMPONENT_HX: X-component of magnetic field H.
 * @GWY_MFM_COMPONENT_HY: Y-component of magnetic field H.
 * @GWY_MFM_COMPONENT_HZ: Z-component of magnetic field H.
 *
 * Type of field component calculated in magnetic field microscopy.
 *
 * Since: 2.51
 **/

/**
 * SECTION:mfm
 * @title: MFM
 * @short_description: Magnetic force microscopy
 **/

/* vim: set cin et ts=4 sw=4 cino=>1s,e0,n0,f0,{0,}0,^0,\:1s,=0,g1s,h0,t0,+1s,c3,(0,u0 : */
