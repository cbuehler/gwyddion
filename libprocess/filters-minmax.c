/*
 *  $Id$
 *  Copyright (C) 2003-2016 David Necas (Yeti), Petr Klapetek.
 *  E-mail: yeti@gwyddion.net, klapetek@gwyddion.net.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA 02110-1301, USA.
 */

#include <string.h>
#include <stdlib.h>
#include <libgwyddion/gwymacros.h>
#include <libgwyddion/gwymath.h>
#include <libprocess/filters.h>
#include <libprocess/elliptic.h>
#include <libprocess/stats.h>
#include <libprocess/linestats.h>
#include <libprocess/grains.h>
#include "gwyprocessinternal.h"

/* Data for one row.  To be used in conjuction with MinMaxPrecomputedReq. */
typedef struct {
    gdouble *storage;
    gdouble **each;
    gdouble **even;
} MinMaxPrecomputedRow;

typedef struct {
    guint sublen1;   /* Even length for the even-odd scheme. */
    guint sublen2;
    gboolean needed;
    gboolean even_even : 1;
    gboolean even_odd : 1;
} MinMaxPrecomputedLen;

/* Resolved set of required block lengths and the rules how to compute them. */
typedef struct {
    /* NB: The array sizes are maxlen_even+1 and maxlen_each+1 because maxlen
     * is really the maximum length, inclusive. */
    MinMaxPrecomputedLen *each;
    MinMaxPrecomputedLen *even;
    guint maxlen_each;
    guint maxlen_even;
    guint nbuffers;   /* The actual number of row buffers (for storage size) */
} MinMaxPrecomputedReq;

typedef struct {
    guint row;
    guint col;
    guint len;
} MaskSegment;

typedef struct {
    MaskSegment *segments;
    guint nsegments;
} MaskRLE;

typedef struct {
    MaskRLE *mrle;
    MinMaxPrecomputedReq *req;
    MinMaxPrecomputedRow **prows;
    gdouble *extrowbuf;
    guint rowbuflen;
    guint kxres;
    guint kyres;
} MinMaxPrecomputed;

typedef void (*MinMaxPrecomputedRowFill)(const MinMaxPrecomputedReq *req,
                                         MinMaxPrecomputedRow *prow,
                                         const gdouble *x,
                                         guint rowlen);

static void find_required_lengths_recursive(MinMaxPrecomputedReq *req,
                                            guint blocklen,
                                            gboolean is_even);

/**
 * gwy_data_field_area_filter_minimum:
 * @data_field: A data field to apply minimum filter to.
 * @size: Neighbourhood size for minimum search.
 * @col: Upper-left column coordinate.
 * @row: Upper-left row coordinate.
 * @width: Area width (number of columns).
 * @height: Area height (number of rows).
 *
 * Filters a rectangular part of a data field with minimum filter.
 *
 * This operation is often called erosion filter.
 **/
void
gwy_data_field_area_filter_minimum(GwyDataField *data_field,
                                   gint size,
                                   gint col,
                                   gint row,
                                   gint width,
                                   gint height)
{
    GwyDataField *buffer, *buffer2;
    gint d, i, j, ip, ii, im, jp, jm;
    gint ep, em;  /* positive and negative excess */
    gdouble *buf, *buf2;
    gdouble v;

    g_return_if_fail(GWY_IS_DATA_FIELD(data_field));
    g_return_if_fail(col >= 0 && row >= 0
                     && width > 0 && height > 0
                     && col + width <= data_field->xres
                     && row + height <= data_field->yres);
    g_return_if_fail(size > 0);
    if (size == 1)
        return;

    /* FIXME: does this silly case need an alternative implementation? */
    if (size/2 >= MIN(width, height)) {
        g_warning("Too large kernel size for too small area.");
        return;
    }

    buffer = gwy_data_field_new(width, height, 1.0, 1.0, FALSE);
    buffer2 = gwy_data_field_new(width, height, 1.0, 1.0, FALSE);
    buf = buffer->data;
    buf2 = buffer2->data;

    d = 1;
    gwy_data_field_area_copy(data_field, buffer, col, row, width, height, 0, 0);
    while (3*d < size) {
        for (i = 0; i < height; i++) {
            ii = i*width;
            im = MAX(i - d, 0)*width;
            ip = MIN(i + d, height-1)*width;
            for (j = 0; j < width; j++) {
                jm = MAX(j - d, 0);
                jp = MIN(j + d, width-1);

                v = MIN(buf[im + jm], buf[im + jp]);
                if (v > buf[im + j])
                    v = buf[im + j];
                if (v > buf[ii + jm])
                    v = buf[ii + jm];
                if (v > buf[ii + j])
                    v = buf[ii + j];
                if (v > buf[ip + j])
                    v = buf[ip + j];
                if (v > buf[ii + jp])
                    v = buf[ii + jp];
                if (v > buf[ip + jm])
                    v = buf[ip + jm];
                if (v > buf[ip + jp])
                    v = buf[ip + jp];

                buf2[ii + j] = v;
            }
        }
        /* XXX: This breaks the relation between buffer and buf */
        GWY_SWAP(gdouble*, buf, buf2);
        d *= 3;
    }


    /* Now we have to overlay the neighbourhoods carefully to get exactly
     * @size-sized squares.  There are two cases:
     * 1. @size <= 2*d, it's enough to take four corner representants
     * 2. @size > 2*d, it's necessary to take all nine representants
     */
    ep = size/2;
    em = (size - 1)/2;

    for (i = 0; i < height; i++) {
        ii = i*width;
        im = (MAX(i - em, 0) + d/2)*width;
        ip = (MIN(i + ep, height-1) - d/2)*width;

        for (j = 0; j < width; j++) {
            jm = MAX(j - em, 0) + d/2;
            jp = MIN(j + ep, width-1) - d/2;

            v = MIN(buf[im + jm], buf[im + jp]);
            if (2*d < size) {
                if (v > buf[im + j])
                    v = buf[im + j];
                if (v > buf[ii + jm])
                    v = buf[ii + jm];
                if (v > buf[ii + j])
                    v = buf[ii + j];
                if (v > buf[ii + jp])
                    v = buf[ii + jp];
                if (v > buf[ip + j])
                    v = buf[ip + j];
            }
            if (v > buf[ip + jm])
                v = buf[ip + jm];
            if (v > buf[ip + jp])
                v = buf[ip + jp];

            buf2[ii + j] = v;
        }
    }
    buffer->data = buf;
    buffer2->data = buf2;

    gwy_data_field_area_copy(buffer2, data_field,
                             0, 0, width, height, col, row);

    g_object_unref(buffer2);
    g_object_unref(buffer);
}

/**
 * gwy_data_field_filter_minimum:
 * @data_field: A data field to apply minimum filter to.
 * @size: Neighbourhood size for minimum search.
 *
 * Filters a data field with minimum filter.
 **/
void
gwy_data_field_filter_minimum(GwyDataField *data_field,
                              gint size)
{
    g_return_if_fail(GWY_IS_DATA_FIELD(data_field));
    gwy_data_field_area_filter_minimum(data_field, size, 0, 0,
                                       data_field->xres, data_field->yres);
}

/**
 * gwy_data_field_area_filter_maximum:
 * @data_field: A data field to apply maximum filter to.
 * @size: Neighbourhood size for maximum search.
 * @col: Upper-left column coordinate.
 * @row: Upper-left row coordinate.
 * @width: Area width (number of columns).
 * @height: Area height (number of rows).
 *
 * Filters a rectangular part of a data field with maximum filter.
 *
 * This operation is often called dilation filter.
 **/
void
gwy_data_field_area_filter_maximum(GwyDataField *data_field,
                                   gint size,
                                   gint col,
                                   gint row,
                                   gint width,
                                   gint height)
{
    GwyDataField *buffer, *buffer2;
    gint d, i, j, ip, ii, im, jp, jm;
    gint ep, em;  /* positive and negative excess */
    gdouble *buf, *buf2;
    gdouble v;

    g_return_if_fail(GWY_IS_DATA_FIELD(data_field));
    g_return_if_fail(col >= 0 && row >= 0
                     && width > 0 && height > 0
                     && col + width <= data_field->xres
                     && row + height <= data_field->yres);
    g_return_if_fail(size > 0);
    if (size == 1)
        return;

    /* FIXME: does this silly case need an alternative implementation? */
    if (size/2 >= MIN(width, height)) {
        g_warning("Too large kernel size for too small area.");
        return;
    }

    buffer = gwy_data_field_new(width, height, 1.0, 1.0, FALSE);
    buffer2 = gwy_data_field_new(width, height, 1.0, 1.0, FALSE);
    buf = buffer->data;
    buf2 = buffer2->data;

    d = 1;
    gwy_data_field_area_copy(data_field, buffer, col, row, width, height, 0, 0);
    while (3*d < size) {
        for (i = 0; i < height; i++) {
            ii = i*width;
            im = MAX(i - d, 0)*width;
            ip = MIN(i + d, height-1)*width;
            for (j = 0; j < width; j++) {
                jm = MAX(j - d, 0);
                jp = MIN(j + d, width-1);

                v = MAX(buf[im + jm], buf[im + jp]);
                if (v < buf[im + j])
                    v = buf[im + j];
                if (v < buf[ii + jm])
                    v = buf[ii + jm];
                if (v < buf[ii + j])
                    v = buf[ii + j];
                if (v < buf[ip + j])
                    v = buf[ip + j];
                if (v < buf[ii + jp])
                    v = buf[ii + jp];
                if (v < buf[ip + jm])
                    v = buf[ip + jm];
                if (v < buf[ip + jp])
                    v = buf[ip + jp];

                buf2[ii + j] = v;
            }
        }
        /* XXX: This breaks the relation between buffer and buf */
        GWY_SWAP(gdouble*, buf, buf2);
        d *= 3;
    }


    /* Now we have to overlay the neighbourhoods carefully to get exactly
     * @size-sized squares.  There are two cases:
     * 1. @size <= 2*d, it's enough to take four corner representants
     * 2. @size > 2*d, it's necessary to take all nine representants
     */
    ep = size/2;
    em = (size - 1)/2;

    for (i = 0; i < height; i++) {
        ii = i*width;
        im = (MAX(i - em, 0) + d/2)*width;
        ip = (MIN(i + ep, height-1) - d/2)*width;

        for (j = 0; j < width; j++) {
            jm = MAX(j - em, 0) + d/2;
            jp = MIN(j + ep, width-1) - d/2;

            v = MAX(buf[im + jm], buf[im + jp]);
            if (2*d < size) {
                if (v < buf[im + j])
                    v = buf[im + j];
                if (v < buf[ii + jm])
                    v = buf[ii + jm];
                if (v < buf[ii + j])
                    v = buf[ii + j];
                if (v < buf[ii + jp])
                    v = buf[ii + jp];
                if (v < buf[ip + j])
                    v = buf[ip + j];
            }
            if (v < buf[ip + jm])
                v = buf[ip + jm];
            if (v < buf[ip + jp])
                v = buf[ip + jp];

            buf2[ii + j] = v;
        }
    }
    buffer->data = buf;
    buffer2->data = buf2;

    gwy_data_field_area_copy(buffer2, data_field,
                             0, 0, width, height, col, row);

    g_object_unref(buffer2);
    g_object_unref(buffer);
}

/**
 * gwy_data_field_filter_maximum:
 * @data_field: A data field to apply maximum filter to.
 * @size: Neighbourhood size for maximum search.
 *
 * Filters a data field with maximum filter.
 **/
void
gwy_data_field_filter_maximum(GwyDataField *data_field,
                              gint size)
{
    g_return_if_fail(GWY_IS_DATA_FIELD(data_field));
    gwy_data_field_area_filter_maximum(data_field, size, 0, 0,
                                       data_field->xres, data_field->yres);
}

static inline gboolean
maybe_set_req(MinMaxPrecomputedLen *precomp)
{
    if (precomp->needed)
        return TRUE;

    precomp->needed = TRUE;
    return FALSE;
}

static inline void
fill_req_subs(MinMaxPrecomputedLen *precomp,
              guint sublen1, guint sublen2,
              gboolean even_odd, gboolean even_even)
{
    precomp->sublen1 = sublen1;
    precomp->sublen2 = sublen2;
    precomp->even_even = even_even;
    precomp->even_odd = even_odd;
    g_assert(!even_odd || !even_even);
    g_assert(!even_odd || sublen1 % 2 == 0);
    g_assert(!even_even || (sublen1 % 2 == 0 && sublen2 % 2 == 0));
}

static void
find_required_lengths_recursive(MinMaxPrecomputedReq *req,
                                guint blocklen, gboolean is_even)
{
    MinMaxPrecomputedLen *precomp;
    guint i, j, any = 0;

    g_assert(blocklen);

    if (is_even) {
        g_assert(blocklen % 2 == 0);

        precomp = req->even + blocklen;
        if (maybe_set_req(precomp))
            return;

        if (blocklen == 2) {
            /* Even(2) = Each(1) + Each(1) */
            fill_req_subs(precomp, 1, 1, FALSE, FALSE);
            find_required_lengths_recursive(req, 1, FALSE);
        }
        else if (blocklen % 4 == 0) {
            /* Even(4m) = Even(2m) + Even(2m) */
            fill_req_subs(precomp, blocklen/2, blocklen/2, FALSE, TRUE);
            find_required_lengths_recursive(req, blocklen/2, TRUE);
        }
        else if (blocklen % 4 == 2) {
            /* Even(4m+2) = Even(2m+2) + Even(2m) */
            fill_req_subs(precomp, blocklen/2 - 1, blocklen/2 + 1, FALSE, TRUE);
            find_required_lengths_recursive(req, blocklen/2 - 1, TRUE);
            find_required_lengths_recursive(req, blocklen/2 + 1, TRUE);
        }
        else {
            g_assert_not_reached();
        }
    }
    else {
        precomp = req->each + blocklen;
        if (maybe_set_req(precomp))
            return;

        if (blocklen == 1) {
            /* Even(1), this is always required.  There is no construction
             * rule, of course.*/
            req->each[1].needed = TRUE;
        }
        else if (blocklen % 2 == 0) {
            /* Try to find a split into two existing lengths. */
            for (i = 1, j = blocklen-1; i < (blocklen + 1)/2; i++, j--) {
                if (req->each[i].needed && req->each[j].needed) {
                    fill_req_subs(precomp, i, j, FALSE, FALSE);
                    return;
                }
            }

            /* Each(2m) = Each(m) + Each(m) */
            fill_req_subs(precomp, blocklen/2, blocklen/2, FALSE, FALSE);
            find_required_lengths_recursive(req, blocklen/2, FALSE);
        }
        else if (blocklen % 2 == 1) {
            /* Try to find a split into two existing lengths. */
            for (i = 1, j = blocklen-1; i < (blocklen + 1)/2; i++, j--) {
                if (req->each[i].needed && req->each[j].needed) {
                    fill_req_subs(precomp, i, j, FALSE, FALSE);
                    return;
                }
                if (req->even[i].needed && req->each[j].needed) {
                    fill_req_subs(precomp, i, j, TRUE, FALSE);
                    return;
                }
                if (req->each[i].needed && req->even[j].needed) {
                    fill_req_subs(precomp, j, i, TRUE, FALSE);
                    return;
                }
                if (req->each[i].needed)
                    any = i;
            }
            /* Or split to one existing and one new. */
            if (any) {
                fill_req_subs(precomp, any, blocklen - any, FALSE, FALSE);
                find_required_lengths_recursive(req, blocklen - any, FALSE);
                return;
            }

            if (blocklen % 4 == 1) {
                /* Each(4m+1) = Even(2m) + Each(2m+1), Each(2m+1) + Even(2m) */
                fill_req_subs(precomp, blocklen/2, blocklen/2 + 1, TRUE, FALSE);
                find_required_lengths_recursive(req, blocklen/2, TRUE);
                find_required_lengths_recursive(req, blocklen/2 + 1, FALSE);
            }
            else if (blocklen % 4 == 3) {
                /* Each(4m+3) = Even(2m+2) + Each(2m+1), Each(2m+1) + Even(2m+2) */
                fill_req_subs(precomp, blocklen/2 + 1, blocklen/2, TRUE, FALSE);
                find_required_lengths_recursive(req, blocklen/2 + 1, TRUE);
                find_required_lengths_recursive(req, blocklen/2, FALSE);
            }
            else {
                g_assert_not_reached();
            }
        }
        else {
            g_assert_not_reached();
        }
    }
}

static MinMaxPrecomputedReq*
find_required_lengths_for_set(const guint *blocklens, guint nlens)
{
    MinMaxPrecomputedReq *req = g_new(MinMaxPrecomputedReq, 1);
    guint *blens = g_new(guint, 2*nlens);
    guint i, n, maxlen;

    /* Find unique lengths and sort them in ascending order to blens[],
     * starting ar position nlens. */
    gwy_assign(blens, blocklens, nlens);
    gwy_guint_sort(nlens, blens);

    blens[nlens] = blens[0];
    for (i = n = 1; i < nlens; i++) {
        if (blens[i] > blens[i-1])
            blens[nlens + n++] = blens[i];
    }

    maxlen = blens[nlens + n-1];
    req->maxlen_each = maxlen;
    req->maxlen_even = maxlen;
    req->each = g_new0(MinMaxPrecomputedLen, maxlen+1);
    req->even = g_new0(MinMaxPrecomputedLen, maxlen+1);
    for (i = 0; i < n; i++)
        find_required_lengths_recursive(req, blens[nlens + i], FALSE);

    g_free(blens);

    for (i = maxlen; i; i--) {
        if (req->even[i].needed)
            break;
    }
    req->maxlen_even = i;

    req->nbuffers = 0;
    for (i = 1; i <= req->maxlen_each; i++) {
        if (req->each[i].needed)
            req->nbuffers++;
    }
    for (i = 2; i <= req->maxlen_even; i++) {
        if (req->even[i].needed)
            req->nbuffers++;
    }

    return req;
}

static void
min_max_precomputed_req_free(MinMaxPrecomputedReq *req)
{
    g_free(req->each);
    g_free(req->even);
    g_free(req);
}

/* Allocate data buffers for all lengths.  Do not allocate the Each(1) buffer,
 * we use a direct pointer to the data row for that. */
static MinMaxPrecomputedRow*
min_max_precomputed_row_alloc(const MinMaxPrecomputedReq *req,
                              guint rowlen)
{
    MinMaxPrecomputedRow *prow = g_new0(MinMaxPrecomputedRow, 1);
    gdouble *p;
    guint i;

    prow->storage = p = g_new(gdouble, rowlen*req->nbuffers);
    prow->each = g_new0(gdouble*, req->maxlen_each + 1);
    if (req->maxlen_even)
        prow->even = g_new0(gdouble*, req->maxlen_even + 1);

    for (i = 1; i <= req->maxlen_each; i++) {
        if (req->each[i].needed) {
            prow->each[i] = p;
            p += rowlen;
        }
    }
    for (i = 2; i <= req->maxlen_even; i++) {
        if (req->even[i].needed) {
            prow->even[i] = p;
            p += rowlen;
        }
    }

    return prow;
}

static void
compose_max_row_data_each(gdouble *target,
                          const gdouble *sub1,
                          guint sublen1,
                          const gdouble *sub2,
                          guint sublen2,
                          guint rowlen)
{
    guint i, n;

    g_return_if_fail(sublen1 + sublen2 <= rowlen);
    g_return_if_fail(target);
    g_return_if_fail(sub1);
    g_return_if_fail(sub2);

    sub2 += sublen1;
    n = rowlen - (sublen1 + sublen2);
    i = 0;
    while (i <= n) {
        target[i] = (*sub1 < *sub2) ? *sub2 : *sub1;
        i++;
        sub1++;
        sub2++;
    }
}

static void
compose_max_row_data_even_odd(gdouble *target,
                              const gdouble *even,
                              guint evenlen,
                              const gdouble *odd,
                              guint oddlen,
                              guint rowlen)
{
    const gdouble *even2, *odd2;
    guint i, n;

    g_return_if_fail(evenlen + oddlen <= rowlen);
    g_return_if_fail(evenlen % 2 == 0);
    g_return_if_fail(target);
    g_return_if_fail(even);
    g_return_if_fail(odd);

    odd2 = odd + 1;
    even2 = even + oddlen + 1;
    odd += evenlen;
    n = rowlen - (evenlen + oddlen);
    i = 0;
    while (i+1 <= n) {
        /* Now even points to an even position. */
        target[i] = (*even < *odd) ? *odd : *even;
        i++;
        even += 2;
        odd += 2;

        /* Now even2 points to an even position. */
        target[i] = (*even2 < *odd2) ? *odd2 : *even2;
        i++;
        even2 += 2;
        odd2 += 2;
    }

    if (i <= n) {
        /* Now even points to an even position. */
        target[i] = (*even < *odd) ? *odd : *even;
        i++;
    }
    if (i <= n) {
        /* Now even2 points to an even position. */
        target[i] = (*even2 < *odd2) ? *odd2 : *even2;
    }
}

static void
compose_max_row_data_even(gdouble *target,
                          const gdouble *sub1,
                          guint sublen1,
                          const gdouble *sub2,
                          guint sublen2,
                          guint rowlen)
{
    guint i, n;

    g_return_if_fail(sublen1 + sublen2 <= rowlen);
    g_return_if_fail(sublen1 % 2 == 0);
    g_return_if_fail(sublen2 % 2 == 0);
    g_return_if_fail(target);
    g_return_if_fail(sub1);
    g_return_if_fail(sub2);

    sub2 += sublen1;
    n = rowlen - (sublen1 + sublen2);
    i = 0;
    while (i <= n) {
        target[i] = (*sub1 < *sub2) ? *sub2 : *sub1;
        i += 2;
        sub1 += 2;
        sub2 += 2;
    }
}

static void
compose_max_row_data_two(gdouble *target,
                         const gdouble *one,
                         guint rowlen)
{
    guint i, n;

    g_return_if_fail(2 <= rowlen);
    g_return_if_fail(target);
    g_return_if_fail(one);

    n = rowlen - 2;
    i = 0;
    while (i <= n) {
        target[i] = (*one < *(one + 1)) ? *(one + 1) : *one;
        i += 2;
        one += 2;
    }
}

/* Precomputes maxima for row.  Minimum is always computed from given index
 * blocklen values *forwards*, i.e. the block is not symmetrical it starts at
 * the given index. */
static void
max_precomputed_row_fill(const MinMaxPrecomputedReq *req,
                         MinMaxPrecomputedRow *prow,
                         const gdouble *x,
                         guint rowlen)
{
    guint blen;

    /* The row itself, AKA Each(1). */
    gwy_assign(prow->each[1], x, rowlen);

    for (blen = 2; blen <= req->maxlen_each; blen++) {
        const MinMaxPrecomputedLen *precomp;

        precomp = req->each + blen;
        if (precomp->needed) {
            g_assert(!precomp->even_even);
            if (precomp->even_odd) {
                compose_max_row_data_even_odd(prow->each[blen],
                                              prow->even[precomp->sublen1],
                                              precomp->sublen1,
                                              prow->each[precomp->sublen2],
                                              precomp->sublen2,
                                              rowlen);
            }
            else {
                compose_max_row_data_each(prow->each[blen],
                                          prow->each[precomp->sublen1],
                                          precomp->sublen1,
                                          prow->each[precomp->sublen2],
                                          precomp->sublen2,
                                          rowlen);
            }
        }

        if (blen > req->maxlen_even)
            continue;

        precomp = req->even + blen;
        if (precomp->needed) {
            g_assert(!precomp->even_odd);
            if (precomp->even_even) {
                compose_max_row_data_even(prow->even[blen],
                                          prow->even[precomp->sublen1],
                                          precomp->sublen1,
                                          prow->even[precomp->sublen2],
                                          precomp->sublen2,
                                          rowlen);
            }
            else {
                g_assert(blen == 2);
                g_assert(precomp->sublen1 == 1);
                compose_max_row_data_two(prow->even[blen],
                                         prow->each[precomp->sublen1],
                                         rowlen);
            }
        }
    }
}

static void
compose_min_row_data_each(gdouble *target,
                          const gdouble *sub1,
                          guint sublen1,
                          const gdouble *sub2,
                          guint sublen2,
                          guint rowlen)
{
    guint i, n;

    g_return_if_fail(sublen1 + sublen2 <= rowlen);
    g_return_if_fail(target);
    g_return_if_fail(sub1);
    g_return_if_fail(sub2);

    sub2 += sublen1;
    n = rowlen - (sublen1 + sublen2);
    i = 0;
    while (i <= n) {
        target[i] = (*sub1 > *sub2) ? *sub2 : *sub1;
        i++;
        sub1++;
        sub2++;
    }
}

static void
compose_min_row_data_even_odd(gdouble *target,
                              const gdouble *even,
                              guint evenlen,
                              const gdouble *odd,
                              guint oddlen,
                              guint rowlen)
{
    const gdouble *even2, *odd2;
    guint i, n;

    g_return_if_fail(evenlen + oddlen <= rowlen);
    g_return_if_fail(evenlen % 2 == 0);
    g_return_if_fail(target);
    g_return_if_fail(even);
    g_return_if_fail(odd);

    odd2 = odd + 1;
    even2 = even + oddlen + 1;
    odd += evenlen;
    n = rowlen - (evenlen + oddlen);
    i = 0;
    while (i+1 <= n) {
        /* Now even points to an even position. */
        target[i] = (*even > *odd) ? *odd : *even;
        i++;
        even += 2;
        odd += 2;

        /* Now even2 points to an even position. */
        target[i] = (*even2 > *odd2) ? *odd2 : *even2;
        i++;
        even2 += 2;
        odd2 += 2;
    }

    if (i <= n) {
        /* Now even points to an even position. */
        target[i] = (*even > *odd) ? *odd : *even;
        i++;
    }
    if (i <= n) {
        /* Now even2 points to an even position. */
        target[i] = (*even2 > *odd2) ? *odd2 : *even2;
    }
}

static void
compose_min_row_data_even(gdouble *target,
                          const gdouble *sub1,
                          guint sublen1,
                          const gdouble *sub2,
                          guint sublen2,
                          guint rowlen)
{
    guint i, n;

    g_return_if_fail(sublen1 + sublen2 <= rowlen);
    g_return_if_fail(sublen1 % 2 == 0);
    g_return_if_fail(sublen2 % 2 == 0);
    g_return_if_fail(target);
    g_return_if_fail(sub1);
    g_return_if_fail(sub2);

    sub2 += sublen1;
    n = rowlen - (sublen1 + sublen2);
    i = 0;
    while (i <= n) {
        target[i] = (*sub1 > *sub2) ? *sub2 : *sub1;
        i += 2;
        sub1 += 2;
        sub2 += 2;
    }
}

static void
compose_min_row_data_two(gdouble *target,
                         const gdouble *one,
                         guint rowlen)
{
    guint i, n;

    g_return_if_fail(2 <= rowlen);
    g_return_if_fail(target);
    g_return_if_fail(one);

    n = rowlen - 2;
    i = 0;
    while (i <= n) {
        target[i] = (*one > *(one + 1)) ? *(one + 1) : *one;
        i += 2;
        one += 2;
    }
}

/* Precomputes minima for row.  Minimum is always computed from given index
 * blocklen values *forwards*, i.e. the block is not symmetrical it starts at
 * the given index. */
static void
min_precomputed_row_fill(const MinMaxPrecomputedReq *req,
                         MinMaxPrecomputedRow *prow,
                         const gdouble *x,
                         guint rowlen)
{
    guint blen;

    /* The row itself, AKA Each(1). */
    gwy_assign(prow->each[1], x, rowlen);

    for (blen = 2; blen <= req->maxlen_each; blen++) {
        const MinMaxPrecomputedLen *precomp;

        precomp = req->each + blen;
        if (precomp->needed) {
            g_assert(!precomp->even_even);
            if (precomp->even_odd) {
                compose_min_row_data_even_odd(prow->each[blen],
                                              prow->even[precomp->sublen1],
                                              precomp->sublen1,
                                              prow->each[precomp->sublen2],
                                              precomp->sublen2,
                                              rowlen);
            }
            else {
                compose_min_row_data_each(prow->each[blen],
                                          prow->each[precomp->sublen1],
                                          precomp->sublen1,
                                          prow->each[precomp->sublen2],
                                          precomp->sublen2,
                                          rowlen);
            }
        }

        if (blen > req->maxlen_even)
            continue;

        precomp = req->even + blen;
        if (precomp->needed) {
            g_assert(!precomp->even_odd);
            if (precomp->even_even) {
                compose_min_row_data_even(prow->even[blen],
                                          prow->even[precomp->sublen1],
                                          precomp->sublen1,
                                          prow->even[precomp->sublen2],
                                          precomp->sublen2,
                                          rowlen);
            }
            else {
                g_assert(blen == 2);
                compose_min_row_data_two(prow->even[blen],
                                         prow->each[precomp->sublen1],
                                         rowlen);
            }
        }
    }
}

static void
min_max_precomputed_row_copy(MinMaxPrecomputedRow *target,
                             const MinMaxPrecomputedRow *source,
                             const MinMaxPrecomputedReq *req,
                             guint rowlen)
{
    gwy_assign(target->storage, source->storage, rowlen*req->nbuffers);
}

static void
min_max_precomputed_row_free(MinMaxPrecomputedRow *prow)
{
    g_free(prow->each);
    g_free(prow->even);
    g_free(prow->storage);
    g_free(prow);
}

static MaskRLE*
run_length_encode_mask(GwyDataField *mask)
{
    GArray *segments = g_array_new(FALSE, FALSE, sizeof(MaskSegment));
    MaskRLE *mrle = g_new0(MaskRLE, 1);
    const gdouble *data = mask->data;
    guint xres = mask->xres, yres = mask->yres;
    guint i, j, l;

    for (i = 0; i < yres; i++) {
        j = l = 0;
        while (j + l < xres) {
            if (*(data++))
                l++;
            else {
                if (l) {
                    MaskSegment seg = { i, j, l };
                    g_array_append_val(segments, seg);
                    j += l;
                    l = 0;
                }
                j++;
            }
        }
        if (l) {
            MaskSegment seg = { i, j, l };
            g_array_append_val(segments, seg);
        }
    }

    mrle->nsegments = segments->len;
    mrle->segments = (MaskSegment*)g_array_free(segments, FALSE);
    return mrle;
}

static void
mask_rle_free(MaskRLE *mrle)
{
    g_free(mrle->segments);
    g_free(mrle);
}

/* Analyse the set of segments and make a composition plan. */
static MinMaxPrecomputedReq*
find_required_lengths_for_rle(const MaskRLE *mrle)
{
    MinMaxPrecomputedReq *req;
    guint *lengths = g_new(guint, mrle->nsegments);
    guint i;

    for (i = 0; i < mrle->nsegments; i++)
        lengths[i] = mrle->segments[i].len;
    req = find_required_lengths_for_set(lengths, mrle->nsegments);
    g_free(lengths);

    return req;
}

static inline void
fill_block(gdouble *data, guint len, gdouble value)
{
    while (len--)
        *(data++) = value;
}

static inline void
row_extend_base(const gdouble *in, gdouble *out,
                guint *pos, guint *width, guint res,
                guint *extend_left, guint *extend_right)
{
    guint e2r, e2l;

    /* Expand the ROI to the right as far as possible */
    e2r = MIN(*extend_right, res - (*pos + *width));
    *width += e2r;
    *extend_right -= e2r;

    /* Expand the ROI to the left as far as possible */
    e2l = MIN(*extend_left, *pos);
    *width += e2l;
    *extend_left -= e2l;
    *pos -= e2l;

    /* Direct copy of the ROI */
    gwy_assign(out + *extend_left, in + *pos, *width);
}

static void
row_extend_border(const gdouble *in, gdouble *out,
                  guint pos, guint width, guint res,
                  guint extend_left, guint extend_right,
                  G_GNUC_UNUSED gdouble value)
{
    row_extend_base(in, out, &pos, &width, res, &extend_left, &extend_right);
    /* Forward-extend */
    fill_block(out + extend_left + width, extend_right, in[res-1]);
    /* Backward-extend */
    fill_block(out, extend_left, in[0]);
}

static void
mask_rle_execute_min_max(const MaskRLE *mrle, MinMaxPrecomputedRow **prows,
                         gdouble *outbuf, guint width, gboolean maximum)
{
    const MaskSegment *seg = mrle->segments;
    guint n = mrle->nsegments, i, j;
    const gdouble *segdata = prows[seg->row]->each[seg->len] + seg->col;

    gwy_assign(outbuf, segdata, width);
    seg++;
    for (i = n-1; i; i--, seg++) {
        segdata = prows[seg->row]->each[seg->len] + seg->col;
        if (maximum) {
            for (j = 0; j < width; j++) {
                if (outbuf[j] < segdata[j])
                    outbuf[j] = segdata[j];
            }
        }
        else {
            for (j = 0; j < width; j++) {
                if (outbuf[j] > segdata[j])
                    outbuf[j] = segdata[j];
            }
        }
    }
}

static gboolean
gwy_data_field_area_rle_analyse(GwyDataField *kernel,
                                gint width,
                                MinMaxPrecomputed *mmp)
{
    guint kxres, kyres, i;

    kxres = kernel->xres;
    kyres = kernel->yres;
    mmp->rowbuflen = width + kxres-1;
    mmp->kxres = kxres;
    mmp->kyres = kyres;

    /* Run-length encode the mask, i.e. transform it to a set of segments
     * and their positions. */
    mmp->mrle = run_length_encode_mask(kernel);
    if (!mmp->mrle->nsegments) {
        mask_rle_free(mmp->mrle);
        return FALSE;
    }

    mmp->req = find_required_lengths_for_rle(mmp->mrle);

    /* Create the row buffers for running extrema of various lengths. */
    mmp->prows = g_new(MinMaxPrecomputedRow*, kyres);
    for (i = 0; i < kyres; i++)
        mmp->prows[i] = min_max_precomputed_row_alloc(mmp->req, mmp->rowbuflen);

    mmp->extrowbuf = g_new(gdouble, mmp->rowbuflen);

    return TRUE;
}

static int
compare_segment(const void *pa, const void *pb)
{
    const MaskSegment *a = (const MaskSegment*)pa, *b = (const MaskSegment*)pb;

    if (a->row < b->row)
        return -1;
    if (a->row > b->row)
        return 1;
    if (a->col < b->col)
        return -1;
    if (a->col > b->col)
        return 1;
    return 0;
}

/* Rotate the RLE data by pi.  The set of block lengths does not change.
 * Therefore, the decompositions do not change either.  The only thing that
 * changes is the positions of the RLE segments. */
static void
gwy_data_field_area_rle_flip(MaskRLE *mrle, guint kxres, guint kyres)
{
    guint i;

    for (i = 0; i < mrle->nsegments; i++) {
        MaskSegment *seg = mrle->segments + i;
        seg->col = kxres - seg->col - seg->len;
        seg->row = kyres-1 - seg->row;
    }

    qsort(mrle->segments, mrle->nsegments, sizeof(MaskSegment),
          compare_segment);
}

static void
gwy_data_field_area_rle_free(MinMaxPrecomputed *mmp)
{
    guint i;

    g_free(mmp->extrowbuf);
    for (i = 0; i < mmp->kyres; i++)
        min_max_precomputed_row_free(mmp->prows[i]);
    g_free(mmp->prows);
    min_max_precomputed_req_free(mmp->req);
    mask_rle_free(mmp->mrle);
}

static void
gwy_data_field_area_min_max_execute(GwyDataField *dfield,
                                    gdouble *outbuf,
                                    MinMaxPrecomputed *mmp,
                                    gboolean maximum,
                                    gint col, gint row,
                                    gint width, gint height)
{
    MaskRLE *mrle = mmp->mrle;
    MinMaxPrecomputedReq *req = mmp->req;
    MinMaxPrecomputedRow **prows = mmp->prows, *prow;
    MinMaxPrecomputedRowFill precomp_row_fill;
    guint xres, yres, i, ii;
    guint extend_up, extend_down, extend_left, extend_right;
    gdouble *d, *extrowbuf = mmp->extrowbuf;
    guint rowbuflen = mmp->rowbuflen;

    xres = dfield->xres;
    yres = dfield->yres;
    d = dfield->data;
    precomp_row_fill = (maximum
                        ? max_precomputed_row_fill
                        : min_precomputed_row_fill);

    /* Initialise the buffers for the zeroth row of the area.  For the maximum
     * operation we even-sized kernels to the other direction to obtain
     * morphological operation according to definitions. */
    if (maximum) {
        extend_up = mmp->kyres/2;
        extend_down = (mmp->kyres - 1)/2;
        extend_left = mmp->kxres/2;
        extend_right = (mmp->kxres - 1)/2;
    }
    else {
        extend_up = (mmp->kyres - 1)/2;
        extend_down = mmp->kyres/2;
        extend_left = (mmp->kxres - 1)/2;
        extend_right = mmp->kxres/2;
    }

    for (i = 0; i <= extend_down; i++) {
        if (row + i < yres) {
            row_extend_border(d + xres*(row + i), extrowbuf,
                              col, width, xres,
                              extend_left, extend_right,
                              0.0);
            precomp_row_fill(req, prows[i + extend_up], extrowbuf, rowbuflen);
        }
        else
            min_max_precomputed_row_copy(prows[i], prows[i-1], req, rowbuflen);
    }
    for (i = 1; i <= extend_up; i++) {
        ii = extend_up - i;
        if (i <= (guint)row) {
            row_extend_border(d + xres*(row - i), extrowbuf,
                              col, width, xres,
                              extend_left, extend_right,
                              0.0);
            precomp_row_fill(req, prows[ii], extrowbuf, rowbuflen);
        }
        else
            min_max_precomputed_row_copy(prows[ii],
                                         prows[(ii + 1) % mmp->kyres],
                                         req, rowbuflen);
    }

    /* Go through the rows and extract the minima or maxima from the
     * precomputed segment data. */
    i = 0;
    while (TRUE) {
        mask_rle_execute_min_max(mrle, prows, outbuf + i*width, width, maximum);
        i++;
        if (i == (guint)height)
            break;

        /* Rotate physically prows[] so that the current row is at the zeroth
         * position.  We could use cyclic buffers but then all subordinate
         * functions would know how to handle them and calculating mod() for
         * all indexing is inefficient anyway. */
        prow = prows[0];
        for (ii = 0; ii < mmp->kyres-1; ii++)
            prows[ii] = prows[ii+1];
        prows[mmp->kyres-1] = prow;

        /* Precompute the new row at the bottom. */
        ii = row + i + extend_down;
        if (ii < yres) {
            row_extend_border(d + xres*ii, extrowbuf,
                              col, width, xres,
                              extend_left, extend_right,
                              0.0);
            precomp_row_fill(req, prow, extrowbuf, rowbuflen);
        }
        else {
            g_assert(mmp->kyres >= 2);
            min_max_precomputed_row_copy(prow, prows[mmp->kyres-2],
                                         req, rowbuflen);
        }
    }
}

static gboolean
kernel_is_nonempty(GwyDataField *dfield)
{
    guint i, n = dfield->xres * dfield->yres;
    gdouble *d = dfield->data;

    for (i = 0; i < n; i++, d++) {
        if (*d)
            return TRUE;
    }
    return FALSE;
}

/* NB: The kernel passed to this function should be non-empty. */
static void
gwy_data_field_area_filter_min_max_real(GwyDataField *data_field,
                                        GwyDataField *kernel,
                                        GwyMinMaxFilterType filtertype,
                                        gint col, gint row,
                                        gint width, gint height)
{
    MinMaxPrecomputed mmp;
    gdouble *outbuf, *d;
    gint i, j, xres, yres, kxres, kyres;

    g_return_if_fail(GWY_IS_DATA_FIELD(data_field));
    g_return_if_fail(col >= 0 && row >= 0
                     && width > 0 && height > 0
                     && col + width <= data_field->xres
                     && row + height <= data_field->yres);

    xres = data_field->xres;
    yres = data_field->yres;
    kxres = kernel->xres;
    kyres = kernel->yres;
    d = data_field->data;

    if (filtertype == GWY_MIN_MAX_FILTER_MINIMUM
        || filtertype == GWY_MIN_MAX_FILTER_MAXIMUM) {
        gboolean is_max = (filtertype == GWY_MIN_MAX_FILTER_MAXIMUM);

        gwy_data_field_area_rle_analyse(kernel, width, &mmp);
        if (is_max)
            gwy_data_field_area_rle_flip(mmp.mrle, kxres, kyres);
        outbuf = g_new(gdouble, width*height);
        gwy_data_field_area_min_max_execute(data_field, outbuf, &mmp, is_max,
                                            col, row, width, height);
        gwy_data_field_area_rle_free(&mmp);

        d += row*xres + col;
        for (i = 0; i < height; i++)
            gwy_assign(d + i*xres, outbuf + i*width, width);
        gwy_data_field_invalidate(data_field);
        g_free(outbuf);
    }
    else if (filtertype == GWY_MIN_MAX_FILTER_RANGE
             || filtertype == GWY_MIN_MAX_FILTER_NORMALIZATION) {
        gdouble *outbuf2;

        gwy_data_field_area_rle_analyse(kernel, width, &mmp);
        outbuf = g_new(gdouble, width*height);
        gwy_data_field_area_min_max_execute(data_field, outbuf, &mmp, FALSE,
                                            col, row, width, height);

        gwy_data_field_area_rle_flip(mmp.mrle, kxres, kyres);
        outbuf2 = g_new(gdouble, width*height);
        gwy_data_field_area_min_max_execute(data_field, outbuf2, &mmp, TRUE,
                                            col, row, width, height);
        gwy_data_field_area_rle_free(&mmp);

        d += row*xres + col;
        if (filtertype == GWY_MIN_MAX_FILTER_RANGE) {
            for (i = 0; i < height; i++) {
                for (j = 0; j < width; j++)
                    d[i*xres + j] = outbuf2[i*width + j] - outbuf[i*width + j];
            }
        }
        else {
            for (i = 0; i < height; i++) {
                for (j = 0; j < width; j++) {
                    gdouble min = outbuf[i*width + j];
                    gdouble max = outbuf2[i*width + j];

                    if (G_UNLIKELY(min == max))
                        d[i*xres + j] = 0.5;
                    else
                        d[i*xres + j] = (d[i*xres + j] - min)/(max - min);
                }
            }
        }
        gwy_data_field_invalidate(data_field);
        g_free(outbuf2);
        g_free(outbuf);
    }
    else if (filtertype == GWY_MIN_MAX_FILTER_OPENING
             || filtertype == GWY_MIN_MAX_FILTER_CLOSING) {
        gboolean is_closing = (filtertype == GWY_MIN_MAX_FILTER_CLOSING);
        /* To limit the area of application but keep the influence of
         * surrouding pixels as if we did erosion and dilation on the entire
         * field, we must perform the first operation in an extended area. */
        gint extcol = MAX(0, col - kxres/2);
        gint extrow = MAX(0, row - kyres/2);
        gint extwidth = MIN(xres, col + width + kxres/2) - extcol;
        gint extheight = MIN(yres, row + height + kyres/2) - extrow;
        GwyDataField *tmpfield;

        gwy_data_field_area_rle_analyse(kernel, extwidth, &mmp);
        if (is_closing)
            gwy_data_field_area_rle_flip(mmp.mrle, kxres, kyres);
        tmpfield = gwy_data_field_new(extwidth, extheight, extwidth, extheight,
                                      FALSE);
        gwy_data_field_area_min_max_execute(data_field, tmpfield->data, &mmp,
                                            is_closing,
                                            extcol, extrow,
                                            extwidth, extheight);

        if (extcol == col && extrow == row
            && extwidth == width && extheight == height) {
            /* Avoid repeating the analysis for full-field application. */
            gwy_data_field_area_rle_flip(mmp.mrle, kxres, kyres);
        }
        else {
            gwy_data_field_area_rle_free(&mmp);
            gwy_data_field_area_rle_analyse(kernel, width, &mmp);
            if (!is_closing)
                gwy_data_field_area_rle_flip(mmp.mrle, kxres, kyres);
        }
        outbuf = g_new(gdouble, width*height);
        gwy_data_field_area_min_max_execute(tmpfield, outbuf, &mmp,
                                            !is_closing,
                                            col - extcol, row - extrow,
                                            width, height);
        gwy_data_field_area_rle_free(&mmp);
        g_object_unref(tmpfield);

        d += row*xres + col;
        for (i = 0; i < height; i++)
            gwy_assign(d + i*xres, outbuf + i*width, width);
        gwy_data_field_invalidate(data_field);

        g_free(outbuf);
    }
    else {
        g_return_if_reached();
    }
}

/**
 * gwy_data_field_area_filter_min_max:
 * @data_field: A data field to apply the filter to.
 * @kernel: Data field defining the flat structuring element.
 * @filtertype: The type of filter to apply.
 * @col: Upper-left column coordinate.
 * @row: Upper-left row coordinate.
 * @width: Area width (number of columns).
 * @height: Area height (number of rows).
 *
 * Applies a morphological operation with a flat structuring element to a
 * part of a data field.
 *
 * Morphological operations with flat structuring elements can be expressed
 * using minimum (erosion) and maximum (dilation) filters that are the basic
 * operations this function can perform.
 *
 * The kernel field is a mask that defines the shape of the flat structuring
 * element.  It is reflected for all maximum operations (dilation).  For
 * symmetrical kernels this does not matter.  You can use
 * gwy_data_field_elliptic_area_fill() to create a true circular (or
 * elliptical) kernel.
 *
 * The kernel is implicitly centered, i.e. it will be applied symmetrically to
 * avoid unexpected data movement.  Even-sized kernels (generally not
 * recommended) will extend farther towards the top left image corner for
 * minimum (erosion) and towards the bottom right corner for maximum (dilation)
 * operations due to the reflection.  If you need off-center structuring
 * elements you can add empty rows or columns to one side of the kernel to
 * counteract the symmetrisation.
 *
 * The operation is linear-time in kernel size for any convex kernel.  Note
 * gwy_data_field_area_filter_minimum() and
 * gwy_data_field_area_filter_maximum(), which are limited to square
 * structuring elements, are much faster for large sizes of the squares.
 *
 * The exterior is always handled as %GWY_EXTERIOR_BORDER_EXTEND.
 *
 * Since: 2.43
 **/
void
gwy_data_field_area_filter_min_max(GwyDataField *data_field,
                                   GwyDataField *kernel,
                                   GwyMinMaxFilterType filtertype,
                                   gint col, gint row,
                                   gint width, gint height)
{
    GwyDataField *redkernel;

    g_return_if_fail(GWY_IS_DATA_FIELD(kernel));
    redkernel = gwy_data_field_duplicate(kernel);
    gwy_data_field_grains_autocrop(redkernel, TRUE, NULL, NULL, NULL, NULL);
    if (kernel_is_nonempty(redkernel)) {
        gwy_data_field_area_filter_min_max_real(data_field, redkernel,
                                                filtertype,
                                                col, row, width, height);
    }
    g_object_unref(redkernel);
}

/**
 * gwy_data_field_area_filter_disc_asf:
 * @data_field: A data field to apply the filter to.
 * @radius: Maximum radius of the circular structuring element, in pixels.
 *          For radius 0 and smaller the filter is no-op.
 * @closing: %TRUE requests an opening-closing filter (i.e. ending with
 *           closing), %FALSE requests a closing-opening filter (i.e. ending
 *           with opening).
 * @col: Upper-left column coordinate.
 * @row: Upper-left row coordinate.
 * @width: Area width (number of columns).
 * @height: Area height (number of rows).
 *
 * Applies an alternating sequential morphological filter with a flat disc
 * structuring element to a part of a data field.
 *
 * Alternating sequential filter is a filter consisting of repeated opening and
 * closing (or closing and opening) with progressively larger structuring
 * elements.  This function performs such filtering for sequence of structuring
 * elements consisting of true Euclidean discs with increasing radii.  The
 * largest disc in the sequence fits into a (2@size + 1) × (2@size + 1) square.
 *
 * Since: 2.43
 **/
void
gwy_data_field_area_filter_disc_asf(GwyDataField *data_field,
                                    gint radius,
                                    gboolean closing,
                                    gint col,
                                    gint row,
                                    gint width,
                                    gint height)
{
    GwyMinMaxFilterType filtertype1, filtertype2;
    gint r;

    g_return_if_fail(GWY_IS_DATA_FIELD(data_field));
    g_return_if_fail(col >= 0 && row >= 0
                     && width > 0 && height > 0
                     && col + width <= data_field->xres
                     && row + height <= data_field->yres);

    if (closing) {
        filtertype1 = GWY_MIN_MAX_FILTER_OPENING;
        filtertype2 = GWY_MIN_MAX_FILTER_CLOSING;
    }
    else {
        filtertype1 = GWY_MIN_MAX_FILTER_CLOSING;
        filtertype2 = GWY_MIN_MAX_FILTER_OPENING;
    }

    for (r = 1; r <= radius; r++) {
        gint size = 2*r + 1;
        GwyDataField *kernel = gwy_data_field_new(size, size, size, size, TRUE);
        gwy_data_field_elliptic_area_fill(kernel, 0, 0, size, size, 1.0);
        gwy_data_field_area_filter_min_max_real(data_field, kernel, filtertype1,
                                                col, row, width, height);
        gwy_data_field_area_filter_min_max_real(data_field, kernel, filtertype2,
                                                col, row, width, height);
        g_object_unref(kernel);
    }
}

/* vim: set cin et ts=4 sw=4 cino=>1s,e0,n0,f0,{0,}0,^0,\:1s,=0,g1s,h0,t0,+1s,c3,(0,u0 : */
