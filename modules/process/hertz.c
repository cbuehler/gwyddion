/*
 *  @(#) $Id$
 *  Copyright (C) 2017-2018 Anna Charvatova Campbell
 *  E-mail: acampbellova@cmi.cz
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
 */

#include <gtk/gtk.h>
#include <libgwyddion/gwymacros.h>
#include <libgwyddion/gwymath.h>
#include <libgwyddion/gwyversion.h>
#include <libgwymodule/gwymodule.h>
#include <libprocess/stats.h>
#include <libprocess/filters.h>
#include <libgwydgets/gwydgets.h>
#include <app/gwyapp.h>
#include "config.h"
#include "preview.h"

#define HERTZ_RUN_MODES  (GWY_RUN_IMMEDIATE | GWY_RUN_INTERACTIVE)

typedef enum  {
    PREVIEW_MEANCURV = 0,
    PREVIEW_GAUSSCURV = 1,
    PREVIEW_MODULUS = 2,
    PREVIEW_DEFORMATION = 3,
    PREVIEW_MASK = 4
} HertzPreviewType;

/* Data for this function. */
typedef struct {
    gdouble modulus; /* in Pa */
    gdouble radius;  /* in m */
    gdouble load;    /* in N */
    gdouble mag; /* power of radius unit */
    HertzPreviewType preview_type;
    gboolean instant_update;
} HertzArgs;

typedef struct {
    GtkObject *modulus;
    GtkObject *radius;
    GtkObject *load;
    GtkWidget *view;
    GtkWidget *color_button;
    GtkWidget *instant_update;
    GSList *preview_type;
    GwyPixmapLayer *player;
    GwyPixmapLayer *mlayer;
    GwyContainer *original_data;
    GwyContainer *result_data;
    GwyDataField *original_dfield;
    HertzArgs *args;
} HertzControls;

static gboolean      module_register           (void);
static void          hertz_modulus             (GwyContainer *data,
                                                GwyRunType run);
static void          hertz_modulus_dialog      (HertzArgs *args,
                                                GwyContainer *data,
                                                GwyDataField *dfield,
                                                gint id);
static void          hertz_modulus_reset_dialog(HertzControls *controls,
                                                HertzArgs *args);
static void          hertz_modulus_do          (GwyContainer *original_data,
                                                GwyContainer *result_data,
                                                gint id);
static void          hertz_modulus_calc        (GwyDataField *dfield,
                                                HertzArgs *args,
                                                GwyContainer *result_data);
static void          run_noninteractive        (HertzArgs *args,
                                                GwyContainer *data,
                                                GwyDataField *dfield,
                                                gint id);
static void          modulus_changed           (GtkAdjustment *adj,
                                                HertzControls *controls);
static void          radius_changed            (GtkAdjustment *adj,
                                                HertzControls *controls);
static void          load_changed              (GtkAdjustment *adj,
                                                HertzControls *controls);
static void          preview_type_changed      (GtkToggleButton *button,
                                                HertzControls *controls);
static void          instant_update_changed    (GtkToggleButton *check,
                                                HertzControls *controls);
static void          set_visible_images        (HertzControls *controls);
static void          update_view               (HertzControls *controls,
                                                HertzArgs *args);
static GwyContainer* create_result_container   (GwyContainer *data,
                                                GwyDataField *dfield,
                                                gint id);
static void          hertz_modulus_load_args   (GwyContainer *container,
                                                HertzArgs *args);
static void          hertz_modulus_save_args   (GwyContainer *container,
                                                HertzArgs *args);

static const HertzArgs hertz_modulus_defaults = {
    13e9,
    42,
    1e-6,
    1,
    0,
    FALSE,
};

/* The module info. */
static GwyModuleInfo module_info = {
    GWY_MODULE_ABI_VERSION,
    &module_register,
    "Calculates the apparent Young's modulus of a rough surface "
        "according to Hertzian contact theory.",
    "Anna Charvatova Campbell <acampbellova@cmi.cz>",
    "0.1",
    "Anna Charvatova Campbell",
    "2017",
};

/* This is the ONLY exported symbol.  The argument is the module info.
 * NO semicolon after. */
GWY_MODULE_QUERY2(module_info, hertz)

    /* Module registering function.
     * Called at Gwyddion startup and registeres one or more function.
     */
static gboolean
module_register(void)
{
    gwy_process_func_register("hertz_modulus",
                              (GwyProcessFunc)&hertz_modulus,
                              N_("/SPM M_odes/_Force and Indentation/_Hertz contact..."),
                              NULL,
                              HERTZ_RUN_MODES,
                              GWY_MENU_FLAG_DATA,
                              N_("Hertzian contact theory."));

    return TRUE;
}

static void
hertz_modulus(GwyContainer *data, GwyRunType run)
{
    HertzArgs args;
    GwyDataField *dfield;
    gint id;

    g_return_if_fail(run & HERTZ_RUN_MODES);

    args = hertz_modulus_defaults;
    hertz_modulus_load_args(gwy_app_settings_get(), &args);

    gwy_app_data_browser_get_current(GWY_APP_DATA_FIELD, &dfield,
                                     GWY_APP_DATA_FIELD_ID, &id,
                                     0);
    g_return_if_fail(dfield);

    if (run == GWY_RUN_IMMEDIATE) {
        run_noninteractive(&args, data, dfield, id);
    }
    else {
        hertz_modulus_dialog(&args, data, dfield, id);
    }
}

static void
run_noninteractive(HertzArgs *args, GwyContainer *data,
                   GwyDataField *dfield, gint id)
{
    GwyContainer *result_data;

    result_data = create_result_container(data, dfield, id);
    hertz_modulus_calc(dfield, args, result_data);
    hertz_modulus_do(data, result_data, id);
    g_object_unref(result_data);
}


static void
hertz_modulus_dialog(HertzArgs *args, GwyContainer *data,
                     GwyDataField *dfield, gint id)
{

    GtkWidget *dialog, *table, *hbox, *label, *button;
    GtkObject *gtkobj;
    HertzControls controls;
    gint response, row;
    GwySIUnit *unit;
    gchar *str;
    GwySIValueFormat *zvf;
    GdkColor gdkcolor = { 0, 51118, 0, 0 };


    controls.args = args;

    dialog = gtk_dialog_new_with_buttons(_("Hertzian contact modulus"), NULL, 0,
            NULL);

    button = gwy_stock_like_button_new(_("Co_mpute"), GTK_STOCK_EXECUTE);
    gtk_dialog_add_action_widget(GTK_DIALOG(dialog), button,
            RESPONSE_CALCULATE);

    gtk_dialog_add_button(GTK_DIALOG(dialog), _("_Reset"), RESPONSE_RESET);
    gtk_dialog_add_button(GTK_DIALOG(dialog),
                          GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL);
    gtk_dialog_add_button(GTK_DIALOG(dialog),
                          GTK_STOCK_OK, GTK_RESPONSE_OK);
    gtk_dialog_set_default_response(GTK_DIALOG(dialog), GTK_RESPONSE_OK);
    gwy_help_add_to_proc_dialog(GTK_DIALOG(dialog), GWY_HELP_DEFAULT);

    hbox = gtk_hbox_new(FALSE, 2);

    gtk_box_pack_start(GTK_BOX(GTK_DIALOG(dialog)->vbox), hbox,
                       FALSE, FALSE, 4);

    /* z-unit */
    unit = gwy_data_field_get_si_unit_z(dfield);
    str = gwy_si_unit_get_string(unit, GWY_SI_UNIT_FORMAT_PLAIN);
    zvf = gwy_data_field_get_value_format_z(dfield, GWY_SI_UNIT_FORMAT_PLAIN,
                                            NULL);
    args->mag = zvf->magnitude;



    /* Store original data */
    controls.original_data = data;
    controls.original_dfield = dfield;

    controls.result_data = create_result_container(data, dfield, id);

    /* show rescaled result */
    controls.view =  create_preview(controls.result_data,
                                    0, PREVIEW_SIZE, TRUE);
    controls.player = gwy_data_view_get_base_layer(GWY_DATA_VIEW(controls.view));
    controls.mlayer = gwy_data_view_get_alpha_layer(GWY_DATA_VIEW(controls.view));

    gtk_box_pack_start(GTK_BOX(hbox), controls.view, TRUE, TRUE, 4);

    /* parameters set by user */
    table = gtk_table_new(10, 4, FALSE);
    gtk_table_set_row_spacings(GTK_TABLE(table), 2);
    gtk_table_set_col_spacings(GTK_TABLE(table), 6);
    gtk_container_set_border_width(GTK_CONTAINER(table), 4);
    gtk_box_pack_start(GTK_BOX(hbox), table, TRUE, TRUE, 4);

    row = 0;
    if (g_strcmp0(str, "m") != 0) {
        label = gtk_label_new("Values should be height values. \n"
                              "The following results don't make much sense.");
       gtk_widget_modify_fg(label, GTK_STATE_NORMAL, &gdkcolor);
        gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);
        gtk_table_attach(GTK_TABLE(table), label, 0, 2, row, row+1,
                         GTK_EXPAND | GTK_FILL, 0, 0, 0);
        row++;
    }
    g_free(str);

    controls.modulus = gtk_adjustment_new(args->modulus*1e-9,
                                          1e-3, 1e3, 0.001, 1, 0);

    gtkobj = controls.modulus;
    gwy_table_attach_hscale(table, row, _("_Contact modulus:"),
            "GPa", gtkobj, GWY_HSCALE_DEFAULT);
    g_signal_connect(gtkobj, "value-changed",
                     G_CALLBACK(modulus_changed), &controls);

    row++;

    controls.radius = gtk_adjustment_new(args->radius,
                                         0.1, 1e6, 0.1, 1, 0);

    gtkobj = controls.radius;
    gwy_table_attach_hscale(table, row, _("_Tip radius:"),
            zvf->units, gtkobj, GWY_HSCALE_DEFAULT);
    g_signal_connect(gtkobj, "value_changed",
                     G_CALLBACK(radius_changed), &controls);
    row++;


    controls.load = gtk_adjustment_new(args->load*1e6,
                                         0.1, 1e6, 0.1, 1, 0);

    gtkobj = controls.load;
    gwy_table_attach_hscale(table, row, _("_Load applied:"),
            "uN", gtkobj, GWY_HSCALE_DEFAULT);
    g_signal_connect(gtkobj, "value_changed",
                     G_CALLBACK(load_changed), &controls);
    row++;


    label = gtk_label_new(_("Preview:"));
    gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);
    gtk_table_attach(GTK_TABLE(table), label, 0, 2, row, row+1,
                     GTK_FILL, 0, 0, 0);
    row++;

    controls.preview_type
        = gwy_radio_buttons_createl(G_CALLBACK(preview_type_changed), &controls,
                                    args->preview_type,
                                    _("Mean _curvature"), PREVIEW_MEANCURV,
                                    _("Gaussian c_urvature"), PREVIEW_GAUSSCURV,
                                    _("Contact _modulus"), PREVIEW_MODULUS,
                                    _("_Deformation"), PREVIEW_DEFORMATION,
                                    _("Excluded _points"), PREVIEW_MASK,
                                    NULL);
    row = gwy_radio_buttons_attach_to_table(controls.preview_type,
                                            GTK_TABLE(table), 3, row);
    gtk_table_set_row_spacing(GTK_TABLE(table), row-1, 8);


    controls.color_button = create_mask_color_button(controls.result_data,
                                                     dialog, 0);
    gwy_table_attach_hscale(table, row, _("_Mask color:"), NULL,
                            GTK_OBJECT(controls.color_button),
                            GWY_HSCALE_WIDGET_NO_EXPAND);

    row ++;

    controls.instant_update
        = gtk_check_button_new_with_mnemonic(_("_Instant updates"));
    gtk_table_attach(GTK_TABLE(table), controls.instant_update,
            0, 3, row, row+1, GTK_EXPAND | GTK_FILL, 0, 0, 0);
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(controls.instant_update),
            controls.args->instant_update);
    g_signal_connect(controls.instant_update, "toggled",
            G_CALLBACK(instant_update_changed),
            &controls);

    row++;


    update_view(&controls, args);

    gtk_widget_show_all(dialog);
    do {
        response = gtk_dialog_run(GTK_DIALOG(dialog));
        switch (response) {
            case GTK_RESPONSE_CANCEL:
            case GTK_RESPONSE_DELETE_EVENT:
            gtk_widget_destroy(dialog);
            case GTK_RESPONSE_NONE:
            g_object_unref(controls.result_data);
            return ;
            break;

            case GTK_RESPONSE_OK:
            hertz_modulus_do(controls.original_data, controls.result_data, id);
            break;

            case RESPONSE_RESET:
            hertz_modulus_reset_dialog(&controls, args);
            break;

            case RESPONSE_CALCULATE:
            update_view(&controls, args);
            break;

            default:
            g_assert_not_reached();
            break;
        }
    } while (response != GTK_RESPONSE_OK);

    hertz_modulus_save_args(gwy_app_settings_get(), args);
    gtk_widget_destroy(dialog);
    g_object_unref(controls.result_data);

    return ;
}

static void
hertz_modulus_reset_dialog(HertzControls *controls, HertzArgs *args)
{
    gdouble mag;

    mag = args->mag;
    *args = hertz_modulus_defaults;
    args->mag = mag;

    gtk_adjustment_set_value(GTK_ADJUSTMENT(controls->modulus),
                             args->modulus*1e-9);
    gtk_adjustment_set_value(GTK_ADJUSTMENT(controls->radius), args->radius);
    gtk_adjustment_set_value(GTK_ADJUSTMENT(controls->load),
                             args->load*1e6);


    update_view(controls, args);
}

static GwyContainer *
create_result_container(GwyContainer *data, GwyDataField *dfield, gint id)
{
    GwyDataField *result_field, *mask;
    GwySIUnit *unit;
    GwyContainer *result_data;

    result_data = gwy_container_new();

    /* mean curvature */
    result_field = gwy_data_field_new_alike(dfield, TRUE);
    unit = gwy_si_unit_new("m");
    gwy_si_unit_power(unit, -1, unit);
    gwy_data_field_set_si_unit_z(result_field, unit);
    g_object_unref(unit);
    gwy_container_set_object_by_name(result_data, "/0/data",
                                     result_field);
    gwy_app_sync_data_items(data, result_data, id, 0, FALSE,
                            GWY_DATA_ITEM_GRADIENT,
                            GWY_DATA_ITEM_MASK_COLOR,
                            0);
    mask = create_mask_field(result_field);
    gwy_container_set_object_by_name(result_data, "/0/mask",
                                     mask);
    g_object_unref(result_field);
    g_object_unref(mask);

    /* Gaussian curvature */
    result_field = gwy_data_field_new_alike(dfield, TRUE);
    unit = gwy_si_unit_new("m");
    gwy_si_unit_power(unit, -2, unit);
    gwy_data_field_set_si_unit_z(result_field, unit);
    g_object_unref(unit);
    gwy_container_set_object_by_name(result_data, "/1/data",
                                     result_field);
    gwy_app_sync_data_items(data, result_data, id, 1, FALSE,
                            GWY_DATA_ITEM_GRADIENT,
                            GWY_DATA_ITEM_MASK_COLOR,
                            0);
    mask = create_mask_field(result_field);
    gwy_container_set_object_by_name(result_data, "/1/mask",
                                     mask);
    g_object_unref(result_field);
    g_object_unref(mask);


    /* reduced modulus */
    result_field = gwy_data_field_new_alike(dfield, TRUE);
    unit = gwy_si_unit_new("Pa");
    gwy_data_field_set_si_unit_z(result_field, unit);
    g_object_unref(unit);

    gwy_container_set_object_by_name(result_data, "/2/data",
                                     result_field);
    gwy_app_sync_data_items(data, result_data, id, 2, FALSE,
                            GWY_DATA_ITEM_GRADIENT,
                            0);
    mask = create_mask_field(result_field);
    gwy_container_set_object_by_name(result_data, "/2/mask",
                                     mask);
    g_object_unref(result_field);
    g_object_unref(mask);

    /* deformation */
    result_field = gwy_data_field_new_alike(dfield, TRUE);
    unit = gwy_si_unit_new("m");
    gwy_data_field_set_si_unit_z(result_field, unit);
    g_object_unref(unit);

    gwy_container_set_object_by_name(result_data, "/3/data",
                                     result_field);
    gwy_app_sync_data_items(data, result_data, id, 3, FALSE,
                            GWY_DATA_ITEM_GRADIENT,
                            0);
    mask = create_mask_field(result_field);
    gwy_container_set_object_by_name(result_data, "/3/mask",
                                     mask);
    g_object_unref(result_field);
    g_object_unref(mask);


    /* mask */
    mask = create_mask_field(dfield);
    gwy_container_set_object_by_name(result_data, "/4/data",
                                     mask);

    return result_data;
}

static void
modulus_changed(GtkAdjustment *adj,
                HertzControls *controls)
{
    controls->args->modulus = gtk_adjustment_get_value(adj)*1e9;
    if (controls->args->instant_update)
    update_view(controls, controls->args);
}

static void
radius_changed(GtkAdjustment *adj,
               HertzControls *controls)
{
    controls->args->radius = gtk_adjustment_get_value(adj);
    if (controls->args->instant_update)
    update_view(controls, controls->args);
}

static void
load_changed(GtkAdjustment *adj,
               HertzControls *controls)
{
    controls->args->load = gtk_adjustment_get_value(adj)*1e-6;
    if (controls->args->instant_update)
        update_view(controls, controls->args);
}

static void
preview_type_changed(GtkToggleButton *button,
                     HertzControls *controls)
{

    if (button && !gtk_toggle_button_get_active(button))
        return;

    controls->args->preview_type
        = gwy_radio_buttons_get_current(controls->preview_type);

    set_visible_images(controls);

}

static void
instant_update_changed(GtkToggleButton *check,
        HertzControls *controls)
{
    controls->args->instant_update = gtk_toggle_button_get_active(check);
    if (controls->args->instant_update)
        update_view(controls, controls->args);
}

static void
set_visible_images(HertzControls *controls)
{

    switch (controls->args->preview_type) {
        case PREVIEW_MEANCURV:
        g_object_set(controls->player, "data-key", "/0/data", NULL);
        break;
        case PREVIEW_GAUSSCURV:
        g_object_set(controls->player, "data-key", "/1/data", NULL);
        break;
        case PREVIEW_MODULUS:
        g_object_set(controls->player, "data-key", "/2/data", NULL);
        break;
        case PREVIEW_DEFORMATION:
        g_object_set(controls->player, "data-key", "/3/data", NULL);
        break;
        case PREVIEW_MASK:
        g_object_set(controls->player, "data-key", "/4/data", NULL);
        break;
    }
}

static const gchar modulus_key[]  = "/module/hertzcontact/modulus";
static const gchar load_key[]  = "/module/hertzcontact/load";
static const gchar radius_key[]  = "/module/hertzcontact/tipradius";
static const gchar mag_key[]  = "/module/hertzcontact/magnitude";
static const gchar instant_update_key[]  = "/module/hertzcontact/instant_update";

static void
hertz_modulus_load_args(GwyContainer *container,
                        HertzArgs *args)
{
    *args = hertz_modulus_defaults;

    gwy_container_gis_double_by_name(container, modulus_key, &args->modulus);
    gwy_container_gis_double_by_name(container, load_key, &args->load);
    gwy_container_gis_double_by_name(container, radius_key, &args->radius);
    gwy_container_gis_double_by_name(container, mag_key, &args->mag);
    gwy_container_gis_boolean_by_name(container, instant_update_key, &args->instant_update);

    args->modulus = MAX(args->modulus, 0.0);
    args->load = MAX(args->load, 0.0);
    args->radius = MAX(args->radius, 0.0);
}

static void
hertz_modulus_save_args(GwyContainer *container,
                        HertzArgs *args)
{
    gwy_container_set_double_by_name(container, modulus_key, args->modulus);
    gwy_container_set_double_by_name(container, load_key, args->load);
    gwy_container_set_double_by_name(container, radius_key, args->radius);
    gwy_container_set_double_by_name(container, mag_key, args->mag);
    gwy_container_set_boolean_by_name(container, instant_update_key, args->instant_update);
}


static void
update_view(HertzControls *controls, HertzArgs *args)
{
    hertz_modulus_calc(controls->original_dfield, args, controls->result_data);
    g_object_set(controls->mlayer, "data-key", "/0/mask", NULL);
}

static void
hertz_modulus_calc(GwyDataField *dfield, HertzArgs *args,
                   GwyContainer *result_data)
{
    GwyDataField *meancurv, *gausscurv, *modulus, *mask, *dz;
    GwyDataField *dxfield;
    GwyDataField *dyfield;
    GwyDataField *dxxfield;
    GwyDataField *dxyfield;
    GwyDataField *dyyfield;
    gint i, j;
    gint xres, yres;
    gdouble *px, *py, *pxx, *pyy, *pxy;
    gdouble *pc, *pg, *pE, *m, *pz;
    gdouble R, E, w, F, coef;


    meancurv = GWY_DATA_FIELD(gwy_container_get_object_by_name(result_data,
                                                               "/0/data"));
    gausscurv = GWY_DATA_FIELD(gwy_container_get_object_by_name(result_data,
                                                                "/1/data"));
    mask = GWY_DATA_FIELD(gwy_container_get_object_by_name(result_data,
                                                           "/0/mask"));
    modulus = GWY_DATA_FIELD(gwy_container_get_object_by_name(result_data,
                                                              "/2/data"));
    dz = GWY_DATA_FIELD(gwy_container_get_object_by_name(result_data,
                                                              "/3/data"));
    gwy_data_field_clear(mask);

    R = args->radius*args->mag;
    E = args->modulus;
    F = args->load;

    xres = gwy_data_field_get_xres(dfield);
    yres = gwy_data_field_get_yres(dfield);

    dxfield = gwy_data_field_new_alike(dfield, FALSE);
    dyfield = gwy_data_field_new_alike(dfield, FALSE);
    dxxfield = gwy_data_field_new_alike(dfield, FALSE);
    dyyfield = gwy_data_field_new_alike(dfield, FALSE);
    dxyfield = gwy_data_field_new_alike(dfield, FALSE);

    px = gwy_data_field_get_data(dxfield);
    py = gwy_data_field_get_data(dyfield);

    for (j = 0; j < yres; j++) {
        for (i = 0; i < xres; i++) {
            *px = gwy_data_field_get_xder(dfield, i, j);
            px++;
            *py = gwy_data_field_get_yder(dfield, i, j);
            py++;
        }
    }

    pxx = gwy_data_field_get_data(dxxfield);
    pxy = gwy_data_field_get_data(dxyfield);
    pyy = gwy_data_field_get_data(dyyfield);

    for (j = 0; j < yres; j++) {
        for (i = 0; i < xres; i++) {
            *pxx = gwy_data_field_get_xder(dxfield, i, j);
            pxx++;
            *pxy = gwy_data_field_get_yder(dxfield, i, j);
            pxy++;
            *pyy = gwy_data_field_get_yder(dyfield, i, j);
            pyy++;
        }
    }

    px = gwy_data_field_get_data(dxfield);
    py = gwy_data_field_get_data(dyfield);
    pxx = gwy_data_field_get_data(dxxfield);
    pxy = gwy_data_field_get_data(dxyfield);
    pyy = gwy_data_field_get_data(dyyfield);


    pc = gwy_data_field_get_data(meancurv);
    pg = gwy_data_field_get_data(gausscurv);
    for (i = 0; i < xres*yres; i++, px++, py++, pxx++, pxy++, pyy++, pc++, pg++) {
        w = 1+(*px)*(*px)+(*py)*(*py);
        *pc = 0.5*((1+(*px)*(*px))*(*pyy)
                   + (1+(*py)*(*py))*(*pxx)
                   - 2*(*pxy)*(*px)*(*py))
            /pow(w, 1.5);
        *pg = ((*pxx)*(*pyy) - (*pxy)*(*pxy))/w/w;
    }

    pc = gwy_data_field_get_data(meancurv);
    pg = gwy_data_field_get_data(gausscurv);
    pE = gwy_data_field_get_data(modulus);
    pz = gwy_data_field_get_data(dz);
    m = gwy_data_field_get_data(mask);

    coef = pow( 9./16 *F*F/R, 1./3);
    for (i = 0; i < xres*yres; i++, pc++, pg++, pE++, pz++, m++) {
        if (1 - 2*(*pc)*R + R*R*(*pg) <= 0) {
            *m = 1;
            *pE = -1;
            *pz = -1e-9;
        }
        else {
            *pE = E*pow(1 - 2*(*pc)*R + R*R*(*pg), -0.25);
            *pz = coef*pow(*pE, -2./3);
        }
    }

    gwy_data_field_data_changed(mask);
    gwy_data_field_data_changed(meancurv);
    gwy_data_field_data_changed(gausscurv);
    gwy_data_field_data_changed(modulus);
    gwy_data_field_data_changed(dz);

}


static void
hertz_modulus_do(GwyContainer *original_data, GwyContainer *result_data,
                 gint id)
{
    GwyDataField *meancurv, *gausscurv, *modulus, *deformation, *mask;
    gint newid;
    GString *str = g_string_new(NULL);


    /* mean curvature */
    meancurv = GWY_DATA_FIELD(gwy_container_get_object_by_name(result_data,
                                                               "/0/data"));
    mask = GWY_DATA_FIELD(gwy_container_get_object_by_name(result_data,
                                                           "/0/mask"));

    newid = gwy_app_data_browser_add_data_field(meancurv,
                                                original_data, TRUE);
    gwy_app_set_data_field_title(original_data, newid,
                                 _("Mean curvature"));
    g_string_printf(str, "/%d/mask", newid);
    gwy_container_set_object_by_name(original_data, str->str, mask);
    gwy_app_channel_log_add_proc(original_data, id, newid);
    id = newid;

    /* Gaussian curvature */
    gausscurv = GWY_DATA_FIELD(gwy_container_get_object_by_name(result_data,
                                                                "/1/data"));
    mask = GWY_DATA_FIELD(gwy_container_get_object_by_name(result_data,
                                                           "/0/mask"));

    newid = gwy_app_data_browser_add_data_field(gausscurv,
                                                original_data, TRUE);
    gwy_app_set_data_field_title(original_data, newid,
                                 _("Gaussian curvature"));
    g_string_printf(str, "/%d/mask", newid);
    gwy_container_set_object_by_name(original_data, str->str, mask);
    gwy_app_channel_log_add_proc(original_data, id, newid);
    id = newid;

    /* reduced modulus */
    modulus = GWY_DATA_FIELD(gwy_container_get_object_by_name(result_data,
                                                              "/2/data"));
    mask = GWY_DATA_FIELD(gwy_container_get_object_by_name(result_data,
                                                           "/0/mask"));

    newid = gwy_app_data_browser_add_data_field(modulus, original_data, TRUE);
    gwy_app_set_data_field_title(original_data, newid,
                                 _("Hertzian contact modulus"));
    g_string_printf(str, "/%d/mask", newid);
    gwy_container_set_object_by_name(original_data, str->str, mask);
    gwy_app_channel_log_add_proc(original_data, id, newid);

    /* deformation */
    deformation = GWY_DATA_FIELD(gwy_container_get_object_by_name(result_data,
                                                              "/3/data"));
    mask = GWY_DATA_FIELD(gwy_container_get_object_by_name(result_data,
                                                           "/0/mask"));

    newid = gwy_app_data_browser_add_data_field(deformation, original_data, TRUE);
    gwy_app_set_data_field_title(original_data, newid,
                                 _("Hertzian therory deformation"));
    g_string_printf(str, "/%d/mask", newid);
    gwy_container_set_object_by_name(original_data, str->str, mask);
    gwy_app_channel_log_add_proc(original_data, id, newid);
}

/* vim: set cin et ts=4 sw=4 cino=>1s,e0,n0,f0,{0,}0,^0,\:1s,=0,g1s,h0,t0,+1s,c3,(0,u0 : */
