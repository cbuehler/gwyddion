/*
 *  $Id$
 *  Copyright (C) 2017 David Necas (Yeti).
 *  E-mail: yeti@gwyddion.net.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA 02110-1301, USA.
 */

#include "config.h"
#include <gtk/gtk.h>
#include <libgwyddion/gwymacros.h>
#include <libgwyddion/gwymath.h>
#include <libprocess/gwyprocesstypes.h>
#include <libprocess/filters.h>
#include <libgwydgets/gwydgetutils.h>
#include <libgwydgets/gwystock.h>
#include <libgwymodule/gwymodule-process.h>
#include <app/gwyapp.h>
#include "preview.h"

#define BINNING_RUN_MODES (GWY_RUN_IMMEDIATE | GWY_RUN_INTERACTIVE)

typedef struct {
    gint binw;
    gint binh;
    gint xoff;
    gint yoff;
    gint trim_lowest;
    gint trim_highest;
    gboolean square_bin;
    gboolean trim_symm;
    gboolean is_sum;
    /* Interface only */
    gint xres;
    gint yres;
    gint max_binw;
    gint max_binh;
} BinningArgs;

typedef struct {
    BinningArgs *args;
    GtkObject *binw;
    GtkObject *binh;
    GtkObject *xoff;
    GtkObject *yoff;
    GtkObject *trim_lowest;
    GtkObject *trim_highest;
    GtkWidget *square_bin;
    GtkWidget *trim_symm;
    GtkWidget *is_sum;
    GtkWidget *newdims;
    gboolean in_update;
    gboolean last_edited_is_y;
} BinningControls;

static gboolean module_register      (void);
static void     binning              (GwyContainer *data,
                                      GwyRunType run);
static gboolean binning_dialog       (BinningArgs *args);
static void     square_bin_changed   (GtkToggleButton *toggle,
                                      BinningControls *controls);
static void     trim_symm_changed    (GtkToggleButton *toggle,
                                      BinningControls *controls);
static void     is_sum_changed       (GtkToggleButton *toggle,
                                      BinningControls *controls);
static void     update_binsize_upper (BinningControls *controls);
static void     update_trim_upper    (BinningControls *controls);
static void     update_new_dims      (BinningControls *controls);
static void     binh_changed         (GtkAdjustment *adj,
                                      BinningControls *controls);
static void     binw_changed         (GtkAdjustment *adj,
                                      BinningControls *controls);
static void     xoff_changed         (GtkAdjustment *adj,
                                      BinningControls *controls);
static void     yoff_changed         (GtkAdjustment *adj,
                                      BinningControls *controls);
static void     trim_lowest_changed  (GtkAdjustment *adj,
                                      BinningControls *controls);
static void     trim_highest_changed (GtkAdjustment *adj,
                                      BinningControls *controls);
static void     binning_sanitize_args(BinningArgs *args);
static void     binning_load_args    (GwyContainer *container,
                                      BinningArgs *args);
static void     binning_save_args    (GwyContainer *container,
                                      BinningArgs *args);

static const BinningArgs binning_defaults = {
    3, 3,
    0, 0,
    1, 1,
    TRUE, TRUE,
    FALSE,
    /* Interface only */
    0, 0, 0, 0,
};

static GwyModuleInfo module_info = {
    GWY_MODULE_ABI_VERSION,
    &module_register,
    N_("Creates a smaller image using binning."),
    "Yeti <yeti@gwyddion.net>",
    "1.0",
    "David Nečas (Yeti)",
    "2017",
};

GWY_MODULE_QUERY2(module_info, binning)

static gboolean
module_register(void)
{
    gwy_process_func_register("binning",
                              (GwyProcessFunc)&binning,
                              N_("/_Basic Operations/_Binning..."),
                              GWY_STOCK_BINNING,
                              BINNING_RUN_MODES,
                              GWY_MENU_FLAG_DATA,
                              N_("Reduce size by binning"));

    return TRUE;
}

static void
binning(GwyContainer *data, GwyRunType run)
{
    GwyDataField *dfields[3];
    GQuark quark;
    gint oldid, newid;
    BinningArgs args;
    gboolean ok;

    g_return_if_fail(run & BINNING_RUN_MODES);
    gwy_app_data_browser_get_current(GWY_APP_DATA_FIELD, dfields + 0,
                                     GWY_APP_MASK_FIELD, dfields + 1,
                                     GWY_APP_SHOW_FIELD, dfields + 2,
                                     GWY_APP_DATA_FIELD_ID, &oldid,
                                     0);
    g_return_if_fail(dfields[0]);

    binning_load_args(gwy_app_settings_get(), &args);
    args.xres = gwy_data_field_get_xres(dfields[0]);
    args.yres = gwy_data_field_get_yres(dfields[0]);
    args.max_binw = (args.xres-1)/2 + 1;
    args.max_binh = (args.yres-1)/2 + 1;
    args.binw = MIN(args.binw, args.max_binw);
    args.binh = MIN(args.binh, args.max_binh);
    args.xoff = MIN(args.xoff, args.binw-1);
    args.yoff = MIN(args.yoff, args.binh-1);
    if (args.trim_lowest + args.trim_highest >= args.binw*args.binh) {
        args.trim_lowest = 0;
        args.trim_highest = 0;
    }

    if (run == GWY_RUN_INTERACTIVE) {
        ok = binning_dialog(&args);
        binning_save_args(gwy_app_settings_get(), &args);
        if (!ok)
            return;
    }

    dfields[0] = gwy_data_field_new_binned(dfields[0],
                                           args.binw, args.binh,
                                           args.xoff, args.yoff,
                                           args.trim_lowest,
                                           args.trim_highest);
    /* Only apply is_sum to data.  We do not want to sum mask, and it is
     * pointless to do it with presentation either. */
    if (args.is_sum)
        gwy_data_field_multiply(dfields[0], args.binw*args.binh);

    if (dfields[1]) {
        dfields[1] = gwy_data_field_new_binned(dfields[1],
                                               args.binw, args.binh,
                                               args.xoff, args.yoff,
                                               args.trim_lowest,
                                               args.trim_highest);
        gwy_data_field_threshold(dfields[1], 0.5, 0.0, 1.0);
    }

    if (dfields[2]) {
        dfields[2] = gwy_data_field_new_binned(dfields[2],
                                               args.binw, args.binh,
                                               args.xoff, args.yoff,
                                               args.trim_lowest,
                                               args.trim_highest);
    }

    newid = gwy_app_data_browser_add_data_field(dfields[0], data, TRUE);
    g_object_unref(dfields[0]);
    gwy_app_sync_data_items(data, data, oldid, newid, FALSE,
                            GWY_DATA_ITEM_GRADIENT,
                            GWY_DATA_ITEM_RANGE,
                            GWY_DATA_ITEM_MASK_COLOR,
                            0);
    if (dfields[1]) {
        quark = gwy_app_get_mask_key_for_id(newid);
        gwy_container_set_object(data, quark, dfields[1]);
        g_object_unref(dfields[1]);
    }
    if (dfields[2]) {
        quark = gwy_app_get_show_key_for_id(newid);
        gwy_container_set_object(data, quark, dfields[2]);
        g_object_unref(dfields[2]);
    }

    gwy_app_set_data_field_title(data, newid, _("Binned Data"));
    gwy_app_channel_log_add_proc(data, oldid, newid);
}

static gboolean
binning_dialog(BinningArgs *args)
{
    GtkWidget *dialog, *table, *label;
    BinningControls controls;
    gint response, row, binsize;

    gwy_clear(&controls, 1);
    controls.args = args;

    dialog = gtk_dialog_new_with_buttons(_("Binning"), NULL, 0,
                                         _("_Reset"), RESPONSE_RESET,
                                         GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
                                         GTK_STOCK_OK, GTK_RESPONSE_OK,
                                         NULL);
    gtk_dialog_set_default_response(GTK_DIALOG(dialog), GTK_RESPONSE_OK);
    gwy_help_add_to_proc_dialog(GTK_DIALOG(dialog), GWY_HELP_DEFAULT);

    table = gtk_table_new(12, 3, FALSE);
    gtk_table_set_row_spacings(GTK_TABLE(table), 2);
    gtk_table_set_col_spacings(GTK_TABLE(table), 6);
    gtk_container_set_border_width(GTK_CONTAINER(table), 4);
    gtk_box_pack_start(GTK_BOX(GTK_DIALOG(dialog)->vbox), table,
                       FALSE, FALSE, 4);
    row = 0;

    gtk_table_attach(GTK_TABLE(table),
                     gwy_label_new_header(_("Bin Dimensions")),
                     0, 3, row, row+1, GTK_FILL, 0, 0, 0);
    row++;

    controls.binw = gtk_adjustment_new(args->binw, 1, args->max_binw, 1, 10, 0);
    gwy_table_attach_adjbar(table, row++, _("_Width:"), _("px"),
                            controls.binw, GWY_HSCALE_LOG | GWY_HSCALE_SNAP);
    g_signal_connect(controls.binw, "value-changed",
                     G_CALLBACK(binw_changed), &controls);

    controls.binh = gtk_adjustment_new(args->binh, 1, args->max_binh, 1, 10, 0);
    gwy_table_attach_adjbar(table, row++, _("_Height:"), _("px"),
                            controls.binh, GWY_HSCALE_LOG | GWY_HSCALE_SNAP);
    g_signal_connect(controls.binh, "value-changed",
                     G_CALLBACK(binh_changed), &controls);

    controls.square_bin
        = gtk_check_button_new_with_mnemonic(_("_Square bin"));
    gtk_table_attach(GTK_TABLE(table), controls.square_bin, 0, 3, row, row+1,
                     GTK_FILL, 0, 0, 0);
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(controls.square_bin),
                                 args->square_bin);
    g_signal_connect(controls.square_bin, "toggled",
                     G_CALLBACK(square_bin_changed), &controls);
    row++;

    label = controls.newdims = gtk_label_new(NULL);
    gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);
    gtk_table_attach(GTK_TABLE(table), label, 0, 2, row, row+1,
                     GTK_FILL, 0, 0, 0);
    row++;

    gtk_table_set_row_spacing(GTK_TABLE(table), row-1, 8);
    controls.xoff = gtk_adjustment_new(args->xoff, 0, args->binw-1, 1, 10, 0);
    gwy_table_attach_adjbar(table, row++, _("_X offset:"), _("px"),
                            controls.xoff, GWY_HSCALE_LINEAR | GWY_HSCALE_SNAP);
    g_signal_connect(controls.xoff, "value-changed",
                     G_CALLBACK(xoff_changed), &controls);

    controls.yoff = gtk_adjustment_new(args->yoff, 0, args->binh-1, 1, 10, 0);
    gwy_table_attach_adjbar(table, row++, _("_Y offset:"), _("px"),
                            controls.yoff, GWY_HSCALE_LINEAR | GWY_HSCALE_SNAP);
    g_signal_connect(controls.yoff, "value-changed",
                     G_CALLBACK(yoff_changed), &controls);

    gtk_table_set_row_spacing(GTK_TABLE(table), row-1, 8);
    gtk_table_attach(GTK_TABLE(table),
                     gwy_label_new_header(_("Options")),
                     0, 3, row, row+1, GTK_FILL, 0, 0, 0);
    row++;

    binsize = args->binw*args->binh;
    controls.trim_lowest = gtk_adjustment_new(args->trim_lowest,
                                              0, binsize-1, 1, 10, 0);
    gwy_table_attach_adjbar(table, row++, _("Trim _lowest:"), NULL,
                            controls.trim_lowest,
                            GWY_HSCALE_LINEAR | GWY_HSCALE_SNAP);
    g_signal_connect(controls.trim_lowest, "value-changed",
                     G_CALLBACK(trim_lowest_changed), &controls);

    controls.trim_highest = gtk_adjustment_new(args->trim_highest,
                                               0, binsize-1, 1, 10, 0);
    gwy_table_attach_adjbar(table, row++, _("Trim hi_ghest:"), NULL,
                            controls.trim_highest,
                            GWY_HSCALE_LINEAR | GWY_HSCALE_SNAP);
    g_signal_connect(controls.trim_highest, "value-changed",
                     G_CALLBACK(trim_highest_changed), &controls);

    controls.trim_symm
        = gtk_check_button_new_with_mnemonic(_("_Trim symmetrically"));
    gtk_table_attach(GTK_TABLE(table), controls.trim_symm, 0, 3, row, row+1,
                     GTK_FILL, 0, 0, 0);
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(controls.trim_symm),
                                 args->trim_symm);
    g_signal_connect(controls.trim_symm, "toggled",
                     G_CALLBACK(trim_symm_changed), &controls);
    row++;

    controls.is_sum
        = gtk_check_button_new_with_mnemonic(_("_Sum instead of averaging"));
    gtk_table_attach(GTK_TABLE(table), controls.is_sum, 0, 3, row, row+1,
                     GTK_FILL, 0, 0, 0);
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(controls.is_sum),
                                 args->is_sum);
    g_signal_connect(controls.is_sum, "toggled",
                     G_CALLBACK(is_sum_changed), &controls);
    row++;

    update_binsize_upper(&controls);
    update_trim_upper(&controls);
    update_new_dims(&controls);
    gtk_widget_show_all(dialog);

    do {
        response = gtk_dialog_run(GTK_DIALOG(dialog));
        switch (response) {
            case GTK_RESPONSE_CANCEL:
            case GTK_RESPONSE_DELETE_EVENT:
            gtk_widget_destroy(dialog);
            case GTK_RESPONSE_NONE:
            return FALSE;
            break;

            case GTK_RESPONSE_OK:
            break;

            case RESPONSE_RESET:
            /* TODO */
            break;

            default:
            g_assert_not_reached();
            break;
        }
    } while (response != GTK_RESPONSE_OK);

    gtk_widget_destroy(dialog);

    return TRUE;
}

static void
square_bin_changed(GtkToggleButton *toggle, BinningControls *controls)
{
    BinningArgs *args = controls->args;
    GtkAdjustment *binwadj = GTK_ADJUSTMENT(controls->binw);
    GtkAdjustment *binhadj = GTK_ADJUSTMENT(controls->binh);

    args->square_bin = gtk_toggle_button_get_active(toggle);
    if (controls->in_update)
        return;

    if (args->square_bin && args->binw != args->binh) {
        controls->in_update = TRUE;
        if (args->binw > args->max_binh)
            gtk_adjustment_set_value(binwadj, args->binh);
        else if (args->binh > args->max_binw)
            gtk_adjustment_set_value(binhadj, args->binw);
        else if (controls->last_edited_is_y)
            gtk_adjustment_set_value(binwadj, args->binh);
        else
            gtk_adjustment_set_value(binhadj, args->binw);
        controls->in_update = FALSE;
    }
    update_binsize_upper(controls);
}

static void
trim_symm_changed(GtkToggleButton *toggle, BinningControls *controls)
{
    BinningArgs *args = controls->args;
    GtkAdjustment *lowestadj = GTK_ADJUSTMENT(controls->trim_lowest);
    GtkAdjustment *highestadj = GTK_ADJUSTMENT(controls->trim_highest);
    gint m;

    args->trim_symm = gtk_toggle_button_get_active(toggle);
    if (controls->in_update)
        return;

    if (args->trim_symm) {
        m = MIN(args->trim_lowest, args->trim_highest);
        controls->in_update = TRUE;
        gtk_adjustment_set_value(lowestadj, m);
        gtk_adjustment_set_value(highestadj, m);
        controls->in_update = FALSE;
    }
    update_trim_upper(controls);
}

static void
is_sum_changed(GtkToggleButton *toggle, BinningControls *controls)
{
    BinningArgs *args = controls->args;

    args->is_sum = gtk_toggle_button_get_active(toggle);
}

static void
update_binsize_upper(BinningControls *controls)
{
    BinningArgs *args = controls->args;
    gdouble m;  /* The property is double. */

    if (args->square_bin) {
        m = MIN(args->max_binw, args->max_binh);
        g_object_set(controls->binw, "upper", m, NULL);
        g_object_set(controls->binh, "upper", m, NULL);
    }
    else {
        g_object_set(controls->binw, "upper", 1.0*args->max_binw, NULL);
        g_object_set(controls->binh, "upper", 1.0*args->max_binh, NULL);
    }
}

static void
update_trim_upper(BinningControls *controls)
{
    BinningArgs *args = controls->args;
    gint binsize;
    gdouble m;  /* The property is double. */

    binsize = args->binw*args->binh;
    m = args->trim_symm ? (binsize-1)/2 : binsize-1;
    g_object_set(controls->trim_lowest, "upper", m, NULL);
    g_object_set(controls->trim_highest, "upper", m, NULL);
}

static void
update_new_dims(BinningControls *controls)
{
    BinningArgs *args = controls->args;
    gchar *s;

    s = g_strdup_printf(_("New dimensions: %d × %d pixels"),
                        (args->xres - args->xoff)/args->binw,
                        (args->yres - args->yoff)/args->binh);
    gtk_label_set_text(GTK_LABEL(controls->newdims), s);
    g_free(s);
}

static void
binw_changed(GtkAdjustment *adj, BinningControls *controls)
{
    BinningArgs *args = controls->args;
    gint binsize;
    gdouble m;  /* The property is double. */

    args->binw = gwy_adjustment_get_int(adj);
    binsize = args->binw*args->binh;
    if (args->xoff > args->binw-1)
        gtk_adjustment_set_value(GTK_ADJUSTMENT(controls->xoff), args->binw-1);
    if (args->trim_lowest + args->trim_highest >= binsize) {
        gtk_adjustment_set_value(GTK_ADJUSTMENT(controls->trim_lowest), 0);
        gtk_adjustment_set_value(GTK_ADJUSTMENT(controls->trim_highest), 0);
    }
    m = args->binw-1;
    g_object_set(controls->xoff, "upper", m, NULL);
    update_trim_upper(controls);
    update_new_dims(controls);

    if (controls->in_update)
        return;

    if (args->square_bin)
        gtk_adjustment_set_value(GTK_ADJUSTMENT(controls->binh), args->binw);
    controls->last_edited_is_y = FALSE;
}

static void
binh_changed(GtkAdjustment *adj, BinningControls *controls)
{
    BinningArgs *args = controls->args;
    gint binsize;
    gdouble m;  /* The property is double. */

    args->binh = gwy_adjustment_get_int(adj);
    binsize = args->binw*args->binh;
    if (args->yoff > args->binh-1)
        gtk_adjustment_set_value(GTK_ADJUSTMENT(controls->yoff), args->binh-1);
    if (args->trim_lowest + args->trim_highest >= binsize) {
        gtk_adjustment_set_value(GTK_ADJUSTMENT(controls->trim_lowest), 0);
        gtk_adjustment_set_value(GTK_ADJUSTMENT(controls->trim_highest), 0);
    }
    m = args->binh-1;
    g_object_set(controls->yoff, "upper", m, NULL);
    update_trim_upper(controls);
    update_new_dims(controls);

    if (controls->in_update)
        return;

    if (args->square_bin)
        gtk_adjustment_set_value(GTK_ADJUSTMENT(controls->binw), args->binh);
    controls->last_edited_is_y = TRUE;
}

static void
xoff_changed(GtkAdjustment *adj, BinningControls *controls)
{
    BinningArgs *args = controls->args;

    args->xoff = gwy_adjustment_get_int(adj);
    update_new_dims(controls);
}

static void
yoff_changed(GtkAdjustment *adj, BinningControls *controls)
{
    BinningArgs *args = controls->args;

    args->yoff = gwy_adjustment_get_int(adj);
    update_new_dims(controls);
}

static void
trim_lowest_changed(GtkAdjustment *adj, BinningControls *controls)
{
    BinningArgs *args = controls->args;
    gint binsize;

    args->trim_lowest = gwy_adjustment_get_int(adj);
    if (controls->in_update)
        return;

    binsize = args->binw*args->binh;
    controls->in_update = TRUE;
    if (args->trim_symm) {
        gtk_adjustment_set_value(GTK_ADJUSTMENT(controls->trim_highest),
                                 args->trim_lowest);
    }
    else if (args->trim_lowest + args->trim_highest >= binsize) {
        gtk_adjustment_set_value(GTK_ADJUSTMENT(controls->trim_highest),
                                 binsize-1 - args->trim_lowest);
    }
    controls->in_update = FALSE;
}

static void
trim_highest_changed(GtkAdjustment *adj, BinningControls *controls)
{
    BinningArgs *args = controls->args;
    gint binsize;

    args->trim_highest = gwy_adjustment_get_int(adj);
    if (controls->in_update)
        return;

    binsize = args->binw*args->binh;
    controls->in_update = TRUE;
    if (args->trim_symm) {
        gtk_adjustment_set_value(GTK_ADJUSTMENT(controls->trim_lowest),
                                 args->trim_highest);
    }
    else if (args->trim_lowest + args->trim_highest >= binsize) {
        gtk_adjustment_set_value(GTK_ADJUSTMENT(controls->trim_lowest),
                                 binsize-1 - args->trim_highest);
    }
    controls->in_update = FALSE;
}

static const gchar binh_key[]         = "/module/binning/binh";
static const gchar binw_key[]         = "/module/binning/binw";
static const gchar is_sum_key[]       = "/module/binning/is_sum";
static const gchar square_bin_key[]   = "/module/binning/square_bin";
static const gchar trim_highest_key[] = "/module/binning/trim_highest";
static const gchar trim_lowest_key[]  = "/module/binning/trim_lowest";
static const gchar trim_symm_key[]    = "/module/binning/trim_symm";
static const gchar xoff_key[]         = "/module/binning/xoff";
static const gchar yoff_key[]         = "/module/binning/yoff";

static void
binning_sanitize_args(BinningArgs *args)
{
    /* We need sanitize with respect to the image. */
    if (args->binw != args->binh)
        args->square_bin = FALSE;
    if (args->trim_lowest != args->trim_highest)
        args->square_bin = FALSE;
    args->xoff = MAX(args->xoff, 0);
    args->yoff = MAX(args->yoff, 0);
    args->binw = MAX(args->binw, 1);
    args->binh = MAX(args->binh, 1);
    args->trim_lowest = MAX(args->trim_lowest, 0);
    args->trim_highest = MAX(args->trim_highest, 0);
    args->is_sum = !!args->is_sum;
}

static void
binning_load_args(GwyContainer *container,
                BinningArgs *args)
{
    *args = binning_defaults;

    gwy_container_gis_int32_by_name(container, binw_key, &args->binw);
    gwy_container_gis_int32_by_name(container, binh_key, &args->binh);
    gwy_container_gis_int32_by_name(container, xoff_key, &args->xoff);
    gwy_container_gis_int32_by_name(container, yoff_key, &args->yoff);
    gwy_container_gis_int32_by_name(container, trim_lowest_key,
                                    &args->trim_lowest);
    gwy_container_gis_int32_by_name(container, trim_highest_key,
                                    &args->trim_highest);
    gwy_container_gis_boolean_by_name(container, trim_symm_key,
                                      &args->trim_symm);
    gwy_container_gis_boolean_by_name(container, square_bin_key,
                                      &args->square_bin);
    gwy_container_gis_boolean_by_name(container, is_sum_key,
                                      &args->is_sum);
    binning_sanitize_args(args);
}

static void
binning_save_args(GwyContainer *container,
                BinningArgs *args)
{
    gwy_container_set_int32_by_name(container, binw_key, args->binw);
    gwy_container_set_int32_by_name(container, binh_key, args->binh);
    gwy_container_set_int32_by_name(container, xoff_key, args->xoff);
    gwy_container_set_int32_by_name(container, yoff_key, args->yoff);
    gwy_container_set_int32_by_name(container, trim_lowest_key,
                                    args->trim_lowest);
    gwy_container_set_int32_by_name(container, trim_highest_key,
                                    args->trim_highest);
    gwy_container_set_boolean_by_name(container, trim_symm_key,
                                      args->trim_symm);
    gwy_container_set_boolean_by_name(container, square_bin_key,
                                      args->square_bin);
    gwy_container_set_boolean_by_name(container, is_sum_key,
                                      args->is_sum);
}

/* vim: set cin et ts=4 sw=4 cino=>1s,e0,n0,f0,{0,}0,^0,\:1s,=0,g1s,h0,t0,+1s,c3,(0,u0 : */
