/*
 *  $Id$
 *  Copyright (C) 2014-2017 David Necas (Yeti), Petr Klapetek, Sven Neumann.
 *  E-mail: yeti@gwyddion.net, klapetek@gwyddion.net, neumann@jpk.com.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA 02110-1301, USA.
 */

#include "config.h"
#include <string.h>
#include <stdlib.h>
#include <gtk/gtk.h>
#include <libgwyddion/gwymacros.h>
#include <libgwyddion/gwymath.h>
#include <libgwyddion/gwyresults.h>
#include <libprocess/grains.h>
#include <libprocess/linestats.h>
#include <libgwydgets/gwydgetutils.h>
#include <libgwydgets/gwyradiobuttons.h>
#include <libgwydgets/gwygrainvaluemenu.h>
#include <libgwydgets/gwystock.h>
#include <libgwymodule/gwymodule-process.h>
#include <app/gwyapp.h>
#include <app/gwymoduleutils.h>

#define STATS_RUN_MODES GWY_RUN_INTERACTIVE
#define INSCRIBE_RUN_MODES GWY_RUN_IMMEDIATE

typedef struct {
    gdouble mean;
    gdouble median;
    gdouble rms;
    gdouble q25;
    gdouble q75;
    GwySIValueFormat *vf;
} GrainQuantityStats;

typedef struct {
    GwyResultsReportType report_style;
} GrainSummaryArgs;

typedef struct {
    GwyResultsReportType report_style;
    guint expanded;
} GrainStatArgs;

typedef struct {
    GrainStatArgs *args;
    GrainQuantityStats *stats;
    gboolean same_units;
} GrainStatsControls;

static gboolean            module_register        (void);
static void                grain_summary          (GwyContainer *data,
                                                   GwyRunType run);
static void                grain_stat             (GwyContainer *data,
                                                   GwyRunType run);
static void                grain_inscribe_discs   (GwyContainer *data,
                                                   GwyRunType run);
static void                grain_exscribe_circles (GwyContainer *data,
                                                   GwyRunType run);
static GrainQuantityStats* calculate_stats        (GwyDataField *dfield,
                                                   GwyDataField *mfield);
static gdouble             calc_average           (gdouble *values,
                                                   guint n);
static gdouble             calc_rms               (const gdouble *values,
                                                   guint n,
                                                   gdouble mean);
static gdouble             calc_semicirc_average  (const gdouble *angles,
                                                   guint n);
static gdouble             calc_semicirc_rms      (const gdouble *angles,
                                                   guint n,
                                                   gdouble mean);
static gdouble             calc_semicirc_median   (gdouble *angles,
                                                   guint n,
                                                   guint *medpos);
static void                calc_semicirc_quartiles(const gdouble *angles,
                                                   guint n,
                                                   guint medpos,
                                                   gdouble *q25,
                                                   gdouble *q75);
static void                render_grain_stat      (GtkTreeViewColumn *column,
                                                   GtkCellRenderer *renderer,
                                                   GtkTreeModel *model,
                                                   GtkTreeIter *iter,
                                                   gpointer user_data);
static void                summary_load_args      (GwyContainer *container,
                                                   GrainSummaryArgs *args);
static void                summary_save_args      (GwyContainer *container,
                                                   GrainSummaryArgs *args);
static void                stat_load_args         (GwyContainer *container,
                                                   GrainStatArgs *args);
static void                stat_save_args         (GwyContainer *container,
                                                   GrainStatArgs *args);

static const GrainSummaryArgs grain_summary_defaults = {
    GWY_RESULTS_REPORT_COLON,
};

static const GrainStatArgs grain_stat_defaults = {
    GWY_RESULTS_REPORT_COLON, 0,
};

static GwyModuleInfo module_info = {
    GWY_MODULE_ABI_VERSION,
    &module_register,
    N_("Displays overall grain statistics."),
    "Petr Klapetek <petr@klapetek.cz>, Sven Neumann <neumann@jpk.com>, "
        "Yeti <yeti@gwyddion.net>",
    "1.3",
    "David Nečas (Yeti) & Petr Klapetek & Sven Neumann",
    "2015",
};

GWY_MODULE_QUERY2(module_info, grain_stat)

static gboolean
module_register(void)
{
    gwy_process_func_register("grain_summary",
                              (GwyProcessFunc)&grain_summary,
                              N_("/_Grains/S_ummary..."),
                              NULL,
                              STATS_RUN_MODES,
                              GWY_MENU_FLAG_DATA | GWY_MENU_FLAG_DATA_MASK,
                              N_("Grain summary information"));
    gwy_process_func_register("grain_stat",
                              (GwyProcessFunc)&grain_stat,
                              N_("/_Grains/S_tatistics..."),
                              GWY_STOCK_GRAINS_STATISTICS,
                              STATS_RUN_MODES,
                              GWY_MENU_FLAG_DATA | GWY_MENU_FLAG_DATA_MASK,
                              N_("Grain property statistics"));
    gwy_process_func_register("grain_inscribe_discs",
                              (GwyProcessFunc)&grain_inscribe_discs,
                              N_("/_Grains/Select _Inscribed Discs"),
                              GWY_STOCK_GRAIN_INSCRIBED_CIRCLE,
                              INSCRIBE_RUN_MODES,
                              GWY_MENU_FLAG_DATA | GWY_MENU_FLAG_DATA_MASK,
                              N_("Create a selection visualizing discs "
                                 "inscribed into grains"));
    gwy_process_func_register("grain_exscribe_circles",
                              (GwyProcessFunc)&grain_exscribe_circles,
                              N_("/_Grains/Select _Circumscribed Circles"),
                              GWY_STOCK_GRAIN_EXSCRIBED_CIRCLE,
                              INSCRIBE_RUN_MODES,
                              GWY_MENU_FLAG_DATA | GWY_MENU_FLAG_DATA_MASK,
                              N_("Create a selection visualizing grain "
                                 "circumcircles"));

    return TRUE;
}

static gdouble
grains_get_total_value(GwyDataField *dfield,
                       gint ngrains,
                       const gint *grains,
                       gdouble **values,
                       GwyGrainQuantity quantity)
{
    gint i;
    gdouble sum;

    *values = gwy_data_field_grains_get_values(dfield, *values, ngrains, grains,
                                               quantity);
    sum = 0.0;
    for (i = 1; i <= ngrains; i++)
        sum += (*values)[i];

    return sum;
}

static void
grain_summary(GwyContainer *data, GwyRunType run)
{
    const gchar *guivalues[] = {
        "ngrains",
        "area", "relarea", "meanarea",
        "meansize",
        "vol_0", "vol_min", "vol_laplace",
        "bound_len",
    };

    GrainSummaryArgs args;
    GtkWidget *dialog, *table, *label, *rexport;
    GwyDataField *dfield, *mfield;
    GwyResults *results;
    gint xres, yres, ngrains, id, row;
    gdouble area, size, vol_0, vol_min, vol_laplace, bound_len, xreal, yreal;
    GString *str;
    gdouble *values;
    gint *grains;
    const gchar *key;
    guint i;

    g_return_if_fail(run & STATS_RUN_MODES);
    summary_load_args(gwy_app_settings_get(), &args);
    gwy_app_data_browser_get_current(GWY_APP_DATA_FIELD, &dfield,
                                     GWY_APP_MASK_FIELD, &mfield,
                                     GWY_APP_DATA_FIELD_ID, &id,
                                     0);
    g_return_if_fail(dfield);
    g_return_if_fail(mfield);

    results = gwy_results_new();
    gwy_results_add_header(results, N_("Grain Summary"));
    gwy_results_add_value_str(results, "file", N_("File"));
    gwy_results_add_value_str(results, "image", N_("Image"));
    gwy_results_add_separator(results);
    gwy_results_add_value_int(results, "ngrains", N_("Number of grains"));
    gwy_results_add_value(results, "area",
                          N_("Total projected area (abs.)"),
                          "power-x", 1, "power-y", 1,
                          NULL);
    gwy_results_add_value_percents(results, "relarea",
                                   _("Total projected area (rel.)"));
    gwy_results_add_value(results, "meanarea",
                          N_("Mean grain area"),
                          "power-x", 1, "power-y", 1,
                          NULL);
    gwy_results_add_value_x(results, "meansize", N_("Mean grain size"));
    gwy_results_add_value(results, "vol_0",
                          N_("Total grain volume (zero)"),
                          "power-x", 1, "power-y", 1, "power-z", 1,
                          NULL);
    gwy_results_add_value(results, "vol_min",
                          N_("Total grain volume (minimum)"),
                          "power-x", 1, "power-y", 1, "power-z", 1,
                          NULL);
    gwy_results_add_value(results, "vol_laplace",
                          N_("Total grain volume (Laplace)"),
                          "power-x", 1, "power-y", 1, "power-z", 1,
                          NULL);
    gwy_results_add_value_x(results, "bound_len",
                            N_("Total projected boundary length"));

    gwy_results_set_unit(results, "x", gwy_data_field_get_si_unit_xy(dfield));
    gwy_results_set_unit(results, "y", gwy_data_field_get_si_unit_xy(dfield));
    gwy_results_set_unit(results, "z", gwy_data_field_get_si_unit_z(dfield));

    gwy_results_fill_filename(results, "file", data);
    gwy_results_fill_channel(results, "image", data, id);

    xres = gwy_data_field_get_xres(mfield);
    yres = gwy_data_field_get_yres(mfield);
    xreal = gwy_data_field_get_xreal(dfield);
    yreal = gwy_data_field_get_yreal(dfield);

    grains = g_new0(gint, xres*yres);
    ngrains = gwy_data_field_number_grains(mfield, grains);
    values = NULL;
    area = grains_get_total_value(dfield, ngrains, grains, &values,
                                  GWY_GRAIN_VALUE_PROJECTED_AREA);
    size = grains_get_total_value(dfield, ngrains, grains, &values,
                                  GWY_GRAIN_VALUE_EQUIV_SQUARE_SIDE);
    vol_0 = grains_get_total_value(dfield, ngrains, grains, &values,
                                   GWY_GRAIN_VALUE_VOLUME_0);
    vol_min = grains_get_total_value(dfield, ngrains, grains, &values,
                                     GWY_GRAIN_VALUE_VOLUME_MIN);
    vol_laplace = grains_get_total_value(dfield, ngrains, grains, &values,
                                         GWY_GRAIN_VALUE_VOLUME_LAPLACE);
    bound_len = grains_get_total_value(dfield, ngrains, grains, &values,
                                       GWY_GRAIN_VALUE_FLAT_BOUNDARY_LENGTH);
    g_free(values);
    g_free(grains);

    gwy_results_fill_values(results,
                            "ngrains", ngrains,
                            "area", area,
                            "relarea", area/(xreal*yreal),
                            "meanarea", area/ngrains,
                            "meansize", size/ngrains,
                            "vol_0", vol_0,
                            "vol_min", vol_min,
                            "vol_laplace", vol_laplace,
                            "bound_len", bound_len,
                            NULL);

    dialog = gtk_dialog_new_with_buttons(_("Grain Summary"), NULL, 0,
                                         GTK_STOCK_CLOSE, GTK_RESPONSE_CLOSE,
                                         NULL);
    gwy_help_add_to_proc_dialog(GTK_DIALOG(dialog), GWY_HELP_DEFAULT);

    table = gtk_table_new(G_N_ELEMENTS(guivalues), 2, FALSE);
    gtk_table_set_col_spacings(GTK_TABLE(table), 6);
    gtk_table_set_row_spacings(GTK_TABLE(table), 2);
    gtk_container_add(GTK_CONTAINER(GTK_DIALOG(dialog)->vbox), table);
    gtk_container_set_border_width(GTK_CONTAINER(table), 4);
    row = 0;

    str = g_string_new(NULL);
    for (i = 0; i < G_N_ELEMENTS(guivalues); i++) {
        key = guivalues[i];
        g_string_assign(str, gwy_results_get_label(results, key));
        g_string_append_c(str, ':');
        label = gtk_label_new(str->str);
        gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);
        gtk_table_attach(GTK_TABLE(table), label, 0, 1, row, row+1,
                         GTK_FILL, 0, 0, 0);

        label = gtk_label_new(NULL);
        gtk_label_set_markup(GTK_LABEL(label),
                             gwy_results_get_full(results, key));
        gtk_misc_set_alignment(GTK_MISC(label), 1.0, 0.5);
        gtk_label_set_selectable(GTK_LABEL(label), TRUE);
        gtk_table_attach(GTK_TABLE(table), label, 1, 2, row, row+1,
                         GTK_EXPAND | GTK_FILL, 0, 0, 0);
        row++;
    }
    g_string_free(str, TRUE);

    rexport = gwy_results_export_new(args.report_style);
    gwy_results_export_set_title(GWY_RESULTS_EXPORT(rexport),
                                 _("Save Grain Summary"));
    gwy_results_export_set_results(GWY_RESULTS_EXPORT(rexport), results);
    gtk_box_pack_start(GTK_BOX(GTK_DIALOG(dialog)->vbox), rexport,
                       FALSE, FALSE, 0);

    gtk_widget_show_all(dialog);
    gtk_dialog_run(GTK_DIALOG(dialog));
    args.report_style
        = gwy_results_export_get_format(GWY_RESULTS_EXPORT(rexport));
    gtk_widget_destroy(dialog);
    g_object_unref(results);

    summary_save_args(gwy_app_settings_get(), &args);
}

static void
grain_stat(G_GNUC_UNUSED GwyContainer *data, GwyRunType run)
{
    static const gchar *columns[] = {
        N_("Mean"), N_("Median"), N_("RMS"), N_("IQR"), N_("Units"),
    };

    GtkWidget *dialog, *scwin, *treeview;
    GwyDataField *dfield, *mfield;
    GwySIUnit *siunitxy, *siunitz;
    GtkTreeSelection *selection;
    GtkTreeViewColumn *column;
    GtkCellRenderer *renderer;
    GrainStatsControls controls;
    GrainStatArgs args;
    guint n, i;
    gint id;

    g_return_if_fail(run & STATS_RUN_MODES);
    stat_load_args(gwy_app_settings_get(), &args);
    gwy_app_data_browser_get_current(GWY_APP_DATA_FIELD, &dfield,
                                     GWY_APP_MASK_FIELD, &mfield,
                                     GWY_APP_DATA_FIELD_ID, &id,
                                     0);
    g_return_if_fail(dfield);
    g_return_if_fail(mfield);

    siunitxy = gwy_data_field_get_si_unit_xy(dfield);
    siunitz = gwy_data_field_get_si_unit_z(dfield);
    controls.args = &args;
    controls.same_units = gwy_si_unit_equal(siunitxy, siunitz);
    controls.stats = calculate_stats(dfield, mfield);

    dialog = gtk_dialog_new_with_buttons(_("Grain Summary"), NULL, 0,
                                         GTK_STOCK_CLOSE, GTK_RESPONSE_CLOSE,
                                         NULL);
    gtk_window_set_default_size(GTK_WINDOW(dialog), 640, 640);
    gwy_help_add_to_proc_dialog(GTK_DIALOG(dialog), GWY_HELP_DEFAULT);

    scwin = gtk_scrolled_window_new(NULL, NULL);
    gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(scwin),
                                   GTK_POLICY_NEVER, GTK_POLICY_AUTOMATIC);
    gtk_box_pack_start(GTK_BOX(GTK_DIALOG(dialog)->vbox), scwin, TRUE, TRUE, 0);

    treeview = gwy_grain_value_tree_view_new(FALSE,
                                             "name", "symbol_markup", NULL);
    gtk_tree_view_set_headers_visible(GTK_TREE_VIEW(treeview), TRUE);
    gtk_container_add(GTK_CONTAINER(scwin), treeview);

    renderer = gtk_cell_renderer_text_new();
    g_object_set(renderer, "xalign", 1.0, NULL);
    for (i = 0; i < G_N_ELEMENTS(columns); i++) {
        column = gtk_tree_view_column_new();
        g_object_set_data(G_OBJECT(column), "id", GUINT_TO_POINTER(i));
        gtk_tree_view_column_set_sizing(column, GTK_TREE_VIEW_COLUMN_AUTOSIZE);
        gtk_tree_view_append_column(GTK_TREE_VIEW(treeview), column);
        gtk_tree_view_column_set_title(column, columns[i]);
        gtk_tree_view_column_set_alignment(column, 0.5);
        gtk_tree_view_column_pack_start(column, renderer, TRUE);
        gtk_tree_view_column_set_cell_data_func(column, renderer,
                                                render_grain_stat, &controls,
                                                NULL);
    }

    selection = gtk_tree_view_get_selection(GTK_TREE_VIEW(treeview));
    gtk_tree_selection_set_mode(selection, GTK_SELECTION_NONE);

    gwy_grain_value_tree_view_set_expanded_groups(GTK_TREE_VIEW(treeview),
                                                  args.expanded);

    gtk_widget_show_all(dialog);
    gtk_dialog_run(GTK_DIALOG(dialog));
    args.expanded = gwy_grain_value_tree_view_get_expanded_groups
                                                     (GTK_TREE_VIEW(treeview));
    gtk_widget_destroy(dialog);

    stat_save_args(gwy_app_settings_get(), &args);

    n = gwy_inventory_get_n_items(gwy_grain_values());
    for (i = 0; i < n; i++)
        gwy_si_unit_value_format_free(controls.stats[i].vf);
    g_free(controls.stats);
}

static void
render_grain_stat(GtkTreeViewColumn *column,
                  GtkCellRenderer *renderer,
                  GtkTreeModel *model,
                  GtkTreeIter *iter,
                  gpointer user_data)
{
    GrainStatsControls *controls = (GrainStatsControls*)user_data;
    const GrainQuantityStats *stat;
    GwyGrainValue *gvalue = NULL;
    gdouble value;
    gchar buf[64];
    const gchar *name;
    gint i, id;

    gtk_tree_model_get(model, iter,
                       GWY_GRAIN_VALUE_STORE_COLUMN_ITEM, &gvalue,
                       -1);
    if (!gvalue) {
        g_object_set(renderer, "text", "", NULL);
        return;
    }

    g_object_unref(gvalue);
    if (!controls->same_units
        && (gwy_grain_value_get_flags(gvalue) & GWY_GRAIN_VALUE_SAME_UNITS)) {
        g_object_set(renderer, "text", _("N.A."), NULL);
        return;
    }

    id = GPOINTER_TO_UINT(g_object_get_data(G_OBJECT(column), "id"));

    name = gwy_resource_get_name(GWY_RESOURCE(gvalue));
    i = gwy_inventory_get_item_position(gwy_grain_values(), name);
    if (i < 0) {
        g_warning("Grain value not present in inventory.");
        g_object_set(renderer, "text", "", NULL);
        return;
    }

    stat = controls->stats + i;
    if (id == 0)
        value = stat->mean;
    else if (id == 1)
        value = stat->median;
    else if (id == 2)
        value = stat->rms;
    else if (id == 3)
        value = stat->q75 - stat->q25;
    else {
        g_object_set(renderer, "markup", stat->vf->units, NULL);
        return;
    }

    g_snprintf(buf, sizeof(buf), "%.*f",
               stat->vf->precision, value/stat->vf->magnitude);
    g_object_set(renderer, "markup", buf, NULL);
}

static GrainQuantityStats*
calculate_stats(GwyDataField *dfield, GwyDataField *mfield)
{
    GwySIUnitFormatStyle style = GWY_SI_UNIT_FORMAT_VFMARKUP;
    GwyInventory *inventory;
    GwyGrainValue **gvalues;
    GrainQuantityStats *stats, *stat;
    GwyGrainQuantity quantity;
    GwySIUnit *siunitxy, *siunitz, *siunit;
    guint n, i, j, xres, yres, ngrains;
    gdouble p[3], pv[3], v;
    gint *grains;
    gint powerxy, powerz;
    gboolean is_angle;
    gdouble **values;

    xres = gwy_data_field_get_xres(mfield);
    yres = gwy_data_field_get_yres(mfield);
    grains = g_new0(gint, xres*yres);
    ngrains = gwy_data_field_number_grains(mfield, grains);

    inventory = gwy_grain_values();
    n = gwy_inventory_get_n_items(inventory);

    gvalues = g_new(GwyGrainValue*, n);
    values = g_new(gdouble*, n);
    for (i = 0; i < n; i++) {
        gvalues[i] = gwy_inventory_get_nth_item(inventory, i);
        values[i] = g_new(gdouble, ngrains + 1);
    }

    gwy_grain_values_calculate(n, gvalues, values, dfield, ngrains, grains);
    g_free(grains);

    stats = g_new(GrainQuantityStats, n);
    p[0] = 25.0;
    p[1] = 50.0;
    p[2] = 75.0;
    siunit = NULL;
    for (i = 0; i < n; i++) {
        stat = stats + i;
        is_angle = (gwy_grain_value_get_flags(gvalues[i])
                    & GWY_GRAIN_VALUE_IS_ANGLE);
        /* Exclude the zeroth value of the array as no-grain. */
        if (is_angle) {
            stat->mean = calc_semicirc_average(values[i]+1, ngrains);
            stat->rms = calc_semicirc_rms(values[i]+1, ngrains, stat->mean);
            stat->median = calc_semicirc_median(values[i]+1, ngrains, &j);
            calc_semicirc_quartiles(values[i]+1, ngrains, j,
                                    &stat->q25, &stat->q75);
        }
        else {
            gwy_math_percentiles(ngrains, values[i]+1,
                                 GWY_PERCENTILE_INTERPOLATION_LINEAR, 3, p, pv);
            stat->q25 = pv[0];
            stat->median = pv[1];
            stat->q75 = pv[2];
            stat->mean = calc_average(values[i]+1, ngrains);
            stat->rms = calc_rms(values[i]+1, ngrains, stat->mean);
        }

        quantity = gwy_grain_value_get_quantity(gvalues[i]);
        if (quantity == GWY_GRAIN_VALUE_PIXEL_AREA) {
            stat->vf = gwy_si_unit_value_format_new(1.0, 1,
                                                    _("px<sup>2</sup>"));
            continue;
        }
        if (is_angle) {
            stat->vf = gwy_si_unit_value_format_new(G_PI/180.0, 2, _("deg"));
            continue;
        }

        siunitxy = gwy_data_field_get_si_unit_xy(dfield);
        siunitz = gwy_data_field_get_si_unit_z(dfield);
        powerxy = gwy_grain_value_get_power_xy(gvalues[i]);
        powerz = gwy_grain_value_get_power_z(gvalues[i]);
        siunit = gwy_si_unit_power_multiply(siunitxy, powerxy, siunitz, powerz,
                                            siunit);
        v = fmax(fabs(stat->mean), fabs(stat->median));
        v = fmax(v, stat->rms);
        v = fmax(v, stat->q75 - stat->q25);
        stat->vf = gwy_si_unit_get_format_with_digits(siunit, style, v, 3,
                                                      NULL);
    }
    g_object_unref(siunit);
    g_free(values);
    g_free(gvalues);

    return stats;
}

static gdouble
calc_average(gdouble *values, guint n)
{
    return gwy_math_trimmed_mean(n, values, 0, 0);
}

static gdouble
calc_rms(const gdouble *values, guint n, gdouble mean)
{
    gdouble v, s2 = 0.0;
    guint j;

    if (n < 2)
        return 0.0;

    for (j = 0; j < n; j++) {
        v = values[j] - mean;
        s2 += v*v;
    }
    return sqrt(s2/(n - 1));
}

/* We need an average value that does not distinguishes between opposite
 * directions because all grain angular quantities are unoriented.  Do it by
 * multiplying the angles by 2. */
static gdouble
calc_semicirc_average(const gdouble *angles, guint n)
{
    gdouble sc = 0.0, ss = 0.0;
    guint j;

    for (j = 0; j < n; j++) {
        sc += cos(2.0*angles[j]);
        ss += sin(2.0*angles[j]);
    }
    return gwy_canonicalize_angle(0.5*atan2(ss, sc), FALSE, FALSE);
}

static gdouble
calc_semicirc_rms(const gdouble *angles, guint n, gdouble mean)
{
    gdouble v, s2 = 0.0;
    guint j;

    if (n < 2)
        return 0.0;

    for (j = 0; j < n; j++) {
        /* Move the difference to [-π/2,π/2] range. */
        v = gwy_canonicalize_angle(angles[j] - mean, FALSE, FALSE);
        s2 += v*v;
    }
    return sqrt(s2/(n - 1));
}

/* Find semicircular median in linear time (or close to, we also sort the
 * array).  */
static gdouble
calc_semicirc_median(gdouble *angles, guint n, guint *medpos)
{
    gdouble Sforw, Sback, v, Sbest;
    guint j, jmed, jopposite, jbest;

    /* If there is one angle it is the median.  If there are two then any of
     * them is the median. */
    if (n < 3) {
        *medpos = 0;
        return angles[0];
    }

    gwy_math_sort(n, angles);
    /* Choose one value to be a speculative median at random.  Calculate the
     * sums of distances.  Find the first angle which is closer in the opposite
     * direction.  */
    jmed = jopposite = 0;
    Sforw = Sback = 0.0;
    for (j = 1; j < n; j++) {
        v = angles[j] - angles[jmed];
        if (v >= G_PI_2) {
            jopposite = j;
            break;
        }
        Sforw += v;
    }
    while (j < n) {
        v = (G_PI + angles[jmed]) - angles[j];
        Sback += v;
        j++;
    }
    Sbest = Sforw + Sback;
    jbest = 0;

    /* Now sequentially try all the other angles.  When we move by delta
     * forward, we can recalculate Sforw and Sback and then possible advance
     * jopposite. */
    while (++jmed < n) {
        v = angles[jmed] - angles[jmed-1];
        if (jopposite > jmed) {
            Sforw -= (jopposite - jmed)*v;
            Sback += (jmed + n - jopposite)*v;
        }
        else {
            Sforw -= (jopposite + n - jmed)*v;
            Sback += (jmed - jopposite)*v;
        }

        while (TRUE) {
            v = angles[jopposite] - angles[jmed];
            if (jopposite > jmed && v < G_PI_2) {
                Sback += v - G_PI;
                Sforw += v;
                jopposite = (jopposite + 1) % n;
            }
            else if (jopposite < jmed && -v > G_PI_2) {
                Sback += v;
                Sforw += v + G_PI;
                jopposite++;
            }
            else
                break;
        }

        if (Sback + Sforw < Sbest) {
            Sbest = Sback + Sforw;
            jbest = jmed;
        }
    }

    *medpos = jbest;
    return angles[jbest];
}

static void
calc_semicirc_quartiles(const gdouble *angles, guint n, guint medpos,
                        gdouble *q25, gdouble *q75)
{
    guint j;

    if (n < 3) {
        *q25 = *q75 = angles[medpos];
        return;
    }

    j = (medpos + n + n/4 - n/2) % n;
    *q25 = angles[j];

    j = (medpos + 3*n/4 - n/2) % n;
    *q75 = angles[j];
}

static GwySelection*
create_selection(const gchar *typename,
                 guint *ngrains)
{
    GParamSpecInt *pspec;
    GObjectClass *klass;
    GType type;

    type = g_type_from_name(typename);
    g_return_val_if_fail(type, NULL);

    klass = g_type_class_ref(type);
    pspec = (GParamSpecInt*)g_object_class_find_property(klass, "max-objects");
    g_return_val_if_fail(G_IS_PARAM_SPEC_UINT(pspec), NULL);

    if ((gint)*ngrains > pspec->maximum) {
        g_warning("Too many grains for %s, only first %d will be shown.",
                  typename, pspec->maximum);
        *ngrains = pspec->maximum;
    }
    return g_object_new(type, "max-objects", *ngrains, NULL);
}

/* FIXME: It would be nice to have something like that also for minimum and
 * maximum bounding dimensions. */
static void
grain_inscribe_discs(GwyContainer *data, GwyRunType run)
{
    static const GwyGrainQuantity quantities[] = {
        GWY_GRAIN_VALUE_INSCRIBED_DISC_R,
        GWY_GRAIN_VALUE_INSCRIBED_DISC_X,
        GWY_GRAIN_VALUE_INSCRIBED_DISC_Y,
    };

    GwyDataField *dfield, *mfield;
    GwySelection *selection;
    guint ngrains, i;
    gint *grains;
    gdouble *inscd;
    gdouble *values[3];
    gchar *key;
    gint id;

    g_return_if_fail(run & INSCRIBE_RUN_MODES);
    gwy_app_data_browser_get_current(GWY_APP_DATA_FIELD, &dfield,
                                     GWY_APP_MASK_FIELD, &mfield,
                                     GWY_APP_DATA_FIELD_ID, &id,
                                     0);

    grains = g_new0(gint, mfield->xres * mfield->yres);
    ngrains = gwy_data_field_number_grains(mfield, grains);
    inscd = g_new(gdouble, 3*(ngrains + 1));
    for (i = 0; i < 3; i++)
        values[i] = inscd + i*(ngrains + 1);

    gwy_data_field_grains_get_quantities(dfield, values, quantities, 3,
                                         ngrains, grains);

    selection = create_selection("GwySelectionEllipse", &ngrains);
    for (i = 1; i <= ngrains; i++) {
        gdouble r = values[0][i], x = values[1][i], y = values[2][i];
        gdouble xy[4] = { x - r, y - r, x + r, y + r };
        gwy_selection_set_object(selection, i-1, xy);
    }

    key = g_strdup_printf("/%d/select/ellipse", id);
    gwy_container_set_object_by_name(data, key, selection);
    g_object_unref(selection);

    g_free(grains);
    g_free(inscd);
}

static void
grain_exscribe_circles(GwyContainer *data, GwyRunType run)
{
    static const GwyGrainQuantity quantities[] = {
        GWY_GRAIN_VALUE_CIRCUMCIRCLE_R,
        GWY_GRAIN_VALUE_CIRCUMCIRCLE_X,
        GWY_GRAIN_VALUE_CIRCUMCIRCLE_Y,
    };

    GwyDataField *dfield, *mfield;
    GwySelection *selection;
    guint ngrains, i;
    gint *grains;
    gdouble *circc;
    gdouble *values[3];
    gchar *key;
    gint id;

    g_return_if_fail(run & INSCRIBE_RUN_MODES);
    gwy_app_data_browser_get_current(GWY_APP_DATA_FIELD, &dfield,
                                     GWY_APP_MASK_FIELD, &mfield,
                                     GWY_APP_DATA_FIELD_ID, &id,
                                     0);

    grains = g_new0(gint, mfield->xres * mfield->yres);
    ngrains = gwy_data_field_number_grains(mfield, grains);
    circc = g_new(gdouble, 3*(ngrains + 1));
    for (i = 0; i < 3; i++)
        values[i] = circc + i*(ngrains + 1);

    gwy_data_field_grains_get_quantities(dfield, values, quantities, 3,
                                         ngrains, grains);

    selection = create_selection("GwySelectionEllipse", &ngrains);
    for (i = 1; i <= ngrains; i++) {
        gdouble r = values[0][i], x = values[1][i], y = values[2][i];
        gdouble xy[4] = { x - r, y - r, x + r, y + r };
        gwy_selection_set_object(selection, i-1, xy);
    }

    key = g_strdup_printf("/%d/select/ellipse", id);
    gwy_container_set_object_by_name(data, key, selection);
    g_object_unref(selection);

    g_free(grains);
    g_free(circc);
}

static const gchar report_style_key_summary[] = "/module/grain_summary/report_style";

static void
summary_load_args(GwyContainer *container, GrainSummaryArgs *args)
{
    *args = grain_summary_defaults;

    gwy_container_gis_enum_by_name(container, report_style_key_summary,
                                   &args->report_style);
    /* FIXME: sanitize is hairy, make a helper for that. */
}

static void
summary_save_args(GwyContainer *container, GrainSummaryArgs *args)
{
    gwy_container_set_enum_by_name(container, report_style_key_summary,
                                   args->report_style);
}

static const gchar expanded_key[]          = "/module/grain_stat/expanded";
static const gchar report_style_key_stat[] = "/module/grain_stat/report_style";

static void
stat_load_args(GwyContainer *container, GrainStatArgs *args)
{
    *args = grain_stat_defaults;

    /* FIXME: sanitize is hairy, make a helper for that. */
    gwy_container_gis_enum_by_name(container, report_style_key_stat,
                                   &args->report_style);
    gwy_container_gis_int32_by_name(container, expanded_key, &args->expanded);
}

static void
stat_save_args(GwyContainer *container, GrainStatArgs *args)
{
    gwy_container_set_enum_by_name(container, report_style_key_stat,
                                   args->report_style);
    gwy_container_set_int32_by_name(container, expanded_key, args->expanded);
}

/* vim: set cin et ts=4 sw=4 cino=>1s,e0,n0,f0,{0,}0,^0,\:1s,=0,g1s,h0,t0,+1s,c3,(0,u0 : */
