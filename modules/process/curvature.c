/*
 *  $Id$
 *  Copyright (C) 2009-2017 David Necas (Yeti).
 *  E-mail: yeti@gwyddion.net.net.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA 02110-1301, USA.
 */

#include "config.h"
#include <string.h>
#include <stdlib.h>
#include <gtk/gtk.h>
#include <libgwyddion/gwymacros.h>
#include <libgwyddion/gwymath.h>
#include <libgwyddion/gwyresults.h>
#include <libprocess/level.h>
#include <libprocess/gwyprocesstypes.h>
#include <libgwydgets/gwyradiobuttons.h>
#include <libgwydgets/gwynullstore.h>
#include <libgwymodule/gwymodule-process.h>
#include <app/gwymoduleutils.h>
#include <app/gwyapp.h>
#include "preview.h"

#define CURVATURE_RUN_MODES (GWY_RUN_IMMEDIATE | GWY_RUN_INTERACTIVE)

typedef enum {
    PARAM_X0,
    PARAM_Y0,
    PARAM_A,
    PARAM_R1,
    PARAM_R2,
    PARAM_PHI1,
    PARAM_PHI2,
    PARAM_NPARAMS
} CurvatureParamType;

typedef struct {
    gdouble d, t, x, y;
} Intersection;

typedef struct {
    gboolean set_selection;
    gboolean plot_graph;
    GwyMaskingType masking;
    GwyResultsReportType report_style;
    GwyAppDataId target_graph;
} CurvatureArgs;

typedef struct {
    CurvatureArgs *args;
    gdouble params[PARAM_NPARAMS];
    GwyResults *results;
    GtkWidget *dialog;
    GSList *masking_group;
    GtkWidget *set_selection;
    GtkWidget *plot_graph;
    GtkWidget *target_graph;
    GtkWidget *view;
    GtkWidget *graph;
    GtkWidget *rexport;
    GtkWidget *warning;
    GwyNullStore *paramstore;
    GwyGraphModel *gmodel;
    GwySelection *selection;
    GwyContainer *data;
} CurvatureControls;

static gboolean module_register                (void);
static void     curvature                      (GwyContainer *data,
                                                GwyRunType run);
static void     curvature_do                   (GwyContainer *data,
                                                GwyDataField *dfield,
                                                GwyDataField *mfield,
                                                gint oldid,
                                                const CurvatureArgs *args);
static gboolean curvature_dialog               (CurvatureArgs *args,
                                                GwyContainer *data,
                                                GwyDataField *dfield,
                                                GwyDataField *mfield,
                                                gint id);
static void     curvature_set_selection_changed(GtkToggleButton *button,
                                                CurvatureControls *controls);
static void     curvature_plot_graph_changed   (GtkToggleButton *button,
                                                CurvatureControls *controls);
static void     update_target_graphs           (CurvatureControls *controls);
static gboolean filter_target_graphs           (GwyContainer *data,
                                                gint id,
                                                gpointer user_data);
static void     target_graph_changed           (CurvatureControls *controls);
static void     init_graph_model_units         (GwyGraphModel *gmodel,
                                                GwyDataField *dfield);
static void     curvature_dialog_update        (CurvatureControls *controls,
                                                CurvatureArgs *args);
static void     curvature_masking_changed      (GtkToggleButton *button,
                                                CurvatureControls *controls);
static void     report_style_changed           (CurvatureControls *controls,
                                                GwyResultsExport *rexport);
static void     curvature_update_preview       (CurvatureControls *controls,
                                                CurvatureArgs *args);
static void     load_args                      (GwyContainer *container,
                                                CurvatureArgs *args);
static void     save_args                      (GwyContainer *container,
                                                CurvatureArgs *args);
static void     sanitize_args                  (CurvatureArgs *args);

static const CurvatureArgs curvature_defaults = {
    TRUE,
    FALSE,
    GWY_MASK_IGNORE,
    GWY_RESULTS_REPORT_COLON,
    GWY_APP_DATA_ID_NONE,
};

static const gchar *guivalues[] = {
    "x0", "y0", "z0", "r1", "r2", "phi1", "phi2",
};

static GwyAppDataId target_id = GWY_APP_DATA_ID_NONE;

static GwyModuleInfo module_info = {
    GWY_MODULE_ABI_VERSION,
    &module_register,
    N_("Calculates overall curvature."),
    "Yeti <yeti@gwyddion.net>",
    "2.0",
    "David Nečas (Yeti)",
    "2009",
};

GWY_MODULE_QUERY2(module_info, curvature)

static gboolean
module_register(void)
{
    gwy_process_func_register("curvature",
                              (GwyProcessFunc)&curvature,
                              N_("/_Level/_Curvature..."),
                              GWY_STOCK_CURVATURE,
                              CURVATURE_RUN_MODES,
                              GWY_MENU_FLAG_DATA,
                              N_("Calculate overall curvature"));

    return TRUE;
}

static void
curvature(GwyContainer *data, GwyRunType run)
{
    GwyDataField *dfield, *mfield;
    CurvatureArgs args;
    gboolean ok;
    gint id;

    g_return_if_fail(run & CURVATURE_RUN_MODES);
    g_return_if_fail(g_type_from_name("GwyLayerLine"));
    gwy_app_data_browser_get_current(GWY_APP_DATA_FIELD, &dfield,
                                     GWY_APP_MASK_FIELD, &mfield,
                                     GWY_APP_DATA_FIELD_ID, &id,
                                     0);
    g_return_if_fail(dfield);

    if (!gwy_si_unit_equal(gwy_data_field_get_si_unit_xy(dfield),
                           gwy_data_field_get_si_unit_z(dfield))) {
        GtkWidget *dialog;

        dialog = gtk_message_dialog_new
                        (gwy_app_find_window_for_channel(data, id),
                         GTK_DIALOG_DESTROY_WITH_PARENT,
                         GTK_MESSAGE_ERROR,
                         GTK_BUTTONS_OK,
                         _("%s: Lateral dimensions and value must "
                           "be the same physical quantity."),
                         _("Curvature"));
        gtk_dialog_run(GTK_DIALOG(dialog));
        gtk_widget_destroy(dialog);
        return;
    }

    load_args(gwy_app_settings_get(), &args);
    if (run == GWY_RUN_INTERACTIVE) {
        ok = curvature_dialog(&args, data, dfield, mfield, id);
        save_args(gwy_app_settings_get(), &args);
        if (!ok)
            return;
    }
    curvature_do(data, dfield, mfield, id, &args);
    if (args.set_selection)
        gwy_app_channel_log_add_proc(data, id, id);
}

static int
compare_double(const void *a, const void *b)
{
    const gdouble *da = (const gdouble*)a;
    const gdouble *db = (const gdouble*)b;

    if (*da < *db)
        return -1;
    if (*da > *db)
        return 1;
    return 0;
}

static gboolean
intersect_with_boundary(gdouble x_0, gdouble y_0,
                        gdouble phi,
                        gdouble w, gdouble h,
                        Intersection *i1, Intersection *i2)
{
    enum { NISEC = 4 };
    Intersection isec[NISEC];
    gdouble diag;
    guint i;

    /* With x = 0 */
    isec[0].t = -x_0/cos(phi);
    isec[0].x = 0.0;
    isec[0].y = y_0 - x_0*tan(phi);

    /* With x = w */
    isec[1].t = (w - x_0)/cos(phi);
    isec[1].x = w;
    isec[1].y = y_0 + (w - x_0)*tan(phi);

    /* With y = 0 */
    isec[2].t = -y_0/sin(phi);
    isec[2].x = x_0 - y_0/tan(phi);
    isec[2].y = 0.0;

    /* With y = h */
    isec[3].t = (h - y_0)/sin(phi);
    isec[3].x = x_0 + (h - y_0)/tan(phi);
    isec[3].y = h;

    /* Distance from centre must be at most half the diagonal. */
    diag = 0.5*hypot(w, h);
    for (i = 0; i < NISEC; i++) {
        isec[i].d = hypot(isec[i].x - 0.5*w, isec[i].y - 0.5*h)/diag;
        gwy_debug("isec[%u]: %g", i, isec[i].d);
    }

    qsort(isec, NISEC, sizeof(Intersection), compare_double);

    for (i = 0; i < NISEC; i++) {
        if (isec[i].d > 1.0)
            break;
    }

    gwy_debug("intersections: %u", i);
    switch (i) {
        case 0:
        case 2:
        break;

        case 1:
        i = 0;
        break;

        case 3:
        i = 2;
        break;

        case 4:
        i = 2;
        /* Pick the right two intersections if it goes through two opposite
         * corners. */
        if (fabs(isec[0].t - isec[1].t) < fabs(isec[0].t - isec[2].t))
            isec[1] = isec[2];
        break;

        default:
        g_assert_not_reached();
        break;
    }

    if (i) {
        if (isec[0].t <= isec[1].t) {
            *i1 = isec[0];
            *i2 = isec[1];
        }
        else {
            *i1 = isec[1];
            *i2 = isec[0];
        }
        return TRUE;
    }
    return FALSE;
}

/* Does not include x and y offsets of the data field */
static gboolean
curvature_calculate(GwyDataField *dfield,
                    GwyDataField *mask,
                    const CurvatureArgs *args,
                    gdouble *params,
                    Intersection *i1,
                    Intersection *i2)
{
    enum { DEGREE = 2 };
    enum { A, BX, CXX, BY, CXY, CYY, NTERMS };
    gint term_powers[2*NTERMS];
    gdouble coeffs[NTERMS], ccoeffs[NTERMS];
    gdouble xreal, yreal, qx, qy, q, mx, my;
    gint xres, yres, i, j, k;
    gboolean ok;

    k = 0;
    g_assert(NTERMS == (DEGREE + 1)*(DEGREE + 2)/2);
    for (i = 0; i <= DEGREE; i++) {
        for (j = 0; j <= DEGREE - i; j++) {
            term_powers[k++] = j;
            term_powers[k++] = i;
        }
    }

    gwy_data_field_fit_poly(dfield, mask, NTERMS, term_powers,
                            args->masking != GWY_MASK_INCLUDE, coeffs);
    gwy_debug("NORM a=%g, bx=%g, by=%g, cxx=%g, cxy=%g, cyy=%g",
              coeffs[A], coeffs[BX], coeffs[BY],
              coeffs[CXX], coeffs[CXY], coeffs[CYY]);

    /* Transform coeffs from normalized coordinates to coordinates that are
     * still numerically around 1 but have the right aspect ratio. */
    xres = gwy_data_field_get_xres(dfield);
    yres = gwy_data_field_get_yres(dfield);
    xreal = gwy_data_field_get_xreal(dfield);
    yreal = gwy_data_field_get_yreal(dfield);
    qx = 2.0/xreal*xres/(xres - 1.0);
    qy = 2.0/yreal*yres/(yres - 1.0);
    q = sqrt(qx*qy);
    mx = sqrt(qx/qy);
    my = sqrt(qy/qx);

    ccoeffs[0] = coeffs[A];
    ccoeffs[1] = mx*coeffs[BX];
    ccoeffs[2] = my*coeffs[BY];
    ccoeffs[3] = mx*mx*coeffs[CXX];
    ccoeffs[4] = coeffs[CXY];
    ccoeffs[5] = my*my*coeffs[CYY];
    gwy_math_curvature(ccoeffs,
                       params + PARAM_R1, params + PARAM_R2,
                       params + PARAM_PHI1, params + PARAM_PHI2,
                       params + PARAM_X0, params + PARAM_Y0, params + PARAM_A);
    /* Transform to physical values. */
    /* FIXME: Why we have q*q here? */
    params[PARAM_R1] = 1.0/(q*q*params[PARAM_R1]);
    params[PARAM_R2] = 1.0/(q*q*params[PARAM_R2]);
    params[PARAM_X0] = params[PARAM_X0]/q + 0.5*xreal;
    params[PARAM_Y0] = params[PARAM_Y0]/q + 0.5*yreal;

    ok = TRUE;
    for (i = 0; i < 2; i++) {
        ok &= intersect_with_boundary(params[PARAM_X0], params[PARAM_Y0],
                                      -params[PARAM_PHI1 + i],
                                      xreal, yreal, i1 + i, i2 + i);
    }

    params[PARAM_X0] += gwy_data_field_get_xoffset(dfield);
    params[PARAM_Y0] += gwy_data_field_get_yoffset(dfield);

    return ok;
}

static gboolean
curvature_set_selection(GwyDataField *dfield,
                        const Intersection *i1,
                        const Intersection *i2,
                        GwySelection *selection)
{
    gdouble xreal, yreal;
    gdouble xy[4];
    gint xres, yres;
    guint i;

    xreal = gwy_data_field_get_xreal(dfield);
    yreal = gwy_data_field_get_yreal(dfield);
    xres = gwy_data_field_get_xres(dfield);
    yres = gwy_data_field_get_yres(dfield);
    for (i = 0; i < 2; i++) {
        xy[0] = CLAMP(i1[i].x, 0, xreal*(xres - 1)/xres);
        xy[1] = CLAMP(i1[i].y, 0, yreal*(yres - 1)/yres);
        xy[2] = CLAMP(i2[i].x, 0, xreal*(xres - 1)/xres);
        xy[3] = CLAMP(i2[i].y, 0, yreal*(yres - 1)/yres);
        gwy_selection_set_object(selection, i, xy);
    }

    return TRUE;
}

static gboolean
curvature_plot_graph(GwyDataField *dfield,
                     const Intersection *i1,
                     const Intersection *i2,
                     GwyGraphModel *gmodel)
{
    GwyGraphCurveModel *gcmodel;
    GwyDataLine *dline;
    gint xres, yres;
    guint i;

    if (!gwy_graph_model_get_n_curves(gmodel)) {
        GwySIUnit *siunitxy, *siunitz;
        gchar *s;

        siunitxy = gwy_si_unit_duplicate(gwy_data_field_get_si_unit_xy(dfield));
        siunitz = gwy_si_unit_duplicate(gwy_data_field_get_si_unit_z(dfield));
        g_object_set(gmodel,
                     "title", _("Curvature Sections"),
                     "si-unit-x", siunitxy,
                     "si-unit-y", siunitz,
                     NULL);
        g_object_unref(siunitxy);
        g_object_unref(siunitz);

        for (i = 0; i < 2; i++) {
            gcmodel = gwy_graph_curve_model_new();
            s = g_strdup_printf(_("Profile %d"), (gint)i+1);
            g_object_set(gcmodel,
                         "description", s,
                         "mode", GWY_GRAPH_CURVE_LINE,
                         "color", gwy_graph_get_preset_color(i),
                         NULL);
            g_free(s);
            gwy_graph_model_add_curve(gmodel, gcmodel);
            g_object_unref(gcmodel);
        }
    }
    else {
        g_assert(gwy_graph_model_get_n_curves(gmodel) == 2);
    }

    dline = gwy_data_line_new(1, 1.0, FALSE);
    xres = gwy_data_field_get_xres(dfield);
    yres = gwy_data_field_get_yres(dfield);
    for (i = 0; i < 2; i++) {
        gint col1 = gwy_data_field_rtoj(dfield, i1[i].x);
        gint row1 = gwy_data_field_rtoi(dfield, i1[i].y);
        gint col2 = gwy_data_field_rtoj(dfield, i2[i].x);
        gint row2 = gwy_data_field_rtoi(dfield, i2[i].y);

        gwy_data_field_get_profile(dfield, dline,
                                   CLAMP(col1, 0, xres-1),
                                   CLAMP(row1, 0, yres-1),
                                   CLAMP(col2, 0, xres-1),
                                   CLAMP(row2, 0, yres-1),
                                   -1, 1, GWY_INTERPOLATION_BILINEAR);
        gwy_data_line_set_offset(dline,
                                 i1[i].t/(i2[i].t - i1[i].t)
                                 * gwy_data_line_get_real(dline));
        gcmodel = gwy_graph_model_get_curve(gmodel, i);
        gwy_graph_curve_model_set_data_from_dataline(gcmodel, dline, 0, 0);
    }
    g_object_unref(dline);

    return TRUE;
}

static void
curvature_do(GwyContainer *data,
             GwyDataField *dfield,
             GwyDataField *mfield,
             gint oldid,
             const CurvatureArgs *args)
{
    gdouble params[PARAM_NPARAMS];
    Intersection i1[2], i2[2];
    gchar *key;

    if (!curvature_calculate(dfield, mfield, args, params, i1, i2))
        return;

    if (args->set_selection) {
        GwySelection *selection;

        selection = g_object_new(g_type_from_name("GwySelectionLine"),
                                 "max-objects", 1024,
                                 NULL);
        curvature_set_selection(dfield, i1, i2, selection);
        key = g_strdup_printf("/%d/select/line", oldid);
        gwy_container_set_object_by_name(data, key, selection);
        g_object_unref(selection);
    }

    if (args->plot_graph) {
        GwyGraphModel *gmodel;

        gmodel = gwy_graph_model_new();
        curvature_plot_graph(dfield, i1, i2, gmodel);
        gwy_app_add_graph_or_curves(gmodel, data, &args->target_graph, 1);
        g_object_unref(gmodel);
    }
}

static void
render_name(G_GNUC_UNUSED GtkTreeViewColumn *column,
            GtkCellRenderer *renderer,
            GtkTreeModel *model,
            GtkTreeIter *iter,
            gpointer data)
{
    const CurvatureControls *controls = (const CurvatureControls*)data;
    gint i;

    gtk_tree_model_get(model, iter, 0, &i, -1);
    g_object_set(renderer,
                 "text", gwy_results_get_label(controls->results, guivalues[i]),
                 NULL);
}

static void
render_symbol(G_GNUC_UNUSED GtkTreeViewColumn *column,
              GtkCellRenderer *renderer,
              GtkTreeModel *model,
              GtkTreeIter *iter,
              gpointer data)
{
    const CurvatureControls *controls = (const CurvatureControls*)data;
    gint i;

    gtk_tree_model_get(model, iter, 0, &i, -1);
    g_object_set(renderer,
                 "markup",
                 gwy_results_get_symbol(controls->results, guivalues[i]),
                 NULL);
}

static void
render_value(G_GNUC_UNUSED GtkTreeViewColumn *column,
             GtkCellRenderer *renderer,
             GtkTreeModel *model,
             GtkTreeIter *iter,
             gpointer data)
{
    const CurvatureControls *controls = (const CurvatureControls*)data;
    gint i;

    gtk_tree_model_get(model, iter, 0, &i, -1);
    g_object_set(renderer,
                 "markup",
                 gwy_results_get_full(controls->results, guivalues[i]),
                 NULL);
}

static gboolean
curvature_dialog(CurvatureArgs *args,
                 GwyContainer *data,
                 GwyDataField *dfield,
                 GwyDataField *mfield,
                 gint id)
{
    GtkWidget *dialog, *table, *label, *hbox, *vbox, *treeview;
    GwyDataChooser *chooser;
    GtkTreeSelection *selection;
    GtkTreeViewColumn *column;
    GtkCellRenderer *renderer;
    CurvatureControls controls;
    GwyResults *results;
    GwyResultsExport *rexport;
    gint response;
    gint row;

    controls.args = args;
    gwy_clear(controls.params, PARAM_NPARAMS);

    controls.results = results = gwy_results_new();
    gwy_results_add_header(results, N_("Curvature"));
    gwy_results_add_value_str(results, "file", N_("File"));
    gwy_results_add_value_str(results, "image", N_("Image"));
    gwy_results_add_value_yesno(results, "masking", N_("Mask in use"));
    gwy_results_add_separator(results);

    gwy_results_add_value(results, "x0", N_("Center x position"),
                          "power-x", 1, "symbol", "x<sub>0</sub>", NULL);
    gwy_results_add_value(results, "y0", N_("Center y position"),
                          "power-y", 1, "symbol", "y<sub>0</sub>", NULL);
    gwy_results_add_value(results, "z0", N_("Center value"),
                          "power-z", 1, "symbol", "z<sub>0</sub>", NULL);
    /* The units must be all the same anyway... */
    gwy_results_add_value(results, "r1", N_("Curvature radius 1"),
                          "power-x", 1, "symbol", "r<sub>1</sub>", NULL);
    gwy_results_add_value(results, "r2", N_("Curvature radius 2"),
                          "power-x", 1, "symbol", "r<sub>2</sub>", NULL);
    gwy_results_add_value(results, "phi1", N_("Direction 1"),
                          "is-angle", TRUE, "symbol", "φ<sub>1</sub>", NULL);
    gwy_results_add_value(results, "phi2", N_("Direction 2"),
                          "is-angle", TRUE, "symbol", "φ<sub>2</sub>", NULL);

    gwy_results_set_unit(results, "x", gwy_data_field_get_si_unit_xy(dfield));
    gwy_results_set_unit(results, "y", gwy_data_field_get_si_unit_xy(dfield));
    gwy_results_set_unit(results, "z", gwy_data_field_get_si_unit_z(dfield));

    gwy_results_fill_filename(results, "file", data);
    gwy_results_fill_channel(results, "image", data, id);

    controls.gmodel = gwy_graph_model_new();
    init_graph_model_units(controls.gmodel, dfield);

    dialog = gtk_dialog_new_with_buttons(_("Curvature"),
                                         NULL, 0,
                                         _("_Reset"), RESPONSE_RESET,
                                         GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
                                         GTK_STOCK_OK, GTK_RESPONSE_OK,
                                         NULL);
    gtk_dialog_set_default_response(GTK_DIALOG(dialog), GTK_RESPONSE_OK);
    gwy_help_add_to_proc_dialog(GTK_DIALOG(dialog), GWY_HELP_DEFAULT);
    controls.dialog = dialog;

    hbox = gtk_hbox_new(FALSE, 8);
    gtk_container_set_border_width(GTK_CONTAINER(hbox), 4);
    gtk_box_pack_start(GTK_BOX(GTK_DIALOG(dialog)->vbox), hbox,
                       FALSE, FALSE, 0);

    vbox = gtk_vbox_new(FALSE, 0);
    gtk_box_pack_start(GTK_BOX(hbox), vbox, FALSE, FALSE, 0);

    controls.data = gwy_container_new();
    gwy_container_set_object_by_name(controls.data, "/0/data", dfield);
    if (mfield)
        gwy_container_set_object_by_name(controls.data, "/0/mask", mfield);
    gwy_app_sync_data_items(data, controls.data, id, 0, FALSE,
                            GWY_DATA_ITEM_PALETTE,
                            GWY_DATA_ITEM_MASK_COLOR,
                            GWY_DATA_ITEM_RANGE,
                            GWY_DATA_ITEM_REAL_SQUARE,
                            0);
    controls.view = create_preview(controls.data, 0, PREVIEW_SMALL_SIZE, FALSE);
    controls.selection = create_vector_layer(GWY_DATA_VIEW(controls.view),
                                             0, "Line", FALSE);
    g_object_set(controls.selection, "max-objects", 2, NULL);
    gtk_box_pack_start(GTK_BOX(vbox), controls.view, FALSE, FALSE, 4);

    table = gtk_table_new(5 + (mfield ? 4 : 0), 3, FALSE);
    gtk_table_set_row_spacings(GTK_TABLE(table), 2);
    gtk_table_set_col_spacings(GTK_TABLE(table), 6);
    gtk_box_pack_start(GTK_BOX(vbox), table, FALSE, FALSE, 0);
    row = 0;

    label = gwy_label_new_header(_("Output Type"));
    gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);
    gtk_table_attach(GTK_TABLE(table), label,
                     0, 2, row, row+1, GTK_FILL, 0, 0, 0);
    row++;

    controls.set_selection
        = gtk_check_button_new_with_mnemonic(_("_Set selection"));
    gtk_table_attach(GTK_TABLE(table), controls.set_selection,
                     0, 2, row, row+1, GTK_FILL, 0, 0, 0);
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(controls.set_selection),
                                 args->set_selection);
    g_signal_connect(controls.set_selection, "toggled",
                     G_CALLBACK(curvature_set_selection_changed), &controls);
    row++;

    controls.plot_graph
        = gtk_check_button_new_with_mnemonic(_("_Plot graph"));
    gtk_table_attach(GTK_TABLE(table), controls.plot_graph,
                     0, 2, row, row+1, GTK_FILL, 0, 0, 0);
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(controls.plot_graph),
                                 args->plot_graph);
    g_signal_connect(controls.plot_graph, "toggled",
                     G_CALLBACK(curvature_plot_graph_changed), &controls);
    row++;

    gtk_table_set_row_spacing(GTK_TABLE(table), row-1, 8);
    controls.target_graph = gwy_data_chooser_new_graphs();
    chooser = GWY_DATA_CHOOSER(controls.target_graph);
    gwy_data_chooser_set_none(chooser, _("New graph"));
    gwy_data_chooser_set_active(chooser, NULL, -1);
    gwy_data_chooser_set_filter(chooser, filter_target_graphs, &controls, NULL);
    gwy_data_chooser_set_active_id(chooser, &args->target_graph);
    gwy_data_chooser_get_active_id(chooser, &args->target_graph);
    gwy_table_attach_adjbar(GTK_WIDGET(table), row, _("Target _graph:"), NULL,
                            GTK_OBJECT(controls.target_graph),
                            GWY_HSCALE_WIDGET_NO_EXPAND);
    gwy_table_hscale_set_sensitive(GTK_OBJECT(controls.target_graph),
                                   args->plot_graph);
    g_signal_connect_swapped(controls.target_graph, "changed",
                             G_CALLBACK(target_graph_changed), &controls);
    row++;

    if (mfield) {
        gtk_table_set_row_spacing(GTK_TABLE(table), row-1, 8);
        label = gwy_label_new_header(_("Masking Mode"));
        gtk_table_attach(GTK_TABLE(table), label,
                        0, 3, row, row+1, GTK_EXPAND | GTK_FILL, 0, 0, 0);
        row++;

        controls.masking_group
            = gwy_radio_buttons_create(gwy_masking_type_get_enum(), -1,
                                       G_CALLBACK(curvature_masking_changed),
                                       &controls, args->masking);
        row = gwy_radio_buttons_attach_to_table(controls.masking_group,
                                                GTK_TABLE(table), 3, row);
        gtk_table_set_row_spacing(GTK_TABLE(table), row, 8);
    }
    else
        controls.masking_group = NULL;

    controls.warning = gtk_label_new(NULL);
    gtk_misc_set_alignment(GTK_MISC(controls.warning), 0.0, 0.5);
    gtk_table_attach(GTK_TABLE(table), controls.warning,
                     0, 3, row, row+1, GTK_EXPAND | GTK_FILL, 0, 0, 0);

    vbox = gtk_vbox_new(FALSE, 0);
    gtk_box_pack_start(GTK_BOX(hbox), vbox, TRUE, TRUE, 0);

    controls.graph = gwy_graph_new(controls.gmodel);
    gwy_graph_enable_user_input(GWY_GRAPH(controls.graph), FALSE);
    gtk_widget_set_size_request(controls.graph, 320, 260);
    g_object_unref(controls.gmodel);

    gtk_box_pack_start(GTK_BOX(vbox), controls.graph, TRUE, TRUE, 4);

    controls.paramstore = gwy_null_store_new(PARAM_NPARAMS);
    treeview
        = gtk_tree_view_new_with_model(GTK_TREE_MODEL(controls.paramstore));
    g_object_unref(controls.paramstore);
    gtk_tree_view_set_headers_visible(GTK_TREE_VIEW(treeview), FALSE);
    gtk_box_pack_start(GTK_BOX(vbox), treeview, FALSE, FALSE, 4);

    selection = gtk_tree_view_get_selection(GTK_TREE_VIEW(treeview));
    gtk_tree_selection_set_mode(selection, GTK_SELECTION_NONE);

    renderer = gtk_cell_renderer_text_new();
    column = gtk_tree_view_column_new_with_attributes(_("Parameter"), renderer,
                                                      NULL);
    gtk_tree_view_column_set_cell_data_func(column, renderer,
                                            render_name, &controls, NULL);
    gtk_tree_view_column_set_expand(column, TRUE);
    gtk_tree_view_append_column(GTK_TREE_VIEW(treeview), column);

    renderer = gtk_cell_renderer_text_new();
    column = gtk_tree_view_column_new_with_attributes(_("Symbol"), renderer,
                                                      NULL);
    gtk_tree_view_column_set_cell_data_func(column, renderer,
                                            render_symbol, &controls, NULL);
    gtk_tree_view_column_set_expand(column, TRUE);
    gtk_tree_view_append_column(GTK_TREE_VIEW(treeview), column);

    renderer = gtk_cell_renderer_text_new();
    g_object_set(renderer, "xalign", 1.0, NULL);
    column = gtk_tree_view_column_new_with_attributes(_("Value"), renderer,
                                                      NULL);
    gtk_tree_view_column_set_cell_data_func(column, renderer,
                                            render_value, &controls, NULL);
    gtk_tree_view_column_set_expand(column, TRUE);
    gtk_tree_view_append_column(GTK_TREE_VIEW(treeview), column);

    controls.rexport = gwy_results_export_new(args->report_style);
    rexport = GWY_RESULTS_EXPORT(controls.rexport);
    gwy_results_export_set_title(rexport, _("Save Curvature"));
    gwy_results_export_set_results(rexport, results);
    gtk_box_pack_start(GTK_BOX(vbox), controls.rexport, FALSE, FALSE, 0);
    g_signal_connect_swapped(rexport, "format-changed",
                             G_CALLBACK(report_style_changed), &controls);

    curvature_update_preview(&controls, args);

    gtk_widget_show_all(dialog);
    do {
        response = gtk_dialog_run(GTK_DIALOG(dialog));
        switch (response) {
            case GTK_RESPONSE_CANCEL:
            case GTK_RESPONSE_DELETE_EVENT:
            gtk_widget_destroy(dialog);
            case GTK_RESPONSE_NONE:
            g_object_unref(results);
            return FALSE;
            break;

            case GTK_RESPONSE_OK:
            break;

            case RESPONSE_RESET:
            *args = curvature_defaults;
            curvature_dialog_update(&controls, args);
            break;

            default:
            g_assert_not_reached();
            break;
        }
    } while (response != GTK_RESPONSE_OK);

    gtk_widget_destroy(dialog);
    g_object_unref(results);

    return TRUE;
}

static void
curvature_dialog_update(CurvatureControls *controls,
                        CurvatureArgs *args)
{
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(controls->set_selection),
                                 args->set_selection);
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(controls->plot_graph),
                                 args->plot_graph);
    if (controls->masking_group)
        gwy_radio_buttons_set_current(controls->masking_group, args->masking);
}

static void
curvature_set_selection_changed(GtkToggleButton *button,
                                CurvatureControls *controls)
{
    CurvatureArgs *args = controls->args;

    args->set_selection = gtk_toggle_button_get_active(button);
}

static void
curvature_plot_graph_changed(GtkToggleButton *button,
                             CurvatureControls *controls)
{
    CurvatureArgs *args = controls->args;

    args->plot_graph = gtk_toggle_button_get_active(button);
    gwy_table_hscale_set_sensitive(GTK_OBJECT(controls->target_graph),
                                   args->plot_graph);
}

static void
curvature_masking_changed(GtkToggleButton *button,
                          CurvatureControls *controls)
{
    CurvatureArgs *args;

    if (!gtk_toggle_button_get_active(button))
        return;

    args = controls->args;
    args->masking = gwy_radio_buttons_get_current(controls->masking_group);
    curvature_update_preview(controls, args);
}

static void
report_style_changed(CurvatureControls *controls, GwyResultsExport *rexport)
{
    controls->args->report_style = gwy_results_export_get_format(rexport);
}

static void
update_target_graphs(CurvatureControls *controls)
{
    GwyDataChooser *chooser = GWY_DATA_CHOOSER(controls->target_graph);
    gwy_data_chooser_refilter(chooser);
}

static gboolean
filter_target_graphs(GwyContainer *data, gint id, gpointer user_data)
{
    CurvatureControls *controls = (CurvatureControls*)user_data;
    GwyGraphModel *gmodel = controls->gmodel, *targetgmodel;
    GQuark quark = gwy_app_get_graph_key_for_id(id);

    g_return_val_if_fail(gmodel, FALSE);
    return (gwy_container_gis_object(data, quark, (GObject**)&targetgmodel)
            && gwy_graph_model_units_are_compatible(gmodel, targetgmodel));
}

static void
target_graph_changed(CurvatureControls *controls)
{
    GwyDataChooser *chooser = GWY_DATA_CHOOSER(controls->target_graph);
    GwyAppDataId *target = &controls->args->target_graph;

    gwy_data_chooser_get_active_id(chooser, target);
}

static void
init_graph_model_units(GwyGraphModel *gmodel,
                       GwyDataField *dfield)
{
    GwySIUnit *unit;

    unit = gwy_data_field_get_si_unit_xy(dfield);
    unit = gwy_si_unit_duplicate(unit);
    g_object_set(gmodel, "si-unit-x", unit, NULL);
    g_object_unref(unit);

    unit = gwy_data_field_get_si_unit_z(dfield);
    unit = gwy_si_unit_duplicate(unit);
    g_object_set(gmodel, "si-unit-y", unit, NULL);
    g_object_unref(unit);
}

static void
curvature_update_preview(CurvatureControls *controls,
                         CurvatureArgs *args)
{
    GwyDataField *source, *mask = NULL;
    GwySelection *selection;
    Intersection i1[2], i2[2];
    gdouble params[PARAM_NPARAMS];
    gboolean ok;
    guint i;

    source = gwy_container_get_object_by_name(controls->data, "/0/data");
    selection = gwy_container_get_object_by_name(controls->data,
                                                 "/0/select/line");
    gwy_container_gis_object_by_name(controls->data, "/0/mask", &mask);

    ok = curvature_calculate(source, mask, args, params, i1, i2);

    gwy_results_fill_values(controls->results,
                            "masking",
                            mask && (args->masking != GWY_MASK_IGNORE),
                            NULL);
    if (ok) {
        gwy_results_fill_values(controls->results,
                                "x0", params[PARAM_X0],
                                "y0", params[PARAM_Y0],
                                "z0", params[PARAM_A],
                                "r1", params[PARAM_R1],
                                "r2", params[PARAM_R2],
                                "phi1", params[PARAM_PHI1],
                                "phi2", params[PARAM_PHI2],
                                NULL);
        curvature_set_selection(source, i1, i2, selection);
        curvature_plot_graph(source, i1, i2, controls->gmodel);
        gtk_label_set_text(GTK_LABEL(controls->warning), "");
    }
    else {
        gwy_results_set_nav(controls->results,
                            G_N_ELEMENTS(guivalues), guivalues);
        gwy_selection_clear(selection);
        gwy_graph_model_remove_all_curves(controls->gmodel);
        gtk_label_set_text(GTK_LABEL(controls->warning),
                           _("Axes are outside the image."));
    }
    gwy_results_export_set_actions_sensitive(GWY_RESULTS_EXPORT(controls->rexport),
                                             ok);

    for (i = 0; i < PARAM_NPARAMS; i++)
        gwy_null_store_row_changed(controls->paramstore, i);

    update_target_graphs(controls);
}

static const gchar masking_key[]       = "/module/curvature/masking";
static const gchar plot_graph_key[]    = "/module/curvature/plot_graph";
static const gchar report_style_key[]  = "/module/curvature/report_style";
static const gchar set_selection_key[] = "/module/curvature/set_selection";

static void
sanitize_args(CurvatureArgs *args)
{
    args->masking = gwy_enum_sanitize_value(args->masking,
                                            GWY_TYPE_MASKING_TYPE);
    args->set_selection = !!args->set_selection;
    args->plot_graph = !!args->plot_graph;
    gwy_app_data_id_verify_graph(&args->target_graph);
}

static void
load_args(GwyContainer *container,
          CurvatureArgs *args)
{
    *args = curvature_defaults;

    gwy_container_gis_enum_by_name(container, masking_key, &args->masking);
    gwy_container_gis_enum_by_name(container, report_style_key,
                                   &args->report_style);
    gwy_container_gis_boolean_by_name(container, set_selection_key,
                                      &args->set_selection);
    gwy_container_gis_boolean_by_name(container, plot_graph_key,
                                      &args->plot_graph);
    args->target_graph = target_id;
    sanitize_args(args);
}

static void
save_args(GwyContainer *container,
          CurvatureArgs *args)
{
    target_id = args->target_graph;
    gwy_container_set_enum_by_name(container, masking_key, args->masking);
    gwy_container_set_enum_by_name(container, report_style_key,
                                   args->report_style);
    gwy_container_set_boolean_by_name(container, set_selection_key,
                                      args->set_selection);
    gwy_container_set_boolean_by_name(container, plot_graph_key,
                                      args->plot_graph);
}

/* vim: set cin et ts=4 sw=4 cino=>1s,e0,n0,f0,{0,}0,^0,\:1s,=0,g1s,h0,t0,+1s,c3,(0,u0 : */
