/*
 *  $Id$
 *  Copyright (C) 2017-2018 David Necas (Yeti), Petr Klapetek.
 *  E-mail: yeti@gwyddion.net, klapetek@gwyddion.net.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA 02110-1301, USA.
 */

#include "config.h"
#include <gtk/gtk.h>
#include <libgwyddion/gwymacros.h>
#include <libgwyddion/gwymath.h>
#include <libgwyddion/gwynlfit.h>
#include <libprocess/gwyprocesstypes.h>
#include <libprocess/arithmetic.h>
#include <libprocess/inttrans.h>
#include <libgwydgets/gwycombobox.h>
#include <libgwydgets/gwystock.h>
#include <libgwymodule/gwymodule-process.h>
#include <app/gwymoduleutils.h>
#include <app/gwyapp.h>
#include "mfmops.h"

#define PSF_RUN_MODES (GWY_RUN_IMMEDIATE | GWY_RUN_INTERACTIVE)

#define DECLARE_PSF(name) \
    static gdouble  psf_##name##_fit_func   (guint i, \
                                             const gdouble *param, \
                                             gpointer user_data, \
                                             gboolean *success); \
    static void     psf_##name##_fit_diff   (guint i, \
                                             const gdouble *param, \
                                             const gboolean *fixed_param, \
                                             GwyNLFitIdxFunc func, \
                                             gpointer user_data, \
                                             gdouble *der, \
                                             gboolean *success); \
    static gboolean psf_##name##_init_params(GwyDataField *model_re, \
                                             GwyDataField *model_im, \
                                             GwyDataField *data_re, \
                                             GwyDataField *data_im, \
                                             GwyDataField *freq_x, \
                                             GwyDataField *freq_y, \
                                             gdouble *params); \
    static void     psf_##name##_fill_psf   (GwyDataField *freq_x, \
                                             GwyDataField *freq_y, \
                                             GwyDataField *buf_re, \
                                             GwyDataField *buf_im, \
                                             GwyDataField *psf, \
                                             GwyDataField *psf_fft, \
                                             const gdouble *param);

typedef enum {
    PSF_FUNC_GAUSSIAN = 0,
    PSF_FUNC_AGAUSSIAN = 1,
    PSF_FUNC_EXPONENTIAL = 2,
    PSF_FUNC_NFUNCTIONS,
} PSFFunctionType;

typedef struct {
    PSFFunctionType function;
    GwyAppDataId op1;
    GwyAppDataId op2;
} PSFArgs;

typedef gboolean (*PSFParamInitFunc)(GwyDataField *model_re,
                                     GwyDataField *model_im,
                                     GwyDataField *data_re,
                                     GwyDataField *data_im,
                                     GwyDataField *freq_x,
                                     GwyDataField *freq_y,
                                     gdouble *params);
typedef void (*PSFFillFunc)(GwyDataField *freq_x,
                            GwyDataField *freq_y,
                            GwyDataField *buf_re,
                            GwyDataField *buf_im,
                            GwyDataField *psf,
                            GwyDataField *psf_fft,
                            const gdouble *params);

typedef struct {
    const gchar *name;
    GwyNLFitIdxFunc func;
    GwyNLFitIdxDiffFunc diff;
    PSFParamInitFunc initpar;
    PSFFillFunc fill;
    guint nparams;
} PSFFunction;

typedef struct {
    guint xres;
    guint yres;
    gdouble *xfreq;
    gdouble *yfreq;
    gdouble *model_re;
    gdouble *model_im;
    gdouble *data_re;
    gdouble *data_im;
} PSFEstimateData;

typedef struct {
    PSFArgs *args;
    GtkWidget *chooser_op2;
    GtkWidget *function;
    GtkWidget *dialogue;
} PSFControls;

static gboolean   module_register     (void);
static void       psf                 (GwyContainer *data,
                                       GwyRunType run);
static gboolean   psf_dialogue        (PSFArgs *args);
static GtkWidget* create_function_menu(GCallback callback,
                                       gpointer cbdata,
                                       gint current);
static void       psf_data_changed    (GwyDataChooser *chooser,
                                       GwyAppDataId *object);
static gboolean   psf_data_filter     (GwyContainer *data,
                                       gint id,
                                       gpointer user_data);
static void       function_changed    (GtkComboBox *combo,
                                       PSFControls *controls);
static void       fit_psf             (GwyDataField *model,
                                       GwyDataField *data,
                                       GwyDataField *psf,
                                       GwyDataField *convolved,
                                       const PSFFunction *func);
static void       psf_load_args       (GwyContainer *container,
                                       PSFArgs *args);
static void       psf_save_args       (GwyContainer *container,
                                       PSFArgs *args);
static void       psf_sanitize_args   (PSFArgs *args);

DECLARE_PSF(gaussian);
DECLARE_PSF(agaussian);
DECLARE_PSF(exponential);

static GwyAppDataId op2_id = GWY_APP_DATA_ID_NONE;

static const PSFArgs psf_defaults = {
    PSF_FUNC_GAUSSIAN,
    GWY_APP_DATA_ID_NONE, GWY_APP_DATA_ID_NONE
};

static const PSFFunction functions[] = {
    {
        N_("Gaussian"),
        psf_gaussian_fit_func,
        psf_gaussian_fit_diff,
        psf_gaussian_init_params,
        psf_gaussian_fill_psf,
        2,
    },
    {
        N_("Gaussian (asymmetric)"),
        psf_agaussian_fit_func,
        psf_agaussian_fit_diff,
        psf_agaussian_init_params,
        psf_agaussian_fill_psf,
        3,
    },
    {
        N_("Frequency-space exponential"),
        psf_exponential_fit_func,
        psf_exponential_fit_diff,
        psf_exponential_init_params,
        psf_exponential_fill_psf,
        2,
    },
};

static GwyModuleInfo module_info = {
    GWY_MODULE_ABI_VERSION,
    &module_register,
    N_("Point spread function estimation by fitting explicit function form."),
    "Yeti <yeti@gwyddion.net>",
    "1.2",
    "David Nečas (Yeti) & Petr Klapetek",
    "2017",
};

GWY_MODULE_QUERY2(module_info, psf_fit)

static gboolean
module_register(void)
{
    gwy_process_func_register("psf-fit",
                              (GwyProcessFunc)&psf,
                              N_("/_Statistics/_PSF Fit..."),
                              NULL,
                              PSF_RUN_MODES,
                              GWY_MENU_FLAG_DATA,
                              N_("Fit PSF from known data and image"));

    return TRUE;
}

static void
psf(GwyContainer *data, GwyRunType run)
{
    GwyDataField *model, *dfield, *psf, *convolved;
    PSFArgs args;
    gboolean ok;
    GQuark quark;
    GwyContainer *mydata;
    gint newid;
    gdouble q;

    g_return_if_fail(run & PSF_RUN_MODES);

    psf_load_args(gwy_app_settings_get(), &args);

    gwy_app_data_browser_get_current(GWY_APP_DATA_FIELD_ID, &args.op1.id,
                                     GWY_APP_CONTAINER_ID, &args.op1.datano,
                                     0);

    ok = psf_dialogue(&args);
    psf_save_args(gwy_app_settings_get(), &args);
    if (!ok)
        return;

    mydata = gwy_app_data_browser_get(args.op1.datano);
    quark = gwy_app_get_data_key_for_id(args.op1.id);
    dfield = GWY_DATA_FIELD(gwy_container_get_object(mydata, quark));

    mydata = gwy_app_data_browser_get(args.op2.datano);
    quark = gwy_app_get_data_key_for_id(args.op2.id);
    model = GWY_DATA_FIELD(gwy_container_get_object(mydata, quark));

    psf = gwy_data_field_new_alike(dfield, FALSE);
    convolved = gwy_data_field_new_alike(dfield, FALSE);
    fit_psf(model, dfield, psf, convolved, functions + args.function);

    /* See psf.c for normalisation convention. */
    q = sqrt(gwy_data_field_get_xres(dfield)*gwy_data_field_get_yres(dfield))
        /(gwy_data_field_get_xreal(dfield)*gwy_data_field_get_yreal(dfield));

    gwy_data_field_multiply(psf, q);
    newid = gwy_app_data_browser_add_data_field(psf, data, TRUE);
    gwy_app_sync_data_items(data, data, args.op1.id, newid, FALSE,
                            GWY_DATA_ITEM_GRADIENT,
                            GWY_DATA_ITEM_MASK_COLOR,
                            0);
    gwy_app_set_data_field_title(data, newid, _("PSF"));
    gwy_app_channel_log_add_proc(data, args.op1.id, newid);

    newid = gwy_app_data_browser_add_data_field(convolved, data, TRUE);
    gwy_app_sync_data_items(data, data, args.op1.id, newid, FALSE,
                            GWY_DATA_ITEM_GRADIENT,
                            GWY_DATA_ITEM_MASK_COLOR,
                            0);
    gwy_app_set_data_field_title(data, newid, _("PSF*P"));
    gwy_app_channel_log_add_proc(data, args.op1.id, newid);

    g_object_unref(psf);
    g_object_unref(convolved);
}

static gboolean
psf_dialogue(PSFArgs *args)
{
    PSFControls controls;
    GtkWidget *dialogue, *table, *label;
    GwyDataChooser *chooser;
    gint response, row;

    controls.args = args;

    dialogue = gtk_dialog_new_with_buttons(_("Fit PSF"), NULL, 0,
                                           GTK_STOCK_CANCEL,
                                           GTK_RESPONSE_CANCEL,
                                           GTK_STOCK_OK,
                                           GTK_RESPONSE_OK,
                                           NULL);
    gtk_dialog_set_default_response(GTK_DIALOG(dialogue), GTK_RESPONSE_OK);
    gwy_help_add_to_proc_dialog(GTK_DIALOG(dialogue), GWY_HELP_DEFAULT);
    controls.dialogue = dialogue;

    table = gtk_table_new(2, 2, FALSE);
    gtk_table_set_row_spacings(GTK_TABLE(table), 2);
    gtk_table_set_col_spacings(GTK_TABLE(table), 6);
    gtk_container_set_border_width(GTK_CONTAINER(table), 4);
    gtk_box_pack_start(GTK_BOX(GTK_DIALOG(dialogue)->vbox), table,
                       TRUE, TRUE, 4);
    row = 0;

    label = gtk_label_new_with_mnemonic(_("_Ideal response:"));
    gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);
    gtk_table_attach(GTK_TABLE(table), label, 0, 1, row, row+1,
                     GTK_FILL, 0, 0, 0);

    controls.chooser_op2 = gwy_data_chooser_new_channels();
    chooser = GWY_DATA_CHOOSER(controls.chooser_op2);
    g_object_set_data(G_OBJECT(chooser), "dialog", dialogue);
    gwy_data_chooser_set_active_id(chooser, &args->op2);
    gwy_data_chooser_set_filter(chooser,
                                psf_data_filter, &args->op1, NULL);
    gtk_table_attach(GTK_TABLE(table), controls.chooser_op2, 1, 2, row, row+1,
                     GTK_FILL, 0, 0, 0);
    gtk_label_set_mnemonic_widget(GTK_LABEL(label), controls.chooser_op2);
    g_signal_connect(chooser, "changed",
                     G_CALLBACK(psf_data_changed), &args->op2);
    psf_data_changed(chooser, &args->op2);
    row++;

    label = gtk_label_new_with_mnemonic(_("_Function type:"));
    gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);
    gtk_table_attach(GTK_TABLE(table), label, 0, 1, row, row+1,
                     GTK_FILL, 0, 0, 0);

    controls.function = create_function_menu(G_CALLBACK(function_changed),
                                             &controls,
                                             args->function);
    gtk_table_attach(GTK_TABLE(table), controls.function, 1, 2, row, row+1,
                     GTK_FILL, 0, 0, 0);
    gtk_label_set_mnemonic_widget(GTK_LABEL(label), controls.function);
    row++;

    gtk_widget_show_all(dialogue);

    do {
        response = gtk_dialog_run(GTK_DIALOG(dialogue));
        switch (response) {
            case GTK_RESPONSE_CANCEL:
            case GTK_RESPONSE_DELETE_EVENT:
            gtk_widget_destroy(dialogue);
            case GTK_RESPONSE_NONE:
            return FALSE;
            break;

            case GTK_RESPONSE_OK:
            break;

            default:
            g_assert_not_reached();
            break;
        }
    } while (response != GTK_RESPONSE_OK);

    gtk_widget_destroy(dialogue);

    return TRUE;
}

static GtkWidget*
create_function_menu(GCallback callback, gpointer cbdata, gint current)
{
    static GwyEnum *entries = NULL;

    if (!entries) {
        guint i;

        entries = g_new(GwyEnum, PSF_FUNC_NFUNCTIONS);
        for (i = 0; i < PSF_FUNC_NFUNCTIONS; i++) {
            entries[i].value = i;
            entries[i].name = functions[i].name;
        }
    }

    return gwy_enum_combo_box_new(entries, PSF_FUNC_NFUNCTIONS,
                                  callback, cbdata, current, TRUE);
}

static void
psf_data_changed(GwyDataChooser *chooser, GwyAppDataId *object)
{
    GtkWidget *dialog;

    gwy_data_chooser_get_active_id(chooser, object);
    gwy_debug("data: %d %d", object->datano, object->id);

    dialog = g_object_get_data(G_OBJECT(chooser), "dialog");
    g_assert(GTK_IS_DIALOG(dialog));
    gtk_dialog_set_response_sensitive(GTK_DIALOG(dialog), GTK_RESPONSE_OK,
                                      object->datano);
}

static gboolean
psf_data_filter(GwyContainer *data, gint id, gpointer user_data)
{

    GwyAppDataId *object = (GwyAppDataId*)user_data;
    GwyDataField *op1, *op2;
    GQuark quark;

    quark = gwy_app_get_data_key_for_id(id);
    op1 = GWY_DATA_FIELD(gwy_container_get_object(data, quark));

    data = gwy_app_data_browser_get(object->datano);
    quark = gwy_app_get_data_key_for_id(object->id);
    op2 = GWY_DATA_FIELD(gwy_container_get_object(data, quark));

    if (op1 == op2)
        return FALSE;

    return !gwy_data_field_check_compatibility(op1, op2,
                                               GWY_DATA_COMPATIBILITY_RES
                                               | GWY_DATA_COMPATIBILITY_REAL
                                               | GWY_DATA_COMPATIBILITY_LATERAL);
}

static void
function_changed(GtkComboBox *combo, PSFControls *controls)
{
    controls->args->function = gwy_enum_combo_box_get_active(combo);
}

static gdouble
calculate_root_mean_square_complex(const gdouble *pre, const gdouble *pim,
                                   guint xres, guint yres)
{
    guint k;
    gdouble sum = 0.0;

    for (k = 0; k < xres*yres; k++)
        sum += pre[k]*pre[k] + pim[k]*pim[k];

    return sqrt(sum);
}

static void
precaulcate_frequencies(GwyDataField *model,
                        GwyDataField *freq_x, GwyDataField *freq_y)
{
    guint xres = gwy_data_field_get_xres(model),
          yres = gwy_data_field_get_yres(model);
    gdouble sx = 2.0*G_PI/gwy_data_field_get_xreal(model),
            sy = 2.0*G_PI/gwy_data_field_get_yreal(model);
    gdouble *fx = gwy_data_field_get_data(freq_x);
    gdouble *fy = gwy_data_field_get_data(freq_y);
    gdouble vx, vy;
    guint i, j;

    fx[0] = fy[0] = 0.0;

    for (j = 1; j <= xres/2; j++) {
        vx = j*sx;
        fx[xres-j] = -vx;
        fx[j] = vx;
        fy[j] = fy[xres-1] = 0.0;
    }

    for (i = 1; i <= yres/2; i++) {
        vy = i*sy;
        fx[i*xres] = fx[(yres-i)*xres] = 0.0;
        fy[(yres-i)*xres] = -vy;
        fy[i*xres] = vy;
    }

    for (i = 1; i <= yres/2; i++) {
        vy = i*sy;
        for (j = 1; j <= xres/2; j++) {
            vx = j*sx;
            fx[(yres-i)*xres + xres-j] = -vx;
            fx[i*xres + xres-j] = -vx;
            fx[(yres-i)*xres + j] = vx;
            fx[i*xres + j] = vx;
            fy[(yres-i)*xres + xres-j] = -vy;
            fy[(yres-i)*xres + j] = -vy;
            fy[i*xres + xres-j] = vy;
            fy[i*xres + j] = vy;
        }
    }
}

/* Suppress higher frequencies somewhat; they are lots of them and contain
 * noise.
 * Do not bother with weighting inside the fitting when we can just
 * premultiply model and data to achieve the same effect on cheap. */
static void
weight_fourier_components(GwyDataField *fftfield,
                          GwyDataField *freq_x, GwyDataField *freq_y)
{
    guint xres = gwy_data_field_get_xres(fftfield),
          yres = gwy_data_field_get_yres(fftfield);
    const gdouble *fx = gwy_data_field_get_data(freq_x);
    const gdouble *fy = gwy_data_field_get_data(freq_y);
    gdouble *d = gwy_data_field_get_data(fftfield);
    guint k = (yres/2)*xres + xres/2;
    gdouble fmax2 = fx[k]*fx[k] + fy[k]*fy[k];
    gdouble factor = 3.0/fmax2;

    for (k = 0; k < xres*yres; k++)
        d[k] /= 1.0 + factor*(fx[k]*fx[k] + fy[k]*fy[k]);
}

#ifdef DEBUG
static void
debug_print_params(const PSFFunction *func, const gdouble *params)
{
    GString *str = g_string_new(NULL);
    guint i;

    for (i = 0; i < func->nparams; i++)
        g_string_append_printf(str, " %g", params[i]);
    gwy_debug("params %s", str->str);
    g_string_free(str, TRUE);
}
#else
#define debug_print_params(func,params) /* */
#endif

static void
fit_psf(GwyDataField *model, GwyDataField *data,
        GwyDataField *psf, GwyDataField *convolved,
        const PSFFunction *func)
{
    PSFEstimateData psfedata;
    guint xres = gwy_data_field_get_xres(model),
          yres = gwy_data_field_get_yres(model);
    GwyDataField *model_re, *model_im, *data_re, *data_im, *freq_x, *freq_y;
    GwyNLFitter *fitter = NULL;
    gdouble rss;
    gdouble *params = g_new(gdouble, func->nparams);

    freq_x = gwy_data_field_new_alike(model, FALSE);
    freq_y = gwy_data_field_new_alike(model, FALSE);
    precaulcate_frequencies(model, freq_x, freq_y);

    model_re = gwy_data_field_new_alike(model, FALSE);
    model_im = gwy_data_field_new_alike(model, FALSE);
    gwy_data_field_2dfft_raw(model, NULL, model_re, model_im,
                             GWY_TRANSFORM_DIRECTION_FORWARD);

    data_re = gwy_data_field_new_alike(data, FALSE);
    data_im = gwy_data_field_new_alike(data, FALSE);
    gwy_data_field_2dfft_raw(data, NULL, data_re, data_im,
                             GWY_TRANSFORM_DIRECTION_FORWARD);

    /* There can be a constant offset between the two fields.  Ignore it. */
    gwy_data_field_set_val(model_re, 0, 0, 0.0);
    gwy_data_field_set_val(data_re, 0, 0, 0.0);

    if (!func->initpar(model_re, model_im, data_re, data_im,
                       freq_x, freq_y, params)) {
        g_warning("Initial parameter estimation failed.");
        gwy_data_field_clear(psf);
        gwy_data_field_set_val(psf, 0, 0, 1.0);
        goto fail;
    }

    weight_fourier_components(model_re, freq_x, freq_y);
    weight_fourier_components(model_im, freq_x, freq_y);
    weight_fourier_components(data_re, freq_x, freq_y);
    weight_fourier_components(data_im, freq_x, freq_y);

    psfedata.xres = xres;
    psfedata.yres = yres;
    psfedata.model_re = gwy_data_field_get_data(model_re);
    psfedata.model_im = gwy_data_field_get_data(model_im);
    psfedata.data_re = gwy_data_field_get_data(data_re);
    psfedata.data_im = gwy_data_field_get_data(data_im);
    psfedata.xfreq = gwy_data_field_get_data(freq_x);
    psfedata.yfreq = gwy_data_field_get_data(freq_y);

    fitter = gwy_math_nlfit_new_idx(func->func, func->diff);
    rss = gwy_math_nlfit_fit_idx(fitter, 2*xres*yres,
                                 func->nparams, params, &psfedata);
    gwy_debug("Fitted rss %g", rss);
    if (!(rss >= 0.0)) {
        g_warning("Initial parameter estimation failed.");
        gwy_data_field_clear(psf);
        gwy_data_field_set_val(psf, 0, 0, 1.0);
        goto fail;
    }
    debug_print_params(func, params);

    /* Use freq_x as a buffer for FFT(psf) */
    func->fill(freq_x, freq_y, data_re, data_im, psf, freq_x, params);
    /* Must do FFT again because we weighted the fields. */
    gwy_data_field_2dfft_raw(model, NULL, model_re, model_im,
                             GWY_TRANSFORM_DIRECTION_FORWARD);

    gwy_data_field_multiply_fields(data_re, model_re, freq_x);
    gwy_data_field_multiply_fields(data_im, model_im, freq_x);
    gwy_data_field_2dfft_raw(data_re, data_im, convolved, model_im,
                             GWY_TRANSFORM_DIRECTION_BACKWARD);

    set_transfer_function_units(model, data, psf);

fail:
    if (fitter)
        gwy_math_nlfit_free(fitter);
    g_free(params);
    g_object_unref(freq_x);
    g_object_unref(freq_y);
    g_object_unref(data_im);
    g_object_unref(data_re);
    g_object_unref(model_re);
    g_object_unref(model_im);
}

static gdouble
estimate_width(const gdouble *pre, const gdouble *pim,
               const gdouble *fx, const gdouble *fy,
               guint xres, guint yres)
{
    guint k;
    gdouble sum = 0.0;

    for (k = 0; k < xres*yres; k++)
        sum += (fx[k]*fx[k] + fy[k]*fy[k])*(pre[k]*pre[k] + pim[k]*pim[k]);

    return sqrt(sum);
}

/* We fit G*model on data, so our residuum function is G*model-data. */
static gdouble
psf_gaussian_fit_func(guint i, const gdouble *param, gpointer user_data,
                      gboolean *success)
{
    PSFEstimateData *psfedata = (PSFEstimateData*)user_data;
    guint k = i/2;
    gdouble A = param[0], width = param[1];
    gdouble fx = psfedata->xfreq[k], fy = psfedata->yfreq[k];
    gdouble r2 = (fx*fx + fy*fy)/(width*width);
    gdouble g, m, d;

    if (G_UNLIKELY(width == 0.0)) {
        *success = FALSE;
        return 0.0;
    }

    *success = TRUE;
    g = exp(-r2);
    m = (i % 2) ? psfedata->model_im[k] : psfedata->model_re[k];
    d = (i % 2) ? psfedata->data_im[k] : psfedata->data_re[k];
    return A*g*m - d;
}

static void
psf_gaussian_fit_diff(guint i,
                      const gdouble *param, const gboolean *fixed_param,
                      G_GNUC_UNUSED GwyNLFitIdxFunc func, gpointer user_data,
                      gdouble *der,
                      gboolean *success)
{
    PSFEstimateData *psfedata = (PSFEstimateData*)user_data;
    guint k = i/2;
    gdouble A = param[0], width = param[1];
    gdouble fx = psfedata->xfreq[k], fy = psfedata->yfreq[k];
    gdouble r2 = (fx*fx + fy*fy)/(width*width);
    gdouble g, m;

    if (G_UNLIKELY(width == 0.0)) {
        *success = FALSE;
        return;
    }

    *success = TRUE;
    g = exp(-r2);
    m = (i % 2) ? psfedata->model_im[k] : psfedata->model_re[k];
    der[0] = (fixed_param && fixed_param[0]) ? 0.0 : g*m;
    der[1] = (fixed_param && fixed_param[1]) ? 0.0 : 2.0*A/width*r2*g*m;
}

static gboolean
psf_gaussian_init_params(GwyDataField *model_re, GwyDataField *model_im,
                         GwyDataField *data_re, GwyDataField *data_im,
                         GwyDataField *freq_x, GwyDataField *freq_y,
                         gdouble *params)
{
    guint xres = gwy_data_field_get_xres(model_re),
          yres = gwy_data_field_get_yres(model_re);
    const gdouble *mre = gwy_data_field_get_data_const(model_re);
    const gdouble *mim = gwy_data_field_get_data_const(model_im);
    const gdouble *dre = gwy_data_field_get_data_const(data_re);
    const gdouble *dim = gwy_data_field_get_data_const(data_im);
    const gdouble *fx = gwy_data_field_get_data_const(freq_x);
    const gdouble *fy = gwy_data_field_get_data_const(freq_y);
    gdouble q_model, q_data, w_model, w_data;

    /* Amplitude. */
    q_model = calculate_root_mean_square_complex(mre, mim, xres, yres);
    q_data = calculate_root_mean_square_complex(dre, dim, xres, yres);
    if (!q_model || !q_data)
        params[0] = 0.0;
    else
        params[0] = q_data/q_model;
    gwy_debug("q_model %g, q_data %g => amplitude %g",
              q_model, q_data, params[0]);

    /* Width. */
    w_model = estimate_width(mre, mim, fx, fy, xres, yres)/q_model;
    w_data = estimate_width(dre, dim, fx, fy, xres, yres)/q_data;
    params[1] = 0.7*sqrt(fmax(w_model*w_model - w_data*w_data, 0.0))
                + 0.3*MIN(w_model, w_data);
    gwy_debug("w_model %g, w_data %g => width %g",
              w_model, w_data, params[1]);

    return params[0] > 0.0 && params[1] > 0.0;
}

static void
psf_gaussian_fill_psf(GwyDataField *freq_x, GwyDataField *freq_y,
                      G_GNUC_UNUSED GwyDataField *buf_re, GwyDataField *buf_im,
                      GwyDataField *psf, GwyDataField *psf_fft,
                      const gdouble *param)
{
    gdouble A = param[0], w = param[1];
    guint xres = gwy_data_field_get_xres(freq_x),
          yres = gwy_data_field_get_yres(freq_x);
    const gdouble *fx = gwy_data_field_get_data_const(freq_x);
    const gdouble *fy = gwy_data_field_get_data_const(freq_y);
    gdouble *pf = gwy_data_field_get_data(psf_fft);
    gdouble r2, g;
    guint k;

    for (k = 0; k < xres*yres; k++) {
        r2 = (fx[k]*fx[k] + fy[k]*fy[k])/(w*w);
        g = exp(-r2);
        pf[k] = A*g;
    }

    gwy_data_field_2dfft_raw(psf_fft, NULL, psf, buf_im,
                             GWY_TRANSFORM_DIRECTION_BACKWARD);
    gwy_data_field_2dfft_humanize(psf);
}

static gdouble
psf_agaussian_fit_func(guint i, const gdouble *param, gpointer user_data,
                       gboolean *success)
{
    PSFEstimateData *psfedata = (PSFEstimateData*)user_data;
    guint k = i/2;
    gdouble A = param[0], widthx = param[1], widthy = param[2];
    gdouble fx = psfedata->xfreq[k]/widthx, fy = psfedata->yfreq[k]/widthy;
    gdouble r2 = fx*fx + fy*fy;
    gdouble g, m, d;

    if (G_UNLIKELY(widthx == 0.0 || widthy == 0.0)) {
        *success = FALSE;
        return 0.0;
    }

    *success = TRUE;
    g = exp(-r2);
    m = (i % 2) ? psfedata->model_im[k] : psfedata->model_re[k];
    d = (i % 2) ? psfedata->data_im[k] : psfedata->data_re[k];
    return A*g*m - d;
}

static void
psf_agaussian_fit_diff(guint i,
                       const gdouble *param, const gboolean *fixed_param,
                       G_GNUC_UNUSED GwyNLFitIdxFunc func, gpointer user_data,
                       gdouble *der,
                       gboolean *success)
{
    PSFEstimateData *psfedata = (PSFEstimateData*)user_data;
    guint k = i/2;
    gdouble A = param[0], widthx = param[1], widthy = param[2];
    gdouble fx = psfedata->xfreq[k]/widthx, fy = psfedata->yfreq[k]/widthy;
    gdouble r2 = fx*fx + fy*fy;
    gdouble g, m;

    if (G_UNLIKELY(widthx == 0.0 || widthy == 0.0)) {
        *success = FALSE;
        return;
    }

    *success = TRUE;
    g = exp(-r2);
    m = (i % 2) ? psfedata->model_im[k] : psfedata->model_re[k];
    der[0] = (fixed_param && fixed_param[0]) ? 0.0 : g*m;
    der[1] = (fixed_param && fixed_param[1]) ? 0.0 : 2.0*A/widthx*fx*fx*g*m;
    der[2] = (fixed_param && fixed_param[2]) ? 0.0 : 2.0*A/widthy*fy*fy*g*m;
}

static gboolean
psf_agaussian_init_params(GwyDataField *model_re, GwyDataField *model_im,
                          GwyDataField *data_re, GwyDataField *data_im,
                          GwyDataField *freq_x, GwyDataField *freq_y,
                          gdouble *params)
{
    if (!psf_gaussian_init_params(model_re, model_im, data_re, data_im,
                                  freq_x, freq_y, params))
        return FALSE;

    params[2] = params[1];
    return TRUE;
}

static void
psf_agaussian_fill_psf(GwyDataField *freq_x, GwyDataField *freq_y,
                       G_GNUC_UNUSED GwyDataField *buf_re, GwyDataField *buf_im,
                       GwyDataField *psf, GwyDataField *psf_fft,
                       const gdouble *param)
{
    gdouble A = param[0], wx = param[1], wy = param[2];
    guint xres = gwy_data_field_get_xres(freq_x),
          yres = gwy_data_field_get_yres(freq_x);
    const gdouble *fx = gwy_data_field_get_data_const(freq_x);
    const gdouble *fy = gwy_data_field_get_data_const(freq_y);
    gdouble *pf = gwy_data_field_get_data(psf_fft);
    gdouble r2, g;
    guint k;

    for (k = 0; k < xres*yres; k++) {
        r2 = fx[k]*fx[k]/(wx*wx) + fy[k]*fy[k]/(wy*wy);
        g = exp(-r2);
        pf[k] = A*g;
    }

    gwy_data_field_2dfft_raw(psf_fft, NULL, psf, buf_im,
                             GWY_TRANSFORM_DIRECTION_BACKWARD);
    gwy_data_field_2dfft_humanize(psf);
}

/* We fit G*model on data, so our residuum function is G*model-data. */
static gdouble
psf_exponential_fit_func(guint i, const gdouble *param, gpointer user_data,
                         gboolean *success)
{
    PSFEstimateData *psfedata = (PSFEstimateData*)user_data;
    guint k = i/2;
    gdouble A = param[0], width = param[1];
    gdouble fx = psfedata->xfreq[k], fy = psfedata->yfreq[k];
    gdouble r2 = (fx*fx + fy*fy)/(width*width);
    gdouble g, m, d;

    if (G_UNLIKELY(width == 0.0)) {
        *success = FALSE;
        return 0.0;
    }

    *success = TRUE;
    g = exp(-sqrt(r2));
    m = (i % 2) ? psfedata->model_im[k] : psfedata->model_re[k];
    d = (i % 2) ? psfedata->data_im[k] : psfedata->data_re[k];
    return A*g*m - d;
}

static void
psf_exponential_fit_diff(guint i,
                         const gdouble *param, const gboolean *fixed_param,
                         G_GNUC_UNUSED GwyNLFitIdxFunc func, gpointer user_data,
                         gdouble *der,
                         gboolean *success)
{
    PSFEstimateData *psfedata = (PSFEstimateData*)user_data;
    guint k = i/2;
    gdouble A = param[0], width = param[1];
    gdouble fx = psfedata->xfreq[k], fy = psfedata->yfreq[k];
    gdouble r2 = (fx*fx + fy*fy)/(width*width);
    gdouble g, m;

    if (G_UNLIKELY(width == 0.0)) {
        *success = FALSE;
        return;
    }

    *success = TRUE;
    g = exp(-sqrt(r2));
    m = (i % 2) ? psfedata->model_im[k] : psfedata->model_re[k];
    der[0] = (fixed_param && fixed_param[0]) ? 0.0 : g*m;
    der[1] = (fixed_param && fixed_param[1]) ? 0.0 : 2.0*A/width*r2*g*m;
}

static gboolean
psf_exponential_init_params(GwyDataField *model_re, GwyDataField *model_im,
                            GwyDataField *data_re, GwyDataField *data_im,
                            GwyDataField *freq_x, GwyDataField *freq_y,
                            gdouble *params)
{
    guint xres = gwy_data_field_get_xres(model_re),
          yres = gwy_data_field_get_yres(model_re);
    const gdouble *mre = gwy_data_field_get_data_const(model_re);
    const gdouble *mim = gwy_data_field_get_data_const(model_im);
    const gdouble *dre = gwy_data_field_get_data_const(data_re);
    const gdouble *dim = gwy_data_field_get_data_const(data_im);
    const gdouble *fx = gwy_data_field_get_data_const(freq_x);
    const gdouble *fy = gwy_data_field_get_data_const(freq_y);
    gdouble q_model, q_data, w_model, w_data;

    /* Amplitude. */
    q_model = calculate_root_mean_square_complex(mre, mim, xres, yres);
    q_data = calculate_root_mean_square_complex(dre, dim, xres, yres);
    if (!q_model || !q_data)
        params[0] = 0.0;
    else
        params[0] = q_data/q_model;
    gwy_debug("q_model %g, q_data %g => amplitude %g",
              q_model, q_data, params[0]);

    /* Width. */
    w_model = estimate_width(mre, mim, fx, fy, xres, yres)/q_model;
    w_data = estimate_width(dre, dim, fx, fy, xres, yres)/q_data;
    params[1] = 0.7*sqrt(fmax(w_model*w_model - w_data*w_data, 0.0))
                + 0.3*MIN(w_model, w_data);
    gwy_debug("w_model %g, w_data %g => width %g",
              w_model, w_data, params[1]);

    return params[0] > 0.0 && params[1] > 0.0;
}

static void
psf_exponential_fill_psf(GwyDataField *freq_x, GwyDataField *freq_y,
                         G_GNUC_UNUSED GwyDataField *buf_re,
                         GwyDataField *buf_im,
                         GwyDataField *psf, GwyDataField *psf_fft,
                         const gdouble *param)
{
    gdouble A = param[0], w = param[1];
    guint xres = gwy_data_field_get_xres(freq_x),
          yres = gwy_data_field_get_yres(freq_x);
    const gdouble *fx = gwy_data_field_get_data_const(freq_x);
    const gdouble *fy = gwy_data_field_get_data_const(freq_y);
    gdouble *pf = gwy_data_field_get_data(psf_fft);
    gdouble r2, g;
    guint k;

    for (k = 0; k < xres*yres; k++) {
        r2 = (fx[k]*fx[k] + fy[k]*fy[k])/(w*w);
        g = exp(-sqrt(r2));
        pf[k] = A*g;
    }

    gwy_data_field_2dfft_raw(psf_fft, NULL, psf, buf_im,
                             GWY_TRANSFORM_DIRECTION_BACKWARD);
    gwy_data_field_2dfft_humanize(psf);
}

static const gchar function_key[] = "/module/psf-fit/function";

static void
psf_sanitize_args(PSFArgs *args)
{
    args->function = CLAMP(args->function, 0, PSF_FUNC_NFUNCTIONS-1);
    gwy_app_data_id_verify_channel(&args->op2);
}

static void
psf_load_args(GwyContainer *container,
              PSFArgs *args)
{
    *args = psf_defaults;

    gwy_container_gis_enum_by_name(container, function_key, &args->function);
    args->op2 = op2_id;
    psf_sanitize_args(args);
}

static void
psf_save_args(GwyContainer *container,
              PSFArgs *args)
{
    op2_id = args->op2;

    gwy_container_set_enum_by_name(container, function_key, args->function);
}

/* vim: set cin et ts=4 sw=4 cino=>1s,e0,n0,f0,{0,}0,^0,\:1s,=0,g1s,h0,t0,+1s,c3,(0,u0 : */
