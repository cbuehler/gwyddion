/*
 *  $Id$
 *  Copyright (C) 2007-2017 David Necas (Yeti).
 *  E-mail: yeti@gwyddion.net.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA 02110-1301, USA.
 */

#include "config.h"
#include <stdlib.h>
#include <gtk/gtk.h>
#include <libgwyddion/gwymacros.h>
#include <libgwyddion/gwymath.h>
#include <libprocess/level.h>
#include <libprocess/filters.h>
#include <libprocess/grains.h>
#include <libprocess/stats.h>
#include <libgwydgets/gwystock.h>
#include <libgwydgets/gwycombobox.h>
#include <libgwymodule/gwymodule-process.h>
#include <app/gwymoduleutils.h>
#include <app/gwyapp.h>
#include "preview.h"

#define ACF2D_RUN_MODES (GWY_RUN_IMMEDIATE | GWY_RUN_INTERACTIVE)

/* Keep it simple and use a predefined set of zooms, these seem suitable. */
typedef enum {
    ZOOM_1 = 1,
    ZOOM_4 = 4,
    ZOOM_16 = 16
} ZoomType;

enum {
    GRAPH_WIDTH = 320,
    NLINES = 12,
    MIN_RESOLUTION = 4,
    MAX_RESOLUTION = 16384,
    MAX_THICKNESS = 128,
};

typedef enum {
    LEVELLING_NONE = 0,
    LEVELLING_MEAN_VALUE = 1,
    LEVELLING_MEAN_PLANE = 2,
    LEVELLING_NTYPES,
} LevellingType;

typedef struct {
    GwyResultsReportType report_style;
    GwyMaskingType masking;
    LevellingType level;
    gdouble threshold;
    ZoomType zoom;
    /* Graphing */
    gboolean separate;
    gboolean fixres;
    gint resolution;
    gint thickness;
    GwyInterpolationType interpolation;
    GwyAppDataId target_graph;
} ACFArgs;

static const gchar *guivalues[] = {
    "Sal", "tau2", "phi1", "phi2", "Str",
};

typedef struct {
    ACFArgs *args;
    GwyResults *results;
    GtkWidget *dialogue;
    GwyContainer *mydata;
    GtkWidget *view;
    GwyDataField *dfield;
    GwyDataField *mask;
    GwyDataField *acf;
    GwyDataField *acfmask;
    GSList *level;
    GSList *masking;
    GSList *zoom;
    GtkObject *threshold;
    GtkWidget *colour_button;
    GtkWidget *guivalues[G_N_ELEMENTS(guivalues)];
    GtkWidget *rexport;
    /* Graphing */
    GtkObject *resolution;
    GtkWidget *fixres;
    GtkObject *thickness;
    GtkWidget *interpolation;
    GwySelection *selection;
    GtkWidget *graph;
    GwyDataLine *line;
    GwyGraphModel *gmodel;
    GtkWidget *separate;
    GtkWidget *target_graph;
} ACFControls;

static gboolean module_register         (void);
static void     acf2d                   (GwyContainer *data,
                                         GwyRunType run);
static void     acf2d_dialogue          (ACFArgs *args,
                                         GwyContainer *data,
                                         GwyDataField *dfield,
                                         GwyDataField *mask,
                                         gint id);
static void     masking_changed         (GtkToggleButton *toggle,
                                         ACFControls *controls);
static void     levelling_changed       (GtkToggleButton *toggle,
                                         ACFControls *controls);
static void     threshold_changed       (ACFControls *controls,
                                         GtkAdjustment *adjustment);
static void     calculate_zoomed_fields (ACFControls *controls);
static void     recalculate_acf         (ACFControls *controls);
static void     update_parameters       (ACFControls *controls);
static void     fixres_changed          (ACFControls *controls,
                                         GtkToggleButton *check);
static void     zoom_changed            (GtkRadioButton *button,
                                         ACFControls *controls);
static void     resolution_changed      (ACFControls *controls,
                                         GtkAdjustment *adj);
static void     thickness_changed       (ACFControls *controls,
                                         GtkAdjustment *adj);
static void     interpolation_changed   (GtkComboBox *combo,
                                         ACFControls *controls);
static void     separate_changed        (ACFControls *controls);
static void     selection_changed       (ACFControls *controls,
                                         gint hint);
static void     update_target_graphs    (ACFControls *controls);
static gboolean filter_target_graphs    (GwyContainer *data,
                                         gint id,
                                         gpointer user_data);
static void     target_graph_changed    (ACFControls *controls);
static void     report_style_changed    (ACFControls *controls,
                                         GwyResultsExport *rexport);
static void     update_curve            (ACFControls *controls,
                                         gint i);
static void     init_graph_model_units  (GwyGraphModel *gmodel,
                                         GwyDataField *psdffield);
static void     calculate_acf           (GwyDataField *dfield,
                                         GwyDataField *mask,
                                         const ACFArgs *args,
                                         GwyDataField *acf,
                                         GwyDataField *acfmask);
static void     create_acf_mask         (GwyDataField *acf,
                                         GwyDataField *acfmask,
                                         const ACFArgs *args);
static void     create_output_graphs    (ACFControls *controls,
                                         GwyContainer *data);
static gint     create_results          (GwyContainer *data,
                                         GwyDataField *acf,
                                         GwyDataField *acfmask,
                                         gint oldid);
static void     acf2d_load_args         (GwyContainer *container,
                                         ACFArgs *args);
static void     acf2d_save_args         (GwyContainer *container,
                                         ACFArgs *args);
static void     acf2d_sanitize_args     (ACFArgs *args);
static void     calculate_acf_statistics(GwyDataField *acfmask,
                                         GwyResults *results);

static const ACFArgs acf2d_defaults = {
    GWY_RESULTS_REPORT_COLON,
    GWY_MASK_IGNORE, LEVELLING_MEAN_VALUE, 0.2,
    ZOOM_1,
    FALSE, FALSE, 120, 1,
    GWY_INTERPOLATION_LINEAR,
    GWY_APP_DATA_ID_NONE,
};

static GwyAppDataId target_id = GWY_APP_DATA_ID_NONE;

static GwyModuleInfo module_info = {
    GWY_MODULE_ABI_VERSION,
    &module_register,
    N_("Calculates and analyzes two-dimensional autocorrelation function."),
    "Yeti <yeti@gwyddion.net>",
    "2.0",
    "David Nečas (Yeti)",
    "2007",
};

GWY_MODULE_QUERY2(module_info, acf2d)

static gboolean
module_register(void)
{
    gwy_process_func_register("acf2d",
                              (GwyProcessFunc)&acf2d,
                              N_("/_Statistics/2D Auto_correlation..."),
                              NULL,
                              ACF2D_RUN_MODES,
                              GWY_MENU_FLAG_DATA,
                              N_("Calculate 2D autocorrelation function"));

    return TRUE;
}

static void
acf2d(GwyContainer *data, GwyRunType run)
{
    GwyDataField *dfield, *mask, *acf, *acfmask;
    ACFArgs args;
    gint id, newid;

    g_return_if_fail(run & ACF2D_RUN_MODES);
    acf2d_load_args(gwy_app_settings_get(), &args);
    gwy_app_data_browser_get_current(GWY_APP_DATA_FIELD, &dfield,
                                     GWY_APP_MASK_FIELD, &mask,
                                     GWY_APP_DATA_FIELD_ID, &id,
                                     0);
    g_return_if_fail(dfield);

    if (run == GWY_RUN_IMMEDIATE) {
        acf = gwy_data_field_new(1, 1, 1.0, 1.0, FALSE);
        acfmask = gwy_data_field_new(1, 1, 1.0, 1.0, FALSE);
        calculate_acf(dfield, mask, &args, acf, acfmask);
        newid = create_results(data, acf, acfmask, id);
        gwy_app_sync_data_items(data, data, id, newid, FALSE,
                                GWY_DATA_ITEM_MASK_COLOR,
                                0);
        g_object_unref(acf);
        g_object_unref(acfmask);
    }
    else {
        acf2d_dialogue(&args, data, dfield, mask, id);
        acf2d_save_args(gwy_app_settings_get(), &args);
    }
}

static void
acf2d_dialogue(ACFArgs *args,
               GwyContainer *data,
               GwyDataField *dfield,
               GwyDataField *mask,
               gint id)
{
    ACFControls controls;
    GtkWidget *hbox, *vbox, *notebook, *label, *hbox2;
    GwyDataChooser *chooser;
    GtkTable *table;
    GtkDialog *dialogue;
    GwyGraph *graph;
    GwyGraphArea *area;
    GwyResults *results;
    GwyResultsExport *rexport;
    gint response, row, newid;
    guint i;
    GString *str;
    GSList *l;

    gwy_clear(&controls, 1);
    controls.args = args;
    controls.dfield = dfield;
    controls.mask = mask;
    controls.acf = gwy_data_field_new(1, 1, 1.0, 1.0, FALSE);
    controls.acfmask = gwy_data_field_new(1, 1, 1.0, 1.0, FALSE);
    calculate_acf(dfield, mask, args, controls.acf, controls.acfmask);

    controls.results = results = gwy_results_new();
    gwy_results_add_header(results, N_("Autocorrelation Function"));
    gwy_results_add_value_str(results, "file", N_("File"));
    gwy_results_add_value_str(results, "image", N_("Image"));
    gwy_results_add_value_yesno(results, "masking", N_("Mask in use"));
    gwy_results_add_separator(results);

    gwy_results_add_value(results, "Sal", N_("Autocorrelation length"),
                          "power-x", 1, "symbol", "S<sub>al</sub>", NULL);
    gwy_results_add_value_x(results, "tau2", N_("Slowest decay length"));
    gwy_results_add_value_angle(results, "phi1", N_("Fastest decay direction"));
    gwy_results_add_value_angle(results, "phi2", N_("Slowest decay direction"));
    gwy_results_add_value(results, "Str", N_("Texture aspect ratio"),
                          "symbol", "S<sub>tr</sub>", NULL);

    gwy_results_set_unit(results, "x", gwy_data_field_get_si_unit_xy(dfield));
    gwy_results_set_unit(results, "y", gwy_data_field_get_si_unit_xy(dfield));
    gwy_results_set_unit(results, "z", gwy_data_field_get_si_unit_z(dfield));
    gwy_results_fill_filename(results, "file", data);
    gwy_results_fill_channel(results, "image", data, id);

    controls.dialogue
        = gtk_dialog_new_with_buttons(_("Autocorrelation Function"), NULL, 0,
                                      GTK_STOCK_CLEAR, RESPONSE_CLEAR,
                                      GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
                                      GTK_STOCK_OK, GTK_RESPONSE_OK,
                                      NULL);
    dialogue = GTK_DIALOG(controls.dialogue);
    gtk_dialog_set_default_response(dialogue, GTK_RESPONSE_OK);
    gwy_help_add_to_proc_dialog(GTK_DIALOG(dialogue), GWY_HELP_DEFAULT);

    hbox = gtk_hbox_new(FALSE, 2);

    gtk_box_pack_start(GTK_BOX(GTK_DIALOG(dialogue)->vbox), hbox,
                       FALSE, FALSE, 4);

    /***** ACF preview *****/
    controls.mydata = gwy_container_new();
    calculate_zoomed_fields(&controls);
    gwy_app_sync_data_items(data, controls.mydata, id, 0, FALSE,
                            GWY_DATA_ITEM_PALETTE,
                            GWY_DATA_ITEM_MASK_COLOR,
                            GWY_DATA_ITEM_REAL_SQUARE,
                            0);
    controls.view = create_preview(controls.mydata, 0, PREVIEW_SIZE, TRUE);
    controls.selection = create_vector_layer(GWY_DATA_VIEW(controls.view), 0,
                                             "Point", TRUE);
    gwy_selection_set_max_objects(controls.selection, NLINES);
    g_object_set(gwy_data_view_get_top_layer(GWY_DATA_VIEW(controls.view)),
                 "draw-as-vector", TRUE,
                 NULL);
    g_signal_connect_swapped(controls.selection, "changed",
                             G_CALLBACK(selection_changed), &controls);

    gtk_box_pack_start(GTK_BOX(hbox), controls.view, FALSE, FALSE, 4);

    /***** Graph *****/
    vbox = gtk_vbox_new(FALSE, 0);
    gtk_box_pack_start(GTK_BOX(hbox), vbox, TRUE, TRUE, 4);

    controls.gmodel = gwy_graph_model_new();
    g_object_set(controls.gmodel,
                 "title", _("ACF Section"),
                 "axis-label-bottom", "τ",
                 "axis-label-left", "G",
                 NULL);
    init_graph_model_units(controls.gmodel, controls.acf);
    controls.line = gwy_data_line_new(1, 1.0, FALSE);

    controls.graph = gwy_graph_new(controls.gmodel);
    graph = GWY_GRAPH(controls.graph);
    gtk_widget_set_size_request(controls.graph, GRAPH_WIDTH, -1);
    gwy_graph_set_axis_visible(graph, GTK_POS_LEFT, FALSE);
    gwy_graph_set_axis_visible(graph, GTK_POS_RIGHT, FALSE);
    gwy_graph_set_axis_visible(graph, GTK_POS_TOP, FALSE);
    gwy_graph_set_axis_visible(graph, GTK_POS_BOTTOM, FALSE);
    gwy_graph_enable_user_input(graph, FALSE);
    area = GWY_GRAPH_AREA(gwy_graph_get_area(graph));
    gwy_graph_area_enable_user_input(area, FALSE);
    gtk_box_pack_start(GTK_BOX(vbox), controls.graph, TRUE, TRUE, 0);

    notebook = gtk_notebook_new();
    gtk_box_pack_start(GTK_BOX(vbox), notebook, FALSE, FALSE, 0);

    /***** ACF controls *****/
    table = GTK_TABLE(gtk_table_new(6 + 4*(!!mask), 3, FALSE));
    gtk_table_set_row_spacings(table, 2);
    gtk_table_set_col_spacings(table, 6);
    gtk_container_set_border_width(GTK_CONTAINER(table), 4);
    gtk_notebook_append_page(GTK_NOTEBOOK(notebook), GTK_WIDGET(table),
                             gtk_label_new("ACF"));
    row = 0;

    hbox2 = gtk_hbox_new(FALSE, 8);
    gtk_table_attach(table, hbox2, 0, 4, row, row+1, GTK_FILL, 0, 0, 0);
    label = gtk_label_new(_("Zoom:"));
    gtk_box_pack_start(GTK_BOX(hbox2), label, FALSE, FALSE, 0);

    controls.zoom = gwy_radio_buttons_createl(G_CALLBACK(zoom_changed),
                                              &controls,
                                              args->zoom,
                                              "1×", ZOOM_1,
                                              "4×", ZOOM_4,
                                              "16×", ZOOM_16,
                                              NULL);
    for (l = controls.zoom; l; l = g_slist_next(l)) {
        GtkWidget *widget = GTK_WIDGET(l->data);
        gtk_box_pack_start(GTK_BOX(hbox2), widget, FALSE, FALSE, 0);
    }
    row++;

    label = gtk_label_new(_("Data adjustment:"));
    gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);
    gtk_table_attach(table, label, 0, 2, row, row+1, GTK_FILL, 0, 0, 0);
    row++;

    controls.level
        = gwy_radio_buttons_createl(G_CALLBACK(levelling_changed), &controls,
                                    args->level,
                                    gwy_sgettext("data-adjustment|None"),
                                    LEVELLING_NONE,
                                    _("Mean value subtraction"),
                                    LEVELLING_MEAN_VALUE,
                                    _("Plane leveling"),
                                    LEVELLING_MEAN_PLANE,
                                    NULL);
    row = gwy_radio_buttons_attach_to_table(controls.level, table, 2, row);

    gtk_table_set_row_spacing(table, row-1, 8);
    if (mask) {
        gtk_table_attach(table,
                         gwy_label_new_header(_("Masking Mode")),
                         0, 2, row, row+1, GTK_FILL, 0, 0, 0);
        row++;

        controls.masking
            = gwy_radio_buttons_create(gwy_masking_type_get_enum(), -1,
                                       G_CALLBACK(masking_changed),
                                       &controls, args->masking);
        row = gwy_radio_buttons_attach_to_table(controls.masking, table,
                                                2, row);
        gtk_table_set_row_spacing(table, row-1, 8);
    }
    else
        controls.masking = NULL;

    /***** Graph controls *****/
    table = GTK_TABLE(gtk_table_new(5, 3, FALSE));
    gtk_table_set_row_spacings(table, 2);
    gtk_table_set_col_spacings(table, 6);
    gtk_container_set_border_width(GTK_CONTAINER(table), 4);
    gtk_notebook_append_page(GTK_NOTEBOOK(notebook), GTK_WIDGET(table),
                             gtk_label_new("Graph"));
    row = 0;

    controls.resolution = gtk_adjustment_new(controls.args->resolution,
                                             MIN_RESOLUTION, MAX_RESOLUTION,
                                             1, 10, 0);
    gwy_table_attach_adjbar(GTK_WIDGET(table), row,
                            _("_Fixed resolution:"), NULL,
                            controls.resolution,
                            GWY_HSCALE_CHECK | GWY_HSCALE_SQRT);
    g_signal_connect_swapped(controls.resolution, "value-changed",
                             G_CALLBACK(resolution_changed), &controls);
    controls.fixres = gwy_table_hscale_get_check(controls.resolution);
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(controls.fixres),
                                 controls.args->fixres);
    g_signal_connect_swapped(controls.fixres, "toggled",
                             G_CALLBACK(fixres_changed), &controls);
    row++;

    controls.thickness = gtk_adjustment_new(controls.args->thickness,
                                            1, MAX_THICKNESS, 1, 10, 0);
    gwy_table_attach_adjbar(GTK_WIDGET(table), row, _("_Thickness:"), _("px"),
                            controls.thickness, GWY_HSCALE_DEFAULT);
    g_signal_connect_swapped(controls.thickness, "value-changed",
                             G_CALLBACK(thickness_changed), &controls);
    row++;

    controls.separate = gtk_check_button_new_with_mnemonic(_("_Separate curves"));
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(controls.separate),
                                 args->separate);
    gtk_table_attach(table, controls.separate,
                     0, 2, row, row+1, GTK_FILL, 0, 0, 0);
    g_signal_connect_swapped(controls.separate, "toggled",
                             G_CALLBACK(separate_changed), &controls);
    row++;

    controls.interpolation
        = gwy_enum_combo_box_new(gwy_interpolation_type_get_enum(), -1,
                                 G_CALLBACK(interpolation_changed), &controls,
                                 controls.args->interpolation, TRUE);
    gwy_table_attach_adjbar(GTK_WIDGET(table), row,
                            _("_Interpolation type:"), NULL,
                            GTK_OBJECT(controls.interpolation),
                            GWY_HSCALE_WIDGET_NO_EXPAND);
    row++;

    controls.target_graph = gwy_data_chooser_new_graphs();
    chooser = GWY_DATA_CHOOSER(controls.target_graph);
    gwy_data_chooser_set_none(chooser, _("New graph"));
    gwy_data_chooser_set_active(chooser, NULL, -1);
    gwy_data_chooser_set_filter(chooser, filter_target_graphs, &controls, NULL);
    gwy_data_chooser_set_active_id(chooser, &args->target_graph);
    gwy_data_chooser_get_active_id(chooser, &args->target_graph);
    gwy_table_attach_adjbar(GTK_WIDGET(table), row,
                            _("Target _graph:"), NULL,
                            GTK_OBJECT(controls.target_graph),
                            GWY_HSCALE_WIDGET_NO_EXPAND);
    g_signal_connect_swapped(controls.target_graph, "changed",
                             G_CALLBACK(target_graph_changed), &controls);
    row++;

    /***** Parameters *****/
    table = GTK_TABLE(gtk_table_new(8, 3, FALSE));
    gtk_table_set_row_spacings(table, 2);
    gtk_table_set_col_spacings(table, 6);
    gtk_container_set_border_width(GTK_CONTAINER(table), 4);
    gtk_notebook_append_page(GTK_NOTEBOOK(notebook), GTK_WIDGET(table),
                             gtk_label_new("Parameters"));
    row = 0;

    controls.threshold = gtk_adjustment_new(args->threshold,
                                            0.0, 1.0, 0.001, 0.1, 0);
    gwy_table_attach_adjbar(GTK_WIDGET(table), row++, _("_Threshold"), NULL,
                            controls.threshold, GWY_HSCALE_LINEAR);
    g_signal_connect_swapped(controls.threshold, "value-changed",
                             G_CALLBACK(threshold_changed), &controls);

    controls.colour_button = create_mask_color_button(controls.mydata,
                                                      GTK_WIDGET(dialogue), 0);
    gwy_table_attach_adjbar(GTK_WIDGET(table), row++, _("_Mask color:"), NULL,
                            GTK_OBJECT(controls.colour_button),
                            GWY_HSCALE_WIDGET_NO_EXPAND);

    str = g_string_new(NULL);
    for (i = 0; i < G_N_ELEMENTS(guivalues); i++) {
        g_string_assign(str, gwy_results_get_label_with_symbol(results,
                                                               guivalues[i]));
        g_string_append_c(str, ':');
        label = gtk_label_new(str->str);
        gtk_label_set_use_markup(GTK_LABEL(label), TRUE);
        gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);
        gtk_table_attach(GTK_TABLE(table), label, 0, 1, row, row+1,
                         GTK_FILL, 0, 0, 0);

        label = controls.guivalues[i] = gtk_label_new(NULL);
        gtk_misc_set_alignment(GTK_MISC(label), 1.0, 0.5);
        gtk_label_set_selectable(GTK_LABEL(label), TRUE);
        gtk_table_attach(GTK_TABLE(table), label, 1, 2, row, row+1,
                         GTK_EXPAND | GTK_FILL, 0, 0, 0);
        row++;
    }
    g_string_free(str, TRUE);

    controls.rexport = gwy_results_export_new(args->report_style);
    rexport = GWY_RESULTS_EXPORT(controls.rexport);
    gwy_results_export_set_title(rexport, _("Save Parameters"));
    gwy_results_export_set_results(rexport, results);
    gtk_table_attach(table, controls.rexport, 0, 3, row, row+1,
                     GTK_FILL, 0, 0, 0);
    g_signal_connect_swapped(rexport, "format-changed",
                             G_CALLBACK(report_style_changed), &controls);

    update_parameters(&controls);

    gtk_widget_show_all(controls.dialogue);
    do {
        response = gtk_dialog_run(dialogue);
        switch (response) {
            case GTK_RESPONSE_CANCEL:
            case GTK_RESPONSE_DELETE_EVENT:
            gtk_widget_destroy(controls.dialogue);
            case GTK_RESPONSE_NONE:
            goto finalize;
            break;

            case GTK_RESPONSE_OK:
            break;

            case RESPONSE_CLEAR:
            gwy_selection_clear(controls.selection);
            break;

            default:
            g_assert_not_reached();
            break;
        }
    } while (response != GTK_RESPONSE_OK);

    newid = create_results(data, controls.acf, controls.acfmask, id);
    gwy_app_sync_data_items(controls.mydata, data, 0, newid, FALSE,
                            GWY_DATA_ITEM_MASK_COLOR,
                            0);
    create_output_graphs(&controls, data);
    gtk_widget_destroy(controls.dialogue);
finalize:
    g_object_unref(controls.mydata);
    g_object_unref(controls.acf);
    g_object_unref(controls.acfmask);
    g_object_unref(controls.results);
}

static void
masking_changed(GtkToggleButton *toggle, ACFControls *controls)
{
    if (!gtk_toggle_button_get_active(toggle))
        return;

    controls->args->masking = gwy_radio_button_get_value(GTK_WIDGET(toggle));
    recalculate_acf(controls);
}

static void
levelling_changed(GtkToggleButton *toggle, ACFControls *controls)
{
    if (!gtk_toggle_button_get_active(toggle))
        return;

    controls->args->level = gwy_radio_button_get_value(GTK_WIDGET(toggle));
    recalculate_acf(controls);
}

static void
threshold_changed(ACFControls *controls, GtkAdjustment *adjustment)
{
    controls->args->threshold = gtk_adjustment_get_value(adjustment);
    create_acf_mask(controls->acf, controls->acfmask, controls->args);
    gwy_data_field_data_changed(controls->acfmask);
    calculate_zoomed_fields(controls);
    update_parameters(controls);
}

static void
calculate_zoomed_fields(ACFControls *controls)
{
    ACFArgs *args = controls->args;
    ZoomType zoom = args->zoom;
    GwyDataField *zoomed, *zoomedmask;
    guint xres, yres, width, height;

    zoomed = controls->acf;
    zoomedmask = controls->acfmask;
    xres = gwy_data_field_get_xres(zoomed);
    yres = gwy_data_field_get_yres(zoomed);

    if (zoom == ZOOM_1) {
        g_object_ref(zoomed);
        g_object_ref(zoomedmask);
    }
    else {
        width = (xres/zoom) | 1;
        height = (yres/zoom) | 1;

        if (width < 17)
            width = MAX(width, MIN(17, xres));

        if (height < 17)
            height = MAX(height, MIN(17, yres));

        zoomed = gwy_data_field_area_extract(zoomed,
                                             (xres - width)/2,
                                             (yres - height)/2,
                                             width, height);
        gwy_data_field_set_xoffset(zoomed, -0.5*zoomed->xreal);
        gwy_data_field_set_yoffset(zoomed, -0.5*zoomed->yreal);

        zoomedmask = gwy_data_field_area_extract(zoomedmask,
                                                 (xres - width)/2,
                                                 (yres - height)/2,
                                                 width, height);
        gwy_data_field_set_xoffset(zoomedmask, -0.5*zoomedmask->xreal);
        gwy_data_field_set_yoffset(zoomedmask, -0.5*zoomedmask->yreal);
    }
    gwy_container_set_object(controls->mydata,
                             gwy_app_get_data_key_for_id(0), zoomed);
    gwy_container_set_object(controls->mydata,
                             gwy_app_get_mask_key_for_id(0), zoomedmask);
    g_object_unref(zoomed);
    g_object_unref(zoomedmask);
}

static void
recalculate_acf(ACFControls *controls)
{
    calculate_acf(controls->dfield, controls->mask, controls->args,
                  controls->acf, controls->acfmask);
    gwy_data_field_data_changed(controls->acf);
    gwy_data_field_data_changed(controls->acfmask);
    calculate_zoomed_fields(controls);
    selection_changed(controls, -1);
    update_parameters(controls);
}

static void
update_parameters(ACFControls *controls)
{
    gboolean masking;
    guint i;

    calculate_acf_statistics(controls->acfmask, controls->results);
    masking = controls->mask && (controls->args->masking != GWY_MASK_IGNORE);
    gwy_results_fill_values(controls->results, "masking", masking, NULL);

    for (i = 0; i < G_N_ELEMENTS(guivalues); i++) {
        gtk_label_set_markup(GTK_LABEL(controls->guivalues[i]),
                             gwy_results_get_full(controls->results,
                                                  guivalues[i]));
    }
}

static void
zoom_changed(GtkRadioButton *button, ACFControls *controls)
{
    ACFArgs *args = controls->args;
    ZoomType zoom = gwy_radio_buttons_get_current(controls->zoom);
    GwyDataField *acf;
    gdouble xoff, yoff;

    if (button && zoom == args->zoom)
        return;

    acf = gwy_container_get_object(controls->mydata,
                                   gwy_app_get_data_key_for_id(0));
    xoff = gwy_data_field_get_xoffset(acf);
    yoff = gwy_data_field_get_yoffset(acf);
    args->zoom = zoom;
    calculate_zoomed_fields(controls);
    gwy_set_data_preview_size(GWY_DATA_VIEW(controls->view), PREVIEW_SIZE);
    acf = gwy_container_get_object(controls->mydata,
                                   gwy_app_get_data_key_for_id(0));
    xoff -= gwy_data_field_get_xoffset(acf);
    yoff -= gwy_data_field_get_yoffset(acf);
    gwy_selection_move(controls->selection, xoff, yoff);
}

static void
resolution_changed(ACFControls *controls, GtkAdjustment *adj)
{
    controls->args->resolution = gwy_adjustment_get_int(adj);
    /* Resolution can be changed only when fixres == TRUE */
    selection_changed(controls, -1);
}

static void
thickness_changed(ACFControls *controls, GtkAdjustment *adj)
{
    controls->args->thickness = gwy_adjustment_get_int(adj);
    selection_changed(controls, -1);
}

static void
fixres_changed(ACFControls *controls, GtkToggleButton *check)
{
    controls->args->fixres = gtk_toggle_button_get_active(check);
    selection_changed(controls, -1);
}

static void
interpolation_changed(GtkComboBox *combo, ACFControls *controls)
{
    controls->args->interpolation = gwy_enum_combo_box_get_active(combo);
    selection_changed(controls, -1);
}

static void
separate_changed(ACFControls *controls)
{
    ACFArgs *args = controls->args;
    args->separate
        = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(controls->separate));
    gwy_table_hscale_set_sensitive(GTK_OBJECT(controls->target_graph),
                                   !args->separate);
    update_target_graphs(controls);
}

static void
selection_changed(ACFControls *controls, gint hint)
{
    gint i, n;

    n = gwy_selection_get_data(controls->selection, NULL);
    if (hint < 0) {
        gwy_graph_model_remove_all_curves(controls->gmodel);
        for (i = 0; i < n; i++)
            update_curve(controls, i);
    }
    else
        update_curve(controls, hint);
}

static void
update_target_graphs(ACFControls *controls)
{
    GwyDataChooser *chooser = GWY_DATA_CHOOSER(controls->target_graph);
    gwy_data_chooser_refilter(chooser);
}

static gboolean
filter_target_graphs(GwyContainer *data, gint id, gpointer user_data)
{
    ACFControls *controls = (ACFControls*)user_data;
    GwyGraphModel *gmodel = controls->gmodel, *targetgmodel;
    GQuark quark = gwy_app_get_graph_key_for_id(id);

    if (controls->args->separate)
        return FALSE;

    g_return_val_if_fail(gmodel, FALSE);
    return (gwy_container_gis_object(data, quark, (GObject**)&targetgmodel)
            && gwy_graph_model_units_are_compatible(gmodel, targetgmodel));
}

static void
target_graph_changed(ACFControls *controls)
{
    GwyDataChooser *chooser = GWY_DATA_CHOOSER(controls->target_graph);

    gwy_data_chooser_get_active_id(chooser, &controls->args->target_graph);
}

static void
report_style_changed(ACFControls *controls, GwyResultsExport *rexport)
{
    controls->args->report_style = gwy_results_export_get_format(rexport);
}

static void
update_curve(ACFControls *controls, gint i)
{
    GwyGraphCurveModel *gcmodel;
    GwyDataField *acf;
    gdouble xy[4], xofffull, xoffzoom, yofffull, yoffzoom;
    gint xl0, yl0, xl1, yl1;
    gint n, lineres;
    gchar *desc;

    if (!gwy_selection_get_object(controls->selection, i, xy)) {
        g_return_if_reached();
    }

    acf = gwy_container_get_object(controls->mydata,
                                   gwy_app_get_data_key_for_id(0));
    xoffzoom = gwy_data_field_get_xoffset(acf);
    yoffzoom = gwy_data_field_get_yoffset(acf);
    xy[0] += xoffzoom;
    xy[1] += yoffzoom;

    xofffull = gwy_data_field_get_xoffset(controls->acf);
    yofffull = gwy_data_field_get_yoffset(controls->acf);

    xl0 = gwy_data_field_get_xres(controls->acf)/2;
    yl0 = gwy_data_field_get_yres(controls->acf)/2;
    xl1 = floor(gwy_data_field_rtoj(controls->acf, xy[0] - xofffull));
    yl1 = floor(gwy_data_field_rtoi(controls->acf, xy[1] - yofffull));

    if (!controls->args->fixres) {
        lineres = GWY_ROUND(hypot(abs(xl0 - xl1) + 1, abs(yl0 - yl1) + 1));
        lineres = MAX(lineres, MIN_RESOLUTION);
    }
    else
        lineres = controls->args->resolution;

    gwy_data_field_get_profile(controls->acf, controls->line,
                               xl0, yl0, xl1, yl1,
                               lineres,
                               controls->args->thickness,
                               controls->args->interpolation);

    n = gwy_graph_model_get_n_curves(controls->gmodel);
    if (i < n) {
        gcmodel = gwy_graph_model_get_curve(controls->gmodel, i);
    }
    else {
        gcmodel = gwy_graph_curve_model_new();
        g_object_set(gcmodel,
                     "mode", GWY_GRAPH_CURVE_LINE,
                     "color", gwy_graph_get_preset_color(i),
                     NULL);
        gwy_graph_model_add_curve(controls->gmodel, gcmodel);
        g_object_unref(gcmodel);
    }

    gwy_graph_curve_model_set_data_from_dataline(gcmodel, controls->line, 0, 0);
    desc = g_strdup_printf(_("ACF %.0f°"), 180.0/G_PI*atan2(-xy[1], xy[0]));
    g_object_set(gcmodel, "description", desc, NULL);
    g_free(desc);
}

static void
init_graph_model_units(GwyGraphModel *gmodel,
                       GwyDataField *dfield)
{
    GwySIUnit *unit;

    unit = gwy_data_field_get_si_unit_xy(dfield);
    unit = gwy_si_unit_duplicate(unit);
    g_object_set(gmodel, "si-unit-x", unit, NULL);
    g_object_unref(unit);

    unit = gwy_data_field_get_si_unit_z(dfield);
    unit = gwy_si_unit_duplicate(unit);
    g_object_set(gmodel, "si-unit-y", unit, NULL);
    g_object_unref(unit);
}

static gint
create_results(GwyContainer *data,
               GwyDataField *acf, GwyDataField *acfmask,
               gint oldid)
{
    gint newid;

    newid = gwy_app_data_browser_add_data_field(acf, data, TRUE);
    gwy_container_set_object(data, gwy_app_get_mask_key_for_id(newid), acfmask);
    gwy_app_set_data_field_title(data, newid, _("2D ACF"));
    gwy_app_channel_log_add_proc(data, oldid, newid);

    return newid;
}

static void
calculate_acf(GwyDataField *dfield, GwyDataField *mask,
              const ACFArgs *args,
              GwyDataField *acf, GwyDataField *acfmask)
{
    GwyDataField *field_for_acf;
    gint xres = gwy_data_field_get_xres(dfield);
    gint yres = gwy_data_field_get_yres(dfield);
    gdouble a, bx, by;

    /* Reuse acfmask for the field because we do not need it for
     * gwy_data_field_area_2dacf_mask().  Reuse acf for the modified mask
     * because discard it immediately. */
    if (args->level == LEVELLING_MEAN_VALUE) {
        gwy_data_field_resample(acfmask, xres, yres, GWY_INTERPOLATION_NONE);
        gwy_data_field_copy(dfield, acfmask, TRUE);
        a = gwy_data_field_area_get_avg_mask(acfmask, mask, args->masking,
                                             0, 0, xres, yres);
        gwy_data_field_add(acfmask, -a);
        field_for_acf = acfmask;
    }
    else if (args->level == LEVELLING_MEAN_PLANE) {
        gwy_data_field_resample(acfmask, xres, yres, GWY_INTERPOLATION_NONE);
        gwy_data_field_copy(dfield, acfmask, TRUE);
        if (mask && args->masking == GWY_MASK_EXCLUDE) {
            gwy_data_field_resample(acf, xres, yres,
                                    GWY_INTERPOLATION_NONE);
            gwy_data_field_copy(mask, acf, FALSE);
            gwy_data_field_area_fit_plane(acfmask, acf, 0, 0, xres, yres,
                                          &a, &bx, &by);
        }
        else if (mask && args->masking == GWY_MASK_INCLUDE) {
            gwy_data_field_area_fit_plane(acfmask, mask, 0, 0, xres, yres,
                                          &a, &bx, &by);
        }
        else
            gwy_data_field_fit_plane(acfmask, &a, &bx, &by);

        gwy_data_field_plane_level(acfmask, a, bx, by);
        field_for_acf = acfmask;
    }
    else
        field_for_acf = dfield;

    gwy_data_field_area_2dacf_mask(field_for_acf, acf, mask, args->masking,
                                   0, 0, xres, yres, 0, 0, NULL);
    create_acf_mask(acf, acfmask, args);
}

static void
create_acf_mask(GwyDataField *acf, GwyDataField *acfmask,
                const ACFArgs *args)
{
    gint xres = gwy_data_field_get_xres(acf);
    gint yres = gwy_data_field_get_yres(acf);
    gdouble sigma2;

    sigma2 = gwy_data_field_get_val(acf, xres/2, yres/2);
    gwy_data_field_resample(acfmask, xres, yres, GWY_INTERPOLATION_NONE);
    gwy_data_field_copy(acf, acfmask, TRUE);
    gwy_data_field_threshold(acfmask, args->threshold*sigma2, 0.0, 1.0);
    gwy_data_field_grains_extract_grain(acfmask, xres/2, yres/2);
    gwy_si_unit_set_from_string(gwy_data_field_get_si_unit_z(acfmask), NULL);
}

static void
create_output_graphs(ACFControls *controls, GwyContainer *data)
{
    GwyGraphCurveModel *gcmodel;
    GwyGraphModel *gmodel;
    ACFArgs *args = controls->args;
    gchar *s;
    gint i, n;

    n = gwy_selection_get_data(controls->selection, NULL);
    if (!n)
        return;

    if (!args->separate) {
        gwy_app_add_graph_or_curves(controls->gmodel,
                                    data, &args->target_graph, 1);
        return;
    }

    for (i = 0; i < n; i++) {
        gmodel = gwy_graph_model_new_alike(controls->gmodel);
        gcmodel = gwy_graph_model_get_curve(controls->gmodel, i);
        gcmodel = gwy_graph_curve_model_duplicate(gcmodel);
        gwy_graph_model_add_curve(gmodel, gcmodel);
        g_object_unref(gcmodel);
        g_object_get(gcmodel, "description", &s, NULL);
        g_object_set(gmodel, "title", s, NULL);
        g_free(s);
        gwy_app_data_browser_add_graph_model(gmodel, data, TRUE);
        g_object_unref(gmodel);
    }
}

/* XXX: This is not very efficient and probably does not do anything useful
 * when the grain is non-convex. But what exactly should be done when it is
 * non-convex is anyone's guess... */
static gboolean
find_decays(GwyDataField *mask,
            gdouble *r1, gdouble *r2, gdouble *phi1, gdouble *phi2)
{
    const gdouble *data = gwy_data_field_get_data_const(mask);
    guint xres = gwy_data_field_get_xres(mask);
    guint yres = gwy_data_field_get_yres(mask);
    gdouble dx = gwy_data_field_get_xmeasure(mask);
    gdouble dy = gwy_data_field_get_ymeasure(mask);
    gdouble phimin = 0.0, r2min = G_MAXDOUBLE;
    gdouble phimax = 0.0, r2max = 0.0;
    gdouble xc, yc;
    guint i, j, k;

    xc = 0.5*dx*xres;
    yc = 0.5*dy*yres;
    for (i = 0; i <= yres; i++) {
        k = i*xres;
        for (j = 0; j <= xres; j++, k++) {
            /*
             * 1 2
             * 3 4
             */
            guint g0 = i && j && data[k - xres - 1] > 0.0;
            guint g1 = i && j < xres && data[k - xres] > 0.0;
            guint g2 = i < yres && j && data[k - 1] > 0.0;
            guint g3 = i < yres && j < xres && data[k] > 0.0;
            guint g = g0 | (g1 << 1) | (g2 << 2) | (g3 << 3);

            if (g != 0 && g != 15) {
                gdouble x = j*dx - xc, y = i*dy - yc;
                gdouble rr = x*x + y*y;

                if (rr < r2min) {
                    r2min = rr;
                    phimin = atan2(-y, x);
                }
                if (rr > r2max) {
                    r2max = rr;
                    phimax = atan2(-y, x);
                }
            }
        }
    }

    if (r2min == G_MAXDOUBLE) {
        *r1 = *r2 = *phi1 = *phi2 = 0.0;
        return FALSE;
    }

    *r1 = sqrt(r2min);
    *r2 = sqrt(r2max);

    *phi1 = gwy_canonicalize_angle(phimin, FALSE, FALSE);
    *phi2 = gwy_canonicalize_angle(phimax, FALSE, FALSE);

    return TRUE;
}

static void
calculate_acf_statistics(GwyDataField *acfmask, GwyResults *results)
{
    gdouble rmin, rmax, phimin, phimax;

    if (!find_decays(acfmask, &rmin, &rmax, &phimin, &phimax)) {
        gwy_results_set_nav(results, G_N_ELEMENTS(guivalues), guivalues);
        return;
    }

    gwy_results_fill_values(results,
                            "Sal", rmin, "tau2", rmax,
                            "phi1", phimin, "phi2", phimax,
                            "Str", rmax/rmin,
                            NULL);
}

static const gchar fixres_key[]        = "/module/acf2d/fixres";
static const gchar interpolation_key[] = "/module/acf2d/interpolation";
static const gchar level_key[]         = "/module/acf2d/level";
static const gchar masking_key[]       = "/module/acf2d/masking";
static const gchar report_style_key[]  = "/module/acf2d/report_style";
static const gchar resolution_key[]    = "/module/acf2d/resolution";
static const gchar separate_key[]      = "/module/acf2d/separate";
static const gchar thickness_key[]     = "/module/acf2d/thickness";
static const gchar threshold_key[]     = "/module/acf2d/threshold";
static const gchar zoom_key[]          = "/module/acf2d/zoom";

static void
acf2d_sanitize_args(ACFArgs *args)
{
    args->masking = gwy_enum_sanitize_value(args->masking,
                                            GWY_TYPE_MASKING_TYPE);
    args->level = CLAMP(args->level, 0, LEVELLING_NTYPES-1);
    args->threshold = CLAMP(args->threshold, 0.0, 1.0);
    args->separate = !!args->separate;
    args->fixres = !!args->fixres;
    args->resolution = CLAMP(args->resolution, MIN_RESOLUTION, MAX_RESOLUTION);
    args->thickness = CLAMP(args->thickness, 1, MAX_THICKNESS);
    args->interpolation = gwy_enum_sanitize_value(args->interpolation,
                                                  GWY_TYPE_INTERPOLATION_TYPE);
    if (args->zoom != ZOOM_1 && args->zoom != ZOOM_4 && args->zoom != ZOOM_16)
        args->zoom = acf2d_defaults.zoom;
    gwy_app_data_id_verify_graph(&args->target_graph);
}

static void
acf2d_load_args(GwyContainer *container, ACFArgs *args)
{
    *args = acf2d_defaults;

    gwy_container_gis_enum_by_name(container, report_style_key,
                                   &args->report_style);
    gwy_container_gis_enum_by_name(container, masking_key, &args->masking);
    gwy_container_gis_enum_by_name(container, level_key, &args->level);
    gwy_container_gis_double_by_name(container, threshold_key,
                                     &args->threshold);
    gwy_container_gis_boolean_by_name(container, separate_key, &args->separate);
    gwy_container_gis_boolean_by_name(container, fixres_key, &args->fixres);
    gwy_container_gis_int32_by_name(container, resolution_key,
                                    &args->resolution);
    gwy_container_gis_int32_by_name(container, thickness_key, &args->thickness);
    gwy_container_gis_enum_by_name(container, interpolation_key,
                                   &args->interpolation);
    gwy_container_gis_enum_by_name(container, zoom_key, &args->zoom);
    args->target_graph = target_id;
    acf2d_sanitize_args(args);
}

static void
acf2d_save_args(GwyContainer *container, ACFArgs *args)
{
    gwy_container_set_enum_by_name(container, report_style_key,
                                   args->report_style);
    gwy_container_set_enum_by_name(container, masking_key, args->masking);
    gwy_container_set_enum_by_name(container, level_key, args->level);
    gwy_container_set_double_by_name(container, threshold_key, args->threshold);
    gwy_container_set_boolean_by_name(container, separate_key, args->separate);
    gwy_container_set_boolean_by_name(container, fixres_key, args->fixres);
    gwy_container_set_int32_by_name(container, resolution_key,
                                    args->resolution);
    gwy_container_set_int32_by_name(container, thickness_key, args->thickness);
    gwy_container_set_enum_by_name(container, interpolation_key,
                                   args->interpolation);
    gwy_container_set_enum_by_name(container, zoom_key, args->zoom);
    target_id = args->target_graph;
}

/* vim: set cin et ts=4 sw=4 cino=>1s,e0,n0,f0,{0,}0,^0,\:1s,=0,g1s,h0,t0,+1s,c3,(0,u0 : */
