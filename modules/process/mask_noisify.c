/*
 *  $Id$
 *  Copyright (C) 2017 David Necas (Yeti).
 *  E-mail: yeti@gwyddion.net.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA 02110-1301, USA.
 */

#include "config.h"
#include <stdlib.h>
#include <libgwyddion/gwymacros.h>
#include <libgwyddion/gwyrandgenset.h>
#include <libprocess/grains.h>
#include <libgwydgets/gwyradiobuttons.h>
#include <libgwydgets/gwystock.h>
#include <libgwydgets/gwydgetutils.h>
#include <libgwymodule/gwymodule-process.h>
#include <app/gwyapp.h>

#define NOSIFYMASK_RUN_MODES (GWY_RUN_IMMEDIATE | GWY_RUN_INTERACTIVE)

typedef enum {
    NOISE_DIRECTION_BOTH = 0,
    NOISE_DIRECTION_UP   = 1,
    NOISE_DIRECTION_DOWN = 2,
    NOISE_DIRECTION_NTYPES
} NoiseDirectionType;

typedef struct {
    NoiseDirectionType direction;
    gdouble density;
    gboolean only_boundaries;
} MaskNoiseArgs;

typedef struct {
    MaskNoiseArgs *args;
    GtkWidget *dialog;
    GSList *direction;
    GtkObject *density;
    GtkWidget *only_boundaries;
} MaskNoiseControls;

static gboolean module_register        (void);
static void     mask_noisify           (GwyContainer *data,
                                        GwyRunType run);
static gboolean masknoise_dialog       (MaskNoiseArgs *args);
static void     direction_type_changed (GtkWidget *button,
                                        MaskNoiseControls *controls);
static void     density_changed        (GtkAdjustment *adj,
                                        MaskNoiseControls *controls);
static void     only_boundaries_changed(GtkToggleButton *toggle,
                                        MaskNoiseControls *controls);
static void     masknoise_do           (GwyDataField *mfield,
                                        MaskNoiseArgs *args);
static void     masknoise_sanitize_args(MaskNoiseArgs *args);
static void     masknoise_load_args    (GwyContainer *settings,
                                        MaskNoiseArgs *args);
static void     masknoise_save_args    (GwyContainer *settings,
                                        MaskNoiseArgs *args);

static const MaskNoiseArgs masknoise_defaults = {
    NOISE_DIRECTION_BOTH,
    0.1,
    FALSE,
};

static GwyModuleInfo module_info = {
    GWY_MODULE_ABI_VERSION,
    &module_register,
    N_("Adds salt and/or pepper noise to mask."),
    "Yeti <yeti@gwyddion.net>",
    "1.0",
    "David Nečas (Yeti)",
    "2017",
};

GWY_MODULE_QUERY2(module_info, mask_noisify)

static gboolean
module_register(void)
{
    gwy_process_func_register("mask_noisify",
                              (GwyProcessFunc)&mask_noisify,
                              N_("/_Mask/_Noisify..."),
                              NULL,
                              NOSIFYMASK_RUN_MODES,
                              GWY_MENU_FLAG_DATA_MASK | GWY_MENU_FLAG_DATA,
                              N_("Add noise to mask"));

    return TRUE;
}

static void
mask_noisify(GwyContainer *data, GwyRunType run)
{
    GwyDataField *mfield;
    MaskNoiseArgs args;
    GQuark quark;
    gint id;

    g_return_if_fail(run & NOSIFYMASK_RUN_MODES);
    gwy_app_data_browser_get_current(GWY_APP_MASK_FIELD, &mfield,
                                     GWY_APP_MASK_FIELD_KEY, &quark,
                                     GWY_APP_DATA_FIELD_ID, &id,
                                     0);
    g_return_if_fail(mfield);

    masknoise_load_args(gwy_app_settings_get(), &args);
    if (run == GWY_RUN_IMMEDIATE || masknoise_dialog(&args)) {
        gwy_app_undo_qcheckpointv(data, 1, &quark);
        masknoise_do(mfield, &args);
        gwy_data_field_data_changed(mfield);
        gwy_app_channel_log_add_proc(data, id, id);
    }
    masknoise_save_args(gwy_app_settings_get(), &args);
}

static gboolean
masknoise_dialog(MaskNoiseArgs *args)
{
    MaskNoiseControls controls;
    GtkWidget *dialog;
    GtkWidget *table, *label;
    gint response, row = 0;

    controls.args = args;

    dialog = gtk_dialog_new_with_buttons(_("Noisify Mask"),
                                         NULL, 0,
                                         GTK_STOCK_CANCEL,
                                         GTK_RESPONSE_CANCEL,
                                         GTK_STOCK_OK,
                                         GTK_RESPONSE_OK,
                                         NULL);
    gtk_dialog_set_default_response(GTK_DIALOG(dialog), GTK_RESPONSE_OK);
    gwy_help_add_to_proc_dialog(GTK_DIALOG(dialog), GWY_HELP_DEFAULT);
    controls.dialog = dialog;

    table = gtk_table_new(NOISE_DIRECTION_NTYPES + 3, 3, FALSE);
    gtk_table_set_row_spacings(GTK_TABLE(table), 2);
    gtk_table_set_col_spacings(GTK_TABLE(table), 6);
    gtk_container_set_border_width(GTK_CONTAINER(table), 4);
    gtk_box_pack_start(GTK_BOX(GTK_DIALOG(dialog)->vbox), table, TRUE, TRUE, 0);

    label = gtk_label_new(_("Noise type:"));
    gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);
    gtk_table_attach(GTK_TABLE(table), label,
                     0, 2, row, row+1, GTK_FILL, 0, 0, 0);
    row++;

    controls.direction
        = gwy_radio_buttons_createl(G_CALLBACK(direction_type_changed),
                                    &controls, args->direction,
                                    _("S_ymmetrical"), NOISE_DIRECTION_BOTH,
                                    _("One-sided _positive"), NOISE_DIRECTION_UP,
                                    _("One-sided _negative"), NOISE_DIRECTION_DOWN,
                                    NULL);
    row = gwy_radio_buttons_attach_to_table(controls.direction,
                                            GTK_TABLE(table), 2, row);

    gtk_table_set_row_spacing(GTK_TABLE(table), row-1, 8);
    controls.density = gtk_adjustment_new(args->density, 0.0, 1.0, 0.0001, 0.1,
                                          0);
    gwy_table_attach_adjbar(table, row, _("Densi_ty:"), NULL,
                            controls.density, GWY_HSCALE_SQRT);
    g_signal_connect(controls.density, "value-changed",
                     G_CALLBACK(density_changed), &controls);
    row++;

    controls.only_boundaries
        = gtk_check_button_new_with_mnemonic(_("_Alter only boundaries"));
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(controls.only_boundaries),
                                 args->only_boundaries);
    gtk_table_attach(GTK_TABLE(table), controls.only_boundaries,
                     0, 2, row, row+1, GTK_FILL, 0, 0, 0);
    g_signal_connect(controls.only_boundaries, "toggled",
                     G_CALLBACK(only_boundaries_changed), &controls);

    gtk_widget_show_all(dialog);

    do {
        response = gtk_dialog_run(GTK_DIALOG(dialog));
        switch (response) {
            case GTK_RESPONSE_CANCEL:
            case GTK_RESPONSE_DELETE_EVENT:
            gtk_widget_destroy(dialog);
            case GTK_RESPONSE_NONE:
            return FALSE;
            break;

            case GTK_RESPONSE_OK:
            break;

            default:
            g_assert_not_reached();
            break;
        }
    } while (response != GTK_RESPONSE_OK);

    gtk_widget_destroy(dialog);

    return TRUE;
}

static void
direction_type_changed(GtkWidget *button, MaskNoiseControls *controls)
{
    controls->args->direction = gwy_radio_button_get_value(button);
}

static void
density_changed(GtkAdjustment *adj, MaskNoiseControls *controls)
{
    controls->args->density = gtk_adjustment_get_value(adj);
}

static void
only_boundaries_changed(GtkToggleButton *toggle, MaskNoiseControls *controls)
{
    controls->args->only_boundaries = gtk_toggle_button_get_active(toggle);
}

static void
masknoise_do(GwyDataField *mask, MaskNoiseArgs *args)
{
    GwyRandGenSet *rngset = gwy_rand_gen_set_new(1);
    NoiseDirectionType direction = args->direction;
    gboolean only_boundaries = args->only_boundaries, on_boundary;
    guint xres, yres, iter, i, j, n, nind, k, have_bits = 0;
    gint is_m, change_to;
    guint *indices;
    gdouble *m;
    guint32 r;

    xres = gwy_data_field_get_xres(mask);
    yres = gwy_data_field_get_yres(mask);
    n = xres*yres;
    nind = GWY_ROUND(n*args->density);
    indices = gwy_rand_gen_set_choose_shuffle(rngset, 0, n, nind);
    m = gwy_data_field_get_data(mask);

    for (iter = 0; iter < nind; iter++) {
        k = indices[iter];

        /* No-ops. */
        is_m = (m[k] > 0.0);
        if (direction == NOISE_DIRECTION_UP)
            change_to = 1;
        else if (direction == NOISE_DIRECTION_DOWN)
            change_to = 0;
        else {
            if (!have_bits) {
                r = gwy_rand_gen_set_int(rngset, 0);
                have_bits = 32;
            }
            change_to = (r & 1);
            have_bits--;
        }
        if (!change_to == !is_m)
            continue;

        /* Are we on a boundary?  This cannot be pre-determined because we
         * allow progressive boundary alteration. */
        if (only_boundaries) {
            on_boundary = FALSE;
            i = k/xres;
            j = k % xres;
            if (!on_boundary && i > 0 && !is_m == !(m[k-xres] <= 0.0))
                on_boundary = TRUE;
            if (!on_boundary && j > 0 && !is_m == !(m[k-1] <= 0.0))
                on_boundary = TRUE;
            if (!on_boundary && j < xres-1 && !is_m == !(m[k+1] <= 0.0))
                on_boundary = TRUE;
            if (!on_boundary && i < yres-1 && !is_m == !(m[k+xres] <= 0.0))
                on_boundary = TRUE;
            if (!on_boundary)
                continue;
        }

        m[k] = change_to;
    }

    g_free(indices);
}

static const gchar density_key[]         = "/module/mask_noisify/density";
static const gchar direction_key[]       = "/module/mask_noisify/direction";
static const gchar only_boundaries_key[] = "/module/mask_noisify/only_boundaries";

static void
masknoise_sanitize_args(MaskNoiseArgs *args)
{
    args->direction = MIN(args->direction, NOISE_DIRECTION_NTYPES-1);
    args->density = CLAMP(args->density, 0.0, 1.0);
    args->only_boundaries = !!args->only_boundaries;
}

static void
masknoise_load_args(GwyContainer *settings,
                    MaskNoiseArgs *args)
{
    *args = masknoise_defaults;
    gwy_container_gis_enum_by_name(settings, direction_key, &args->direction);
    gwy_container_gis_double_by_name(settings, density_key, &args->density);
    gwy_container_gis_boolean_by_name(settings, only_boundaries_key,
                                      &args->only_boundaries);
    masknoise_sanitize_args(args);
}

static void
masknoise_save_args(GwyContainer *settings,
                    MaskNoiseArgs *args)
{
    gwy_container_set_enum_by_name(settings, direction_key, args->direction);
    gwy_container_set_double_by_name(settings, density_key, args->density);
    gwy_container_set_boolean_by_name(settings, only_boundaries_key,
                                      args->only_boundaries);
}

/* vim: set cin et ts=4 sw=4 cino=>1s,e0,n0,f0,{0,}0,^0,\:1s,=0,g1s,h0,t0,+1s,c3,(0,u0 : */
