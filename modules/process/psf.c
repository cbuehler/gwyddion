/*
 *  $Id$
 *  Copyright (C) 2017 David Necas (Yeti), Petr Klapetek.
 *  E-mail: yeti@gwyddion.net, klapetek@gwyddion.net.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA 02110-1301, USA.
 */

#include "config.h"
#include <gtk/gtk.h>
#include <libgwyddion/gwymacros.h>
#include <libgwyddion/gwymath.h>
#include <libprocess/gwyprocesstypes.h>
#include <libprocess/arithmetic.h>
#include <libprocess/inttrans.h>
#include <libprocess/stats.h>
#include <libgwydgets/gwydgetutils.h>
#include <libgwydgets/gwycombobox.h>
#include <libgwydgets/gwystock.h>
#include <libgwymodule/gwymodule-process.h>
#include <app/gwymoduleutils.h>
#include <app/gwyapp.h>
#include "mfmops.h"
#include "preview.h"

#define PSF_RUN_MODES GWY_RUN_INTERACTIVE

#define field_convolve_default(field, kernel) \
    gwy_data_field_area_ext_convolve((field), \
                                     0, 0, \
                                     gwy_data_field_get_xres(field), \
                                     gwy_data_field_get_yres(field), \
                                     (field), (kernel), \
                                     GWY_EXTERIOR_MIRROR_EXTEND, 0.0, TRUE)

typedef enum {
    GWY_PSF_DISPLAY_DATA   = 0,
    GWY_PSF_DISPLAY_RESULT = 1,
    GWY_PSF_DISPLAY_CONVOLVED = 2,
    GWY_PSF_DISPLAY_DIFF   = 3
} GwyPsfDisplayType;

typedef struct {
    gdouble sigma;
    GwyAppDataId op1;
    GwyAppDataId op2;
    gboolean update;
    GwyPsfDisplayType display;
} PsfArgs;

typedef struct {
    PsfArgs *args;
    GtkObject *sigma;
    GtkWidget *chooser_op2;
    GtkWidget *update;
    GtkWidget *display;
    GwyDataField *result;
    GwyDataField *dfield;
    GtkWidget *view;
    GwyContainer *mydata;
    GtkWidget *dialog;
} PsfControls;

static gboolean module_register      (void);
static void     psf                  (GwyContainer *data,
                                      GwyRunType run);
static gboolean psf_dialog           (PsfArgs *args,
                                      GwyContainer *data);
static void     psf_load_args        (GwyContainer *container,
                                      PsfArgs *args);
static void     psf_save_args        (GwyContainer *container,
                                      PsfArgs *args);
static void     psf_sanitize_args    (PsfArgs *args);
static void     sigma_changed        (GtkAdjustment *adj,
                                      PsfControls *controls);
static void     psf_data_changed     (GwyDataChooser *chooser,
                                      GwyAppDataId *object);
static gboolean psf_data_filter      (GwyContainer *data,
                                      gint id,
                                      gpointer user_data);
static void     update_changed       (GtkToggleButton *button,
                                      PsfControls *controls);
static void     preview              (PsfControls *controls,
                                      PsfArgs *args);
static void     display_changed      (GtkComboBox *combo,
                                      PsfControls *controls);
static void     calculate_psf        (GwyDataField *measured,
                                      GwyDataField *ideal,
                                      GwyDataField *psf,
                                      gdouble sigma);
static gboolean fit                  (PsfArgs *args,
                                      GwySetFractionFunc set_fraction);

static GwyAppDataId op2_id = GWY_APP_DATA_ID_NONE;

static const PsfArgs psf_defaults = {
    10, GWY_APP_DATA_ID_NONE, GWY_APP_DATA_ID_NONE, TRUE, GWY_PSF_DISPLAY_RESULT,
};


static GwyModuleInfo module_info = {
    GWY_MODULE_ABI_VERSION,
    &module_register,
    N_("Point spread function estimation"),
    "Petr Klapetek <klapetek@gwyddion.net>",
    "1.1",
    "David Nečas (Yeti) & Petr Klapetek",
    "2017",
};

GWY_MODULE_QUERY2(module_info, psf)

static gboolean
module_register(void)
{
    gwy_process_func_register("psf",
                              (GwyProcessFunc)&psf,
                              N_("/_Statistics/_PSF Guess..."),
                              NULL,
                              PSF_RUN_MODES,
                              GWY_MENU_FLAG_DATA,
                              N_("Estimate PSF from known data and image"));

    return TRUE;
}

static void
psf(GwyContainer *data, GwyRunType run)
{
    GwyDataField *dfield1, *dfield2, *psf, *psfb, *convolved, *subtracted;
    PsfArgs args;
    gboolean ok;
    GQuark quark;
    GwyContainer *mydata;
    gint newid;

    g_return_if_fail(run & PSF_RUN_MODES);

    psf_load_args(gwy_app_settings_get(), &args);

    gwy_app_data_browser_get_current(GWY_APP_DATA_FIELD_ID, &args.op1.id,
                                     GWY_APP_CONTAINER_ID, &args.op1.datano,
                                     0);

    ok = psf_dialog(&args, data);
    psf_save_args(gwy_app_settings_get(), &args);
    if (!ok)
        return;

    mydata = gwy_app_data_browser_get(args.op1.datano);
    quark = gwy_app_get_data_key_for_id(args.op1.id);
    dfield1 = GWY_DATA_FIELD(gwy_container_get_object(mydata, quark));

    mydata = gwy_app_data_browser_get(args.op2.datano);
    quark = gwy_app_get_data_key_for_id(args.op2.id);
    dfield2 = GWY_DATA_FIELD(gwy_container_get_object(mydata, quark));

    psf = gwy_data_field_new_alike(dfield1, TRUE);
    calculate_psf(dfield1, dfield2, psf, args.sigma/100);

    psfb = gwy_data_field_duplicate(psf);
    newid = gwy_app_data_browser_add_data_field(psfb, data, TRUE);
    gwy_app_sync_data_items(data, data, args.op1.id, newid, FALSE,
                        GWY_DATA_ITEM_GRADIENT,
                        GWY_DATA_ITEM_MASK_COLOR,
                        0);

    gwy_app_set_data_field_title(data, newid, _("PSF"));
    gwy_app_channel_log_add_proc(data, args.op1.id, newid);
    gwy_object_unref(psfb);

    convolved = gwy_data_field_new_alike(dfield1, TRUE);
    gwy_data_field_copy(dfield2, convolved, TRUE);
    field_convolve_default(convolved, psf);

    newid = gwy_app_data_browser_add_data_field(convolved, data, TRUE);
    gwy_app_sync_data_items(data, data, args.op1.id, newid, FALSE,
                        GWY_DATA_ITEM_GRADIENT,
                        GWY_DATA_ITEM_MASK_COLOR,
                        0);

    gwy_app_set_data_field_title(data, newid, _("PSF*P"));
    gwy_app_channel_log_add_proc(data, args.op1.id, newid);


    subtracted = gwy_data_field_duplicate(convolved);
    gwy_data_field_subtract_fields(subtracted, convolved, dfield1);

    newid = gwy_app_data_browser_add_data_field(subtracted, data, TRUE);
    gwy_app_sync_data_items(data, data, args.op1.id, newid, FALSE,
                        GWY_DATA_ITEM_GRADIENT,
                        GWY_DATA_ITEM_MASK_COLOR,
                        0);

    gwy_app_set_data_field_title(data, newid, _("PSF*P - I"));
    gwy_app_channel_log_add_proc(data, args.op1.id, newid);


    g_object_unref(psf);
    g_object_unref(convolved);
    g_object_unref(subtracted);
}

static gboolean
psf_dialog(PsfArgs *args, GwyContainer *data)
{
    GtkWidget *dialog, *table, *hbox, *spin;
    GwyDataChooser *chooser;
    PsfControls controls;
    gint response, row, G_GNUC_UNUSED datano, id;
    GQuark quark;
    GwyContainer *mydata;
    gboolean ok;

    static const GwyEnum psf_outputs[] = {
        { N_("Data"),              GWY_PSF_DISPLAY_DATA,      },
        { N_("PSF"),               GWY_PSF_DISPLAY_RESULT,    },
        { N_("Convolved"),         GWY_PSF_DISPLAY_CONVOLVED,    },
        { N_("Difference"),        GWY_PSF_DISPLAY_DIFF,      },
    };

    controls.args = args;

    dialog = gtk_dialog_new_with_buttons(_("Estimate PSF"), NULL, 0,
                                         _("_Fit"), RESPONSE_ESTIMATE,
                                         _("_Plot Inits"), RESPONSE_PREVIEW,
                                         GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
                                         GTK_STOCK_OK, GTK_RESPONSE_OK,
                                         NULL);
    gtk_dialog_set_default_response(GTK_DIALOG(dialog), GTK_RESPONSE_OK);
    gtk_dialog_set_response_sensitive(GTK_DIALOG(dialog), RESPONSE_PREVIEW,
                                      !args->update);
    controls.dialog = dialog;

    hbox = gtk_hbox_new(FALSE, 2);
    gtk_box_pack_start(GTK_BOX(GTK_DIALOG(dialog)->vbox), GTK_WIDGET(hbox),
                       FALSE, FALSE, 4);

    mydata = gwy_app_data_browser_get(args->op1.datano);
    quark = gwy_app_get_data_key_for_id(args->op1.id);
    datano = args->op1.datano;
    id = args->op1.id;
    controls.dfield = GWY_DATA_FIELD(gwy_container_get_object(mydata, quark));
    controls.result = gwy_data_field_new_alike(controls.dfield, TRUE);

    controls.mydata = gwy_container_new();
    gwy_container_set_object_by_name(controls.mydata, "/0/data",
                                     controls.result);
    gwy_app_sync_data_items(data, controls.mydata, id, 0, FALSE,
                            GWY_DATA_ITEM_PALETTE,
                            GWY_DATA_ITEM_MASK_COLOR,
                            GWY_DATA_ITEM_RANGE,
                            GWY_DATA_ITEM_REAL_SQUARE,
                            0);
    controls.view = create_preview(controls.mydata, 0, PREVIEW_SIZE, TRUE);
    gtk_box_pack_start(GTK_BOX(hbox), controls.view, FALSE, FALSE, 4);

    table = gtk_table_new(7, 3, FALSE);
    gtk_table_set_row_spacings(GTK_TABLE(table), 2);
    gtk_table_set_col_spacings(GTK_TABLE(table), 6);
    gtk_container_set_border_width(GTK_CONTAINER(table), 4);
    gtk_box_pack_start(GTK_BOX(hbox), table, TRUE, TRUE, 4);
    row = 0;

    controls.chooser_op2 = gwy_data_chooser_new_channels();
    chooser = GWY_DATA_CHOOSER(controls.chooser_op2);
    g_object_set_data(G_OBJECT(chooser), "dialog", dialog);
    gwy_data_chooser_set_active_id(chooser, &args->op2);
    gwy_data_chooser_set_filter(chooser, psf_data_filter, &args->op1, NULL);
    g_signal_connect(chooser, "changed",
                     G_CALLBACK(psf_data_changed), &args->op2);
    psf_data_changed(chooser, &args->op2);
    gwy_table_attach_adjbar(table, row, _("_Ideal response:"), NULL,
                            GTK_OBJECT(chooser), GWY_HSCALE_WIDGET_NO_EXPAND);
    gtk_table_set_row_spacing(GTK_TABLE(table), row, 8);
    row++;

    controls.display
        = gwy_enum_combo_box_new(psf_outputs, G_N_ELEMENTS(psf_outputs),
                                 G_CALLBACK(display_changed),
                                 &controls, args->display, TRUE);
    gwy_table_attach_adjbar(table, row, _("_Display:"), NULL,
                            GTK_OBJECT(controls.display),
                            GWY_HSCALE_WIDGET_NO_EXPAND);
    row++;

    controls.sigma = gtk_adjustment_new(args->sigma, 1e-5, 1e2, 1e-5, 1, 0);
    spin = gwy_table_attach_adjbar(table, row, _("_Sigma init:"), "%",
                                   controls.sigma, GWY_HSCALE_LOG);
    gtk_spin_button_set_digits(GTK_SPIN_BUTTON(spin), 7);
    g_object_set_data(G_OBJECT(controls.sigma), "controls", &controls);
    g_signal_connect(controls.sigma, "value-changed",
                     G_CALLBACK(sigma_changed), &controls);
    row++;

    controls.update = gtk_check_button_new_with_mnemonic(_("I_nstant updates"));
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(controls.update),
                                 args->update);
    gtk_table_attach(GTK_TABLE(table), controls.update,
                     0, 2, row, row+1, GTK_EXPAND | GTK_FILL, 0, 0, 0);
    g_signal_connect(controls.update, "toggled",
                     G_CALLBACK(update_changed), &controls);
    row++;

    if (controls.args->update)
        preview(&controls, controls.args);

    gtk_widget_show_all(dialog);
    do {
        response = gtk_dialog_run(GTK_DIALOG(dialog));
        switch (response) {
            case GTK_RESPONSE_CANCEL:
            case GTK_RESPONSE_DELETE_EVENT:
            gtk_widget_destroy(dialog);
            case GTK_RESPONSE_NONE:
            return FALSE;
            break;

            case GTK_RESPONSE_OK:
            break;

            case RESPONSE_PREVIEW:
            preview(&controls, args);
            break;

            case RESPONSE_ESTIMATE:
            gwy_app_wait_start(GTK_WINDOW(dialog), _("Searching..."));
            gwy_app_wait_set_fraction(0.0);
            ok = fit(args, gwy_app_wait_set_fraction);
            gwy_app_wait_finish();
            if (ok) {
                gtk_adjustment_set_value(GTK_ADJUSTMENT(controls.sigma),
                                         args->sigma);
                if (!controls.args->update)
                    preview(&controls, args);
            }
            break;

            default:
            g_assert_not_reached();
            break;
        }
    } while (response != GTK_RESPONSE_OK);

    gtk_widget_destroy(dialog);

    return TRUE;
}

static void
display_changed(GtkComboBox *combo, PsfControls *controls)
{
    controls->args->display = gwy_enum_combo_box_get_active(combo);
    if (controls->args->update)
        preview(controls, controls->args);
}

static void
sigma_changed(GtkAdjustment *adj, PsfControls *controls)
{
    controls->args->sigma = gtk_adjustment_get_value(adj);
    if (controls->args->update)
        preview(controls, controls->args);
}

static void
update_changed(GtkToggleButton *button, PsfControls *controls)
{
    controls->args->update = gtk_toggle_button_get_active(button);
    gtk_dialog_set_response_sensitive(GTK_DIALOG(controls->dialog),
                                      RESPONSE_PREVIEW,
                                      !controls->args->update);
    if (controls->args->update)
        preview(controls, controls->args);
}

static gdouble
mean_square(GwyDataField *dfield)
{
    const gdouble *d = gwy_data_field_get_data_const(dfield);
    guint i, n = dfield->xres * dfield->yres;
    gdouble s = 0.0;

    for (i = 0; i < n; i++)
        s += d[i]*d[i];

    return s/n;
}

static gdouble
normalised_square_gradient_integral(GwyDataField *dfield)
{
    const gdouble *d = gwy_data_field_get_data_const(dfield);
    guint i, j, k, xres = dfield->xres, yres = dfield->yres;
    gdouble s = 0.0, m, min, max, dx, dy, hx, hy;

    gwy_data_field_get_min_max(dfield, &min, &max);
    hx = gwy_data_field_get_xmeasure(dfield);
    hy = gwy_data_field_get_ymeasure(dfield);
    if (fabs(log(hx/hy) - 1.0) < 1e-5) {
        /* We can factor out the step here. */
        for (i = 0; i < yres-1; i++) {
            k = i*xres;
            for (j = 0; j < xres-1; j++, k++) {
                dx = d[k+1] - d[k];
                dy = d[k+xres] - d[k];
                s += dx*dx + dy*dy;
            }
        }
        /* Convert the plain sum to an integral. */
        s /= hx*hy;
    }
    else {
        for (i = 0; i < yres-1; i++) {
            k = i*xres;
            for (j = 0; j < xres-1; j++, k++) {
                dx = (d[k+1] - d[k])/hx;
                dy = (d[k+xres] - d[k])/hy;
                s += dx*dx + dy*dy;
            }
        }
        /* Convert the plain sum to an integral. */
        s *= hx*hy;
    }

    /* Normalise. */
    m = fmax(fabs(min), fabs(max));
    return s/(G_PI*m*m);
}

static gboolean
fit(PsfArgs *args, GwySetFractionFunc set_fraction)
{
    GwyDataField *dfield1, *dfield2, *psf, *convolved;
    GQuark quark;
    GwyContainer *mydata;
    gdouble sigma, msq, m;
    gint isigma;
    guint i, im, n;
    GArray *results;

    mydata = gwy_app_data_browser_get(args->op1.datano);
    quark = gwy_app_get_data_key_for_id(args->op1.id);
    dfield1 = GWY_DATA_FIELD(gwy_container_get_object(mydata, quark));

    mydata = gwy_app_data_browser_get(args->op2.datano);
    quark = gwy_app_get_data_key_for_id(args->op2.id);
    dfield2 = GWY_DATA_FIELD(gwy_container_get_object(mydata, quark));

    psf = gwy_data_field_new_alike(dfield1, TRUE);
    convolved = gwy_data_field_new_alike(dfield1, TRUE);

    results = g_array_new(FALSE, FALSE, sizeof(gdouble));
    for (isigma = -14; isigma <= 0; isigma += 1) {
        sigma = pow10(0.5*isigma);
        g_array_append_val(results, sigma);

        calculate_psf(dfield1, dfield2, psf, sigma);
        msq = normalised_square_gradient_integral(psf);
        g_array_append_val(results, msq);

        gwy_data_field_copy(dfield2, convolved, TRUE);
        field_convolve_default(convolved, psf);
        gwy_data_field_subtract_fields(convolved, convolved, dfield1);
        /* Differences must be calculated from zero, not mean plane. */
        msq = mean_square(convolved);
        g_array_append_val(results, msq);

        if (set_fraction && !set_fraction((isigma + 14.0)/15.0)) {
            g_object_unref(convolved);
            g_object_unref(psf);
            g_array_free(results, TRUE);
            return FALSE;
        }
    }
    g_object_unref(convolved);
    g_object_unref(psf);

    n = results->len/3;

    /* Normalise both square sums using the edge (maximum) values. */
    m = g_array_index(results, gdouble, 1);
    for (i = 1; i < n; i++) {
        msq = g_array_index(results, gdouble, 3*i + 1);
        if (msq > m)
            m = msq;
    }
    for (i = 0; i < n; i++)
        g_array_index(results, gdouble, 3*i + 1) /= m;

    m = g_array_index(results, gdouble, 2);
    for (i = 1; i < n; i++) {
        msq = g_array_index(results, gdouble, 3*i + 2);
        if (msq > m)
            m = msq;
    }
    for (i = 0; i < n; i++)
        g_array_index(results, gdouble, 3*i + 2) /= m;

    /* Find minimum. */
    m = G_MAXDOUBLE;
    im = 0;
    for (i = 0; i < n; i++) {
        msq = (g_array_index(results, gdouble, 3*i + 1)
               + g_array_index(results, gdouble, 3*i + 2));
        if (msq < m) {
            im = i;
            m = msq;
        }
    }

    /* Attempt to refine minimum position. */
    if (im > 0 && im < n-1) {
        gdouble c[3];
        gdouble x, sigma1, sigma2;

        for (i = 0; i < 3; i++) {
            c[i] = -(g_array_index(results, gdouble, 3*(im-1 + i) + 1)
                     + g_array_index(results, gdouble, 3*(im-1 + i) + 2));
        }
        gwy_math_refine_maximum_1d(c, &x);
        if (x < 0.0) {
            x += 1.0;
            im -= 1;
        }
        sigma1 = g_array_index(results, gdouble, 3*im);
        sigma2 = g_array_index(results, gdouble, 3*(im + 1));
        sigma = exp((1.0 - x)*log(sigma1) + x*log(sigma2));
    }
    else
        sigma = g_array_index(results, gdouble, 3*im);

    args->sigma = 100.0*sigma;
    g_array_free(results, TRUE);

    return TRUE;
}

static void
preview(PsfControls *controls, PsfArgs *args)
{
    GwyDataField *dfield1, *dfield2, *psf, *convolved;
    GQuark quark;
    GwyContainer *mydata;
    gdouble realsigma;

    mydata = gwy_app_data_browser_get(args->op1.datano);
    quark = gwy_app_get_data_key_for_id(args->op1.id);
    dfield1 = GWY_DATA_FIELD(gwy_container_get_object(mydata, quark));

    if (args->op2.datano < 0 || args->op2.id < 0) {
        if (args->display == GWY_PSF_DISPLAY_DATA)
            gwy_data_field_copy(dfield1, controls->result, TRUE);
        else
            gwy_data_field_clear(controls->result);
        gwy_data_field_data_changed(controls->result);
        return;
    }

    mydata = gwy_app_data_browser_get(args->op2.datano);
    quark = gwy_app_get_data_key_for_id(args->op2.id);
    dfield2 = GWY_DATA_FIELD(gwy_container_get_object(mydata, quark));

    psf = gwy_data_field_new_alike(dfield1, TRUE);

    realsigma = args->sigma/100.0;
    calculate_psf(dfield1, dfield2, psf, realsigma);

    if (args->display == GWY_PSF_DISPLAY_DATA) {
        gwy_data_field_copy(dfield1, controls->result, TRUE);
        gwy_data_field_data_changed(controls->result);
    }
    else if (args->display == GWY_PSF_DISPLAY_RESULT) {
        gwy_data_field_copy(psf, controls->result, TRUE);
        gwy_data_field_data_changed(controls->result);
    }
    else if (args->display == GWY_PSF_DISPLAY_CONVOLVED) {
        convolved = gwy_data_field_new_alike(dfield1, TRUE);
        gwy_data_field_copy(dfield2, convolved, TRUE);
        field_convolve_default(convolved, psf);
        gwy_data_field_copy(convolved, controls->result, TRUE);
        gwy_data_field_data_changed(controls->result);
        gwy_object_unref(convolved);

    }
    else {
        convolved = gwy_data_field_new_alike(dfield1, TRUE);
        gwy_data_field_copy(dfield2, convolved, TRUE);
        field_convolve_default(convolved, psf);
        gwy_data_field_subtract_fields(convolved, convolved, dfield1);
        gwy_data_field_copy(convolved, controls->result, TRUE);
        gwy_data_field_data_changed(controls->result);
        gwy_object_unref(convolved);
    }
}

static void
psf_data_changed(GwyDataChooser *chooser, GwyAppDataId *object)
{
    GtkWidget *dialog;

    gwy_data_chooser_get_active_id(chooser, object);
    gwy_debug("data: %d %d", object->datano, object->id);

    dialog = g_object_get_data(G_OBJECT(chooser), "dialog");
    g_assert(GTK_IS_DIALOG(dialog));
    gtk_dialog_set_response_sensitive(GTK_DIALOG(dialog), GTK_RESPONSE_OK,
                                      object->datano);
}

static gboolean
psf_data_filter(GwyContainer *data, gint id, gpointer user_data)
{

    GwyAppDataId *object = (GwyAppDataId*)user_data;
    GwyDataField *op1, *op2;
    GQuark quark;

    quark = gwy_app_get_data_key_for_id(id);
    op1 = GWY_DATA_FIELD(gwy_container_get_object(data, quark));

    data = gwy_app_data_browser_get(object->datano);
    quark = gwy_app_get_data_key_for_id(object->id);
    op2 = GWY_DATA_FIELD(gwy_container_get_object(data, quark));

    /* It does not make sense to crosscorrelate with itself */
    if (op1 == op2)
        return FALSE;

    return !gwy_data_field_check_compatibility(op1, op2,
                                               GWY_DATA_COMPATIBILITY_RES
                                               | GWY_DATA_COMPATIBILITY_REAL
                                               | GWY_DATA_COMPATIBILITY_LATERAL);
}

/*
 * XXX: I*P should be an integral.  The plain convolution sum needs to be
 * multiplied by area/npixels to approximate the integral.  This also means
 * we have to *divide* the estimated PSF by the same factor.
 */
static void
calculate_psf(GwyDataField *measured, GwyDataField *ideal,
              GwyDataField *psf, gdouble sigma)
{
    GwyDataField *frideal, *fiideal, *frmeasured, *fimeasured,
                 *frpsf, *fipsf, *ipsf;
    gint i;
    gint xres = gwy_data_field_get_xres(measured);
    gint yres = gwy_data_field_get_yres(measured);
    gdouble *frmdata, *fimdata, *fridata, *fiidata, *frpsfdata, *fipsfdata;
    gdouble rv, iv, inorm, realsigma, rs2, min, max, q;

    frideal = gwy_data_field_new_alike(measured, TRUE);
    fiideal = gwy_data_field_new_alike(measured, TRUE);
    frmeasured = gwy_data_field_new_alike(measured, TRUE);
    fimeasured = gwy_data_field_new_alike(measured, TRUE);
    frpsf = gwy_data_field_new_alike(measured, TRUE);
    fipsf = gwy_data_field_new_alike(measured, TRUE);
    ipsf = gwy_data_field_new_alike(measured, TRUE);

    gwy_data_field_2dfft_raw(measured, NULL, frmeasured, fimeasured,
                             GWY_TRANSFORM_DIRECTION_FORWARD);
    gwy_data_field_2dfft_raw(ideal, NULL, frideal, fiideal,
                             GWY_TRANSFORM_DIRECTION_FORWARD);

    frmdata = gwy_data_field_get_data(frmeasured);
    fimdata = gwy_data_field_get_data(fimeasured);
    fridata = gwy_data_field_get_data(frideal);
    fiidata = gwy_data_field_get_data(fiideal);

    frpsfdata = gwy_data_field_get_data(frpsf);
    fipsfdata = gwy_data_field_get_data(fipsf);

    gwy_data_field_get_min_max(frideal, &min, &max);
    realsigma = sigma*(max - min);
    rs2 = realsigma*realsigma;
    q = sqrt(xres*yres)/(gwy_data_field_get_xreal(ideal)
                         * gwy_data_field_get_yreal(ideal));

    for (i = 0; i < xres*yres; i++) {
        inorm = fridata[i]*fridata[i] + fiidata[i]*fiidata[i];

        rv = (frmdata[i]*fridata[i] + fimdata[i]*fiidata[i])/(inorm + rs2);
        iv = (-frmdata[i]*fiidata[i] + fimdata[i]*fridata[i])/(inorm + rs2);

        frpsfdata[i] = rv*q;
        fipsfdata[i] = iv*q;
    }

    gwy_data_field_2dfft_raw(frpsf, fipsf, psf, ipsf,
                             GWY_TRANSFORM_DIRECTION_BACKWARD);
    gwy_data_field_2dfft_humanize(psf);
    set_transfer_function_units(ideal, measured, psf);

    gwy_object_unref(frideal);
    gwy_object_unref(fiideal);
    gwy_object_unref(frmeasured);
    gwy_object_unref(fimeasured);
    gwy_object_unref(frpsf);
    gwy_object_unref(fipsf);
    gwy_object_unref(ipsf);
}

static const gchar sigma_key[]   = "/module/psf/sigma";
static const gchar update_key[]  = "/module/psf/update";
static const gchar display_key[] = "/module/psf/display";

static void
psf_sanitize_args(PsfArgs *args)
{
    gwy_app_data_id_verify_channel(&args->op2);
}

static void
psf_load_args(GwyContainer *container, PsfArgs *args)
{
    *args = psf_defaults;

    gwy_container_gis_boolean_by_name(container, update_key, &args->update);
    gwy_container_gis_enum_by_name(container, display_key, &args->display);
    gwy_container_gis_double_by_name(container, sigma_key, &args->sigma);
    args->op2 = op2_id;

    psf_sanitize_args(args);
}

static void
psf_save_args(GwyContainer *container, PsfArgs *args)
{
    op2_id = args->op2;

    gwy_container_set_boolean_by_name(container, update_key, args->update);
    gwy_container_set_enum_by_name(container, display_key, args->display);
    gwy_container_set_double_by_name(container, sigma_key, args->sigma);
}

/* vim: set cin et ts=4 sw=4 cino=>1s,e0,n0,f0,{0,}0,^0,\:1s,=0,g1s,h0,t0,+1s,c3,(0,u0 : */
