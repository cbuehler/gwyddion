/*
 *  @(#) $Id$
 *  Copyright (C) 2003-2018 David Necas (Yeti), Anna Charvatova Campbell
 *  E-mail: yeti@gwyddion.net, acampbellova@cmi.cz
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA  02110-1301, USA.
 */

#include <gtk/gtk.h>
#include <libgwyddion/gwymacros.h>
#include <libgwyddion/gwymath.h>
#include <libgwyddion/gwyversion.h>
#include <libgwyddion/gwyutils.h>
#include <libgwymodule/gwymodule.h>
#include <libprocess/stats.h>
#include <libprocess/gwyprocess.h>
#include <libprocess/filters.h>
#include <libgwydgets/gwydgets.h>
#include <app/gwyapp.h>
#include "config.h"
#include "preview.h"


#define TIPSHAPE_RUN_MODES GWY_RUN_INTERACTIVE

enum {
    MIN_RESOLUTION = 10,
    MAX_RESOLUTION = 1000
};

/* Data for this function. */
typedef struct {
    GwyAppDataId target_graph;
    gboolean calc_unc;
    gdouble uncx;
    gdouble uncy;
    gdouble uncz;
    gdouble zmag;
    gdouble xymag;
    gboolean options_visible;
    gboolean instant_update;
    gint res;
    gboolean fixres;
} TipShapeArgs;

typedef struct {
    GwyContainer *data;
    GwyDataField *dfield;
    GtkWidget *graph;
    GtkWidget *target_graph;
    GtkWidget *calcunc;
    GtkObject *uncx;
    GtkObject *uncy;
    GtkObject *uncz;
    GtkWidget *options;
    GtkWidget *instant_update;
    GtkObject *resolution;
    GtkWidget *fixres;
    GwyGraphModel *gmodel;
    TipShapeArgs *args;
} TipShapeControls;

static gboolean module_register       (void);
static void     tipshape              (GwyContainer *data,
                                       GwyRunType run);
static void     tipshape_dialog       (TipShapeArgs *args,
                                       GwyContainer *data);
static void     tipshape_do           (TipShapeArgs *args,
                                       TipShapeControls *controls);
static void     tipshape_load_args    (GwyContainer *container,
                                       TipShapeArgs *args);
static void     tipshape_save_args    (GwyContainer *container,
                                       TipShapeArgs *args);
static void     tipshape_dialog_update(TipShapeControls *controls,
                                       TipShapeArgs *args);
static gboolean update_graph          (TipShapeArgs *args,
                                       TipShapeControls *controls);
static void     calc_unc_toggled      (TipShapeControls *controls,
                                       GtkToggleButton *button);
static void     unc_x_changed         (GtkObject *obj,
                                       TipShapeControls *controls);
static void     unc_y_changed         (GtkObject *obj,
                                       TipShapeControls *controls);
static void     unc_z_changed         (GtkObject *obj,
                                       TipShapeControls *controls);
static void     resolution_changed    (TipShapeControls *controls,
                                       GtkAdjustment *adj);
static void     fixres_changed        (GtkToggleButton *check,
                                       TipShapeControls *controls);
static void     options_expanded      (GtkExpander *expander,
                                       GParamSpec *pspec,
                                       TipShapeControls *controls);
static void     instant_update_changed(GtkToggleButton *check,
                                       TipShapeControls *controls);
static void     tipshape_dialog_reset (TipShapeControls *controls,
                                       TipShapeArgs *args);
static gboolean tipshape_calc         (GwyDataField *dfield,
                                       GwyDataLine *line,
                                       GwyDataLine *uline,
                                       TipShapeArgs *args);

static const TipShapeArgs tipshape_defaults = {
    GWY_APP_DATA_ID_NONE,
    FALSE,
    0,
    0,
    0,
    1,
    1,
    FALSE,
    FALSE,
    100,
    TRUE,
};

/* The module info. */
static GwyModuleInfo module_info = {
    GWY_MODULE_ABI_VERSION,
    &module_register,
    N_("Calculates the tip area function."),
    "Anna Charvatova Campbell <acampbellova@cmi.cz>",
    PACKAGE_VERSION,
    "Anna Charvatova Campbell",
    "2004",
};

/* This is the ONLY exported symbol.  The argument is the module info.
 * NO semicolon after. */
GWY_MODULE_QUERY2(module_info, tipshape)

    /* Module registering function.
     * Called at Gwyddion startup and registeres one or more function.
     */
static gboolean
module_register(void)
{
    gwy_process_func_register("tipshape",
                              (GwyProcessFunc)&tipshape,
                              N_("/SPM M_odes/_Force and Indentation/_Area function..."),
                              NULL,
                              TIPSHAPE_RUN_MODES,
                              GWY_MENU_FLAG_DATA,
                              N_("Calculate tip shape."));

    return TRUE;
}

static void
tipshape(GwyContainer *data, GwyRunType run)
{
    TipShapeArgs args;

    g_return_if_fail(run & TIPSHAPE_RUN_MODES);
    tipshape_load_args(gwy_app_settings_get(), &args);
    tipshape_dialog(&args, data);
    tipshape_save_args(gwy_app_settings_get(), &args);
}

static void
tipshape_dialog(TipShapeArgs *args, GwyContainer *data)
{
    GtkWidget *dialog, *table, *hbox, *vbox,  *button;
    GtkObject *gtkobj;
    TipShapeControls controls;
    gint response, row;
    GwySIValueFormat *siformat_xy, *siformat_z ;

    gwy_app_data_browser_get_current(GWY_APP_DATA_FIELD, &controls.dfield, 0);
    g_return_if_fail(controls.dfield);

    siformat_xy = gwy_data_field_get_value_format_xy(controls.dfield,
                                                     GWY_SI_UNIT_FORMAT_PLAIN,
                                                     NULL);
    args->xymag = siformat_xy->magnitude;
    siformat_z = gwy_data_field_get_value_format_z(controls.dfield,
                                                   GWY_SI_UNIT_FORMAT_PLAIN,
                                                   NULL);
    args->zmag = siformat_z->magnitude;


    controls.args = args;
    controls.data = data;
    controls.gmodel = gwy_graph_model_new();

    dialog = gtk_dialog_new_with_buttons(_("Tip area function"), NULL, 0, NULL);

    button = gwy_stock_like_button_new(_("Co_mpute"), GTK_STOCK_EXECUTE);
    gtk_dialog_add_action_widget(GTK_DIALOG(dialog), button,
                                 RESPONSE_CALCULATE);

    gtk_dialog_add_button(GTK_DIALOG(dialog), _("_Reset"), RESPONSE_RESET);

    gtk_dialog_add_button(GTK_DIALOG(dialog),
                          GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL);
    gtk_dialog_add_button(GTK_DIALOG(dialog),
                          GTK_STOCK_OK, GTK_RESPONSE_OK);
    gtk_dialog_set_default_response(GTK_DIALOG(dialog), GTK_RESPONSE_OK);
    gwy_help_add_to_proc_dialog(GTK_DIALOG(dialog), GWY_HELP_DEFAULT);

    hbox = gtk_hbox_new(FALSE, 0);
    gtk_box_pack_start(GTK_BOX(GTK_DIALOG(dialog)->vbox), hbox, TRUE, TRUE, 0);

    vbox = gtk_vbox_new(FALSE, 8);
    gtk_container_set_border_width(GTK_CONTAINER(vbox), 4);
    gtk_box_pack_start(GTK_BOX(hbox), vbox, FALSE, FALSE, 0);

    table = gtk_table_new(4, 2, FALSE);
    gtk_table_set_row_spacings(GTK_TABLE(table), 2);
    gtk_table_set_col_spacings(GTK_TABLE(table), 6);
    gtk_box_pack_start(GTK_BOX(vbox), table, FALSE, FALSE, 0);

    row = 0;
    button = gtk_check_button_new_with_mnemonic(_("Calculate uncertainties"));
    controls.calcunc = button;
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(button),
                                 args->calc_unc);
    gtk_table_attach(GTK_TABLE(table), button,
                     0, 1, row, row+1, GTK_EXPAND | GTK_FILL, 0, 0, 0);
    g_signal_connect_swapped(button, "toggled",
                             G_CALLBACK(calc_unc_toggled),
                             &controls);
    row++;


    controls.uncx = gtk_adjustment_new(args->uncx/args->xymag,
                                       0, 1000, 0.00001, 1, 0);
    gtkobj = controls.uncx;

    gwy_table_attach_hscale(table, row,
                            _("_x pixel size uncertainty:"),
                            siformat_xy->units, gtkobj,
                            GWY_HSCALE_DEFAULT);
    gwy_table_hscale_set_sensitive(controls.uncx, args->calc_unc);
    g_signal_connect(gtkobj, "value-changed",
                     G_CALLBACK(unc_x_changed), &controls);


    row++;

    controls.uncy = gtk_adjustment_new(args->uncy/args->xymag,
                                       0, 1000, 0.00001, 1, 0);
    gtkobj = controls.uncy;

    gwy_table_attach_hscale(table, row,
                            _("_y pixel size uncertainty:"),
                            siformat_xy->units, gtkobj,
                            GWY_HSCALE_DEFAULT);
    gwy_table_hscale_set_sensitive(controls.uncy, args->calc_unc);
    g_signal_connect(gtkobj, "value-changed",
                     G_CALLBACK(unc_y_changed), &controls);


    row++;

    controls.uncz = gtk_adjustment_new(args->uncz/args->zmag,
                                       0, 1000, 0.001, 1, 0);
    gtkobj = controls.uncz;

    gwy_table_attach_hscale(table, row,
                            _("Uncertainty _z:"), siformat_z->units, gtkobj,
                            GWY_HSCALE_DEFAULT);
    gwy_table_hscale_set_sensitive(controls.uncz, args->calc_unc);
    g_signal_connect(gtkobj, "value-changed",
                     G_CALLBACK(unc_z_changed), &controls);


    row++;
    /* Options */
    controls.options = gtk_expander_new(_("<b>Options</b>"));
    gtk_expander_set_use_markup(GTK_EXPANDER(controls.options), TRUE);
    gtk_expander_set_expanded(GTK_EXPANDER(controls.options),
                              controls.args->options_visible);
    g_signal_connect(controls.options, "notify::expanded",
                     G_CALLBACK(options_expanded), &controls);
    gtk_box_pack_start(GTK_BOX(vbox), controls.options, FALSE, FALSE, 0);

    table = gtk_table_new(10, 4, FALSE);
    gtk_table_set_col_spacings(GTK_TABLE(table), 6);
    gtk_table_set_row_spacings(GTK_TABLE(table), 2);
    gtk_container_set_border_width(GTK_CONTAINER(table), 4);
    gtk_container_add(GTK_CONTAINER(controls.options), table);
    row = 0;

    controls.instant_update
        = gtk_check_button_new_with_mnemonic(_("_Instant updates"));
    gtk_table_attach(GTK_TABLE(table), controls.instant_update,
                     0, 3, row, row+1, GTK_EXPAND | GTK_FILL, 0, 0, 0);
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(controls.instant_update),
                                 controls.args->instant_update);
    g_signal_connect(controls.instant_update, "toggled",
                     G_CALLBACK(instant_update_changed),
                     &controls);

    row++;

    controls.resolution = gtk_adjustment_new(args->res,
                                             MIN_RESOLUTION, MAX_RESOLUTION,
                                             1, 10, 0);
    gwy_table_attach_hscale(GTK_WIDGET(table), row, _("_Fix res.:"), NULL,
                            controls.resolution,
                            GWY_HSCALE_CHECK | GWY_HSCALE_SQRT);
    g_signal_connect_swapped(controls.resolution, "value-changed",
                             G_CALLBACK(resolution_changed),
                             &controls);
    controls.fixres = gwy_table_hscale_get_check(controls.resolution);
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(controls.fixres),
                                 args->fixres);
    g_signal_connect(controls.fixres, "toggled",
                     G_CALLBACK(fixres_changed), &controls);
    gtk_table_set_row_spacing(GTK_TABLE(table), row, 8);
    row++;




    /* Graph */
    controls.graph = gwy_graph_new(controls.gmodel);
    g_object_unref(controls.gmodel);
    gtk_widget_set_size_request(controls.graph, 400, 300);

    gtk_box_pack_start(GTK_BOX(hbox), controls.graph, TRUE, TRUE, 4);
    gwy_graph_set_status(GWY_GRAPH(controls.graph), GWY_GRAPH_STATUS_XSEL);
    gwy_graph_enable_user_input(GWY_GRAPH(controls.graph), FALSE);


    gtk_widget_show_all(dialog);
    tipshape_dialog_update(&controls, args);

    do {
        response = gtk_dialog_run(GTK_DIALOG(dialog));
        switch (response) {
            case GTK_RESPONSE_CANCEL:
            case GTK_RESPONSE_DELETE_EVENT:
            gtk_widget_destroy(dialog);
            case GTK_RESPONSE_NONE:
            return;
            break;

            case GTK_RESPONSE_OK:
            tipshape_do(args, &controls);
            break;

            case RESPONSE_RESET:
            tipshape_dialog_reset(&controls, args);
            break;

            case RESPONSE_CALCULATE:
            tipshape_dialog_update(&controls, args);
            break;

            default:
            g_assert_not_reached();
            break;
        }
    } while (response != GTK_RESPONSE_OK);

    gtk_widget_destroy(dialog);


}


static void
tipshape_do(TipShapeArgs *args,
            TipShapeControls *controls)
{
    update_graph(args, controls);
    gwy_app_add_graph_or_curves(controls->gmodel,
                                controls->data, &args->target_graph, 2);
}

static gboolean tipshape_calc(GwyDataField *dfield,
                              GwyDataLine *line,
                              GwyDataLine *uline,
                              TipShapeArgs *args)
{
    gint i, j;
    gint xres, yres;
    gint num;
    gdouble zedge, ztmp;
    gdouble dx, dy;
    gdouble *p, *m;
    gdouble uSxy, uSz;
    GwyDataLine *ll;
    GwyDataField *dd, *mask;
    gdouble z, wp, wm;
    gdouble dl;

    xres = dfield->xres;
    yres = dfield->yres;

    dd = gwy_data_field_duplicate(dfield);
    ll = gwy_data_line_new(1, 1.0, FALSE);

    /* top */
    gwy_data_field_get_row(dd, ll, 0);
    zedge = gwy_data_line_get_max(ll);
    /*bottom */
    gwy_data_field_get_row(dd, ll, xres-1);
    ztmp = gwy_data_line_get_max(ll);
    zedge = (zedge > ztmp ? zedge : ztmp);
    /*left */
    gwy_data_field_get_column(dd, ll, 0);
    ztmp = gwy_data_line_get_max(ll);
    zedge = (zedge > ztmp ? zedge : ztmp);
    /*right */
    gwy_data_field_get_column(dd, ll, yres -1);
    ztmp = gwy_data_line_get_max(ll);
    zedge = (zedge > ztmp ? zedge : ztmp);

    gwy_data_field_add(dd, -zedge);

    dx = gwy_data_field_get_xmeasure(dd);
    dy = gwy_data_field_get_ymeasure(dd);


    /* mask data below edge */
    num = 0;
    p = gwy_data_field_get_data(dd);
    mask = create_mask_field(dd);
    m = gwy_data_field_get_data(mask);
    for (j = 0 ; j < yres; j++) {
        for (i = 0; i < xres; i++) {
            if (*p > 0) {
                *m = 1;
                num++;
            }
            p++;
            m++;
        }
    }

    gwy_data_field_area_cdh(dd, mask, line, 0, 0, xres, yres, args->res);
    gwy_data_line_multiply(line, -1.0);
    gwy_data_line_add(line, 1.0);
    gwy_data_line_multiply(line, dx*dy*num);
    gwy_data_line_invert(line, TRUE, FALSE);
    gwy_data_line_set_si_unit_x(line, gwy_data_field_get_si_unit_z(dd));
    gwy_data_line_set_si_unit_y(line,
                                gwy_si_unit_power(gwy_data_field_get_si_unit_xy(dd),
                                                  2, NULL));



    if (args->calc_unc) {

        if (uline->res != args->res)
            gwy_data_line_resample(uline, args->res, GWY_INTERPOLATION_NONE);

        gwy_data_line_copy(line, uline);
        gwy_data_line_set_offset(uline, gwy_data_line_get_offset(line));
        gwy_data_line_set_real(uline, gwy_data_line_get_real(line));
        gwy_data_line_set_si_unit_x(uline, gwy_data_line_get_si_unit_x(line));
        gwy_data_line_set_si_unit_y(uline, gwy_data_line_get_si_unit_y(line));

        uSxy =  args->uncx*args->uncx/dx/dx + args->uncy*args->uncy/dy/dy ;

        dl = line->real/line->res;

        p = gwy_data_line_get_data(uline);
        for (i = 0; i < args->res; i++) {
            z = i*dl;
            wp = gwy_data_line_get_dval_real(line, z+args->uncz+dl/2,
                                             GWY_INTERPOLATION_LINEAR);
            wm = gwy_data_line_get_dval_real(line, z-args->uncz+dl/2,
                                             GWY_INTERPOLATION_LINEAR);

            uSz = (wp - wm)/sqrt(3.);
            *p *= sqrt((uSz/line->data[i])*(uSz/line->data[i]) + uSxy);
            p++;
        }

    }

    g_object_unref(ll);
    g_object_unref(dd);
    return TRUE;

}

static gboolean
update_graph(TipShapeArgs *args,
             TipShapeControls *controls)
{
    GwyDataLine  *line,  *uline;
    GwyGraphCurveModel *gcmodel;


    gwy_graph_model_remove_all_curves(controls->gmodel);
    line = gwy_data_line_new(1, 1.0, FALSE);
    uline = gwy_data_line_new(1, 1.0, FALSE);

    tipshape_calc(controls->dfield, line, uline, args);

    g_object_set(controls->gmodel,
                 "title", "Area function",
                 "axis-label-bottom", "depth",
                 "axis-label-left", "area",
                 NULL);


    gcmodel = gwy_graph_curve_model_new();
    g_object_set(gcmodel,
                 "mode", GWY_GRAPH_CURVE_LINE,
                 "description", "Area function",
                 NULL);

    gwy_graph_curve_model_set_data_from_dataline(gcmodel, line, 0, 0);
    gwy_graph_model_set_units_from_data_line(controls->gmodel, line);
    gwy_graph_model_add_curve(controls->gmodel, gcmodel);

    g_object_unref(gcmodel);


    if (args->calc_unc) {
        gcmodel = gwy_graph_curve_model_new();
        g_object_set(gcmodel,
                     "mode", GWY_GRAPH_CURVE_LINE,
                     "description", "uncertainty",
                     NULL);

        gwy_graph_curve_model_set_data_from_dataline(gcmodel, uline, 0, 0);
        gwy_graph_model_add_curve(controls->gmodel, gcmodel);
        g_object_unref(gcmodel);
    }



    g_object_unref(line);
    g_object_unref(uline);

    return TRUE;
}


static const gchar calc_unc_key[] = "/module/tipshape/calc_unc";
static const gchar uncx_key[] = "/module/tipshape/uncx";
static const gchar uncy_key[] = "/module/tipshape/uncy";
static const gchar uncz_key[] = "/module/tipshape/uncz";
static const gchar options_visible_key[] = "/module/tipshape/options_visible";
static const gchar instant_update_key[]  = "/module/tipshape/instant_update";
static const gchar fixres_key[]          = "/module/tipshape/fixres";
static const gchar resolution_key[]      = "/module/tipshape/resolution";

static void
tipshape_load_args(GwyContainer *container,
                   TipShapeArgs *args)
{
    *args = tipshape_defaults;
    gwy_container_gis_boolean_by_name(container, calc_unc_key, &args->calc_unc);
    gwy_container_gis_double_by_name(container, uncx_key, &args->uncx);
    gwy_container_gis_double_by_name(container, uncy_key, &args->uncy);
    gwy_container_gis_double_by_name(container, uncz_key, &args->uncz);
    gwy_container_gis_boolean_by_name(container, options_visible_key,
                                      &args->options_visible);
    gwy_container_gis_boolean_by_name(container, instant_update_key,
                                      &args->instant_update);
    gwy_container_gis_boolean_by_name(container, fixres_key,
                                      &args->fixres);
    gwy_container_gis_int32_by_name(container, resolution_key,
                                    &args->res);
}

static void
tipshape_save_args(GwyContainer *container,
                   TipShapeArgs *args)
{
    gwy_container_set_boolean_by_name(container, calc_unc_key, args->calc_unc);
    gwy_container_set_double_by_name(container, uncx_key, args->uncx);
    gwy_container_set_double_by_name(container, uncy_key, args->uncy);
    gwy_container_set_double_by_name(container, uncz_key, args->uncz);
    gwy_container_set_boolean_by_name(container, options_visible_key,
                                      args->options_visible);
    gwy_container_set_boolean_by_name(container, instant_update_key,
                                      args->instant_update);
    gwy_container_set_boolean_by_name(container, fixres_key,
                                      args->fixres);
    gwy_container_set_int32_by_name(container, resolution_key,
                                    args->res);
}

static void
tipshape_dialog_update(TipShapeControls *controls,
                       TipShapeArgs *args)
{
    update_graph(args, controls);
}

static void
calc_unc_toggled(TipShapeControls *controls, GtkToggleButton *button)
{
    TipShapeArgs *args;
    args = controls->args;

    controls->args->calc_unc = gtk_toggle_button_get_active(button);

    gwy_table_hscale_set_sensitive(controls->uncx, args->calc_unc);
    gwy_table_hscale_set_sensitive(controls->uncy, args->calc_unc);
    gwy_table_hscale_set_sensitive(controls->uncz, args->calc_unc);

    if (controls->args->instant_update)
        update_graph(controls->args, controls);
}


static void
unc_x_changed(GtkObject *obj,
              TipShapeControls *controls)
{
    controls->args->uncx = gtk_adjustment_get_value(GTK_ADJUSTMENT(obj));
    controls->args->uncx *= controls->args->xymag;
    if (controls->args->instant_update)
        update_graph(controls->args, controls);
}

static void
unc_y_changed(GtkObject *obj,
              TipShapeControls *controls)
{
    controls->args->uncy = gtk_adjustment_get_value(GTK_ADJUSTMENT(obj));
    controls->args->uncy *= controls->args->xymag;
    if (controls->args->instant_update)
        update_graph(controls->args, controls);
}

static void
unc_z_changed(GtkObject *obj,
              TipShapeControls *controls)
{
    controls->args->uncz = gtk_adjustment_get_value(GTK_ADJUSTMENT(obj));
    controls->args->uncz *= controls->args->zmag;
    if (controls->args->instant_update)
        update_graph(controls->args, controls);
}

static void
resolution_changed(TipShapeControls *controls,
                   GtkAdjustment *adj)
{
    controls->args->res = gwy_adjustment_get_int(adj);
    if (controls->args->instant_update)
        update_graph(controls->args, controls);
}

static void
fixres_changed                 (GtkToggleButton *check,
                                TipShapeControls *controls)
{
    controls->args->fixres = gtk_toggle_button_get_active(check);
    if (controls->args->instant_update)
        update_graph(controls->args, controls);
}

static void
options_expanded(GtkExpander *expander,
                 G_GNUC_UNUSED GParamSpec *pspec,
                 TipShapeControls *controls)
{
    controls->args->options_visible = gtk_expander_get_expanded(expander);
}

static void
instant_update_changed(GtkToggleButton *check,
                       TipShapeControls *controls)
{
    controls->args->instant_update = gtk_toggle_button_get_active(check);
    if (controls->args->instant_update)
        update_graph(controls->args, controls);
}


static void
tipshape_dialog_reset(TipShapeControls *controls, TipShapeArgs *args)
{

    *args = tipshape_defaults;
    gtk_adjustment_set_value(GTK_ADJUSTMENT(controls->uncx), args->uncx);
    gtk_adjustment_set_value(GTK_ADJUSTMENT(controls->uncy), args->uncy);
    gtk_adjustment_set_value(GTK_ADJUSTMENT(controls->uncz), args->uncz);

    gwy_table_hscale_set_sensitive(controls->uncx, args->calc_unc);
    gwy_table_hscale_set_sensitive(controls->uncy, args->calc_unc);
    gwy_table_hscale_set_sensitive(controls->uncz, args->calc_unc);

    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(controls->calcunc),
                                 args->calc_unc);
}
/* vim: set cin et ts=4 sw=4 cino=>1s,e0,n0,f0,{0,}0,^0,\:1s,=0,g1s,h0,t0,+1s,c3,(0,u0 : */
