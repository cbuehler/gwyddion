/*
 *  $Id$
 *  Copyright (C) 2009-2017 David Necas (Yeti).
 *  E-mail: yeti@gwyddion.net.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA 02110-1301, USA.
 */

#include "config.h"
#include <string.h>
#include <gtk/gtk.h>
#include <libgwyddion/gwymacros.h>
#include <libgwyddion/gwymath.h>
#include <libgwyddion/gwyrandgenset.h>
#include <libprocess/stats.h>
#include <libprocess/arithmetic.h>
#include <libprocess/spline.h>
#include <libgwydgets/gwystock.h>
#include <libgwymodule/gwymodule-process.h>
#include <app/gwyapp.h>
#include "dimensions.h"
#include "preview.h"

#define FIBRE_SYNTH_RUN_MODES (GWY_RUN_IMMEDIATE | GWY_RUN_INTERACTIVE)

/* Large spline oversampling is OK because straight segments converge very
 * quickly so we only get substantial oversampling in sharp turns – and we want
 * it there. */
#define OVERSAMPLE 12.0

/* Always consume this many random numbers from RNG_DEFORM when creating
 * a spline for generator stability. */
enum {
    FIBRE_MAX_POINTS = 80,
};

enum {
    PAGE_DIMENSIONS = 0,
    PAGE_GENERATOR  = 1,
    PAGE_PLACEMENT  = 2,
    PAGE_NPAGES
};

typedef enum {
    RNG_WIDTH,
    RNG_HEIGHT,
    RNG_POSITION,
    RNG_ANGLE,
    RNG_HTRUNC,
    RNG_DEFORM,
    RNG_SEGVAR,
    RNG_NRNGS
} FibreSynthRng;

typedef enum {
    FIBRE_SYNTH_CIRCLE    = 0,
    FIBRE_SYNTH_TRIANGLE  = 1,
    FIBRE_SYNTH_SQUARE    = 2,
    FIBRE_SYNTH_PARABOLA  = 3,
    FIBRE_SYNTH_QUADRATIC = 4,
    FIBRE_SYNTH_NTYPES
} FibreSynthType;

typedef struct _FibreSynthControls FibreSynthControls;

typedef struct {
    guint size;
    guint len;
    gint *data;
} IntList;

typedef struct {
    gdouble u;
    gdouble wfactor;
    gdouble hfactor;
} FibreSegmentVar;

typedef gdouble (*FibreShapeFunc)(gdouble v);

/* This scheme makes the object type list easily reordeable in the GUI without
 * changing the ids.  */
typedef struct {
    FibreSynthType type;
    const gchar *name;
} FibreSynthFeature;

typedef struct {
    gint active_page;
    gint seed;
    gboolean randomize;
    gboolean update;
    FibreSynthType type;
    gdouble coverage;
    gdouble width;
    gdouble width_noise;
    gdouble width_var;
    gdouble height;
    gboolean height_bound;
    gdouble height_noise;
    gdouble height_var;
    gdouble htrunc;
    gdouble htrunc_noise;
    gdouble angle;
    gdouble angle_noise;
    gdouble latdeform;
    gdouble latdeform_noise;
    gdouble lendeform;
    gdouble lendeform_noise;
    gdouble deform_density;
} FibreSynthArgs;

struct _FibreSynthControls {
    FibreSynthArgs *args;
    GwyDimensions *dims;
    GwyRandGenSet *rngset;
    GtkWidget *dialog;
    GtkWidget *view;
    GtkWidget *update;
    GtkWidget *update_now;
    GtkObject *seed;
    GtkWidget *randomize;
    GtkTable *table;
    GtkWidget *type;
    GtkObject *coverage;
    GtkWidget *coverage_value;
    GtkWidget *coverage_units;
    GtkObject *width;
    GtkWidget *width_value;
    GtkWidget *width_units;
    GtkObject *width_noise;
    GtkObject *width_var;
    GtkObject *height;
    GtkWidget *height_units;
    GtkWidget *height_init;
    GtkWidget *height_bound;
    GtkObject *height_noise;
    GtkObject *height_var;
    GtkObject *htrunc;
    GtkObject *htrunc_noise;
    GtkObject *angle;
    GtkObject *angle_noise;
    GtkObject *latdeform;
    GtkObject *latdeform_noise;
    GtkObject *lendeform;
    GtkObject *lendeform_noise;
    GtkObject *deform_density;
    GwyContainer *mydata;
    GwyDataField *surface;
    gdouble pxsize;
    gdouble zscale;
    gboolean in_init;
    gulong sid;
};

static gboolean   module_register       (void);
static void       fibre_synth           (GwyContainer *data,
                                         GwyRunType run);
static void       run_noninteractive    (FibreSynthArgs *args,
                                         const GwyDimensionArgs *dimsargs,
                                         GwyRandGenSet *rngset,
                                         GwyContainer *data,
                                         GwyDataField *dfield,
                                         gint oldid,
                                         GQuark quark);
static gboolean   fibre_synth_dialog    (FibreSynthArgs *args,
                                         GwyDimensionArgs *dimsargs,
                                         GwyRandGenSet *rngset,
                                         GwyContainer *data,
                                         GwyDataField *dfield,
                                         gint id);
static GtkWidget* shape_selector_new    (FibreSynthControls *controls);
static void       update_controls       (FibreSynthControls *controls,
                                         FibreSynthArgs *args);
static void       page_switched         (FibreSynthControls *controls,
                                         GtkNotebookPage *page,
                                         gint pagenum);
static void       update_values         (FibreSynthControls *controls);
static void       shape_selected        (GtkComboBox *combo,
                                         FibreSynthControls *controls);
static void       height_init_clicked   (FibreSynthControls *controls);
static void       update_coverage_value (FibreSynthControls *controls);
static gint       attach_deformation    (FibreSynthControls *controls,
                                         const gchar *name,
                                         gint row,
                                         GtkObject **adj,
                                         gdouble *target);
static gint       attach_segvar         (FibreSynthControls *controls,
                                         gint row,
                                         GtkObject **adj,
                                         gdouble *target);
static void       fibre_synth_invalidate(FibreSynthControls *controls);
static gboolean   preview_gsource       (gpointer user_data);
static void       preview               (FibreSynthControls *controls);
static gboolean   fibre_synth_do        (const FibreSynthArgs *args,
                                         const GwyDimensionArgs *dimsargs,
                                         GwyRandGenSet *rngset,
                                         GwyDataField *dfield,
                                         GwySetMessageFunc set_message,
                                         GwySetFractionFunc set_fraction);
static void       fibre_synth_add_one   (GwyDataField *surface,
                                         GwyDataField *fibre,
                                         GwyDataField *ucoord,
                                         IntList *usedpts,
                                         GwySpline *spline,
                                         GArray *segvar,
                                         const FibreSynthArgs *args,
                                         const GwyDimensionArgs *dimsargs,
                                         GwyRandGenSet *rngset);
static glong      calculate_n_fibres    (const FibreSynthArgs *args,
                                         guint xres,
                                         guint yres);
static void       fibre_synth_load_args (GwyContainer *container,
                                         FibreSynthArgs *args,
                                         GwyDimensionArgs *dimsargs);
static void       fibre_synth_save_args (GwyContainer *container,
                                         const FibreSynthArgs *args,
                                         const GwyDimensionArgs *dimsargs);

#define GWY_SYNTH_CONTROLS FibreSynthControls
#define GWY_SYNTH_INVALIDATE(controls) \
    update_coverage_value(controls); \
    fibre_synth_invalidate(controls)

#include "synth.h"

static const FibreSynthArgs fibre_synth_defaults = {
    PAGE_DIMENSIONS,
    42, TRUE, TRUE,
    FIBRE_SYNTH_CIRCLE,
    0.5,
    5.0, 0.0, 0.0,
    1.0, TRUE, 0.0, 0.0,
    1.0, 0.0,
    0.0, 0.0,
    0.1, 0.0,
    0.05, 0.0,
    5.0,
};

static const GwyDimensionArgs dims_defaults = GWY_DIMENSION_ARGS_INIT;

static const FibreSynthFeature features[] = {
    { FIBRE_SYNTH_CIRCLE,    N_("Semi-circle"),      },
    { FIBRE_SYNTH_TRIANGLE,  N_("Triangle"),         },
    { FIBRE_SYNTH_SQUARE,    N_("Rectangle"),        },
    { FIBRE_SYNTH_PARABOLA,  N_("Parabola"),         },
    { FIBRE_SYNTH_QUADRATIC, N_("Quadratic spline"), },
};

static GwyModuleInfo module_info = {
    GWY_MODULE_ABI_VERSION,
    &module_register,
    N_("Generates surfaces composed from randomly placed fibers."),
    "Yeti <yeti@gwyddion.net>",
    "1.0",
    "David Nečas (Yeti)",
    "2017",
};

GWY_MODULE_QUERY2(module_info, fibre_synth)

static gboolean
module_register(void)
{
    gwy_process_func_register("fibre_synth",
                              (GwyProcessFunc)&fibre_synth,
                              N_("/S_ynthetic/_Deposition/_Fibers..."),
                              GWY_STOCK_SYNTHETIC_FIBRES,
                              FIBRE_SYNTH_RUN_MODES,
                              0,
                              N_("Generate surface of randomly placed fibers"));

    return TRUE;
}

static void
fibre_synth(GwyContainer *data, GwyRunType run)
{
    FibreSynthArgs args;
    GwyDimensionArgs dimsargs;
    GwyRandGenSet *rngset;
    GwyDataField *dfield;
    GQuark quark;
    gint id;

    g_return_if_fail(run & FIBRE_SYNTH_RUN_MODES);
    fibre_synth_load_args(gwy_app_settings_get(), &args, &dimsargs);
    gwy_app_data_browser_get_current(GWY_APP_DATA_FIELD, &dfield,
                                     GWY_APP_DATA_FIELD_ID, &id,
                                     GWY_APP_DATA_FIELD_KEY, &quark,
                                     0);

    rngset = gwy_rand_gen_set_new(RNG_NRNGS);
    if (run == GWY_RUN_IMMEDIATE
        || fibre_synth_dialog(&args, &dimsargs, rngset, data, dfield, id))
        run_noninteractive(&args, &dimsargs, rngset, data, dfield, id, quark);

    if (run == GWY_RUN_INTERACTIVE)
        fibre_synth_save_args(gwy_app_settings_get(), &args, &dimsargs);

    gwy_rand_gen_set_free(rngset);
    gwy_dimensions_free_args(&dimsargs);
}

static void
run_noninteractive(FibreSynthArgs *args,
                   const GwyDimensionArgs *dimsargs,
                   GwyRandGenSet *rngset,
                   GwyContainer *data,
                   GwyDataField *dfield,
                   gint oldid,
                   GQuark quark)
{
    GwyDataField *newfield;
    GwySIUnit *siunit;
    gboolean replace = dimsargs->replace && dfield;
    gboolean add = dimsargs->add && dfield;
    gboolean ok;
    gint newid;

    if (args->randomize)
        args->seed = g_random_int() & 0x7fffffff;

    if (add || replace) {
        if (add)
            newfield = gwy_data_field_duplicate(dfield);
        else
            newfield = gwy_data_field_new_alike(dfield, TRUE);
    }
    else {
        gdouble mag = pow10(dimsargs->xypow10) * dimsargs->measure;
        newfield = gwy_data_field_new(dimsargs->xres, dimsargs->yres,
                                      mag*dimsargs->xres, mag*dimsargs->yres,
                                      TRUE);

        siunit = gwy_data_field_get_si_unit_xy(newfield);
        gwy_si_unit_set_from_string(siunit, dimsargs->xyunits);

        siunit = gwy_data_field_get_si_unit_z(newfield);
        gwy_si_unit_set_from_string(siunit, dimsargs->zunits);
    }

    gwy_app_wait_start(gwy_app_find_window_for_channel(data, oldid),
                       _("Initializing..."));
    ok = fibre_synth_do(args, dimsargs, rngset, newfield,
                        gwy_app_wait_set_message, gwy_app_wait_set_fraction);
    gwy_app_wait_finish();

    if (!ok) {
        g_object_unref(newfield);
        return;
    }

    if (replace) {
        gwy_app_undo_qcheckpointv(data, 1, &quark);
        gwy_container_set_object(data, gwy_app_get_data_key_for_id(oldid),
                                 newfield);
        gwy_app_channel_log_add_proc(data, oldid, oldid);
        g_object_unref(newfield);
        newid = oldid;
    }
    else {
        if (data) {
            newid = gwy_app_data_browser_add_data_field(newfield, data, TRUE);
            if (oldid != -1)
                gwy_app_sync_data_items(data, data, oldid, newid, FALSE,
                                        GWY_DATA_ITEM_GRADIENT,
                                        0);
        }
        else {
            newid = 0;
            data = gwy_container_new();
            gwy_container_set_object(data, gwy_app_get_data_key_for_id(newid),
                                     newfield);
            gwy_app_data_browser_add(data);
            gwy_app_data_browser_reset_visibility(data,
                                                  GWY_VISIBILITY_RESET_SHOW_ALL);
            g_object_unref(data);
        }

        gwy_app_set_data_field_title(data, newid, _("Generated"));
        gwy_app_channel_log_add_proc(data, add ? oldid : -1, newid);
        g_object_unref(newfield);
    }
}

static gboolean
fibre_synth_dialog(FibreSynthArgs *args,
                   GwyDimensionArgs *dimsargs,
                   GwyRandGenSet *rngset,
                   GwyContainer *data,
                   GwyDataField *dfield_template,
                   gint id)
{
    GtkWidget *dialog, *table, *vbox, *hbox, *notebook;
    FibreSynthControls controls;
    GwyDataField *dfield;
    gboolean finished;
    gint response;
    gint row;

    gwy_clear(&controls, 1);
    controls.in_init = TRUE;
    controls.args = args;
    controls.rngset = rngset;
    controls.pxsize = 1.0;
    dialog = gtk_dialog_new_with_buttons(_("Random Fibers"),
                                         NULL, 0,
                                         _("_Reset"), RESPONSE_RESET,
                                         GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
                                         GTK_STOCK_OK, GTK_RESPONSE_OK,
                                         NULL);
    gtk_dialog_set_default_response(GTK_DIALOG(dialog), GTK_RESPONSE_OK);
    gwy_help_add_to_proc_dialog(GTK_DIALOG(dialog), GWY_HELP_DEFAULT);
    controls.dialog = dialog;

    hbox = gtk_hbox_new(FALSE, 2);
    gtk_box_pack_start(GTK_BOX(GTK_DIALOG(dialog)->vbox), hbox,
                       FALSE, FALSE, 4);

    vbox = gtk_vbox_new(FALSE, 4);
    gtk_box_pack_start(GTK_BOX(hbox), vbox, FALSE, FALSE, 4);

    controls.mydata = gwy_container_new();
    dfield = gwy_data_field_new(PREVIEW_SIZE, PREVIEW_SIZE,
                                dimsargs->measure*PREVIEW_SIZE,
                                dimsargs->measure*PREVIEW_SIZE,
                                TRUE);
    gwy_container_set_object_by_name(controls.mydata, "/0/data", dfield);
    if (dfield_template) {
        gwy_app_sync_data_items(data, controls.mydata, id, 0, FALSE,
                                GWY_DATA_ITEM_PALETTE,
                                0);
        controls.surface = gwy_synth_surface_for_preview(dfield_template,
                                                         PREVIEW_SIZE);
        controls.zscale = 3.0*gwy_data_field_get_rms(dfield_template);
    }
    controls.view = create_preview(controls.mydata, 0, PREVIEW_SIZE, FALSE);
    gtk_box_pack_start(GTK_BOX(vbox), controls.view, FALSE, FALSE, 0);

    gtk_box_pack_start(GTK_BOX(vbox),
                       gwy_synth_instant_updates_new(&controls,
                                                     &controls.update_now,
                                                     &controls.update,
                                                     &args->update),
                       FALSE, FALSE, 0);
    g_signal_connect_swapped(controls.update_now, "clicked",
                             G_CALLBACK(preview), &controls);

    gtk_box_pack_start(GTK_BOX(vbox),
                       gwy_synth_random_seed_new(&controls,
                                                 &controls.seed, &args->seed),
                       FALSE, FALSE, 0);

    controls.randomize = gwy_synth_randomize_new(&args->randomize);
    gtk_box_pack_start(GTK_BOX(vbox), controls.randomize, FALSE, FALSE, 0);

    notebook = gtk_notebook_new();
    gtk_box_pack_start(GTK_BOX(hbox), notebook, TRUE, TRUE, 4);
    g_signal_connect_swapped(notebook, "switch-page",
                             G_CALLBACK(page_switched), &controls);

    controls.dims = gwy_dimensions_new(dimsargs, dfield_template);
    gtk_notebook_append_page(GTK_NOTEBOOK(notebook),
                             gwy_dimensions_get_widget(controls.dims),
                             gtk_label_new(_("Dimensions")));
    if (controls.dims->add)
        g_signal_connect_swapped(controls.dims->add, "toggled",
                                 G_CALLBACK(fibre_synth_invalidate), &controls);

    table = gtk_table_new(15 + (dfield_template ? 1 : 0), 3, FALSE);
    controls.table = GTK_TABLE(table);
    gtk_table_set_row_spacings(GTK_TABLE(table), 2);
    gtk_table_set_col_spacings(GTK_TABLE(table), 6);
    gtk_container_set_border_width(GTK_CONTAINER(table), 4);
    gtk_notebook_append_page(GTK_NOTEBOOK(notebook), table,
                             gtk_label_new(_("Generator")));
    row = 0;

    controls.type = shape_selector_new(&controls);
    gwy_table_attach_adjbar(table, row, _("_Shape:"), NULL,
                            GTK_OBJECT(controls.type),
                            GWY_HSCALE_WIDGET_NO_EXPAND);
    row++;

    controls.coverage = gtk_adjustment_new(args->coverage,
                                           0.001, 20.0, 0.001, 1.0, 0);
    g_object_set_data(G_OBJECT(controls.coverage), "target", &args->coverage);
    gwy_table_attach_adjbar(table, row, _("Co_verage:"), NULL,
                            controls.coverage, GWY_HSCALE_SQRT);
    g_signal_connect_swapped(controls.coverage, "value-changed",
                             G_CALLBACK(gwy_synth_double_changed), &controls);
    row++;

    controls.coverage_value = gtk_label_new(NULL);
    gtk_misc_set_alignment(GTK_MISC(controls.coverage_value), 1.0, 0.5);
    gtk_table_attach(controls.table, controls.coverage_value,
                     1, 2, row, row+1, GTK_FILL, 0, 0, 0);

    controls.coverage_units = gtk_label_new(_("obj."));
    gtk_misc_set_alignment(GTK_MISC(controls.coverage_units), 0.0, 0.5);
    gtk_table_attach(GTK_TABLE(table), controls.coverage_units,
                     2, 3, row, row+1, GTK_FILL, 0, 0, 0);
    gtk_table_set_row_spacing(GTK_TABLE(table), row, 8);
    row++;

    gtk_table_attach(GTK_TABLE(table), gwy_label_new_header(_("Size")),
                     0, 2, row, row+1, GTK_FILL, 0, 0, 0);
    row++;

    controls.width = gtk_adjustment_new(args->width, 1.0, 1000.0, 0.1, 10.0, 0);
    row = gwy_synth_attach_lateral(&controls, row, controls.width, &args->width,
                                   _("_Width:"), GWY_HSCALE_LOG, NULL,
                                   &controls.width_value,
                                   &controls.width_units);
    row = gwy_synth_attach_variance(&controls, row,
                                    &controls.width_noise, &args->width_noise);
    row = attach_segvar(&controls, row, &controls.width_var, &args->width_var);

    gtk_table_set_row_spacing(GTK_TABLE(table), row-1, 8);
    gtk_table_attach(GTK_TABLE(table), gwy_label_new_header(_("Height")),
                     0, 2, row, row+1, GTK_FILL, 0, 0, 0);
    row++;

    row = gwy_synth_attach_height(&controls, row,
                                  &controls.height, &args->height,
                                  _("_Height:"), NULL, &controls.height_units);

    if (dfield_template) {
        controls.height_init
            = gtk_button_new_with_mnemonic(_("_Like Current Image"));
        g_signal_connect_swapped(controls.height_init, "clicked",
                                 G_CALLBACK(height_init_clicked), &controls);
        gtk_table_attach(GTK_TABLE(table), controls.height_init,
                         0, 2, row, row+1, GTK_FILL, 0, 0, 0);
        row++;
    }

    controls.height_bound
        = gtk_check_button_new_with_mnemonic(_("Scales _with width"));
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(controls.height_bound),
                                 args->height_bound);
    g_object_set_data(G_OBJECT(controls.height_bound),
                      "target", &args->height_bound);
    gtk_table_attach(GTK_TABLE(table), controls.height_bound,
                     0, 2, row, row+1, GTK_FILL, 0, 0, 0);
    g_signal_connect_swapped(controls.height_bound, "toggled",
                             G_CALLBACK(gwy_synth_boolean_changed), &controls);
    row++;

    row = gwy_synth_attach_variance(&controls, row,
                                    &controls.height_noise,
                                    &args->height_noise);
    row = attach_segvar(&controls, row, &controls.height_var,
                        &args->height_var);

    row = gwy_synth_attach_truncation(&controls, row,
                                      &controls.htrunc, &args->htrunc);
    row = gwy_synth_attach_variance(&controls, row,
                                    &controls.htrunc_noise,
                                    &args->htrunc_noise);

    table = gtk_table_new(8, 3, FALSE);
    controls.table = GTK_TABLE(table);
    gtk_table_set_row_spacings(GTK_TABLE(table), 2);
    gtk_table_set_col_spacings(GTK_TABLE(table), 6);
    gtk_container_set_border_width(GTK_CONTAINER(table), 4);
    gtk_notebook_append_page(GTK_NOTEBOOK(notebook), table,
                             gtk_label_new(_("Placement")));
    row = 0;

    row = gwy_synth_attach_orientation(&controls, row,
                                       &controls.angle, &args->angle);
    row = gwy_synth_attach_variance(&controls, row,
                                    &controls.angle_noise, &args->angle_noise);

    gtk_table_set_row_spacing(GTK_TABLE(table), row-1, 8);
    gtk_table_attach(GTK_TABLE(table), gwy_label_new_header(_("Deformation")),
                     0, 2, row, row+1, GTK_FILL, 0, 0, 0);
    row++;

    controls.deform_density
        = gtk_adjustment_new(args->deform_density,
                             0.5, FIBRE_MAX_POINTS - 1.0, 0.01, 1.0, 0.0);
    g_object_set_data(G_OBJECT(controls.deform_density), "target",
                      &args->deform_density);
    gwy_table_attach_adjbar(table, row, _("Densi_ty:"), NULL,
                            controls.deform_density, GWY_HSCALE_SQRT);
    g_signal_connect_swapped(controls.deform_density, "value-changed",
                             G_CALLBACK(gwy_synth_double_changed), &controls);
    row++;

    gtk_table_set_row_spacing(GTK_TABLE(table), row-1, 8);
    row = attach_deformation(&controls, _("_Lateral:"), row,
                             &controls.latdeform, &args->latdeform);
    row = gwy_synth_attach_variance(&controls, row,
                                    &controls.latdeform_noise,
                                    &args->latdeform_noise);

    gtk_table_set_row_spacing(GTK_TABLE(table), row-1, 8);
    row = attach_deformation(&controls, _("Le_ngthwise:"), row,
                             &controls.lendeform, &args->lendeform);
    row = gwy_synth_attach_variance(&controls, row,
                                    &controls.lendeform_noise,
                                    &args->lendeform_noise);

    gtk_widget_show_all(dialog);
    controls.in_init = FALSE;
    /* Must be done when widgets are shown, see GtkNotebook docs */
    gtk_notebook_set_current_page(GTK_NOTEBOOK(notebook), args->active_page);
    update_values(&controls);
    fibre_synth_invalidate(&controls);

    finished = FALSE;
    while (!finished) {
        response = gtk_dialog_run(GTK_DIALOG(dialog));
        switch (response) {
            case GTK_RESPONSE_CANCEL:
            case GTK_RESPONSE_DELETE_EVENT:
            case GTK_RESPONSE_OK:
            gtk_widget_destroy(dialog);
            case GTK_RESPONSE_NONE:
            finished = TRUE;
            break;

            case RESPONSE_RESET:
            {
                gboolean temp = args->update;
                gint temp2 = args->active_page;
                *args = fibre_synth_defaults;
                args->active_page = temp2;
                args->update = temp;
            }
            controls.in_init = TRUE;
            update_controls(&controls, args);
            controls.in_init = FALSE;
            if (args->update)
                preview(&controls);
            break;

            default:
            g_assert_not_reached();
            break;
        }
    }

    if (controls.sid) {
        g_source_remove(controls.sid);
        controls.sid = 0;
    }
    g_object_unref(controls.mydata);
    GWY_OBJECT_UNREF(controls.surface);
    gwy_dimensions_free(controls.dims);

    return response == GTK_RESPONSE_OK;
}

static GtkWidget*
shape_selector_new(FibreSynthControls *controls)
{
    GtkWidget *combo;
    GwyEnum *model;
    guint n, i;

    n = G_N_ELEMENTS(features);
    model = g_new(GwyEnum, n);
    for (i = 0; i < n; i++) {
        model[i].value = features[i].type;
        model[i].name = features[i].name;
    }

    combo = gwy_enum_combo_box_new(model, n,
                                   G_CALLBACK(shape_selected), controls,
                                   controls->args->type, TRUE);
    g_object_weak_ref(G_OBJECT(combo), (GWeakNotify)g_free, model);

    return combo;
}

static void
update_controls(FibreSynthControls *controls,
                FibreSynthArgs *args)
{
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(controls->update),
                                 args->update);
    gtk_adjustment_set_value(GTK_ADJUSTMENT(controls->seed), args->seed);
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(controls->randomize),
                                 args->randomize);
    gtk_adjustment_set_value(GTK_ADJUSTMENT(controls->coverage),
                             args->coverage);
    gtk_adjustment_set_value(GTK_ADJUSTMENT(controls->width), args->width);
    gtk_adjustment_set_value(GTK_ADJUSTMENT(controls->width_noise),
                             args->width_noise);
    gtk_adjustment_set_value(GTK_ADJUSTMENT(controls->height), args->height);
    gtk_adjustment_set_value(GTK_ADJUSTMENT(controls->height_noise),
                             args->height_noise);
    gtk_adjustment_set_value(GTK_ADJUSTMENT(controls->htrunc), args->htrunc);
    gtk_adjustment_set_value(GTK_ADJUSTMENT(controls->htrunc_noise),
                             args->htrunc_noise);
    gtk_adjustment_set_value(GTK_ADJUSTMENT(controls->angle), args->angle);
    gtk_adjustment_set_value(GTK_ADJUSTMENT(controls->angle_noise),
                             args->angle_noise);
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(controls->height_bound),
                                 args->height_bound);
    gwy_enum_combo_box_set_active(GTK_COMBO_BOX(controls->type), args->type);
}

static void
page_switched(FibreSynthControls *controls,
              G_GNUC_UNUSED GtkNotebookPage *page,
              gint pagenum)
{
    if (controls->in_init)
        return;

    controls->args->active_page = pagenum;
    if (pagenum == PAGE_GENERATOR)
        update_values(controls);
}

static void
update_values(FibreSynthControls *controls)
{
    GwyDimensions *dims = controls->dims;

    controls->pxsize = dims->args->measure * pow10(dims->args->xypow10);
    if (controls->height_units)
        gtk_label_set_markup(GTK_LABEL(controls->height_units),
                             dims->zvf->units);
    gtk_label_set_markup(GTK_LABEL(controls->width_units),
                         dims->xyvf->units);

    gwy_synth_update_lateral(controls, GTK_ADJUSTMENT(controls->width));
    update_coverage_value(controls);
}

static void
shape_selected(GtkComboBox *combo,
               FibreSynthControls *controls)
{
    controls->args->type = gwy_enum_combo_box_get_active(combo);
    update_coverage_value(controls);
    fibre_synth_invalidate(controls);
}

static void
height_init_clicked(FibreSynthControls *controls)
{
    gdouble mag = pow10(controls->dims->args->zpow10);
    gtk_adjustment_set_value(GTK_ADJUSTMENT(controls->height),
                             controls->zscale/mag);
}

static void
update_coverage_value(FibreSynthControls *controls)
{
    glong nobjects;
    guchar buf[32];

    if (controls->in_init)
        return;

    nobjects = calculate_n_fibres(controls->args,
                                  controls->dims->args->xres,
                                  controls->dims->args->yres);
    g_snprintf(buf, sizeof(buf), "%ld", nobjects);
    gtk_label_set_text(GTK_LABEL(controls->coverage_value), buf);
}

static gint
attach_deformation(FibreSynthControls *controls,
                   const gchar *name,
                   gint row,
                   GtkObject **adj,
                   gdouble *target)
{
    GtkWidget *spin;

    *adj = gtk_adjustment_new(*target, 0.0, 1.0, 0.001, 0.1, 0);
    g_object_set_data(G_OBJECT(*adj), "target", target);

    spin = gwy_table_attach_adjbar(GTK_WIDGET(controls->table),
                                   row, name, NULL, *adj,
                                   GWY_HSCALE_SQRT);
    gtk_spin_button_set_digits(GTK_SPIN_BUTTON(spin), 3);
    g_signal_connect_swapped(*adj, "value-changed",
                             G_CALLBACK(gwy_synth_double_changed), controls);

    row++;

    return row;
}

static gint
attach_segvar(FibreSynthControls *controls,
              gint row,
              GtkObject **adj,
              gdouble *target)
{
    GtkWidget *spin;

    *adj = gtk_adjustment_new(*target, 0.0, 1.0, 0.001, 0.1, 0);
    g_object_set_data(G_OBJECT(*adj), "target", target);

    spin = gwy_table_attach_adjbar(GTK_WIDGET(controls->table),
                                   row, _("Along fiber:"), NULL, *adj,
                                   GWY_HSCALE_SQRT);
    gtk_spin_button_set_digits(GTK_SPIN_BUTTON(spin), 3);
    g_signal_connect_swapped(*adj, "value-changed",
                             G_CALLBACK(gwy_synth_double_changed), controls);

    row++;

    return row;
}

static void
fibre_synth_invalidate(FibreSynthControls *controls)
{
    /* create preview if instant updates are on */
    if (controls->args->update && !controls->in_init && !controls->sid) {
        controls->sid = g_idle_add_full(G_PRIORITY_LOW, preview_gsource,
                                        controls, NULL);
    }
}

static gboolean
preview_gsource(gpointer user_data)
{
    FibreSynthControls *controls = (FibreSynthControls*)user_data;
    controls->sid = 0;

    preview(controls);

    return FALSE;
}

static void
preview(FibreSynthControls *controls)
{
    FibreSynthArgs *args = controls->args;
    GwyDataField *dfield;
    gboolean ok;

    dfield = GWY_DATA_FIELD(gwy_container_get_object_by_name(controls->mydata,
                                                             "/0/data"));
    if (controls->dims->args->add && controls->surface)
        gwy_data_field_copy(controls->surface, dfield, FALSE);
    else
        gwy_data_field_clear(dfield);

    if (args->update) {
        fibre_synth_do(args, controls->dims->args, controls->rngset, dfield,
                       NULL, NULL);
        gwy_data_field_data_changed(dfield);
        return;
    }

    gwy_app_wait_start(GTK_WINDOW(controls->dialog), _("Initializing..."));
    ok = fibre_synth_do(args, controls->dims->args, controls->rngset, dfield,
                        gwy_app_wait_set_message, gwy_app_wait_set_fraction);
    gwy_app_wait_finish();

    if (!ok) {
        if (controls->dims->args->add && controls->surface)
            gwy_data_field_copy(controls->surface, dfield, FALSE);
        else
            gwy_data_field_clear(dfield);
    }
    gwy_data_field_data_changed(dfield);
}

static inline IntList*
int_list_new(guint prealloc)
{
    IntList *list = g_slice_new0(IntList);
    prealloc = MAX(prealloc, 16);
    list->size = prealloc;
    list->data = g_new(gint, list->size);
    return list;
}

static inline void
int_list_add(IntList *list, gint i)
{
    if (G_UNLIKELY(list->len == list->size)) {
        list->size = MAX(2*list->size, 16);
        list->data = g_renew(gint, list->data, list->size);
    }

    list->data[list->len] = i;
    list->len++;
}

static inline void
int_list_free(IntList *list)
{
    g_free(list->data);
    g_slice_free(IntList, list);
}

static inline GwyXY
vecdiff(const GwyXY *a, const GwyXY *b)
{
    GwyXY r;

    r.x = a->x - b->x;
    r.y = a->y - b->y;

    return r;
}

static inline GwyXY
veclincomb(const GwyXY *a, gdouble qa, const GwyXY *b, gdouble qb)
{
    GwyXY r;

    r.x = qa*a->x + qb*b->x;
    r.y = qa*a->y + qb*b->y;

    return r;
}

static inline gdouble
dotprod(const GwyXY *a, const GwyXY *b)
{
    return a->x*b->x + a->y*b->y;
}

static inline gdouble
vecnorm2(const GwyXY *a)
{
    return a->x*a->x + a->y*a->y;
}

static inline gdouble
vecprodz(const GwyXY *a, const GwyXY *b)
{
    return a->x*b->y - a->y*b->x;
}

static void
order_trapezoid_vertically(const GwyXY *p, const GwyXY *q,
                           const GwyXY *pp, const GwyXY *qq,
                           const GwyXY **top, const GwyXY **m1,
                           const GwyXY **m2, const GwyXY **bottom)
{
    const GwyXY *pts[4];
    guint i, j;

    pts[0] = p;
    pts[1] = q;
    pts[2] = pp;
    pts[3] = qq;
    for (i = 0; i < 3; i++) {
        for (j = i; j < 4; j++) {
            if (pts[i]->y > pts[j]->y
                || (pts[i]->y == pts[j]->y && pts[i]->x == pts[j]->x))
                GWY_SWAP(const GwyXY*, pts[i], pts[j]);
        }
    }

    *top = pts[0];
    *m1 = pts[1];
    *m2 = pts[2];
    *bottom = pts[3];
}

static void
fill_vsegment(const GwyXY *lfrom, const GwyXY *lto,
              const GwyXY *rfrom, const GwyXY *rto,
              gdouble *fdata, gdouble *udata, gint xres, gint yres,
              gint ifrom, gint ito,
              const GwyXY *r, const GwyXY *rp, const GwyXY *rq, const GwyXY *d,
              gdouble wp, gdouble wq, gdouble lp, gdouble lq,
              gboolean positive,
              IntList *usedpts)
{
    gdouble denoml, denomr;
    gint i, j, jfrom, jto, jleftlim, jrightlim;
    gdouble tl, tr, u, v, s, w, dnorm;
    gdouble *frow, *urow;
    GwyXY pt, rr;

    dnorm = vecnorm2(d);
    denoml = MAX(lto->y - lfrom->y, 1e-9);
    denomr = MAX(rto->y - rfrom->y, 1e-9);

    jleftlim = (gint)floor(fmin(lfrom->x, lto->x) - 1.0);
    jleftlim = MAX(jleftlim, 0);
    jrightlim = (gint)ceil(fmax(rfrom->x, rto->x) + 1.0);
    jrightlim = MIN(jrightlim, xres-1);

    for (i = ifrom; i <= ito; i++) {
        tl = (i - lfrom->y)/denoml;
        jfrom = (gint)floor(tl*lto->x + (1.0 - tl)*lfrom->x);
        jfrom = MAX(jfrom, jleftlim);

        tr = (i - rfrom->y)/denomr;
        jto = (gint)ceil(tr*rto->x + (1.0 - tr)*rfrom->x);
        jto = MIN(jto, jrightlim);

        pt.y = i - r->y;
        frow = fdata + i*xres;
        urow = udata + i*xres;
        g_assert_cmpint(i, >=, 0);
        g_assert_cmpint(i, <, yres);
        for (j = jfrom; j <= jto; j++) {
            pt.x = j - r->x;
            /* u is the approximate coordinate along the segment
             * v is the approximate distance from the centre
             * both are from [0, 1] inside the trapezoid.
             * Exact coordinates can be introduced and calculated but it
             * seems to require solving some ugly quadratic equations and is
             * not necessary for rendering the fibre.  If we want fibre height
             * varying continuously along its length we also need to remember u
             * coordinates somewhere. */
            u = dotprod(&pt, d)/dnorm + 0.5;
            u = CLAMP(u, 0.0, 1.0);
            w = wp*(1.0 - u) + wq*u;
            rr = veclincomb(rp, 1.0 - u, rq, u);
            /* This is one Newton iteration of w*|r'| from initial estimate
             * |r'| ≈ w, which should be always good.  It avoid slow sqrt()
             * and as a bonus, it is a sum of two positive terms so it has to
             * behave nicely. */
            s = 0.5*(w*w + vecnorm2(&rr));
            v = dotprod(&pt, &rr)/s;
            g_assert_cmpint(j, >=, 0);
            g_assert_cmpint(j, <, xres);
            if (v >= 0.0 && v <= 1.0 && v < fabs(frow[j])) {
                /* Only add the pixel when first encountering it. */
                if (frow[j] == G_MAXDOUBLE)
                    int_list_add(usedpts, i*xres + j);
                frow[j] = positive ? v : -v;
                urow[j] = lp*(1.0 - u) + lq*u;
            }
        }
    }
}

/* p-q is the fibre ‘spine’, pp-qq is the outer boundary. */
static void
fill_trapezoid(gdouble *fdata, gdouble *udata, gint xres, gint yres,
               const GwyXY *p, const GwyXY *q,
               const GwyXY *pp, const GwyXY *qq,
               gdouble wp, gdouble wq, gdouble lp, gdouble lq,
               gboolean positive, IntList *usedpts)
{
    const GwyXY *top, *mid1, *mid2, *bottom;
    const GwyXY *lfrom, *lto, *rfrom, *rto;
    GwyXY d, dd, diag, s, r, rp, rq;
    gboolean mid1_is_right, mid2_is_right;
    gint ifrom, ito;

    /* If we are totally outside, abort.  This does not detect trapezoids
     * hugging the rectangle boundary line, but there are only a small
     * fraction of them. */
    if (fmin(fmin(p->x, q->x), fmin(pp->x, qq->x)) > xres+1.0)
        return;
    if (fmin(fmin(p->y, q->y), fmin(pp->y, qq->y)) > yres+1.0)
        return;
    if (fmax(fmax(p->x, q->x), fmax(pp->x, qq->x)) < -1.0)
        return;
    if (fmax(fmax(p->y, q->y), fmax(pp->y, qq->y)) < -1.0)
        return;

    /* If the points on the outer boundary are in reverse order (too large
     * width compared to local curvature), just invert the order to get some
     * kind of untwisted trapezoid.  The result still does not have to be
     * convex, but the filling does not fail if the outer boundary is weird
     * because we do not use the pp-qq vector. */
    d = vecdiff(q, p);
    dd = vecdiff(qq, pp);
    if (dotprod(&d, &dd) < 0.0) {
        GWY_SWAP(const GwyXY*, pp, qq);
    }

    r = veclincomb(p, 0.5, q, 0.5);
    rp = vecdiff(pp, p);
    rq = vecdiff(qq, q);

    order_trapezoid_vertically(p, q, pp, qq, &top, &mid1, &mid2, &bottom);
    diag = vecdiff(bottom, top);
    s = vecdiff(mid1, top);
    mid1_is_right = (vecprodz(&s, &diag) >= 0.0);
    s = vecdiff(mid2, top);
    mid2_is_right = (vecprodz(&s, &diag) > 0.0);

    /* The top triangle.  May be skipped if the top line is horizontal. */
    if (mid1->y > top->y + 1e-9) {
        ifrom = (gint)floor(top->y);
        ifrom = MAX(ifrom, 0);
        ito = (gint)ceil(mid1->y);
        ito = MIN(ito, yres-1);

        lfrom = rfrom = top;
        rto = mid1_is_right ? mid1 : (mid2_is_right ? mid2 : bottom);
        lto = mid1_is_right ? (mid2_is_right ? bottom : mid2) : mid1;
        fill_vsegment(lfrom, lto, rfrom, rto,
                      fdata, udata, xres, yres, ifrom, ito,
                      &r, &rp, &rq, &d, wp, wq, lp, lq, positive, usedpts);
    }

    /* The middle part.  May be skipped if the mid1 and mid2 are on the same
     * horizontal line.*/
    if (mid2->y > mid1->y + 1e-9) {
        ifrom = (gint)floor(mid1->y);
        ifrom = MAX(ifrom, 0);
        ito = (gint)ceil(mid2->y);
        ito = MIN(ito, yres-1);

        lfrom = mid1_is_right ? top : mid1;
        rfrom = mid1_is_right ? mid1 : top;
        lto = mid2_is_right ? bottom : mid2;
        rto = mid2_is_right ? mid2 : bottom;
        fill_vsegment(lfrom, lto, rfrom, rto,
                      fdata, udata, xres, yres, ifrom, ito,
                      &r, &rp, &rq, &d, wp, wq, lp, lq, positive, usedpts);
    }

    /* The bottom triangle.  May be skipped if the bottom line is horizontal. */
    if (bottom->y > mid2->y + 1e-9) {
        ifrom = (gint)floor(mid2->y);
        ifrom = MAX(ifrom, 0);
        ito = (gint)ceil(bottom->y);
        ito = MIN(ito, yres-1);

        lfrom = mid2_is_right ? (mid1_is_right ? bottom : mid1) : mid2;
        rfrom = mid2_is_right ? mid2 : (mid1_is_right ? mid1 : top);
        lto = rto = bottom;
        fill_vsegment(lfrom, lto, rfrom, rto,
                      fdata, udata, xres, yres, ifrom, ito,
                      &r, &rp, &rq, &d, wp, wq, lp, lq, positive, usedpts);
    }
}

static gboolean
fibre_synth_do(const FibreSynthArgs *args,
               const GwyDimensionArgs *dimsargs,
               GwyRandGenSet *rngset,
               GwyDataField *dfield,
               GwySetMessageFunc set_message,
               GwySetFractionFunc set_fraction)
{
    GwyDataField *fibre, *ucoord, *extfield;
    GwySpline *spline;
    GArray *segvar;
    IntList *usedpts;
    guint i, nfib, xres, yres, extw;
    gboolean ok = FALSE;

    gwy_rand_gen_set_init(rngset, args->seed);

    xres = gwy_data_field_get_xres(dfield);
    yres = gwy_data_field_get_yres(dfield);
    extw = MIN(xres, yres)/8 + GWY_ROUND(2*args->width) + 16;
    extfield = gwy_data_field_extend(dfield, extw, extw, extw, extw,
                                     GWY_EXTERIOR_BORDER_EXTEND, 0.0, FALSE);

    usedpts = int_list_new(0);
    segvar = g_array_new(FALSE, FALSE, sizeof(FibreSegmentVar));
    fibre = gwy_data_field_new_alike(extfield, TRUE);
    ucoord = gwy_data_field_new_alike(extfield, TRUE);
    spline = gwy_spline_new();
    gwy_data_field_fill(fibre, G_MAXDOUBLE);

    if (set_message && !set_message(_("Generating fibers...")))
        goto finish;

    nfib = calculate_n_fibres(args, xres, yres);
    for (i = 0; i < nfib; i++) {
        fibre_synth_add_one(extfield, fibre, ucoord, usedpts, spline, segvar,
                            args, dimsargs, rngset);
        if (set_fraction && !set_fraction((i + 1.0)/nfib))
            goto finish;
    }
    gwy_data_field_area_copy(extfield, dfield, extw, extw, xres, yres, 0, 0);

    ok = TRUE;

finish:
    g_array_free(segvar, TRUE);
    gwy_spline_free(spline);
    int_list_free(usedpts);
    g_object_unref(extfield);
    g_object_unref(fibre);
    g_object_unref(ucoord);

    return ok;
}

static gdouble
generate_deformed(GwyRandGenSet *rngset, gdouble deformation, gdouble noise)
{
    gdouble delta;

    delta = gwy_rand_gen_set_gaussian(rngset, RNG_DEFORM, noise);
    delta = deformation*exp(delta);
    return gwy_rand_gen_set_gaussian(rngset, RNG_DEFORM, delta);
}

static void
calculate_segment_var(const GwyXY *xy, guint n, GArray *segvar,
                      GwyRandGenSet *rngset, gdouble ptstep,
                      gdouble width_var,
                      gdouble height_var, gboolean height_bound)
{
    FibreSegmentVar *segvardata;
    gdouble s, l;
    guint i;

    g_array_set_size(segvar, n);
    segvardata = &g_array_index(segvar, FibreSegmentVar, 0);
    segvardata[0].u = 0.0;
    segvardata[0].wfactor = 0.0;
    segvardata[0].hfactor = 0.0;
    for (i = 1; i < n; i++) {
        GwyXY d = vecdiff(xy + i, xy + i-1);

        l = sqrt(vecnorm2(&d))/OVERSAMPLE;
        segvardata[i].u = segvardata[i-1].u + l;

        /* Mix a new random number with the previous one for short segments. */
        l = fmin(l/ptstep, 1.0);
        l *= l;
        s = gwy_rand_gen_set_gaussian(rngset, RNG_SEGVAR, width_var);
        segvardata[i].wfactor = (1.0 - l)*segvardata[i-1].wfactor + l*s;
        s = gwy_rand_gen_set_gaussian(rngset, RNG_SEGVAR, height_var);
        segvardata[i].hfactor = (1.0 - l)*segvardata[i-1].hfactor + l*s;
    }

    for (i = 0; i < n; i++) {
        segvardata[i].wfactor = exp(2.0*segvardata[i].wfactor);
        segvardata[i].hfactor = exp(2.0*segvardata[i].hfactor);
        if (height_bound)
            segvardata[i].hfactor *= segvardata[i].wfactor;
    }
}

static void
generate_fibre_spline(gint xres, gint yres,
                      GwySpline *spline,
                      GArray *segvar,
                      const FibreSynthArgs *args,
                      GwyRandGenSet *rngset)
{
    static GwyXY points[2*FIBRE_MAX_POINTS + 1];

    const GwyXY *xy;
    gdouble angle = args->angle, ca, sa;
    gdouble xoff, yoff, x, y, s, ptstep;
    guint i, npts;

    angle = args->angle;
    if (args->angle_noise)
        angle += gwy_rand_gen_set_gaussian(rngset, RNG_ANGLE,
                                           2*args->angle_noise);
    ca = cos(angle);
    sa = sin(angle);

    s = hypot(xres, yres);
    x = s*(gwy_rand_gen_set_double(rngset, RNG_POSITION) - 0.5);
    y = s*(gwy_rand_gen_set_double(rngset, RNG_POSITION) - 0.5);
    xoff = xres/2 + ca*x + sa*y;
    yoff = yres/2 - sa*x + ca*y;
    ptstep = s/args->deform_density;

    /* Generate the full number of points for image stability when parameters
     * change. */
    points[FIBRE_MAX_POINTS].x = xoff;
    points[FIBRE_MAX_POINTS].y = yoff;
    for (i = 1; i < FIBRE_MAX_POINTS; i++) {
        x = ptstep*(i + generate_deformed(rngset,
                                          args->lendeform,
                                          args->lendeform_noise));
        y = ptstep*generate_deformed(rngset,
                                     args->latdeform, args->latdeform_noise);
        points[FIBRE_MAX_POINTS + i].x = ca*x + sa*y + xoff;
        points[FIBRE_MAX_POINTS + i].y = -sa*x + ca*y + yoff;

        x = -ptstep*(i + generate_deformed(rngset,
                                           args->lendeform,
                                           args->lendeform_noise));
        y = ptstep*generate_deformed(rngset,
                                     args->latdeform, args->latdeform_noise);
        points[FIBRE_MAX_POINTS - i].x = ca*x + sa*y + xoff;
        points[FIBRE_MAX_POINTS - i].y = -sa*x + ca*y + yoff;
    }

    /* Generate the last point always undisturbed so it cannot lie inside. */
    x = ptstep*FIBRE_MAX_POINTS;
    points[2*FIBRE_MAX_POINTS].x = ca*x + xoff;
    points[2*FIBRE_MAX_POINTS].y = -sa*x + yoff;

    x = -ptstep*FIBRE_MAX_POINTS;
    points[0].x = ca*x + xoff;
    points[0].y = -sa*x + yoff;

    for (i = 0; i < G_N_ELEMENTS(points); i++) {
        points[i].x *= OVERSAMPLE;
        points[i].y *= OVERSAMPLE;
    }

    gwy_spline_set_points(spline, points, 2*FIBRE_MAX_POINTS + 1);

    /* XXX: This depends on spline not freeing xy[] before it makes a copy. */
    xy = gwy_spline_sample_naturally(spline, &npts);
    gwy_spline_set_points(spline, xy, npts);

    calculate_segment_var(xy, npts, segvar, rngset, ptstep,
                          args->width_var,
                          args->height_var, args->height_bound);
}

static void
fibre_synth_add_one(GwyDataField *surface,
                    GwyDataField *fibre,
                    GwyDataField *ucoord,
                    IntList *usedpts,
                    GwySpline *spline,
                    GArray *segvar,
                    const FibreSynthArgs *args,
                    const GwyDimensionArgs *dimsargs,
                    GwyRandGenSet *rngset)
{
    gdouble height_base = args->height * pow10(dimsargs->zpow10);
    FibreSegmentVar *segvardata;
    gdouble z, m, width, height, htrunc, u;
    guint i, k, npts;
    const GwyXY *xy, *txy;
    gint j, xres, yres;
    gdouble *data, *fdata, *udata;
    gboolean needs_heightvar;

    xres = gwy_data_field_get_xres(fibre);
    yres = gwy_data_field_get_yres(fibre);
    fdata = gwy_data_field_get_data(fibre);
    udata = gwy_data_field_get_data(ucoord);
    data = gwy_data_field_get_data(surface);

    needs_heightvar = (args->height_var > 0.0
                       || (args->width_var > 0.0 && args->height_bound));

    width = 0.5*args->width;
    if (args->width_noise) {
        width *= exp(gwy_rand_gen_set_gaussian(rngset, RNG_WIDTH,
                                               args->width_noise));
    }

    height = height_base;
    if (args->height_bound)
        height *= width/args->width;
    if (args->height_noise) {
        height *= exp(gwy_rand_gen_set_gaussian(rngset, RNG_HEIGHT,
                                                args->height_noise));
    }

    /* Use a specific distribution for htrunc. */
    if (args->htrunc_noise) {
        gdouble q = exp(gwy_rand_gen_set_gaussian(rngset, RNG_HTRUNC,
                                                  args->htrunc_noise));
        htrunc = q/(q + 1.0/args->htrunc - 1.0);
    }
    else
        htrunc = args->htrunc;

    generate_fibre_spline(xres, yres, spline, segvar, args, rngset);
    npts = gwy_spline_get_npoints(spline);
    xy = gwy_spline_get_points(spline);
    txy = gwy_spline_get_tangents(spline);
    segvardata = &g_array_index(segvar, FibreSegmentVar, 0);

    for (k = 0; k+1 < npts; k++) {
        GwyXY p, q, pp, qq;
        gdouble wp = width*segvardata[k].wfactor;
        gdouble wq = width*segvardata[k+1].wfactor;

        p.x = xy[k].x/OVERSAMPLE;
        p.y = xy[k].y/OVERSAMPLE;
        q.x = xy[k+1].x/OVERSAMPLE;
        q.y = xy[k+1].y/OVERSAMPLE;

        pp.x = p.x - wp*txy[k].y;
        pp.y = p.y + wp*txy[k].x;
        qq.x = q.x - wq*txy[k+1].y;
        qq.y = q.y + wq*txy[k+1].x;
        fill_trapezoid(fdata, udata, xres, yres, &p, &q, &pp, &qq,
                       wp, wq, k, k+1, TRUE, usedpts);

        pp.x = p.x + wp*txy[k].y;
        pp.y = p.y - wp*txy[k].x;
        qq.x = q.x + wq*txy[k+1].y;
        qq.y = q.y - wq*txy[k+1].x;
        fill_trapezoid(fdata, udata, xres, yres, &p, &q, &pp, &qq,
                       wp, wq, k, k+1, FALSE, usedpts);
    }

    m = G_MAXDOUBLE;
    for (k = 0; k < usedpts->len; k++)
        m = fmin(m, data[usedpts->data[k]]);
    for (k = 0; k < usedpts->len; k++) {
        i = usedpts->data[k];
        z = fdata[i];
        if (args->type == FIBRE_SYNTH_CIRCLE)
            z = sqrt(1.0 - fmin(z*z, 1.0));
        else if (args->type == FIBRE_SYNTH_TRIANGLE)
            z = 1.0 - fabs(z);
        else if (args->type == FIBRE_SYNTH_SQUARE)
            z = 1.0;
        else if (args->type == FIBRE_SYNTH_PARABOLA)
            z = 1.0 - z*z;
        else if (args->type == FIBRE_SYNTH_QUADRATIC) {
            z = fabs(z);
            z = (z <= 1.0/3.0) ? 0.75*(1.0 - 3*z*z) : 1.125*(1.0 - z)*(1.0 - z);
        }
        else {
            g_assert_not_reached();
        }

        if (z > htrunc)
            z = htrunc;
        z *= height;

        if (needs_heightvar) {
            u = udata[i];
            j = floor(u);
            u -= j;
            if (G_UNLIKELY(j >= npts-1)) {
                j = npts-2;
                u = 1.0;
            }
            else if (G_UNLIKELY(j < 0)) {
                j = 0;
                u = 0.0;
            }
            z *= (1.0 - u)*segvardata[j].hfactor + u*segvardata[j+1].hfactor;
        }

        data[i] = fmax(data[i], m + z);
        fdata[i] = G_MAXDOUBLE;
    }
    usedpts->len = 0;
}

static glong
calculate_n_fibres(const FibreSynthArgs *args, guint xres, guint yres)
{
    /* The distribution of area differs from the distribution of widths. */
    gdouble noise_corr = exp(args->width_noise*args->width_noise);
    /* FIXME: Should correct for deformation which increases the length,
     * possibly for orientation distribution (orthogonal are shorter but
     * more likely completely inside, so the dependence is unclear). */
    gdouble length = hypot(xres, yres);
    gdouble mean_fibre_area = 0.125*args->width * length * noise_corr;
    gdouble must_cover = args->coverage*xres*yres;
    return (glong)ceil(must_cover/mean_fibre_area);
}

static const gchar active_page_key[]     = "/module/fibre_synth/active_page";
static const gchar angle_key[]           = "/module/fibre_synth/angle";
static const gchar angle_noise_key[]     = "/module/fibre_synth/angle_noise";
static const gchar coverage_key[]        = "/module/fibre_synth/coverage";
static const gchar deform_density_key[]  = "/module/fibre_synth/deform_density";
static const gchar height_bound_key[]    = "/module/fibre_synth/height_bound";
static const gchar height_key[]          = "/module/fibre_synth/height";
static const gchar height_noise_key[]    = "/module/fibre_synth/height_noise";
static const gchar htrunc_key[]          = "/module/fibre_synth/htrunc";
static const gchar htrunc_noise_key[]    = "/module/fibre_synth/htrunc_noise";
static const gchar latdeform_key[]       = "/module/fibre_synth/latdeform";
static const gchar latdeform_noise_key[] = "/module/fibre_synth/latdeform_noise";
static const gchar lendeform_key[]       = "/module/fibre_synth/lendeform";
static const gchar lendeform_noise_key[] = "/module/fibre_synth/lendeform_noise";
static const gchar prefix[]              = "/module/fibre_synth";
static const gchar randomize_key[]       = "/module/fibre_synth/randomize";
static const gchar seed_key[]            = "/module/fibre_synth/seed";
static const gchar type_key[]            = "/module/fibre_synth/type";
static const gchar update_key[]          = "/module/fibre_synth/update";
static const gchar width_key[]           = "/module/fibre_synth/width";
static const gchar width_noise_key[]     = "/module/fibre_synth/width_noise";
static const gchar width_var_key[]       = "/module/fibre_synth/width_var";
static const gchar height_var_key[]      = "/module/fibre_synth/height_var";

static void
fibre_synth_sanitize_args(FibreSynthArgs *args)
{
    args->active_page = CLAMP(args->active_page,
                              PAGE_DIMENSIONS, PAGE_NPAGES-1);
    args->update = !!args->update;
    args->seed = MAX(0, args->seed);
    args->randomize = !!args->randomize;
    args->type = MIN(args->type, FIBRE_SYNTH_NTYPES-1);
    args->coverage = CLAMP(args->coverage, 0.001, 20.0);
    args->width = CLAMP(args->width, 1.0, 1000.0);
    args->width_noise = CLAMP(args->width_noise, 0.0, 1.0);
    args->width_var = CLAMP(args->width_var, 0.0, 1.0);
    args->height = CLAMP(args->height, 0.001, 10000.0);
    args->height_noise = CLAMP(args->height_noise, 0.0, 1.0);
    args->height_var = CLAMP(args->height_var, 0.0, 1.0);
    args->height_bound = !!args->height_bound;
    args->htrunc = CLAMP(args->htrunc, 0.001, 1.0);
    args->htrunc_noise = CLAMP(args->htrunc_noise, 0.0, 1.0);
    args->angle = CLAMP(args->angle, -G_PI, G_PI);
    args->angle_noise = CLAMP(args->angle_noise, 0.0, 1.0);
    args->latdeform = CLAMP(args->latdeform, 0.0, 1.0);
    args->latdeform_noise = CLAMP(args->latdeform_noise, 0.0, 1.0);
    args->lendeform = CLAMP(args->lendeform, 0.0, 1.0);
    args->lendeform_noise = CLAMP(args->lendeform_noise, 0.0, 1.0);
    args->deform_density = CLAMP(args->deform_density,
                                 0.5, FIBRE_MAX_POINTS - 1.0);
}

static void
fibre_synth_load_args(GwyContainer *container,
                      FibreSynthArgs *args,
                      GwyDimensionArgs *dimsargs)
{
    *args = fibre_synth_defaults;

    gwy_container_gis_int32_by_name(container, active_page_key,
                                    &args->active_page);
    gwy_container_gis_boolean_by_name(container, update_key, &args->update);
    gwy_container_gis_int32_by_name(container, seed_key, &args->seed);
    gwy_container_gis_boolean_by_name(container, randomize_key,
                                      &args->randomize);
    gwy_container_gis_enum_by_name(container, type_key, &args->type);
    gwy_container_gis_double_by_name(container, coverage_key, &args->coverage);
    gwy_container_gis_double_by_name(container, width_key, &args->width);
    gwy_container_gis_double_by_name(container, width_noise_key,
                                     &args->width_noise);
    gwy_container_gis_double_by_name(container, width_var_key,
                                     &args->width_var);
    gwy_container_gis_double_by_name(container, height_key, &args->height);
    gwy_container_gis_double_by_name(container, height_noise_key,
                                     &args->height_noise);
    gwy_container_gis_double_by_name(container, height_var_key,
                                     &args->height_var);
    gwy_container_gis_boolean_by_name(container, height_bound_key,
                                      &args->height_bound);
    gwy_container_gis_double_by_name(container, htrunc_key, &args->htrunc);
    gwy_container_gis_double_by_name(container, htrunc_noise_key,
                                     &args->htrunc_noise);
    gwy_container_gis_double_by_name(container, angle_key, &args->angle);
    gwy_container_gis_double_by_name(container, angle_noise_key,
                                     &args->angle_noise);
    gwy_container_gis_double_by_name(container, latdeform_key,
                                     &args->latdeform);
    gwy_container_gis_double_by_name(container, latdeform_noise_key,
                                     &args->latdeform_noise);
    gwy_container_gis_double_by_name(container, lendeform_key,
                                     &args->lendeform);
    gwy_container_gis_double_by_name(container, lendeform_noise_key,
                                     &args->lendeform_noise);
    gwy_container_gis_double_by_name(container, deform_density_key,
                                     &args->deform_density);
    fibre_synth_sanitize_args(args);

    gwy_clear(dimsargs, 1);
    gwy_dimensions_copy_args(&dims_defaults, dimsargs);
    gwy_dimensions_load_args(dimsargs, container, prefix);
}

static void
fibre_synth_save_args(GwyContainer *container,
                      const FibreSynthArgs *args,
                      const GwyDimensionArgs *dimsargs)
{
    gwy_container_set_int32_by_name(container, active_page_key,
                                    args->active_page);
    gwy_container_set_boolean_by_name(container, update_key, args->update);
    gwy_container_set_int32_by_name(container, seed_key, args->seed);
    gwy_container_set_boolean_by_name(container, randomize_key,
                                      args->randomize);
    gwy_container_set_enum_by_name(container, type_key, args->type);
    gwy_container_set_double_by_name(container, coverage_key, args->coverage);
    gwy_container_set_double_by_name(container, width_key, args->width);
    gwy_container_set_double_by_name(container, width_noise_key,
                                     args->width_noise);
    gwy_container_set_double_by_name(container, width_var_key, args->width_var);
    gwy_container_set_double_by_name(container, height_key, args->height);
    gwy_container_set_double_by_name(container, height_noise_key,
                                     args->height_noise);
    gwy_container_set_double_by_name(container, height_var_key,
                                     args->height_var);
    gwy_container_set_boolean_by_name(container, height_bound_key,
                                      args->height_bound);
    gwy_container_set_double_by_name(container, htrunc_key, args->htrunc);
    gwy_container_set_double_by_name(container, htrunc_noise_key,
                                     args->htrunc_noise);
    gwy_container_set_double_by_name(container, angle_key, args->angle);
    gwy_container_set_double_by_name(container, angle_noise_key,
                                     args->angle_noise);
    gwy_container_set_double_by_name(container, latdeform_key, args->latdeform);
    gwy_container_set_double_by_name(container, latdeform_noise_key,
                                     args->latdeform_noise);
    gwy_container_set_double_by_name(container, lendeform_key, args->lendeform);
    gwy_container_set_double_by_name(container, lendeform_noise_key,
                                     args->lendeform_noise);
    gwy_container_set_double_by_name(container, deform_density_key,
                                     args->deform_density);

    gwy_dimensions_save_args(dimsargs, container, prefix);
}

/* vim: set cin et ts=4 sw=4 cino=>1s,e0,n0,f0,{0,}0,^0,\:1s,=0,g1s,h0,t0,+1s,c3,(0,u0 : */
