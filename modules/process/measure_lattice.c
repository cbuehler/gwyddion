/*
 *  $Id$
 *  Copyright (C) 2015-2017 David Necas (Yeti).
 *  E-mail: yeti@gwyddion.net.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA 02110-1301, USA.
 */

#include "config.h"
#include <stdlib.h>
#include <string.h>
#include <gtk/gtk.h>
#include <libgwyddion/gwymacros.h>
#include <libgwyddion/gwymath.h>
#include <libprocess/gwyprocesstypes.h>
#include <libprocess/arithmetic.h>
#include <libprocess/stats.h>
#include <libprocess/correct.h>
#include <libprocess/filters.h>
#include <libprocess/grains.h>
#include <libprocess/elliptic.h>
#include <libprocess/inttrans.h>
#include <libgwydgets/gwydataview.h>
#include <libgwydgets/gwylayer-basic.h>
#include <libgwydgets/gwydgetutils.h>
#include <libgwydgets/gwyradiobuttons.h>
#include <libgwydgets/gwycombobox.h>
#include <libgwydgets/gwystock.h>
#include <libgwymodule/gwymodule-process.h>
#include <app/gwymoduleutils.h>
#include <app/gwyapp.h>
#include "preview.h"

#define LATMEAS_RUN_MODES (GWY_RUN_INTERACTIVE)

typedef enum {
    IMAGE_DATA,
    IMAGE_ACF,
    IMAGE_PSDF,
    IMAGE_NMODES
} ImageMode;

typedef enum {
    SELECTION_LATTICE,
    SELECTION_POINT,
    SELECTION_NMODES,
} SelectionMode;

/* Keep it simple and use a predefined set of zooms, these seem suitable. */
typedef enum {
    ZOOM_1 = 1,
    ZOOM_4 = 4,
    ZOOM_16 = 16
} ZoomType;

typedef struct {
    ImageMode image_mode;
    SelectionMode selection_mode;
    ZoomType zoom_acf;
    ZoomType zoom_psdf;
    gboolean fix_hacf;
    GwyResultsReportType report_style;
    /* Cache */
    GType lattice_layer;
    GType point_layer;
    GType lattice_selection;
    GType point_selection;
} LatMeasArgs;

typedef struct {
    LatMeasArgs *args;
    GwyResults *results;
    GtkWidget *dialog;
    GtkWidget *view;
    GwyVectorLayer *vlayer;
    GwySelection *selection;
    gulong selection_id;
    GwyContainer *mydata;
    GtkWidget *zoom_label;
    GSList *zoom;
    GSList *image_mode;
    GSList *selection_mode;
    GtkWidget *fix_hacf;
    GwySIValueFormat *vf;
    GwySIValueFormat *vfphi;
    GtkWidget *a1_x;
    GtkWidget *a1_y;
    GtkWidget *a1;
    GtkWidget *phi1;
    GtkWidget *a2_x;
    GtkWidget *a2_y;
    GtkWidget *a2;
    GtkWidget *phi2;
    GtkWidget *phi;
    GtkWidget *rexport;
    /* We always keep the direct-space selection here. */
    gdouble xy[4];
} LatMeasControls;

static gboolean   module_register       (void);
static void       measure_lattice       (GwyContainer *data,
                                         GwyRunType run);
static void       lat_meas_dialog       (LatMeasArgs *args,
                                         GwyContainer *data,
                                         GwyDataField *dfield,
                                         gint id);
static GtkWidget* make_lattice_table    (LatMeasControls *controls);
static void       do_estimate           (LatMeasControls *controls);
static void       refine                (LatMeasControls *controls);
static void       init_selection        (LatMeasControls *controls);
static void       set_selection         (LatMeasControls *controls,
                                         const gdouble *xy);
static gboolean   get_selection         (LatMeasControls *controls,
                                         gdouble *xy);
static void       image_mode_changed    (GtkToggleButton *button,
                                         LatMeasControls *controls);
static void       selection_mode_changed(GtkToggleButton *button,
                                         LatMeasControls *controls);
static void       zoom_changed          (GtkRadioButton *button,
                                         LatMeasControls *controls);
static void       fix_hacf_changed      (LatMeasControls *controls,
                                         GtkToggleButton *toggle);
static void       report_style_changed  (LatMeasControls *controls,
                                         GwyResultsExport *rexport);
static void       calculate_acf_full    (LatMeasControls *controls,
                                         GwyDataField *dfield);
static void       calculate_psdf_full   (LatMeasControls *controls,
                                         GwyDataField *dfield);
static void       calculate_zoomed_field(LatMeasControls *controls);
static void       selection_changed     (LatMeasControls *controls);
static gboolean   transform_selection   (gdouble *xy);
static void       invert_matrix         (gdouble *dest,
                                         const gdouble *src);
static gdouble    matrix_det            (const gdouble *m);
static void       load_args             (GwyContainer *container,
                                         LatMeasArgs *args);
static void       save_args             (GwyContainer *container,
                                         LatMeasArgs *args);
static void       sanitize_args         (LatMeasArgs *args);

static const LatMeasArgs lat_meas_defaults = {
    IMAGE_DATA, SELECTION_LATTICE, ZOOM_1, ZOOM_1,
    FALSE,
    GWY_RESULTS_REPORT_COLON,
    /* Cache */
    0, 0, 0, 0,
};

static GwyModuleInfo module_info = {
    GWY_MODULE_ABI_VERSION,
    &module_register,
    N_("Measures parameters of two-dimensional lattices."),
    "Yeti <yeti@gwyddion.net>",
    "2.1",
    "David Nečas (Yeti)",
    "2015",
};

GWY_MODULE_QUERY2(module_info, measure_lattice)

static gboolean
module_register(void)
{
    gwy_process_func_register("measure_lattice",
                              (GwyProcessFunc)&measure_lattice,
                              N_("/_Statistics/Measure _Lattice..."),
                              GWY_STOCK_MEASURE_LATTICE,
                              LATMEAS_RUN_MODES,
                              GWY_MENU_FLAG_DATA,
                              N_("Measure lattice"));

    return TRUE;
}

static void
measure_lattice(GwyContainer *data, GwyRunType run)
{
    LatMeasArgs args;
    GwyDataField *dfield;
    gint id;

    g_return_if_fail(run & LATMEAS_RUN_MODES);
    load_args(gwy_app_settings_get(), &args);
    args.lattice_layer = g_type_from_name("GwyLayerLattice");
    args.point_layer = g_type_from_name("GwyLayerPoint");
    args.lattice_selection = g_type_from_name("GwySelectionLattice");
    args.point_selection = g_type_from_name("GwySelectionPoint");
    g_assert(args.lattice_layer);
    g_assert(args.point_layer);

    gwy_app_data_browser_get_current(GWY_APP_DATA_FIELD, &dfield,
                                     GWY_APP_DATA_FIELD_ID, &id,
                                     0);
    g_return_if_fail(dfield);

    lat_meas_dialog(&args, data, dfield, id);
    save_args(gwy_app_settings_get(), &args);
}

static void
lat_meas_dialog(LatMeasArgs *args,
                GwyContainer *data,
                GwyDataField *dfield,
                gint id)
{
    GtkWidget *hbox, *hbox2, *label, *lattable, *alignment;
    GtkDialog *dialog;
    GtkTable *table;
    GSList *l;
    LatMeasControls controls;
    ImageMode image_mode;
    SelectionMode selection_mode;
    gint response, row;
    GwyPixmapLayer *layer;
    GwyVectorLayer *vlayer;
    GwySelection *selection;
    GwyResults *results;
    GwyResultsExport *rexport;
    gchar selkey[40];

    gwy_clear(&controls, 1);
    controls.args = args;
    controls.vlayer = NULL;

    /* Start with ACF and LATTICE selection because that is nice to initialise.
     * We switch at the end. */
    image_mode = args->image_mode;
    selection_mode = args->selection_mode;
    args->image_mode = IMAGE_ACF;
    args->selection_mode = SELECTION_LATTICE;

    controls.results = results = gwy_results_new();
    gwy_results_add_header(results, N_("Measure Lattice"));
    gwy_results_add_value_str(results, "file", N_("File"));
    gwy_results_add_value_str(results, "image", N_("Image"));
    gwy_results_add_separator(results);

    gwy_results_add_header(results, N_("First vector"));
    gwy_results_add_value(results, "a1x", N_("X-component"),
                          "power-x", 1, "symbol", "a<sub>1x</sub>", NULL);
    gwy_results_add_value(results, "a1y", N_("Y-component"),
                          "power-x", 1, "symbol", "a<sub>1y</sub>", NULL);
    gwy_results_add_value(results, "a1", N_("Length"),
                          "power-x", 1, "symbol", "a<sub>1</sub>", NULL);
    gwy_results_add_value(results, "phi1", N_("Direction"),
                          "is-angle", TRUE, "symbol", "φ<sub>1</sub>", NULL);
    gwy_results_add_separator(results);

    gwy_results_add_header(results, N_("Second vector"));
    gwy_results_add_value(results, "a2x", N_("X-component"),
                          "power-x", 1, "symbol", "a<sub>2x</sub>", NULL);
    gwy_results_add_value(results, "a2y", N_("Y-component"),
                          "power-x", 1, "symbol", "a<sub>2y</sub>", NULL);
    gwy_results_add_value(results, "a2", N_("Length"),
                          "power-x", 1, "symbol", "a<sub>2</sub>", NULL);
    gwy_results_add_value(results, "phi2", N_("Direction"),
                          "is-angle", TRUE, "symbol", "φ<sub>2</sub>", NULL);
    gwy_results_add_separator(results);

    gwy_results_add_header(results, N_("Primitive cell"));
    gwy_results_add_value(results, "A", N_("Area"),
                          "power-x", 1, "power-y", 1, "symbol", "A", NULL);
    gwy_results_add_value(results, "phi", N_("Angle"),
                          "is-angle", TRUE, "symbol", "φ", NULL);

    gwy_results_bind_formats(results,
                             "a1x", "a1y", "a1", "a2x", "a2y", "a2",
                             NULL);

    gwy_results_set_unit(results, "x", gwy_data_field_get_si_unit_xy(dfield));
    gwy_results_set_unit(results, "y", gwy_data_field_get_si_unit_xy(dfield));
    gwy_results_set_unit(results, "z", gwy_data_field_get_si_unit_z(dfield));
    gwy_results_fill_filename(results, "file", data);
    gwy_results_fill_channel(results, "image", data, id);

    controls.dialog = gtk_dialog_new_with_buttons(_("Measure Lattice"),
                                                  NULL, 0, NULL);
    dialog = GTK_DIALOG(controls.dialog);
    gtk_dialog_add_button(dialog, _("_Reset"), RESPONSE_RESET);
    gtk_dialog_add_button(dialog, gwy_sgettext("verb|_Estimate"),
                          RESPONSE_ESTIMATE);
    gtk_dialog_add_button(dialog, _("_Refine"), RESPONSE_REFINE);
    gtk_dialog_add_button(dialog, GTK_STOCK_OK, GTK_RESPONSE_OK);
    gtk_dialog_set_default_response(dialog, GTK_RESPONSE_OK);
    gwy_help_add_to_proc_dialog(GTK_DIALOG(dialog), GWY_HELP_DEFAULT);

    hbox = gtk_hbox_new(FALSE, 2);

    gtk_box_pack_start(GTK_BOX(GTK_DIALOG(dialog)->vbox), hbox,
                       FALSE, FALSE, 4);

    controls.mydata = gwy_container_new();
    gwy_container_set_object_by_name(controls.mydata, "/0/data", dfield);
    gwy_app_sync_data_items(data, controls.mydata, id, 0, FALSE,
                            GWY_DATA_ITEM_RANGE_TYPE,
                            GWY_DATA_ITEM_RANGE,
                            GWY_DATA_ITEM_GRADIENT,
                            GWY_DATA_ITEM_REAL_SQUARE,
                            0);

    calculate_acf_full(&controls, dfield);
    calculate_psdf_full(&controls, dfield);
    calculate_zoomed_field(&controls);
    gwy_app_sync_data_items(data, controls.mydata, id, 1, FALSE,
                            GWY_DATA_ITEM_GRADIENT,
                            GWY_DATA_ITEM_REAL_SQUARE,
                            0);

    alignment = gtk_alignment_new(0.0, 0.0, 0.0, 0.0);
    gtk_box_pack_start(GTK_BOX(hbox), alignment, FALSE, FALSE, 4);

    controls.view = gwy_data_view_new(controls.mydata);
    g_object_unref(controls.mydata);
    layer = gwy_layer_basic_new();
    g_object_set(layer,
                 "gradient-key", "/0/base/palette",
                 "data-key", "/1/data",
                 "range-type-key", "/1/base/range-type",
                 "min-max-key", "/1/base",
                 NULL);
    gwy_data_view_set_data_prefix(GWY_DATA_VIEW(controls.view), "/0/data");
    gwy_data_view_set_base_layer(GWY_DATA_VIEW(controls.view), layer);
    gwy_set_data_preview_size(GWY_DATA_VIEW(controls.view), PREVIEW_SIZE);
    /* We know we are starting with Lattice. */
    vlayer = g_object_new(args->lattice_layer,
                          "selection-key", "/0/select/lattice",
                          NULL);
    gwy_data_view_set_top_layer(GWY_DATA_VIEW(controls.view), vlayer);
    controls.selection = gwy_vector_layer_ensure_selection(vlayer);
    gwy_selection_set_max_objects(controls.selection, 1);
    controls.selection_id
        = g_signal_connect_swapped(controls.selection, "changed",
                                   G_CALLBACK(selection_changed), &controls);

    gtk_container_add(GTK_CONTAINER(alignment), controls.view);

    table = GTK_TABLE(gtk_table_new(11, 4, FALSE));
    gtk_table_set_row_spacings(table, 2);
    gtk_table_set_col_spacings(table, 6);
    gtk_container_set_border_width(GTK_CONTAINER(table), 4);
    gtk_box_pack_end(GTK_BOX(hbox), GTK_WIDGET(table), TRUE, TRUE, 0);
    row = 0;

    label = gtk_label_new(_("Display:"));
    gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);
    gtk_table_attach(table, label, 0, 4, row, row+1, GTK_FILL, 0, 0, 0);
    row++;

    controls.image_mode
        = gwy_radio_buttons_createl(G_CALLBACK(image_mode_changed), &controls,
                                    args->image_mode,
                                    _("_Data"), IMAGE_DATA,
                                    _("_ACF"), IMAGE_ACF,
                                    _("_PSDF"), IMAGE_PSDF,
                                    NULL);
    row = gwy_radio_buttons_attach_to_table(controls.image_mode,
                                            table, 4, row);

    hbox2 = gtk_hbox_new(FALSE, 8);
    gtk_table_attach(GTK_TABLE(table), hbox2, 0, 4, row, row+1,
                     GTK_FILL, 0, 0, 0);
    label = controls.zoom_label = gtk_label_new(_("Zoom:"));
    gtk_box_pack_start(GTK_BOX(hbox2), label, FALSE, FALSE, 0);

    controls.zoom
        = gwy_radio_buttons_createl(G_CALLBACK(zoom_changed), &controls,
                                    args->zoom_acf,
                                    "1×", ZOOM_1,
                                    "4×", ZOOM_4,
                                    "16×", ZOOM_16,
                                    NULL);
    for (l = controls.zoom; l; l = g_slist_next(l)) {
        GtkWidget *widget = GTK_WIDGET(l->data);
        gtk_box_pack_start(GTK_BOX(hbox2), widget, FALSE, FALSE, 0);
    }
    row++;

    controls.fix_hacf
        = gtk_check_button_new_with_mnemonic(_("Interpolate _horizontal ACF"));
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(controls.fix_hacf),
                                 args->fix_hacf);
    gtk_table_attach(table, controls.fix_hacf,
                     0, 2, row, row+1, GTK_FILL, 0, 0, 0);
    g_signal_connect_swapped(controls.fix_hacf, "toggled",
                             G_CALLBACK(fix_hacf_changed), &controls);
    row++;

    gtk_table_set_row_spacing(table, row-1, 8);
    label = gtk_label_new(_("Show lattice as:"));
    gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);
    gtk_table_attach(table, label, 0, 4, row, row+1, GTK_FILL, 0, 0, 0);
    row++;

    controls.selection_mode
        = gwy_radio_buttons_createl(G_CALLBACK(selection_mode_changed),
                                    &controls,
                                    args->selection_mode,
                                    _("_Lattice"), SELECTION_LATTICE,
                                    _("_Vectors"), SELECTION_POINT,
                                    NULL);
    row = gwy_radio_buttons_attach_to_table(controls.selection_mode,
                                            table, 4, row);

    gtk_table_set_row_spacing(table, row-1, 8);
    label = gwy_label_new_header(_("Lattice Vectors"));
    gtk_table_attach(table, label, 0, 4, row, row+1, GTK_FILL, 0, 0, 0);
    row++;

    controls.vf
        = gwy_data_field_get_value_format_xy(dfield,
                                             GWY_SI_UNIT_FORMAT_MARKUP, NULL);
    controls.vf->precision += 2;

    controls.vfphi = gwy_si_unit_value_format_new(G_PI/180.0, 2, _("deg"));

    lattable = make_lattice_table(&controls);
    gtk_table_attach(table, lattable, 0, 4, row, row+1,
                     GTK_EXPAND | GTK_FILL, 0, 0, 0);
    row++;

    gtk_table_set_row_spacing(table, row-1, 8);
    controls.rexport = gwy_results_export_new(args->report_style);
    rexport = GWY_RESULTS_EXPORT(controls.rexport);
    gwy_results_export_set_title(rexport, _("Save Parameters"));
    gwy_results_export_set_results(rexport, results);
    gtk_table_attach(table, controls.rexport, 0, 4, row, row+1,
                     GTK_FILL, 0, 0, 0);
    g_signal_connect_swapped(rexport, "format-changed",
                             G_CALLBACK(report_style_changed), &controls);

    /* Restore lattice from data if any is present. */
    g_snprintf(selkey, sizeof(selkey), "/%d/select/lattice", id);
    if (gwy_container_gis_object_by_name(data, selkey, &selection)) {
        if (gwy_selection_get_object(selection, 0, controls.xy))
            set_selection(&controls, controls.xy);
    }
    else
        do_estimate(&controls);

    gwy_radio_buttons_set_current(controls.selection_mode, selection_mode);
    gwy_radio_buttons_set_current(controls.image_mode, image_mode);

    gtk_widget_show_all(controls.dialog);
    do {
        response = gtk_dialog_run(dialog);
        switch (response) {
            case GTK_RESPONSE_CANCEL:
            case GTK_RESPONSE_DELETE_EVENT:
            gtk_widget_destroy(controls.dialog);
            case GTK_RESPONSE_NONE:
            return;
            break;

            case GTK_RESPONSE_OK:
            break;

            case RESPONSE_RESET:
            init_selection(&controls);
            break;

            case RESPONSE_ESTIMATE:
            do_estimate(&controls);
            break;

            case RESPONSE_REFINE:
            refine(&controls);
            break;

            default:
            g_assert_not_reached();
            break;
        }
    } while (response != GTK_RESPONSE_OK);

    if (get_selection(&controls, controls.xy)) {
        selection = g_object_new(g_type_from_name("GwySelectionLattice"),
                                 "max-objects", 1,
                                 NULL);
        gwy_selection_set_data(selection, 1, controls.xy);
        gwy_container_set_object_by_name(data, selkey, selection);
        g_object_unref(selection);
    }

    g_signal_handler_disconnect(controls.selection, controls.selection_id);
    if (controls.vlayer)
        GWY_OBJECT_UNREF(controls.vlayer);
    gtk_widget_destroy(controls.dialog);
}

static GtkWidget*
make_lattice_table(LatMeasControls *controls)
{
    GtkWidget *table, *label;
    GString *str = g_string_new(NULL);

    table = gtk_table_new(4, 5, FALSE);
    gtk_table_set_row_spacings(GTK_TABLE(table), 2);
    gtk_table_set_col_spacings(GTK_TABLE(table), 6);

    /* header row */
    g_string_assign(str, "x");
    if (strlen(controls->vf->units))
        g_string_append_printf(str, " [%s]", controls->vf->units);
    label = gtk_label_new(NULL);
    gtk_label_set_markup(GTK_LABEL(label), str->str);
    gtk_table_attach(GTK_TABLE(table), label, 1, 2, 0, 1, GTK_FILL, 0, 0, 0);

    g_string_assign(str, "y");
    if (strlen(controls->vf->units))
        g_string_append_printf(str, " [%s]", controls->vf->units);
    label = gtk_label_new(NULL);
    gtk_label_set_markup(GTK_LABEL(label), str->str);
    gtk_table_attach(GTK_TABLE(table), label, 2, 3, 0, 1,
                     GTK_EXPAND | GTK_FILL, 0, 0, 0);

    g_string_assign(str, _("length"));
    if (strlen(controls->vf->units))
        g_string_append_printf(str, " [%s]", controls->vf->units);
    label = gtk_label_new(NULL);
    gtk_label_set_markup(GTK_LABEL(label), str->str);
    gtk_table_attach(GTK_TABLE(table), label, 3, 4, 0, 1,
                     GTK_EXPAND | GTK_FILL, 0, 0, 0);

    g_string_assign(str, _("angle"));
    if (strlen(controls->vfphi->units))
        g_string_append_printf(str, " [%s]", controls->vfphi->units);
    label = gtk_label_new(NULL);
    gtk_label_set_markup(GTK_LABEL(label), str->str);
    gtk_table_attach(GTK_TABLE(table), label, 4, 5, 0, 1,
                     GTK_EXPAND | GTK_FILL, 0, 0, 0);

    /* a1 */
    label = gtk_label_new(NULL);
    gtk_label_set_markup(GTK_LABEL(label), "a<sub>1</sub>:");
    gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);
    gtk_table_attach(GTK_TABLE(table), label, 0, 1, 1, 2,
                     GTK_EXPAND | GTK_FILL, 0, 0, 0);

    controls->a1_x = label = gtk_label_new(NULL);
    gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);
    gtk_label_set_width_chars(GTK_LABEL(label), 8);
    gtk_table_attach(GTK_TABLE(table), label, 1, 2, 1, 2,
                     GTK_EXPAND | GTK_FILL, 0, 0, 0);

    controls->a1_y = label = gtk_label_new(NULL);
    gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);
    gtk_label_set_width_chars(GTK_LABEL(label), 8);
    gtk_table_attach(GTK_TABLE(table), label, 2, 3, 1, 2,
                     GTK_EXPAND | GTK_FILL, 0, 0, 0);

    controls->a1 = label = gtk_label_new(NULL);
    gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);
    gtk_label_set_width_chars(GTK_LABEL(label), 8);
    gtk_table_attach(GTK_TABLE(table), label, 3, 4, 1, 2,
                     GTK_EXPAND | GTK_FILL, 0, 0, 0);

    controls->phi1 = label = gtk_label_new(NULL);
    gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);
    gtk_label_set_width_chars(GTK_LABEL(label), 8);
    gtk_table_attach(GTK_TABLE(table), label, 4, 5, 1, 2,
                     GTK_EXPAND | GTK_FILL, 0, 0, 0);

    /* a2 */
    label = gtk_label_new(NULL);
    gtk_label_set_markup(GTK_LABEL(label), "a<sub>2</sub>:");
    gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);
    gtk_table_attach(GTK_TABLE(table), label, 0, 1, 2, 3,
                     GTK_EXPAND | GTK_FILL, 0, 0, 0);

    controls->a2_x = label = gtk_label_new(NULL);
    gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);
    gtk_label_set_width_chars(GTK_LABEL(label), 8);
    gtk_table_attach(GTK_TABLE(table), label, 1, 2, 2, 3,
                     GTK_EXPAND | GTK_FILL, 0, 0, 0);

    controls->a2_y = label = gtk_label_new(NULL);
    gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);
    gtk_label_set_width_chars(GTK_LABEL(label), 8);
    gtk_table_attach(GTK_TABLE(table), label, 2, 3, 2, 3,
                     GTK_EXPAND | GTK_FILL, 0, 0, 0);

    controls->a2 = label = gtk_label_new(NULL);
    gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);
    gtk_label_set_width_chars(GTK_LABEL(label), 8);
    gtk_table_attach(GTK_TABLE(table), label, 3, 4, 2, 3,
                     GTK_EXPAND | GTK_FILL, 0, 0, 0);

    controls->phi2 = label = gtk_label_new(NULL);
    gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);
    gtk_label_set_width_chars(GTK_LABEL(label), 8);
    gtk_table_attach(GTK_TABLE(table), label, 4, 5, 2, 3,
                     GTK_EXPAND | GTK_FILL, 0, 0, 0);

    /* phi */
    label = gtk_label_new(NULL);
    gtk_label_set_markup(GTK_LABEL(label), "ϕ:");
    gtk_misc_set_alignment(GTK_MISC(label), 1.0, 0.5);
    gtk_label_set_width_chars(GTK_LABEL(label), 8);
    gtk_table_attach(GTK_TABLE(table), label, 3, 4, 3, 4,
                     GTK_EXPAND | GTK_FILL, 0, 0, 0);

    controls->phi = label = gtk_label_new(NULL);
    gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);
    gtk_table_attach(GTK_TABLE(table), label, 4, 5, 3, 4,
GTK_EXPAND | GTK_FILL, 0, 0, 0);

    g_string_free(str, TRUE);

    return table;
}

static GwyDataField*
get_full_dfield_for_mode(LatMeasControls *controls)
{
    LatMeasArgs *args = controls->args;
    GwyDataField *dfield;
    GwyDataLine *hacf;
    const gchar *key;

    if (args->image_mode == IMAGE_PSDF)
        key = "/3/data/full";
    else
        key = "/2/data/full";

    dfield = GWY_DATA_FIELD(gwy_container_get_object_by_name(controls->mydata,
                                                             key));

    if (args->image_mode == IMAGE_ACF) {
        if (args->fix_hacf)
            key = "/2/hacf-fixed";
        else
            key = "/2/hacf";

        hacf = GWY_DATA_LINE(gwy_container_get_object_by_name(controls->mydata,
                                                              key));
        gwy_data_field_set_row(dfield, hacf, gwy_data_field_get_yres(dfield)/2);
    }

    return dfield;
}

static void
do_estimate(LatMeasControls *controls)
{
    GwyDataField *dfield;
    gboolean ok;

    dfield = get_full_dfield_for_mode(controls);
    gwy_clear(controls->xy, 4);
    if (controls->args->image_mode == IMAGE_PSDF)
        ok = gwy_data_field_measure_lattice_psdf(dfield, controls->xy);
    else
        ok = gwy_data_field_measure_lattice_acf(dfield, controls->xy);

    if (ok)
        set_selection(controls, controls->xy);
    else
        init_selection(controls);
}

static void
init_selection(LatMeasControls *controls)
{
    GwyDataField *dfield;

    dfield = gwy_container_get_object_by_name(controls->mydata, "/0/data");
    controls->xy[0] = dfield->xreal/20;
    controls->xy[1] = 0.0;
    controls->xy[2] = 0.0;
    controls->xy[3] = -dfield->yreal/20;
    set_selection(controls, controls->xy);
}

static void
set_selection(LatMeasControls *controls, const gdouble *xy)
{
    LatMeasArgs *args = controls->args;
    GwySelection *selection = controls->selection;
    GType stype = G_OBJECT_TYPE(selection);
    GwyDataField *dfield;
    gdouble xoff, yoff;
    gdouble tmpxy[4];

    gwy_assign(tmpxy, xy, 4);
    if (args->image_mode == IMAGE_PSDF)
        transform_selection(tmpxy);

    if (g_type_is_a(stype, args->lattice_selection)) {
        gwy_selection_set_data(selection, 1, tmpxy);
        return;
    }

    /* This is much more convoluted because point selections have origin of
     * real coordinates in the top left corner. */
    g_return_if_fail(g_type_is_a(stype, args->point_selection));
    if (args->image_mode == IMAGE_DATA)
        dfield = gwy_container_get_object_by_name(controls->mydata, "/0/data");
    else
        dfield = gwy_container_get_object_by_name(controls->mydata, "/1/data");

    xoff = 0.5*dfield->xreal;
    yoff = 0.5*dfield->yreal;
    tmpxy[0] += xoff;
    tmpxy[1] += yoff;
    tmpxy[2] += xoff;
    tmpxy[3] += yoff;
    gwy_selection_set_data(selection, 2, tmpxy);
}

static gboolean
get_selection(LatMeasControls *controls, gdouble *xy)
{
    GwySelection *selection = controls->selection;
    LatMeasArgs *args = controls->args;
    GType stype = G_OBJECT_TYPE(selection);
    GwyDataField *dfield;
    gdouble xoff, yoff;

    if (!gwy_selection_is_full(selection))
        return FALSE;

    gwy_selection_get_data(selection, xy);

    if (g_type_is_a(stype, args->lattice_selection)) {
        if (args->image_mode == IMAGE_PSDF)
            transform_selection(xy);
        return TRUE;
    }

    /* This is much more convoluted because point selections have origin of
     * real coordinates in the top left corner. */
    g_return_val_if_fail(g_type_is_a(stype, args->point_selection), FALSE);
    if (args->image_mode == IMAGE_DATA)
        dfield = gwy_container_get_object_by_name(controls->mydata, "/0/data");
    else
        dfield = gwy_container_get_object_by_name(controls->mydata, "/1/data");

    xoff = 0.5*dfield->xreal;
    yoff = 0.5*dfield->yreal;
    xy[0] -= xoff;
    xy[1] -= yoff;
    xy[2] -= xoff;
    xy[3] -= yoff;
    if (args->image_mode == IMAGE_PSDF)
        transform_selection(xy);

    return TRUE;
}

static void
image_mode_changed(G_GNUC_UNUSED GtkToggleButton *button,
                   LatMeasControls *controls)
{
    LatMeasArgs *args = controls->args;
    GwyDataView *dataview;
    GwyPixmapLayer *layer;
    ImageMode mode;
    gboolean zoom_sens;
    gdouble xy[4];   /* To survive the nested zoom and whatever changes. */
    GSList *l;

    mode = gwy_radio_buttons_get_current(controls->image_mode);
    gwy_debug("current: %u, new: %u", args->image_mode, mode);
    if (mode == args->image_mode)
        return;

    get_selection(controls, xy);
    args->image_mode = mode;
    dataview = GWY_DATA_VIEW(controls->view);
    layer = gwy_data_view_get_base_layer(dataview);

    if (mode == IMAGE_ACF)
        gwy_radio_buttons_set_current(controls->zoom, args->zoom_acf);
    else if (mode == IMAGE_PSDF)
        gwy_radio_buttons_set_current(controls->zoom, args->zoom_psdf);

    calculate_zoomed_field(controls);

    if (args->image_mode == IMAGE_DATA) {
        g_object_set(layer,
                     "data-key", "/0/data",
                     "range-type-key", "/0/base/range-type",
                     "min-max-key", "/0/base",
                     NULL);
        zoom_sens = FALSE;
        if (args->selection_mode == SELECTION_POINT) {
            controls->vlayer = gwy_data_view_get_top_layer(dataview);
            g_object_ref(controls->vlayer);
            gwy_data_view_set_top_layer(dataview, NULL);
        }
    }
    else {
        /* There are no range-type and min-max keys, which is the point,
         * because we want full-colour-scale, whatever the image has set. */
        g_object_set(layer,
                     "data-key", "/1/data",
                     "range-type-key", "/1/base/range-type",
                     "min-max-key", "/1/base",
                     NULL);
        if (mode == IMAGE_ACF)
            gwy_container_set_enum_by_name(controls->mydata,
                                           "/1/base/range-type",
                                           GWY_LAYER_BASIC_RANGE_AUTO);
        else
            gwy_container_set_enum_by_name(controls->mydata,
                                           "/1/base/range-type",
                                           GWY_LAYER_BASIC_RANGE_ADAPT);
        zoom_sens = TRUE;
        if (controls->vlayer) {
            gwy_data_view_set_top_layer(dataview, controls->vlayer);
            GWY_OBJECT_UNREF(controls->vlayer);
        }
    }

    gwy_set_data_preview_size(dataview, PREVIEW_SIZE);

    gtk_widget_set_sensitive(controls->zoom_label, zoom_sens);
    for (l = controls->zoom; l; l = g_slist_next(l))
        gtk_widget_set_sensitive(GTK_WIDGET(l->data), zoom_sens);

    set_selection(controls, xy);
}

static void
selection_mode_changed(G_GNUC_UNUSED GtkToggleButton *button,
                       LatMeasControls *controls)
{
    LatMeasArgs *args = controls->args;
    GwyDataView *dataview;
    GwyVectorLayer *vlayer;
    SelectionMode mode;
    guint maxobjects;

    mode = gwy_radio_buttons_get_current(controls->selection_mode);
    gwy_debug("current: %u, new: %u", args->selection_mode, mode);
    if (mode == args->selection_mode)
        return;

    get_selection(controls, controls->xy);
    g_signal_handler_disconnect(controls->selection, controls->selection_id);
    controls->selection_id = 0;
    args->selection_mode = mode;

    dataview = GWY_DATA_VIEW(controls->view);
    if (controls->vlayer) {
        gwy_data_view_set_top_layer(dataview, controls->vlayer);
        GWY_OBJECT_UNREF(controls->vlayer);
    }

    if (mode == SELECTION_LATTICE) {
        vlayer = g_object_new(args->lattice_layer,
                              "selection-key", "/0/select/lattice",
                              NULL);
        maxobjects = 1;
    }
    else {
        vlayer = g_object_new(args->point_layer,
                              "selection-key", "/0/select/point",
                              "draw-as-vector", TRUE,
                              NULL);
        maxobjects = 2;
    }

    gwy_data_view_set_top_layer(GWY_DATA_VIEW(controls->view), vlayer);
    controls->selection = gwy_vector_layer_ensure_selection(vlayer);
    gwy_selection_set_max_objects(controls->selection, maxobjects);
    set_selection(controls, controls->xy);
    if (mode == SELECTION_POINT && args->image_mode == IMAGE_DATA) {
        controls->vlayer = g_object_ref(vlayer);
        gwy_data_view_set_top_layer(dataview, NULL);
    }
    controls->selection_id
        = g_signal_connect_swapped(controls->selection, "changed",
                                   G_CALLBACK(selection_changed), controls);
}

static void
zoom_changed(GtkRadioButton *button,
             LatMeasControls *controls)
{
    LatMeasArgs *args = controls->args;
    ZoomType zoom = gwy_radio_buttons_get_current(controls->zoom);

    get_selection(controls, controls->xy);
    if (args->image_mode == IMAGE_ACF) {
        if (button && zoom == args->zoom_acf)
            return;
        args->zoom_acf = zoom;
    }
    else if (args->image_mode == IMAGE_PSDF) {
        if (button && zoom == args->zoom_psdf)
            return;
        args->zoom_psdf = zoom;
    }
    else {
        g_assert(args->image_mode == IMAGE_DATA);
        return;
    }

    calculate_zoomed_field(controls);
    gwy_set_data_preview_size(GWY_DATA_VIEW(controls->view), PREVIEW_SIZE);
    set_selection(controls, controls->xy);
}

static void
fix_hacf_changed(LatMeasControls *controls,
                 GtkToggleButton *toggle)
{
    LatMeasArgs *args = controls->args;

    args->fix_hacf = gtk_toggle_button_get_active(toggle);
    if (args->image_mode == IMAGE_ACF)
        calculate_zoomed_field(controls);
}

static void
report_style_changed(LatMeasControls *controls, GwyResultsExport *rexport)
{
    controls->args->report_style = gwy_results_export_get_format(rexport);
}

static void
calculate_acf_full(LatMeasControls *controls,
                   GwyDataField *dfield)
{
    GwyDataField *acf, *mid, *mask;
    GwyDataLine *hacf;
    guint acfwidth, acfheight;

    dfield = gwy_data_field_duplicate(dfield);
    gwy_data_field_add(dfield, -gwy_data_field_get_avg(dfield));
    acf = gwy_data_field_new_alike(dfield, FALSE);
    acfwidth = dfield->xres/2;
    acfheight = dfield->yres/2;
    gwy_data_field_area_2dacf(dfield, acf, 0, 0, dfield->xres, dfield->yres,
                              acfwidth, acfheight);
    g_object_unref(dfield);
    gwy_container_set_object_by_name(controls->mydata, "/2/data/full", acf);
    g_object_unref(acf);

    /* Remember the middle row as we may replace it. */
    acfheight = gwy_data_field_get_yres(acf);
    acfwidth = gwy_data_field_get_xres(acf);
    hacf = gwy_data_line_new(acfwidth, 1.0, FALSE);
    gwy_data_field_get_row(acf, hacf, acfheight/2);
    gwy_container_set_object_by_name(controls->mydata, "/2/hacf", hacf);
    g_object_unref(hacf);

    /* Remember interpolated middle row. */
    mid = gwy_data_field_area_extract(acf, 0, acfheight/2-1, acfwidth, 3);
    mask = gwy_data_field_new(acfwidth, 3, acfwidth, 3, TRUE);
    gwy_data_field_area_fill(mask, 0, 1, acfwidth, 1, 1.0);
    gwy_data_field_set_val(mask, acfwidth/2, 1, 0.0);
    gwy_data_field_laplace_solve(mid, mask, -1, 1.0);
    hacf = gwy_data_line_new(acfwidth, 1.0, FALSE);
    gwy_data_field_get_row(mid, hacf, 1);
    gwy_container_set_object_by_name(controls->mydata, "/2/hacf-fixed", hacf);
    g_object_unref(hacf);
    g_object_unref(mask);
    g_object_unref(mid);
}

static gint
reduce_size(gint n)
{
    gint n0 = (n & 1) ? n : n-1;
    gint nmin = MAX(n0, 65);
    gint nred = 3*n/4;
    gint nredodd = (nred & 1) ? nred : nred-1;
    return MIN(nmin, nredodd);
}

static void
calculate_psdf_full(LatMeasControls *controls,
                    GwyDataField *dfield)
{
    GwyDataField *reout, *imout, *psdf;
    gint i, j, psdfwidth, psdfheight;

    reout = gwy_data_field_new_alike(dfield, FALSE);
    imout = gwy_data_field_new_alike(dfield, FALSE);
    gwy_data_field_2dfft(dfield, NULL, reout, imout,
                         GWY_WINDOWING_HANN, GWY_TRANSFORM_DIRECTION_FORWARD,
                         GWY_INTERPOLATION_LINEAR,  /* ignored */
                         FALSE, 1);
    gwy_data_field_fft_postprocess(reout, TRUE);
    gwy_data_field_fft_postprocess(imout, TRUE);
    gwy_data_field_hypot_of_fields(reout, reout, imout);
    /* XXX: We do not fix the value units because they should not leak from the
     * in-module preview. */
    g_object_unref(imout);

    psdfwidth = reduce_size(dfield->xres);
    psdfheight = reduce_size(dfield->yres);
    i = dfield->yres - psdfheight - (dfield->yres - psdfheight)/2;
    j = dfield->xres - psdfwidth - (dfield->xres - psdfwidth)/2;
    psdf = gwy_data_field_area_extract(reout, j, i, psdfwidth, psdfheight);
    g_object_unref(reout);
    gwy_data_field_set_xoffset(psdf, -0.5*psdf->xreal);
    gwy_data_field_set_yoffset(psdf, -0.5*psdf->yreal);
    gwy_container_set_object_by_name(controls->mydata, "/3/data/full", psdf);
    g_object_unref(psdf);
}

static void
calculate_zoomed_field(LatMeasControls *controls)
{
    LatMeasArgs *args = controls->args;
    ZoomType zoom;
    GwyDataField *zoomed;
    guint xres, yres, width, height;

    if (args->image_mode == IMAGE_ACF)
        zoom = args->zoom_acf;
    else if (args->image_mode == IMAGE_PSDF)
        zoom = args->zoom_psdf;
    else
        return;

    zoomed = get_full_dfield_for_mode(controls);
    xres = gwy_data_field_get_xres(zoomed);
    yres = gwy_data_field_get_yres(zoomed);

    if (zoom == ZOOM_1) {
        g_object_ref(zoomed);
    }
    else {
        width = (xres/zoom) | 1;
        height = (yres/zoom) | 1;

        if (width < 17)
            width = MAX(width, MIN(17, xres));

        if (height < 17)
            height = MAX(height, MIN(17, yres));

        zoomed = gwy_data_field_area_extract(zoomed,
                                             (xres - width)/2,
                                             (yres - height)/2,
                                             width, height);
        gwy_data_field_set_xoffset(zoomed, -0.5*zoomed->xreal);
        gwy_data_field_set_yoffset(zoomed, -0.5*zoomed->yreal);
    }
    gwy_container_set_object_by_name(controls->mydata, "/1/data", zoomed);
    g_object_unref(zoomed);
}

static void
refine(LatMeasControls *controls)
{
    GwyDataField *dfield;
    gdouble xy[4];
    gboolean ok;

    if (!get_selection(controls, xy))
        return;

    dfield = get_full_dfield_for_mode(controls);
    if (controls->args->image_mode == IMAGE_PSDF)
        ok = gwy_data_field_measure_lattice_psdf(dfield, xy);
    else
        ok = gwy_data_field_measure_lattice_acf(dfield, xy);

    if (ok)
        set_selection(controls, xy);
}

static void
update_value_label(GtkWidget *label, gdouble value, GwySIValueFormat *vf,
                   GString *str)
{
    g_string_printf(str, "%.*f", vf->precision, value/vf->magnitude);
    gtk_label_set_text(GTK_LABEL(label), str->str);
}

static void
selection_changed(LatMeasControls *controls)
{
    gdouble a1, a2, phi1, phi2, phi, A;
    GString *str;
    gdouble xy[4];

    if (!get_selection(controls, xy)) {
        gwy_results_set_na(controls->results,
                           "a1x", "a1y", "a1", "phi1",
                           "a2x", "a2y", "a2", "phi2",
                           "phi", "A",
                           NULL);
        return;
    }

    a1 = hypot(xy[0], xy[1]);
    a2 = hypot(xy[2], xy[3]);
    phi1 = atan2(-xy[1], xy[0]);
    phi2 = atan2(-xy[3], xy[2]);
    phi = gwy_canonicalize_angle(phi2 - phi1, TRUE, TRUE);
    A = fabs(matrix_det(xy));

    gwy_results_fill_values(controls->results,
                            "a1x", xy[0], "a1y", -xy[1],
                            "a1", a1, "phi1", phi1,
                            "a2x", xy[2], "a2y", -xy[3],
                            "a2", a1, "phi2", phi1,
                            "phi", phi, "A", A,
                            NULL);

    str = g_string_new(NULL);
    update_value_label(controls->a1_x, xy[0], controls->vf, str);
    update_value_label(controls->a1_y, -xy[1], controls->vf, str);
    update_value_label(controls->a1, a1, controls->vf, str);
    update_value_label(controls->phi1, phi1, controls->vfphi, str);
    update_value_label(controls->a2_x, xy[2], controls->vf, str);
    update_value_label(controls->a2_y, -xy[3], controls->vf, str);
    update_value_label(controls->a2, a2, controls->vf, str);
    update_value_label(controls->phi2, phi2, controls->vfphi, str);
    update_value_label(controls->phi, phi, controls->vfphi, str);

    g_string_free(str, TRUE);
}

static gboolean
transform_selection(gdouble *xy)
{
    gdouble D = matrix_det(xy);
    gdouble a = fabs(xy[0]*xy[3]) + fabs(xy[1]*xy[2]);

    if (fabs(D)/a < 1e-9)
        return FALSE;

    invert_matrix(xy, xy);
    /* Transpose. */
    GWY_SWAP(gdouble, xy[1], xy[2]);
    return TRUE;
}

/* Permit dest = src */
static void
invert_matrix(gdouble *dest,
              const gdouble *src)
{
    gdouble D = matrix_det(src);
    gdouble xy[4];

    gwy_debug("D %g", D);
    xy[0] = src[3]/D;
    xy[1] = -src[1]/D;
    xy[2] = -src[2]/D;
    xy[3] = src[0]/D;
    dest[0] = xy[0];
    dest[1] = xy[1];
    dest[2] = xy[2];
    dest[3] = xy[3];
}

static gdouble
matrix_det(const gdouble *m)
{
    return m[0]*m[3] - m[1]*m[2];
}

static const gchar fix_hacf_key[]       = "/module/measure_lattice/fix_hacf";
static const gchar image_mode_key[]     = "/module/measure_lattice/image_mode";
static const gchar report_style_key[]   = "/module/measure_lattice/report_style";
static const gchar selection_mode_key[] = "/module/measure_lattice/selection_mode";
static const gchar zoom_acf_key[]       = "/module/measure_lattice/zoom_acf";
static const gchar zoom_psdf_key[]      = "/module/measure_lattice/zoom_psdf";

static void
sanitize_args(LatMeasArgs *args)
{
    args->selection_mode = MIN(args->selection_mode, SELECTION_NMODES-1);
    args->image_mode = MIN(args->image_mode, IMAGE_NMODES-1);
    args->fix_hacf = !!args->fix_hacf;
    if (args->zoom_acf != ZOOM_1
        && args->zoom_acf != ZOOM_4
        && args->zoom_acf != ZOOM_16)
        args->zoom_acf = lat_meas_defaults.zoom_acf;
    if (args->zoom_psdf != ZOOM_1
        && args->zoom_psdf != ZOOM_4
        && args->zoom_psdf != ZOOM_16)
        args->zoom_psdf = lat_meas_defaults.zoom_psdf;
}

static void
load_args(GwyContainer *container, LatMeasArgs *args)
{
    *args = lat_meas_defaults;

    gwy_container_gis_enum_by_name(container, image_mode_key,
                                   &args->image_mode);
    gwy_container_gis_enum_by_name(container, selection_mode_key,
                                   &args->selection_mode);
    gwy_container_gis_enum_by_name(container, zoom_acf_key, &args->zoom_acf);
    gwy_container_gis_enum_by_name(container, zoom_psdf_key, &args->zoom_psdf);
    gwy_container_gis_boolean_by_name(container, fix_hacf_key, &args->fix_hacf);
    gwy_container_gis_enum_by_name(container, report_style_key,
                                   &args->report_style);
    sanitize_args(args);
}

static void
save_args(GwyContainer *container, LatMeasArgs *args)
{
    gwy_container_set_enum_by_name(container, image_mode_key,
                                   args->image_mode);
    gwy_container_set_enum_by_name(container, selection_mode_key,
                                   args->selection_mode);
    gwy_container_set_enum_by_name(container, zoom_acf_key, args->zoom_acf);
    gwy_container_set_enum_by_name(container, zoom_psdf_key, args->zoom_psdf);
    gwy_container_set_boolean_by_name(container, fix_hacf_key, args->fix_hacf);
    gwy_container_set_enum_by_name(container, report_style_key,
                                   args->report_style);
}

/* vim: set cin et ts=4 sw=4 cino=>1s,e0,n0,f0,{0,}0,^0,\:1s,=0,g1s,h0,t0,+1s,c3,(0,u0 : */
