/*
 *  $Id$
 *  Copyright (C) 2003-2013 David Necas (Yeti), Petr Klapetek.
 *  E-mail: yeti@gwyddion.net, klapetek@gwyddion.net.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA 02110-1301, USA.
 */

#include "config.h"
#include <stdlib.h>
#include <string.h>
#include <libgwyddion/gwymacros.h>
#include <libgwyddion/gwymath.h>
#include <gtk/gtk.h>
#include <libprocess/stats.h>
#include <libgwydgets/gwydgetutils.h>
#include <libgwydgets/gwycombobox.h>
#include <libgwydgets/gwystock.h>
#include <libgwymodule/gwymodule-process.h>
#include <app/gwymoduleutils.h>
#include <app/gwyapp.h>
#include "preview.h"

#define CALIBRATE_RUN_MODES (GWY_RUN_IMMEDIATE | GWY_RUN_INTERACTIVE)

/* for compatibility checks */
#define EPSILON 1e-6

typedef struct {
    gdouble xratio;
    gdouble yratio;
    gdouble zratio;
    gint xyexponent;
    gint zexponent;
    gboolean square;
    gboolean new_channel;
    gboolean match_size;
    gdouble xreal;
    gdouble yreal;
    gdouble zreal;
    gdouble x0;
    gdouble y0;
    gdouble zshift;
    gdouble xorig;
    gdouble yorig;
    gdouble zorig;
    gdouble x0orig;
    gdouble y0orig;
    gint xyorigexp;
    gint zorigexp;
    gint xres;
    gint yres;
    gchar *xyunit;
    gchar *xyunitorig;
    gchar *zunit;
    gchar *zunitorig;
    GwyAppDataId sizeid;
    GwyAppDataId targetid;
} CalibrateArgs;

typedef struct {
    CalibrateArgs *args;
    GtkWidget *dialog;
    GtkObject *xratio;
    GtkObject *yratio;
    GtkObject *zratio;
    GtkWidget *match_size;
    GtkWidget *size_chooser;
    GtkWidget *xyexponent;
    GtkWidget *zexponent;
    GtkWidget *xpower10;
    GtkWidget *ypower10;
    GtkWidget *zpower10;
    GtkWidget *square;
    GtkWidget *new_channel;
    GtkObject *xreal;
    GtkObject *yreal;
    GtkObject *zreal;
    GtkObject *x0;
    GtkObject *y0;
    GtkObject *zshift;
    gboolean in_update;
    GtkWidget *xyunits;
    GtkWidget *zunits;
    GtkWidget *ok;
} CalibrateControls;

static gboolean module_register        (void);
static void     calibrate              (GwyContainer *data,
                                        GwyRunType run);
static gboolean calibrate_dialog       (CalibrateArgs *args,
                                        GwyDataField *dfield);
static void     dialog_reset           (CalibrateControls *controls,
                                        CalibrateArgs *args);
static void     xratio_changed         (GtkAdjustment *adj,
                                        CalibrateControls *controls);
static void     yratio_changed         (GtkAdjustment *adj,
                                        CalibrateControls *controls);
static void     zratio_changed         (GtkAdjustment *adj,
                                        CalibrateControls *controls);
static void     xreal_changed          (GtkAdjustment *adj,
                                        CalibrateControls *controls);
static void     yreal_changed          (GtkAdjustment *adj,
                                        CalibrateControls *controls);
static void     x0_changed             (GtkAdjustment *adj,
                                        CalibrateControls *controls);
static void     y0_changed             (GtkAdjustment *adj,
                                        CalibrateControls *controls);
static void     zshift_changed         (GtkAdjustment *adj,
                                        CalibrateControls *controls);
static void     zreal_changed          (GtkAdjustment *adj,
                                        CalibrateControls *controls);
static void     square_changed         (GtkWidget *check,
                                        CalibrateControls *controls);
static void     new_channel_changed    (GtkWidget *check,
                                        CalibrateControls *controls);
static void     xyexponent_changed     (GtkWidget *combo,
                                        CalibrateControls *controls);
static void     zexponent_changed      (GtkWidget *combo,
                                        CalibrateControls *controls);
static void     calibrate_dialog_update(CalibrateControls *controls,
                                        CalibrateArgs *args);
static void     calibrate_sanitize_args(CalibrateArgs *args);
static void     calibrate_load_args    (GwyContainer *container,
                                        CalibrateArgs *args);
static void     calibrate_save_args    (GwyContainer *container,
                                        CalibrateArgs *args);
static void     units_change           (GtkWidget *button,
                                        CalibrateControls *controls);
static void     match_size_changed     (GtkToggleButton *toggle,
                                        CalibrateControls *controls);
static void     size_channel_changed   (GwyDataChooser *toggle,
                                        CalibrateControls *controls);
static gboolean mould_filter           (GwyContainer *data,
                                        gint id,
                                        gpointer user_data);
static void     set_combo_from_unit    (GtkWidget *combo,
                                        const gchar *str,
                                        gint basepower);

static const CalibrateArgs calibrate_defaults = {
    1.0,
    1.0,
    1.0,
    -6,
    -6,
    TRUE, TRUE, FALSE,
    0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0, 0, 0, 0,
    "m", "m", "m", "m",
    GWY_APP_DATA_ID_NONE,
    GWY_APP_DATA_ID_NONE,
};

static GwyAppDataId mould_id = GWY_APP_DATA_ID_NONE;

static GwyModuleInfo module_info = {
    GWY_MODULE_ABI_VERSION,
    &module_register,
    N_("Recalibrates scan lateral dimensions or value range."),
    "Petr Klapetek <klapetek@gwyddion.net>",
    "2.15",
    "David Nečas (Yeti) & Petr Klapetek",
    "2003",
};

GWY_MODULE_QUERY2(module_info, calibrate)

static gboolean
module_register(void)
{
    gwy_process_func_register("calibrate",
                              (GwyProcessFunc)&calibrate,
                              N_("/_Basic Operations/_Dimensions and Units..."),
                              GWY_STOCK_DATA_MEASURE,
                              CALIBRATE_RUN_MODES,
                              GWY_MENU_FLAG_DATA,
                              N_("Change physical dimensions, units "
                                 "or value scale"));

    return TRUE;
}

static void
calibrate(GwyContainer *data, GwyRunType run)
{
    GwyDataField *dfields[3];
    GQuark quarks[3];
    GQuark quark;
    gint oldid, newid, datano;
    CalibrateArgs args;
    gboolean ok;
    GwySIUnit *siunitxy, *siunitz;

    g_return_if_fail(run & CALIBRATE_RUN_MODES);
    gwy_app_data_browser_get_current(GWY_APP_DATA_FIELD, dfields + 0,
                                     GWY_APP_MASK_FIELD, dfields + 1,
                                     GWY_APP_SHOW_FIELD, dfields + 2,
                                     GWY_APP_DATA_FIELD_KEY, quarks + 0,
                                     GWY_APP_MASK_FIELD_KEY, quarks + 1,
                                     GWY_APP_SHOW_FIELD_KEY, quarks + 2,
                                     GWY_APP_DATA_FIELD_ID, &oldid,
                                     GWY_APP_CONTAINER_ID, &datano,
                                     0);
    g_return_if_fail(dfields[0]);

    calibrate_load_args(gwy_app_settings_get(), &args);
    args.targetid.datano = datano;
    args.targetid.id = oldid;
    args.xorig = gwy_data_field_get_xreal(dfields[0]);
    args.yorig = gwy_data_field_get_yreal(dfields[0]);
    args.zorig = gwy_data_field_get_max(dfields[0])
                 - gwy_data_field_get_min(dfields[0]);
    args.xres = gwy_data_field_get_xres(dfields[0]);
    args.yres = gwy_data_field_get_yres(dfields[0]);
    args.x0orig = gwy_data_field_get_xoffset(dfields[0]);
    args.y0orig = gwy_data_field_get_yoffset(dfields[0]);
    args.xyorigexp = 3*floor(log10(args.xorig*args.yorig)/6);
    args.zorigexp = 3*floor(log10(args.zorig)/3);
    args.xreal = args.xratio * args.xorig;
    args.yreal = args.yratio * args.yorig;
    args.zreal = args.zratio * args.zorig;
    args.xyexponent = 3*floor(log10(args.xreal*args.yreal)/6);
    args.zexponent = 3*floor(log10(args.zreal)/3);
    args.x0 = args.x0orig;
    args.y0 = args.y0orig;
    siunitxy = gwy_data_field_get_si_unit_xy(dfields[0]);
    args.xyunitorig = gwy_si_unit_get_string(siunitxy,
                                             GWY_SI_UNIT_FORMAT_VFMARKUP);
    args.xyunit = g_strdup(args.xyunitorig);
    siunitz = gwy_data_field_get_si_unit_z(dfields[0]);
    args.zunitorig = gwy_si_unit_get_string(siunitz,
                                            GWY_SI_UNIT_FORMAT_VFMARKUP);
    args.zunit = g_strdup(args.zunitorig);

    if (run == GWY_RUN_INTERACTIVE) {
        ok = calibrate_dialog(&args, dfields[0]);
        calibrate_save_args(gwy_app_settings_get(), &args);
        if (!ok) {
            g_free(args.xyunit);
            g_free(args.xyunitorig);
            g_free(args.zunit);
            g_free(args.zunitorig);
            return;
        }
    }

    if (!args.new_channel) {
        guint n = 1;
        if (dfields[1]) {
            n++;
            if (dfields[2])
                n++;
        }
        else if (dfields[2]) {
            quarks[1] = quarks[2];
            quarks[2] = 0;
            n++;
        }
        gwy_app_undo_qcheckpointv(data, n, quarks);
    }

    if (args.new_channel)
        dfields[0] = gwy_data_field_duplicate(dfields[0]);

    gwy_data_field_set_xreal(dfields[0], args.xreal);
    gwy_data_field_set_yreal(dfields[0], args.yreal);
    if (args.zratio != 1.0)
        gwy_data_field_multiply(dfields[0], args.zratio);
    if (args.zshift != 0.0)
        gwy_data_field_add(dfields[0], args.zshift);
    gwy_data_field_set_xoffset(dfields[0], args.x0);
    gwy_data_field_set_yoffset(dfields[0], args.y0);
    if (!gwy_strequal(args.xyunit, args.xyunitorig)) {
        siunitxy = gwy_si_unit_new(args.xyunit);
        gwy_data_field_set_si_unit_xy(dfields[0], siunitxy);
        g_object_unref(siunitxy);
    }

    if (args.zunit != args.zunitorig) {
        /* Ensure colour axis redraw when only the units change (but not the
         * range) by setting a new unit object. */
        siunitz = gwy_si_unit_new(args.zunit);
        gwy_data_field_set_si_unit_z(dfields[0], siunitz);
        g_object_unref(siunitz);
    }

    if (dfields[1]) {
        if (args.new_channel)
            dfields[1] = gwy_data_field_duplicate(dfields[1]);

        gwy_data_field_set_xreal(dfields[1], args.xreal);
        gwy_data_field_set_yreal(dfields[1], args.yreal);
        gwy_data_field_set_xoffset(dfields[1], args.x0);
        gwy_data_field_set_xoffset(dfields[1], args.y0);
        if (!gwy_strequal(args.xyunit, args.xyunitorig)) {
            siunitxy = gwy_si_unit_new(args.xyunit);
            gwy_data_field_set_si_unit_xy(dfields[1], siunitxy);
            g_object_unref(siunitxy);
        }
    }

    if (dfields[2]) {
        if (args.new_channel)
            dfields[2] = gwy_data_field_duplicate(dfields[2]);

        gwy_data_field_set_xreal(dfields[2], args.xreal);
        gwy_data_field_set_yreal(dfields[2], args.yreal);
        gwy_data_field_set_xoffset(dfields[2], args.x0);
        gwy_data_field_set_xoffset(dfields[2], args.y0);
        if (!gwy_strequal(args.xyunit, args.xyunitorig)) {
            siunitxy = gwy_si_unit_new(args.xyunit);
            gwy_data_field_set_si_unit_xy(dfields[2], siunitxy);
            g_object_unref(siunitxy);
        }
    }

    if (args.new_channel) {
        newid = gwy_app_data_browser_add_data_field(dfields[0], data, TRUE);
        g_object_unref(dfields[0]);
        gwy_app_sync_data_items(data, data, oldid, newid, FALSE,
                                GWY_DATA_ITEM_GRADIENT,
                                GWY_DATA_ITEM_RANGE,
                                GWY_DATA_ITEM_MASK_COLOR,
                                0);
        if (dfields[1]) {
            quark = gwy_app_get_mask_key_for_id(newid);
            gwy_container_set_object(data, quark, dfields[1]);
            g_object_unref(dfields[1]);
        }
        if (dfields[2]) {
            quark = gwy_app_get_show_key_for_id(newid);
            gwy_container_set_object(data, quark, dfields[2]);
            g_object_unref(dfields[2]);
        }

        gwy_app_set_data_field_title(data, newid, _("Recalibrated Data"));
        gwy_app_channel_log_add_proc(data, oldid, newid);
    }
    else {
        guint i;

        for (i = 0; i < 3; i++) {
            if (dfields[i])
                gwy_data_field_data_changed(dfields[i]);
        }

        if (args.xratio != 1.0 || args.yratio != 1.0)
            gwy_app_data_clear_selections(data, oldid);

        gwy_app_channel_log_add_proc(data, oldid, oldid);
    }

    g_free(args.xyunit);
    g_free(args.xyunitorig);
    g_free(args.zunit);
    g_free(args.zunitorig);
}

static gboolean
calibrate_dialog(CalibrateArgs *args,
                 GwyDataField *dfield)
{
    GtkWidget *dialog, *spin, *table, *label, *hbox, *hbox2;
    GwyDataChooser *chooser;
    GwySIUnit *unit;
    CalibrateControls controls;
    GwySIValueFormat *vf = NULL;
    gint row, response;
    gchar *buf;

    dialog = gtk_dialog_new_with_buttons(_("Dimensions and Units"), NULL, 0,
                                         _("_Reset"), RESPONSE_RESET,
                                         GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
                                         NULL);
    controls.dialog = dialog;

    controls.ok = gtk_dialog_add_button(GTK_DIALOG(dialog), GTK_STOCK_OK, GTK_RESPONSE_OK);
    gtk_dialog_set_default_response(GTK_DIALOG(dialog), GTK_RESPONSE_OK);
    gwy_help_add_to_proc_dialog(GTK_DIALOG(dialog), GWY_HELP_DEFAULT);

    controls.args = args;
    controls.in_update = TRUE;

    hbox = gtk_hbox_new(FALSE, 20);
    gtk_container_add(GTK_CONTAINER(GTK_DIALOG(dialog)->vbox), hbox);
    gtk_container_set_border_width(GTK_CONTAINER(hbox), 4);

    table = gtk_table_new(21, 4, FALSE);
    gtk_table_set_row_spacings(GTK_TABLE(table), 2);
    gtk_table_set_col_spacings(GTK_TABLE(table), 6);
    gtk_box_pack_start(GTK_BOX(hbox), table, TRUE, TRUE, 0);
    row = 0;

    /***** Current Dimensions *****/

    gtk_table_attach(GTK_TABLE(table),
                     gwy_label_new_header(_("Current Dimensions")),
                     0, 3, row, row+1, GTK_FILL, 0, 0, 0);
    row++;

    vf = gwy_data_field_get_value_format_xy(dfield,
                                            GWY_SI_UNIT_FORMAT_VFMARKUP, vf);

    label = gtk_label_new(_("Dimensions:"));
    gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);
    gtk_table_attach(GTK_TABLE(table), label,
                     0, 1, row, row+1, GTK_FILL, 0, 0, 0);

    buf = g_strdup_printf("%.*f%s%s × %.*f%s%s",
                          vf->precision, args->xorig/vf->magnitude,
                          *vf->units ? " " : "", vf->units,
                          vf->precision, args->yorig/vf->magnitude,
                          *vf->units ? " " : "", vf->units);
    label = gtk_label_new(buf);
    g_free(buf);
    gtk_label_set_use_markup(GTK_LABEL(label), TRUE);
    gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);
    gtk_table_attach(GTK_TABLE(table), label,
                     1, 4, row, row+1, GTK_FILL, 0, 0, 0);
    row++;

    label = gtk_label_new(_("Offsets:"));
    gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);
    gtk_table_attach(GTK_TABLE(table), label,
                     0, 1, row, row+1, GTK_FILL, 0, 0, 0);

    buf = g_strdup_printf("(%.*f%s%s, %.*f%s%s)",
                          vf->precision, args->x0orig/vf->magnitude,
                          *vf->units ? " " : "", vf->units,
                          vf->precision, args->y0orig/vf->magnitude,
                          *vf->units ? " " : "", vf->units);
    label = gtk_label_new(buf);
    g_free(buf);
    gtk_label_set_use_markup(GTK_LABEL(label), TRUE);
    gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);
    gtk_table_attach(GTK_TABLE(table), label,
                     1, 4, row, row+1, GTK_FILL, 0, 0, 0);
    row++;

    vf = gwy_data_field_get_value_format_z(dfield,
                                            GWY_SI_UNIT_FORMAT_VFMARKUP, vf);
    label = gtk_label_new(_("Value range:"));
    gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);
    gtk_table_attach(GTK_TABLE(table), label,
                     0, 1, row, row+1, GTK_FILL, 0, 0, 0);

    buf = g_strdup_printf("%.*f%s%s",
                          vf->precision, args->zorig/vf->magnitude,
                          *vf->units ? " " : "", vf->units);
    label = gtk_label_new(buf);
    g_free(buf);
    gtk_label_set_use_markup(GTK_LABEL(label), TRUE);
    gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);
    gtk_table_attach(GTK_TABLE(table), label,
                     1, 4, row, row+1, GTK_FILL, 0, 0, 0);
    row++;

    gwy_si_unit_value_format_free(vf);

    /***** New Real Dimensions *****/

    gtk_table_set_row_spacing(GTK_TABLE(table), row-1, 8);
    gtk_table_attach(GTK_TABLE(table),
                     gwy_label_new_header(_("New Real Dimensions")),
                     0, 3, row, row+1, GTK_FILL, 0, 0, 0);
    row++;

    controls.size_chooser = gwy_data_chooser_new_channels();
    chooser = GWY_DATA_CHOOSER(controls.size_chooser);
    gwy_data_chooser_set_filter(chooser, mould_filter, &args->targetid, NULL);
    if (!gwy_data_chooser_set_active_id(chooser, &args->sizeid))
        args->match_size = FALSE;

    gwy_table_attach_adjbar(table, row, _("_Match pixel size:"), NULL,
                            GTK_OBJECT(controls.size_chooser),
                            GWY_HSCALE_CHECK | GWY_HSCALE_WIDGET_NO_EXPAND);
    controls.match_size
        = gwy_table_hscale_get_check(GTK_OBJECT(controls.size_chooser));
    g_signal_connect(controls.match_size, "toggled",
                     G_CALLBACK(match_size_changed), &controls);

    if (gwy_data_chooser_get_active(chooser, NULL)) {
        /* XXX: Extend the chooser one column to the right.  Dirty. */
        if ((hbox2 = gtk_widget_get_parent(controls.size_chooser))
            && GTK_IS_HBOX(hbox2)) {
            gtk_container_child_set(GTK_CONTAINER(table), hbox2,
                                    "right-attach", 3,
                                    NULL);
        }
        g_signal_connect(controls.size_chooser, "changed",
                         G_CALLBACK(size_channel_changed), &controls);
    }
    else {
        gwy_table_hscale_set_sensitive(GTK_OBJECT(controls.size_chooser),
                                       FALSE);
        gtk_widget_set_no_show_all(controls.match_size, TRUE);
        gtk_widget_set_no_show_all(controls.size_chooser, TRUE);
        args->match_size = FALSE;
    }
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(controls.match_size),
                                 args->match_size);
    row++;

    controls.xreal = gtk_adjustment_new(args->xreal/pow10(args->xyexponent),
                                        0.01, 10000, 1, 10, 0);
    spin = gwy_table_attach_adjbar(table, row, _("_X range:"), NULL,
                                   controls.xreal, GWY_HSCALE_LOG);
    gtk_spin_button_set_digits(GTK_SPIN_BUTTON(spin), 3);

    unit = gwy_data_field_get_si_unit_xy(dfield);
    controls.xyexponent
        = gwy_combo_box_metric_unit_new(G_CALLBACK(xyexponent_changed),
                                        &controls, -15, 6, unit,
                                        args->xyexponent);
    gtk_table_attach(GTK_TABLE(table), controls.xyexponent,
                     2, 3, row, row+2, GTK_FILL | GTK_SHRINK, 0, 0, 0);

    controls.xyunits = gtk_button_new_with_label(gwy_sgettext("verb|Change"));
    g_object_set_data(G_OBJECT(controls.xyunits), "id", (gpointer)"xy");
    gtk_table_attach(GTK_TABLE(table), controls.xyunits,
                     3, 4, row, row+2, GTK_FILL | GTK_SHRINK, 0, 0, 0);
    row++;

    controls.yreal = gtk_adjustment_new(args->yreal/pow10(args->xyexponent),
                                        0.01, 10000, 1, 10, 0);
    spin = gwy_table_attach_adjbar(table, row, _("_Y range:"), NULL,
                                   controls.yreal, GWY_HSCALE_LOG);
    gtk_spin_button_set_digits(GTK_SPIN_BUTTON(spin), 3);
    row++;

    controls.square = gtk_check_button_new_with_mnemonic(_("_Square samples"));
    gtk_table_attach(GTK_TABLE(table), controls.square,
                     0, 2, row, row+1, GTK_FILL, 0, 0, 0);
    row++;

    /***** Lateral Offsets *****/

    controls.x0 = gtk_adjustment_new(args->x0/pow10(args->xyexponent),
                                     -10000, 10000, 1, 10, 0);
    spin = gwy_table_attach_adjbar(table, row, _("_X offset:"), NULL,
                                   controls.x0, GWY_HSCALE_SQRT);
    gtk_spin_button_set_digits(GTK_SPIN_BUTTON(spin), 2);
    row++;

    controls.y0 = gtk_adjustment_new(args->y0/pow10(args->xyexponent),
                                     -10000, 10000, 1, 10, 0);
    spin = gwy_table_attach_adjbar(table, row, _("_Y offset:"), NULL,
                                   controls.y0, GWY_HSCALE_SQRT);
    gtk_spin_button_set_digits(GTK_SPIN_BUTTON(spin), 2);
    row++;

    /***** New Value Range *****/

    gtk_table_set_row_spacing(GTK_TABLE(table), row-1, 8);
    gtk_table_attach(GTK_TABLE(table),
                     gwy_label_new_header(_("New Value Range")),
                     0, 2, row, row+1, GTK_FILL, 0, 0, 0);
    row++;

    controls.zreal = gtk_adjustment_new(args->zreal/pow10(args->xyexponent),
                                     -10000, 10000, 1, 10, 0);
    spin = gwy_table_attach_adjbar(table, row, _("_Z range:"), NULL,
                                   controls.zreal, GWY_HSCALE_SQRT);
    gtk_spin_button_set_digits(GTK_SPIN_BUTTON(spin), 2);

    unit = gwy_data_field_get_si_unit_z(dfield);
    controls.zexponent
        = gwy_combo_box_metric_unit_new(G_CALLBACK(zexponent_changed),
                                        &controls, -15, 6, unit,
                                        args->zexponent);
    gtk_table_attach(GTK_TABLE(table), controls.zexponent,
                     2, 3, row, row+1, GTK_FILL | GTK_SHRINK, 0, 0, 0);

    controls.zunits = gtk_button_new_with_label(gwy_sgettext("verb|Change"));
    g_object_set_data(G_OBJECT(controls.zunits), "id", (gpointer)"z");
    gtk_table_attach(GTK_TABLE(table), controls.zunits,
                     3, 4, row, row+1, GTK_FILL | GTK_SHRINK, 0, 0, 0);
    row++;

    /***** Value Shift *****/

    controls.zshift = gtk_adjustment_new(args->zshift/pow10(args->xyexponent),
                                     -10000, 10000, 1, 10, 0);
    spin = gwy_table_attach_adjbar(table, row, _("Z shi_ft:"), NULL,
                                   controls.zshift, GWY_HSCALE_SQRT);
    gtk_spin_button_set_digits(GTK_SPIN_BUTTON(spin), 2);
    row++;

    /***** Calibration Coefficients *****/

    gtk_table_set_row_spacing(GTK_TABLE(table), row-1, 8);
    gtk_table_attach(GTK_TABLE(table),
                     gwy_label_new_header(_("Calibration Coefficients")),
                     0, 2, row, row+1, GTK_FILL, 0, 0, 0);
    row++;

    controls.xratio = gtk_adjustment_new(args->xratio, 0.001, 1000, 0.1, 1, 0);
    spin = gwy_table_attach_adjbar(table, row, _("_X calibration factor:"), " ",
                                   controls.xratio, GWY_HSCALE_LOG);
    gtk_spin_button_set_digits(GTK_SPIN_BUTTON(spin), 3);
    controls.xpower10 = gwy_table_hscale_get_units(controls.xratio);
    row++;

    controls.yratio = gtk_adjustment_new(args->yratio, 0.001, 1000, 0.1, 1, 0);
    spin = gwy_table_attach_adjbar(table, row, _("_Y calibration factor:"), " ",
                                   controls.yratio, GWY_HSCALE_LOG);
    gtk_spin_button_set_digits(GTK_SPIN_BUTTON(spin), 3);
    controls.ypower10 = gwy_table_hscale_get_units(controls.yratio);
    row++;

    controls.zratio = gtk_adjustment_new(args->zratio, -1000, 1000, 0.1, 1, 0);
    spin = gwy_table_attach_adjbar(table, row, _("_Z calibration factor:"), " ",
                                   controls.zratio, GWY_HSCALE_SQRT);
    gtk_spin_button_set_digits(GTK_SPIN_BUTTON(spin), 3);
    controls.zpower10 = gwy_table_hscale_get_units(controls.zratio);
    row++;

    /***** Options *****/

    gtk_table_set_row_spacing(GTK_TABLE(table), row-1, 8);
    label = gwy_label_new_header(_("Options"));
    gtk_table_attach(GTK_TABLE(table), label,
                     0, 2, row, row+1, GTK_FILL, 0, 0, 0);
    row++;

    controls.new_channel
        = gtk_check_button_new_with_mnemonic(_("Create new image"));
    gtk_table_attach(GTK_TABLE(table), controls.new_channel,
                     0, 2, row, row+1, GTK_FILL, 0, 0, 0);
    row++;

    g_signal_connect(controls.xreal, "value-changed",
                     G_CALLBACK(xreal_changed), &controls);
    g_signal_connect(controls.yreal, "value-changed",
                     G_CALLBACK(yreal_changed), &controls);
    g_signal_connect(controls.xyunits, "clicked",
                     G_CALLBACK(units_change), &controls);
    g_signal_connect(controls.zunits, "clicked",
                     G_CALLBACK(units_change), &controls);
    g_signal_connect(controls.x0, "value-changed",
                     G_CALLBACK(x0_changed), &controls);
    g_signal_connect(controls.y0, "value-changed",
                     G_CALLBACK(y0_changed), &controls);
    g_signal_connect(controls.zshift, "value-changed",
                     G_CALLBACK(zshift_changed), &controls);
    g_signal_connect(controls.zreal, "value-changed",
                     G_CALLBACK(zreal_changed), &controls);
    g_signal_connect(controls.xratio, "value-changed",
                     G_CALLBACK(xratio_changed), &controls);
    g_signal_connect(controls.yratio, "value-changed",
                     G_CALLBACK(yratio_changed), &controls);
    g_signal_connect(controls.zratio, "value-changed",
                     G_CALLBACK(zratio_changed), &controls);
    g_signal_connect(controls.square, "toggled",
                     G_CALLBACK(square_changed), &controls);
    g_signal_connect(controls.new_channel, "toggled",
                     G_CALLBACK(new_channel_changed), &controls);

    controls.in_update = FALSE;
    /* sync all fields */
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(controls.match_size),
                                 args->match_size);
    calibrate_dialog_update(&controls, args);

    gtk_widget_show_all(dialog);
    do {
        response = gtk_dialog_run(GTK_DIALOG(dialog));
        switch (response) {
            case GTK_RESPONSE_CANCEL:
            case GTK_RESPONSE_DELETE_EVENT:
            gtk_widget_destroy(dialog);
            case GTK_RESPONSE_NONE:
            return FALSE;
            break;

            case GTK_RESPONSE_OK:
            break;

            case RESPONSE_RESET:
            dialog_reset(&controls, args);
            break;

            default:
            g_assert_not_reached();
            break;
        }
    } while (response != GTK_RESPONSE_OK);

    gtk_widget_destroy(dialog);

    return TRUE;
}

static void
dialog_reset(CalibrateControls *controls,
             CalibrateArgs *args)
{
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(controls->square),
                                 calibrate_defaults.square);
    /* Don't reset this one
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(controls->new_channel),
                                 calibrate_defaults.new_channel);
                                 */

    set_combo_from_unit(controls->xyexponent,
                        args->xyunitorig, args->xyorigexp);
    gwy_enum_combo_box_set_active(GTK_COMBO_BOX(controls->xyexponent),
                                  args->xyorigexp);
    gtk_adjustment_set_value(GTK_ADJUSTMENT(controls->xreal),
                             args->xorig/pow10(args->xyorigexp));
    gtk_adjustment_set_value(GTK_ADJUSTMENT(controls->yreal),
                             args->yorig/pow10(args->xyorigexp));
    gtk_adjustment_set_value(GTK_ADJUSTMENT(controls->yratio),
                             calibrate_defaults.yratio);
    gtk_adjustment_set_value(GTK_ADJUSTMENT(controls->xratio),
                             calibrate_defaults.xratio);

    set_combo_from_unit(controls->zexponent,
                        args->zunitorig, args->zorigexp);
    gwy_enum_combo_box_set_active(GTK_COMBO_BOX(controls->zexponent),
                                  args->zorigexp);
    gtk_adjustment_set_value(GTK_ADJUSTMENT(controls->zreal),
                             args->zorig/pow10(args->zorigexp));
    gtk_adjustment_set_value(GTK_ADJUSTMENT(controls->zratio),
                             calibrate_defaults.zratio);

    gtk_adjustment_set_value(GTK_ADJUSTMENT(controls->x0),
                             args->x0orig/pow10(args->xyexponent));
    gtk_adjustment_set_value(GTK_ADJUSTMENT(controls->y0),
                             args->y0orig/pow10(args->xyexponent));
    gtk_adjustment_set_value(GTK_ADJUSTMENT(controls->zshift),
                             calibrate_defaults.zshift);

    calibrate_dialog_update(controls, args);

    args->zreal = args->zorig;
    args->xreal = args->xorig;
    args->yreal = args->yorig;
    args->x0 = args->x0orig;
    args->y0 = args->y0orig;
}

static void
xratio_changed(GtkAdjustment *adj,
               CalibrateControls *controls)
{
    CalibrateArgs *args = controls->args;
    gdouble newratio;

    if (controls->in_update)
        return;

    controls->in_update = TRUE;
    newratio = gtk_adjustment_get_value(adj) * pow10(args->xyexponent
                                                     - args->xyorigexp);
    gwy_debug("newratio: %.14g (%.14g)", newratio, args->xratio);
    /* XXX: Sometimes a strange infinite update cycle far in non-significant
     * digits can occur.  Not sure why. */
    if (fabs(log(newratio/args->xratio)) < 1e-8) {
        controls->in_update = FALSE;
        return;
    }

    args->xratio = newratio;
    args->xreal = args->xratio * args->xorig;
    if (args->square) {
        args->yreal = args->xreal/args->xres * args->yres;
        args->yratio = args->yreal/args->yorig;
    }
    calibrate_dialog_update(controls, args);
    controls->in_update = FALSE;
}

static void
yratio_changed(GtkAdjustment *adj,
               CalibrateControls *controls)
{
    CalibrateArgs *args = controls->args;
    gdouble newratio;

    if (controls->in_update)
        return;

    controls->in_update = TRUE;
    newratio = gtk_adjustment_get_value(adj) * pow10(args->xyexponent
                                                     - args->xyorigexp);
    gwy_debug("newratio: %.14g (%.14g)", newratio, args->yratio);
    /* XXX: Sometimes a strange infinite update cycle far in non-significant
     * digits can occur.  Not sure why. */
    if (fabs(log(newratio/args->yratio)) < 1e-8) {
        controls->in_update = FALSE;
        return;
    }

    args->yratio = newratio;
    args->yreal = args->yratio * args->yorig;
    if (args->square) {
        args->xreal = args->yreal/args->yres * args->xres;
        args->xratio = args->xreal/args->xorig;
    }
    calibrate_dialog_update(controls, args);
    controls->in_update = FALSE;
}

static void
zratio_changed(GtkAdjustment *adj,
               CalibrateControls *controls)
{
    CalibrateArgs *args = controls->args;
    gdouble newratio;

    if (controls->in_update)
        return;

    controls->in_update = TRUE;
    newratio = gtk_adjustment_get_value(adj) * pow10(args->zexponent
                                                     - args->zorigexp);
    /* XXX: Sometimes a strange infinite update cycle far in non-significant
     * digits can occur.  Not sure why. */
    if (fabs(log(newratio/args->zratio)) < 1e-8) {
        controls->in_update = FALSE;
        return;
    }

    args->zratio = newratio;
    args->zreal = args->zratio * args->zorig;
    calibrate_dialog_update(controls, args);
    controls->in_update = FALSE;
}

static void
xreal_changed(GtkAdjustment *adj,
              CalibrateControls *controls)
{
    CalibrateArgs *args = controls->args;
    gdouble newxreal;

    if (controls->in_update)
        return;

    controls->in_update = TRUE;
    newxreal = gtk_adjustment_get_value(adj) * pow10(args->xyexponent);
    gwy_debug("newxreal: %.14g (%.14g)", newxreal, args->xreal);
    /* XXX: Sometimes a strange infinite update cycle far in non-significant
     * digits can occur.  Not sure why. */
    if (fabs(log(newxreal/args->xreal)) < 1e-8) {
        controls->in_update = FALSE;
        return;
    }

    args->xreal = newxreal;
    args->xratio = args->xreal/args->xorig;
    if (args->square) {
        args->yreal = args->xreal/args->xres * args->yres;
        args->yratio = args->yreal/args->yorig;
    }
    calibrate_dialog_update(controls, args);
    controls->in_update = FALSE;

}

static void
yreal_changed(GtkAdjustment *adj,
              CalibrateControls *controls)
{
    CalibrateArgs *args = controls->args;
    gdouble newyreal;

    if (controls->in_update)
        return;

    controls->in_update = TRUE;
    newyreal = gtk_adjustment_get_value(adj) * pow10(args->xyexponent);
    gwy_debug("newyreal: %.14g (%.14g)", newyreal, args->yreal);
    /* XXX: Sometimes a strange infinite update cycle far in non-significant
     * digits can occur.  Not sure why. */
    if (fabs(log(newyreal/args->yreal)) < 1e-8) {
        controls->in_update = FALSE;
        return;
    }

    args->yreal = newyreal;
    args->yratio = args->yreal/args->yorig;
    if (args->square) {
        args->xreal = args->yreal/args->yres * args->xres;
        args->xratio = args->xreal/args->xorig;
    }
    calibrate_dialog_update(controls, args);
    controls->in_update = FALSE;
}

static void
x0_changed(GtkAdjustment *adj,
           CalibrateControls *controls)
{
    CalibrateArgs *args = controls->args;
    gdouble newx0;

    if (controls->in_update)
        return;

    controls->in_update = TRUE;
    newx0 = gtk_adjustment_get_value(adj) * pow10(args->xyexponent);
    /* XXX: Sometimes a strange infinite update cycle far in non-significant
     * digits can occur.  Not sure why. */
    if (newx0 == args->x0 || fabs(log(newx0/args->x0)) < 1e-8) {
        controls->in_update = FALSE;
        return;
    }

    args->x0 = newx0;
    calibrate_dialog_update(controls, args);
    controls->in_update = FALSE;
}

static void
y0_changed(GtkAdjustment *adj,
           CalibrateControls *controls)
{
    CalibrateArgs *args = controls->args;
    gdouble newy0;

    if (controls->in_update)
        return;

    controls->in_update = TRUE;
    newy0 = gtk_adjustment_get_value(adj) * pow10(args->xyexponent);
    /* XXX: Sometimes a strange infinite update cycle far in non-significant
     * digits can occur.  Not sure why. */
    if (newy0 == args->y0 || fabs(log(newy0/args->y0)) < 1e-8) {
        controls->in_update = FALSE;
        return;
    }

    args->y0 = newy0;
    calibrate_dialog_update(controls, args);
    controls->in_update = FALSE;
}

static void
zshift_changed(GtkAdjustment *adj,
               CalibrateControls *controls)
{
    CalibrateArgs *args = controls->args;
    gdouble newzshift;

    if (controls->in_update)
        return;

    controls->in_update = TRUE;
    newzshift = gtk_adjustment_get_value(adj) * pow10(args->zexponent);
    /* XXX: Sometimes a strange infinite update cycle far in non-significant
     * digits can occur.  Not sure why. */
    if (newzshift == args->zshift || fabs(log(newzshift/args->zshift)) < 1e-8) {
        controls->in_update = FALSE;
        return;
    }

    args->zshift = newzshift;
    calibrate_dialog_update(controls, args);
    controls->in_update = FALSE;
}

static void
zreal_changed(GtkAdjustment *adj,
              CalibrateControls *controls)
{
    CalibrateArgs *args = controls->args;
    gdouble newzreal;

    if (controls->in_update)
        return;

    controls->in_update = TRUE;
    newzreal = gtk_adjustment_get_value(adj) * pow10(args->zexponent);
    /* XXX: Sometimes a strange infinite update cycle far in non-significant
     * digits can occur.  Not sure why. */
    if (fabs(log(newzreal/args->zreal)) < 1e-8) {
        controls->in_update = FALSE;
        return;
    }

    args->zreal = newzreal;
    args->zratio = args->zreal/args->zorig;
    calibrate_dialog_update(controls, args);
    controls->in_update = FALSE;
}

static void
square_changed(GtkWidget *check,
               CalibrateControls *controls)
{
    CalibrateArgs *args = controls->args;

    if (controls->in_update)
        return;

    controls->in_update = TRUE;

    args->square
        = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(check));
    if (args->square) {
        args->yreal = args->xreal/args->xres * args->yres;
        args->yratio = args->yreal/args->yorig;
    }
    calibrate_dialog_update(controls, args);
    controls->in_update = FALSE;
}

static void
new_channel_changed(GtkWidget *check,
                    CalibrateControls *controls)
{
    CalibrateArgs *args = controls->args;
    args->new_channel = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(check));
}

static void
xyexponent_changed(GtkWidget *combo,
                   CalibrateControls *controls)
{
    CalibrateArgs *args = controls->args;

    if (controls->in_update)
        return;

    controls->in_update = TRUE;
    args->xyexponent = gwy_enum_combo_box_get_active(GTK_COMBO_BOX(combo));
    args->xreal = gtk_adjustment_get_value(GTK_ADJUSTMENT(controls->xreal))
                  * pow10(args->xyexponent);
    args->x0 = gtk_adjustment_get_value(GTK_ADJUSTMENT(controls->x0))
               * pow10(args->xyexponent);
    args->xratio = args->xreal/args->xorig;
    args->yreal = gtk_adjustment_get_value(GTK_ADJUSTMENT(controls->yreal))
                  * pow10(args->xyexponent);
    args->y0 = gtk_adjustment_get_value(GTK_ADJUSTMENT(controls->y0))
               * pow10(args->xyexponent);
    args->yratio = args->yreal/args->yorig;
    calibrate_dialog_update(controls, args);
    controls->in_update = FALSE;
}

static void
zexponent_changed(GtkWidget *combo,
                  CalibrateControls *controls)
{
    CalibrateArgs *args = controls->args;

    if (controls->in_update)
        return;

    controls->in_update = TRUE;

    args->zexponent = gwy_enum_combo_box_get_active(GTK_COMBO_BOX(combo));
    args->zreal = gtk_adjustment_get_value(GTK_ADJUSTMENT(controls->zreal))
                  * pow10(args->zexponent);
    args->zratio = args->zreal/args->zorig;
    calibrate_dialog_update(controls, args);
    controls->in_update = FALSE;
}

static void
units_change(GtkWidget *button,
             CalibrateControls *controls)
{
    GtkWidget *dialog, *hbox, *label, *entry;
    const gchar *id, *unit;
    gint response;
    CalibrateArgs *args = controls->args;

    if (controls->in_update)
        return;

    controls->in_update = TRUE;

    id = g_object_get_data(G_OBJECT(button), "id");
    dialog = gtk_dialog_new_with_buttons(_("Change Units"),
                                         NULL,
                                         GTK_DIALOG_MODAL
                                         | GTK_DIALOG_NO_SEPARATOR,
                                         GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
                                         GTK_STOCK_OK, GTK_RESPONSE_OK,
                                         NULL);
    gtk_dialog_set_default_response(GTK_DIALOG(dialog), GTK_RESPONSE_OK);

    hbox = gtk_hbox_new(FALSE, 6);
    gtk_container_set_border_width(GTK_CONTAINER(hbox), 4);
    gtk_box_pack_start(GTK_BOX(GTK_DIALOG(dialog)->vbox), hbox,
                       FALSE, FALSE, 0);

    label = gtk_label_new_with_mnemonic(_("New _units:"));
    gtk_box_pack_start(GTK_BOX(hbox), label, TRUE, TRUE, 0);

    entry = gtk_entry_new();
    gtk_entry_set_activates_default(GTK_ENTRY(entry), TRUE);
    gtk_box_pack_start(GTK_BOX(hbox), entry, TRUE, TRUE, 0);

    gtk_widget_show_all(dialog);
    response = gtk_dialog_run(GTK_DIALOG(dialog));
    if (response != GTK_RESPONSE_OK) {
        gtk_widget_destroy(dialog);
        return;
    }

    unit = gtk_entry_get_text(GTK_ENTRY(entry));
    if (gwy_strequal(id, "xy")) {
        set_combo_from_unit(controls->xyexponent, unit, 0);
        g_free(controls->args->xyunit);
        controls->args->xyunit = g_strdup(unit);
    }
    else if (gwy_strequal(id, "z")) {
        set_combo_from_unit(controls->zexponent, unit, 0);
        g_free(controls->args->zunit);
        controls->args->zunit = g_strdup(unit);
    }

    gtk_widget_destroy(dialog);

    calibrate_dialog_update(controls, args);
    controls->in_update = FALSE;
}

static void
match_size_changed(GtkToggleButton *toggle,
                   CalibrateControls *controls)
{
    gboolean matching = gtk_toggle_button_get_active(toggle);

    controls->args->match_size = matching;
    gwy_table_hscale_set_sensitive(controls->xreal, !matching);
    gwy_table_hscale_set_sensitive(controls->yreal, !matching);
    gwy_table_hscale_set_sensitive(controls->xratio, !matching);
    gwy_table_hscale_set_sensitive(controls->yratio, !matching);
    gtk_widget_set_sensitive(controls->xyunits, !matching);
    gtk_widget_set_sensitive(controls->xyexponent, !matching);
    gtk_widget_set_sensitive(controls->square, !matching);
    /* This is fixed to sensitive again in size_channel_changed() any channel
     * is selected. */
    gtk_dialog_set_response_sensitive(GTK_DIALOG(controls->dialog),
                                      GTK_RESPONSE_OK, !matching);

    if (matching) {
        size_channel_changed(GWY_DATA_CHOOSER(controls->size_chooser),
                             controls);
    }
}

static void
size_channel_changed(GwyDataChooser *chooser,
                     CalibrateControls *controls)
{
    CalibrateArgs *args = controls->args;
    GwyContainer *data;
    GwyDataField *mould;
    GQuark quark;
    gdouble dx, dy;
    GwySIUnit *unit;

    if (!gwy_data_chooser_get_active_id(chooser, &args->sizeid)) {
        gtk_dialog_set_response_sensitive(GTK_DIALOG(controls->dialog),
                                          GTK_RESPONSE_OK, FALSE);
        return;
    }

    quark = gwy_app_get_data_key_for_id(args->sizeid.id);
    data = gwy_app_data_browser_get(args->sizeid.datano);
    mould = GWY_DATA_FIELD(gwy_container_get_object(data, quark));
    dx = gwy_data_field_get_xmeasure(mould);
    dy = gwy_data_field_get_ymeasure(mould);
    unit = gwy_data_field_get_si_unit_xy(mould);

    controls->in_update = TRUE;
    args->xreal = dx * args->xres;
    args->yreal = dy * args->yres;
    args->xratio = args->xreal/args->xorig;
    args->yratio = args->yreal/args->yorig;
    args->xyexponent = 3*floor(log10(args->xreal*args->yreal)/6);
    g_free(args->xyunit);
    args->xyunit = gwy_si_unit_get_string(unit, GWY_SI_UNIT_FORMAT_PLAIN);
    args->square = (fabs(log(dx/dy)) <= EPSILON);

    set_combo_from_unit(controls->xyexponent, args->xyunit, args->xyexponent);
    gwy_enum_combo_box_set_active(GTK_COMBO_BOX(controls->xyexponent),
                                  args->xyexponent);
    calibrate_dialog_update(controls, args);
    controls->in_update = FALSE;
}

static gboolean
mould_filter(GwyContainer *data,
             gint id,
             gpointer user_data)
{
    GwyAppDataId *object = (GwyAppDataId*)user_data;
    GwyContainer *objdata = gwy_app_data_browser_get(object->datano);
    return data != objdata || id != object->id;
}

static void
set_combo_from_unit(GtkWidget *combo,
                    const gchar *str,
                    gint basepower)
{
    GwySIUnit *unit;
    gint power10;

    unit = gwy_si_unit_new_parse(str, &power10);
    power10 += basepower;
    gwy_combo_box_metric_unit_set_unit(GTK_COMBO_BOX(combo),
                                       power10 - 6, power10 + 6, unit);
    g_object_unref(unit);
}

static void
calibrate_dialog_update(CalibrateControls *controls,
                        CalibrateArgs *args)
{
    gchar buffer[32];
    gint e;

    gwy_debug("setting xreal to: %.14g",
              args->xreal/pow10(args->xyexponent));
    gtk_adjustment_set_value(GTK_ADJUSTMENT(controls->xreal),
                             args->xreal/pow10(args->xyexponent));
    gwy_debug("setting yreal to: %.14g",
              args->yreal/pow10(args->xyexponent));
    gtk_adjustment_set_value(GTK_ADJUSTMENT(controls->yreal),
                             args->yreal/pow10(args->xyexponent));
    gtk_adjustment_set_value(GTK_ADJUSTMENT(controls->x0),
                             args->x0/pow10(args->xyexponent));
    gtk_adjustment_set_value(GTK_ADJUSTMENT(controls->y0),
                             args->y0/pow10(args->xyexponent));

    gtk_adjustment_set_value(GTK_ADJUSTMENT(controls->zreal),
                             args->zreal/pow10(args->zexponent));
    gtk_adjustment_set_value(GTK_ADJUSTMENT(controls->zshift),
                             args->zshift/pow10(args->zexponent));

    gwy_debug("setting xratio to: %.14g",
              args->xratio/pow10(args->xyexponent - args->xyorigexp));
    gtk_adjustment_set_value(GTK_ADJUSTMENT(controls->xratio),
                             args->xratio/pow10(args->xyexponent
                                                - args->xyorigexp));
    gwy_debug("setting yratio to: %.14g",
              args->yratio/pow10(args->xyexponent - args->xyorigexp));
    gtk_adjustment_set_value(GTK_ADJUSTMENT(controls->yratio),
                             args->yratio/pow10(args->xyexponent
                                                - args->xyorigexp));
    gtk_adjustment_set_value(GTK_ADJUSTMENT(controls->zratio),
                             args->zratio/pow10(args->zexponent
                                                - args->zorigexp));

    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(controls->square),
                                 args->square);
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(controls->new_channel),
                                 args->new_channel);

    e = args->xyexponent - args->xyorigexp;
    if (!e)
        buffer[0] = '\0';
    else
        g_snprintf(buffer, sizeof(buffer), "× 10<sup>%d</sup>", e);
    gtk_label_set_markup(GTK_LABEL(controls->xpower10), buffer);
    gtk_label_set_markup(GTK_LABEL(controls->ypower10), buffer);

    e = args->zexponent - args->zorigexp;
    if (!e)
        buffer[0] = '\0';
    else
        g_snprintf(buffer, sizeof(buffer), "× 10<sup>%d</sup>", e);
    gtk_label_set_markup(GTK_LABEL(controls->zpower10), buffer);

    if (args->zratio == 0 || args->zreal == 0)
        gtk_widget_set_sensitive(controls->ok, FALSE);
    else
        gtk_widget_set_sensitive(controls->ok, TRUE);
}

static const gchar xratio_key[]      = "/module/calibrate/xratio";
static const gchar yratio_key[]      = "/module/calibrate/yratio";
static const gchar zratio_key[]      = "/module/calibrate/zratio";
static const gchar zshift_key[]      = "/module/calibrate/zshift";
static const gchar square_key[]      = "/module/calibrate/square";
static const gchar new_channel_key[] = "/module/calibrate/new_channel";
static const gchar match_size_key[]  = "/module/calibrate/match_size";

static void
calibrate_sanitize_args(CalibrateArgs *args)
{
    args->xratio = CLAMP(args->xratio, 1e-15, 1e15);
    args->yratio = CLAMP(args->yratio, 1e-15, 1e15);
    args->zratio = CLAMP(args->zratio, -1e15, 1e15);
    args->zshift = CLAMP(args->zshift, -1e-9, 1e9);
    args->square = !!args->square;
    args->new_channel = !!args->new_channel;
    args->match_size = !!args->match_size;
    gwy_app_data_id_verify_channel(&args->sizeid);
}

static void
calibrate_load_args(GwyContainer *container,
                    CalibrateArgs *args)
{
    *args = calibrate_defaults;

    gwy_container_gis_double_by_name(container, xratio_key, &args->xratio);
    gwy_container_gis_double_by_name(container, yratio_key, &args->yratio);
    gwy_container_gis_double_by_name(container, zratio_key, &args->zratio);
    gwy_container_gis_double_by_name(container, zshift_key, &args->zshift);
    gwy_container_gis_boolean_by_name(container, square_key, &args->square);
    gwy_container_gis_boolean_by_name(container, new_channel_key,
                                      &args->new_channel);
    gwy_container_gis_boolean_by_name(container, match_size_key,
                                      &args->match_size);
    args->sizeid = mould_id;
    calibrate_sanitize_args(args);
}

static void
calibrate_save_args(GwyContainer *container,
                    CalibrateArgs *args)
{
    mould_id = args->sizeid;
    gwy_container_set_double_by_name(container, xratio_key, args->xratio);
    gwy_container_set_double_by_name(container, yratio_key, args->yratio);
    gwy_container_set_double_by_name(container, zratio_key, args->zratio);
    gwy_container_set_double_by_name(container, zshift_key, args->zshift);
    gwy_container_set_boolean_by_name(container, square_key, args->square);
    gwy_container_set_boolean_by_name(container, new_channel_key,
                                      args->new_channel);
    gwy_container_set_boolean_by_name(container, match_size_key,
                                      args->match_size);
}

/* vim: set cin et ts=4 sw=4 cino=>1s,e0,n0,f0,{0,}0,^0,\:1s,=0,g1s,h0,t0,+1s,c3,(0,u0 : */
