/*
 *  $Id$
 *  Copyright (C) 2017 David Necas (Yeti), Petr Klapetek.
 *  E-mail: yeti@gwyddion.net, klapetek@gwyddion.net.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA 02110-1301, USA.
 */

#ifndef __GWY_PROCESS_MFMOPS_H__
#define __GWY_PROCESS_MFMOPS_H__

#include <libgwyddion/gwymath.h>
#include <libprocess/gwyprocesstypes.h>
#include <libprocess/arithmetic.h>
#include <libprocess/inttrans.h>
#include <libprocess/filters.h>
#include <libprocess/stats.h>

#define MU_0 1.256637061435917295e-6

#define MFM_DIMENSION_ARGS_INIT \
    { 256, 256, 5.0, (gpointer)"m", NULL, -9, 0, FALSE, FALSE }

/* XXX XXX XXX: add find-shift as a public function (with wait callback)? */
/* XXX XXX XXX: add Wiener filter (calculate_psf) as a public function?
 *              it should be renamed then because it is just a deconvolution
 *              and can be used to recover either operand, not just the PSF */

/* helper function, keep here? */
static void set_transfer_function_units                   (GwyDataField *source,
                                                           GwyDataField *image,
                                                           GwyDataField *transferfunc);
/* field ??? ??? ??? */
static void get_force_gradient_from_phase                 (GwyDataField *in,
                                                           GwyDataField *out,
                                                           gdouble spring_constant,
                                                           gdouble quality);
static void get_force_gradient_from_frequency_shift       (GwyDataField *in,
                                                           GwyDataField *out,
                                                           gdouble spring_constant,
                                                           gdouble base_frequency);
static void get_force_gradient_from_amplitude_shift       (GwyDataField *in,
                                                           GwyDataField *out,
                                                           gdouble spring_constant,
                                                           gdouble quality,
                                                           gdouble base_amplitude);
/* volume ??? ??? ??? */
static void get_volume_force_gradient_from_phase          (GwyBrick *in,
                                                           GwyBrick *out,
                                                           gdouble spring_constant,
                                                           gdouble quality);
static void get_volume_force_gradient_from_frequency_shift(GwyBrick *in,
                                                           GwyBrick *out,
                                                           gdouble spring_constant,
                                                           gdouble base_frequency);
static void get_volume_force_gradient_from_amplitude_shift(GwyBrick *in,
                                                           GwyBrick *out,
                                                           gdouble spring_constant,
                                                           gdouble quality,
                                                           gdouble base_amplitude);

G_GNUC_UNUSED
static void
set_transfer_function_units(GwyDataField *source, GwyDataField *image,
                            GwyDataField *transferfunc)
{
    GwySIUnit *sunit, *iunit, *tunit, *xyunit;

    xyunit = gwy_data_field_get_si_unit_xy(image);
    sunit = gwy_data_field_get_si_unit_z(source);
    iunit = gwy_data_field_get_si_unit_z(image);
    tunit = gwy_data_field_get_si_unit_z(transferfunc);
    gwy_si_unit_divide(iunit, sunit, tunit);
    gwy_si_unit_power_multiply(tunit, 1, xyunit, -2, tunit);
}

G_GNUC_UNUSED
static void
get_force_gradient_from_phase(GwyDataField *in, GwyDataField *out, gdouble spring_constant, gdouble quality)
{
    gdouble q = gwy_data_field_get_xres(in)*gwy_data_field_get_yres(in)/(gwy_data_field_get_xreal(in)*gwy_data_field_get_yreal(in));
    gdouble factor = spring_constant*M_PI*q/(quality*180*MU_0);
    GwySIUnit *siunit = gwy_si_unit_new("A^2/m^3");

    gwy_data_field_resample(out, gwy_data_field_get_xres(in), gwy_data_field_get_yres(in), GWY_INTERPOLATION_NONE);

    gwy_data_field_copy(in, out, TRUE);
    gwy_data_field_multiply(out, factor);
    gwy_data_field_set_si_unit_z(out, siunit);
    g_object_unref(siunit);
}

G_GNUC_UNUSED
static void
get_force_gradient_from_frequency_shift(GwyDataField *in, GwyDataField *out, gdouble spring_constant, gdouble base_frequency)
{
    gdouble q = gwy_data_field_get_xres(in)*gwy_data_field_get_yres(in)/(gwy_data_field_get_xreal(in)*gwy_data_field_get_yreal(in));
    gdouble factor = 2*spring_constant*M_PI*q/(base_frequency*180*MU_0);
    GwySIUnit *siunit = gwy_si_unit_new("A^2/m^3");

    gwy_data_field_resample(out, gwy_data_field_get_xres(in), gwy_data_field_get_yres(in), GWY_INTERPOLATION_NONE);

    gwy_data_field_copy(in, out, TRUE);
    gwy_data_field_multiply(out, factor);
    gwy_data_field_set_si_unit_z(out, siunit);
    g_object_unref(siunit);
}

G_GNUC_UNUSED
static void
get_force_gradient_from_amplitude_shift(GwyDataField *in, GwyDataField *out, gdouble spring_constant, gdouble quality, gdouble base_amplitude)
{

    gdouble q = gwy_data_field_get_xres(in)*gwy_data_field_get_yres(in)/(gwy_data_field_get_xreal(in)*gwy_data_field_get_yreal(in));
    gdouble factor = 3*sqrt(3)*spring_constant*M_PI*q/(2*base_amplitude*quality*180*MU_0);
    GwySIUnit *siunit = gwy_si_unit_new("A^2/m^3");

    gwy_data_field_resample(out, gwy_data_field_get_xres(in), gwy_data_field_get_yres(in), GWY_INTERPOLATION_NONE);

    gwy_data_field_copy(in, out, TRUE);
    gwy_data_field_multiply(out, factor);
    gwy_data_field_set_si_unit_z(out, siunit);
    g_object_unref(siunit);

}


// the same code for volume data, this should not be here repeated twice !!!

G_GNUC_UNUSED
static void
get_volume_force_gradient_from_phase(GwyBrick *in, GwyBrick *out, gdouble spring_constant, gdouble quality)
{
    gdouble q = gwy_brick_get_xres(in)*gwy_brick_get_yres(in)/(gwy_brick_get_xreal(in)*gwy_brick_get_yreal(in));
    gdouble factor = spring_constant*M_PI*q/(quality*180*MU_0);
    GwySIUnit *siunit = gwy_si_unit_new("A^2/m^3");

    gwy_brick_resample(out, gwy_brick_get_xres(in), gwy_brick_get_yres(in), gwy_brick_get_zres(in), GWY_INTERPOLATION_NONE);

    gwy_brick_copy(in, out, TRUE);
    gwy_brick_multiply(out, factor);
    gwy_brick_set_si_unit_w(out, siunit);
    g_object_unref(siunit);
}

G_GNUC_UNUSED
static void
get_volume_force_gradient_from_frequency_shift(GwyBrick *in, GwyBrick *out, gdouble spring_constant, gdouble base_frequency)
{
    gdouble q = gwy_brick_get_xres(in)*gwy_brick_get_yres(in)/(gwy_brick_get_xreal(in)*gwy_brick_get_yreal(in));
    gdouble factor = 2*spring_constant*M_PI*q/(base_frequency*180*MU_0);
    GwySIUnit *siunit = gwy_si_unit_new("A^2/m^3");

    gwy_brick_resample(out, gwy_brick_get_xres(in), gwy_brick_get_yres(in), gwy_brick_get_zres(in), GWY_INTERPOLATION_NONE);

    gwy_brick_copy(in, out, TRUE);
    gwy_brick_multiply(out, factor);
    gwy_brick_set_si_unit_w(out, siunit);
    g_object_unref(siunit);
}

G_GNUC_UNUSED
static void
get_volume_force_gradient_from_amplitude_shift(GwyBrick *in, GwyBrick *out, gdouble spring_constant, gdouble quality, gdouble base_amplitude)
{

    gdouble q = gwy_brick_get_xres(in)*gwy_brick_get_yres(in)/(gwy_brick_get_xreal(in)*gwy_brick_get_yreal(in));
    gdouble factor = 3*sqrt(3)*spring_constant*M_PI*q/(2*base_amplitude*quality*180*MU_0);
    GwySIUnit *siunit = gwy_si_unit_new("A^2/m^3");

    gwy_brick_resample(out, gwy_brick_get_xres(in), gwy_brick_get_yres(in), gwy_brick_get_zres(in), GWY_INTERPOLATION_NONE);

    gwy_brick_copy(in, out, TRUE);
    gwy_brick_multiply(out, factor);
    gwy_brick_set_si_unit_w(out, siunit);
    g_object_unref(siunit);

}

#endif

/* vim: set cin et ts=4 sw=4 cino=>1s,e0,n0,f0,{0,}0,^0,\:1s,=0,g1s,h0,t0,+1s,c3,(0,u0 : */
