/*
 *  $Id$
 *  Copyright (C) 2018 David Necas (Yeti).
 *  E-mail: yeti@gwyddion.net.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA 02110-1301, USA.
 */

#include "config.h"
#include <string.h>
#include <stdlib.h>
#include <gtk/gtk.h>
#include <libgwyddion/gwymacros.h>
#include <libgwyddion/gwymath.h>
#include <libgwyddion/gwyrandgenset.h>
#include <libprocess/arithmetic.h>
#include <libprocess/elliptic.h>
#include <libprocess/grains.h>
#include <libprocess/stats.h>
#include <libprocess/filters.h>
#include <libgwymodule/gwymodule-process.h>
#include <libgwydgets/gwystock.h>
#include <app/gwyapp.h>
#include "preview.h"
#include "dimensions.h"

#define DISC_SYNTH_RUN_MODES (GWY_RUN_IMMEDIATE | GWY_RUN_INTERACTIVE)

enum {
    PAGE_DIMENSIONS = 0,
    PAGE_GENERATOR  = 1,
    PAGE_NPAGES
};

typedef enum {
    RNG_POSITION,
    RNG_RADIUS_INIT,
    RNG_HEIGHT,
    RNG_NRNGS
} ObjSynthRng;

typedef struct {
    gdouble x;
    gdouble y;
    gdouble r;
} Disc;

typedef struct {
    gint gno;
    gint col, row, width, height;  /* Bounding box. */
    Disc d;
} AvailableArea;

static const GwyGrainQuantity disc_quantities[] = {
    GWY_GRAIN_VALUE_INSCRIBED_DISC_X,
    GWY_GRAIN_VALUE_INSCRIBED_DISC_Y,
    GWY_GRAIN_VALUE_INSCRIBED_DISC_R,
};

enum {
    NDISCQUANT = G_N_ELEMENTS(disc_quantities)
};

typedef struct {
    gdouble *values[NDISCQUANT];
    gdouble *values_storage;
    gint *bboxes;
    guint ngrains;
    guint size;
} GatherAreasData;

typedef struct {
    gint active_page;
    gint seed;
    gboolean randomize;
    gboolean update;
    gdouble radius_init;
    gdouble radius_init_noise;
    gdouble radius_min;
    gdouble separation;
    gdouble height;
    gdouble height_noise;
    gdouble gap_thickness;
    gboolean make_tiles;
    gboolean apply_opening;
    gint opening_size;
} DiscSynthArgs;

typedef struct {
    DiscSynthArgs *args;
    GwyDimensions *dims;
    GtkWidget *dialog;
    GtkWidget *view;
    GtkWidget *update;
    GtkWidget *update_now;
    GtkObject *seed;
    GtkTable *table;
    GtkWidget *randomize;
    GtkObject *radius_init;
    GtkWidget *radius_init_value;
    GtkWidget *radius_init_units;
    GtkObject *radius_init_noise;
    GtkObject *radius_min;
    GtkWidget *radius_min_value;
    GtkWidget *radius_min_units;
    GtkObject *separation;
    GtkWidget *separation_value;
    GtkWidget *separation_units;
    GtkObject *height;
    GtkWidget *height_units;
    GtkWidget *height_init;
    GtkObject *height_noise;
    GtkWidget *make_tiles;
    GtkObject *gap_thickness;
    GtkWidget *gap_thickness_value;
    GtkWidget *gap_thickness_units;
    GtkWidget *apply_opening;
    GtkObject *opening_size;
    GwyContainer *mydata;
    GwyDataField *surface;
    gdouble pxsize;
    gdouble zscale;
    gboolean in_init;
    gulong sid;
} DiscSynthControls;

static gboolean module_register      (void);
static void     disc_synth           (GwyContainer *data,
                                      GwyRunType run);
static void     run_noninteractive   (DiscSynthArgs *args,
                                      const GwyDimensionArgs *dimsargs,
                                      GwyContainer *data,
                                      GwyDataField *dfield,
                                      gint oldid,
                                      GQuark quark);
static gboolean disc_synth_dialog    (DiscSynthArgs *args,
                                      GwyDimensionArgs *dimsargs,
                                      GwyContainer *data,
                                      GwyDataField *dfield,
                                      gint id);
static void     update_controls      (DiscSynthControls *controls,
                                      DiscSynthArgs *args);
static void     page_switched        (DiscSynthControls *controls,
                                      GtkNotebookPage *page,
                                      gint pagenum);
static void     update_values        (DiscSynthControls *controls);
static void     update_sensitivity   (DiscSynthControls *controls);
static void     height_init_clicked  (DiscSynthControls *controls);
static void     disc_synth_invalidate(DiscSynthControls *controls);
static gboolean preview_gsource      (gpointer user_data);
static void     preview              (DiscSynthControls *controls);
static gboolean disc_synth_do        (const DiscSynthArgs *args,
                                      const GwyDimensionArgs *dimsargs,
                                      GwyDataField *dfield,
                                      GwySetMessageFunc set_message,
                                      GwySetFractionFunc set_fraction);
static void     disc_synth_load_args (GwyContainer *container,
                                      DiscSynthArgs *args,
                                      GwyDimensionArgs *dimsargs);
static void     disc_synth_save_args (GwyContainer *container,
                                      const DiscSynthArgs *args,
                                      const GwyDimensionArgs *dimsargs);

#define GWY_SYNTH_CONTROLS DiscSynthControls
#define GWY_SYNTH_INVALIDATE(controls) \
    disc_synth_invalidate(controls)

#include "synth.h"

static const DiscSynthArgs disc_synth_defaults = {
    PAGE_DIMENSIONS,
    42, TRUE, TRUE,
    30.0, 0.0, 12.0, 3.0,
    1.0, 0.5,
    3, TRUE,
    FALSE, 20,
};

static const GwyDimensionArgs dims_defaults = GWY_DIMENSION_ARGS_INIT;

static GwyModuleInfo module_info = {
    GWY_MODULE_ABI_VERSION,
    &module_register,
    N_("Generates random more or less touching discs."),
    "Yeti <yeti@gwyddion.net>",
    "1.0",
    "David Nečas (Yeti)",
    "2018",
};

GWY_MODULE_QUERY2(module_info, disc_synth)

static gboolean
module_register(void)
{
    gwy_process_func_register("disc_synth",
                              (GwyProcessFunc)&disc_synth,
                              N_("/S_ynthetic/D_iscs..."),
                              GWY_STOCK_SYNTHETIC_DISCS,
                              DISC_SYNTH_RUN_MODES,
                              0,
                              N_("Generate surface of random discs"));

    return TRUE;
}

static void
disc_synth(GwyContainer *data, GwyRunType run)
{
    DiscSynthArgs args;
    GwyDimensionArgs dimsargs;
    GwyDataField *dfield;
    GQuark quark;
    gint id;

    g_return_if_fail(run & DISC_SYNTH_RUN_MODES);
    disc_synth_load_args(gwy_app_settings_get(), &args, &dimsargs);
    gwy_app_data_browser_get_current(GWY_APP_DATA_FIELD, &dfield,
                                     GWY_APP_DATA_FIELD_ID, &id,
                                     GWY_APP_DATA_FIELD_KEY, &quark,
                                     0);

    if (run == GWY_RUN_IMMEDIATE
        || disc_synth_dialog(&args, &dimsargs, data, dfield, id))
        run_noninteractive(&args, &dimsargs, data, dfield, id, quark);

    if (run == GWY_RUN_INTERACTIVE)
        disc_synth_save_args(gwy_app_settings_get(), &args, &dimsargs);

    gwy_dimensions_free_args(&dimsargs);
}

static gboolean
disc_synth_dialog(DiscSynthArgs *args,
                   GwyDimensionArgs *dimsargs,
                   GwyContainer *data,
                   GwyDataField *dfield_template,
                   gint id)
{
    GtkWidget *dialog, *table, *vbox, *hbox, *notebook;
    DiscSynthControls controls;
    GwyDataField *dfield;
    gboolean finished;
    gint response;
    gint row;

    gwy_clear(&controls, 1);
    controls.in_init = TRUE;
    controls.args = args;
    dialog = gtk_dialog_new_with_buttons(_("Random Discs"),
                                         NULL, 0,
                                         _("_Reset"), RESPONSE_RESET,
                                         GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
                                         GTK_STOCK_OK, GTK_RESPONSE_OK,
                                         NULL);
    gtk_dialog_set_default_response(GTK_DIALOG(dialog), GTK_RESPONSE_OK);
    gwy_help_add_to_proc_dialog(GTK_DIALOG(dialog), GWY_HELP_DEFAULT);
    controls.dialog = dialog;

    hbox = gtk_hbox_new(FALSE, 2);
    gtk_box_pack_start(GTK_BOX(GTK_DIALOG(dialog)->vbox), hbox,
                       FALSE, FALSE, 4);

    vbox = gtk_vbox_new(FALSE, 4);
    gtk_box_pack_start(GTK_BOX(hbox), vbox, FALSE, FALSE, 4);

    controls.mydata = gwy_container_new();
    dfield = gwy_data_field_new(PREVIEW_SIZE, PREVIEW_SIZE,
                                dimsargs->measure*PREVIEW_SIZE,
                                dimsargs->measure*PREVIEW_SIZE,
                                TRUE);
    gwy_container_set_object_by_name(controls.mydata, "/0/data", dfield);
    if (dfield_template) {
        gwy_app_sync_data_items(data, controls.mydata, id, 0, FALSE,
                                GWY_DATA_ITEM_PALETTE,
                                0);
        controls.surface = gwy_synth_surface_for_preview(dfield_template,
                                                         PREVIEW_SIZE);
        controls.zscale = gwy_data_field_get_rms(dfield_template);
    }
    controls.view = create_preview(controls.mydata, 0, PREVIEW_SIZE, FALSE);
    gtk_box_pack_start(GTK_BOX(vbox), controls.view, FALSE, FALSE, 0);

    gtk_box_pack_start(GTK_BOX(vbox),
                       gwy_synth_instant_updates_new(&controls,
                                                     &controls.update_now,
                                                     &controls.update,
                                                     &args->update),
                       FALSE, FALSE, 0);
    g_signal_connect_swapped(controls.update_now, "clicked",
                             G_CALLBACK(preview), &controls);

    gtk_box_pack_start(GTK_BOX(vbox),
                       gwy_synth_random_seed_new(&controls,
                                                 &controls.seed, &args->seed),
                       FALSE, FALSE, 0);

    controls.randomize = gwy_synth_randomize_new(&args->randomize);
    gtk_box_pack_start(GTK_BOX(vbox), controls.randomize, FALSE, FALSE, 0);

    notebook = gtk_notebook_new();
    gtk_box_pack_start(GTK_BOX(hbox), notebook, TRUE, TRUE, 4);
    g_signal_connect_swapped(notebook, "switch-page",
                             G_CALLBACK(page_switched), &controls);

    controls.dims = gwy_dimensions_new(dimsargs, dfield_template);
    gtk_notebook_append_page(GTK_NOTEBOOK(notebook),
                             gwy_dimensions_get_widget(controls.dims),
                             gtk_label_new(_("Dimensions")));
    if (controls.dims->add)
        g_signal_connect_swapped(controls.dims->add, "toggled",
                                 G_CALLBACK(disc_synth_invalidate), &controls);

    table = gtk_table_new(17 + (dfield_template ? 1 : 0), 3, FALSE);
    controls.table = GTK_TABLE(table);
    gtk_table_set_row_spacings(controls.table, 2);
    gtk_table_set_col_spacings(controls.table, 6);
    gtk_container_set_border_width(GTK_CONTAINER(table), 4);
    gtk_notebook_append_page(GTK_NOTEBOOK(notebook), table,
                             gtk_label_new(_("Generator")));
    row = 0;

    gtk_table_attach(GTK_TABLE(table), gwy_label_new_header(_("Discs")),
                     0, 2, row, row+1, GTK_FILL, 0, 0, 0);
    row++;

    controls.radius_init = gtk_adjustment_new(args->radius_init, 5.0, 1000.0,
                                              0.1, 10.0, 0);
    row = gwy_synth_attach_lateral(&controls, row,
                                   controls.radius_init, &args->radius_init,
                                   _("Starting _radius:"), GWY_HSCALE_LOG,
                                   NULL,
                                   &controls.radius_init_value,
                                   &controls.radius_init_units);
    row = gwy_synth_attach_variance(&controls, row,
                                    &controls.radius_init_noise,
                                    &args->radius_init_noise);

    controls.radius_min = gtk_adjustment_new(args->radius_min, 3.0, 1000.0,
                                             0.1, 10.0, 0);
    row = gwy_synth_attach_lateral(&controls, row,
                                   controls.radius_min, &args->radius_min,
                                   _("_Minimum radius:"), GWY_HSCALE_LOG,
                                   NULL,
                                   &controls.radius_min_value,
                                   &controls.radius_min_units);

    controls.separation = gtk_adjustment_new(args->separation, 3.0, 120.0,
                                             0.1, 10.0, 0);
    row = gwy_synth_attach_lateral(&controls, row,
                                   controls.separation, &args->separation,
                                   _("_Separation:"), GWY_HSCALE_LOG,
                                   NULL,
                                   &controls.separation_value,
                                   &controls.separation_units);

    gtk_table_set_row_spacing(GTK_TABLE(table), row-1, 8);
    gtk_table_attach(GTK_TABLE(table), gwy_label_new_header(_("Tiles")),
                     0, 2, row, row+1, GTK_FILL, 0, 0, 0);
    row++;

    controls.make_tiles
        = gtk_check_button_new_with_mnemonic(_("_Transform to tiles"));
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(controls.make_tiles),
                                 args->make_tiles);
    g_object_set_data(G_OBJECT(controls.make_tiles),
                      "target", &args->make_tiles);
    gtk_table_attach(GTK_TABLE(table), controls.make_tiles,
                     0, 2, row, row+1, GTK_FILL, 0, 0, 0);
    g_signal_connect_swapped(controls.make_tiles, "toggled",
                             G_CALLBACK(gwy_synth_boolean_changed), &controls);
    g_signal_connect_swapped(controls.make_tiles, "toggled",
                             G_CALLBACK(update_sensitivity), &controls);
    row++;

    controls.gap_thickness = gtk_adjustment_new(args->gap_thickness,
                                                 1.0, 250.0, 0.1, 10.0, 0);
    row = gwy_synth_attach_lateral(&controls, row,
                                   controls.gap_thickness,
                                   &args->gap_thickness,
                                   _("_Gap thickness:"), GWY_HSCALE_LOG,
                                   NULL,
                                   &controls.gap_thickness_value,
                                   &controls.gap_thickness_units);

    gtk_table_set_row_spacing(GTK_TABLE(table), row-1, 8);
    controls.apply_opening
        = gtk_check_button_new_with_mnemonic(_("Apply opening _filter"));
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(controls.apply_opening),
                                 args->apply_opening);
    g_object_set_data(G_OBJECT(controls.apply_opening),
                      "target", &args->apply_opening);
    gtk_table_attach(GTK_TABLE(table), controls.apply_opening,
                     0, 2, row, row+1, GTK_FILL, 0, 0, 0);
    g_signal_connect_swapped(controls.apply_opening, "toggled",
                             G_CALLBACK(gwy_synth_boolean_changed), &controls);
    g_signal_connect_swapped(controls.apply_opening, "toggled",
                             G_CALLBACK(update_sensitivity), &controls);
    row++;

    controls.opening_size = gtk_adjustment_new(args->opening_size,
                                               1.0, 250.0, 1.0, 10.0, 0);
    g_object_set_data(G_OBJECT(controls.opening_size), "target",
                      &args->opening_size);
    gwy_table_attach_adjbar(table, row, _("Si_ze:"), _("px"),
                            GTK_OBJECT(controls.opening_size),
                            GWY_HSCALE_LOG | GWY_HSCALE_SNAP);
    g_signal_connect_swapped(controls.opening_size, "value-changed",
                             G_CALLBACK(gwy_synth_int_changed), &controls);
    row++;

    gtk_table_set_row_spacing(GTK_TABLE(table), row-1, 8);
    gtk_table_attach(GTK_TABLE(table), gwy_label_new_header(_("Height")),
                     0, 2, row, row+1, GTK_FILL, 0, 0, 0);
    row++;

    row = gwy_synth_attach_height(&controls, row,
                                  &controls.height, &args->height,
                                  _("_Height:"), NULL, &controls.height_units);
    row = gwy_synth_attach_variance(&controls, row,
                                    &controls.height_noise,
                                    &args->height_noise);

    if (dfield_template) {
        controls.height_init
            = gtk_button_new_with_mnemonic(_("_Like Current Image"));
        g_signal_connect_swapped(controls.height_init, "clicked",
                                 G_CALLBACK(height_init_clicked), &controls);
        gtk_table_attach(GTK_TABLE(table), controls.height_init,
                         0, 2, row, row+1, GTK_FILL, 0, 0, 0);
        row++;
    }

    gtk_widget_show_all(dialog);
    controls.in_init = FALSE;
    /* Must be done when widgets are shown, see GtkNotebook docs */
    gtk_notebook_set_current_page(GTK_NOTEBOOK(notebook), args->active_page);
    update_values(&controls);
    update_sensitivity(&controls);
    disc_synth_invalidate(&controls);

    finished = FALSE;
    while (!finished) {
        response = gtk_dialog_run(GTK_DIALOG(dialog));
        switch (response) {
            case GTK_RESPONSE_CANCEL:
            case GTK_RESPONSE_DELETE_EVENT:
            case GTK_RESPONSE_OK:
            gtk_widget_destroy(dialog);
            case GTK_RESPONSE_NONE:
            finished = TRUE;
            break;

            case RESPONSE_RESET:
            {
                gboolean temp = args->update;
                gint temp2 = args->active_page;
                *args = disc_synth_defaults;
                args->active_page = temp2;
                args->update = temp;
            }
            controls.in_init = TRUE;
            update_controls(&controls, args);
            controls.in_init = FALSE;
            if (args->update)
                preview(&controls);
            break;

            default:
            g_assert_not_reached();
            break;
        }
    }

    if (controls.sid) {
        g_source_remove(controls.sid);
        controls.sid = 0;
    }
    g_object_unref(controls.mydata);
    GWY_OBJECT_UNREF(controls.surface);
    gwy_dimensions_free(controls.dims);

    return response == GTK_RESPONSE_OK;
}

static void
update_controls(DiscSynthControls *controls,
                DiscSynthArgs *args)
{
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(controls->update),
                                 args->update);
    gtk_adjustment_set_value(GTK_ADJUSTMENT(controls->seed), args->seed);
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(controls->randomize),
                                 args->randomize);
    gtk_adjustment_set_value(GTK_ADJUSTMENT(controls->height), args->height);
    gtk_adjustment_set_value(GTK_ADJUSTMENT(controls->radius_init),
                             args->radius_init);
    gtk_adjustment_set_value(GTK_ADJUSTMENT(controls->radius_init_noise),
                             args->radius_init_noise);
    gtk_adjustment_set_value(GTK_ADJUSTMENT(controls->radius_min),
                             args->radius_min);
    gtk_adjustment_set_value(GTK_ADJUSTMENT(controls->separation),
                             args->separation);
    gtk_adjustment_set_value(GTK_ADJUSTMENT(controls->gap_thickness),
                             args->gap_thickness);
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(controls->make_tiles),
                                 args->make_tiles);
    gtk_adjustment_set_value(GTK_ADJUSTMENT(controls->opening_size),
                             args->opening_size);
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(controls->apply_opening),
                                 args->apply_opening);
}

static void
page_switched(DiscSynthControls *controls,
              G_GNUC_UNUSED GtkNotebookPage *page,
              gint pagenum)
{
    if (controls->in_init)
        return;

    controls->args->active_page = pagenum;

    if (pagenum == PAGE_GENERATOR)
        update_values(controls);
}

static void
update_values(DiscSynthControls *controls)
{
    GwyDimensions *dims = controls->dims;

    controls->pxsize = dims->args->measure * pow10(dims->args->xypow10);
    if (controls->height_units) {
        gtk_label_set_markup(GTK_LABEL(controls->height_units),
                             dims->zvf->units);
    }
    gtk_label_set_markup(GTK_LABEL(controls->radius_init_units),
                         dims->xyvf->units);
    gtk_label_set_markup(GTK_LABEL(controls->radius_min_units),
                         dims->xyvf->units);
    gtk_label_set_markup(GTK_LABEL(controls->separation_units),
                         dims->xyvf->units);
    gtk_label_set_markup(GTK_LABEL(controls->gap_thickness_units),
                         dims->xyvf->units);

    gwy_synth_update_lateral(controls, GTK_ADJUSTMENT(controls->radius_init));
    gwy_synth_update_lateral(controls, GTK_ADJUSTMENT(controls->radius_min));
    gwy_synth_update_lateral(controls, GTK_ADJUSTMENT(controls->separation));
    gwy_synth_update_lateral(controls, GTK_ADJUSTMENT(controls->gap_thickness));
}

static void
update_sensitivity(DiscSynthControls *controls)
{
    gboolean is_tiles = controls->args->make_tiles;
    gboolean is_open = controls->args->apply_opening && is_tiles;

    gwy_table_hscale_set_sensitive(controls->gap_thickness, is_tiles);
    gtk_widget_set_sensitive(controls->apply_opening, is_tiles);
    gwy_table_hscale_set_sensitive(controls->opening_size, is_open);
}

static void
height_init_clicked(DiscSynthControls *controls)
{
    gdouble mag = pow10(controls->dims->args->zpow10);
    gtk_adjustment_set_value(GTK_ADJUSTMENT(controls->height),
                             controls->zscale/mag);
}

static void
disc_synth_invalidate(DiscSynthControls *controls)
{
    /* create preview if instant updates are on */
    if (controls->args->update && !controls->in_init && !controls->sid) {
        controls->sid = g_idle_add_full(G_PRIORITY_LOW, preview_gsource,
                                        controls, NULL);
    }
}

static gboolean
preview_gsource(gpointer user_data)
{
    DiscSynthControls *controls = (DiscSynthControls*)user_data;
    controls->sid = 0;

    preview(controls);

    return FALSE;
}

static void
preview(DiscSynthControls *controls)
{
    DiscSynthArgs *args = controls->args;
    GwyDataField *dfield;
    gboolean ok;

    dfield = GWY_DATA_FIELD(gwy_container_get_object_by_name(controls->mydata,
                                                             "/0/data"));
    if (controls->dims->args->add && controls->surface)
        gwy_data_field_copy(controls->surface, dfield, FALSE);
    else
        gwy_data_field_clear(dfield);

    if (args->update) {
        gwy_app_wait_cursor_start(GTK_WINDOW(controls->dialog));
        disc_synth_do(args, controls->dims->args, dfield, NULL, NULL);
        gwy_app_wait_cursor_finish(GTK_WINDOW(controls->dialog));
        gwy_data_field_data_changed(dfield);
        return;
    }

    gwy_app_wait_start(GTK_WINDOW(controls->dialog), _("Initializing..."));
    ok = disc_synth_do(args, controls->dims->args, dfield,
                       gwy_app_wait_set_message, gwy_app_wait_set_fraction);
    gwy_app_wait_finish();

    if (ok)
        gwy_data_field_data_changed(dfield);
}

static void
run_noninteractive(DiscSynthArgs *args,
                   const GwyDimensionArgs *dimsargs,
                   GwyContainer *data,
                   GwyDataField *dfield,
                   gint oldid,
                   GQuark quark)
{
    GwySIUnit *siunit;
    gboolean replace = dimsargs->replace && dfield;
    gboolean add = dimsargs->add && dfield;
    gboolean ok;
    gint newid;

    if (args->randomize)
        args->seed = g_random_int() & 0x7fffffff;

    if (replace) {
        /* Always take a reference so that we can always unref. */
        g_object_ref(dfield);

        gwy_app_undo_qcheckpointv(data, 1, &quark);
        if (!add)
            gwy_data_field_clear(dfield);

        gwy_app_channel_log_add_proc(data, oldid, oldid);
    }
    else {
        if (add)
            dfield = gwy_data_field_duplicate(dfield);
        else {
            gdouble mag = pow10(dimsargs->xypow10) * dimsargs->measure;
            dfield = gwy_data_field_new(dimsargs->xres, dimsargs->yres,
                                        mag*dimsargs->xres, mag*dimsargs->yres,
                                        TRUE);

            siunit = gwy_data_field_get_si_unit_xy(dfield);
            gwy_si_unit_set_from_string(siunit, dimsargs->xyunits);

            siunit = gwy_data_field_get_si_unit_z(dfield);
            gwy_si_unit_set_from_string(siunit, dimsargs->zunits);
        }
    }

    gwy_app_wait_start(gwy_app_find_window_for_channel(data, oldid),
                       _("Initializing..."));
    /* disc_synth_do() does not change the datafield unless it returns TRUE. */
    ok = disc_synth_do(args, dimsargs, dfield,
                       gwy_app_wait_set_message, gwy_app_wait_set_fraction);
    gwy_app_wait_finish();

    if (!ok) {
        g_object_unref(dfield);
        return;
    }

    if (replace)
        gwy_data_field_data_changed(dfield);
    else {
        if (data) {
            newid = gwy_app_data_browser_add_data_field(dfield, data, TRUE);
            if (oldid != -1)
                gwy_app_sync_data_items(data, data, oldid, newid, FALSE,
                                        GWY_DATA_ITEM_GRADIENT,
                                        0);
        }
        else {
            newid = 0;
            data = gwy_container_new();
            gwy_container_set_object(data, gwy_app_get_data_key_for_id(newid),
                                     dfield);
            gwy_app_data_browser_add(data);
            gwy_app_data_browser_reset_visibility(data,
                                                  GWY_VISIBILITY_RESET_SHOW_ALL);
            g_object_unref(data);
        }

        gwy_app_set_data_field_title(data, newid, _("Generated"));
        gwy_app_channel_log_add_proc(data, add ? oldid : -1, newid);
    }
    g_object_unref(dfield);
}

/* XXX: Fake resize function, only correct when we know the allocated size
 * is sufficient! */
static void
data_field_resize(GwyDataField *data_field,
                  gint xres, gint yres)
{
    data_field->xres = xres;
    data_field->yres = yres;
}

static void
circular_area_fill_periodic(GwyDataField *data_field,
                            gint col, gint row,
                            gdouble radius,
                            gdouble value)
{
    gint i, j, ii, jj, r, r2, xres, yres, jfrom, jto;
    gdouble *d, *drow;
    gdouble s;

    if (radius < 0.0)
        return;

    r2 = floor(radius*radius + 1e-12);
    r = floor(radius + 1e-12);
    xres = data_field->xres;
    yres = data_field->yres;
    d = data_field->data;

    /* Ensure circle centre inside the base area. */
    col %= xres;
    if (col < 0)
        col += xres;

    row %= yres;
    if (row < 0)
        row += yres;

    for (i = -r; i <= r; i++) {
        ii = (i + row + yres) % yres;
        drow = d + ii*xres;

        s = sqrt(r2 - i*i);
        jfrom = col + ceil(-s);
        jto = col + floor(s);
        /* If we are to fill the entire row just do it simply.  As a benefit,
         * we can assume we fill less than one entire row in the other cases. */
        if (jto+1 - jfrom >= xres) {
            for (j = xres; j; j--, drow++)
                *drow = value;
        }
        /* Sticking outside to the left. */
        else if (jfrom < 0) {
            for (j = 0; j <= jto; j++)
                drow[j] = value;
            jj = (jfrom + xres) % xres;
            for (j = jj; j < xres; j++)
                drow[j] = value;
        }
        /* Sticking outside to the right. */
        else if (jto >= xres) {
            jj = (jto + 1) % xres;
            for (j = 0; j < jj; j++)
                drow[j] = value;
            for (j = jfrom; j < xres; j++)
                drow[j] = value;
        }
        /* Entirely inside. */
        else {
            for (j = jfrom; j <= jto; j++)
                drow[j] = value;
        }
    }

    gwy_data_field_invalidate(data_field);
}

static void
generate_seed_discs(const DiscSynthArgs *args,
                    GwyDataField *dfield,
                    GArray *discs,
                    GwyRandGenSet *rngset)
{
    gint i, xres, yres, failcount;
    gdouble separ = args->separation;
    GRand *rng;

    rng = gwy_rand_gen_set_rng(rngset, RNG_POSITION);

    xres = gwy_data_field_get_xres(dfield);
    yres = gwy_data_field_get_xres(dfield);
    gwy_data_field_fill(dfield, 1.0);

    failcount = 0;
    while (failcount < 15) {
        Disc d;

        d.x = g_rand_double(rng)*xres;
        d.y = g_rand_double(rng)*yres;
        d.r = args->radius_init;
        if (args->radius_init_noise) {
            d.r *= exp(gwy_rand_gen_set_gaussian(rngset, RNG_RADIUS_INIT,
                                                 args->radius_init_noise));
        }

        for (i = 0; i < discs->len; i++) {
            const Disc *p = &g_array_index(discs, Disc, i);
            gdouble dx, dy, s;

            if (d.x <= p->x)
                dx = MIN(p->x - d.x, d.x + xres - p->x);
            else
                dx = MIN(d.x - p->x, p->x + xres - d.x);

            if (d.y <= p->y)
                dy = MIN(p->y - d.y, d.y + yres - p->y);
            else
                dy = MIN(d.y - p->y, p->y + yres - d.y);

            s = d.r + p->r + separ;
            if (dx*dx + dy*dy <= s*s)
                break;
        }
        if (i == discs->len) {
            g_array_append_val(discs, d);
            failcount = 0;
        }
        else
            failcount++;
    }

    for (i = 0; i < discs->len; i++) {
        Disc *p = &g_array_index(discs, Disc, i);

        circular_area_fill_periodic(dfield, p->x, p->y,
                                    p->r + 0.5*separ, 0.0);
    }
}

static void
gather_areas_data_ensure(GatherAreasData *gdata, guint ngrains)
{
    guint i;

    if (ngrains+1 > gdata->size) {
        gdata->size = ngrains+1;
        gdata->bboxes = g_renew(gint, gdata->bboxes, 4*gdata->size);
        gdata->values_storage = g_renew(gdouble, gdata->values_storage,
                                        NDISCQUANT*gdata->size);
    }
    gdata->ngrains = ngrains;
    for (i = 0; i < NDISCQUANT; i++)
        gdata->values[i] = gdata->values_storage + i*(ngrains + 1);
}

static void
gather_areas_data_free(GatherAreasData *gdata)
{
    g_free(gdata->bboxes);
    g_free(gdata->values_storage);
}

static void
append_available_areas(const GatherAreasData *gdata, gdouble minr,
                       GArray *areas)
{
    gdouble *values[NDISCQUANT];
    gint *bboxes;
    guint i, ngrains;

    ngrains = gdata->ngrains;
    bboxes = gdata->bboxes;
    for (i = 0; i < NDISCQUANT; i++)
        values[i] = gdata->values[i];

    for (i = 1; i <= ngrains; i++) {
        AvailableArea area;

        if (values[2][i] < minr)
            continue;

        area.gno = i;
        area.col = bboxes[4*i + 0];
        area.row = bboxes[4*i + 1];
        area.width = bboxes[4*i + 2];
        area.height = bboxes[4*i + 3];
        area.d.x = values[0][i];
        area.d.y = values[1][i];
        area.d.r = values[2][i];
        g_array_append_val(areas, area);
    }
}

static void
gather_available_areas_nonperiodic(GwyDataField *dfield,
                                   GArray *areas,
                                   gdouble minr,
                                   const gint *grains,
                                   GatherAreasData *gdata,
                                   gint ngrains)
{
    gather_areas_data_ensure(gdata, ngrains);
    gwy_data_field_get_grain_bounding_boxes(dfield, ngrains, grains,
                                            gdata->bboxes);
    gwy_data_field_grains_get_quantities(dfield, gdata->values,
                                         disc_quantities, NDISCQUANT,
                                         ngrains, grains);
    append_available_areas(gdata, minr, areas);
}

/* Shift data field, renumber grains to @gshifted and find inscribed discs.
 * Those that are larger than discs currently in @gdata values are used to
 * replace the smaller ones. */
static void
update_areas_for_shifted(GwyDataField *dfield,
                         GwyDataField *shifted,
                         gdouble minr,
                         const gint *grains,
                         gint *gshifted,
                         GatherAreasData *gdata,
                         GatherAreasData *gsdata,
                         gint xshift,
                         gint yshift)
{
    gdouble *values[NDISCQUANT], *svalues[NDISCQUANT];
    gint xres, yres, ngs, i, j, gno, col, row;

    xres = dfield->xres;
    yres = dfield->yres;

    if (xshift && yshift) {
        gwy_data_field_area_copy(dfield, shifted,
                                 0, 0, xres - xshift, yres - yshift,
                                 xshift, yshift);
        gwy_data_field_area_copy(dfield, shifted,
                                 xres - xshift, 0, xshift, yres - yshift,
                                 0, yshift);
        gwy_data_field_area_copy(dfield, shifted,
                                 0, yres - yshift, xres - xshift, yshift,
                                 xshift, 0);
        gwy_data_field_area_copy(dfield, shifted,
                                 xres - xshift, yres - yshift, xshift, yshift,
                                 0, 0);
    }
    else if (xshift) {
        gwy_data_field_area_copy(dfield, shifted,
                                 0, 0, xres - xshift, yres, xshift, 0);
        gwy_data_field_area_copy(dfield, shifted,
                                 xres - xshift, 0, xshift, yres, 0, 0);
    }
    else if (yshift) {
        gwy_data_field_area_copy(dfield, shifted,
                                 0, 0, xres, yres - yshift, 0, yshift);
        gwy_data_field_area_copy(dfield, shifted,
                                 0, yres - yshift, xres, yshift, 0, 0);
    }
    else {
        shifted = dfield;
    }

    gwy_clear(gshifted, xres*yres);
    ngs = gwy_data_field_number_grains(shifted, gshifted);
    gather_areas_data_ensure(gsdata, ngs);
    gwy_data_field_grains_get_quantities(shifted, gsdata->values,
                                         disc_quantities, NDISCQUANT,
                                         ngs, gshifted);

    for (i = 0; i < NDISCQUANT; i++) {
        values[i] = gdata->values[i];
        svalues[i] = gsdata->values[i];
    }
    for (i = 1; i <= ngs; i++) {
        if (svalues[2][i] < minr)
            continue;

        /* Get the grain number in the corresponding periodic numbering. */
        svalues[0][i] -= xshift;
        svalues[1][i] -= yshift;
        col = ((gint)floor(svalues[0][i]) + xres) % xres;
        row = ((gint)floor(svalues[1][i]) + yres) % yres;
        gno = grains[row*xres + col];
        /* Update if better radius is found. */
        if (svalues[2][i] > values[2][gno]) {
            for (j = 0; j < NDISCQUANT; j++)
                values[j][gno] = svalues[j][i];
        }
    }
}

static void
gather_available_areas(GwyDataField *dfield,
                       GwyDataField *shifted,
                       GArray *areas,
                       gdouble minr,
                       const gint *grains,
                       gint *gshifted,
                       GatherAreasData *gdata,
                       GatherAreasData *gsdata,
                       gint ngrains,
                       gboolean is_periodic_x,
                       gboolean is_periodic_y)
{
    guint i;
    gint xres, yres;

    gather_areas_data_ensure(gdata, ngrains);
    gwy_data_field_get_grain_bounding_boxes_periodic(dfield,
                                                     ngrains, grains,
                                                     gdata->bboxes);
    /* Init the disc radii for all grains to zero. */
    for (i = 1; i <= ngrains; i++)
        gdata->values[2][i] = 0.0;

    xres = dfield->xres;
    yres = dfield->yres;
    data_field_resize(shifted, xres, yres);
    gwy_data_field_set_xreal(shifted, 1.0*xres);
    gwy_data_field_set_yreal(shifted, 1.0*yres);

    update_areas_for_shifted(dfield, shifted, minr, grains, gshifted,
                             gdata, gsdata, 0, 0);
    if (is_periodic_x) {
        update_areas_for_shifted(dfield, shifted, minr, grains, gshifted,
                                 gdata, gsdata, xres/2, 0);
    }
    if (is_periodic_y) {
        update_areas_for_shifted(dfield, shifted, minr, grains, gshifted,
                                 gdata, gsdata, 0, yres/2);
    }
    if (is_periodic_x && is_periodic_y) {
        update_areas_for_shifted(dfield, shifted, minr, grains, gshifted,
                                 gdata, gsdata, xres/2, yres/2);
    }

    append_available_areas(gdata, minr, areas);
}

static void
process_area(GwyDataField *dfield,
             const AvailableArea *area,
             gint *grains,
             gdouble separ,
             GwyDataField *workspace)
{
    gint xres, yres, width, height, i, j, ii, jj, gno, col, row;
    gdouble *d, *drow, *w, *wrow;
    gint *grow;

    xres = dfield->xres;
    yres = dfield->yres;
    width = area->width;
    height = area->height;
    col = area->col;
    row = area->row;
    gno = area->gno;

    /* First clear the area in @dfield (the result).
     * In principle, this can eat some pixels of other grains.  It should
     * not because of the separation, but...  */
    circular_area_fill_periodic(dfield, floor(area->d.x), floor(area->d.y),
                                area->d.r + 0.5*separ, 0.0);

    /* Then extract the former area to @workspace, clearing it also there. */
    data_field_resize(workspace, width, height);
    gwy_data_field_set_xreal(workspace, 1.0*width);
    gwy_data_field_set_yreal(workspace, 1.0*height);

    d = gwy_data_field_get_data(dfield);
    w = gwy_data_field_get_data(workspace);
    for (i = 0; i < height; i++) {
        wrow = w + i*width;
        ii = (i + row) % yres;
        drow = d + ii*xres + col;
        grow = grains + ii*xres + col;
        jj = MIN(xres, col + width) - col;
        /* Non-periodic row part. */
        for (j = 0; j < jj; j++) {
            if (drow[j] > 0.0)
                wrow[j] = (grow[j] == gno);
            else {
                grow[j] = 0.0;
                wrow[j] = 0.0;
            }
        }
        /* Possibly periodic row part. */
        drow = d + ii*xres + col-xres;
        grow = grains + ii*xres + col-xres;
        for (j = jj; j < width; j++) {
            if (drow[j] > 0.0)
                wrow[j] = (grow[j] == gno);
            else {
                grow[j] = 0.0;
                wrow[j] = 0.0;
            }
        }
    }
}

static void
reintegrate_split_grains(GwyDataField *dfield,
                         const AvailableArea *area,
                         gint *grains,
                         const gint *gtmp,
                         gint gnomax)
{
    gint xres, yres, width, height, i, j, ii, jj, col, row;
    const gint *gtrow;
    gint *grow;

    xres = dfield->xres;
    yres = dfield->yres;
    width = area->width;
    height = area->height;
    col = area->col;
    row = area->row;

    for (i = 0; i < height; i++) {
        gtrow = gtmp + i*width;
        ii = (i + row) % yres;
        /* Non-periodic row part. */
        grow = grains + ii*xres + col;
        jj = MIN(xres, col + width) - col;
        for (j = 0; j < jj; j++) {
            if (gtrow[j])
               grow[j] = gtrow[j] + gnomax;
        }
        /* Possibly periodic row part. */
        grow = grains + ii*xres + col-xres;
        for (j = jj; j < width; j++) {
            if (gtrow[j])
               grow[j] = gtrow[j] + gnomax;
        }
    }
}

static gboolean
disc_synth_do(const DiscSynthArgs *args,
              const GwyDimensionArgs *dimsargs,
              GwyDataField *dfield,
              GwySetMessageFunc set_message,
              GwySetFractionFunc set_fraction)
{
    GwyDataField *discfield, *workspace, *shifted;
    guint xres, yres, ext, apos, i, n;
    gdouble z, height_base = args->height * pow10(dimsargs->zpow10);
    GatherAreasData gdata, gsdata;
    gint ng, ngrains, gnomax;
    GwyRandGenSet *rngset;
    GArray *areas, *discs;
    gint *grains, *gtmp, *gshifted;
    gdouble *heights, *d;
    gdouble minr = args->radius_min;
    gdouble separ = args->separation;
    gdouble progtot, prog, progq = 0.98, progmax = 1.0/(1.0 - progq);
    gboolean is_periodic_x, is_periodic_y, apply_opening;

    xres = gwy_data_field_get_xres(dfield);
    yres = gwy_data_field_get_yres(dfield);
    discfield = gwy_data_field_new(xres, yres, 1.0*xres, 1.0*yres, FALSE);
    gwy_data_field_fill(discfield, 1.0);
    workspace = gwy_data_field_new_alike(discfield, FALSE);
    shifted = gwy_data_field_new_alike(workspace, FALSE);
    /* In case someone calls the module explicitly... */
    apply_opening = args->make_tiles && args->apply_opening;

    rngset = gwy_rand_gen_set_new(RNG_NRNGS);
    gwy_rand_gen_set_init(rngset, args->seed);

    discs = g_array_new(FALSE, FALSE, sizeof(Disc));
    generate_seed_discs(args, discfield, discs, rngset);

    if (set_message && !set_message(_("Generating discs..."))) {
        g_object_unref(shifted);
        g_object_unref(discfield);
        g_object_unref(workspace);
        g_array_free(discs, TRUE);
        gwy_rand_gen_set_free(rngset);
        return FALSE;
    }

    gwy_clear(&gdata, 1);
    gwy_clear(&gsdata, 1);
    grains = g_new0(gint, 3*xres*yres);
    gtmp = grains + xres*yres;
    gshifted = gtmp + xres*yres;
    gnomax = ngrains = gwy_data_field_number_grains_periodic(discfield, grains);
    areas = g_array_new(FALSE, FALSE, sizeof(AvailableArea));
    gather_available_areas(discfield, shifted, areas, minr + 0.5*separ,
                           grains, gshifted, &gdata, &gsdata, ngrains,
                           TRUE, TRUE);

    apos = 0;
    prog = 1.0;
    progtot = 0.0;
    while (apos < areas->len) {
        /* Copy value, @areas can get reallocated. */
        AvailableArea area = g_array_index(areas, AvailableArea, apos);

        g_array_append_val(discs, area.d);
        is_periodic_x = (area.width == xres);
        is_periodic_y = (area.height == yres);
        /* There is a complication.  If the area is full-sized only in one
         * dimension we must treat it as periodic only in this dimension.
         * This means adding one empty row or column to prevent across-border
         * connectivity in the non-periodic dimension. */
        if (is_periodic_x && !is_periodic_y)
            area.height++;
        else if (!is_periodic_x && is_periodic_y)
            area.width++;

        process_area(discfield, &area, grains, separ, workspace);
        gwy_clear(gtmp, workspace->xres*workspace->yres);
        n = areas->len;
        if (is_periodic_x || is_periodic_y) {
            ng = gwy_data_field_number_grains_periodic(workspace, gtmp);
            gather_available_areas(workspace, shifted, areas, minr + 0.5*separ,
                                   gtmp, gshifted, &gdata, &gsdata, ng,
                                   is_periodic_x, is_periodic_y);
        }
        else {
            ng = gwy_data_field_number_grains(workspace, gtmp);
            gather_available_areas_nonperiodic(workspace, areas,
                                               minr + 0.5*separ,
                                               gtmp, &gdata, ng);
        }
        /* Correct grain numbers by adding @gnomax to all.  The grain numbers
         * in @grains are discontinuous but that does not matter because for
         * all operations we extract a single grain. */
        reintegrate_split_grains(discfield, &area, grains, gtmp, gnomax);
        for (i = n; i < areas->len; i++) {
            AvailableArea *a = &g_array_index(areas, AvailableArea, i);

            a->gno += gnomax;
            a->col = (a->col + area.col) % xres;
            a->row = (a->row + area.row) % yres;
            a->d.x += area.col;
            a->d.y += area.row;
        }
        gnomax += ng;
        apos++;

        /* We have no idea how many iterations it will take but we know they
         * are getting faster as we are processing progressively smaller areas.
         * Use geometric series to fake a reasonably behaving progress bar. */
        progtot += prog;
        prog *= progq;
        if (set_fraction && !set_fraction(progtot/progmax)) {
            g_free(grains);
            g_array_free(areas, TRUE);
            g_array_free(discs, TRUE);
            g_object_unref(shifted);
            g_object_unref(workspace);
            g_object_unref(discfield);
            gwy_rand_gen_set_free(rngset);
            gather_areas_data_free(&gdata);
            gather_areas_data_free(&gsdata);
            return FALSE;
        }
    }

    g_array_free(areas, TRUE);
    g_object_unref(shifted);
    GWY_OBJECT_UNREF(workspace);
    gather_areas_data_free(&gdata);
    gather_areas_data_free(&gsdata);

    gwy_data_field_fill(discfield, 1.0);
    ext = apply_opening ? 4*args->opening_size/3 + 1 : 0;
    for (i = 0; i < discs->len; i++) {
        const Disc *p = &g_array_index(discs, Disc, i);

        circular_area_fill_periodic(discfield, floor(p->x), floor(p->y),
                                    p->r, 0.0);
        ext = MAX(ext, (gint)ceil(p->r));
    }
    g_array_free(discs, TRUE);

    progtot = 0.5*(progtot + progmax);
    if (set_fraction && !set_fraction(progtot/progmax)) {
        g_object_unref(discfield);
        g_free(grains);
        gwy_rand_gen_set_free(rngset);
        return FALSE;
    }

    if (args->make_tiles) {
        workspace = gwy_data_field_extend(discfield, ext, ext, ext, ext,
                                          GWY_EXTERIOR_PERIODIC, 0.0, FALSE);
        gwy_data_field_grains_invert(workspace);
        gwy_data_field_grains_grow(workspace, 0.5*MIN(xres, yres),
                                   GWY_DISTANCE_TRANSFORM_EUCLIDEAN, TRUE);
        gwy_data_field_grains_invert(workspace);
        if (args->gap_thickness >= 2.0) {
            gwy_data_field_grains_grow(workspace, 0.7*args->gap_thickness,
                                       GWY_DISTANCE_TRANSFORM_EUCLIDEAN,
                                       FALSE);
            gwy_data_field_grains_shrink(workspace, 0.2*args->gap_thickness,
                                         GWY_DISTANCE_TRANSFORM_EUCLIDEAN,
                                         FALSE);
        }
        gwy_data_field_grains_invert(workspace);
    }
    else
        gwy_data_field_grains_invert(discfield);

    progtot = 0.5*(progtot + progmax);
    if (set_fraction && !set_fraction(progtot/progmax)) {
        GWY_OBJECT_UNREF(workspace);
        g_object_unref(discfield);
        g_free(grains);
        gwy_rand_gen_set_free(rngset);
        return FALSE;
    }

    if (apply_opening) {
        n = args->opening_size;
        shifted = gwy_data_field_new(n, n, n, n, TRUE);
        gwy_data_field_elliptic_area_fill(shifted, 0, 0, n, n, 1.0);
        gwy_data_field_area_filter_min_max(workspace, shifted,
                                           GWY_MIN_MAX_FILTER_OPENING,
                                           0, 0, xres + 2*ext, yres + 2*ext);
        g_object_unref(shifted);
    }
    if (args->make_tiles) {
        gwy_data_field_area_copy(workspace, discfield,
                                 ext, ext, xres, yres, 0, 0);
    }

    if (args->height_noise) {
        gwy_clear(grains, xres*yres);
        ngrains = gwy_data_field_number_grains_periodic(discfield, grains);
        heights = g_new0(gdouble, ngrains+1);
        heights[0] = 0.0;
        for (i = 1; i <= ngrains; i++) {
            z = gwy_rand_gen_set_gaussian(rngset, RNG_HEIGHT,
                                          args->height_noise);
            z = sqrt(z*z + 1.0) + z;   /* To (0, ∞) */
            heights[i] = z * height_base;
        }

        d = gwy_data_field_get_data(discfield);
        for (i = 0; i < xres*yres; i++)
            d[i] = heights[grains[i]];
        g_free(heights);
    }
    else
        gwy_data_field_multiply(discfield, height_base);

    g_free(grains);
    gwy_rand_gen_set_free(rngset);

    gwy_data_field_copy(discfield, dfield, FALSE);
    GWY_OBJECT_UNREF(workspace);
    g_object_unref(discfield);

    return TRUE;
}

static const gchar prefix[]                = "/module/disc_synth";
static const gchar active_page_key[]       = "/module/disc_synth/active_page";
static const gchar apply_opening_key[]     = "/module/disc_synth/apply_opening";
static const gchar gap_thickness_key[]     = "/module/disc_synth/gap_thickness";
static const gchar height_key[]            = "/module/disc_synth/height";
static const gchar height_noise_key[]      = "/module/disc_synth/height_noise";
static const gchar make_tiles_key[]        = "/module/disc_synth/make_tiles";
static const gchar opening_size_key[]      = "/module/disc_synth/opening_size";
static const gchar radius_init_key[]       = "/module/disc_synth/radius_init";
static const gchar radius_init_noise_key[] = "/module/disc_synth/radius_init_noise";
static const gchar radius_min_key[]        = "/module/disc_synth/radius_min";
static const gchar randomize_key[]         = "/module/disc_synth/randomize";
static const gchar seed_key[]              = "/module/disc_synth/seed";
static const gchar separation_key[]        = "/module/disc_synth/separation";
static const gchar update_key[]            = "/module/disc_synth/update";

static void
disc_synth_sanitize_args(DiscSynthArgs *args)
{
    args->active_page = CLAMP(args->active_page,
                              PAGE_DIMENSIONS, PAGE_NPAGES-1);
    args->update = !!args->update;
    args->seed = MAX(0, args->seed);
    args->randomize = !!args->randomize;
    args->radius_init = CLAMP(args->radius_init, 5.0, 1000.0);
    args->radius_init_noise = CLAMP(args->radius_init_noise, 0.0, 1.0);
    args->radius_min = CLAMP(args->radius_min, 3.0, 1000.0);
    args->separation = CLAMP(args->separation, 3.0, 120.0);
    args->height = CLAMP(args->height, 0.001, 10000.0);
    args->height_noise = CLAMP(args->height_noise, 0.0, 1.0);
    args->make_tiles = !!args->make_tiles;
    args->gap_thickness = CLAMP(args->gap_thickness, 1.0, 250.0);
    args->apply_opening = !!args->apply_opening;
    args->opening_size = CLAMP(args->opening_size, 1, 250);
}

static void
disc_synth_load_args(GwyContainer *container,
                     DiscSynthArgs *args,
                     GwyDimensionArgs *dimsargs)
{
    *args = disc_synth_defaults;

    gwy_container_gis_int32_by_name(container, active_page_key,
                                    &args->active_page);
    gwy_container_gis_boolean_by_name(container, update_key, &args->update);
    gwy_container_gis_int32_by_name(container, seed_key, &args->seed);
    gwy_container_gis_boolean_by_name(container, randomize_key,
                                      &args->randomize);
    gwy_container_gis_double_by_name(container, radius_init_key,
                                     &args->radius_init);
    gwy_container_gis_double_by_name(container, radius_init_noise_key,
                                     &args->radius_init_noise);
    gwy_container_gis_double_by_name(container, radius_min_key,
                                     &args->radius_min);
    gwy_container_gis_double_by_name(container, separation_key,
                                     &args->separation);
    gwy_container_gis_double_by_name(container, height_key, &args->height);
    gwy_container_gis_double_by_name(container, height_noise_key,
                                     &args->height_noise);
    gwy_container_gis_boolean_by_name(container, make_tiles_key,
                                      &args->make_tiles);
    gwy_container_gis_double_by_name(container, gap_thickness_key,
                                     &args->gap_thickness);
    gwy_container_gis_boolean_by_name(container, apply_opening_key,
                                      &args->apply_opening);
    gwy_container_gis_int32_by_name(container, opening_size_key,
                                    &args->opening_size);
    disc_synth_sanitize_args(args);

    gwy_clear(dimsargs, 1);
    gwy_dimensions_copy_args(&dims_defaults, dimsargs);
    gwy_dimensions_load_args(dimsargs, container, prefix);
}

static void
disc_synth_save_args(GwyContainer *container,
                     const DiscSynthArgs *args,
                     const GwyDimensionArgs *dimsargs)
{
    gwy_container_set_int32_by_name(container, active_page_key,
                                    args->active_page);
    gwy_container_set_boolean_by_name(container, update_key, args->update);
    gwy_container_set_int32_by_name(container, seed_key, args->seed);
    gwy_container_set_boolean_by_name(container, randomize_key,
                                      args->randomize);
    gwy_container_set_double_by_name(container, radius_init_key,
                                     args->radius_init);
    gwy_container_set_double_by_name(container, radius_init_noise_key,
                                     args->radius_init_noise);
    gwy_container_set_double_by_name(container, radius_min_key,
                                     args->radius_min);
    gwy_container_set_double_by_name(container, separation_key,
                                     args->separation);
    gwy_container_set_double_by_name(container, height_key, args->height);
    gwy_container_set_double_by_name(container, height_noise_key,
                                     args->height_noise);
    gwy_container_set_boolean_by_name(container, make_tiles_key,
                                      args->make_tiles);
    gwy_container_set_double_by_name(container, gap_thickness_key,
                                     args->gap_thickness);
    gwy_container_set_boolean_by_name(container, apply_opening_key,
                                      args->apply_opening);
    gwy_container_set_int32_by_name(container, opening_size_key,
                                    args->opening_size);

    gwy_dimensions_save_args(dimsargs, container, prefix);
}

/* vim: set cin et ts=4 sw=4 cino=>1s,e0,n0,f0,{0,}0,^0,\:1s,=0,g1s,h0,t0,+1s,c3,(0,u0 : */
