/*
 *  $Id$
 *  Copyright (C) 2018 David Necas (Yeti).
 *  E-mail: yeti@gwyddion.net.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA 02110-1301, USA.
 */

#include "config.h"
#include <gtk/gtk.h>
#include <libgwyddion/gwymacros.h>
#include <libprocess/arithmetic.h>
#include <libprocess/stats.h>
#include <libprocess/correlation.h>
#include <libprocess/filters.h>
#include <libgwydgets/gwydgetutils.h>
#include <libgwydgets/gwycombobox.h>
#include <libgwydgets/gwystock.h>
#include <libgwymodule/gwymodule-process.h>
#include <app/gwymoduleutils.h>
#include <app/gwyapp.h>

#define CONVOLVE_RUN_MODES GWY_RUN_INTERACTIVE

typedef enum {
    CONVOLVE_SIZE_CROP   = 0,
    CONVOLVE_SIZE_KEEP   = 1,
    CONVOLVE_SIZE_EXTEND = 2,
    CONVOLVE_NSIZE,
} ConvolveSizeType;

typedef struct {
    GwyExteriorType exterior;
    ConvolveSizeType outsize;
    gboolean as_integral;
    GwyAppDataId data;
    GwyAppDataId kernel;
} ConvolveArgs;

typedef struct {
    ConvolveArgs *args;
    GtkWidget *dialogue;
    GtkWidget *exterior;
    GtkWidget *outsize;
    GtkWidget *as_integral;
} ConvolveControls;

static gboolean module_register       (void);
static void     convolve              (GwyContainer *data,
                                       GwyRunType run);
static gboolean convolve_dialog       (ConvolveArgs *args);
static void     exterior_changed      (GtkComboBox *combo,
                                       ConvolveControls *controls);
static void     outsize_changed       (GtkComboBox *combo,
                                       ConvolveControls *controls);
static void     as_integral_changed   (GtkToggleButton *toggle,
                                       gboolean *value);
static void     kernel_changed        (GwyDataChooser *chooser,
                                       ConvolveControls *controls);
static gboolean convolve_kernel_filter(GwyContainer *data,
                                       gint id,
                                       gpointer user_data);
static void     convolve_do           (ConvolveArgs *args);
static void     convolve_load_args    (GwyContainer *settings,
                                       ConvolveArgs *args);
static void     convolve_save_args    (GwyContainer *settings,
                                       ConvolveArgs *args);
static void     convolve_sanitize_args(ConvolveArgs *args);

static const ConvolveArgs convolve_defaults = {
    GWY_EXTERIOR_FIXED_VALUE, CONVOLVE_SIZE_KEEP, FALSE,
    GWY_APP_DATA_ID_NONE, GWY_APP_DATA_ID_NONE,
};

static GwyAppDataId kernel_id = GWY_APP_DATA_ID_NONE;

static GwyModuleInfo module_info = {
    GWY_MODULE_ABI_VERSION,
    &module_register,
    N_("Convolves two images."),
    "Yeti <yeti@gwyddion.net>",
    "1.0",
    "David Nečas (Yeti)",
    "2018",
};

GWY_MODULE_QUERY2(module_info, convolve)

static gboolean
module_register(void)
{
    gwy_process_func_register("convolve",
                              (GwyProcessFunc)&convolve,
                              N_("/M_ultidata/_Convolve..."),
                              NULL,
                              CONVOLVE_RUN_MODES,
                              GWY_MENU_FLAG_DATA,
                              N_("Convolve two images"));

    return TRUE;
}

static void
convolve(G_GNUC_UNUSED GwyContainer *data, GwyRunType run)
{
    ConvolveArgs args;
    GwyContainer *settings;

    g_return_if_fail(run & CONVOLVE_RUN_MODES);
    settings = gwy_app_settings_get();
    convolve_load_args(settings, &args);

    gwy_app_data_browser_get_current(GWY_APP_DATA_FIELD_ID, &args.data.id,
                                     GWY_APP_CONTAINER_ID, &args.data.datano,
                                     0);

    if (convolve_dialog(&args))
        convolve_do(&args);

    convolve_save_args(gwy_app_settings_get(), &args);
}

static gboolean
convolve_dialog(ConvolveArgs *args)
{
    ConvolveControls controls;
    GtkWidget *dialog, *table, *chooser;
    gint row, response;
    gboolean ok;

    controls.args = args;

    dialog = gtk_dialog_new_with_buttons(_("Convolve"), NULL, 0,
                                         GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
                                         GTK_STOCK_OK, GTK_RESPONSE_OK,
                                         NULL);
    gtk_dialog_set_default_response(GTK_DIALOG(dialog), GTK_RESPONSE_OK);
    gwy_help_add_to_proc_dialog(GTK_DIALOG(dialog), GWY_HELP_DEFAULT);
    controls.dialogue = dialog;

    table = gtk_table_new(7, 3, FALSE);
    gtk_table_set_row_spacings(GTK_TABLE(table), 2);
    gtk_table_set_col_spacings(GTK_TABLE(table), 6);
    gtk_container_set_border_width(GTK_CONTAINER(table), 4);
    gtk_box_pack_start(GTK_BOX(GTK_DIALOG(dialog)->vbox), table, TRUE, TRUE, 4);
    row = 0;

    chooser = gwy_data_chooser_new_channels();
    g_object_set_data(G_OBJECT(chooser), "dialog", dialog);
    gwy_data_chooser_set_active(GWY_DATA_CHOOSER(chooser), NULL, -1);
    gwy_data_chooser_set_filter(GWY_DATA_CHOOSER(chooser),
                                convolve_kernel_filter, &args->data, NULL);
    gwy_data_chooser_set_active_id(GWY_DATA_CHOOSER(chooser), &args->kernel);
    gwy_data_chooser_get_active_id(GWY_DATA_CHOOSER(chooser), &args->kernel);
    g_signal_connect(chooser, "changed",
                     G_CALLBACK(kernel_changed), &controls);
    gwy_table_attach_adjbar(table, row, _("Convolution _kernel:"), NULL,
                            GTK_OBJECT(chooser), GWY_HSCALE_WIDGET_NO_EXPAND);
    row++;

    controls.as_integral
        = gtk_check_button_new_with_mnemonic(_("Normalize as _integral"));
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(controls.as_integral),
                                 args->as_integral);
    g_signal_connect(controls.as_integral, "toggled",
                     G_CALLBACK(as_integral_changed), &args->as_integral);
    gtk_table_attach(GTK_TABLE(table), controls.as_integral,
                     0, 2, row, row+1, GTK_FILL, 0, 0, 0);
    row++;

    controls.exterior
        = gwy_enum_combo_box_newl(G_CALLBACK(exterior_changed), &controls,
                                  args->exterior,
                                  _("Zero"), GWY_EXTERIOR_FIXED_VALUE,
                                  gwy_sgettext("exterior|Border"),
                                  GWY_EXTERIOR_BORDER_EXTEND,
                                  gwy_sgettext("exterior|Mirror"),
                                  GWY_EXTERIOR_MIRROR_EXTEND,
                                  gwy_sgettext("exterior|Periodic"),
                                  GWY_EXTERIOR_PERIODIC,
                                  gwy_sgettext("exterior|Laplace"),
                                  GWY_EXTERIOR_LAPLACE,
                                  NULL);
    gwy_table_attach_adjbar(table, row, _("_Exterior type:"), NULL,
                            GTK_OBJECT(controls.exterior),
                            GWY_HSCALE_WIDGET_NO_EXPAND);
    row++;

    controls.outsize
        = gwy_enum_combo_box_newl(G_CALLBACK(outsize_changed), &controls,
                                  args->outsize,
                                  gwy_sgettext("Crop to interior"),
                                  CONVOLVE_SIZE_CROP,
                                  gwy_sgettext("Keep size"),
                                  CONVOLVE_SIZE_KEEP,
                                  gwy_sgettext("Extend to convolved"),
                                  CONVOLVE_SIZE_EXTEND,
                                  NULL);
    gwy_table_attach_adjbar(table, row, _("Output _size:"), NULL,
                            GTK_OBJECT(controls.outsize),
                            GWY_HSCALE_WIDGET_NO_EXPAND);
    row++;

    gtk_widget_show_all(dialog);
    kernel_changed(GWY_DATA_CHOOSER(chooser), &controls);

    do {
        response = gtk_dialog_run(GTK_DIALOG(dialog));
        switch (response) {
            case GTK_RESPONSE_CANCEL:
            case GTK_RESPONSE_DELETE_EVENT:
            gtk_widget_destroy(dialog);
            case GTK_RESPONSE_NONE:
            return FALSE;
            break;

            case GTK_RESPONSE_OK:
            gtk_widget_destroy(dialog);
            ok = TRUE;
            break;

            default:
            g_assert_not_reached();
            break;
        }
    } while (!ok);

    return ok;
}

static void
exterior_changed(GtkComboBox *combo, ConvolveControls *controls)
{
    ConvolveArgs *args = controls->args;
    args->exterior = gwy_enum_combo_box_get_active(combo);
}

static void
outsize_changed(GtkComboBox *combo, ConvolveControls *controls)
{
    ConvolveArgs *args = controls->args;
    args->outsize = gwy_enum_combo_box_get_active(combo);
}

static void
as_integral_changed(GtkToggleButton *toggle, gboolean *value)
{
    *value = gtk_toggle_button_get_active(toggle);
}

static void
kernel_changed(GwyDataChooser *chooser, ConvolveControls *controls)
{
    GwyAppDataId *object = &controls->args->kernel;
    GtkWidget *dialog;

    gwy_data_chooser_get_active_id(chooser, object);
    gwy_debug("kernel: %d %d", object->datano, object->id);

    dialog = g_object_get_data(G_OBJECT(chooser), "dialog");
    g_assert(GTK_IS_DIALOG(dialog));
    gtk_dialog_set_response_sensitive(GTK_DIALOG(dialog), GTK_RESPONSE_OK,
                                      object->datano);
}

static gboolean
convolve_kernel_filter(GwyContainer *data, gint id, gpointer user_data)
{
    GwyAppDataId *object = (GwyAppDataId*)user_data;
    GwyDataField *kernel, *dfield;
    GQuark quark;

    quark = gwy_app_get_data_key_for_id(id);
    kernel = GWY_DATA_FIELD(gwy_container_get_object(data, quark));

    data = gwy_app_data_browser_get(object->datano);
    quark = gwy_app_get_data_key_for_id(object->id);
    dfield = GWY_DATA_FIELD(gwy_container_get_object(data, quark));

    /* For non-empty result with CONVOLVE_SIZE_CROP we need a sharp inequality
     * here. */
    if (gwy_data_field_get_xreal(kernel) < gwy_data_field_get_xreal(dfield)/2
        && gwy_data_field_get_yreal(kernel) < gwy_data_field_get_yreal(dfield)/2
        && !gwy_data_field_check_compatibility(kernel, dfield,
                                               GWY_DATA_COMPATIBILITY_LATERAL
                                               | GWY_DATA_COMPATIBILITY_MEASURE))
        return TRUE;

    return FALSE;
}

static void
convolve_do(ConvolveArgs *args)
{
    GwyDataField *dfield, *kernel, *extfield, *retfield;
    GwyContainer *data, *kerneldata;
    GQuark quark;
    gint newid, xres, yres, kxres, kyres;

    kerneldata = gwy_app_data_browser_get(args->kernel.datano);
    quark = gwy_app_get_data_key_for_id(args->kernel.id);
    kernel = GWY_DATA_FIELD(gwy_container_get_object(kerneldata, quark));
    kxres = gwy_data_field_get_xres(kernel);
    kyres = gwy_data_field_get_yres(kernel);

    data = gwy_app_data_browser_get(args->data.datano);
    quark = gwy_app_get_data_key_for_id(args->data.id);
    dfield = GWY_DATA_FIELD(gwy_container_get_object(data, quark));
    xres = gwy_data_field_get_xres(dfield);
    yres = gwy_data_field_get_yres(dfield);

    if (args->outsize == CONVOLVE_SIZE_EXTEND) {
        /* XXX: This way we extend the field twice.  The extension in
         * gwy_data_field_area_ext_convolve() then just messes things up, for
         * instance for periodic exterior.  So extend it all the way here, use
         * zero-filled exterior and crop. */
        extfield = gwy_data_field_extend(dfield, kxres, kxres, kyres, kyres,
                                         args->exterior, 0.0, FALSE);
        retfield = gwy_data_field_new_alike(extfield, FALSE);
        gwy_data_field_area_ext_convolve(extfield,
                                         0, 0, xres + 2*kxres, yres + 2*kyres,
                                         retfield, kernel,
                                         GWY_EXTERIOR_FIXED_VALUE, 0.0,
                                         args->as_integral);
        g_object_unref(extfield);
        gwy_data_field_resize(retfield,
                              kxres/2, kyres/2,
                              xres + 2*kxres - kxres/2,
                              yres + 2*kyres - kyres/2);
    }
    else {
        retfield = gwy_data_field_new_alike(dfield, FALSE);
        gwy_data_field_area_ext_convolve(dfield, 0, 0, xres, yres,
                                         retfield, kernel,
                                         args->exterior, 0.0,
                                         args->as_integral);
        if (args->outsize == CONVOLVE_SIZE_CROP) {
            gwy_data_field_resize(retfield,
                                  kxres/2, kyres/2,
                                  xres + kxres/2 - kxres,
                                  yres + kyres/2 - kyres);
        }
    }

    newid = gwy_app_data_browser_add_data_field(retfield, data, TRUE);
    gwy_app_sync_data_items(data, data,
                            args->data.id, newid, FALSE,
                            GWY_DATA_ITEM_GRADIENT, 0);
    gwy_app_set_data_field_title(data, newid, _("Convolved"));
    gwy_app_channel_log_add_proc(data, args->data.id, newid);
    g_object_unref(retfield);
}

static const gchar as_integral_key[] = "/module/convolve/as_integral";
static const gchar exterior_key[]    = "/module/convolve/exterior";
static const gchar outsize_key[]     = "/module/convolve/outsize";

static void
convolve_sanitize_args(ConvolveArgs *args)
{
    args->exterior = gwy_enum_sanitize_value(args->exterior,
                                             GWY_TYPE_EXTERIOR_TYPE);
    if (args->exterior == GWY_EXTERIOR_UNDEFINED)
        args->exterior = GWY_EXTERIOR_FIXED_VALUE;

    args->outsize = MIN(args->outsize, CONVOLVE_NSIZE-1);
    args->as_integral = !!args->as_integral;
    gwy_app_data_id_verify_channel(&args->kernel);
}

static void
convolve_load_args(GwyContainer *settings,
                  ConvolveArgs *args)
{
    *args = convolve_defaults;
    gwy_container_gis_enum_by_name(settings, exterior_key, &args->exterior);
    gwy_container_gis_enum_by_name(settings, outsize_key, &args->outsize);
    gwy_container_gis_boolean_by_name(settings, as_integral_key,
                                      &args->as_integral);
    args->kernel = kernel_id;
    convolve_sanitize_args(args);
}

static void
convolve_save_args(GwyContainer *settings,
                  ConvolveArgs *args)
{
    kernel_id = args->kernel;
    gwy_container_set_enum_by_name(settings, exterior_key, args->exterior);
    gwy_container_set_enum_by_name(settings, outsize_key, args->outsize);
    gwy_container_set_boolean_by_name(settings, as_integral_key,
                                      args->as_integral);
}

/* vim: set cin et ts=4 sw=4 cino=>1s,e0,n0,f0,{0,}0,^0,\:1s,=0,g1s,h0,t0,+1s,c3,(0,u0 : */
