/*
 *  $Id$
 *  Copyright (C) 2004-2017 David Necas (Yeti), Petr Klapetek.
 *  E-mail: yeti@gwyddion.net, klapetek@gwyddion.net.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA 02110-1301, USA.
 */

#include "config.h"
#include <string.h>
#include <gtk/gtk.h>
#include <libgwyddion/gwymacros.h>
#include <libgwyddion/gwymath.h>
#include <libprocess/stats.h>
#include <libprocess/arithmetic.h>
#include <libprocess/filters.h>
#include <libgwydgets/gwydgetutils.h>
#include <libgwydgets/gwyradiobuttons.h>
#include <libgwymodule/gwymodule-process.h>
#include <app/gwyapp.h>
#include "preview.h"

#define SPHREV_RUN_MODES (GWY_RUN_IMMEDIATE | GWY_RUN_INTERACTIVE)

typedef enum {
    SPHREV_HORIZONTAL = 1,
    SPHREV_VERTICAL,
    SPHREV_BOTH
} SphrevDirection;

typedef struct {
    gdouble size;
    gboolean do_extract;
    SphrevDirection direction;  /* only in 1D */
    /* interface only */
    GwySIValueFormat *valform;
    gdouble pixelsize;
} SphrevArgs;

typedef struct {
    GSList *direction;
    GtkObject *radius;
    GtkObject *size;
    GtkWidget *do_extract;
    gboolean in_update;
} SphrevControls;

static gboolean      module_register     (void);
static void          arcrev              (GwyContainer *data,
                                          GwyRunType run);
static void          sphrev              (GwyContainer *data,
                                          GwyRunType run);
static gboolean      sphrev_dialog       (SphrevArgs *args,
                                          gboolean is_2d);
static void          direction_changed   (GtkWidget *radio,
                                          SphrevArgs *args);
static void          radius_changed      (GtkAdjustment *adj,
                                          SphrevArgs *args);
static void          size_changed        (GtkAdjustment *adj,
                                          SphrevArgs *args);
static void          do_extract_changed  (GtkWidget *check,
                                          SphrevArgs *args);
static void          sphrev_dialog_update(SphrevControls *controls,
                                          SphrevArgs *args);
static GwyDataField* arcrev_horizontal   (SphrevArgs *args,
                                          GwyDataField *dfield);
static GwyDataField* arcrev_vertical     (SphrevArgs *args,
                                          GwyDataField *dfield);
static GwyDataLine*  arcrev_make_arc     (gdouble radius,
                                          gint maxres);
static GwyDataField* sphrev_make_sphere  (gdouble radius,
                                          gint maxres);
static GwyDataField* sphrev_2d           (SphrevArgs *args,
                                          GwyDataField *dfield,
                                          GwySetMessageFunc set_message,
                                          GwySetFractionFunc set_fraction);
static void          sphrev_sanitize_args(SphrevArgs *args);
static void          arcrev_load_args    (GwyContainer *container,
                                          SphrevArgs *args);
static void          arcrev_save_args    (GwyContainer *container,
                                          SphrevArgs *args);
static void          sphrev_load_args    (GwyContainer *container,
                                          SphrevArgs *args);
static void          sphrev_save_args    (GwyContainer *container,
                                          SphrevArgs *args);

static const SphrevArgs sphrev_defaults = {
    20,
    FALSE,
    SPHREV_HORIZONTAL,
    NULL,
    0,
};

static GwyModuleInfo module_info = {
    GWY_MODULE_ABI_VERSION,
    &module_register,
    N_("Subtracts background by arc or sphere revolution."),
    "Yeti <yeti@gwyddion.net>",
    "2.0",
    "David Nečas (Yeti) & Petr Klapetek",
    "2004",
};

GWY_MODULE_QUERY2(module_info, sphere_revolve)

static gboolean
module_register(void)
{
    gwy_process_func_register("arc_revolve",
                              (GwyProcessFunc)&arcrev,
                              N_("/_Level/Revolve _Arc..."),
                              GWY_STOCK_REVOLVE_ARC,
                              SPHREV_RUN_MODES,
                              GWY_MENU_FLAG_DATA,
                              N_("Level data by arc revolution"));
    gwy_process_func_register("sphere_revolve",
                              (GwyProcessFunc)&sphrev,
                              N_("/_Level/Revolve _Sphere..."),
                              GWY_STOCK_REVOLVE_SPHERE,
                              SPHREV_RUN_MODES,
                              GWY_MENU_FLAG_DATA,
                              N_("Level data by sphere revolution"));

    return TRUE;
}

static void
arcrev(GwyContainer *data, GwyRunType run)
{
    GwyDataField *dfield, *background = NULL;
    GwyContainer *settings;
    SphrevArgs args;
    gint oldid, newid;
    GQuark dquark;
    gdouble xr, yr;
    gboolean ok = TRUE;

    g_return_if_fail(run & SPHREV_RUN_MODES);
    gwy_app_data_browser_get_current(GWY_APP_DATA_FIELD, &dfield,
                                     GWY_APP_DATA_FIELD_KEY, &dquark,
                                     GWY_APP_DATA_FIELD_ID, &oldid,
                                     0);
    g_return_if_fail(dfield && dquark);

    settings = gwy_app_settings_get();
    arcrev_load_args(settings, &args);

    /* FIXME: this is bogus for non-square pixels anyway */
    xr = gwy_data_field_get_xreal(dfield)/gwy_data_field_get_xres(dfield);
    yr = gwy_data_field_get_yreal(dfield)/gwy_data_field_get_yres(dfield);
    args.pixelsize = hypot(xr, yr);
    args.valform
        = gwy_data_field_get_value_format_xy(dfield,
                                             GWY_SI_UNIT_FORMAT_VFMARKUP, NULL);
    gwy_debug("pixelsize = %g, vf = (%g, %d, %s)",
              args.pixelsize, args.valform->magnitude, args.valform->precision,
              args.valform->units);

    if (run == GWY_RUN_INTERACTIVE) {
        ok = sphrev_dialog(&args, FALSE);
        arcrev_save_args(settings, &args);
    }

    gwy_si_unit_value_format_free(args.valform);
    if (!ok)
        return;

    gwy_app_undo_qcheckpointv(data, 1, &dquark);
    switch (args.direction) {
        case SPHREV_HORIZONTAL:
        background = arcrev_horizontal(&args, dfield);
        break;

        case SPHREV_VERTICAL:
        background = arcrev_vertical(&args, dfield);
        break;

        case SPHREV_BOTH: {
            GwyDataField *tmp;

            background = arcrev_horizontal(&args, dfield);
            tmp = arcrev_vertical(&args, dfield);
            gwy_data_field_sum_fields(background, background, tmp);
            g_object_unref(tmp);
            gwy_data_field_multiply(background, 0.5);
        }
        break;

        default:
        g_assert_not_reached();
        break;
    }
    gwy_data_field_subtract_fields(dfield, dfield, background);
    gwy_data_field_data_changed(dfield);
    gwy_app_channel_log_add_proc(data, oldid, oldid);

    if (!args.do_extract) {
        g_object_unref(background);
        return;
    }

    newid = gwy_app_data_browser_add_data_field(background, data, TRUE);
    g_object_unref(background);
    gwy_app_sync_data_items(data, data, oldid, newid, FALSE,
                            GWY_DATA_ITEM_GRADIENT,
                            0);
    gwy_app_set_data_field_title(data, newid, _("Background"));
    gwy_app_channel_log_add(data, oldid, newid, NULL, NULL);
}

static void
sphrev(GwyContainer *data, GwyRunType run)
{
    GwyDataField *dfield, *background = NULL;
    GtkWindow *window;
    GwyContainer *settings;
    SphrevArgs args;
    gint oldid, newid;
    GQuark dquark;
    gdouble xr, yr;
    gboolean ok = TRUE;

    g_return_if_fail(run & SPHREV_RUN_MODES);
    gwy_app_data_browser_get_current(GWY_APP_DATA_FIELD, &dfield,
                                     GWY_APP_DATA_FIELD_KEY, &dquark,
                                     GWY_APP_DATA_FIELD_ID, &oldid,
                                     0);
    g_return_if_fail(dfield && dquark);

    settings = gwy_app_settings_get();
    sphrev_load_args(settings, &args);

    /* FIXME: this is bogus for non-square pixels anyway */
    xr = gwy_data_field_get_xreal(dfield)/gwy_data_field_get_xres(dfield);
    yr = gwy_data_field_get_yreal(dfield)/gwy_data_field_get_yres(dfield);
    args.pixelsize = hypot(xr, yr);
    args.valform
        = gwy_data_field_get_value_format_xy(dfield,
                                             GWY_SI_UNIT_FORMAT_VFMARKUP, NULL);
    gwy_debug("pixelsize = %g, vf = (%g, %d, %s)",
              args.pixelsize, args.valform->magnitude, args.valform->precision,
              args.valform->units);

    if (run == GWY_RUN_INTERACTIVE) {
        ok = sphrev_dialog(&args, TRUE);
        sphrev_save_args(settings, &args);
    }

    gwy_si_unit_value_format_free(args.valform);
    if (!ok)
        return;

    window = gwy_app_find_window_for_channel(data, oldid);
    if (window) {
        gwy_app_wait_start(window, _("Initializing..."));
        background = sphrev_2d(&args, dfield,
                               gwy_app_wait_set_message,
                               gwy_app_wait_set_fraction);
        gwy_app_wait_finish();
        if (!background)
            return;
    }
    else
        background = sphrev_2d(&args, dfield, NULL, NULL);

    gwy_app_undo_qcheckpointv(data, 1, &dquark);
    gwy_data_field_subtract_fields(dfield, dfield, background);
    gwy_data_field_data_changed(dfield);
    gwy_app_channel_log_add_proc(data, oldid, oldid);

    if (!args.do_extract) {
        g_object_unref(background);
        return;
    }

    newid = gwy_app_data_browser_add_data_field(background, data, TRUE);
    g_object_unref(background);
    gwy_app_sync_data_items(data, data, oldid, newid, FALSE,
                            GWY_DATA_ITEM_GRADIENT,
                            0);
    gwy_app_set_data_field_title(data, newid, _("Background"));
    gwy_app_channel_log_add(data, oldid, newid, NULL, NULL);
}

static gboolean
sphrev_dialog(SphrevArgs *args, gboolean is_2d)
{
    const GwyEnum directions[] = {
        { N_("_Horizontal direction"), SPHREV_HORIZONTAL, },
        { N_("_Vertical direction"),   SPHREV_VERTICAL,   },
        { N_("_Both directions"),      SPHREV_BOTH,   },
    };
    GtkWidget *dialog, *table, *spin, *label;
    SphrevControls controls;
    gint response, row;
    const gchar *title;
    gdouble q;

    gwy_clear(&controls, 1);

    title = is_2d ? _("Revolve Sphere") : _("Revolve Arc");
    dialog = gtk_dialog_new_with_buttons(title, NULL, 0,
                                         _("_Reset"), RESPONSE_RESET,
                                         GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
                                         GTK_STOCK_OK, GTK_RESPONSE_OK,
                                         NULL);
    gtk_dialog_set_default_response(GTK_DIALOG(dialog), GTK_RESPONSE_OK);
    gwy_help_add_to_proc_dialog(GTK_DIALOG(dialog), GWY_HELP_DEFAULT);

    table = gtk_table_new(3 + 4*(!is_2d), 3, FALSE);
    gtk_table_set_row_spacings(GTK_TABLE(table), 2);
    gtk_table_set_col_spacings(GTK_TABLE(table), 6);
    gtk_container_set_border_width(GTK_CONTAINER(table), 4);
    gtk_box_pack_start(GTK_BOX(GTK_DIALOG(dialog)->vbox), table,
                       FALSE, FALSE, 4);
    row = 0;
    controls.in_update = TRUE;

    q = args->pixelsize/args->valform->magnitude;
    gwy_debug("q = %f", q);
    controls.radius = gtk_adjustment_new(q*args->size, q, 1024*q, q, 10*q, 0);
    spin = gwy_table_attach_adjbar(table, row, _("Real _radius:"),
                                   args->valform->units, controls.radius,
                                   GWY_HSCALE_SQRT);
    gtk_spin_button_set_digits(GTK_SPIN_BUTTON(spin), args->valform->precision);
    g_object_set_data(G_OBJECT(controls.radius), "controls", &controls);
    g_signal_connect(controls.radius, "value-changed",
                     G_CALLBACK(radius_changed), args);
    row++;

    controls.size = gtk_adjustment_new(args->size, 1, 1024, 1, 10, 0);
    gwy_table_attach_adjbar(table, row, _("_Pixel radius:"), _("px"),
                            controls.size, GWY_HSCALE_SQRT | GWY_HSCALE_SNAP);
    g_object_set_data(G_OBJECT(controls.size), "controls", &controls);
    g_signal_connect(controls.size, "value-changed",
                     G_CALLBACK(size_changed), args);
    row++;

    if (!is_2d) {
        gtk_table_set_row_spacing(GTK_TABLE(table), row-1, 8);
        label = gtk_label_new(_("Direction:"));
        gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);
        gtk_table_attach(GTK_TABLE(table), label,
                         0, 2, row, row+1, GTK_FILL, 0, 0, 0);
        row++;

        controls.direction
            = gwy_radio_buttons_create(directions, G_N_ELEMENTS(directions),
                                       G_CALLBACK(direction_changed), args,
                                       args->direction);
        row = gwy_radio_buttons_attach_to_table(controls.direction,
                                                GTK_TABLE(table), 2, row);
    }

    gtk_table_set_row_spacing(GTK_TABLE(table), row-1, 8);
    controls.do_extract
        = gtk_check_button_new_with_mnemonic(_("E_xtract background"));
    gtk_table_attach(GTK_TABLE(table), controls.do_extract,
                     0, 2, row, row+1, GTK_FILL, 0, 0, 0);
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(controls.do_extract),
                                 args->do_extract);
    g_signal_connect(controls.do_extract, "toggled",
                     G_CALLBACK(do_extract_changed), args);
    row++;

    controls.in_update = FALSE;

    gtk_widget_show_all(dialog);
    do {
        response = gtk_dialog_run(GTK_DIALOG(dialog));
        switch (response) {
            case GTK_RESPONSE_CANCEL:
            case GTK_RESPONSE_DELETE_EVENT:
            gtk_widget_destroy(dialog);
            case GTK_RESPONSE_NONE:
            return FALSE;
            break;

            case GTK_RESPONSE_OK:
            break;

            case RESPONSE_RESET:
            args->size = sphrev_defaults.size;
            args->direction = sphrev_defaults.direction;
            args->do_extract = sphrev_defaults.do_extract;
            sphrev_dialog_update(&controls, args);
            break;

            default:
            g_assert_not_reached();
            break;
        }
    } while (response != GTK_RESPONSE_OK);

    gtk_widget_destroy(dialog);

    return TRUE;
}

static void
direction_changed(GtkWidget *radio, SphrevArgs *args)
{
    if (!gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(radio)))
        return;

    args->direction = gwy_radio_button_get_value(radio);
}

static void
radius_changed(GtkAdjustment *adj, SphrevArgs *args)
{
    SphrevControls *controls;

    controls = g_object_get_data(G_OBJECT(adj), "controls");
    if (controls->in_update)
        return;

    controls->in_update = TRUE;
    args->size = gtk_adjustment_get_value(adj)
                 * args->valform->magnitude/args->pixelsize;
    sphrev_dialog_update(controls, args);
    controls->in_update = FALSE;
}

static void
size_changed(GtkAdjustment *adj, SphrevArgs *args)
{
    SphrevControls *controls;

    controls = g_object_get_data(G_OBJECT(adj), "controls");
    if (controls->in_update)
        return;

    controls->in_update = TRUE;
    args->size = gtk_adjustment_get_value(adj);
    sphrev_dialog_update(controls, args);
    controls->in_update = FALSE;
}

static void
do_extract_changed(GtkWidget *check, SphrevArgs *args)
{
    args->do_extract = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(check));
}

static void
sphrev_dialog_update(SphrevControls *controls, SphrevArgs *args)
{
    gtk_adjustment_set_value(GTK_ADJUSTMENT(controls->radius),
                             args->size
                             * args->pixelsize/args->valform->magnitude);
    gtk_adjustment_set_value(GTK_ADJUSTMENT(controls->size), args->size);
    if (controls->direction)
        gwy_radio_buttons_set_current(controls->direction, args->direction);
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(controls->do_extract),
                                 args->do_extract);
}

/* An efficient summing algorithm.  Although I'm author of this code, don't
 * ask me how it works... */
static void
moving_sums(gint res, const gdouble *row, gdouble *buffer, gint size)
{
    gdouble *sum, *sum2;
    gint i, ls2, rs2;

    memset(buffer, 0, 2*res*sizeof(gdouble));
    sum = buffer;
    sum2 = buffer + res;

    ls2 = size/2;
    rs2 = (size - 1)/2;

    /* Shortcut: very large size */
    if (rs2 >= res) {
        for (i = 0; i < res; i++) {
            sum[i] += row[i];
            sum2[i] += row[i]*row[i];
        }
        for (i = 1; i < res; i++) {
            sum[i] = sum[0];
            sum2[i] = sum2[0];
        }
        return;
    }

    /* Phase 1: Fill first element */
    for (i = 0; i <= rs2; i++) {
       sum[0] += row[i];
       sum2[0] += row[i]*row[i];
    }

    /* Phase 2: Next elements only gather new data */
    for (i = 1; i <= MIN(ls2, res-1 - rs2); i++) {
        sum[i] = sum[i-1] + row[i + rs2];
        sum2[i] = sum2[i-1] + row[i + rs2]*row[i + rs2];
    }

    /* Phase 3a: Moving a sprat! */
    for (i = ls2+1; i <= res-1 - rs2; i++) {
        sum[i] = sum[i-1] + row[i + rs2] - row[i - ls2 - 1];
        sum2[i] = sum2[i-1] + row[i + rs2]*row[i + rs2]
                  - row[i - ls2 - 1]*row[i - ls2 - 1];
    }

    /* Phase 3b: Moving a whale! */
    for (i = res-1 - rs2; i <= ls2; i++) {
        sum[i] = sum[i-1];
        sum2[i] = sum2[i-1];
    }

    /* Phase 4: Next elements only lose data */
    for (i = MAX(ls2+1, res - rs2); i < res; i++) {
        sum[i] = sum[i-1] - row[i - ls2 - 1];
        sum2[i] = sum2[i-1] - row[i - ls2 - 1]*row[i - ls2 - 1];
    }
}

static GwyDataField*
arcrev_horizontal(SphrevArgs *args, GwyDataField *dfield)
{
    GwyDataField *rfield;
    GwyDataLine *sphere;
    gdouble *rdata, *sphdata, *sum, *sum2, *weight, *tmp;
    const gdouble *data;
    gdouble q;
    gint i, j, k, size, xres, yres;

    data = gwy_data_field_get_data_const(dfield);
    rfield = gwy_data_field_duplicate(dfield);
    xres = gwy_data_field_get_xres(rfield);
    yres = gwy_data_field_get_yres(rfield);
    rdata = gwy_data_field_get_data(rfield);

    q = gwy_data_field_get_rms(dfield)/sqrt(2.0/3.0 - G_PI/16.0);
    sphere = arcrev_make_arc(args->size, gwy_data_field_get_xres(dfield));

    /* Scale-freeing.
     * Data is normalized to have the same RMS as if it was composed from
     * arcs of radius args->radius.  Actually we normalize the sphere
     * instead, but the effect is the same.  */
    gwy_data_line_multiply(sphere, -q);
    sphdata = gwy_data_line_get_data(sphere);
    size = gwy_data_line_get_res(sphere)/2;

    sum = g_new(gdouble, 4*xres);
    sum2 = sum + xres;
    weight = sum + 2*xres;
    tmp = sum + 3*xres;

    /* Weights for RMS filter.  The fool-proof way is to sum 1's. */
    for (j = 0; j < xres; j++)
        weight[j] = 1.0;
    moving_sums(xres, weight, sum, size);
    gwy_assign(weight, sum, xres);

    for (i = 0; i < yres; i++) {
        const gdouble *drow = data + i*xres;
        gdouble *rrow = rdata + i*xres;

        /* Kill data that stick down too much */
        moving_sums(xres, data + i*xres, sum, size);
        for (j = 0; j < xres; j++) {
            /* transform to avg - 2.5*rms */
            sum[j] = sum[j]/weight[j];
            sum2[j] = 2.5*sqrt(sum2[j]/weight[j] - sum[j]*sum[j]);
            sum[j] -= sum2[j];
        }
        for (j = 0; j < xres; j++)
            tmp[j] = MAX(drow[j], sum[j]);

        /* Find the touching point */
        for (j = 0; j < xres; j++) {
            gdouble *row = tmp + j;
            gint from, to;
            gdouble min;

            from = MAX(0, j-size) - j;
            to = MIN(j+size, xres-1) - j;
            min = G_MAXDOUBLE;
            for (k = from; k <= to; k++) {
                gdouble d = -sphdata[size+k] + row[k];
                if (d < min)
                    min = d;
            }
            rrow[j] = min;
        }
    }

    g_free(sum);
    g_object_unref(sphere);

    return rfield;
}

static GwyDataField*
arcrev_vertical(SphrevArgs *args, GwyDataField *dfield)
{
    GwyDataField *result, *flipped;

    flipped = gwy_data_field_new_alike(dfield, FALSE);
    gwy_data_field_flip_xy(dfield, flipped, FALSE);
    result = arcrev_horizontal(args, flipped);
    gwy_data_field_flip_xy(result, flipped, FALSE);
    g_object_unref(result);

    return flipped;
}

static GwyDataLine*
arcrev_make_arc(gdouble radius, gint maxres)
{
    GwyDataLine *arc;
    gdouble *data;
    gint i, size;

    size = GWY_ROUND(MIN(radius, maxres));
    arc = gwy_data_line_new(2*size+1, 1.0, FALSE);
    data = gwy_data_line_get_data(arc);

    if (radius/8 > maxres) {
        /* Pathological case: very flat sphere */
        for (i = 0; i <= size; i++) {
            gdouble u = i/radius;

            data[size+i] = data[size-i] = u*u/2.0*(1.0 + u*u/4.0*(1 + u*u/2.0));
        }
    }
    else {
        /* Normal sphere */
        for (i = 0; i <= size; i++) {
            gdouble u = i/radius;

            if (G_UNLIKELY(u > 1.0))
                data[size+i] = data[size-i] = 1.0;
            else
                data[size+i] = data[size-i] = 1.0 - sqrt(1.0 - u*u);
        }
    }

    return arc;
}

static GwyDataField*
sphrev_2d(SphrevArgs *args, GwyDataField *dfield,
          GwySetMessageFunc set_message, GwySetFractionFunc set_fraction)
{
    GwyDataField *meanfield, *rmsfield, *rfield, *sphere;
    gdouble *rdata, *sphdata, *tmp;
    gint i, j, ii, jj, sres, size, xres, yres, sc;
    gdouble q;
    gboolean ok = FALSE;

    rfield = gwy_data_field_duplicate(dfield);
    xres = gwy_data_field_get_xres(rfield);
    yres = gwy_data_field_get_yres(rfield);
    rdata = gwy_data_field_get_data(rfield);

    q = gwy_data_field_get_rms(dfield)/sqrt(5.0/6.0);
    sphere = sphrev_make_sphere(args->size, gwy_data_field_get_xres(dfield));

    /* Scale-freeing.
     * Data is normalized to have the same RMS as if it was composed from
     * spheres of radius args->radius.  Actually we normalize the sphere
     * instead, but the effect is the same.  */
    gwy_data_field_multiply(sphere, -q);
    sphdata = gwy_data_field_get_data(sphere);
    sres = gwy_data_field_get_xres(sphere);
    size = sres/2;
    sc = size*sres + size;

    meanfield = gwy_data_field_duplicate(dfield);
    rmsfield = gwy_data_field_duplicate(dfield);
    /* XXX: 1D apparently uses size/2 here.  Not sure why, mimic it. */
    gwy_data_field_filter_mean(meanfield, size/2);
    gwy_data_field_filter_rms(rmsfield, size/2);

    /* Transform mean value data to avg - 2.5*rms for outlier cut-off.
     * Allows using rmsfield as a scratch buffer for the trimmed data. */
    gwy_data_field_multiply(rmsfield, 2.5);
    gwy_data_field_subtract_fields(meanfield, meanfield, rmsfield);

    gwy_data_field_max_of_fields(rmsfield, meanfield, dfield);

    if (set_message && !set_message(_("Revolving sphere...")))
        goto finish;

    tmp = gwy_data_field_get_data(rmsfield);

    for (i = 0; i < yres; i++) {
        gint ifrom, ito;

        ifrom = MAX(0, i-size) - i;
        ito = MIN(i+size, yres-1) - i;
        for (j = 0; j < xres; j++) {
            gint jfrom, jto;
            gdouble min;

            jfrom = MAX(0, j-size) - j;
            jto = MIN(j+size, xres-1) - j;

            /* Find the touching point */
            min = G_MAXDOUBLE;
            for (ii = ifrom; ii <= ito; ii++) {
                const gdouble *srow = sphdata + (sc + ii*sres + jfrom);
                const gdouble *drow = tmp + ((i + ii)*xres + j + jfrom);

                for (jj = jto+1 - jfrom; jj; jj--, srow++, drow++) {
                    gdouble s = *srow, d = *drow;

                    if (s >= -q) {
                        d -= s;
                        if (d < min)
                            min = d;
                    }
                }
            }
            rdata[i*xres + j] = min;
        }
        if (set_fraction && !set_fraction((i + 1.0)/yres))
            goto finish;
    }

    ok = TRUE;

finish:
    g_object_unref(rmsfield);
    g_object_unref(meanfield);
    g_object_unref(sphere);

    if (!ok)
        GWY_OBJECT_UNREF(rfield);

    return rfield;
}

static GwyDataField*
sphrev_make_sphere(gdouble radius, gint maxres)
{
    GwyDataField *sphere;
    gdouble *data;
    gint i, j, size, res, sc;

    size = GWY_ROUND(MIN(radius, maxres));
    res = 2*size + 1;
    sphere = gwy_data_field_new(res, res, 1.0, 1.0, FALSE);
    data = gwy_data_field_get_data(sphere);
    sc = size*res + size;

    if (radius/8 > maxres) {
        /* Pathological case: very flat sphere */
        for (i = 0; i <= size; i++) {
            gdouble u = i/radius;

            for (j = 0; j <= size; j++) {
                gdouble v = j/radius;
                gdouble r2 = u*u + v*v;
                gdouble z = r2/2.0*(1.0 + r2/4.0*(1 + r2/2.0));

                data[sc - res*i - j] = data[sc - res*i + j] = z;
                data[sc + res*i - j] = data[sc + res*i + j] = z;
            }
        }
    }
    else {
        /* Normal sphere */
        for (i = 0; i <= size; i++) {
            gdouble u = i/radius;

            for (j = 0; j <= size; j++) {
                gdouble v = j/radius;
                gdouble r2 = u*u + v*v;
                gdouble z = r2 > 1.0 ? 2.0 : 1.0 - sqrt(1.0 - r2);

                data[sc - res*i - j] = data[sc - res*i + j] = z;
                data[sc + res*i - j] = data[sc + res*i + j] = z;
            }
        }
    }

    return sphere;
}

static const gchar arc_radius_key[]     = "/module/arc_revolve/radius";
static const gchar arc_direction_key[]  = "/module/arc_revolve/direction";
static const gchar arc_do_extract_key[] = "/module/arc_revolve/do_extract";

static const gchar sph_radius_key[]     = "/module/sphere_revolve/radius";
static const gchar sph_do_extract_key[] = "/module/sphere_revolve/do_extract";

static void
sphrev_sanitize_args(SphrevArgs *args)
{
    args->size = CLAMP(args->size, 1, 1024);
    args->direction = CLAMP(args->direction, SPHREV_HORIZONTAL, SPHREV_BOTH);
    args->do_extract = !!args->do_extract;
}

static void
arcrev_load_args(GwyContainer *container, SphrevArgs *args)
{
    *args = sphrev_defaults;

    gwy_container_gis_double_by_name(container, arc_radius_key, &args->size);
    gwy_container_gis_enum_by_name(container, arc_direction_key,
                                   &args->direction);
    gwy_container_gis_boolean_by_name(container, arc_do_extract_key,
                                      &args->do_extract);
    sphrev_sanitize_args(args);
}

static void
arcrev_save_args(GwyContainer *container, SphrevArgs *args)
{
    gwy_container_set_double_by_name(container, arc_radius_key, args->size);
    gwy_container_set_enum_by_name(container, arc_direction_key,
                                   args->direction);
    gwy_container_set_boolean_by_name(container, arc_do_extract_key,
                                      args->do_extract);
}

static void
sphrev_load_args(GwyContainer *container, SphrevArgs *args)
{
    *args = sphrev_defaults;

    gwy_container_gis_double_by_name(container, sph_radius_key, &args->size);
    gwy_container_gis_boolean_by_name(container, sph_do_extract_key,
                                      &args->do_extract);
    sphrev_sanitize_args(args);
}

static void
sphrev_save_args(GwyContainer *container, SphrevArgs *args)
{
    gwy_container_set_double_by_name(container, sph_radius_key, args->size);
    gwy_container_set_boolean_by_name(container, sph_do_extract_key,
                                      args->do_extract);
}

/* vim: set cin et ts=4 sw=4 cino=>1s,e0,n0,f0,{0,}0,^0,\:1s,=0,g1s,h0,t0,+1s,c3,(0,u0 : */
