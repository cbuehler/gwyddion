/*
 *  $Id$
 *  Copyright (C) 2017 David Necas (Yeti).
 *  E-mail: yeti@gwyddion.net.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA 02110-1301, USA.
 */

#include "config.h"
#include <string.h>
#include <gtk/gtk.h>
#include <libgwyddion/gwymacros.h>
#include <libgwyddion/gwymath.h>
#include <libgwyddion/gwyrandgenset.h>
#include <libprocess/stats.h>
#include <libprocess/arithmetic.h>
#include <libprocess/filters.h>
#include <libgwydgets/gwystock.h>
#include <libgwymodule/gwymodule-process.h>
#include <app/gwyapp.h>
#include "dimensions.h"
#include "preview.h"

#define PILEUP_SYNTH_RUN_MODES (GWY_RUN_IMMEDIATE | GWY_RUN_INTERACTIVE)

#define DECLARE_OBJECT(name) \
    static gdouble render_base_##name(gdouble x, \
                                      gdouble y, \
                                      gdouble aspect); \
    static gboolean intersect_##name(GwyXYZ *ptl, \
                                     GwyXYZ *ptu, \
                                     gdouble aspect); \
    static gdouble getcov_##name(gdouble aspect)

enum {
    PAGE_DIMENSIONS = 0,
    PAGE_GENERATOR  = 1,
    PAGE_NPAGES
};

typedef enum {
    RNG_ID,
    RNG_WIDTH,
    RNG_ASPECT,
    RNG_ANGLE,
    RNG_NRNGS
} PileupSynthRng;

typedef enum {
    SCULPT_UP       = 0,
    SCULPT_DOWN     = 1,
    SCULPT_RANDOMLY = 2,
    SCULPT_NTYPES
} SculptType;

typedef enum {
    PILEUP_SYNTH_ELLIPSOID = 0,
    PILEUP_SYNTH_BAR       = 1,
    PILEUP_SYNTH_CYLINDER  = 2,
    PILEUP_SYNTH_NUGGET    = 3,
    PILEUP_SYNTH_HEXAGONAL = 4,
    PILEUP_SYNTH_NTYPES
} PileupSynthType;

typedef struct _PileupSynthControls PileupSynthControls;

/* Avoid reallocations by using a single buffer for objects that can only
 * grow. */
typedef struct {
    gint xres;
    gint yres;
    gsize size;
    gdouble *lower;   /* lower surface, as positive */
    gdouble *upper;   /* upper surface */
} PileupSynthObject;

typedef gdouble  (*PileupBaseFunc)     (gdouble x, gdouble y,
                                        gdouble aspect);
typedef gboolean (*PileupIntersectFunc)(GwyXYZ *ptl, GwyXYZ *ptu,
                                        gdouble aspect);
typedef gdouble  (*GetCoverageFunc)    (gdouble aspect);

/* This scheme makes the object type list easily reordeable in the GUI without
 * changing the ids.  */
typedef struct {
    PileupSynthType type;
    const gchar *name;
    PileupBaseFunc render_base;
    PileupIntersectFunc intersect;
    GetCoverageFunc get_coverage;
} PileupSynthFeature;

typedef struct {
    gint active_page;
    gint seed;
    gboolean randomize;
    gboolean update;
    PileupSynthType type;
    gboolean avoid_stacking;
    gdouble coverage;
    gdouble stickout;
    gdouble width;
    gdouble width_noise;
    gdouble aspect;
    gdouble aspect_noise;
    gdouble angle;
    gdouble angle_noise;
} PileupSynthArgs;

struct _PileupSynthControls {
    PileupSynthArgs *args;
    GwyDimensions *dims;
    GwyRandGenSet *rngset;
    GtkWidget *dialog;
    GtkWidget *view;
    GtkWidget *update;
    GtkWidget *update_now;
    GtkObject *seed;
    GtkWidget *randomize;
    GtkTable *table;
    GtkWidget *type;
    GtkWidget *avoid_stacking;
    GtkObject *width;
    GtkWidget *width_value;
    GtkWidget *width_units;
    GtkWidget *width_init;
    GtkObject *width_noise;
    GtkObject *aspect;
    GtkObject *aspect_noise;
    GtkObject *angle;
    GtkObject *angle_noise;
    GtkObject *coverage;
    GtkWidget *coverage_value;
    GtkWidget *coverage_units;
    GtkObject *stickout;
    GwyContainer *mydata;
    GwyDataField *surface;
    gboolean in_init;
    gdouble pxsize;
    gulong sid;
};

static gboolean   module_register        (void);
static void       pileup_synth           (GwyContainer *data,
                                          GwyRunType run);
static void       run_noninteractive     (PileupSynthArgs *args,
                                          const GwyDimensionArgs *dimsargs,
                                          GwyRandGenSet *rngset,
                                          GwyContainer *data,
                                          GwyDataField *dfield,
                                          gint oldid,
                                          GQuark quark);
static gboolean   pileup_synth_dialog    (PileupSynthArgs *args,
                                          GwyDimensionArgs *dimsargs,
                                          GwyRandGenSet *rngset,
                                          GwyContainer *data,
                                          GwyDataField *dfield,
                                          gint id);
static GtkWidget* shape_selector_new     (PileupSynthControls *controls);
static void       update_controls        (PileupSynthControls *controls,
                                          PileupSynthArgs *args);
static void       page_switched          (PileupSynthControls *controls,
                                          GtkNotebookPage *page,
                                          gint pagenum);
static void       update_values          (PileupSynthControls *controls);
static void       shape_selected         (GtkComboBox *combo,
                                          PileupSynthControls *controls);
static void       update_coverage_value  (PileupSynthControls *controls);
static void       pileup_synth_invalidate(PileupSynthControls *controls);
static gboolean   preview_gsource        (gpointer user_data);
static void       preview                (PileupSynthControls *controls);
static void       pileup_synth_do        (const PileupSynthArgs *args,
                                          GwyRandGenSet *rngset,
                                          GwyDataField *dfield);
static void       pileup_synth_iter      (GwyDataField *surface,
                                          gboolean *seen,
                                          PileupSynthObject *object,
                                          const PileupSynthArgs *args,
                                          GwyRandGenSet *rngset,
                                          gint nxcells,
                                          gint nycells,
                                          gint xoff,
                                          gint yoff,
                                          gint nobjects,
                                          gint *indices);
static void       pileup_one_object      (PileupSynthObject *object,
                                          GwyDataField *surface,
                                          gboolean *seen,
                                          PileupBaseFunc render_base,
                                          PileupIntersectFunc intersect,
                                          gdouble width,
                                          gdouble length,
                                          gdouble angle,
                                          gdouble stickout,
                                          gint j,
                                          gint i);
static gboolean   check_seen             (gboolean *seen,
                                          gint xres,
                                          gint yres,
                                          PileupSynthObject *object,
                                          gint joff,
                                          gint ioff);
static glong      calculate_n_objects    (const PileupSynthArgs *args,
                                          guint xres,
                                          guint yres);
static void       pileup_synth_load_args (GwyContainer *container,
                                          PileupSynthArgs *args,
                                          GwyDimensionArgs *dimsargs);
static void       pileup_synth_save_args (GwyContainer *container,
                                          const PileupSynthArgs *args,
                                          const GwyDimensionArgs *dimsargs);

#define GWY_SYNTH_CONTROLS PileupSynthControls
#define GWY_SYNTH_INVALIDATE(controls) \
    update_coverage_value(controls); \
    pileup_synth_invalidate(controls)

#include "synth.h"

DECLARE_OBJECT(ellipsoid);
DECLARE_OBJECT(bar);
DECLARE_OBJECT(cylinder);
DECLARE_OBJECT(nugget);
DECLARE_OBJECT(hexagonal);

static const PileupSynthArgs pileup_synth_defaults = {
    PAGE_DIMENSIONS,
    42, TRUE, TRUE,
    PILEUP_SYNTH_ELLIPSOID, FALSE,
    1.0, 0.0,
    20.0, 0.0,
    2.0, 0.0,
    0.0, 0.0,
};

static const GwyDimensionArgs dims_defaults = GWY_DIMENSION_ARGS_INIT;

static const PileupSynthFeature features[] = {
    {
        PILEUP_SYNTH_ELLIPSOID, N_("Ellipsoids"),
        &render_base_ellipsoid, &intersect_ellipsoid, &getcov_ellipsoid,
    },
    {
        PILEUP_SYNTH_BAR, N_("Bars"),
        &render_base_bar, &intersect_bar, &getcov_bar,
    },
    {
        PILEUP_SYNTH_CYLINDER, N_("Cylinders"),
        &render_base_cylinder, &intersect_cylinder, &getcov_cylinder,
    },
    {
        PILEUP_SYNTH_NUGGET, N_("Nuggets"),
        &render_base_nugget, &intersect_nugget, &getcov_nugget,
    },
    {
        PILEUP_SYNTH_HEXAGONAL, N_("Hexagonal rods"),
        &render_base_hexagonal, &intersect_hexagonal, &getcov_hexagonal,
    },
};

static GwyModuleInfo module_info = {
    GWY_MODULE_ABI_VERSION,
    &module_register,
    N_("Generates randomly patterned surfaces by piling up "
       "geometrical shapes."),
    "Yeti <yeti@gwyddion.net>",
    "1.0",
    "David Nečas (Yeti)",
    "2017",
};

GWY_MODULE_QUERY2(module_info, pileup_synth)

static gboolean
module_register(void)
{
    gwy_process_func_register("pileup_synth",
                              (GwyProcessFunc)&pileup_synth,
                              N_("/S_ynthetic/_Deposition/_Pile Up..."),
                              GWY_STOCK_SYNTHETIC_PILEUP,
                              PILEUP_SYNTH_RUN_MODES,
                              0,
                              N_("Generate surface of randomly "
                                 "piled up shapes"));

    return TRUE;
}

static void
pileup_synth(GwyContainer *data, GwyRunType run)
{
    PileupSynthArgs args;
    GwyDimensionArgs dimsargs;
    GwyRandGenSet *rngset;
    GwyDataField *dfield;
    GQuark quark;
    gint id;

    g_return_if_fail(run & PILEUP_SYNTH_RUN_MODES);
    pileup_synth_load_args(gwy_app_settings_get(), &args, &dimsargs);
    gwy_app_data_browser_get_current(GWY_APP_DATA_FIELD, &dfield,
                                     GWY_APP_DATA_FIELD_ID, &id,
                                     GWY_APP_DATA_FIELD_KEY, &quark,
                                     0);

    rngset = gwy_rand_gen_set_new(RNG_NRNGS);
    if (run == GWY_RUN_IMMEDIATE
        || pileup_synth_dialog(&args, &dimsargs, rngset, data, dfield, id))
        run_noninteractive(&args, &dimsargs, rngset, data, dfield, id, quark);

    if (run == GWY_RUN_INTERACTIVE)
        pileup_synth_save_args(gwy_app_settings_get(), &args, &dimsargs);

    gwy_rand_gen_set_free(rngset);
    gwy_dimensions_free_args(&dimsargs);
}

static void
run_noninteractive(PileupSynthArgs *args,
                   const GwyDimensionArgs *dimsargs,
                   GwyRandGenSet *rngset,
                   GwyContainer *data,
                   GwyDataField *dfield,
                   gint oldid,
                   GQuark quark)
{
    GwySIUnit *siunit;
    gboolean replace = dimsargs->replace && dfield;
    gboolean add = dimsargs->add && dfield;
    gint newid;

    if (args->randomize)
        args->seed = g_random_int() & 0x7fffffff;

    if (replace) {
        /* Always take a reference so that we can always unref. */
        g_object_ref(dfield);

        gwy_app_undo_qcheckpointv(data, 1, &quark);
        if (!add)
            gwy_data_field_clear(dfield);

        gwy_app_channel_log_add_proc(data, oldid, oldid);
    }
    else {
        if (add) {
            dfield = gwy_data_field_duplicate(dfield);
        }
        else {
            gdouble mag = pow10(dimsargs->xypow10) * dimsargs->measure;
            dfield = gwy_data_field_new(dimsargs->xres, dimsargs->yres,
                                        mag*dimsargs->xres, mag*dimsargs->yres,
                                        TRUE);

            siunit = gwy_data_field_get_si_unit_xy(dfield);
            gwy_si_unit_set_from_string(siunit, dimsargs->xyunits);
        }
    }

    pileup_synth_do(args, rngset, dfield);

    if (!replace) {
        if (data) {
            newid = gwy_app_data_browser_add_data_field(dfield, data, TRUE);
            if (oldid != -1)
                gwy_app_sync_data_items(data, data, oldid, newid, FALSE,
                                        GWY_DATA_ITEM_GRADIENT,
                                        0);
        }
        else {
            newid = 0;
            data = gwy_container_new();
            gwy_container_set_object(data, gwy_app_get_data_key_for_id(newid),
                                     dfield);
            gwy_app_data_browser_add(data);
            gwy_app_data_browser_reset_visibility(data,
                                                  GWY_VISIBILITY_RESET_SHOW_ALL);
            g_object_unref(data);
        }

        gwy_app_set_data_field_title(data, newid, _("Generated"));
        gwy_app_channel_log_add_proc(data, add ? oldid : -1, newid);
    }
    g_object_unref(dfield);
}

static gboolean
pileup_synth_dialog(PileupSynthArgs *args,
                    GwyDimensionArgs *dimsargs,
                    GwyRandGenSet *rngset,
                    GwyContainer *data,
                    GwyDataField *dfield_template,
                    gint id)
{
    GtkWidget *dialog, *table, *vbox, *hbox, *notebook;
    PileupSynthControls controls;
    GwyDataField *dfield;
    gboolean finished;
    gint response;
    gint row, i;

    gwy_clear(&controls, 1);
    controls.in_init = TRUE;
    controls.args = args;
    controls.rngset = rngset;
    controls.pxsize = 1.0;
    dialog = gtk_dialog_new_with_buttons(_("Pile Up Shapes"),
                                         NULL, 0,
                                         _("_Reset"), RESPONSE_RESET,
                                         GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
                                         GTK_STOCK_OK, GTK_RESPONSE_OK,
                                         NULL);
    gtk_dialog_set_default_response(GTK_DIALOG(dialog), GTK_RESPONSE_OK);
    gwy_help_add_to_proc_dialog(GTK_DIALOG(dialog), GWY_HELP_DEFAULT);
    controls.dialog = dialog;

    hbox = gtk_hbox_new(FALSE, 2);
    gtk_box_pack_start(GTK_BOX(GTK_DIALOG(dialog)->vbox), hbox,
                       FALSE, FALSE, 4);

    vbox = gtk_vbox_new(FALSE, 4);
    gtk_box_pack_start(GTK_BOX(hbox), vbox, FALSE, FALSE, 4);

    controls.mydata = gwy_container_new();
    dfield = gwy_data_field_new(PREVIEW_SIZE, PREVIEW_SIZE,
                                dimsargs->measure*PREVIEW_SIZE,
                                dimsargs->measure*PREVIEW_SIZE,
                                TRUE);
    gwy_container_set_object_by_name(controls.mydata, "/0/data", dfield);
    if (dfield_template) {
        gwy_app_sync_data_items(data, controls.mydata, id, 0, FALSE,
                                GWY_DATA_ITEM_PALETTE,
                                0);
        controls.surface = gwy_synth_surface_for_preview(dfield_template,
                                                         PREVIEW_SIZE);
    }
    controls.view = create_preview(controls.mydata, 0, PREVIEW_SIZE, FALSE);
    gtk_box_pack_start(GTK_BOX(vbox), controls.view, FALSE, FALSE, 0);

    gtk_box_pack_start(GTK_BOX(vbox),
                       gwy_synth_instant_updates_new(&controls,
                                                     &controls.update_now,
                                                     &controls.update,
                                                     &args->update),
                       FALSE, FALSE, 0);
    g_signal_connect_swapped(controls.update_now, "clicked",
                             G_CALLBACK(preview), &controls);

    gtk_box_pack_start(GTK_BOX(vbox),
                       gwy_synth_random_seed_new(&controls,
                                                 &controls.seed, &args->seed),
                       FALSE, FALSE, 0);

    controls.randomize = gwy_synth_randomize_new(&args->randomize);
    gtk_box_pack_start(GTK_BOX(vbox), controls.randomize, FALSE, FALSE, 0);

    notebook = gtk_notebook_new();
    gtk_box_pack_start(GTK_BOX(hbox), notebook, TRUE, TRUE, 4);
    g_signal_connect_swapped(notebook, "switch-page",
                             G_CALLBACK(page_switched), &controls);

    controls.dims = gwy_dimensions_new(dimsargs, dfield_template);
    gtk_notebook_append_page(GTK_NOTEBOOK(notebook),
                             gwy_dimensions_get_widget(controls.dims),
                             gtk_label_new(_("Dimensions")));
    if (controls.dims->add)
        g_signal_connect_swapped(controls.dims->add, "toggled",
                                 G_CALLBACK(pileup_synth_invalidate), &controls);

    /* Hide the z units, they must be the same as xy. */
    table = controls.dims->table;
    gtk_container_child_get(GTK_CONTAINER(table), controls.dims->zunits,
                            "top-attach", &i, NULL);
    gtk_widget_set_no_show_all(controls.dims->zunits, TRUE);
    gtk_widget_set_no_show_all(gwy_table_get_child_widget(table, i, 0), TRUE);
    gtk_widget_set_no_show_all(gwy_table_get_child_widget(table, i, 1), TRUE);

    table = gtk_table_new(16, 3, FALSE);
    controls.table = GTK_TABLE(table);
    gtk_table_set_row_spacings(GTK_TABLE(table), 2);
    gtk_table_set_col_spacings(GTK_TABLE(table), 6);
    gtk_container_set_border_width(GTK_CONTAINER(table), 4);
    gtk_notebook_append_page(GTK_NOTEBOOK(notebook), table,
                             gtk_label_new(_("Generator")));
    row = 0;

    controls.type = shape_selector_new(&controls);
    gwy_table_attach_adjbar(table, row, _("_Shape:"), NULL,
                            GTK_OBJECT(controls.type),
                            GWY_HSCALE_WIDGET_NO_EXPAND);
    row++;

    controls.coverage = gtk_adjustment_new(args->coverage,
                                           0.001, 30.0, 0.001, 1.0, 0);
    g_object_set_data(G_OBJECT(controls.coverage), "target", &args->coverage);
    gwy_table_attach_adjbar(table, row, _("Co_verage:"), NULL,
                            controls.coverage, GWY_HSCALE_SQRT);
    g_signal_connect_swapped(controls.coverage, "value-changed",
                             G_CALLBACK(gwy_synth_double_changed), &controls);
    row++;

    controls.coverage_value = gtk_label_new(NULL);
    gtk_misc_set_alignment(GTK_MISC(controls.coverage_value), 1.0, 0.5);
    gtk_table_attach(controls.table, controls.coverage_value,
                     1, 2, row, row+1, GTK_FILL, 0, 0, 0);

    controls.coverage_units = gtk_label_new(_("obj."));
    gtk_misc_set_alignment(GTK_MISC(controls.coverage_units), 0.0, 0.5);
    gtk_table_attach(GTK_TABLE(table), controls.coverage_units,
                     2, 3, row, row+1, GTK_FILL, 0, 0, 0);
    row++;

    gtk_table_set_row_spacing(GTK_TABLE(table), row-1, 8);
    gtk_table_attach(GTK_TABLE(table), gwy_label_new_header(_("Size")),
                     0, 2, row, row+1, GTK_FILL, 0, 0, 0);
    row++;

    controls.width = gtk_adjustment_new(args->width, 1.0, 1000.0, 0.1, 10.0, 0);
    row = gwy_synth_attach_lateral(&controls, row, controls.width, &args->width,
                                   _("_Width:"), GWY_HSCALE_LOG,
                                   NULL,
                                   &controls.width_value, &controls.width_units);

    row = gwy_synth_attach_variance(&controls, row,
                                    &controls.width_noise, &args->width_noise);

    gtk_table_set_row_spacing(GTK_TABLE(table), row-1, 8);
    gtk_table_attach(controls.table, gwy_label_new_header(_("Aspect Ratio")),
                     0, 2, row, row+1, GTK_FILL, 0, 0, 0);
    row++;

    controls.aspect = gtk_adjustment_new(args->aspect,
                                         1.0, 8.0, 0.01, 1.0, 0);
    g_object_set_data(G_OBJECT(controls.aspect), "target", &args->aspect);
    gwy_table_attach_adjbar(table, row, _("_Aspect ratio:"), NULL,
                            controls.aspect, GWY_HSCALE_LOG);
    g_signal_connect_swapped(controls.aspect, "value-changed",
                             G_CALLBACK(gwy_synth_double_changed), &controls);
    row++;

    row = gwy_synth_attach_variance(&controls, row,
                                    &controls.aspect_noise,
                                    &args->aspect_noise);

    gtk_table_set_row_spacing(GTK_TABLE(table), row-1, 8);
    gtk_table_attach(controls.table, gwy_label_new_header(_("Placement")),
                     0, 2, row, row+1, GTK_FILL, 0, 0, 0);
    row++;

    controls.stickout = gtk_adjustment_new(args->stickout,
                                           -1.0, 1.0, 0.001, 1.0, 0);
    g_object_set_data(G_OBJECT(controls.stickout), "target", &args->stickout);
    gwy_table_attach_adjbar(table, row, _("Colum_narity:"), NULL,
                            controls.stickout, GWY_HSCALE_LINEAR);
    g_signal_connect_swapped(controls.stickout, "value-changed",
                             G_CALLBACK(gwy_synth_double_changed), &controls);
    row++;

    controls.avoid_stacking
        = gtk_check_button_new_with_mnemonic(_("_Avoid stacking"));
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(controls.avoid_stacking),
                                 args->avoid_stacking);
    g_object_set_data(G_OBJECT(controls.avoid_stacking),
                      "target", &args->avoid_stacking);
    gtk_table_attach(GTK_TABLE(table), controls.avoid_stacking,
                     0, 2, row, row+1, GTK_FILL, 0, 0, 0);
    g_signal_connect_swapped(controls.avoid_stacking, "toggled",
                             G_CALLBACK(gwy_synth_boolean_changed), &controls);
    row++;

    row = gwy_synth_attach_orientation(&controls, row,
                                       &controls.angle, &args->angle);
    row = gwy_synth_attach_variance(&controls, row,
                                    &controls.angle_noise, &args->angle_noise);

    gtk_widget_show_all(dialog);
    controls.in_init = FALSE;
    /* Must be done when widgets are shown, see GtkNotebook docs */
    gtk_notebook_set_current_page(GTK_NOTEBOOK(notebook), args->active_page);
    update_values(&controls);
    pileup_synth_invalidate(&controls);

    finished = FALSE;
    while (!finished) {
        response = gtk_dialog_run(GTK_DIALOG(dialog));
        switch (response) {
            case GTK_RESPONSE_CANCEL:
            case GTK_RESPONSE_DELETE_EVENT:
            case GTK_RESPONSE_OK:
            gtk_widget_destroy(dialog);
            case GTK_RESPONSE_NONE:
            finished = TRUE;
            break;

            case RESPONSE_RESET:
            {
                gboolean temp = args->update;
                gint temp2 = args->active_page;
                *args = pileup_synth_defaults;
                args->active_page = temp2;
                args->update = temp;
            }
            controls.in_init = TRUE;
            update_controls(&controls, args);
            controls.in_init = FALSE;
            if (args->update)
                preview(&controls);
            break;

            default:
            g_assert_not_reached();
            break;
        }
    }

    if (controls.sid) {
        g_source_remove(controls.sid);
        controls.sid = 0;
    }
    g_object_unref(controls.mydata);
    GWY_OBJECT_UNREF(controls.surface);
    gwy_dimensions_free(controls.dims);

    return response == GTK_RESPONSE_OK;
}

static const PileupSynthFeature*
get_feature(guint type)
{
    guint i;

    for (i = 0; i < G_N_ELEMENTS(features); i++) {
        if (features[i].type == type)
            return features + i;
    }
    g_warning("Unknown feature %u\n", type);

    return features + 0;
}

static GtkWidget*
shape_selector_new(PileupSynthControls *controls)
{
    GtkWidget *combo;
    GwyEnum *model;
    guint n, i;

    n = G_N_ELEMENTS(features);
    model = g_new(GwyEnum, n);
    for (i = 0; i < n; i++) {
        model[i].value = features[i].type;
        model[i].name = features[i].name;
    }

    combo = gwy_enum_combo_box_new(model, n,
                                   G_CALLBACK(shape_selected), controls,
                                   controls->args->type, TRUE);
    g_object_weak_ref(G_OBJECT(combo), (GWeakNotify)g_free, model);

    return combo;
}

static void
update_controls(PileupSynthControls *controls,
                PileupSynthArgs *args)
{
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(controls->update),
                                 args->update);
    gtk_adjustment_set_value(GTK_ADJUSTMENT(controls->seed), args->seed);
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(controls->randomize),
                                 args->randomize);
    gtk_adjustment_set_value(GTK_ADJUSTMENT(controls->coverage),
                             args->coverage);
    gtk_adjustment_set_value(GTK_ADJUSTMENT(controls->width), args->width);
    gtk_adjustment_set_value(GTK_ADJUSTMENT(controls->width_noise),
                             args->width_noise);
    gtk_adjustment_set_value(GTK_ADJUSTMENT(controls->aspect), args->aspect);
    gtk_adjustment_set_value(GTK_ADJUSTMENT(controls->aspect_noise),
                             args->aspect_noise);
    gtk_adjustment_set_value(GTK_ADJUSTMENT(controls->angle), args->angle);
    gtk_adjustment_set_value(GTK_ADJUSTMENT(controls->angle_noise),
                             args->angle_noise);
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(controls->avoid_stacking),
                                 args->avoid_stacking);
    gwy_enum_combo_box_set_active(GTK_COMBO_BOX(controls->type), args->type);
}

static void
page_switched(PileupSynthControls *controls,
              G_GNUC_UNUSED GtkNotebookPage *page,
              gint pagenum)
{
    if (controls->in_init)
        return;

    controls->args->active_page = pagenum;
    if (pagenum == PAGE_GENERATOR)
        update_values(controls);
}

static void
update_values(PileupSynthControls *controls)
{
    GwyDimensions *dims = controls->dims;

    controls->pxsize = dims->args->measure * pow10(dims->args->xypow10);
    gtk_label_set_markup(GTK_LABEL(controls->width_units),
                         dims->xyvf->units);

    gwy_synth_update_lateral(controls, GTK_ADJUSTMENT(controls->width));
    update_coverage_value(controls);
}

static void
shape_selected(GtkComboBox *combo,
               PileupSynthControls *controls)
{
    controls->args->type = gwy_enum_combo_box_get_active(combo);
    update_coverage_value(controls);
    pileup_synth_invalidate(controls);
}

static void
update_coverage_value(PileupSynthControls *controls)
{
    glong nobjects;
    guchar buf[32];

    if (controls->in_init)
        return;

    nobjects = calculate_n_objects(controls->args,
                                   controls->dims->args->xres,
                                   controls->dims->args->yres);
    g_snprintf(buf, sizeof(buf), "%ld", nobjects);
    gtk_label_set_text(GTK_LABEL(controls->coverage_value), buf);
}

static void
pileup_synth_invalidate(PileupSynthControls *controls)
{
    /* create preview if instant updates are on */
    if (controls->args->update && !controls->in_init && !controls->sid) {
        controls->sid = g_idle_add_full(G_PRIORITY_LOW, preview_gsource,
                                        controls, NULL);
    }
}

static gboolean
preview_gsource(gpointer user_data)
{
    PileupSynthControls *controls = (PileupSynthControls*)user_data;
    controls->sid = 0;

    preview(controls);

    return FALSE;
}

static void
preview(PileupSynthControls *controls)
{
    PileupSynthArgs *args = controls->args;
    GwyDataField *dfield;

    dfield = GWY_DATA_FIELD(gwy_container_get_object_by_name(controls->mydata,
                                                             "/0/data"));
    if (controls->dims->args->add && controls->surface)
        gwy_data_field_copy(controls->surface, dfield, FALSE);
    else
        gwy_data_field_clear(dfield);

    pileup_synth_do(args, controls->rngset, dfield);
}

static void
pileup_synth_do(const PileupSynthArgs *args,
                GwyRandGenSet *rngset,
                GwyDataField *dfield)
{
    PileupSynthObject object = { 0, 0, 0, NULL, NULL };
    gint *indices;
    glong nobjects;
    gint xres, yres, nxcells, nycells, ncells, cellside, niters, i, extend;
    GwyDataField *workspace = NULL;
    gdouble h;
    gboolean *seen = NULL;

    /* The units must be the same. */
    gwy_serializable_clone(G_OBJECT(gwy_data_field_get_si_unit_xy(dfield)),
                           G_OBJECT(gwy_data_field_get_si_unit_z(dfield)));

    /* When adding objects to existing surface which is not level the shapes
     * spill across boundaries.  Prevent that.  This means there is no parity
     * between standalone and add-to-surface object sets, but this is not a
     * smooth change anyway.  */
    if (gwy_data_field_get_rms(dfield)) {
        extend = GWY_ROUND(0.6*args->width * args->aspect);
        workspace = gwy_data_field_extend(dfield,
                                          extend, extend, extend, extend,
                                          GWY_EXTERIOR_BORDER_EXTEND, 0.0,
                                          FALSE);
        GWY_SWAP(GwyDataField*, dfield, workspace);
    }

    xres = gwy_data_field_get_xres(dfield);
    yres = gwy_data_field_get_yres(dfield);
    cellside = sqrt(sqrt(xres*yres));
    nxcells = (xres + cellside-1)/cellside;
    nycells = (yres + cellside-1)/cellside;
    ncells = nxcells*nycells;
    nobjects = calculate_n_objects(args, xres, yres);
    niters = nobjects/ncells;

    /* Scale initial surface to pixel-sized cubes.  We measure all shape
     * parameters in pixels.   This effectiely mean scaling real x and y
     * coordinates by 1/h.  So we must do the same with z.  */
    h = sqrt(gwy_data_field_get_xreal(dfield)/xres
             *gwy_data_field_get_yreal(dfield)/yres);
    gwy_data_field_multiply(dfield, 1.0/h);

    if (args->avoid_stacking)
        seen = g_new0(gboolean, xres*yres);

    gwy_rand_gen_set_init(rngset, args->seed);
    indices = g_new(gint, ncells);

    for (i = 0; i < niters; i++) {
        pileup_synth_iter(dfield, seen, &object, args, rngset,
                          nxcells, nycells, i+1, i+1, ncells, indices);
    }
    pileup_synth_iter(dfield, seen, &object, args, rngset,
                      nxcells, nycells, 0, 0, nobjects % ncells, indices);

    g_free(object.lower);
    g_free(indices);
    g_free(seen);

    /* Scale back to physical. */
    gwy_data_field_multiply(dfield, h);

    if (workspace) {
        GWY_SWAP(GwyDataField*, dfield, workspace);
        gwy_data_field_area_copy(workspace, dfield,
                                 extend, extend,
                                 xres - 2*extend, yres - 2*extend,
                                 0, 0);
        g_object_unref(workspace);
    }
    gwy_data_field_data_changed(dfield);
}

static void
pileup_synth_iter(GwyDataField *surface,
                  gboolean *seen,
                  PileupSynthObject *object,
                  const PileupSynthArgs *args,
                  GwyRandGenSet *rngset,
                  gint nxcells,
                  gint nycells,
                  gint xoff,
                  gint yoff,
                  gint nobjects,
                  gint *indices)
{
    gint xres, yres, ncells, k;
    const PileupSynthFeature *feature;
    PileupBaseFunc render_base;
    PileupIntersectFunc intersect;
    GRand *rngid;

    g_return_if_fail(nobjects <= nxcells*nycells);

    feature = get_feature(args->type);
    render_base = feature->render_base;
    intersect = feature->intersect;
    xres = gwy_data_field_get_xres(surface);
    yres = gwy_data_field_get_yres(surface);
    ncells = nxcells*nycells;

    for (k = 0; k < ncells; k++)
        indices[k] = k;

    rngid = gwy_rand_gen_set_rng(rngset, RNG_ID);
    for (k = 0; k < nobjects; k++) {
        gdouble width, aspect, angle, length;
        gint id, i, j, from, to;

        id = g_rand_int_range(rngid, 0, ncells - k);
        i = indices[id]/nxcells;
        j = indices[id] % nxcells;
        indices[id] = indices[ncells-1 - k];

        width = args->width;
        if (args->width_noise)
            width *= exp(gwy_rand_gen_set_gaussian(rngset, RNG_WIDTH,
                                                   args->width_noise));

        aspect = args->aspect;
        if (args->aspect_noise) {
            aspect *= exp(gwy_rand_gen_set_gaussian(rngset, RNG_ASPECT,
                                                    args->aspect_noise));
            aspect = MAX(aspect, 1.0/aspect);
        }
        length = width*aspect;

        angle = args->angle;
        if (args->angle_noise)
            angle += gwy_rand_gen_set_gaussian(rngset, RNG_ANGLE,
                                               2*args->angle_noise);

        from = (j*xres + nxcells/2)/nxcells;
        to = (j*xres + xres + nxcells/2)/nxcells;
        to = MIN(to, xres);
        j = from + xoff + g_rand_int_range(rngid, 0, to - from);

        from = (i*yres + nycells/2)/nycells;
        to = (i*yres + yres + nycells/2)/nycells;
        to = MIN(to, yres);
        i = from + yoff + g_rand_int_range(rngid, 0, to - from);

        pileup_one_object(object, surface, seen,
                          render_base, intersect,
                          width, length, angle, args->stickout, j, i);
    }
}

static gboolean
check_seen(gboolean *seen, gint xres, gint yres,
           PileupSynthObject *object, gint joff, gint ioff)
{
    gint kxres, kyres;
    gint i, j;
    const gdouble *z;
    gboolean *srow, *end;

    kxres = object->xres;
    kyres = object->yres;

    z = object->upper;
    for (i = 0; i < kyres; i++) {
        srow = seen + ((ioff + i) % yres)*xres;
        end = srow + xres-1;
        srow += joff;
        for (j = 0; j < kxres; j++, z++, srow++) {
            if (*z && *srow)
               return FALSE;
            if (srow == end)
                srow -= xres;
        }
    }

    z = object->upper;
    for (i = 0; i < kyres; i++) {
        srow = seen + ((ioff + i) % yres)*xres;
        end = srow + xres-1;
        srow += joff;
        for (j = 0; j < kxres; j++, z++, srow++) {
            if (*z)
                *srow = TRUE;
            if (srow == end)
                srow -= xres;
        }
    }
    return TRUE;
}

static inline void
pileup_synth_object_resize(PileupSynthObject *object,
                         gint xres, gint yres)
{
    object->xres = xres;
    object->yres = yres;
    if ((guint)(xres*yres) > object->size) {
        g_free(object->lower);
        object->lower = g_new(gdouble, 2*xres*yres);
        object->size = xres*yres;
    }
    object->upper = object->lower + xres*yres;
}

/* Rotate the xy plane to plane with slopes bx and by.
 * Quantities b and bh1 are precalculated: b = √(bx² + by²), bh1 = √(b² + 1). */
static inline void
tilt_point(GwyXYZ *v, gdouble bx, gdouble by, gdouble b, gdouble bh1)
{
    /* Use Rodrigues' rotation formula, factoring out 1/bh1 = cos ϑ. */
    GwyXYZ vrot;
    gdouble q;

    if (b < 1e-9)
        return;

    /* v cos ϑ */
    vrot = *v;

    /* k×v sin ϑ */
    vrot.x += bx*v->z;
    vrot.y += by*v->z;
    vrot.z -= bx*v->x + by*v->y;

    /* k (k.v) (1 - cos θ) */
    q = (bx*v->y - by*v->x)/(1.0 + bh1);
    vrot.x -= q*by;
    vrot.y += q*bx;

    /* Multiply with the common factor 1/bh1. */
    v->x = vrot.x/bh1;
    v->y = vrot.y/bh1;
    v->z = vrot.z/bh1;
}

/* Rotate the vertical plane by angle α (sine and cosine provided). */
static inline void
rotate_point(GwyXYZ *v, gdouble ca, gdouble sa)
{
    gdouble x, y;

    x = ca*v->x - sa*v->y;
    y = sa*v->x + ca*v->y;

    v->x = x;
    v->y = y;
}

/* Scale vector by given factors. */
static inline void
scale_point(GwyXYZ *v, gdouble xsize, gdouble ysize, gdouble height)
{
    v->x *= xsize;
    v->y *= ysize;
    v->z *= height;
}

static inline gdouble
find_base_level_melted(const PileupSynthObject *object,
                       const gdouble *surface,
                       gint xres, gint yres,
                       gint joff, gint ioff)
{
    gint kxres = object->xres, kyres = object->yres;
    gint i, j, w;
    const gdouble *srow, *end, *zl;
    gdouble m, v;

    zl = object->lower;
    w = 0;
    m = 0.0;
    for (i = 0; i < kyres; i++) {
        srow = surface + ((ioff + i) % yres)*xres;
        end = srow + xres-1;
        srow += joff;
        for (j = 0; j < kxres; j++, zl++, srow++) {
            v = *zl;
            if (v) {
                m += (*srow) + (*zl);
                w++;
            }
            if (srow == end)
                srow -= xres;
        }
    }
    if (!w)
        return 0.0;

    return m/w;
}

static inline gdouble
find_base_level_stickout(const PileupSynthObject *object,
                         const gdouble *surface,
                         gint xres, gint yres,
                         gint joff, gint ioff)
{
    gint kxres = object->xres, kyres = object->yres;
    gint i, j;
    const gdouble *srow, *end, *zl;
    gdouble m;

    zl = object->lower;
    m = -G_MAXDOUBLE;
    for (i = 0; i < kyres; i++) {
        srow = surface + ((ioff + i) % yres)*xres;
        end = srow + xres-1;
        srow += joff;
        for (j = 0; j < kxres; j++, zl++, srow++) {
            if (*zl && *srow + *zl > m)
                m = *srow + *zl;
            if (srow == end)
                srow -= xres;
        }
    }
    return (m == -G_MAXDOUBLE) ? 0.0 : m;
}

static inline gdouble
find_base_level_bury(const PileupSynthObject *object,
                     const gdouble *surface,
                     gint xres, gint yres,
                     gint joff, gint ioff)
{
    gint kxres = object->xres, kyres = object->yres;
    gint i, j;
    const gdouble *srow, *end, *zl;
    gdouble m;

    zl = object->lower;
    m = G_MAXDOUBLE;
    for (i = 0; i < kyres; i++) {
        srow = surface + ((ioff + i) % yres)*xres;
        end = srow + xres-1;
        srow += joff;
        for (j = 0; j < kxres; j++, zl++, srow++) {
            if (*zl && *srow < m)
                m = *srow;
            if (srow == end)
                srow -= xres;
        }
    }
    return (m == G_MAXDOUBLE) ? 0.0 : m;
}

static gdouble
find_base_level(const PileupSynthObject *object,
                const gdouble *surface,
                gint xres, gint yres,
                gint joff, gint ioff,
                gdouble stickout)
{
    gdouble m, mx, sa;

    if (stickout > 1.0 - 1e-6)
        return find_base_level_stickout(object, surface, xres, yres, joff, ioff);
    if (stickout < -1.0 + 1e-6)
        return find_base_level_bury(object, surface, xres, yres, joff, ioff);

    m = find_base_level_melted(object, surface, xres, yres, joff, ioff);
    sa = fabs(stickout);
    if (fabs(sa) < 1e-6)
        return m;

    if (stickout > 0.0)
        mx = find_base_level_stickout(object, surface, xres, yres, joff, ioff);
    else
        mx = find_base_level_bury(object, surface, xres, yres, joff, ioff);

    return sa*mx + (1.0 - sa)*m;
}

static inline void
find_weighted_mean_plane(const PileupSynthObject *object,
                         const gdouble *surface,
                         gint xres, gint yres,
                         gint joff, gint ioff,
                         gdouble *pbx, gdouble *pby)
{
    gint kxres = object->xres, kyres = object->yres;
    gint i, j;
    const gdouble *srow, *end, *zu, *zl;
    gdouble cx, cy, cz, x, y, z, sxz, syz, sxx, sxy, syy, w, v, D;

    zu = object->upper;
    zl = object->lower;
    cx = cy = cz = w = 0.0;
    for (i = 0; i < kyres; i++) {
        srow = surface + ((ioff + i) % yres)*xres;
        end = srow + xres-1;
        srow += joff;
        for (j = 0; j < kxres; j++, zl++, zu++, srow++) {
            v = *zl + *zu;
            w += v;
            cx += v*j;
            cy += v*i;
            cz += v*(*srow);
            if (srow == end)
                srow -= xres;
        }
    }
    if (!w) {
        *pbx = *pby = 0.0;
        return;
    }

    cx /= w;
    cy /= w;
    cz /= w;
    zu = object->upper;
    zl = object->lower;
    sxx = sxy = syy = sxz = syz = 0.0;
    for (i = 0; i < kyres; i++) {
        srow = surface + ((ioff + i) % yres)*xres;
        end = srow + xres-1;
        srow += joff;
        for (j = 0; j < kxres; j++, zl++, zu++, srow++) {
            v = *zl + *zu;
            x = j - cx;
            y = i - cy;
            z = *srow - cz;
            sxz += v*x*z;
            syz += v*y*z;
            sxx += v*x*x;
            sxy += v*x*y;
            syy += v*y*y;
            if (srow == end)
                srow -= xres;
        }
    }

    D = sxx*syy - sxy*sxy;
    if (fabs(D) > 1e-12*w*w) {
        *pbx = (sxz*syy - syz*sxy)/D;
        *pby = (syz*sxx - sxz*sxy)/D;
    }
    else
        *pbx = *pby = 0.0;
}

static void
make_tilted_bounding_box(PileupSynthObject *object,
                         gdouble width2, gdouble length2,
                         gdouble angle, gdouble bx, gdouble by)
{
    gdouble ca, sa, b, bh1, xmin, xmax, ymin, ymax;
    gint xres, yres;
    GwyXYZ v;
    guint i;

    ca = cos(angle);
    sa = sin(angle);
    b = sqrt(bx*bx + by*by);
    bh1 = sqrt(b*b + 1.0);

    xmin = ymin = G_MAXDOUBLE;
    xmax = ymax = -G_MAXDOUBLE;

    for (i = 0; i < 8; i++) {
        v.x = (i & 1) ? 1.0 : -1.0;
        v.y = (i & 2) ? 1.0 : -1.0;
        v.z = (i & 4) ? 1.0 : -1.0;
        /* Do everything with opposite signs, i.e. compensate the un-. */
        scale_point(&v, length2, width2, width2);
        rotate_point(&v, ca, sa);
        tilt_point(&v, -bx, -by, b, bh1);
        if (v.x > xmax)
            xmax = v.x;
        if (v.x < xmin)
            xmin = v.x;
        if (v.y > ymax)
            ymax = v.y;
        if (v.y < ymin)
            ymin = v.y;
    }

    xres = 2*(gint)ceil(MAX(xmax, -xmin) + 1.0) | 1;
    yres = 2*(gint)ceil(MAX(ymax, -ymin) + 1.0) | 1;
    pileup_synth_object_resize(object, xres, yres);
}

static inline void
middle_point(const GwyXYZ *a, const GwyXYZ *b, GwyXYZ *c)
{
    c->x = 0.5*(a->x + b->x);
    c->y = 0.5*(a->y + b->y);
    c->z = 0.5*(a->z + b->z);
}

static inline void
point_on_line(const GwyXYZ *c, const GwyXYZ *v, gdouble t, GwyXYZ *pt)
{
    pt->x = c->x + t*v->x;
    pt->y = c->y + t*v->y;
    pt->z = c->z + t*v->z;
}

static inline void
vecdiff(const GwyXYZ *a, const GwyXYZ *b, GwyXYZ *c)
{
    c->x = a->x - b->x;
    c->y = a->y - b->y;
    c->z = a->z - b->z;
}

static inline gdouble
dotprod(const GwyXYZ *a, const GwyXYZ *b)
{
    return a->x*b->x + a->y*b->y + a->z*b->z;
}

static inline gdouble
vecnorm2(const GwyXYZ *a)
{
    return a->x*a->x + a->y*a->y + a->z*a->z;
}

/* We are only interested in equations with two real solutions. */
static inline gboolean
solve_quadratic(gdouble a, gdouble b, gdouble c, gdouble *t1, gdouble *t2)
{
    gdouble D, bsD;

    D = b*b - 4.0*a*c;
    if (D <= 0.0)
        return FALSE;

    if (b >= 0.0)
        bsD = -0.5*(sqrt(D) + b);
    else
        bsD = 0.5*(sqrt(D) - b);

    *t1 = c/bsD;
    *t2 = bsD/a;
    return TRUE;
}

static void
render_base_object(PileupSynthObject *object,
                   PileupBaseFunc render_base,
                   gdouble width2, gdouble length2, gdouble angle)
{
    gdouble ca, sa, xc, yc, aspect;
    gint xres, yres, x, y, i, j;
    gdouble *zl, *zu;

    xres = object->xres;
    yres = object->yres;
    zl = object->lower;
    zu = object->upper;

    aspect = length2/width2;
    ca = cos(angle);
    sa = sin(angle);
    for (i = 0; i < yres; i++) {
        y = i - yres/2;
        for (j = 0; j < xres; j++, zl++, zu++) {
            x = j - xres/2;

            xc = (x*ca + y*sa)/length2;
            yc = (-x*sa + y*ca)/width2;
            *zl = *zu = render_base(xc, yc, aspect);
        }
    }
}

static void
render_general_shape(PileupSynthObject *object,
                     PileupIntersectFunc intersect,
                     gdouble width2, gdouble length2, gdouble angle,
                     gdouble bx, gdouble by)
{
    gdouble b, bh1, ca, sa, x, y, aspect;
    gint xres, yres, i, j;
    gdouble *zl, *zu;
    GwyXYZ ptl, ptu;

    xres = object->xres;
    yres = object->yres;
    zl = object->lower;
    zu = object->upper;

    aspect = length2/width2;
    ca = cos(angle);
    sa = sin(angle);
    b = sqrt(bx*bx + by*by);
    bh1 = sqrt(b*b + 1.0);

    for (i = 0; i < yres; i++) {
        y = i - yres/2;
        for (j = 0; j < xres; j++, zl++, zu++) {
            x = j - xres/2;

            /* Choose a vertical line passing through the pixel, given by
             * two points.  Transform coordinates to ones where the shape
             * is in the canonical position.  The line is no longer vertical
             * but it remains a straight line.  Find intersections with the
             * shape.  Transform back.  The larger one becomes upper, the
             * smaller one -lower. */
            ptl.x = x;
            ptl.y = y;
            ptl.z = -5.0;
            tilt_point(&ptl, bx, by, b, bh1);
            rotate_point(&ptl, ca, -sa);
            scale_point(&ptl, 1.0/length2, 1.0/width2, 1.0/width2);

            ptu.x = x;
            ptu.y = y;
            ptu.z = 5.0;
            tilt_point(&ptu, bx, by, b, bh1);
            rotate_point(&ptu, ca, -sa);
            scale_point(&ptu, 1.0/length2, 1.0/width2, 1.0/width2);

            if (!intersect(&ptl, &ptu, aspect)) {
                *zl = *zu = 0.0;
                continue;
            }

            scale_point(&ptl, length2, width2, width2);
            rotate_point(&ptl, ca, sa);
            tilt_point(&ptl, -bx, -by, b, bh1);

            scale_point(&ptu, length2, width2, width2);
            rotate_point(&ptu, ca, sa);
            tilt_point(&ptu, -bx, -by, b, bh1);

            if (ptl.z <= ptu.z) {
                *zl = -ptl.z;
                *zu = ptu.z;
            }
            else {
                *zl = -ptu.z;
                *zu = ptl.z;
            }
        }
    }
}

static void
sculpt_up(const PileupSynthObject *object,
          gdouble *surface, gint xres, gint yres,
          gint joff, gint ioff, gdouble m)
{
    const gdouble *zu, *zl;
    gdouble *srow, *end;
    gint kxres, kyres, i, j;

    kxres = object->xres;
    kyres = object->yres;
    zl = object->lower;
    zu = object->upper;

    for (i = 0; i < kyres; i++) {
        srow = surface + ((ioff + i) % yres)*xres;
        end = srow + xres-1;
        srow += joff;
        for (j = 0; j < kxres; j++, zl++, zu++, srow++) {
            if ((*zu || *zl) && *srow < *zu + m)
                *srow = *zu + m;
            if (srow == end)
                srow -= xres;
        }
    }
}

static void
pileup_one_object(PileupSynthObject *object, GwyDataField *surface,
                  gboolean *seen,
                  PileupBaseFunc render_base,
                  PileupIntersectFunc intersect,
                  gdouble width, gdouble length, gdouble angle,
                  gdouble stickout,
                  gint j, gint i)
{
    gint xres, yres, ioff, joff;
    gdouble *d;
    gdouble m, bx, by, width2, length2;

    xres = gwy_data_field_get_xres(surface);
    yres = gwy_data_field_get_yres(surface);
    d = gwy_data_field_get_data(surface);

    /* We prefer to work with half-axes, i.e. have the base bounding box
     * [-1,-1,-1] to [1,1,1]. */
    length2 = 0.5*length;
    width2 = 0.5*width;

    make_tilted_bounding_box(object, width2, length2, -angle, 0.0, 0.0);
    render_base_object(object, render_base, width2, length2, -angle);

    /* Recalculate centre to corner position. */
    joff = (j - object->xres/2 + 16384*xres) % xres;
    ioff = (i - object->yres/2 + 16384*yres) % yres;

    if (seen && !check_seen(seen, xres, yres, object, joff, ioff))
        return;

    find_weighted_mean_plane(object, d, xres, yres, joff, ioff, &bx, &by);
    make_tilted_bounding_box(object, width2, length2, -angle, bx, by);
    render_general_shape(object, intersect, width2, length2, -angle, bx, by);
    m = find_base_level(object, d, xres, yres, joff, ioff, stickout);

    /* Recalculate centre to corner position. */
    joff = (j - object->xres/2 + 16384*xres) % xres;
    ioff = (i - object->yres/2 + 16384*yres) % yres;

    sculpt_up(object, d, xres, yres, joff, ioff, m);
}

static glong
calculate_n_objects(const PileupSynthArgs *args,
                    guint xres, guint yres)
{
    /* The distribution of area differs from the distribution of width. */
    const PileupSynthFeature *feature = get_feature(args->type);
    gdouble noise_corr = exp(2.0*args->width_noise*args->width_noise);
    gdouble area_ratio = feature->get_coverage(args->aspect);
    gdouble base_area = args->width*args->width*args->aspect;
    gdouble mean_obj_area = base_area * area_ratio * noise_corr;
    gdouble must_cover = args->coverage*xres*yres;
    return (glong)ceil(must_cover/mean_obj_area);
}

static gboolean
intersect_ellipsoid(GwyXYZ *pt1, GwyXYZ *pt2, G_GNUC_UNUSED gdouble aspect)
{
    GwyXYZ c, v;
    gdouble t1, t2;

    middle_point(pt1, pt2, &c);
    vecdiff(pt2, pt1, &v);
    if (!solve_quadratic(vecnorm2(&v), 2.0*dotprod(&v, &c), vecnorm2(&c) - 1.0,
                         &t1, &t2))
        return FALSE;

    point_on_line(&c, &v, t1, pt1);
    point_on_line(&c, &v, t2, pt2);
    return TRUE;
}

static gboolean
intersect_bar(GwyXYZ *pt1, GwyXYZ *pt2, G_GNUC_UNUSED gdouble aspect)
{
    GwyXYZ c, v, r;
    gdouble t;
    gdouble t1 = G_MAXDOUBLE, t2 = -G_MAXDOUBLE;

    middle_point(pt1, pt2, &c);
    vecdiff(pt2, pt1, &v);

    if (fabs(v.z) > 1e-14) {
        /* z = 1 */
        t = (1.0 - c.z)/v.z;
        point_on_line(&c, &v, t, &r);
        if (fabs(r.x) <= 1.0 && fabs(r.y) <= 1.0) {
            if (t < t1)
                t1 = t;
            if (t > t2)
                t2 = t;
        }
        /* z = -1 */
        t = -(1.0 + c.z)/v.z;
        point_on_line(&c, &v, t, &r);
        if (fabs(r.x) <= 1.0 && fabs(r.y) <= 1.0) {
            if (t < t1)
                t1 = t;
            if (t > t2)
                t2 = t;
        }
    }

    if (fabs(v.y) > 1e-14) {
        /* y = 1 */
        t = (1.0 - c.y)/v.y;
        point_on_line(&c, &v, t, &r);
        if (fabs(r.x) <= 1.0 && fabs(r.z) <= 1.0) {
            if (t < t1)
                t1 = t;
            if (t > t2)
                t2 = t;
        }
        /* y = -1 */
        t = -(1.0 + c.y)/v.y;
        point_on_line(&c, &v, t, &r);
        if (fabs(r.x) <= 1.0 && fabs(r.z) <= 1.0) {
            if (t < t1)
                t1 = t;
            if (t > t2)
                t2 = t;
        }
    }

    if (fabs(v.x) > 1e-14) {
        /* x = 1 */
        t = (1.0 - c.x)/v.x;
        point_on_line(&c, &v, t, &r);
        if (fabs(r.z) <= 1.0 && fabs(r.y) <= 1.0) {
            if (t < t1)
                t1 = t;
            if (t > t2)
                t2 = t;
        }
        /* x = -1 */
        t = -(1.0 + c.x)/v.x;
        point_on_line(&c, &v, t, &r);
        if (fabs(r.z) <= 1.0 && fabs(r.y) <= 1.0) {
            if (t < t1)
                t1 = t;
            if (t > t2)
                t2 = t;
        }
    }

    if (t1 >= t2)
        return FALSE;

    point_on_line(&c, &v, t1, pt1);
    point_on_line(&c, &v, t2, pt2);
    return TRUE;
}

static gboolean
intersect_cylinder(GwyXYZ *pt1, GwyXYZ *pt2, G_GNUC_UNUSED gdouble aspect)
{
    GwyXYZ c, v;
    gdouble t1, t2;

    middle_point(pt1, pt2, &c);
    vecdiff(pt2, pt1, &v);
    /* First, we must hit the infinite cylinder at all. */
    if (!solve_quadratic(v.z*v.z + v.y*v.y,
                         2.0*(v.z*c.z + v.y*c.y),
                         c.z*c.z + c.y*c.y - 1.0,
                         &t1, &t2))
        return FALSE;

    point_on_line(&c, &v, t1, pt1);
    point_on_line(&c, &v, t2, pt2);
    if (pt1->x > pt2->x)
        GWY_SWAP(GwyXYZ, *pt1, *pt2);

    if (pt2->x < -1.0 || pt1->x > 1.0)
        return FALSE;

    if (pt1->x < -1.0) {
        t1 = -(1.0 + c.x)/v.x;
        point_on_line(&c, &v, t1, pt1);
    }
    if (pt2->x > 1.0) {
        t2 = (1.0 - c.x)/v.x;
        point_on_line(&c, &v, t2, pt2);
    }

    return TRUE;
}

static gboolean
intersect_nugget(GwyXYZ *pt1, GwyXYZ *pt2, gdouble aspect)
{
    GwyXYZ c, v, r1, r2;
    gdouble qa, qb, qc, t1, t2;

    middle_point(pt1, pt2, &c);
    vecdiff(pt2, pt1, &v);

    /* First try to hit the cylinder.   We know if we do not hit the infinitely
     * long version of it we cannot hit the object at all.*/
    if (!solve_quadratic(v.z*v.z + v.y*v.y,
                         2.0*(v.z*c.z + v.y*c.y),
                         c.z*c.z + c.y*c.y - 1.0,
                         &t1, &t2))
        return FALSE;

    point_on_line(&c, &v, t1, pt1);
    point_on_line(&c, &v, t2, pt2);
    if (pt1->x > pt2->x) {
        GWY_SWAP(GwyXYZ, *pt1, *pt2);
    }

    if (pt2->x < -1.0 || pt1->x > 1.0)
        return FALSE;

    /* If necessary, find intersections with the two terminating ellipsoids. */
    if (pt1->x < -1.0 + 1.0/aspect) {
        c.x *= aspect;
        v.x *= aspect;
        qa = vecnorm2(&v);
        qb = dotprod(&v, &c) + (aspect - 1.0)*v.x;
        qc = vecnorm2(&c) + aspect*(aspect - 2.0) + 2.0*(aspect - 1.0)*c.x;
        /* We may miss the rounded end completely. */
        if (!solve_quadratic(qa, 2.0*qb, qc, &t1, &t2))
            return FALSE;
        c.x /= aspect;
        v.x /= aspect;
        point_on_line(&c, &v, t1, &r1);
        point_on_line(&c, &v, t2, &r2);
        /* Either one or both intersections can be with the rounded part. */
        *pt1 = (r1.x <= r2.x) ? r1 : r2;
        if (pt2->x < -1.0 + 1.0/aspect)
            *pt2 = (r1.x <= r2.x) ? r2 : r1;
    }

    if (pt2->x > 1.0 - 1.0/aspect) {
        c.x *= aspect;
        v.x *= aspect;
        qa = vecnorm2(&v);
        qb = dotprod(&v, &c) - (aspect - 1.0)*v.x;
        qc = vecnorm2(&c) + aspect*(aspect - 2.0) - 2.0*(aspect - 1.0)*c.x;
        /* We may miss the rounded end completely. */
        if (!solve_quadratic(qa, 2.0*qb, qc, &t1, &t2))
            return FALSE;
        c.x /= aspect;
        v.x /= aspect;
        point_on_line(&c, &v, t1, &r1);
        point_on_line(&c, &v, t2, &r2);
        /* Either one or both intersections can be with the rounded part. */
        *pt2 = (r1.x >= r2.x) ? r1 : r2;
        if (pt1->x > 1.0 - 1.0/aspect)
            *pt1 = (r1.x >= r2.x) ? r2 : r1;
    }

    return TRUE;
}

static gboolean
intersect_hexagonal(GwyXYZ *pt1, GwyXYZ *pt2, G_GNUC_UNUSED gdouble aspect)
{
    GwyXYZ c, v, r;
    gdouble t1, t2, t, d;

    middle_point(pt1, pt2, &c);
    vecdiff(pt2, pt1, &v);

    /* First, we must hit the infinite rod at all. */
    t1 = G_MAXDOUBLE, t2 = -G_MAXDOUBLE;
    if (fabs(v.z) > 1e-14) {
        /* z = 1 */
        t = (1.0 - c.z)/v.z;
        point_on_line(&c, &v, t, &r);
        if (fabs(r.y) <= 0.5) {
            if (t < t1)
                t1 = t;
            if (t > t2)
                t2 = t;
        }
        /* z = -1 */
        t = -(1.0 + c.z)/v.z;
        point_on_line(&c, &v, t, &r);
        if (fabs(r.y) <= 0.5) {
            if (t < t1)
                t1 = t;
            if (t > t2)
                t2 = t;
        }
    }

    d = v.y + 0.5*v.z;
    if (fabs(d) > 1e-14) {
        /* z = 2(1-y) */
        t = (1.0 - c.y - 0.5*c.z)/d;
        point_on_line(&c, &v, t, &r);
        if (fabs(r.y - 0.75) <= 0.25) {
            if (t < t1)
                t1 = t;
            if (t > t2)
                t2 = t;
        }
        /* z = -2(1+y) */
        t = -(1.0 + c.y + 0.5*c.z)/d;
        point_on_line(&c, &v, t, &r);
        if (fabs(r.y + 0.75) <= 0.25) {
            if (t < t1)
                t1 = t;
            if (t > t2)
                t2 = t;
        }
    }

    d = v.y - 0.5*v.z;
    if (fabs(d) > 1e-14) {
        /* z = 2(y-1) */
        t = (1.0 - c.y + 0.5*c.z)/d;
        point_on_line(&c, &v, t, &r);
        if (fabs(r.y - 0.75) <= 0.25) {
            if (t < t1)
                t1 = t;
            if (t > t2)
                t2 = t;
        }
        /* z = 2(y+1) */
        t = -(1.0 + c.y - 0.5*c.z)/d;
        point_on_line(&c, &v, t, &r);
        if (fabs(r.y + 0.75) <= 0.25) {
            if (t < t1)
                t1 = t;
            if (t > t2)
                t2 = t;
        }
    }
    if (t1 >= t2)
        return FALSE;

    point_on_line(&c, &v, t1, pt1);
    point_on_line(&c, &v, t2, pt2);
    if (pt1->x > pt2->x)
        GWY_SWAP(GwyXYZ, *pt1, *pt2);

    if (pt2->x < -1.0 || pt1->x > 1.0)
        return FALSE;

    if (pt1->x < -1.0) {
        t1 = -(1.0 + c.x)/v.x;
        point_on_line(&c, &v, t1, pt1);
    }
    if (pt2->x > 1.0) {
        t2 = (1.0 - c.x)/v.x;
        point_on_line(&c, &v, t2, pt2);
    }

    return TRUE;
}

static gdouble
render_base_ellipsoid(gdouble x, gdouble y, G_GNUC_UNUSED gdouble aspect)
{
    gdouble r;

    r = 1.0 - x*x - y*y;
    return (r > 0.0) ? sqrt(r) : 0.0;
}

static gdouble
render_base_bar(gdouble x, gdouble y, G_GNUC_UNUSED gdouble aspect)
{
    return (fmax(fabs(x), fabs(y)) <= 1.0) ? 1.0 : 0.0;
}

static gdouble
render_base_cylinder(gdouble x, gdouble y, G_GNUC_UNUSED gdouble aspect)
{
    return (fmax(fabs(x), fabs(y)) <= 1.0) ? sqrt(1.0 - y*y) : 0.0;
}

static gdouble
render_base_nugget(gdouble x, gdouble y, gdouble aspect)
{
    gdouble h, r;

    h = 1.0 - 1.0/aspect;
    r = 1.0 - y*y;
    if (r <= 0.0)
        return 0.0;

    x = fabs(x);
    if (x <= h)
        return sqrt(r);

    x = aspect*(x - h);
    r -= x*x;
    return (r > 0.0) ? sqrt(r) : 0.0;
}

static gdouble
render_base_hexagonal(gdouble x, gdouble y, G_GNUC_UNUSED gdouble aspect)
{
    y = fabs(y);
    if (fmax(fabs(x), y) >= 1.0)
       return 0.0;
    return (y <= 0.5) ? 1.0 : 2.0*(1.0 - y);
}

static gdouble
getcov_ellipsoid(G_GNUC_UNUSED gdouble aspect)
{
    return G_PI/4.0;
}

static gdouble
getcov_bar(G_GNUC_UNUSED gdouble aspect)
{
    return 1.0;
}

static gdouble
getcov_cylinder(G_GNUC_UNUSED gdouble aspect)
{
    return 1.0;
}

static gdouble
getcov_nugget(gdouble aspect)
{
    return 1.0 - (1.0 - G_PI/4.0)/aspect;
}

static gdouble
getcov_hexagonal(G_GNUC_UNUSED gdouble aspect)
{
    return 1.0;
}

static const gchar active_page_key[]    = "/module/pileup_synth/active_page";
static const gchar angle_key[]          = "/module/pileup_synth/angle";
static const gchar angle_noise_key[]    = "/module/pileup_synth/angle_noise";
static const gchar aspect_key[]         = "/module/pileup_synth/aspect";
static const gchar aspect_noise_key[]   = "/module/pileup_synth/aspect_noise";
static const gchar avoid_stacking_key[] = "/module/pileup_synth/avoid_stacking";
static const gchar coverage_key[]       = "/module/pileup_synth/coverage";
static const gchar prefix[]             = "/module/pileup_synth";
static const gchar randomize_key[]      = "/module/pileup_synth/randomize";
static const gchar seed_key[]           = "/module/pileup_synth/seed";
static const gchar stickout_key[]       = "/module/pileup_synth/stickout";
static const gchar type_key[]           = "/module/pileup_synth/type";
static const gchar update_key[]         = "/module/pileup_synth/update";
static const gchar width_key[]          = "/module/pileup_synth/width";
static const gchar width_noise_key[]    = "/module/pileup_synth/width_noise";

static void
pileup_synth_sanitize_args(PileupSynthArgs *args)
{
    args->active_page = CLAMP(args->active_page,
                              PAGE_DIMENSIONS, PAGE_NPAGES-1);
    args->update = !!args->update;
    args->seed = MAX(0, args->seed);
    args->randomize = !!args->randomize;
    args->type = MIN(args->type, PILEUP_SYNTH_NTYPES-1);
    args->width = CLAMP(args->width, 1.0, 1000.0);
    args->width_noise = CLAMP(args->width_noise, 0.0, 1.0);
    args->aspect = CLAMP(args->aspect, 1.0, 8.0);
    args->aspect_noise = CLAMP(args->aspect_noise, 0.0, 1.0);
    args->avoid_stacking = !!args->avoid_stacking;
    args->angle = CLAMP(args->angle, -G_PI, G_PI);
    args->angle_noise = CLAMP(args->angle_noise, 0.0, 1.0);
    args->coverage = CLAMP(args->coverage, 0.001, 30.0);
    args->stickout = CLAMP(args->stickout, -1.0, 1.0);
}

static void
pileup_synth_load_args(GwyContainer *container,
                       PileupSynthArgs *args,
                       GwyDimensionArgs *dimsargs)
{
    *args = pileup_synth_defaults;

    gwy_container_gis_int32_by_name(container, active_page_key,
                                    &args->active_page);
    gwy_container_gis_boolean_by_name(container, update_key, &args->update);
    gwy_container_gis_int32_by_name(container, seed_key, &args->seed);
    gwy_container_gis_boolean_by_name(container, randomize_key,
                                      &args->randomize);
    gwy_container_gis_enum_by_name(container, type_key, &args->type);
    gwy_container_gis_double_by_name(container, width_key, &args->width);
    gwy_container_gis_double_by_name(container, width_noise_key,
                                     &args->width_noise);
    gwy_container_gis_double_by_name(container, aspect_key, &args->aspect);
    gwy_container_gis_double_by_name(container, aspect_noise_key,
                                     &args->aspect_noise);
    gwy_container_gis_boolean_by_name(container, avoid_stacking_key,
                                      &args->avoid_stacking);
    gwy_container_gis_double_by_name(container, angle_key, &args->angle);
    gwy_container_gis_double_by_name(container, angle_noise_key,
                                     &args->angle_noise);
    gwy_container_gis_double_by_name(container, coverage_key, &args->coverage);
    gwy_container_gis_double_by_name(container, stickout_key, &args->stickout);
    pileup_synth_sanitize_args(args);

    gwy_clear(dimsargs, 1);
    gwy_dimensions_copy_args(&dims_defaults, dimsargs);
    gwy_dimensions_load_args(dimsargs, container, prefix);
}

static void
pileup_synth_save_args(GwyContainer *container,
                       const PileupSynthArgs *args,
                       const GwyDimensionArgs *dimsargs)
{
    gwy_container_set_int32_by_name(container, active_page_key,
                                    args->active_page);
    gwy_container_set_boolean_by_name(container, update_key, args->update);
    gwy_container_set_int32_by_name(container, seed_key, args->seed);
    gwy_container_set_boolean_by_name(container, randomize_key,
                                      args->randomize);
    gwy_container_set_enum_by_name(container, type_key, args->type);
    gwy_container_set_double_by_name(container, width_key, args->width);
    gwy_container_set_double_by_name(container, width_noise_key,
                                     args->width_noise);
    gwy_container_set_double_by_name(container, aspect_key, args->aspect);
    gwy_container_set_double_by_name(container, aspect_noise_key,
                                     args->aspect_noise);
    gwy_container_set_boolean_by_name(container, avoid_stacking_key,
                                      args->avoid_stacking);
    gwy_container_set_double_by_name(container, angle_key, args->angle);
    gwy_container_set_double_by_name(container, angle_noise_key,
                                     args->angle_noise);
    gwy_container_set_double_by_name(container, coverage_key, args->coverage);
    gwy_container_set_double_by_name(container, stickout_key, args->stickout);

    gwy_dimensions_save_args(dimsargs, container, prefix);
}

/* vim: set cin et ts=4 sw=4 cino=>1s,e0,n0,f0,{0,}0,^0,\:1s,=0,g1s,h0,t0,+1s,c3,(0,u0 : */
