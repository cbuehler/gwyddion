/*
 *  $Id$
 *  Copyright (C) 2004-2017 David Necas (Yeti), Petr Klapetek.
 *  E-mail: yeti@gwyddion.net, klapetek@gwyddion.net.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA 02110-1301, USA.
 */

#include "config.h"
#include <gtk/gtk.h>
#include <libgwyddion/gwymacros.h>
#include <libprocess/arithmetic.h>
#include <libprocess/stats.h>
#include <libprocess/correlation.h>
#include <libprocess/filters.h>
#include <libgwydgets/gwydgetutils.h>
#include <libgwydgets/gwycombobox.h>
#include <libgwydgets/gwystock.h>
#include <libgwymodule/gwymodule-process.h>
#include <app/gwymoduleutils.h>
#include <app/gwyapp.h>

#define MASKCOR_RUN_MODES GWY_RUN_INTERACTIVE

typedef enum {
    GWY_MASKCOR_OBJECTS,
    GWY_MASKCOR_MAXIMA,
    GWY_MASKCOR_SCORE,
    GWY_MASKCOR_LAST
} MaskcorResult;

typedef struct {
    MaskcorResult result;
    gdouble threshold;
    gdouble regcoeff;
    GwyCorrSearchType method;
    gboolean use_mask;
    GwyAppDataId data;
    GwyAppDataId kernel;
} MaskcorArgs;

typedef struct {
    MaskcorArgs *args;
    GtkWidget *dialogue;
    GtkObject *threshold;
    GtkObject *regcoeff;
    GtkWidget *use_mask;
} MaskcorControls;

static gboolean module_register      (void);
static void     maskcor              (GwyContainer *data,
                                      GwyRunType run);
static gboolean maskcor_dialog       (MaskcorArgs *args);
static void     operation_changed    (GtkComboBox *combo,
                                      MaskcorControls *controls);
static void     method_changed       (GtkComboBox *combo,
                                      MaskcorControls *controls);
static void     threshold_changed    (GtkAdjustment *adj,
                                      gdouble *value);
static void     regcoeff_changed     (GtkAdjustment *adj,
                                      gdouble *value);
static void     use_mask_changed     (GtkToggleButton *toggle,
                                      gboolean *value);
static void     kernel_changed       (GwyDataChooser *chooser,
                                      MaskcorControls *controls);
static gboolean maskcor_kernel_filter(GwyContainer *data,
                                      gint id,
                                      gpointer user_data);
static void     maskcor_do           (MaskcorArgs *args);
static void     maskcor_load_args    (GwyContainer *settings,
                                      MaskcorArgs *args);
static void     maskcor_save_args    (GwyContainer *settings,
                                      MaskcorArgs *args);
static void     maskcor_sanitize_args(MaskcorArgs *args);

static const MaskcorArgs maskcor_defaults = {
    GWY_MASKCOR_OBJECTS, 0.95, 0.001,
    GWY_CORR_SEARCH_COVARIANCE_SCORE, TRUE,
    GWY_APP_DATA_ID_NONE, GWY_APP_DATA_ID_NONE,
};

static GwyAppDataId kernel_id = GWY_APP_DATA_ID_NONE;

static GwyModuleInfo module_info = {
    GWY_MODULE_ABI_VERSION,
    &module_register,
    N_("Searches for a detail in another image using correlation."),
    "Petr Klapetek <klapetek@gwyddion.net>, Yeti <yeti@gwyddion.net>",
    "2.1",
    "David Nečas (Yeti) & Petr Klapetek",
    "2004",
};

GWY_MODULE_QUERY2(module_info, maskcor)

static gboolean
module_register(void)
{
    gwy_process_func_register("maskcor",
                              (GwyProcessFunc)&maskcor,
                              N_("/M_ultidata/Correlation _Search..."),
                              GWY_STOCK_CORRELATION_MASK,
                              MASKCOR_RUN_MODES,
                              GWY_MENU_FLAG_DATA,
                              N_("Search for a detail using correlation"));

    return TRUE;
}

static void
maskcor(G_GNUC_UNUSED GwyContainer *data, GwyRunType run)
{
    MaskcorArgs args;
    GwyContainer *settings;

    g_return_if_fail(run & MASKCOR_RUN_MODES);
    settings = gwy_app_settings_get();
    maskcor_load_args(settings, &args);

    gwy_app_data_browser_get_current(GWY_APP_DATA_FIELD_ID, &args.data.id,
                                     GWY_APP_CONTAINER_ID, &args.data.datano,
                                     0);

    if (maskcor_dialog(&args))
        maskcor_do(&args);

    maskcor_save_args(gwy_app_settings_get(), &args);
}

static gboolean
maskcor_dialog(MaskcorArgs *args)
{
    static const GwyEnum methods[] = {
        { N_("Correlation, raw"),           GWY_CORR_SEARCH_COVARIANCE_RAW },
        { N_("Correlation, leveled"),       GWY_CORR_SEARCH_COVARIANCE },
        { N_("Correlation score"),          GWY_CORR_SEARCH_COVARIANCE_SCORE },
        { N_("Height difference, raw"),     GWY_CORR_SEARCH_HEIGHT_DIFF_RAW },
        { N_("Height difference, leveled"), GWY_CORR_SEARCH_HEIGHT_DIFF },
        { N_("Height difference score"),    GWY_CORR_SEARCH_HEIGHT_DIFF_SCORE },
    };

    MaskcorControls controls;
    GtkWidget *dialog, *table, *chooser, *spin, *combo, *method;
    GtkObject *adj;
    gint row, response;
    gboolean ok, is_score;

    controls.args = args;
    is_score = (args->method == GWY_CORR_SEARCH_COVARIANCE_SCORE
                || args->method == GWY_CORR_SEARCH_HEIGHT_DIFF_SCORE);

    dialog = gtk_dialog_new_with_buttons(_("Correlation Search"), NULL, 0,
                                         GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
                                         GTK_STOCK_OK, GTK_RESPONSE_OK,
                                         NULL);
    gtk_dialog_set_default_response(GTK_DIALOG(dialog), GTK_RESPONSE_OK);
    gwy_help_add_to_proc_dialog(GTK_DIALOG(dialog), GWY_HELP_DEFAULT);
    controls.dialogue = dialog;

    table = gtk_table_new(7, 3, FALSE);
    gtk_table_set_row_spacings(GTK_TABLE(table), 2);
    gtk_table_set_col_spacings(GTK_TABLE(table), 6);
    gtk_container_set_border_width(GTK_CONTAINER(table), 4);
    gtk_box_pack_start(GTK_BOX(GTK_DIALOG(dialog)->vbox), table, TRUE, TRUE, 4);
    row = 0;

    /* Kernel */
    chooser = gwy_data_chooser_new_channels();
    g_object_set_data(G_OBJECT(chooser), "dialog", dialog);
    gwy_data_chooser_set_active(GWY_DATA_CHOOSER(chooser), NULL, -1);
    gwy_data_chooser_set_filter(GWY_DATA_CHOOSER(chooser),
                                maskcor_kernel_filter, &args->data, NULL);
    gwy_data_chooser_set_active_id(GWY_DATA_CHOOSER(chooser), &args->kernel);
    gwy_data_chooser_get_active_id(GWY_DATA_CHOOSER(chooser), &args->kernel);
    g_signal_connect(chooser, "changed",
                     G_CALLBACK(kernel_changed), &controls);
    gwy_table_attach_adjbar(table, row, _("_Detail to search:"), NULL,
                            GTK_OBJECT(chooser), GWY_HSCALE_WIDGET_NO_EXPAND);
    row++;

    controls.use_mask = gtk_check_button_new_with_mnemonic(_("Use _mask"));
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(controls.use_mask),
                                 args->use_mask);
    g_signal_connect(controls.use_mask, "toggled",
                     G_CALLBACK(use_mask_changed), &args->use_mask);
    gtk_table_attach(GTK_TABLE(table), controls.use_mask,
                     0, 2, row, row+1, GTK_FILL, 0, 0, 0);
    row++;

    /* Result */
    combo = gwy_enum_combo_box_newl(G_CALLBACK(operation_changed),
                                    &controls, args->result,
                                    _("Objects marked"), GWY_MASKCOR_OBJECTS,
                                    _("Correlation maxima"), GWY_MASKCOR_MAXIMA,
                                    _("Correlation score"), GWY_MASKCOR_SCORE,
                                    NULL);
    gwy_table_attach_adjbar(table, row, _("Output _type:"), NULL,
                            GTK_OBJECT(combo), GWY_HSCALE_WIDGET_NO_EXPAND);
    row++;

    /* Parameters */
    method = gwy_enum_combo_box_new(methods, G_N_ELEMENTS(methods),
                                    G_CALLBACK(method_changed),
                                    &controls, args->method, TRUE);
    gwy_table_attach_adjbar(table, row, _("Correlation _method:"), NULL,
                            GTK_OBJECT(method), GWY_HSCALE_WIDGET_NO_EXPAND);
    row++;

    adj = gtk_adjustment_new(args->threshold, 0.0, 1.0, 0.01, 0.1, 0);
    controls.threshold = adj;
    spin = gwy_table_attach_adjbar(table, row, _("T_hreshold:"), NULL,
                                   adj, GWY_HSCALE_LINEAR);
    gtk_spin_button_set_digits(GTK_SPIN_BUTTON(spin), 3);
    gwy_table_hscale_set_sensitive(adj, args->result != GWY_MASKCOR_SCORE);
    g_signal_connect(adj, "value-changed",
                     G_CALLBACK(threshold_changed), &args->threshold);
    row++;

    adj = gtk_adjustment_new(args->regcoeff, 0.0, 1.0, 0.001, 0.1, 0);
    controls.regcoeff = adj;
    spin = gwy_table_attach_adjbar(table, row, _("Regularization:"), NULL,
                                   adj, GWY_HSCALE_SQRT);
    gwy_table_hscale_set_sensitive(adj, is_score);
    g_signal_connect(adj, "value-changed",
                     G_CALLBACK(regcoeff_changed), &args->regcoeff);
    row++;

    gtk_widget_show_all(dialog);
    kernel_changed(GWY_DATA_CHOOSER(chooser), &controls);

    do {
        response = gtk_dialog_run(GTK_DIALOG(dialog));
        switch (response) {
            case GTK_RESPONSE_CANCEL:
            case GTK_RESPONSE_DELETE_EVENT:
            gtk_widget_destroy(dialog);
            case GTK_RESPONSE_NONE:
            return FALSE;
            break;

            case GTK_RESPONSE_OK:
            gtk_widget_destroy(dialog);
            ok = TRUE;
            break;

            default:
            g_assert_not_reached();
            break;
        }
    } while (!ok);

    return ok;
}

static void
operation_changed(GtkComboBox *combo, MaskcorControls *controls)
{
    MaskcorArgs *args = controls->args;
    args->result = gwy_enum_combo_box_get_active(combo);
    gwy_table_hscale_set_sensitive(controls->threshold,
                                   args->result != GWY_MASKCOR_SCORE);
}

static void
method_changed(GtkComboBox *combo, MaskcorControls *controls)
{
    MaskcorArgs *args = controls->args;
    gboolean is_score;
    args->method = gwy_enum_combo_box_get_active(combo);
    is_score = (args->method == GWY_CORR_SEARCH_COVARIANCE_SCORE
                || args->method == GWY_CORR_SEARCH_HEIGHT_DIFF_SCORE);
    gwy_table_hscale_set_sensitive(controls->regcoeff, is_score);
}

static void
threshold_changed(GtkAdjustment *adj, gdouble *value)
{
    *value = gtk_adjustment_get_value(adj);
}

static void
regcoeff_changed(GtkAdjustment *adj, gdouble *value)
{
    *value = gtk_adjustment_get_value(adj);
}

static void
use_mask_changed(GtkToggleButton *toggle, gboolean *value)
{
    *value = gtk_toggle_button_get_active(toggle);
}

static void
kernel_changed(GwyDataChooser *chooser, MaskcorControls *controls)
{
    GwyAppDataId *object = &controls->args->kernel;
    GwyContainer *container;
    GtkWidget *dialog;
    GwyDataField *mask;
    GQuark quark;

    gwy_data_chooser_get_active_id(chooser, object);
    gwy_debug("kernel: %d %d", object->datano, object->id);

    dialog = g_object_get_data(G_OBJECT(chooser), "dialog");
    g_assert(GTK_IS_DIALOG(dialog));
    gtk_dialog_set_response_sensitive(GTK_DIALOG(dialog), GTK_RESPONSE_OK,
                                      object->datano);

    gtk_widget_set_sensitive(controls->use_mask, FALSE);
    if (object->datano <= 0 || object->id < 0)
        return;

    container = gwy_app_data_browser_get(object->datano);
    quark = gwy_app_get_mask_key_for_id(object->id);
    if (gwy_container_gis_object(container, quark, &mask)
        && GWY_IS_DATA_FIELD(mask))
        gtk_widget_set_sensitive(controls->use_mask, TRUE);
}

static gboolean
maskcor_kernel_filter(GwyContainer *data, gint id, gpointer user_data)
{
    GwyAppDataId *object = (GwyAppDataId*)user_data;
    GwyDataField *kernel, *dfield;
    GQuark quark;

    quark = gwy_app_get_data_key_for_id(id);
    kernel = GWY_DATA_FIELD(gwy_container_get_object(data, quark));

    data = gwy_app_data_browser_get(object->datano);
    quark = gwy_app_get_data_key_for_id(object->id);
    dfield = GWY_DATA_FIELD(gwy_container_get_object(data, quark));

    if (gwy_data_field_get_xreal(kernel) <= gwy_data_field_get_xreal(dfield)/4
        && gwy_data_field_get_yreal(kernel) <= gwy_data_field_get_yreal(dfield)/4
        && !gwy_data_field_check_compatibility(kernel, dfield,
                                               GWY_DATA_COMPATIBILITY_LATERAL
                                               | GWY_DATA_COMPATIBILITY_MEASURE))
        return TRUE;

    return FALSE;
}

static void
plot_correlated(GwyDataField *retfield, gint xsize, gint ysize,
                gdouble threshold)
{
    GwyDataField *tmp;
    gint xres, yres, i, j, col, row, w, h;
    const gdouble *data;

    tmp = gwy_data_field_duplicate(retfield);
    gwy_data_field_clear(retfield);

    xres = gwy_data_field_get_xres(retfield);
    yres = gwy_data_field_get_yres(retfield);
    data = gwy_data_field_get_data_const(tmp);

    /* FIXME: this is very inefficient */
    for (i = 0; i < yres; i++) {
        row = MAX(i - ysize/2, 0);
        h = MIN(i + ysize - ysize/2, yres) - row;
        for (j = 0; j < xres; j++) {
            if (data[i*xres + j] > threshold) {
                col = MAX(j - xsize/2, 0);
                w = MIN(j + xsize - xsize/2, xres) - col;
                gwy_data_field_area_fill(retfield, col, row, w, h, 1.0);
            }
        }
    }

    g_object_unref(tmp);
}

static void
maskcor_do(MaskcorArgs *args)
{
    GwyDataField *dfield, *kernel, *kmask, *retfield;
    GwyContainer *data, *kerneldata;
    gdouble min, max, threshold;
    GQuark quark;
    gint newid;

    kerneldata = gwy_app_data_browser_get(args->kernel.datano);
    quark = gwy_app_get_data_key_for_id(args->kernel.id);
    kernel = GWY_DATA_FIELD(gwy_container_get_object(kerneldata, quark));

    kmask = NULL;
    if (args->use_mask) {
        quark = gwy_app_get_mask_key_for_id(args->kernel.id);
        gwy_container_gis_object(kerneldata, quark, (GObject**)&kmask);
    }

    data = gwy_app_data_browser_get(args->data.datano);
    quark = gwy_app_get_data_key_for_id(args->data.id);
    dfield = GWY_DATA_FIELD(gwy_container_get_object(data, quark));

    retfield = gwy_data_field_new_alike(dfield, FALSE);
    gwy_data_field_correlation_search(dfield, kernel, kmask, retfield,
                                      args->method, args->regcoeff,
                                      GWY_EXTERIOR_BORDER_EXTEND, 0.0);

    threshold = args->threshold;
    if (args->method == GWY_CORR_SEARCH_COVARIANCE_SCORE) {
        /* pass */
    }
    else if (args->method == GWY_CORR_SEARCH_HEIGHT_DIFF_SCORE) {
        threshold = 2.0*(threshold - 1.0);
    }
    else {
        gwy_data_field_get_min_max(retfield, &min, &max);
        threshold = min*threshold + max*(1.0 - threshold);
    }

    /* score - do new data with score */
    if (args->result == GWY_MASKCOR_SCORE) {
        newid = gwy_app_data_browser_add_data_field(retfield, data, TRUE);
        gwy_app_sync_data_items(data, data,
                                args->data.id, newid, FALSE,
                                GWY_DATA_ITEM_GRADIENT, 0);
        gwy_app_set_data_field_title(data, newid,
                                     _("Correlation score"));
        gwy_app_channel_log_add_proc(data, args->data.id, newid);
    }
    else {
        /* add mask */
        quark = gwy_app_get_mask_key_for_id(args->data.id);
        gwy_app_undo_qcheckpointv(data, 1, &quark);
        if (args->result == GWY_MASKCOR_OBJECTS) {
            plot_correlated(retfield,
                            gwy_data_field_get_xres(kernel),
                            gwy_data_field_get_yres(kernel),
                            threshold);
        }
        else if (args->result == GWY_MASKCOR_MAXIMA)
            gwy_data_field_threshold(retfield, threshold, 0.0, 1.0);

        gwy_container_set_object(data, quark, retfield);
        gwy_app_channel_log_add_proc(data, args->data.id, args->data.id);
    }
    g_object_unref(retfield);
}

static const gchar method_key[]    = "/module/maskcor/method";
static const gchar regcoeff_key[]  = "/module/maskcor/regcoeff";
static const gchar result_key[]    = "/module/maskcor/result";
static const gchar threshold_key[] = "/module/maskcor/threshold";
static const gchar use_mask_key[]  = "/module/maskcor/use_mask";

static void
maskcor_sanitize_args(MaskcorArgs *args)
{
    args->result = MIN(args->result, GWY_MASKCOR_LAST-1);
    args->method = MIN(args->method, GWY_CORR_SEARCH_HEIGHT_DIFF_SCORE);
    args->threshold = CLAMP(args->threshold, 0.0, 1.0);
    args->regcoeff = CLAMP(args->regcoeff, 0.0, 1.0);
    args->use_mask = !!args->use_mask;
    gwy_app_data_id_verify_channel(&args->kernel);
}

static void
maskcor_load_args(GwyContainer *settings,
                  MaskcorArgs *args)
{
    *args = maskcor_defaults;
    gwy_container_gis_enum_by_name(settings, result_key, &args->result);
    gwy_container_gis_enum_by_name(settings, method_key, &args->method);
    gwy_container_gis_double_by_name(settings, threshold_key, &args->threshold);
    gwy_container_gis_double_by_name(settings, regcoeff_key, &args->regcoeff);
    gwy_container_gis_boolean_by_name(settings, use_mask_key, &args->use_mask);
    args->kernel = kernel_id;
    maskcor_sanitize_args(args);
}

static void
maskcor_save_args(GwyContainer *settings,
                  MaskcorArgs *args)
{
    kernel_id = args->kernel;
    gwy_container_set_enum_by_name(settings, result_key, args->result);
    gwy_container_set_enum_by_name(settings, method_key, args->method);
    gwy_container_set_double_by_name(settings, threshold_key, args->threshold);
    gwy_container_set_double_by_name(settings, regcoeff_key, args->regcoeff);
    gwy_container_set_boolean_by_name(settings, use_mask_key, args->use_mask);
}

/* vim: set cin et ts=4 sw=4 cino=>1s,e0,n0,f0,{0,}0,^0,\:1s,=0,g1s,h0,t0,+1s,c3,(0,u0 : */
