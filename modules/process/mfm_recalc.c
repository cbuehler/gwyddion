/*
 *  $Id$
 *  Copyright (C) 2003 David Necas (Yeti), Petr Klapetek.
 *  E-mail: yeti@gwyddion.net, klapetek@gwyddion.net.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA 02110-1301, USA.
 */

#include "config.h"
#include <gtk/gtk.h>
#include <libgwyddion/gwymacros.h>
#include <libgwyddion/gwymath.h>
#include <libgwyddion/gwyenum.h>
#include <libprocess/stats.h>
#include <libprocess/inttrans.h>
#include <libprocess/gwyprocesstypes.h>
#include <libgwydgets/gwydgetutils.h>
#include <libgwydgets/gwycombobox.h>
#include <libgwydgets/gwystock.h>
#include <libgwymodule/gwymodule-process.h>
#include <app/gwyapp.h>
#include "preview.h"
#include "mfmops.h"

#define MFM_RECALC_RUN_MODES (GWY_RUN_IMMEDIATE | GWY_RUN_INTERACTIVE)

typedef enum  {
    SIGNAL_PHASE_DEG = 0,
    SIGNAL_PHASE_RAD = 1,
    SIGNAL_FREQUENCY = 2,
    SIGNAL_AMPLITUDE_V = 3,
    SIGNAL_AMPLITUDE_NM = 4
} MfmRecalcSignal;


typedef struct {
    MfmRecalcSignal signal;
    gdouble spring_constant;
    gdouble quality;
    gdouble base_frequency;
    gdouble base_amplitude;
} MfmRecalcArgs;

typedef struct {
    MfmRecalcArgs *args;
    GtkWidget *signal;
    GtkObject *spring_constant;
    GtkObject *quality;
    GtkWidget *quality_widget;
    GtkObject *base_frequency;
    GtkWidget *base_frequency_widget;
    GtkObject *base_amplitude;
    GtkWidget *base_amplitude_widget;
} MfmRecalcControls;

static gboolean module_register           (void);
static void     mfm_recalc                (GwyContainer *data,
                                           GwyRunType run);
static gboolean mfm_recalc_dialog         (MfmRecalcArgs *args,
                                           MfmRecalcSignal guess);
static void     mfm_recalc_dialog_update  (MfmRecalcControls *controls,
                                           MfmRecalcArgs *args);
static void     mfm_recalc_load_args      (GwyContainer *container,
                                           MfmRecalcArgs *args);
static void     mfm_recalc_save_args      (GwyContainer *container,
                                           MfmRecalcArgs *args);
static void     mfm_recalc_sanitize_args  (MfmRecalcArgs *args);
static void     update_sensitivity        (MfmRecalcControls *controls,
                                           MfmRecalcArgs *args);
static void     signal_selected           (GtkComboBox *combo, 
                                           MfmRecalcControls *controls);


static const MfmRecalcArgs mfm_recalc_defaults = {
    SIGNAL_PHASE_DEG, 40, 1000, 150, 0.2,
};

static GwyModuleInfo module_info = {
    GWY_MODULE_ABI_VERSION,
    &module_register,
    N_("Converts the MFM data to force gradient."),
    "Petr Klapetek <klapetek@gwyddion.net>, Robb Puttock <robb.puttock@npl.co.uk>",
    "1.0",
    "David Nečas (Yeti) & Petr Klapetek",
    "2018",
};

GWY_MODULE_QUERY2(module_info, mfm_recalc)

static gboolean
module_register(void)
{
    gwy_process_func_register("mfm_recalc",
                              (GwyProcessFunc)&mfm_recalc,
                              N_("/SPM M_odes/_Magnetic/_Recalculate to force gradient..."),
                              GWY_STOCK_MFM_CURRENT_LINE,
                              MFM_RECALC_RUN_MODES,
                              GWY_MENU_FLAG_DATA,
                              N_("Recalculate to force gradient"));

    return TRUE;
}

static void
issue_warning(GtkWindow *window, gchar* properunit)
{
    GtkWidget *dialog;
    gchar message[100];

    snprintf(message, sizeof(message), "Data value units must be %s for present settings to work", properunit);

    dialog = gtk_message_dialog_new
              (window,
               GTK_DIALOG_DESTROY_WITH_PARENT,
               GTK_MESSAGE_ERROR,
               GTK_BUTTONS_OK,
               message);
          
    gtk_dialog_run(GTK_DIALOG(dialog));
    gtk_widget_destroy(dialog);
    
}

static void
mfm_recalc(GwyContainer *data, GwyRunType run)
{
    GwyDataField *dfield, *out;
    MfmRecalcArgs args;
    gboolean ok;
    gint newid, oldid;
    GwySIUnit *zunit;
    MfmRecalcSignal guess;

    g_return_if_fail(run & MFM_RECALC_RUN_MODES);
    gwy_app_data_browser_get_current(GWY_APP_DATA_FIELD, &dfield,
                                     GWY_APP_DATA_FIELD_ID, &oldid,
                                     0);
    g_return_if_fail(dfield);

    //guess the units
    zunit = gwy_data_field_get_si_unit_z(dfield);
    if (gwy_si_unit_equal_string(zunit, "deg") || gwy_si_unit_equal_string(zunit, "°")) 
        guess = SIGNAL_PHASE_DEG;
    else if (gwy_si_unit_equal_string(zunit, "rad"))
        guess = SIGNAL_PHASE_RAD;
    else if (gwy_si_unit_equal_string(zunit, "Hz"))
        guess = SIGNAL_FREQUENCY;
    else if(gwy_si_unit_equal_string(zunit, "V"))
        guess = SIGNAL_AMPLITUDE_V;
    else guess = SIGNAL_AMPLITUDE_NM;

    args.signal = guess;

    mfm_recalc_load_args(gwy_app_settings_get(), &args);
    if (run == GWY_RUN_INTERACTIVE) {
        ok = mfm_recalc_dialog(&args, guess);
        mfm_recalc_save_args(gwy_app_settings_get(), &args);
        if (!ok)
            return;
    }
 
    //check the units
    if (args.signal == SIGNAL_PHASE_DEG) {
       if (!(gwy_si_unit_equal_string(zunit, "deg") || gwy_si_unit_equal_string(zunit, "°")))
       {
          issue_warning(gwy_app_find_window_for_channel(data, oldid), "degrees ('deg' or '°')");
          return;
       }
    }
    else if (args.signal == SIGNAL_PHASE_RAD) {
       if (!gwy_si_unit_equal_string(zunit, "rad"))
       {
          issue_warning(gwy_app_find_window_for_channel(data, oldid), "radians");
          return;
       }
    }
    else if (args.signal == SIGNAL_FREQUENCY) {
       if (!gwy_si_unit_equal_string(zunit, "Hz"))
       {
          issue_warning(gwy_app_find_window_for_channel(data, oldid), "Hertz (Hz)");
          return;
       }
    }

    else if (args.signal == SIGNAL_AMPLITUDE_V) {
       if (!gwy_si_unit_equal_string(zunit, "rad"))
       {
          issue_warning(gwy_app_find_window_for_channel(data, oldid), "Volts");
          return;
       }
    }

    else if (args.signal == SIGNAL_AMPLITUDE_NM) {
       if (!gwy_si_unit_equal_string(zunit, "m"))
       {
          issue_warning(gwy_app_find_window_for_channel(data, oldid), "meters");
          return;
       }
    }

    out = gwy_data_field_new_alike(dfield, TRUE);
    if (args.signal == SIGNAL_AMPLITUDE_NM) args.base_amplitude *= 1e-9;

    if (args.signal == SIGNAL_PHASE_DEG) {
        get_force_gradient_from_phase(dfield, out, args.spring_constant, args.quality);
        gwy_data_field_multiply(out, M_PI/180);
    } 
    else if (args.signal == SIGNAL_PHASE_RAD) {
        get_force_gradient_from_phase(dfield, out, args.spring_constant, args.quality);
    } 
    else if (args.signal == SIGNAL_FREQUENCY) {
        get_force_gradient_from_frequency_shift(dfield, out, args.spring_constant, args.base_frequency);
    } 
    else get_force_gradient_from_amplitude_shift(dfield, out, args.spring_constant, args.quality, args.base_amplitude);
   


    newid = gwy_app_data_browser_add_data_field(out, data, TRUE);
    g_object_unref(out);
    gwy_app_set_data_field_title(data, newid, _("Recalculated MFM data"));
    gwy_app_sync_data_items(data, data, oldid, newid, FALSE,
                            GWY_DATA_ITEM_PALETTE, 0);
    gwy_app_channel_log_add_proc(data, oldid, newid);

}


static gboolean
mfm_recalc_dialog(MfmRecalcArgs *args, MfmRecalcSignal guess)
{
    GtkWidget *dialog, *table;
    MfmRecalcControls controls;
    gint row, response;

    static const GwyEnum signals[] = {
    { N_("Phase (radians)"),   SIGNAL_PHASE_RAD, },
    { N_("Phase (degrees)"),   SIGNAL_PHASE_DEG, },
    { N_("Frequency shift"),   SIGNAL_FREQUENCY, },
    { N_("Amplitude (V)"),     SIGNAL_AMPLITUDE_V, },
    { N_("Amplitude (nm)"),    SIGNAL_AMPLITUDE_NM, },
     };

    dialog = gtk_dialog_new_with_buttons(_("MFM recalculate"), NULL, 0,
                                         _("_Reset"), RESPONSE_RESET,
                                         GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
                                         GTK_STOCK_OK, GTK_RESPONSE_OK,
                                         NULL);
    gtk_dialog_set_default_response(GTK_DIALOG(dialog), GTK_RESPONSE_OK);
    gwy_help_add_to_proc_dialog(GTK_DIALOG(dialog), GWY_HELP_DEFAULT);

    controls.args = args;

    table = gtk_table_new(2, 4, FALSE);
    gtk_table_set_row_spacings(GTK_TABLE(table), 2);
    gtk_table_set_col_spacings(GTK_TABLE(table), 6);
    gtk_container_set_border_width(GTK_CONTAINER(table), 4);
    gtk_box_pack_start(GTK_BOX(GTK_DIALOG(dialog)->vbox), table,
                       FALSE, FALSE, 4);
    row = 0;

    controls.signal
        = gwy_enum_combo_box_new(signals, G_N_ELEMENTS(signals),
                                 G_CALLBACK(signal_selected),
                                 &controls, args->signal, TRUE);
    gwy_table_attach_adjbar(table, row, _("Input _type:"), NULL,
                            GTK_OBJECT(controls.signal),
                            GWY_HSCALE_WIDGET_NO_EXPAND);
    row++;

    controls.spring_constant = gtk_adjustment_new(args->spring_constant, 0.01, 1000.0, 0.01, 1, 0);
    gwy_table_attach_adjbar(table, row, _("_Spring constant:"), _("N/m"),
                            controls.spring_constant, GWY_HSCALE_LOG);
    row++;

    controls.quality = gtk_adjustment_new(args->quality, 0.01, 10000.0, 0.01, 1, 0);
    controls.quality_widget = gwy_table_attach_adjbar(table, row, _("_Quality factor:"), NULL,
                            controls.quality, GWY_HSCALE_LOG);
    row++;

    controls.base_frequency = gtk_adjustment_new(args->base_frequency, 1, 1000000.0, 1, 10, 0);
    controls.base_frequency_widget = gwy_table_attach_adjbar(table, row, _("_Base frequency:"), _("Hz"),
                            controls.base_frequency, GWY_HSCALE_LOG);
    row++;

    controls.base_amplitude = gtk_adjustment_new(args->base_amplitude, 0.01, 1000.0, 0.01, 1, 0);
    controls.base_amplitude_widget = gwy_table_attach_adjbar(table, row, _("_Base amplitude:"), _("V, nm"),
                            controls.base_amplitude, GWY_HSCALE_LOG);
    row++;

    update_sensitivity(&controls, args);

    gtk_widget_show_all(dialog);
    do {
        response = gtk_dialog_run(GTK_DIALOG(dialog));
        switch (response) {
            case GTK_RESPONSE_CANCEL:
            case GTK_RESPONSE_DELETE_EVENT:
            gtk_widget_destroy(dialog);
            case GTK_RESPONSE_NONE:
            return FALSE;
            break;

            case GTK_RESPONSE_OK:
            break;

            case RESPONSE_RESET:
            *args = mfm_recalc_defaults;
            args->signal = guess;
            mfm_recalc_dialog_update(&controls, args);
            break;

            default:
            g_assert_not_reached();
            break;
        }
    } while (response != GTK_RESPONSE_OK);

    gtk_widget_destroy(dialog);

    return TRUE;
}

static void
mfm_recalc_dialog_update(MfmRecalcControls *controls,
                  MfmRecalcArgs *args)
{
    gtk_adjustment_set_value(GTK_ADJUSTMENT(controls->spring_constant),
                  args->spring_constant);

    gtk_adjustment_set_value(GTK_ADJUSTMENT(controls->quality),
                  args->quality);

    gtk_adjustment_set_value(GTK_ADJUSTMENT(controls->base_frequency),
                  args->base_frequency);

    gtk_adjustment_set_value(GTK_ADJUSTMENT(controls->base_amplitude),
                  args->base_amplitude);

    gwy_enum_combo_box_set_active(GTK_COMBO_BOX(controls->signal),
                  args->signal);

}

static void       
signal_selected(GtkComboBox *combo, MfmRecalcControls *controls)
{
    controls->args->signal = gwy_enum_combo_box_get_active(combo);

    update_sensitivity(controls, controls->args);
}


static void     
update_sensitivity(MfmRecalcControls *controls,
                              MfmRecalcArgs *args)
{
    if (args->signal == SIGNAL_PHASE_DEG || args->signal == SIGNAL_PHASE_RAD) {
        gtk_widget_set_sensitive(controls->base_frequency_widget, FALSE);
        gtk_widget_set_sensitive(controls->quality_widget, TRUE);
        gtk_widget_set_sensitive(controls->base_amplitude_widget, FALSE);
    }
    if (args->signal == SIGNAL_FREQUENCY) {
        gtk_widget_set_sensitive(controls->base_frequency_widget, TRUE);
        gtk_widget_set_sensitive(controls->quality_widget, FALSE);
        gtk_widget_set_sensitive(controls->base_amplitude_widget, FALSE);
    }
    if (args->signal == SIGNAL_AMPLITUDE_V || args->signal == SIGNAL_AMPLITUDE_NM) {
        gtk_widget_set_sensitive(controls->base_frequency_widget, FALSE);
        gtk_widget_set_sensitive(controls->quality_widget, TRUE);
        gtk_widget_set_sensitive(controls->base_amplitude_widget, TRUE);
    }
}
 

static const gchar spring_constant_key[]   = "/module/mfm_recalc/spring_constant";
static const gchar quality_key[]   = "/module/mfm_recalc/quality";
static const gchar signal_key[]  = "/module/mfm_recalc/signal";
static const gchar base_frequency_key[] = "/module/mfm_recalc/base_frequency";
static const gchar base_amplitude_key[] = "/module/mfm_recalc/base_amplitude";
  

static void
mfm_recalc_sanitize_args(MfmRecalcArgs *args)
{
    args->signal = CLAMP(args->signal, 0, SIGNAL_AMPLITUDE_NM);
    args->spring_constant = CLAMP(args->spring_constant, 0.01, 1000);
    args->quality = CLAMP(args->quality, 0.01, 10000);
    args->base_frequency = CLAMP(args->base_frequency, 1, 1000000);
    args->base_amplitude = CLAMP(args->base_amplitude, 0.1, 100);
}

static void
mfm_recalc_load_args(GwyContainer *container,
              MfmRecalcArgs *args)
{
    *args = mfm_recalc_defaults;

    gwy_container_gis_enum_by_name(container, signal_key, &args->signal);
    gwy_container_gis_double_by_name(container, spring_constant_key, &args->spring_constant);
    gwy_container_gis_double_by_name(container, quality_key, &args->quality);
    gwy_container_gis_double_by_name(container, base_frequency_key, &args->base_frequency);
    gwy_container_gis_double_by_name(container, base_amplitude_key, &args->base_amplitude);

    mfm_recalc_sanitize_args(args);
}

static void
mfm_recalc_save_args(GwyContainer *container,
              MfmRecalcArgs *args)
{
    gwy_container_set_enum_by_name(container, signal_key, args->signal);
    gwy_container_set_double_by_name(container, spring_constant_key, args->spring_constant);
    gwy_container_set_double_by_name(container, quality_key, args->quality);
    gwy_container_set_double_by_name(container, base_frequency_key, args->base_frequency);
    gwy_container_set_double_by_name(container, base_amplitude_key, args->base_amplitude);
    
}

/* vim: set cin et ts=4 sw=4 cino=>1s,e0,n0,f0,{0,}0,^0,\:1s,=0,g1s,h0,t0,+1s,c3,(0,u0 : */
