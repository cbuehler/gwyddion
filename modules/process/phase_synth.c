/*
 *  $Id$
 *  Copyright (C) 2017 David Necas (Yeti).
 *  E-mail: yeti@gwyddion.net.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA 02110-1301, USA.
 */

#include "config.h"
#include <string.h>
#include <stdlib.h>
#include <gtk/gtk.h>
#include <libgwyddion/gwymacros.h>
#include <libgwyddion/gwymath.h>
#include <libgwyddion/gwyrandgenset.h>
#include <libprocess/arithmetic.h>
#include <libprocess/elliptic.h>
#include <libprocess/filters.h>
#include <libprocess/grains.h>
#include <libprocess/inttrans.h>
#include <libprocess/stats.h>
#include <libgwymodule/gwymodule-process.h>
#include <libgwydgets/gwystock.h>
#include <app/gwyapp.h>
#include "preview.h"
#include "dimensions.h"

#define PHASE_SYNTH_RUN_MODES (GWY_RUN_IMMEDIATE | GWY_RUN_INTERACTIVE)

enum {
    PAGE_DIMENSIONS = 0,
    PAGE_GENERATOR  = 1,
    PAGE_NPAGES
};

typedef struct {
    gint active_page;
    gint seed;
    gboolean randomize;
    gboolean update;
    gdouble size;
    gdouble size_noise;
    gdouble height;
} PhaseSynthArgs;

typedef struct {
    PhaseSynthArgs *args;
    GwyDimensions *dims;
    GtkWidget *dialog;
    GtkWidget *view;
    GtkWidget *update;
    GtkWidget *update_now;
    GtkObject *seed;
    GtkTable *table;
    GtkWidget *randomize;
    GtkObject *size;
    GtkWidget *size_value;
    GtkWidget *size_units;
    GtkObject *size_noise;
    GtkObject *height;
    GtkWidget *height_units;
    GtkWidget *height_init;
    GwyContainer *mydata;
    GwyDataField *surface;
    GwyDataField *phases;
    gdouble pxsize;
    gdouble zscale;
    gboolean in_init;
    gulong sid;
} PhaseSynthControls;

static gboolean module_register       (void);
static void     phase_synth           (GwyContainer *data,
                                       GwyRunType run);
static void     run_noninteractive    (PhaseSynthArgs *args,
                                       const GwyDimensionArgs *dimsargs,
                                       GwyContainer *data,
                                       GwyDataField *dfield,
                                       gint oldid,
                                       GQuark quark);
static gboolean phase_synth_dialog    (PhaseSynthArgs *args,
                                       GwyDimensionArgs *dimsargs,
                                       GwyContainer *data,
                                       GwyDataField *dfield,
                                       gint id);
static void     update_controls       (PhaseSynthControls *controls,
                                       PhaseSynthArgs *args);
static void     page_switched         (PhaseSynthControls *controls,
                                       GtkNotebookPage *page,
                                       gint pagenum);
static void     update_values         (PhaseSynthControls *controls);
static void     height_init_clicked   (PhaseSynthControls *controls);
static void     phase_synth_invalidate(PhaseSynthControls *controls);
static gboolean preview_gsource       (gpointer user_data);
static void     preview               (PhaseSynthControls *controls);
static void     phase_synth_do        (const PhaseSynthArgs *args,
                                       const GwyDimensionArgs *dimsargs,
                                       GwyDataField *dfield);
static void     phase_synth_load_args (GwyContainer *container,
                                       PhaseSynthArgs *args,
                                       GwyDimensionArgs *dimsargs);
static void     phase_synth_save_args (GwyContainer *container,
                                       const PhaseSynthArgs *args,
                                       const GwyDimensionArgs *dimsargs);

#define GWY_SYNTH_CONTROLS PhaseSynthControls
#define GWY_SYNTH_INVALIDATE(controls) \
    phase_synth_invalidate(controls)

#include "synth.h"

static const PhaseSynthArgs phase_synth_defaults = {
    PAGE_DIMENSIONS,
    42, TRUE, TRUE,
    20.0, 0.05, 1.0,
};

static const GwyDimensionArgs dims_defaults = GWY_DIMENSION_ARGS_INIT;

static GwyModuleInfo module_info = {
    GWY_MODULE_ABI_VERSION,
    &module_register,
    N_("Generates phase-separated structures."),
    "Yeti <yeti@gwyddion.net>",
    "1.1",
    "David Nečas (Yeti)",
    "2017",
};

GWY_MODULE_QUERY2(module_info, phase_synth)

static gboolean
module_register(void)
{
    gwy_process_func_register("phase_synth",
                              (GwyProcessFunc)&phase_synth,
                              N_("/S_ynthetic/P_hases..."),
                              GWY_STOCK_SYNTHETIC_PHASES,
                              PHASE_SYNTH_RUN_MODES,
                              0,
                              N_("Generate surface with separated phases"));

    return TRUE;
}

static void
phase_synth(GwyContainer *data, GwyRunType run)
{
    PhaseSynthArgs args;
    GwyDimensionArgs dimsargs;
    GwyDataField *dfield;
    GQuark quark;
    gint id;

    g_return_if_fail(run & PHASE_SYNTH_RUN_MODES);
    phase_synth_load_args(gwy_app_settings_get(), &args, &dimsargs);
    gwy_app_data_browser_get_current(GWY_APP_DATA_FIELD, &dfield,
                                     GWY_APP_DATA_FIELD_ID, &id,
                                     GWY_APP_DATA_FIELD_KEY, &quark,
                                     0);

    if (run == GWY_RUN_IMMEDIATE
        || phase_synth_dialog(&args, &dimsargs, data, dfield, id))
        run_noninteractive(&args, &dimsargs, data, dfield, id, quark);

    if (run == GWY_RUN_INTERACTIVE)
        phase_synth_save_args(gwy_app_settings_get(), &args, &dimsargs);

    gwy_dimensions_free_args(&dimsargs);
}

static gboolean
phase_synth_dialog(PhaseSynthArgs *args,
                   GwyDimensionArgs *dimsargs,
                   GwyContainer *data,
                   GwyDataField *dfield_template,
                   gint id)
{
    GtkWidget *dialog, *table, *vbox, *hbox, *notebook, *spin;
    PhaseSynthControls controls;
    GwyDataField *dfield;
    GtkObject *adj;
    gboolean finished;
    gint response;
    gint row;

    gwy_clear(&controls, 1);
    controls.in_init = TRUE;
    controls.args = args;
    dialog = gtk_dialog_new_with_buttons(_("Separated Phases"),
                                         NULL, 0,
                                         _("_Reset"), RESPONSE_RESET,
                                         GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
                                         GTK_STOCK_OK, GTK_RESPONSE_OK,
                                         NULL);
    gtk_dialog_set_default_response(GTK_DIALOG(dialog), GTK_RESPONSE_OK);
    gwy_help_add_to_proc_dialog(GTK_DIALOG(dialog), GWY_HELP_DEFAULT);
    controls.dialog = dialog;

    hbox = gtk_hbox_new(FALSE, 2);
    gtk_box_pack_start(GTK_BOX(GTK_DIALOG(dialog)->vbox), hbox,
                       FALSE, FALSE, 4);

    vbox = gtk_vbox_new(FALSE, 4);
    gtk_box_pack_start(GTK_BOX(hbox), vbox, FALSE, FALSE, 4);

    controls.mydata = gwy_container_new();
    dfield = gwy_data_field_new(PREVIEW_SIZE, PREVIEW_SIZE,
                                dimsargs->measure*PREVIEW_SIZE,
                                dimsargs->measure*PREVIEW_SIZE,
                                TRUE);
    gwy_container_set_object_by_name(controls.mydata, "/0/data", dfield);
    controls.phases = gwy_data_field_new_alike(dfield, FALSE);
    if (dfield_template) {
        gwy_app_sync_data_items(data, controls.mydata, id, 0, FALSE,
                                GWY_DATA_ITEM_PALETTE,
                                0);
        controls.surface = gwy_synth_surface_for_preview(dfield_template,
                                                         PREVIEW_SIZE);
        controls.zscale = gwy_data_field_get_rms(dfield_template);
    }
    controls.view = create_preview(controls.mydata, 0, PREVIEW_SIZE, FALSE);
    gtk_box_pack_start(GTK_BOX(vbox), controls.view, FALSE, FALSE, 0);

    gtk_box_pack_start(GTK_BOX(vbox),
                       gwy_synth_instant_updates_new(&controls,
                                                     &controls.update_now,
                                                     &controls.update,
                                                     &args->update),
                       FALSE, FALSE, 0);
    g_signal_connect_swapped(controls.update_now, "clicked",
                             G_CALLBACK(preview), &controls);

    gtk_box_pack_start(GTK_BOX(vbox),
                       gwy_synth_random_seed_new(&controls,
                                                 &controls.seed, &args->seed),
                       FALSE, FALSE, 0);

    controls.randomize = gwy_synth_randomize_new(&args->randomize);
    gtk_box_pack_start(GTK_BOX(vbox), controls.randomize, FALSE, FALSE, 0);

    notebook = gtk_notebook_new();
    gtk_box_pack_start(GTK_BOX(hbox), notebook, TRUE, TRUE, 4);
    g_signal_connect_swapped(notebook, "switch-page",
                             G_CALLBACK(page_switched), &controls);

    controls.dims = gwy_dimensions_new(dimsargs, dfield_template);
    gtk_notebook_append_page(GTK_NOTEBOOK(notebook),
                             gwy_dimensions_get_widget(controls.dims),
                             gtk_label_new(_("Dimensions")));
    if (controls.dims->add)
        g_signal_connect_swapped(controls.dims->add, "toggled",
                                 G_CALLBACK(phase_synth_invalidate), &controls);

    table = gtk_table_new(3 + (dfield_template ? 1 : 0), 3, FALSE);
    controls.table = GTK_TABLE(table);
    gtk_table_set_row_spacings(controls.table, 2);
    gtk_table_set_col_spacings(controls.table, 6);
    gtk_container_set_border_width(GTK_CONTAINER(table), 4);
    gtk_notebook_append_page(GTK_NOTEBOOK(notebook), table,
                             gtk_label_new(_("Generator")));
    row = 0;

    controls.size = gtk_adjustment_new(args->size, 1.0, 1000.0, 0.1, 10.0, 0);
    row = gwy_synth_attach_lateral(&controls, row, controls.size, &args->size,
                                   _("_Size:"), GWY_HSCALE_LOG,
                                   NULL,
                                   &controls.size_value, &controls.size_units);

    adj = gtk_adjustment_new(args->size_noise, 0.001, 0.5, 0.001, 0.1, 0);
    g_object_set_data(G_OBJECT(adj), "target", &args->size_noise);

    spin = gwy_table_attach_adjbar(GTK_WIDGET(controls.table),
                                   row, _("Size s_pread:"), NULL, adj,
                                   GWY_HSCALE_SQRT);
    gtk_spin_button_set_snap_to_ticks(GTK_SPIN_BUTTON(spin), FALSE);
    gtk_spin_button_set_digits(GTK_SPIN_BUTTON(spin), 3);
    g_signal_connect_swapped(adj, "value-changed",
                             G_CALLBACK(gwy_synth_double_changed), &controls);
    row++;

    gtk_table_set_row_spacing(GTK_TABLE(table), row-1, 8);
    row = gwy_synth_attach_height(&controls, row,
                                  &controls.height, &args->height,
                                  _("_Height:"), NULL, &controls.height_units);

    if (dfield_template) {
        controls.height_init
            = gtk_button_new_with_mnemonic(_("_Like Current Image"));
        g_signal_connect_swapped(controls.height_init, "clicked",
                                 G_CALLBACK(height_init_clicked), &controls);
        gtk_table_attach(GTK_TABLE(table), controls.height_init,
                         0, 2, row, row+1, GTK_FILL, 0, 0, 0);
        row++;
    }

    gtk_widget_show_all(dialog);
    controls.in_init = FALSE;
    /* Must be done when widgets are shown, see GtkNotebook docs */
    gtk_notebook_set_current_page(GTK_NOTEBOOK(notebook), args->active_page);
    update_values(&controls);
    phase_synth_invalidate(&controls);

    finished = FALSE;
    while (!finished) {
        response = gtk_dialog_run(GTK_DIALOG(dialog));
        switch (response) {
            case GTK_RESPONSE_CANCEL:
            case GTK_RESPONSE_DELETE_EVENT:
            case GTK_RESPONSE_OK:
            gtk_widget_destroy(dialog);
            case GTK_RESPONSE_NONE:
            finished = TRUE;
            break;

            case RESPONSE_RESET:
            {
                gboolean temp = args->update;
                gint temp2 = args->active_page;
                *args = phase_synth_defaults;
                args->active_page = temp2;
                args->update = temp;
            }
            controls.in_init = TRUE;
            update_controls(&controls, args);
            controls.in_init = FALSE;
            if (args->update)
                preview(&controls);
            break;

            default:
            g_assert_not_reached();
            break;
        }
    }

    if (controls.sid) {
        g_source_remove(controls.sid);
        controls.sid = 0;
    }
    g_object_unref(controls.mydata);
    GWY_OBJECT_UNREF(controls.surface);
    GWY_OBJECT_UNREF(controls.phases);
    gwy_dimensions_free(controls.dims);

    return response == GTK_RESPONSE_OK;
}

static void
update_controls(PhaseSynthControls *controls,
                PhaseSynthArgs *args)
{
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(controls->update),
                                 args->update);
    gtk_adjustment_set_value(GTK_ADJUSTMENT(controls->seed), args->seed);
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(controls->randomize),
                                 args->randomize);
    gtk_adjustment_set_value(GTK_ADJUSTMENT(controls->height), args->height);
    gtk_adjustment_set_value(GTK_ADJUSTMENT(controls->size),
                             args->size);
}

static void
page_switched(PhaseSynthControls *controls,
              G_GNUC_UNUSED GtkNotebookPage *page,
              gint pagenum)
{
    if (controls->in_init)
        return;

    controls->args->active_page = pagenum;

    if (pagenum == PAGE_GENERATOR)
        update_values(controls);
}

static void
update_values(PhaseSynthControls *controls)
{
    GwyDimensions *dims = controls->dims;

    controls->pxsize = dims->args->measure * pow10(dims->args->xypow10);
    if (controls->height_units)
        gtk_label_set_markup(GTK_LABEL(controls->height_units),
                             dims->zvf->units);
    gtk_label_set_markup(GTK_LABEL(controls->size_units),
                         dims->xyvf->units);

    gwy_synth_update_lateral(controls, GTK_ADJUSTMENT(controls->size));
}

static void
height_init_clicked(PhaseSynthControls *controls)
{
    gdouble mag = pow10(controls->dims->args->zpow10);
    gtk_adjustment_set_value(GTK_ADJUSTMENT(controls->height),
                             controls->zscale/mag);
}

static void
phase_synth_invalidate(PhaseSynthControls *controls)
{
    /* create preview if instant updates are on */
    if (controls->args->update && !controls->in_init && !controls->sid) {
        controls->sid = g_idle_add_full(G_PRIORITY_LOW, preview_gsource,
                                        controls, NULL);
    }
}

static gboolean
preview_gsource(gpointer user_data)
{
    PhaseSynthControls *controls = (PhaseSynthControls*)user_data;
    controls->sid = 0;

    preview(controls);

    return FALSE;
}

static void
preview(PhaseSynthControls *controls)
{
    PhaseSynthArgs *args = controls->args;
    GwyDataField *dfield;

    dfield = GWY_DATA_FIELD(gwy_container_get_object_by_name(controls->mydata,
                                                             "/0/data"));
    if (controls->dims->args->add && controls->surface)
        gwy_data_field_copy(controls->surface, dfield, FALSE);
    else
        gwy_data_field_clear(dfield);

    phase_synth_do(args, controls->dims->args, dfield);

    gwy_data_field_data_changed(dfield);
}

static void
run_noninteractive(PhaseSynthArgs *args,
                   const GwyDimensionArgs *dimsargs,
                   GwyContainer *data,
                   GwyDataField *dfield,
                   gint oldid,
                   GQuark quark)
{
    GwySIUnit *siunit;
    gboolean replace = dimsargs->replace && dfield;
    gboolean add = dimsargs->add && dfield;
    gint newid;

    if (args->randomize)
        args->seed = g_random_int() & 0x7fffffff;

    if (replace) {
        /* Always take a reference so that we can always unref. */
        g_object_ref(dfield);

        gwy_app_undo_qcheckpointv(data, 1, &quark);
        if (!add)
            gwy_data_field_clear(dfield);

        gwy_app_channel_log_add_proc(data, oldid, oldid);
    }
    else {
        if (add)
            dfield = gwy_data_field_duplicate(dfield);
        else {
            gdouble mag = pow10(dimsargs->xypow10) * dimsargs->measure;
            dfield = gwy_data_field_new(dimsargs->xres, dimsargs->yres,
                                        mag*dimsargs->xres, mag*dimsargs->yres,
                                        TRUE);

            siunit = gwy_data_field_get_si_unit_xy(dfield);
            gwy_si_unit_set_from_string(siunit, dimsargs->xyunits);

            siunit = gwy_data_field_get_si_unit_z(dfield);
            gwy_si_unit_set_from_string(siunit, dimsargs->zunits);
        }
    }

    phase_synth_do(args, dimsargs, dfield);

    if (replace)
        gwy_data_field_data_changed(dfield);
    else {
        if (data) {
            newid = gwy_app_data_browser_add_data_field(dfield, data, TRUE);
            if (oldid != -1)
                gwy_app_sync_data_items(data, data, oldid, newid, FALSE,
                                        GWY_DATA_ITEM_GRADIENT,
                                        0);
        }
        else {
            newid = 0;
            data = gwy_container_new();
            gwy_container_set_object(data, gwy_app_get_data_key_for_id(newid),
                                     dfield);
            gwy_app_data_browser_add(data);
            gwy_app_data_browser_reset_visibility(data,
                                                  GWY_VISIBILITY_RESET_SHOW_ALL);
            g_object_unref(data);
        }

        gwy_app_set_data_field_title(data, newid, _("Generated"));
        gwy_app_channel_log_add_proc(data, add ? oldid : -1, newid);
    }
    g_object_unref(dfield);
}

#ifdef HAVE_SINCOS
#define _gwy_sincos sincos
#else
static inline void
_gwy_sincos(gdouble x, gdouble *s, gdouble *c)
{
    *s = sin(x);
    *c = cos(x);
}
#endif

static void
generate_narrow_freq_surface(gdouble freq, gdouble freq_range,
                             GwyDataField *buf_re, GwyDataField *buf_im,
                             GwyDataField *out_re, GwyDataField *out_im,
                             GRand *rng)
{
    gdouble *re, *im;
    gint xres, yres, i, j, k;

    re = gwy_data_field_get_data(buf_re);
    im = gwy_data_field_get_data(buf_im);

    xres = gwy_data_field_get_xres(out_re);
    yres = gwy_data_field_get_yres(out_re);

    freq /= G_PI;
    freq_range /= G_PI;

    k = 0;
    for (i = 0; i < yres; i++) {
        gdouble y = (i <= yres/2 ? i : yres-i)/(0.5*yres);
        for (j = 0; j < xres; j++) {
            gdouble x = (j <= xres/2 ? j : xres-j)/(0.5*xres);
            gdouble r = sqrt(x*x + y*y);
            gdouble f, phi, s, c;

            phi = 2.0*G_PI*g_rand_double(rng);
            f = g_rand_double(rng);
            r = fabs((r - freq)/freq_range);
            if (r > 30.0)
                re[k] = im[k] = 0.0;
            else {
                _gwy_sincos(phi, &s, &c);
                r = exp(r);
                f /= r + 1.0/r;
                re[k] = f*s;
                im[k] = f*c;
            }
            k++;
        }
    }
    re[0] = im[0] = 0.0;

    gwy_data_field_2dfft_raw(buf_re, buf_im, out_re, out_im,
                             GWY_TRANSFORM_DIRECTION_BACKWARD);
}

static void
threshold_based_on_distance(GwyDataField *dfield,
                            GwyDataField *buf1,
                            GwyDataField *buf2,
                            GwyDataField *result)
{
    gdouble thresh;

    thresh = gwy_data_field_otsu_threshold(dfield);

    gwy_data_field_copy(dfield, buf1, FALSE);
    gwy_data_field_threshold(buf1, thresh, 0.0, 1.0);
    gwy_data_field_grains_invert(buf1);
    gwy_data_field_grains_thin(buf1);
    gwy_data_field_mark_extrema(dfield, result, FALSE);
    gwy_data_field_max_of_fields(buf1, result, buf1);
    gwy_data_field_grains_invert(buf1);
    gwy_data_field_grain_distance_transform(buf1);

    gwy_data_field_copy(dfield, buf2, FALSE);
    gwy_data_field_threshold(buf2, thresh, 0.0, 1.0);
    gwy_data_field_grains_thin(buf2);
    gwy_data_field_mark_extrema(dfield, result, TRUE);
    gwy_data_field_max_of_fields(buf2, result, buf2);
    gwy_data_field_grains_invert(buf2);
    gwy_data_field_grain_distance_transform(buf2);

    gwy_data_field_subtract_fields(result, buf1, buf2);
    gwy_data_field_threshold(result, 0.0, 0.0, 1.0);
}

#if 0
static GwyXY*
find_extrema_pointset(GwyDataField *dfield, GwyDataField *buf, gboolean maxima,
                      guint *npoints)
{
    const GwyGrainQuantity quantities[] = {
        GWY_GRAIN_VALUE_CENTER_X, GWY_GRAIN_VALUE_CENTER_Y,
    };
    guint xres = gwy_data_field_get_xres(dfield);
    guint yres = gwy_data_field_get_yres(dfield);
    gint *grains = g_new0(gint, xres*yres);
    gdouble **values;
    guint ngrains, i;
    GwyXY *coords;

    gwy_data_field_mark_extrema(dfield, buf, maxima);
    ngrains = gwy_data_field_number_grains(buf, grains);

    values = gwy_data_field_grains_get_quantities(dfield, NULL, quantities, 2,
                                                  ngrains, grains);
    coords = g_new(GwyXY, ngrains);
    for (i = 0; i < ngrains; i++) {
        coords[i].x = values[0][i+1];
        coords[i].y = values[1][i+1];
    }

    g_free(grains);
    g_free(values[0]);
    g_free(values[1]);
    g_free(values);

    *npoints = ngrains;
    return coords;
}
#endif

static void
regularise_with_asf(GwyDataField *dfield, GwyDataField *buf,
                    GwyDataField *kernel, guint maxksize)
{
    guint i, res, xres = dfield->xres, yres = dfield->yres;

    gwy_data_field_copy(dfield, buf, FALSE);
    for (i = 1; i <= maxksize; i++) {
        res = 2*i + 1;
        gwy_data_field_resample(kernel, res, res, GWY_INTERPOLATION_NONE);
        gwy_data_field_clear(kernel);
        gwy_data_field_elliptic_area_fill(kernel, 0, 0, res, res, 1.0);

        gwy_data_field_area_filter_min_max(dfield, kernel,
                                           GWY_MIN_MAX_FILTER_OPENING,
                                           0, 0, xres, yres);
        gwy_data_field_area_filter_min_max(dfield, kernel,
                                           GWY_MIN_MAX_FILTER_CLOSING,
                                           0, 0, xres, yres);

        gwy_data_field_area_filter_min_max(buf, kernel,
                                           GWY_MIN_MAX_FILTER_CLOSING,
                                           0, 0, xres, yres);
        gwy_data_field_area_filter_min_max(buf, kernel,
                                           GWY_MIN_MAX_FILTER_OPENING,
                                           0, 0, xres, yres);
    }
    gwy_data_field_sum_fields(dfield, buf, dfield);
    gwy_data_field_multiply(dfield, 0.5);
}

static void
phase_synth_do(const PhaseSynthArgs *args,
               const GwyDimensionArgs *dimsargs,
               GwyDataField *dfield)
{
    GwyDataField *buf1, *buf2, *buf3, *kernel, *tmp;
    guint xres, yres, kxres, kyres, extsize, asfradius;
    GwyRandGenSet *rngset;
    gdouble freq_range = args->size_noise, freq = G_PI/args->size;

    /* spread is relative */
    freq_range *= freq;
    /* rough mean frequency correction */
    freq /= 1.0 + pow(freq_range/freq, 2)/3.0;

    kxres = GWY_ROUND(2.0*G_PI/freq * 1.2) | 1;
    kyres = GWY_ROUND(2.0*G_PI/freq * 1.2) | 1;
    extsize = GWY_ROUND(G_PI/freq);
    asfradius = GWY_ROUND(0.08*2.0*G_PI/freq);
    gwy_debug("kernel %ux%u, extsize %u, asf %u",
              kxres, kyres, extsize, asfradius);

    xres = gwy_data_field_get_xres(dfield);
    yres = gwy_data_field_get_yres(dfield);

    buf1 = gwy_data_field_new_alike(dfield, FALSE);
    buf2 = gwy_data_field_new_alike(dfield, FALSE);
    buf3 = gwy_data_field_new_alike(dfield, FALSE);

    rngset = gwy_rand_gen_set_new(1);
    gwy_rand_gen_set_init(rngset, args->seed);

    generate_narrow_freq_surface(freq, freq_range, buf1, buf2, dfield, buf3,
                                 gwy_rand_gen_set_rng(rngset, 0));
    tmp = gwy_data_field_extend(dfield, extsize, extsize, extsize, extsize,
                                GWY_EXTERIOR_PERIODIC, 0.0, FALSE);
    gwy_data_field_resample(buf1, tmp->xres, tmp->yres, GWY_INTERPOLATION_NONE);
    gwy_data_field_resample(buf2, tmp->xres, tmp->yres, GWY_INTERPOLATION_NONE);
    gwy_data_field_resample(buf3, tmp->xres, tmp->yres, GWY_INTERPOLATION_NONE);

    kernel = gwy_data_field_new(kxres, kyres, 1.0, 1.0, TRUE);
    gwy_data_field_elliptic_area_fill(kernel, 0, 0, kxres, kyres, 1.0);
    gwy_data_field_area_filter_min_max(dfield, kernel,
                                       GWY_MIN_MAX_FILTER_NORMALIZATION,
                                       0, 0, xres, yres);
    gwy_data_field_copy(tmp, buf3, FALSE);
    threshold_based_on_distance(buf3, buf1, buf2, tmp);
    regularise_with_asf(tmp, buf1, kernel, asfradius);

    gwy_data_field_area_copy(tmp, dfield,
                             extsize, extsize, dfield->xres, dfield->yres,
                             0, 0);
    gwy_data_field_multiply(dfield, args->height * pow10(dimsargs->zpow10));

    g_object_unref(kernel);
    g_object_unref(tmp);
    g_object_unref(buf1);
    g_object_unref(buf2);
    g_object_unref(buf3);

    gwy_rand_gen_set_free(rngset);
}

static const gchar prefix[]          = "/module/phase_synth";
static const gchar active_page_key[] = "/module/phase_synth/active_page";
static const gchar height_key[]      = "/module/phase_synth/height";
static const gchar randomize_key[]   = "/module/phase_synth/randomize";
static const gchar seed_key[]        = "/module/phase_synth/seed";
static const gchar size_key[]        = "/module/phase_synth/size";
static const gchar size_noise_key[]  = "/module/phase_synth/size_noise";
static const gchar update_key[]      = "/module/phase_synth/update";

static void
phase_synth_sanitize_args(PhaseSynthArgs *args)
{
    args->active_page = CLAMP(args->active_page,
                              PAGE_DIMENSIONS, PAGE_NPAGES-1);
    args->update = !!args->update;
    args->seed = MAX(0, args->seed);
    args->randomize = !!args->randomize;
    args->size = CLAMP(args->size, 1.0, 1000.0);
    args->size_noise = CLAMP(args->size_noise, 0.001, 0.5);
    args->height = CLAMP(args->height, 0.001, 10000.0);
}

static void
phase_synth_load_args(GwyContainer *container,
                      PhaseSynthArgs *args,
                      GwyDimensionArgs *dimsargs)
{
    *args = phase_synth_defaults;

    gwy_container_gis_int32_by_name(container, active_page_key,
                                    &args->active_page);
    gwy_container_gis_boolean_by_name(container, update_key, &args->update);
    gwy_container_gis_int32_by_name(container, seed_key, &args->seed);
    gwy_container_gis_boolean_by_name(container, randomize_key,
                                      &args->randomize);
    gwy_container_gis_double_by_name(container, height_key, &args->height);
    gwy_container_gis_double_by_name(container, size_key, &args->size);
    gwy_container_gis_double_by_name(container, size_noise_key,
                                     &args->size_noise);
    phase_synth_sanitize_args(args);

    gwy_clear(dimsargs, 1);
    gwy_dimensions_copy_args(&dims_defaults, dimsargs);
    gwy_dimensions_load_args(dimsargs, container, prefix);
}

static void
phase_synth_save_args(GwyContainer *container,
                      const PhaseSynthArgs *args,
                      const GwyDimensionArgs *dimsargs)
{
    gwy_container_set_int32_by_name(container, active_page_key,
                                    args->active_page);
    gwy_container_set_boolean_by_name(container, update_key, args->update);
    gwy_container_set_int32_by_name(container, seed_key, args->seed);
    gwy_container_set_boolean_by_name(container, randomize_key,
                                      args->randomize);
    gwy_container_set_double_by_name(container, height_key, args->height);
    gwy_container_set_double_by_name(container, size_key, args->size);
    gwy_container_set_double_by_name(container, size_noise_key,
                                     args->size_noise);

    gwy_dimensions_save_args(dimsargs, container, prefix);
}

/* vim: set cin et ts=4 sw=4 cino=>1s,e0,n0,f0,{0,}0,^0,\:1s,=0,g1s,h0,t0,+1s,c3,(0,u0 : */
