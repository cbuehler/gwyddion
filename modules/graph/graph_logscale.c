/*
 *  $Id$
 *  Copyright (C) 2016 David Necas (Yeti).
 *  E-mail: yeti@gwyddion.net.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA 02110-1301, USA.
 */

#include "config.h"
#include <gtk/gtk.h>
#include <libgwyddion/gwymacros.h>
#include <libgwydgets/gwygraphmodel.h>
#include <libgwydgets/gwyradiobuttons.h>
#include <libgwydgets/gwystock.h>
#include <libgwymodule/gwymodule-graph.h>
#include <app/gwyapp.h>
#include <app/gwymoduleutils.h>

typedef enum {
    LOGSCALE_AXIS_X    = 1,
    LOGSCALE_AXIS_Y    = 2,
    LOGSCALE_AXIS_BOTH = 3,
} LogscaleAxisType;

typedef enum {
    LOGSCALE_NEGATIVE_SKIP = 0,
    LOGSCALE_NEGATIVE_ABS = 1,
} LogscaleNegativeType;

typedef enum {
    LOGSCALE_BASE_E  = 0,
    LOGSCALE_BASE_10 = 1,
} LogscaleBaseType;

typedef struct {
    gdouble base;
    LogscaleAxisType axes;
    LogscaleNegativeType negative_y;
} LogscaleArgs;

static gboolean            module_register   (void);
static void                logscale          (GwyGraph *graph);
static gchar*              logscale_label    (const gchar *label,
                                              gdouble base);
static GwyGraphCurveModel* logscale_curve    (GwyGraphCurveModel *gcmodel,
                                              const LogscaleArgs *args);
static gboolean            logscale_dialogue (LogscaleArgs *args);
static void                axes_changed      (GtkToggleButton *toggle,
                                              LogscaleArgs *args);
static void                negative_y_changed(GtkToggleButton *toggle,
                                              LogscaleArgs *args);
static void                base_changed      (GtkToggleButton *toggle,
                                              LogscaleArgs *args);
static void                load_args         (GwyContainer *settings,
                                              LogscaleArgs *args);
static void                save_args         (GwyContainer *settings,
                                              LogscaleArgs *args);

static const LogscaleArgs logscale_defaults = {
    LOGSCALE_AXIS_BOTH, G_E, LOGSCALE_NEGATIVE_ABS,
};

static GwyModuleInfo module_info = {
    GWY_MODULE_ABI_VERSION,
    &module_register,
    N_("Physically transforms graph data to logarithmic scale."),
    "Yeti <yeti@gwyddion.net>",
    "1.0",
    "David Nečas (Yeti)",
    "2016",
};

GWY_MODULE_QUERY2(module_info, graph_logscale)

static gboolean
module_register(void)
{
    gwy_graph_func_register("graph_logscale",
                            (GwyGraphFunc)&logscale,
                            N_("/_Logscale Transform..."),
                            NULL,
                            GWY_MENU_FLAG_GRAPH,
                            N_("Transform graph axes to logarithmic scale"));

    return TRUE;
}

static void
logscale(GwyGraph *graph)
{
    GwyContainer *settings = gwy_app_settings_get();
    GwyContainer *container;
    GwyGraphModel *gmodel, *newgmodel;
    GwyGraphCurveModel *gcmodel;
    GwySIUnit *unit;
    LogscaleArgs args;
    gchar *label, *llabel;
    guint n, i;

    gwy_app_data_browser_get_current(GWY_APP_CONTAINER, &container, 0);
    load_args(settings, &args);
    if (logscale_dialogue(&args)) {
        gmodel = gwy_graph_get_model(graph);
        newgmodel = gwy_graph_model_new_alike(gmodel);

        if (args.axes & LOGSCALE_AXIS_X) {
            g_object_get(gmodel, "axis-label-bottom", &label, NULL);
            llabel = logscale_label(label, args.base);
            g_free(label);
            unit = gwy_si_unit_new(NULL);
            g_object_set(newgmodel,
                         "axis-label-bottom", llabel,
                         "si-unit-x", unit,
                         NULL);
            g_object_unref(unit);
            g_free(llabel);
        }
        if (args.axes & LOGSCALE_AXIS_Y) {
            g_object_get(gmodel, "axis-label-left", &label, NULL);
            llabel = logscale_label(label, args.base);
            g_free(label);
            unit = gwy_si_unit_new(NULL);
            g_object_set(newgmodel,
                         "axis-label-left", llabel,
                         "si-unit-y", unit,
                         NULL);
            g_object_unref(unit);
            g_free(llabel);
        }

        n = gwy_graph_model_get_n_curves(gmodel);
        for (i = 0; i < n; i++) {
            if ((gcmodel = logscale_curve(gwy_graph_model_get_curve(gmodel, i),
                                                                    &args))) {
                gwy_graph_model_add_curve(newgmodel, gcmodel);
                g_object_unref(gcmodel);
            }
        }

        if (gwy_graph_model_get_n_curves(newgmodel))
            gwy_app_data_browser_add_graph_model(newgmodel, container, TRUE);
        g_object_unref(newgmodel);
    }
    save_args(settings, &args);
}

static gchar*
logscale_label(const gchar *label, gdouble base)
{
    if (fabs(base - G_E)/G_E < 1e-6)
        return g_strdup_printf("ln %s", label);
    if (fabs(base - 10.0)/10.0 < 1e-6)
        return g_strdup_printf("log %s", label);
    return g_strdup_printf("log<sub>%g</sub> %s", base, label);
}

static GwyGraphCurveModel*
logscale_curve(GwyGraphCurveModel *gcmodel, const LogscaleArgs *args)
{
    GwyGraphCurveModel *newgcmodel = gwy_graph_curve_model_new_alike(gcmodel);
    const gdouble *xdata, *ydata;
    gdouble *newxydata;
    guint ndata, i, n;
    gboolean logscale_x = (args->axes & LOGSCALE_AXIS_X);
    gboolean logscale_y = (args->axes & LOGSCALE_AXIS_Y);
    gboolean logbase = log(args->base);

    xdata = gwy_graph_curve_model_get_xdata(gcmodel);
    ydata = gwy_graph_curve_model_get_ydata(gcmodel);
    ndata = gwy_graph_curve_model_get_ndata(gcmodel);
    newxydata = g_new(gdouble, 2*ndata);
    for (i = n = 0; i < ndata; i++) {
        gdouble x = xdata[i], y = ydata[i];

        /* FIXME: We might want an option for negative abscissae too. */
        if (logscale_x && x <= 0.0)
            continue;
        if (logscale_y && y == 0.0)
            continue;
        if (logscale_y && y <= 0.0) {
            if (args->negative_y == LOGSCALE_NEGATIVE_SKIP)
                continue;
            y = fabs(y);
        }
        if (logscale_x)
            x = log(x)/logbase;
        if (logscale_y)
            y = log(y)/logbase;

        newxydata[n++] = x;
        newxydata[n++] = y;
    }

    if (n)
        gwy_graph_curve_model_set_data_interleaved(newgcmodel, newxydata, n/2);
    else
        GWY_OBJECT_UNREF(newgcmodel);

    g_free(newxydata);

    return newgcmodel;
}

static gboolean
logscale_dialogue(LogscaleArgs *args)
{
    LogscaleBaseType base_type = LOGSCALE_BASE_E;
    GtkWidget *dialogue, *table, *label;
    GSList *group;
    gint row;
    gboolean ok = FALSE;

    if (fabs(args->base - 10.0)/10.0 < 1e-6)
        base_type = LOGSCALE_BASE_10;

    dialogue = gtk_dialog_new_with_buttons(_("Logscale Transform"),
                                           NULL,
                                           GTK_DIALOG_DESTROY_WITH_PARENT,
                                           GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
                                           GTK_STOCK_OK, GTK_RESPONSE_OK,
                                           NULL);
    gwy_help_add_to_graph_dialog(GTK_DIALOG(dialogue), GWY_HELP_DEFAULT);
    gtk_dialog_set_default_response(GTK_DIALOG(dialogue), GTK_RESPONSE_OK);

    table = gtk_table_new(4+3+3, 3, FALSE);
    gtk_container_set_border_width(GTK_CONTAINER(table), 4);
    gtk_table_set_row_spacings(GTK_TABLE(table), 2);
    gtk_table_set_col_spacings(GTK_TABLE(table), 6);
    gtk_box_pack_start(GTK_BOX(GTK_DIALOG(dialogue)->vbox), table, TRUE, TRUE, 0);
    row = 0;

    label = gtk_label_new(_("Axes to transform:"));
    gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);
    gtk_table_attach(GTK_TABLE(table), label,
                     0, 3, row, row+1, GTK_FILL, GTK_FILL, 0, 0);
    row++;

    group = gwy_radio_buttons_createl(G_CALLBACK(axes_changed), args,
                                      args->axes,
                                      _("_X"), LOGSCALE_AXIS_X,
                                      _("_Y"), LOGSCALE_AXIS_Y,
                                      _("_Both"), LOGSCALE_AXIS_BOTH,
                                      NULL);
    row = gwy_radio_buttons_attach_to_table(group, GTK_TABLE(table), 3, row);

    gtk_table_set_row_spacing(GTK_TABLE(table), row-1, 8);
    label = gtk_label_new(_("Negative value handling:"));
    gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);
    gtk_table_attach(GTK_TABLE(table), label,
                     0, 3, row, row+1, GTK_FILL, GTK_FILL, 0, 0);
    row++;

    group = gwy_radio_buttons_createl(G_CALLBACK(negative_y_changed), args,
                                      args->negative_y,
                                      _("O_mit"), LOGSCALE_NEGATIVE_SKIP,
                                      _("_Take absolute value"),
                                      LOGSCALE_NEGATIVE_ABS,
                                      NULL);
    row = gwy_radio_buttons_attach_to_table(group, GTK_TABLE(table), 3, row);

    gtk_table_set_row_spacing(GTK_TABLE(table), row-1, 8);
    label = gtk_label_new(_("Base:"));
    gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);
    gtk_table_attach(GTK_TABLE(table), label,
                     0, 3, row, row+1, GTK_FILL, GTK_FILL, 0, 0);
    row++;

    group = gwy_radio_buttons_createl(G_CALLBACK(base_changed), args,
                                      base_type,
                                      _("Natural (e)"), LOGSCALE_BASE_E,
                                      _("10"), LOGSCALE_BASE_10,
                                      NULL);
    row = gwy_radio_buttons_attach_to_table(group, GTK_TABLE(table), 3, row);

    gtk_widget_show_all(dialogue);
    switch (gtk_dialog_run(GTK_DIALOG(dialogue))) {
        case GTK_RESPONSE_CANCEL:
        case GTK_RESPONSE_DELETE_EVENT:
        gtk_widget_destroy(dialogue);
        case GTK_RESPONSE_NONE:
        break;

        case GTK_RESPONSE_OK:
        gtk_widget_destroy(dialogue);
        ok = TRUE;
        break;

        default:
        g_assert_not_reached();
        break;
    }

    return ok;
}

static void
axes_changed(GtkToggleButton *toggle, LogscaleArgs *args)
{
    if (gtk_toggle_button_get_active(toggle))
        args->axes = gwy_radio_button_get_value(GTK_WIDGET(toggle));
}

static void
negative_y_changed(GtkToggleButton *toggle, LogscaleArgs *args)
{
    if (gtk_toggle_button_get_active(toggle))
        args->negative_y = gwy_radio_button_get_value(GTK_WIDGET(toggle));
}

static void
base_changed(GtkToggleButton *toggle, LogscaleArgs *args)
{
    LogscaleBaseType base_type;

    if (!gtk_toggle_button_get_active(toggle))
        return;

    base_type = gwy_radio_button_get_value(GTK_WIDGET(toggle));
    if (base_type == LOGSCALE_BASE_10)
        args->base = 10.0;
    else if (base_type == LOGSCALE_BASE_E)
        args->base = G_E;
    else {
        g_assert_not_reached();
    }
}

static const gchar axes_key[]       = "/module/graph_logscale/axes";
static const gchar base_key[]       = "/module/graph_logscale/base";
static const gchar negative_y_key[] = "/module/graph_logscale/negative_y";

static void
load_args(GwyContainer *settings, LogscaleArgs *args)
{
    *args = logscale_defaults;
    gwy_container_gis_enum_by_name(settings, axes_key, &args->axes);
    gwy_container_gis_double_by_name(settings, base_key, &args->base);
    gwy_container_gis_enum_by_name(settings, negative_y_key, &args->negative_y);

    args->axes = CLAMP(args->axes, LOGSCALE_AXIS_X, LOGSCALE_AXIS_BOTH);
    args->base = CLAMP(fabs(args->base), G_MINDOUBLE, G_MAXDOUBLE);
    args->negative_y = MIN(args->negative_y, LOGSCALE_NEGATIVE_ABS);
}

static void
save_args(GwyContainer *settings, LogscaleArgs *args)
{
    gwy_container_set_enum_by_name(settings, axes_key, args->axes);
    gwy_container_set_double_by_name(settings, base_key, args->base);
    gwy_container_set_enum_by_name(settings, negative_y_key, args->negative_y);
}

/* vim: set cin et ts=4 sw=4 cino=>1s,e0,n0,f0,{0,}0,^0,\:1s,=0,g1s,h0,t0,+1s,c3,(0,u0 : */
