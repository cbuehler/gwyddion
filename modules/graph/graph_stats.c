/*
 *  $Id$
 *  Copyright (C) 2016 David Necas (Yeti).
 *  E-mail: yeti@gwyddion.net.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA 02110-1301, USA.
 */

#include "config.h"
#include <stdlib.h>
#include <string.h>
#include <libgwyddion/gwymacros.h>
#include <libgwyddion/gwymath.h>
#include <libprocess/linestats.h>
#include <libgwydgets/gwygraphmodel.h>
#include <libgwydgets/gwycombobox.h>
#include <libgwydgets/gwynullstore.h>
#include <libgwydgets/gwystock.h>
#include <libgwydgets/gwydgetutils.h>
#include <libgwymodule/gwymodule-graph.h>
#include <app/gwymoduleutils.h>
#include <app/gwyapp.h>

typedef struct {
    const gchar *name;
    gsize offset;
} StatsLabel;

typedef struct {
    guint pos;
    guint npts;

    /* Valid from 1 points. */
    gdouble min;
    gdouble max;
    gdouble avg;
    gdouble median;
    /* Valid from 2 points. */
    gdouble ra;
    gdouble rms;
    gdouble skew;
    gdouble kurtosis;
    gdouble projlen;
    gdouble length;
    gdouble variation;
    gdouble integralavg;
    gdouble integral;
    gdouble integralp;
    gdouble integraln;
    gdouble integral2;
} StatsResults;

typedef struct {
    gint curve;
    gdouble from;
    gdouble to;
} StatsArgs;

typedef struct {
    GwyGraphModel *parent_gmodel;
    StatsArgs *args;
    StatsResults stats;
    GwySIValueFormat *xvf;
    GwySIValueFormat *yvf;
    GwySIValueFormat *areavf;
    GwySIValueFormat *lenvf;
    GwySIValueFormat *varvf;
    gulong recalculate_id;

    GtkWidget *dialogue;
    GtkWidget *graph;
    GtkWidget *curve;
    GtkWidget *from;
    GtkWidget *to;
    GtkWidget *npts;

    GtkWidget *min;
    GtkWidget *max;
    GtkWidget *avg;
    GtkWidget *median;
    GtkWidget *ra;
    GtkWidget *rms;
    GtkWidget *skew;
    GtkWidget *kurtosis;
    GtkWidget *projlen;
    GtkWidget *length;
    GtkWidget *variation;
    GtkWidget *integralavg;
    GtkWidget *integral;
    GtkWidget *integralp;
    GtkWidget *integraln;
    GtkWidget *integral2;
} StatsControls;

static gboolean   module_register       (void);
static void       graph_stats           (GwyGraph *graph);
static void       graph_stats_dialogue  (GwyGraphModel *gmodel,
                                         StatsArgs *args);
static GtkWidget* add_aux_button        (GtkWidget *hbox,
                                         const gchar *stock_id,
                                         const gchar *tooltip);
static void       curve_changed         (GtkComboBox *combo,
                                         StatsControls *controls);
static void       range_changed         (GtkWidget *entry,
                                         StatsControls *controls);
static void       stats_limit_selection (StatsControls *controls,
                                         gboolean curve_switch);
static void       stats_get_full_x_range(StatsControls *controls,
                                         gdouble *xmin,
                                         gdouble *xmax);
static void       graph_selected        (GwySelection* selection,
                                         gint i,
                                         StatsControls *controls);
static gboolean   update_stats_table    (gpointer user_data);
static void       invalidate            (StatsControls *controls);
static void       graph_stats_save      (StatsControls *controls);
static void       graph_stats_copy      (StatsControls *controls);
static gchar*     format_report         (StatsControls *controls);
static void       compute_stats         (GwyGraphCurveModel *gcmodel,
                                         StatsResults *stats,
                                         gdouble from,
                                         gdouble to);

/* There are no presistent settings. */
static const StatsArgs stats_defaults = {
    0, -G_MAXDOUBLE, G_MAXDOUBLE,
};

/* NB: The order of the values is important for create_report()! */

/* Simple values, disregarding abscissa */
static const StatsLabel values_simple[] = {
    { N_("Minimum:"),          G_STRUCT_OFFSET(StatsControls, min),         },
    { N_("Maximum:"),          G_STRUCT_OFFSET(StatsControls, max),         },
    { N_("Average value:"),    G_STRUCT_OFFSET(StatsControls, avg),         },
    { N_("Median:"),           G_STRUCT_OFFSET(StatsControls, median),      },
    { N_("Ra:"),               G_STRUCT_OFFSET(StatsControls, ra),          },
    { N_("Rms (Rq):"),         G_STRUCT_OFFSET(StatsControls, rms),         },
    { N_("Skew:"),             G_STRUCT_OFFSET(StatsControls, skew),        },
    { N_("Kurtosis:"),         G_STRUCT_OFFSET(StatsControls, kurtosis),    },
};

/* Integrals */
static const StatsLabel values_integral[] = {
    { N_("Projected length:"), G_STRUCT_OFFSET(StatsControls, projlen),     },
    { N_("Developed length:"), G_STRUCT_OFFSET(StatsControls, length),      },
    { N_("Variation:"),        G_STRUCT_OFFSET(StatsControls, variation),   },
    { N_("Average value:"),    G_STRUCT_OFFSET(StatsControls, integralavg), },
    { N_("Area under curve:"), G_STRUCT_OFFSET(StatsControls, integral),    },
    { N_("Positive area:"),    G_STRUCT_OFFSET(StatsControls, integralp),   },
    { N_("Negative area:"),    G_STRUCT_OFFSET(StatsControls, integraln),   },
    { N_("Root mean square:"), G_STRUCT_OFFSET(StatsControls, integral2),   },
};

static GwyModuleInfo module_info = {
    GWY_MODULE_ABI_VERSION,
    &module_register,
    N_("Calculates simple graph curve statistics."),
    "Yeti <yeti@gwyddion.net>",
    "1.0",
    "David Nečas (Yeti)",
    "2017",
};

GWY_MODULE_QUERY2(module_info, graph_stats)

static gboolean
module_register(void)
{
    gwy_graph_func_register("graph_stats",
                            (GwyGraphFunc)&graph_stats,
                            N_("/_Statistics..."),
                            NULL,
                            GWY_MENU_FLAG_GRAPH,
                            N_("Calculate graph curve statistics"));

    return TRUE;
}

static void
graph_stats(GwyGraph *graph)
{
    StatsArgs args = stats_defaults;

    graph_stats_dialogue(gwy_graph_get_model(graph), &args);
}

/* This seems to be the safe way of doing things.  Calling g_source_remove()
 * and gtk_widget_destroy() seems a race. */
static void
cancel_update(gpointer p, G_GNUC_UNUSED GObject *dialogue)
{
    gulong *sid = (gulong*)p;

    if (*sid) {
        g_source_remove(*sid);
        *sid = 0;
    }
}

static void
graph_stats_dialogue(GwyGraphModel *parent_gmodel, StatsArgs *args)
{
    GtkWidget *dialogue, *hbox, *table, *hbox2, *button, *label;
    GtkWidget **plabel;
    GwyGraphArea *area;
    GwyGraphModel *gmodel;
    GwySelection *selection;
    StatsControls controls;
    gint row;
    gdouble xrange, min, max;
    GwySIUnit *xunit;
    guint i;

    gwy_clear(&controls, 1);
    controls.args = args;
    controls.parent_gmodel = parent_gmodel;
    gmodel = gwy_graph_model_new_alike(parent_gmodel);

    g_object_get(parent_gmodel, "si-unit-x", &xunit, NULL);
    gwy_graph_model_get_x_range(parent_gmodel, &min, &max);
    xrange = MAX(fabs(max), fabs(min));
    controls.xvf
        = gwy_si_unit_get_format_with_digits(xunit, GWY_SI_UNIT_FORMAT_VFMARKUP,
                                             xrange, 3, NULL);
    g_object_unref(xunit);

    dialogue = gtk_dialog_new_with_buttons(_("Graph Statistics"),
                                           NULL, 0, NULL);
    controls.dialogue = dialogue;
    gtk_dialog_add_button(GTK_DIALOG(dialogue), GTK_STOCK_OK, GTK_RESPONSE_OK);
    gtk_dialog_add_button(GTK_DIALOG(dialogue), GTK_STOCK_CANCEL,
                          GTK_RESPONSE_CANCEL);
    gwy_help_add_to_graph_dialog(GTK_DIALOG(dialogue), GWY_HELP_DEFAULT);
    gtk_dialog_set_default_response(GTK_DIALOG(dialogue), GTK_RESPONSE_OK);

    hbox = gtk_hbox_new(FALSE, 2);
    gtk_box_pack_start(GTK_BOX(GTK_DIALOG(dialogue)->vbox), hbox,
                       TRUE, TRUE, 0);

    /* Parameters */
    table = gtk_table_new(6
                          + G_N_ELEMENTS(values_simple)
                          + G_N_ELEMENTS(values_integral),
                          2, FALSE);
    gtk_table_set_row_spacings(GTK_TABLE(table), 2);
    gtk_table_set_col_spacings(GTK_TABLE(table), 6);
    gtk_box_pack_start(GTK_BOX(hbox), table, FALSE, FALSE, 0);
    gtk_container_set_border_width(GTK_CONTAINER(table), 4);
    row = 0;

    controls.curve = gwy_combo_box_graph_curve_new(G_CALLBACK(curve_changed),
                                                   &controls,
                                                   parent_gmodel, args->curve);
    gwy_table_attach_adjbar(table, row, _("_Graph curve:"), NULL,
                            GTK_OBJECT(controls.curve),
                            GWY_HSCALE_WIDGET_NO_EXPAND);
    row++;

    /* Range */
    hbox2 = gtk_hbox_new(FALSE, 6);
    gtk_table_attach(GTK_TABLE(table), hbox2,
                     0, 2, row, row+1, GTK_FILL, 0, 0, 0);
    row++;

    label = gtk_label_new(_("Range:"));
    gtk_box_pack_start(GTK_BOX(hbox2), label, FALSE, FALSE, 0);

    controls.from = gtk_entry_new();
    g_object_set_data(G_OBJECT(controls.from), "id", (gpointer)"from");
    gtk_entry_set_width_chars(GTK_ENTRY(controls.from), 8);
    gtk_box_pack_start(GTK_BOX(hbox2), controls.from, FALSE, FALSE, 0);
    g_signal_connect(controls.from, "activate",
                     G_CALLBACK(range_changed), &controls);
    gwy_widget_set_activate_on_unfocus(controls.from, TRUE);

    label = gtk_label_new(gwy_sgettext("range|to"));
    gtk_box_pack_start(GTK_BOX(hbox2), label, FALSE, FALSE, 0);

    controls.to = gtk_entry_new();
    g_object_set_data(G_OBJECT(controls.to), "id", (gpointer)"to");
    gtk_entry_set_width_chars(GTK_ENTRY(controls.to), 8);
    gtk_box_pack_start(GTK_BOX(hbox2), controls.to, FALSE, FALSE, 0);
    g_signal_connect(controls.to, "activate",
                     G_CALLBACK(range_changed), &controls);
    gwy_widget_set_activate_on_unfocus(controls.to, TRUE);

    label = gtk_label_new(NULL);
    gtk_label_set_markup(GTK_LABEL(label), controls.xvf->units);
    gtk_box_pack_start(GTK_BOX(hbox2), label, FALSE, FALSE, 0);

    label = gtk_label_new(_("Number of points:"));
    gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);
    gtk_table_attach(GTK_TABLE(table), label, 0, 1, row, row+1,
                     GTK_EXPAND | GTK_FILL, 0, 0, 0);

    controls.npts = gtk_label_new(NULL);
    gtk_misc_set_alignment(GTK_MISC(controls.npts), 1.0, 0.5);
    gtk_label_set_selectable(GTK_LABEL(controls.npts), TRUE);
    gtk_table_attach(GTK_TABLE(table), controls.npts, 1, 2, row, row+1,
                     GTK_FILL, 0, 0, 0);
    row++;

    /* Results */
    gtk_table_set_row_spacing(GTK_TABLE(table), row-1, 8);
    gtk_table_attach(GTK_TABLE(table),
                     gwy_label_new_header(_("Simple Parameters")),
                     0, 2, row, row+1, GTK_FILL, 0, 0, 0);
    row++;

    for (i = 0; i < G_N_ELEMENTS(values_simple); i++) {
        label = gtk_label_new(_(values_simple[i].name));
        gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);
        gtk_table_attach(GTK_TABLE(table), label, 0, 1, row, row+1,
                         GTK_EXPAND | GTK_FILL, 0, 0, 0);

        plabel = (GtkWidget**)G_STRUCT_MEMBER_P(&controls,
                                                values_simple[i].offset);
        *plabel = gtk_label_new(NULL);
        gtk_misc_set_alignment(GTK_MISC(*plabel), 1.0, 0.5);
        gtk_label_set_selectable(GTK_LABEL(*plabel), TRUE);
        gtk_table_attach(GTK_TABLE(table), *plabel, 1, 2, row, row+1,
                         GTK_FILL, 0, 0, 0);

        row++;
    }

    gtk_table_set_row_spacing(GTK_TABLE(table), row-1, 8);
    gtk_table_attach(GTK_TABLE(table), gwy_label_new_header(_("Integrals")),
                     0, 2, row, row+1, GTK_FILL, 0, 0, 0);
    row++;

    for (i = 0; i < G_N_ELEMENTS(values_integral); i++) {
        label = gtk_label_new(_(values_integral[i].name));
        gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);
        gtk_table_attach(GTK_TABLE(table), label, 0, 1, row, row+1,
                         GTK_EXPAND | GTK_FILL, 0, 0, 0);

        plabel = (GtkWidget**)G_STRUCT_MEMBER_P(&controls,
                                                values_integral[i].offset);
        *plabel = gtk_label_new(NULL);
        gtk_misc_set_alignment(GTK_MISC(*plabel), 1.0, 0.5);
        gtk_label_set_selectable(GTK_LABEL(*plabel), TRUE);
        gtk_table_attach(GTK_TABLE(table), *plabel, 1, 2, row, row+1,
                         GTK_FILL, 0, 0, 0);

        row++;
    }

    hbox2 = gtk_hbox_new(FALSE, 0);
    gtk_table_attach(GTK_TABLE(table), hbox2,
                     0, 2, row, row+1, GTK_FILL, 0, 0, 0);

    button = add_aux_button(hbox2, GTK_STOCK_SAVE, _("Save table to a file"));
    g_signal_connect_swapped(button, "clicked",
                             G_CALLBACK(graph_stats_save), &controls);

    button = add_aux_button(hbox2, GTK_STOCK_COPY, _("Copy table to clipboard"));
    g_signal_connect_swapped(button, "clicked",
                             G_CALLBACK(graph_stats_copy), &controls);
    row++;

    /* Graph */
    controls.graph = gwy_graph_new(gmodel);
    g_object_unref(gmodel);
    gtk_widget_set_size_request(controls.graph, 400, 300);

    gwy_graph_enable_user_input(GWY_GRAPH(controls.graph), FALSE);
    gtk_box_pack_start(GTK_BOX(hbox), controls.graph, TRUE, TRUE, 0);
    gwy_graph_set_status(GWY_GRAPH(controls.graph), GWY_GRAPH_STATUS_XSEL);

    area = GWY_GRAPH_AREA(gwy_graph_get_area(GWY_GRAPH(controls.graph)));
    selection = gwy_graph_area_get_selection(area, GWY_GRAPH_STATUS_XSEL);
    gwy_selection_set_max_objects(selection, 1);
    g_signal_connect(selection, "changed",
                     G_CALLBACK(graph_selected), &controls);

    curve_changed(GTK_COMBO_BOX(controls.curve), &controls);
    graph_selected(selection, -1, &controls);

    g_object_weak_ref(G_OBJECT(dialogue), cancel_update,
                      &controls.recalculate_id);
    gtk_widget_show_all(dialogue);
    gtk_dialog_run(GTK_DIALOG(dialogue));
    gtk_widget_destroy(dialogue);

    gwy_si_unit_value_format_free(controls.xvf);
    if (controls.yvf)
        gwy_si_unit_value_format_free(controls.yvf);
    if (controls.areavf)
        gwy_si_unit_value_format_free(controls.areavf);
    if (controls.lenvf)
        gwy_si_unit_value_format_free(controls.lenvf);
    if (controls.varvf)
        gwy_si_unit_value_format_free(controls.varvf);
}

static GtkWidget*
add_aux_button(GtkWidget *hbox,
               const gchar *stock_id,
               const gchar *tooltip)
{
    GtkTooltips *tips;
    GtkWidget *button;

    tips = gwy_app_get_tooltips();
    button = gtk_button_new();
    gtk_button_set_relief(GTK_BUTTON(button), GTK_RELIEF_NONE);
    gtk_tooltips_set_tip(tips, button, tooltip, NULL);
    gtk_container_add(GTK_CONTAINER(button),
                      gtk_image_new_from_stock(stock_id,
                                               GTK_ICON_SIZE_SMALL_TOOLBAR));
    gtk_box_pack_end(GTK_BOX(hbox), button, FALSE, FALSE, 0);

    return button;
}

static void
curve_changed(GtkComboBox *combo, StatsControls *controls)
{
    StatsArgs *args = controls->args;
    GwyGraphModel *gmodel;
    GwyGraphCurveModel *gcmodel;

    args->curve = gwy_enum_combo_box_get_active(combo);
    gmodel = gwy_graph_get_model(GWY_GRAPH(controls->graph));
    gwy_graph_model_remove_all_curves(gmodel);
    gcmodel = gwy_graph_model_get_curve(controls->parent_gmodel, args->curve);
    if (gwy_graph_curve_model_is_ordered(gcmodel))
        gwy_graph_model_add_curve(gmodel, gcmodel);
    else {
        gcmodel = gwy_graph_curve_model_duplicate(gcmodel);
        gwy_graph_curve_model_enforce_order(gcmodel);
        gwy_graph_model_add_curve(gmodel, gcmodel);
        g_object_unref(gcmodel);
    }
    invalidate(controls);
}

static void
range_changed(GtkWidget *entry, StatsControls *controls)
{
    const gchar *id;
    gdouble *x, newval;

    id = g_object_get_data(G_OBJECT(entry), "id");
    if (gwy_strequal(id, "from"))
        x = &controls->args->from;
    else
        x = &controls->args->to;

    newval = atof(gtk_entry_get_text(GTK_ENTRY(entry)));
    newval *= controls->xvf->magnitude;
    if (newval == *x)
        return;
    *x = newval;
    stats_limit_selection(controls, FALSE);
}

static void
stats_limit_selection(StatsControls *controls, gboolean curve_switch)
{
    GwySelection *selection;
    GwyGraphArea *area;
    gdouble xmin, xmax;

    area = GWY_GRAPH_AREA(gwy_graph_get_area(GWY_GRAPH(controls->graph)));
    selection = gwy_graph_area_get_selection(area, GWY_GRAPH_STATUS_XSEL);

    if (curve_switch && !gwy_selection_get_data(selection, NULL)) {
        graph_selected(selection, -1, controls);
        return;
    }

    stats_get_full_x_range(controls, &xmin, &xmax);
    controls->args->from = CLAMP(controls->args->from, xmin, xmax);
    controls->args->to = CLAMP(controls->args->to, xmin, xmax);

    if (controls->args->from == xmin && controls->args->to == xmax)
        gwy_selection_clear(selection);
    else {
        gdouble range[2];

        range[0] = controls->args->from;
        range[1] = controls->args->to;
        gwy_selection_set_object(selection, 0, range);
    }
}

static void
stats_get_full_x_range(StatsControls *controls,
                       gdouble *xmin, gdouble *xmax)
{
    GwyGraphModel *gmodel;
    GwyGraphCurveModel *gcmodel;

    gmodel = gwy_graph_get_model(GWY_GRAPH(controls->graph));
    gcmodel = gwy_graph_model_get_curve(gmodel, 0);
    gwy_graph_curve_model_get_x_range(gcmodel, xmin, xmax);
}

static void
graph_selected(GwySelection* selection,
               gint i,
               StatsControls *controls)
{
    GwySIValueFormat *xvf = controls->xvf;
    StatsArgs *args;
    gchar buffer[24];
    gdouble range[2];
    gint nselections;
    gdouble power10;

    g_return_if_fail(i <= 0);

    args = controls->args;
    nselections = gwy_selection_get_data(selection, NULL);
    gwy_selection_get_object(selection, 0, range);

    if (nselections <= 0 || range[0] == range[1])
        stats_get_full_x_range(controls, &args->from, &args->to);
    else {
        args->from = MIN(range[0], range[1]);
        args->to = MAX(range[0], range[1]);
    }
    power10 = pow10(xvf->precision);
    g_snprintf(buffer, sizeof(buffer), "%.*f",
               xvf->precision,
               floor(args->from*power10/xvf->magnitude)/power10);
    gtk_entry_set_text(GTK_ENTRY(controls->from), buffer);
    g_snprintf(buffer, sizeof(buffer), "%.*f",
               xvf->precision,
               ceil(args->to*power10/xvf->magnitude)/power10);
    gtk_entry_set_text(GTK_ENTRY(controls->to), buffer);

    invalidate(controls);
}

static void
invalidate(StatsControls *controls)
{
    if (controls->recalculate_id)
        return;

    controls->recalculate_id = g_idle_add(update_stats_table, controls);
}

static void
update_label(GwySIValueFormat *units,
             GtkWidget *label,
             gdouble value)
{
    static gchar buffer[64];

    g_return_if_fail(units);
    g_return_if_fail(GTK_IS_LABEL(label));

    g_snprintf(buffer, sizeof(buffer), "%.*f%s%s",
               units->precision, value/units->magnitude,
               *units->units ? " " : "", units->units);
    gtk_label_set_markup(GTK_LABEL(label), buffer);
}

static gboolean
update_stats_table(gpointer user_data)
{
    StatsControls *controls = (StatsControls*)user_data;
    StatsArgs *args = controls->args;
    StatsResults *stats = &controls->stats;
    GwyGraphModel *gmodel;
    GwyGraphCurveModel *gcmodel;
    GwySIUnit *xunit = NULL, *yunit = NULL, *areaunit = NULL;
    gboolean same_units;
    gchar buffer[64];

    gmodel = gwy_graph_get_model(GWY_GRAPH(controls->graph));
    gcmodel = gwy_graph_model_get_curve(gmodel, 0);
    compute_stats(gcmodel, &controls->stats, args->from, args->to);

    g_snprintf(buffer, sizeof(buffer), "%u", stats->npts);
    gtk_label_set_text(GTK_LABEL(controls->npts), buffer);

    if (stats->npts < 2) {
        gtk_label_set_text(GTK_LABEL(controls->ra), NULL);
        gtk_label_set_text(GTK_LABEL(controls->rms), NULL);
        gtk_label_set_text(GTK_LABEL(controls->skew), NULL);
        gtk_label_set_text(GTK_LABEL(controls->kurtosis), NULL);
        gtk_label_set_text(GTK_LABEL(controls->projlen), NULL);
        gtk_label_set_text(GTK_LABEL(controls->length), NULL);
        gtk_label_set_text(GTK_LABEL(controls->variation), NULL);
        gtk_label_set_text(GTK_LABEL(controls->integralavg), NULL);
        gtk_label_set_text(GTK_LABEL(controls->integral), NULL);
        gtk_label_set_text(GTK_LABEL(controls->integralp), NULL);
        gtk_label_set_text(GTK_LABEL(controls->integraln), NULL);
        gtk_label_set_text(GTK_LABEL(controls->integral2), NULL);
    }

    if (stats->npts < 1) {
        gtk_label_set_text(GTK_LABEL(controls->min), NULL);
        gtk_label_set_text(GTK_LABEL(controls->max), NULL);
        gtk_label_set_text(GTK_LABEL(controls->avg), NULL);
        gtk_label_set_text(GTK_LABEL(controls->median), NULL);
        goto finish;
    }

    g_object_get(gmodel,
                 "si-unit-x", &xunit,
                 "si-unit-y", &yunit,
                 NULL);
    same_units = gwy_si_unit_equal(xunit, yunit);
    areaunit = gwy_si_unit_multiply(xunit, yunit, NULL);

    controls->yvf
        = gwy_si_unit_get_format_with_digits(yunit, GWY_SI_UNIT_FORMAT_VFMARKUP,
                                             MAX(fabs(stats->min),
                                                 fabs(stats->max)),
                                             3, controls->yvf);
    controls->areavf
        = gwy_si_unit_get_format_with_digits(areaunit,
                                             GWY_SI_UNIT_FORMAT_VFMARKUP,
                                             MAX(fabs(stats->integralp),
                                                 fabs(stats->integraln)),
                                             3, controls->areavf);
    controls->lenvf
        = gwy_si_unit_get_format_with_digits(xunit,
                                             GWY_SI_UNIT_FORMAT_VFMARKUP,
                                             stats->length, 3, controls->lenvf);
    controls->varvf
        = gwy_si_unit_get_format_with_digits(xunit,
                                             GWY_SI_UNIT_FORMAT_VFMARKUP,
                                             stats->variation,
                                             3, controls->varvf);

    update_label(controls->yvf, controls->min, stats->min);
    update_label(controls->yvf, controls->max, stats->max);
    update_label(controls->yvf, controls->avg, stats->avg);
    update_label(controls->yvf, controls->median, stats->median);
    update_label(controls->yvf, controls->rms, stats->rms);
    update_label(controls->yvf, controls->ra, stats->ra);

    g_snprintf(buffer, sizeof(buffer), "%.3g", stats->skew);
    gtk_label_set_text(GTK_LABEL(controls->skew), buffer);
    g_snprintf(buffer, sizeof(buffer), "%.3g", stats->kurtosis);
    gtk_label_set_text(GTK_LABEL(controls->kurtosis), buffer);

    update_label(controls->xvf, controls->projlen, stats->projlen);
    if (same_units)
        update_label(controls->lenvf, controls->length, stats->length);
    else
        gtk_label_set_text(GTK_LABEL(controls->length), _("N.A."));

    update_label(controls->varvf, controls->variation, stats->variation);
    update_label(controls->yvf, controls->integralavg, stats->integralavg);
    update_label(controls->areavf, controls->integral, stats->integral);
    update_label(controls->areavf, controls->integralp, stats->integralp);
    update_label(controls->areavf, controls->integraln, stats->integraln);
    update_label(controls->yvf, controls->integral2, stats->integral2);

finish:
    GWY_OBJECT_UNREF(areaunit);
    GWY_OBJECT_UNREF(yunit);
    GWY_OBJECT_UNREF(xunit);

    controls->recalculate_id = 0;
    return FALSE;
}

static void
graph_stats_save(StatsControls *controls)
{
    gchar *text = format_report(controls);

    gwy_save_auxiliary_data(_("Save Curve Statistics"),
                            GTK_WINDOW(controls->dialogue),
                            -1, text);
    g_free(text);
}

static void
graph_stats_copy(StatsControls *controls)
{
    GtkClipboard *clipboard;
    GdkDisplay *display;
    gchar *text = format_report(controls);

    display = gtk_widget_get_display(controls->dialogue);
    clipboard = gtk_clipboard_get_for_display(display, GDK_SELECTION_CLIPBOARD);
    gtk_clipboard_set_text(clipboard, text, -1);
    g_free(text);
}

static void
append_report_line_label(GString *report, const gchar *label, guint width)
{
    guint len = g_utf8_strlen(label, -1);

    g_string_append(report, label);
    while (len++ < width)
        g_string_append_c(report, ' ');
}

static void
append_report_line_vf(GString *report, const gchar *label, gdouble value,
                      GwySIValueFormat *vf, guint width)
{
    append_report_line_label(report, label, width);
    g_string_append_printf(report, "%.*f%s%s\n",
                           vf->precision + 1, value/vf->magnitude,
                           *vf->units ? " " : "", vf->units);
}

static void
append_report_line_plain(GString *report, const gchar *label, gdouble value,
                         const gchar *format, guint width)
{
    append_report_line_label(report, label, width);
    g_string_append_printf(report, format, value);
    g_string_append_c(report, '\n');
}

static gchar*
format_report(StatsControls *controls)
{
    StatsResults *stats = &controls->stats;
    StatsArgs *args = controls->args;
    const StatsLabel *labels;
    GString *report = g_string_new(NULL);
    GwySIValueFormat *xvf, *yvf, *areavf, *lenvf, *varvf;
    GwySIUnit *xunit = NULL, *yunit = NULL, *areaunit = NULL;
    GwyGraphModel *gmodel = gwy_graph_get_model(GWY_GRAPH(controls->graph));
    GwyGraphCurveModel *gcmodel;
    gdouble xrange, min, max;
    gboolean same_units;
    guint maxw, i;

    if (controls->recalculate_id) {
        gcmodel = gwy_graph_model_get_curve(gmodel, 0);
        g_source_remove(controls->recalculate_id);
        controls->recalculate_id = 0;
        compute_stats(gcmodel, &controls->stats, args->from, args->to);
    }

    maxw = g_utf8_strlen(_("Number of points:"), -1);
    labels = values_simple;
    for (i = 0; i < G_N_ELEMENTS(values_simple); i++) {
        guint len = g_utf8_strlen(_(labels[i].name), -1);
        maxw = MAX(maxw, len);
    }
    labels = values_integral;
    for (i = 0; i < G_N_ELEMENTS(values_integral); i++) {
        guint len = g_utf8_strlen(_(labels[i].name), -1);
        maxw = MAX(maxw, len);
    }
    maxw++;

    g_object_get(gmodel,
                 "si-unit-x", &xunit,
                 "si-unit-y", &yunit,
                 NULL);
    same_units = gwy_si_unit_equal(xunit, yunit);
    areaunit = gwy_si_unit_multiply(xunit, yunit, NULL);

    gwy_graph_model_get_x_range(gmodel, &min, &max);
    xrange = MAX(fabs(max), fabs(min));
    xvf = gwy_si_unit_get_format_with_digits(xunit, GWY_SI_UNIT_FORMAT_PLAIN,
                                             xrange, 3, NULL);
    yvf = gwy_si_unit_get_format_with_digits(yunit, GWY_SI_UNIT_FORMAT_PLAIN,
                                             MAX(fabs(stats->min),
                                                 fabs(stats->max)), 3, NULL);
    areavf = gwy_si_unit_get_format_with_digits(areaunit,
                                                GWY_SI_UNIT_FORMAT_PLAIN,
                                                MAX(fabs(stats->integralp),
                                                    fabs(stats->integraln)),
                                                3, NULL);
    lenvf = gwy_si_unit_get_format_with_digits(xunit, GWY_SI_UNIT_FORMAT_PLAIN,
                                               stats->length, 3, NULL);
    varvf = gwy_si_unit_get_format_with_digits(xunit, GWY_SI_UNIT_FORMAT_PLAIN,
                                               stats->variation, 3, NULL);

    append_report_line_label(report, _("Range:"), maxw);
    g_string_append_printf(report, "%.*f %s %.*f %s\n",
                           xvf->precision, args->from/xvf->magnitude,
                           gwy_sgettext("range|to"),
                           xvf->precision, args->to/xvf->magnitude,
                           xvf->units);

    append_report_line_plain(report, _("Number of points:"), stats->npts,
                             "%.0f", maxw);
    g_string_append_c(report, '\n');

    if (stats->npts < 1)
        goto finish;

    append_report_line_label(report, _("Simple Parameters"), maxw);
    g_string_append_c(report, '\n');

    labels = values_simple;
    append_report_line_vf(report, _(labels[0].name), stats->min, yvf, maxw);
    append_report_line_vf(report, _(labels[1].name), stats->max, yvf, maxw);
    append_report_line_vf(report, _(labels[2].name), stats->avg, yvf, maxw);
    append_report_line_vf(report, _(labels[3].name), stats->median, yvf, maxw);
    if (stats->npts < 2)
        goto finish;

    append_report_line_vf(report, _(labels[4].name), stats->ra, yvf, maxw);
    append_report_line_vf(report, _(labels[5].name), stats->rms, yvf, maxw);
    append_report_line_plain(report, _(labels[6].name),
                             stats->skew, "%.4g", maxw);
    append_report_line_plain(report, _(labels[7].name),
                             stats->kurtosis, "%.4g", maxw);
    g_string_append_c(report, '\n');

    append_report_line_label(report, _("Integrals"), maxw);
    g_string_append_c(report, '\n');

    labels = values_integral;
    append_report_line_vf(report, _(labels[0].name), stats->projlen, xvf, maxw);
    if (same_units) {
        append_report_line_vf(report, _(labels[1].name), stats->length,
                              lenvf, maxw);
    }
    append_report_line_vf(report, _(labels[2].name), stats->variation,
                          varvf, maxw);
    append_report_line_vf(report, _(labels[3].name), stats->integralavg,
                          yvf, maxw);
    append_report_line_vf(report, _(labels[4].name), stats->integral,
                          areavf, maxw);
    append_report_line_vf(report, _(labels[5].name), stats->integralp,
                          areavf, maxw);
    append_report_line_vf(report, _(labels[6].name), stats->integraln,
                          areavf, maxw);
    append_report_line_vf(report, _(labels[7].name), stats->integral2,
                          controls->yvf, maxw);

finish:
    g_object_unref(xunit);
    g_object_unref(yunit);
    g_object_unref(areaunit);
    gwy_si_unit_value_format_free(xvf);
    gwy_si_unit_value_format_free(yvf);
    gwy_si_unit_value_format_free(areavf);
    gwy_si_unit_value_format_free(lenvf);
    gwy_si_unit_value_format_free(varvf);

    return g_string_free(report, FALSE);
}

static void
compute_stats(GwyGraphCurveModel *gcmodel,
              StatsResults *stats,
              gdouble from, gdouble to)
{
    GwyDataLine *dline;
    const gdouble *xdata, *ydata;
    guint ndata, i, pos, len;

    gwy_clear(stats, 1);
    xdata = gwy_graph_curve_model_get_xdata(gcmodel);
    ydata = gwy_graph_curve_model_get_ydata(gcmodel);
    ndata = gwy_graph_curve_model_get_ndata(gcmodel);

    for (pos = 0; pos < ndata && xdata[pos] < from; pos++)
        pos++;
    for (len = ndata; len && xdata[len-1] > to; len--)
        len--;

    if (len <= pos)
        return;

    len -= pos;
    stats->pos = pos;
    stats->npts = len;

    /* Calculate simple quantities only depending on the value distribution
     * using DataLine methods. */
    dline = gwy_data_line_new(len, 1.0, FALSE);
    gwy_assign(gwy_data_line_get_data(dline), ydata + pos, len);
    gwy_data_line_get_min_max(dline, &stats->min, &stats->max);
    stats->avg = gwy_data_line_get_avg(dline);
    stats->median = gwy_data_line_get_median(dline);
    if (len > 1) {
        stats->rms = gwy_data_line_get_rms(dline);
        stats->ra = gwy_data_line_get_ra(dline);
        stats->skew = gwy_data_line_get_skew(dline);
        stats->kurtosis = gwy_data_line_get_kurtosis(dline);
    }
    g_object_unref(dline);
    if (len < 2)
        return;

    stats->projlen = xdata[pos + len-1] - xdata[pos];
    for (i = 0; i < len-1; i++) {
        gdouble y1 = ydata[pos + i], y2 = ydata[pos + i+1];
        gdouble dx = xdata[pos + i+1] - xdata[pos + i];
        gdouble x, dpos = 0.0, dneg = 0.0, d2 = 0.0;

        stats->length += sqrt((y2 - y1)*(y2 - y1) + dx*dx);
        stats->variation += fabs(y2 - y1);
        if (dx <= 0.0)
            continue;

        if (y1 >= 0.0 && y2 >= 0.0) {
            dpos = (y1 + y2)*dx;
            d2 = (y1*y1 + y2*y2)*dx;
        }
        else if (y1 <= 0.0 && y2 <= 0.0) {
            dneg = (y1 + y2)*dx;
            d2 = (y1*y1 + y2*y2)*dx;
        }
        else if (y1 > 0.0 && y2 < 0.0) {
            x = y1/(y1 - y2)*dx;
            dpos = y1*x;
            dneg = y2*(dx - x);
            d2 = (y1*y1*x + y2*y2*(dx - x));
        }
        else if (y1 < 0.0 && y2 > 0.0) {
            x = y2/(y2 - y1)*dx;
            dpos = y2*x;
            dneg = y1*(dx - x);
            d2 = (y1*y1*(dx - x) + y2*y2*x);
        }
        else {
            g_warning("Impossible curve value signs.");
            continue;
        }
        stats->integralp += dpos;
        stats->integraln += dneg;
        stats->integral += dpos + dneg;
        stats->integral2 += d2;
    }

    stats->integralp *= 0.5;
    stats->integraln *= 0.5;
    stats->integral *= 0.5;
    stats->integral2 *= 0.5;
    stats->integralavg = stats->integral/stats->projlen;
    stats->integral2 = sqrt(stats->integral2/stats->projlen);
}

/* vim: set cin et ts=4 sw=4 cino=>1s,e0,n0,f0,{0,}0,^0,\:1s,=0,g1s,h0,t0,+1s,c3,(0,u0 : */
