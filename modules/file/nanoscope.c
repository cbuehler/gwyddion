/*
 *  $Id$
 *  Copyright (C) 2004-2018 David Necas (Yeti), Petr Klapetek.
 *  E-mail: yeti@gwyddion.net, klapetek@gwyddion.net.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA 02110-1301, USA.
 */
#define DEBUG 1
/**
 * [FILE-MAGIC-FREEDESKTOP]
 * <mime-type type="application/x-nanoscope-iii-spm">
 *   <comment>Nanoscope III SPM data</comment>
 *   <magic priority="80">
 *     <match type="string" offset="0" value="\\*File list\r\n"/>
 *     <match type="string" offset="0" value="\\*EC File list\r\n"/>
 *     <match type="string" offset="0" value="?*File list\r\n"/>
 *   </magic>
 * </mime-type>
 **/

/**
 * [FILE-MAGIC-FILEMAGIC]
 * # Nanoscope III
 * # Two header variants.
 * 0 string \\*File\ list\x0d\x0a Nanoscope III SPM binary data
 * 0 string \\*EC\ File\ list\x0d\x0a Nanoscope III electrochemistry SPM binary data
 * 0 string ?*File\ list\x0d\x0a Nanoscope III SPM text data
 **/

/**
 * [FILE-MAGIC-USERGUIDE]
 * Veeco Nanoscope III
 * .001 .002 etc.
 * Read SPS:Limited[1] Volume
 * [1] Spectra curves are imported as graphs, positional information is lost.
 **/

#include "config.h"
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <libgwyddion/gwymacros.h>
#include <libgwyddion/gwyutils.h>
#include <libgwyddion/gwymath.h>
#include <libprocess/stats.h>
#include <libprocess/brick.h>
#include <libprocess/arithmetic.h>
#include <libgwydgets/gwygraphmodel.h>
#include <libgwymodule/gwymodule-file.h>
#include <app/wait.h>
#include <app/gwymoduleutils-file.h>
#include <app/data-browser.h>

#include "err.h"

#define MAGIC_BIN "\\*File list\r\n"
#define MAGIC_TXT "?*File list\r\n"
#define MAGIC_SIZE (sizeof(MAGIC_TXT)-1)

#define MAGIC_BIN_PARTIAL "\\*File list"
#define MAGIC_TXT_PARTIAL "?*File list"
#define MAGIC_SIZE_PARTIAL (sizeof(MAGIC_TXT_PARTIAL)-1)

#define MAGIC_FORCE_BIN "\\*Force file list\r\n"
#define MAGIC_FORCE_SIZE (sizeof(MAGIC_FORCE_BIN)-1)

#define MAGIC_EC_BIN "\\*EC File list\r\n"
#define MAGIC_EC_SIZE (sizeof(MAGIC_EC_BIN)-1)

typedef enum {
    NANOSCOPE_FILE_TYPE_NONE           = 0,
    NANOSCOPE_FILE_TYPE_BIN            = 1,
    NANOSCOPE_FILE_TYPE_BIN32          = 2,
    NANOSCOPE_FILE_TYPE_TXT            = 3,
    NANOSCOPE_FILE_TYPE_FORCE_BIN      = 4,
    NANOSCOPE_FILE_TYPE_FORCE_BIN32    = 5,
    NANOSCOPE_FILE_TYPE_FORCE_VOLUME   = 6,
    NANOSCOPE_FILE_TYPE_FORCE_VOLUME32 = 7,
    NANOSCOPE_FILE_TYPE_BROKEN         = 8
} NanoscopeFileType;

typedef enum {
    NANOSCOPE_VALUE_OLD = 0,
    NANOSCOPE_VALUE_VALUE,
    NANOSCOPE_VALUE_SCALE,
    NANOSCOPE_VALUE_SELECT
} NanoscopeValueType;

typedef enum {
    NANOSCOPE_SPECTRA_IV,
    NANOSCOPE_SPECTRA_FZ,
} NanoscopeSpectraType;

typedef enum {
    NANOSCOPE_BRICK_UNKNOWN  = 0,
    NANOSCOPE_BRICK_APPROACH = 1,
    NANOSCOPE_BRICK_RETRACT  = 2,
} NanoscopeBrickDirection;

/*
 * Old-style record is
 * \Foo: HardValue (HardScale)
 * where HardScale is optional.
 *
 * New-style record is
 * \@Bar: V [SoftScale] (HardScale) HardValue
 * where SoftScale and HardScale are optional.
 */
typedef struct {
    NanoscopeValueType type;
    const gchar *soft_scale;
    gdouble hard_scale;
    const gchar *hard_scale_units;
    gdouble hard_value;
    const gchar *hard_value_str;
    const gchar *hard_value_units;
} NanoscopeValue;

typedef struct {
    GHashTable *hash;
    GwyDataField *dfield;
    GwyGraphModel *graph_model;
    GwyBrick *brick;
    GwyBrick *brick2;
    GwyDataField *min_field;
    GwyDataField *min_field2;
    GwyDataField *range_field;
    GwyDataField *range_field2;
} NanoscopeData;

static gboolean        module_register         (void);
static gint            nanoscope_detect        (const GwyFileDetectInfo *fileinfo,
                                                gboolean only_name);
static GwyContainer*   nanoscope_load          (const gchar *filename,
                                                GwyRunType mode,
                                                GError **error);
static void            create_aux_datafield    (GwyContainer *container,
                                                GwyDataField *dfield,
                                                NanoscopeBrickDirection dir,
                                                const gchar *auxname,
                                                const gchar *name,
                                                const gchar *filename,
                                                gint *i);
static const gchar*    brick_dir_name          (NanoscopeBrickDirection dir);
static gchar*          extract_header          (const guchar *buffer,
                                                gsize size,
                                                GError **error);
static GwyDataField*   hash_to_data_field      (GHashTable *hash,
                                                GHashTable *scannerlist,
                                                GHashTable *scanlist,
                                                GHashTable *contrlist,
                                                NanoscopeFileType file_type,
                                                gsize bufsize,
                                                const guchar *buffer,
                                                gsize gxres,
                                                gsize gyres,
                                                gboolean gnonsquare_aspect,
                                                gchar **p,
                                                GError **error);
static GwyGraphModel*  hash_to_curve           (GHashTable *hash,
                                                GHashTable *forcelist,
                                                GHashTable *scanlist,
                                                GHashTable *scannerlist,
                                                NanoscopeFileType file_type,
                                                gsize bufsize,
                                                const guchar *buffer,
                                                gint gxres,
                                                GError **error);
static GwyBrick*       hash_to_brick           (GHashTable *hash,
                                                GHashTable *forcelist,
                                                GHashTable *scanlist,
                                                GHashTable *scannerlist,
                                                GHashTable *equipmentlist,
                                                NanoscopeFileType file_type,
                                                gsize bufsize,
                                                const guchar *buffer,
                                                GwyBrick **second_brick,
                                                GError **error);
static gboolean        read_text_data          (guint n,
                                                gdouble *data,
                                                gchar **buffer,
                                                gint bpp,
                                                GError **error);
static gboolean        read_binary_data        (gint n,
                                                gdouble *data,
                                                const guchar *buffer,
                                                gint bpp,
                                                gint qbpp,
                                                GError **error);
static GHashTable*     read_hash               (gchar **buffer,
                                                GError **error);
static const gchar*    get_image_data_name     (GHashTable *hash);
static void            get_scan_list_res       (GHashTable *hash,
                                                gsize *xres,
                                                gsize *yres);
static GwySIUnit*      get_scan_size           (GHashTable *hash,
                                                gdouble *xreal,
                                                gdouble *yreal,
                                                GError **error);
static gboolean        has_nonsquare_aspect    (GHashTable *hash);
static GwySIUnit*      get_physical_scale      (GHashTable *hash,
                                                GHashTable *scannerlist,
                                                GHashTable *scanlist,
                                                GHashTable *contrlist,
                                                gdouble *scale,
                                                GError **error);
static GwySIUnit*      get_spec_ordinate_scale (GHashTable *hash,
                                                GHashTable *scanlist,
                                                gdouble *scale,
                                                gboolean *convert_to_force,
                                                GError **error);
static GwySIUnit*      get_spec_abscissa_scale (GHashTable *hash,
                                                GHashTable *forcelist,
                                                GHashTable *scannerlist,
                                                GHashTable *scanlist,
                                                gdouble *xreal,
                                                gdouble *xoff,
                                                NanoscopeSpectraType *spectype,
                                                GError **error);
static GwyContainer*   nanoscope_get_metadata  (GHashTable *hash,
                                                GList *list);
static NanoscopeValue* parse_value             (const gchar *key,
                                                gchar *line);
static void            swap_brick_x_z          (GwyBrick *brick,
                                                GwyBrick *tmp,
                                                gboolean zinv);
static void            set_brick_properties    (GwyBrick *brick,
                                                GwySIUnit *unitxy,
                                                GwySIUnit *unitz,
                                                GwySIUnit *unitw,
                                                gdouble xreal,
                                                gdouble yreal,
                                                gdouble zreal,
                                                gdouble q);
static void            fix_sine_distortion     (GwyBrick *brick,
                                                gdouble *buf);
static GwyDataField*   preview_from_brick      (GwyBrick *brick);
static gint            rebase_force_volume_data(GList *list);
static void            rebase_one_brick        (GwyBrick *brick,
                                                GwyBrick *zbrick,
                                                GwyBrick *tmpbrick,
                                                GwyDataField **pmin_field,
                                                GwyDataField **prange_field);

static GwyModuleInfo module_info = {
    GWY_MODULE_ABI_VERSION,
    &module_register,
    N_("Imports Veeco (Digital Instruments) Nanoscope data files, "
       "version 3 or newer."),
    "Yeti <yeti@gwyddion.net>",
    "0.36",
    "David Nečas (Yeti) & Petr Klapetek",
    "2004",
};

GWY_MODULE_QUERY2(module_info, nanoscope)

static gboolean
module_register(void)
{
    gwy_file_func_register("nanoscope",
                           N_("Nanoscope III files"),
                           (GwyFileDetectFunc)&nanoscope_detect,
                           (GwyFileLoadFunc)&nanoscope_load,
                           NULL,
                           NULL);

    return TRUE;
}

static gint
nanoscope_detect(const GwyFileDetectInfo *fileinfo,
                 gboolean only_name)
{
    gint score = 0;

    if (only_name)
        return 0;

    if (fileinfo->buffer_len > MAGIC_SIZE
        && (!memcmp(fileinfo->head, MAGIC_TXT_PARTIAL, MAGIC_SIZE_PARTIAL)
            || !memcmp(fileinfo->head, MAGIC_BIN_PARTIAL, MAGIC_SIZE_PARTIAL)
            || !memcmp(fileinfo->head, MAGIC_FORCE_BIN, MAGIC_FORCE_SIZE)
            || !memcmp(fileinfo->head, MAGIC_EC_BIN, MAGIC_EC_SIZE)))
        score = 100;

    return score;
}

static GwyContainer*
nanoscope_load(const gchar *filename,
               GwyRunType mode,
               GError **error)
{
    GwyContainer *meta, *container = NULL;
    GError *err = NULL;
    guchar *buffer = NULL;
    gchar *header = NULL, *p, *s;
    const gchar *self, *name;
    gsize size = 0;
    NanoscopeFileType file_type, image_file_type;
    NanoscopeData *ndata;
    NanoscopeValue *val;
    NanoscopeBrickDirection bdir;
    GHashTable *hash, *scannerlist = NULL, *scanlist = NULL, *forcelist = NULL,
               *contrlist = NULL, *equipmentlist = NULL;
    GList *l, *list = NULL;
    gsize xres = 0, yres = 0;
    gint i, n;
    gboolean nonsquare_aspect = FALSE, ok, waiting = FALSE;
    GwyDataField *preview;
    GQuark quark;

    if (!gwy_file_get_contents(filename, &buffer, &size, &err)) {
        err_GET_FILE_CONTENTS(error, &err);
        return NULL;
    }
    file_type = NANOSCOPE_FILE_TYPE_NONE;
    if (size > MAGIC_SIZE) {
        if (!memcmp(buffer, MAGIC_TXT, MAGIC_SIZE))
            file_type = NANOSCOPE_FILE_TYPE_TXT;
        else if (!memcmp(buffer, MAGIC_BIN, MAGIC_SIZE)
                 || !memcmp(buffer, MAGIC_EC_BIN, MAGIC_EC_SIZE))
            file_type = NANOSCOPE_FILE_TYPE_BIN;
        else if (!memcmp(buffer, MAGIC_FORCE_BIN, MAGIC_FORCE_SIZE))
            file_type = NANOSCOPE_FILE_TYPE_FORCE_BIN;
        else if (!memcmp(buffer, MAGIC_TXT_PARTIAL, MAGIC_SIZE_PARTIAL)
                 || !memcmp(buffer, MAGIC_BIN_PARTIAL, MAGIC_SIZE_PARTIAL))
          file_type = NANOSCOPE_FILE_TYPE_BROKEN;
    }
    if (!file_type) {
        gwy_file_abandon_contents(buffer, size, NULL);
        g_set_error(error, GWY_MODULE_FILE_ERROR, GWY_MODULE_FILE_ERROR_DATA,
                    _("File is not a Nanoscope file, "
                      "or it is a unknown subtype."));
        return NULL;
    }
    if (file_type == NANOSCOPE_FILE_TYPE_BROKEN) {
        gwy_file_abandon_contents(buffer, size, NULL);
        g_set_error(error, GWY_MODULE_FILE_ERROR, GWY_MODULE_FILE_ERROR_DATA,
                    _("File has been damaged by change of line endings, "
                      "resulting in corruption of the binary part of the file."
                      "\n\n"
                      "Typically, this occurs if the file is treated as text "
                      "when sent by e-mail uncompressed, sent by FTP in ascii "
                      "mode (use binary), compressed by ‘Send to compressed "
                      "folder’ in some versions of MS Windows, or any other "
                      "file transfer that attempts to store text "
                      "platform-independently."));
        return NULL;
    }

    gwy_debug("File type: %d", file_type);
    if (!(header = extract_header(buffer, size, error))) {
        gwy_file_abandon_contents(buffer, size, NULL);
        return NULL;
    }

    /* as we already know file_type, fix the first char for hash reading */
    *header = '\\';

    p = header;
    while ((hash = read_hash(&p, &err))) {
        ndata = g_new0(NanoscopeData, 1);
        ndata->hash = hash;
        list = g_list_append(list, ndata);

        val = g_hash_table_lookup(hash, "Start context");
        if (val && gwy_strequal(val->hard_value_str, "FVOL"))
            file_type = NANOSCOPE_FILE_TYPE_FORCE_VOLUME;

        /* In version 9.2 all data magically became 32bit. */
        self = g_hash_table_lookup(hash, "#self");
        if (self
            && gwy_stramong(self, "File list", "Force file list", NULL)
            && (val = g_hash_table_lookup(hash, "Version"))) {
            gulong version = strtol(val->hard_value_str, NULL, 16);
            if (version >= 0x09200000) {
                gwy_debug("version is 0x%lx, assuming 32bit data", version);
                if (file_type == NANOSCOPE_FILE_TYPE_BIN)
                    file_type = NANOSCOPE_FILE_TYPE_BIN32;
                else if (file_type == NANOSCOPE_FILE_TYPE_FORCE_BIN)
                    file_type = NANOSCOPE_FILE_TYPE_FORCE_BIN32;
                else if (file_type == NANOSCOPE_FILE_TYPE_FORCE_VOLUME)
                    file_type = NANOSCOPE_FILE_TYPE_FORCE_VOLUME32;
            }
        }
    }

    image_file_type = file_type;
    if (file_type == NANOSCOPE_FILE_TYPE_FORCE_VOLUME32)
        image_file_type = NANOSCOPE_FILE_TYPE_BIN32;
    else if (file_type == NANOSCOPE_FILE_TYPE_FORCE_VOLUME)
        image_file_type = NANOSCOPE_FILE_TYPE_BIN;
    else if (file_type == NANOSCOPE_FILE_TYPE_FORCE_BIN
             || file_type == NANOSCOPE_FILE_TYPE_FORCE_BIN32)
        image_file_type = NANOSCOPE_FILE_TYPE_NONE;

    if (err) {
        g_propagate_error(error, err);
        ok = FALSE;
    }
    else
        ok = TRUE;

    n = 0;
    if (ok
        && mode == GWY_RUN_INTERACTIVE
        && (file_type == NANOSCOPE_FILE_TYPE_FORCE_VOLUME
            || file_type == NANOSCOPE_FILE_TYPE_FORCE_VOLUME32)) {
        gwy_app_wait_start(NULL, _("Reading channels..."));
        waiting = TRUE;
        for (l = list; l; l = g_list_next(l)) {
            ndata = (NanoscopeData*)l->data;
            hash = ndata->hash;
            self = g_hash_table_lookup(hash, "#self");
            if (gwy_stramong(self, "AFM image list", "Ciao image list",
                              "STM image list", "NCAFM image list",
                              "Ciao force image list", "Image list", NULL))
                n++;
        }
        gwy_app_wait_set_fraction(0.01);
    }

    i = 0;
    for (l = list; ok && l; l = g_list_next(l)) {
        ndata = (NanoscopeData*)l->data;
        hash = ndata->hash;
        self = g_hash_table_lookup(hash, "#self");
        /* The alternate names were found in files written by some beast
         * called Nanoscope E software */
        if (gwy_strequal(self, "Scanner list")
            || gwy_strequal(self, "Microscope list")) {
            scannerlist = hash;
            continue;
        }
        if (gwy_strequal(self, "Equipment list")) {
            equipmentlist = hash;
            continue;
        }
        if (gwy_strequal(self, "File list")) {
            continue;
        }
        if (gwy_strequal(self, "Controller list")) {
            contrlist = hash;
            continue;
        }
        if (gwy_stramong(self, "Ciao scan list", "Afm list", "Stm list",
                         "NC Afm list", NULL)) {
            get_scan_list_res(hash, &xres, &yres);
            nonsquare_aspect = has_nonsquare_aspect(hash);
            scanlist = hash;
        }
         if (gwy_stramong(self, "Ciao force list", NULL)) {
            get_scan_list_res(hash, &xres, &yres);
            nonsquare_aspect = has_nonsquare_aspect(hash);
            forcelist = hash;
        }
        if (!gwy_stramong(self, "AFM image list", "Ciao image list",
                          "STM image list", "NCAFM image list",
                          "Ciao force image list", "Image list", NULL))
            continue;

        gwy_debug("processing hash %s", self);
        if (file_type == NANOSCOPE_FILE_TYPE_FORCE_BIN
            || file_type == NANOSCOPE_FILE_TYPE_FORCE_BIN32) {
            ndata->graph_model = hash_to_curve(hash, forcelist, scanlist,
                                               scannerlist,
                                               file_type,
                                               size, buffer,
                                               xres,
                                               error);
            ok = ok && ndata->graph_model;
        }
        else if (file_type == NANOSCOPE_FILE_TYPE_FORCE_VOLUME
                 || file_type == NANOSCOPE_FILE_TYPE_FORCE_VOLUME32) {
            if (gwy_strequal(self, "Ciao force image list")) {
                ndata->brick = hash_to_brick(hash, forcelist, scanlist,
                                             scannerlist, equipmentlist,
                                             file_type,
                                             size, buffer,
                                             &ndata->brick2,
                                             error);
                ok = ok && ndata->brick;
            }
            else {
                ndata->dfield = hash_to_data_field(hash, scannerlist, scanlist,
                                                   contrlist,
                                                   image_file_type,
                                                   size, buffer,
                                                   xres, yres, nonsquare_aspect,
                                                   &p, error);
                ok = ok && ndata->dfield;
            }
        }
        else {
            ndata->dfield = hash_to_data_field(hash, scannerlist, scanlist,
                                               contrlist,
                                               file_type,
                                               size, buffer,
                                               xres, yres, nonsquare_aspect,
                                               &p, error);
            ok = ok && ndata->dfield;
        }

        if (waiting) {
            i++;
            if (!gwy_app_wait_set_fraction((gdouble)i/n)) {
                err_CANCELLED(error);
                ok = FALSE;
                break;
            }
        }
    }

    if (ok
        && (file_type == NANOSCOPE_FILE_TYPE_FORCE_VOLUME
            || file_type == NANOSCOPE_FILE_TYPE_FORCE_VOLUME32))
        rebase_force_volume_data(list);

    if (ok) {
        i = 0;
        container = gwy_container_new();
        for (l = list; l; l = g_list_next(l)) {
            ndata = (NanoscopeData*)l->data;
            if (ndata->dfield) {
                quark = gwy_app_get_data_key_for_id(i);
                gwy_container_set_object(container, quark, ndata->dfield);
                if ((name = get_image_data_name(ndata->hash))) {
                    quark = gwy_app_get_data_title_key_for_id(i);
                    gwy_container_set_const_string(container, quark, name);
                }

                meta = nanoscope_get_metadata(ndata->hash, list);
                quark = gwy_app_get_data_meta_key_for_id(i);
                gwy_container_set_object(container, quark, meta);
                g_object_unref(meta);

                gwy_app_channel_check_nonsquare(container, i);
                gwy_file_channel_import_log_add(container, i, NULL, filename);
                i++;
            }
            if (ndata->graph_model) {
                quark = gwy_app_get_graph_key_for_id(i+1);
                gwy_container_set_object(container, quark, ndata->graph_model);
                i++;
            }
            if (ndata->brick) {
                bdir = (ndata->brick2
                        ? NANOSCOPE_BRICK_APPROACH
                        : NANOSCOPE_BRICK_UNKNOWN);
                quark = gwy_app_get_brick_key_for_id(i);
                gwy_container_set_object(container, quark, ndata->brick);

                preview = preview_from_brick(ndata->brick);
                quark = gwy_app_get_brick_preview_key_for_id(i);
                gwy_container_set_object(container, quark, preview);
                g_object_unref(preview);

                if ((name = get_image_data_name(ndata->hash))) {
                    quark = gwy_app_get_brick_title_key_for_id(i);
                    s = g_strdup_printf("%s%s", name, brick_dir_name(bdir));
                    gwy_container_set_string(container, quark, s);
                }
                gwy_file_volume_import_log_add(container, i, NULL, filename);
                i++;

                create_aux_datafield(container, ndata->min_field, bdir,
                                     "Force curve start", name, filename, &i);
                create_aux_datafield(container, ndata->range_field, bdir,
                                     "Force curve length", name, filename, &i);
            }
            if (ndata->brick2) {
                bdir = NANOSCOPE_BRICK_RETRACT;
                quark = gwy_app_get_brick_key_for_id(i);
                gwy_container_set_object(container, quark, ndata->brick2);

                preview = preview_from_brick(ndata->brick2);
                quark = gwy_app_get_brick_preview_key_for_id(i);
                gwy_container_set_object(container, quark, preview);
                g_object_unref(preview);

                if ((name = get_image_data_name(ndata->hash))) {
                    quark = gwy_app_get_brick_title_key_for_id(i);
                    s = g_strdup_printf("%s%s", name, brick_dir_name(bdir));
                    gwy_container_set_string(container, quark, s);
                }
                gwy_file_volume_import_log_add(container, i, NULL, filename);
                i++;

                create_aux_datafield(container, ndata->min_field2, bdir,
                                     "Force curve start", name, filename, &i);
                create_aux_datafield(container, ndata->range_field2, bdir,
                                     "Force curve length", name, filename, &i);
            }
        }
        if (!i)
            GWY_OBJECT_UNREF(container);
    }

    if (waiting)
        gwy_app_wait_finish();

    for (l = list; l; l = g_list_next(l)) {
        ndata = (NanoscopeData*)l->data;
        GWY_OBJECT_UNREF(ndata->dfield);
        GWY_OBJECT_UNREF(ndata->graph_model);
        GWY_OBJECT_UNREF(ndata->brick);
        GWY_OBJECT_UNREF(ndata->brick2);
        GWY_OBJECT_UNREF(ndata->min_field);
        GWY_OBJECT_UNREF(ndata->min_field2);
        GWY_OBJECT_UNREF(ndata->range_field);
        GWY_OBJECT_UNREF(ndata->range_field2);
        if (ndata->hash)
            g_hash_table_destroy(ndata->hash);
        g_free(ndata);
    }
    gwy_file_abandon_contents(buffer, size, NULL);
    g_free(header);
    g_list_free(list);

    if (!container && ok)
        err_NO_DATA(error);

    return container;
}

static void
create_aux_datafield(GwyContainer *container,
                     GwyDataField *dfield,
                     NanoscopeBrickDirection dir,
                     const gchar *auxname,
                     const gchar *name,
                     const gchar *filename,
                     gint *i)
{
    gchar *title;
    GQuark key;

    if (!dfield)
        return;

    key = gwy_app_get_data_key_for_id(*i);
    gwy_container_set_object(container, key, dfield);

    key = gwy_app_get_data_title_key_for_id(*i);
    if (!name)
        name = "Unknown";

    title = g_strdup_printf("%s%s [%s]", auxname, brick_dir_name(dir), name);
    gwy_container_set_string(container, key, title);

    gwy_file_channel_import_log_add(container, *i, NULL, filename);
    (*i)++;
}

static const gchar*
brick_dir_name(NanoscopeBrickDirection dir)
{
    if (dir == NANOSCOPE_BRICK_APPROACH)
        return " Approach";
    if (dir == NANOSCOPE_BRICK_RETRACT)
        return " Retract";
    return "";
}

static gchar*
extract_header(const guchar *buffer, gsize size, GError **error)
{
    enum { DATA_LEN_LEN = sizeof("\\Data length: ")-1 };
    const guchar *p = buffer;
    guint i, len, header_len;
    gchar *header;

    if (size < 2) {
        err_MISSING_FIELD(error, "Data length");
        return NULL;
    }

    /* Find header size by looking for ‘Data length’ among the few first
     * fields.  All files, even historic ones, apparently carry it.  Actually
     * it is invariably the fifth field. */
    for (i = 0; i < 8; i++) {
        p = memchr(p+1, '\\', size-1 - (p - buffer));
        if (!p) {
            err_MISSING_FIELD(error, "Data length");
            return NULL;
        }
        if (p + DATA_LEN_LEN+1 - buffer >= size) {
            err_MISSING_FIELD(error, "Data length");
            return NULL;
        }
        if (memcmp(p, "\\Data length: ", DATA_LEN_LEN) == 0) {
            p += DATA_LEN_LEN;
            goto found_it;
        }
    }
    err_MISSING_FIELD(error, "Data length");
    return NULL;

found_it:
    len = size - (p - buffer);
    header_len = 0;
    for (i = 0; i < len; i++) {
        if (!g_ascii_isdigit(p[i]))
            break;
        header_len = 10*header_len + g_ascii_digit_value(p[i]);
    }

    if (header_len > size) {
        err_INVALID(error, "Data length");
        return NULL;
    }

    header = g_new(gchar, header_len+1);
    memcpy(header, buffer, header_len);
    header[header_len] = '\0';

    return header;
}

/* Seems correct also for volume data. */
static const gchar*
get_image_data_name(GHashTable *hash)
{
    NanoscopeValue *val;
    const gchar *name = NULL;

    if ((val = g_hash_table_lookup(hash, "@2:Image Data"))
        || (val = g_hash_table_lookup(hash, "@3:Image Data"))
        || (val = g_hash_table_lookup(hash, "@4:Image Data"))) {
        if (val->soft_scale)
            name = val->soft_scale;
        else if (val->hard_value_str)
            name = val->hard_value_str;
    }
    else if ((val = g_hash_table_lookup(hash, "Image data")))
        name = val->hard_value_str;

    return name;
}

static void
get_scan_list_res(GHashTable *hash,
                  gsize *xres, gsize *yres)
{
    NanoscopeValue *val;

    /* XXX: Some observed files contained correct dimensions only in
     * a global section, sizes in `image list' sections were bogus.
     * Version: 0x05300001 */
    if ((val = g_hash_table_lookup(hash, "Samps/line")))
        *xres = (gsize)val->hard_value;
    if ((val = g_hash_table_lookup(hash, "Lines")))
        *yres = (gsize)val->hard_value;
    gwy_debug("Global xres, yres = %lu, %lu", (gulong)*xres, (gulong)*yres);
}

static GwySIUnit*
get_scan_size(GHashTable *hash,
              gdouble *xreal, gdouble *yreal,
              GError **error)
{
    NanoscopeValue *val;
    GwySIUnit *unit;
    gchar un[8];
    gchar *end, *s;
    gint power10;
    gdouble q;

    /* scan size */
    val = g_hash_table_lookup(hash, "Scan size");
    *xreal = g_ascii_strtod(val->hard_value_str, &end);
    if (errno || *end != ' ') {
        g_set_error(error, GWY_MODULE_FILE_ERROR, GWY_MODULE_FILE_ERROR_DATA,
                    _("Cannot parse `Scan size' field."));
        return NULL;
    }
    gwy_debug("xreal = %g", *xreal);
    s = end+1;
    *yreal = g_ascii_strtod(s, &end);
    if (errno || *end != ' ') {
        /* Old files don't have two numbers here, assume equal dimensions */
        *yreal = *xreal;
        end = s;
    }
    gwy_debug("yreal = %g", *yreal);
    while (g_ascii_isspace(*end))
        end++;
    if (sscanf(end, "%7s", un) != 1) {
        g_set_error(error, GWY_MODULE_FILE_ERROR, GWY_MODULE_FILE_ERROR_DATA,
                    _("Cannot parse `Scan size' field."));
        return NULL;
    }
    gwy_debug("xy unit: <%s>", un);
    unit = gwy_si_unit_new_parse(un, &power10);
    q = pow10(power10);
    *xreal *= q;
    *yreal *= q;

    return unit;
}

static gboolean
has_nonsquare_aspect(GHashTable *hash)
{
    NanoscopeValue *val;
    gdouble ar;

    val = g_hash_table_lookup(hash, "Aspect ratio");
    if (!val || gwy_strequal(val->hard_value_str, "1:1"))
        return FALSE;

    ar = g_ascii_strtod(val->hard_value_str, NULL);
    if (ar > 0.0 && ar != 1.0)
        return TRUE;
    return FALSE;
}

static void
add_metadata(gpointer hkey,
             gpointer hvalue,
             gpointer user_data)
{
    gchar *key = (gchar*)hkey;
    NanoscopeValue *val = (NanoscopeValue*)hvalue;
    gchar *v, *w;

    if (gwy_strequal(key, "#self")
        || !val->hard_value_str
        || !val->hard_value_str[0])
        return;

    if (key[0] == '@')
        key++;
    v = g_strdup(val->hard_value_str);
    if (strchr(v, '\272')) {
        w = gwy_strreplace(v, "\272", "deg", -1);
        g_free(v);
        v = w;
    }
    if (strchr(v, '~')) {
        w = gwy_strreplace(v, "~", "µ", -1);
        g_free(v);
        v = w;
    }
    gwy_container_set_string_by_name(GWY_CONTAINER(user_data), key, v);
}

/* FIXME: This is a bit simplistic */
static GwyContainer*
nanoscope_get_metadata(GHashTable *hash,
                       GList *list)
{
    static const gchar *hashes[] = {
        "File list", "Scanner list", "Equipment list", "Ciao scan list",
    };
    GwyContainer *meta;
    GList *l;
    guint i;

    meta = gwy_container_new();

    for (l = list; l; l = g_list_next(l)) {
        GHashTable *h = ((NanoscopeData*)l->data)->hash;
        for (i = 0; i < G_N_ELEMENTS(hashes); i++) {
            if (gwy_strequal(g_hash_table_lookup(h, "#self"), hashes[i])) {
                g_hash_table_foreach(h, add_metadata, meta);
                break;
            }
        }
    }
    g_hash_table_foreach(hash, add_metadata, meta);

    return meta;
}

static GwyDataField*
hash_to_data_field(GHashTable *hash,
                   GHashTable *scannerlist,
                   GHashTable *scanlist,
                   GHashTable *contrlist,
                   NanoscopeFileType file_type,
                   gsize bufsize,
                   const guchar *buffer,
                   gsize gxres,
                   gsize gyres,
                   gboolean gnonsquare_aspect,
                   gchar **p,
                   GError **error)
{
    NanoscopeValue *val;
    GwyDataField *dfield;
    GwySIUnit *unitz, *unitxy;
    gsize xres, yres, bpp, qbpp, offset, size;
    gdouble xreal, yreal, q;
    gdouble *data;
    gboolean size_ok, use_global, nonsquare_aspect;

    if (!require_keys(hash, error, "Samps/line", "Number of lines",
                      "Scan size", "Data offset", "Data length", NULL))
        return NULL;

    val = g_hash_table_lookup(hash, "Samps/line");
    xres = (gsize)val->hard_value;

    val = g_hash_table_lookup(hash, "Number of lines");
    yres = (gsize)val->hard_value;

    /* Bytes/pixel determines the scaling factor, not actual raw data type.
     * This is what Bruker people say. */
    val = g_hash_table_lookup(hash, "Bytes/pixel");
    qbpp = val ? (gsize)val->hard_value : 2;

    bpp = 2;
    if (file_type == NANOSCOPE_FILE_TYPE_BIN32)
        bpp = 4;

    nonsquare_aspect = has_nonsquare_aspect(hash);
    gwy_debug("xres %lu, yres %lu", (gulong)xres, (gulong)yres);
    gwy_debug("gxres %lu, gyres %lu", (gulong)gxres, (gulong)gyres);

    /* Scan size */
    if (!(unitxy = get_scan_size(hash, &xreal, &yreal, error)))
        return NULL;

    /* Prevents possible division by 0 when gxres and gyres are not set for
     * whatever reason. */
    if (!gxres)
        gxres = xres;
    if (!gyres)
        gyres = xres;

    gwy_debug("self: %s", (char*)g_hash_table_lookup(hash, "#self"));
    offset = size = 0;
    if (file_type == NANOSCOPE_FILE_TYPE_BIN
        || file_type == NANOSCOPE_FILE_TYPE_BIN32) {
        val = g_hash_table_lookup(hash, "Data offset");
        offset = (gsize)val->hard_value;

        val = g_hash_table_lookup(hash, "Data length");
        size = (gsize)val->hard_value;

        size_ok = FALSE;
        use_global = FALSE;

        /* Try channel size and local size */
        if (!size_ok && size == bpp*xres*yres)
            size_ok = TRUE;

        if (!size_ok && size == bpp*gxres*gyres) {
            size_ok = TRUE;
            use_global = TRUE;
        }

        /* If they don't match exactly, try whether they at least fit inside */
        if (!size_ok && size > bpp*MAX(xres*yres, gxres*gyres)) {
            size_ok = TRUE;
            use_global = (xres*yres < gxres*gyres);
        }

        if (!size_ok && size > bpp*MIN(xres*yres, gxres*gyres)) {
            size_ok = TRUE;
            use_global = (xres*yres > gxres*gyres);
        }

        if (!size_ok) {
            err_SIZE_MISMATCH(error, size, bpp*xres*yres, TRUE);
            return NULL;
        }

        if (use_global) {
            if (gxres) {
                xreal *= (gdouble)gxres/xres;
                xres = gxres;
            }
            if (gyres) {
                yreal *= (gdouble)gyres/yres;
                yres = gyres;
            }
        }
        else if (nonsquare_aspect) {
            gwy_debug("nonsquare_aspect");
            if (gnonsquare_aspect) {
                gwy_debug("gnonsquare_aspect");
                /* Reported by Peter Eaton.  Not sure if we detect it
                 * correctly. */
                yreal *= yres;
                yreal /= xres;
            }
            else {
                /* This seems to be the common case. */
                yreal *= yres;
                yreal /= gyres;
            }
        }

        if (err_DIMENSION(error, xres) || err_DIMENSION(error, yres))
            return NULL;

        if (err_SIZE_MISMATCH(error, offset + size, bufsize, FALSE))
            return NULL;

        /* Use negated positive conditions to catch NaNs */
        if (!((xreal = fabs(xreal)) > 0)) {
            g_warning("Real x size is 0.0, fixing to 1.0");
            xreal = 1.0;
        }
        if (!((yreal = fabs(yreal)) > 0)) {
            g_warning("Real y size is 0.0, fixing to 1.0");
            yreal = 1.0;
        }
    }

    q = 1.0;
    unitz = get_physical_scale(hash, scannerlist, scanlist, contrlist,
                               &q, error);
    if (!unitz)
        return NULL;

    dfield = gwy_data_field_new(xres, yres, xreal, yreal, FALSE);
    data = gwy_data_field_get_data(dfield);
    switch (file_type) {
        case NANOSCOPE_FILE_TYPE_TXT:
        if (!read_text_data(xres*yres, data, p, qbpp, error)) {
            g_object_unref(dfield);
            return NULL;
        }
        break;

        case NANOSCOPE_FILE_TYPE_BIN:
        case NANOSCOPE_FILE_TYPE_BIN32:
        if (!read_binary_data(xres*yres, data, buffer + offset, bpp, qbpp,
                              error)) {
            g_object_unref(dfield);
            return NULL;
        }
        break;

        default:
        g_assert_not_reached();
        break;
    }
    gwy_data_field_multiply(dfield, q);
    gwy_data_field_invert(dfield, TRUE, FALSE, FALSE);
    gwy_data_field_set_si_unit_z(dfield, unitz);
    g_object_unref(unitz);

    gwy_data_field_set_si_unit_xy(dfield, unitxy);
    g_object_unref(unitxy);

    return dfield;
}

static GwyBrick*
hash_to_brick(GHashTable *hash,
              GHashTable *forcelist,
              GHashTable *scanlist,
              GHashTable *scannerlist,
              GHashTable *equipmentlist,
              NanoscopeFileType file_type,
              gsize bufsize,
              const guchar *buffer,
              GwyBrick **brick2,
              GError **error)
{
    GwyBrick *brick, *tmpbrick;
    NanoscopeValue *val;
    gdouble *data, *data2, *tmp;
    guint xres, yres, zres, offset, length, bpp, qbpp, i, j;
    gdouble xreal, yreal, zreal, zoff, q;
    GwySIUnit *unitxy, *unitw, *unitz;
    NanoscopeSpectraType spectype;
    gboolean continuous = FALSE, have_two_bricks;
    const guchar *p;

    gwy_debug("Loading brick");

    *brick2 = NULL;
    if (!require_keys(hash, error, "Samps/line", "Data offset", "Data length",
                      NULL))
        return NULL;
    if (!require_keys(forcelist, error, "force/line", NULL))
        return NULL;
    if (!require_keys(scanlist, error, "Scan size", "Lines", NULL))
        return NULL;

    if ((val = g_hash_table_lookup(scanlist, "Capture Mode"))) {
        if (strstr(val->hard_value_str, "Continuous")) {
            g_warning("Continuous mode.  Everything is screwed up.");
            continuous = TRUE;
        }
    }

    /* scan size */
    val = g_hash_table_lookup(hash, "Data offset");
    offset = (gsize)val->hard_value;
    gwy_debug("data offset %lu", (glong)offset);

    val = g_hash_table_lookup(hash, "Data length");
    length = (gsize)val->hard_value;
    gwy_debug("data length %lu", (glong)length);

    val = g_hash_table_lookup(hash, "Samps/line");
    zres = (gsize)val->hard_value;
    gwy_debug("samples in force line %lu", (glong)zres);

    val = g_hash_table_lookup(forcelist, "force/line");
    xres = (gsize)val->hard_value;
    gwy_debug("force curves per line %lu", (glong)xres);

    /* It seems the number of volume data lines is always simply the number
     * of lines. */
    val = g_hash_table_lookup(scanlist, "Lines");
    yres = (gsize)val->hard_value;
    gwy_debug("Number of lines %lu", (glong)yres);

    /* Bytes/pixel determines the scaling factor, not actual raw data type.
     * This is what Bruker people say. */
    val = g_hash_table_lookup(hash, "Bytes/pixel");
    qbpp = val ? (gsize)val->hard_value : 2;
    gwy_debug("Bytes/pixel %u", qbpp);

    bpp = 2;
    if (file_type == NANOSCOPE_FILE_TYPE_FORCE_VOLUME32)
        bpp = 4;

    if (err_DIMENSION(error, xres)
        || err_DIMENSION(error, yres)
        || err_DIMENSION(error, zres))
        return NULL;

    if (length == xres*yres*zres*bpp)
        have_two_bricks = FALSE;
    else if (length == 2*xres*yres*zres*bpp)
        have_two_bricks = TRUE;
    else {
        err_SIZE_MISMATCH(error, length, 2*xres*yres*zres*bpp, TRUE);
        return NULL;
    }

    if (err_SIZE_MISMATCH(error, offset + length, bufsize, FALSE))
        return NULL;

    /* Scan size */
    if (!(unitxy = get_scan_size(scanlist, &xreal, &yreal, error)))
        return NULL;

    q = 1.0;
    unitw = get_physical_scale(hash, scannerlist, scanlist, equipmentlist,
                               &q, error);
    gwy_debug("physical scale %g", q);
    if (!unitw) {
        g_object_unref(unitxy);
        return NULL;
    }

    /* XXX XXX XXX: This correct the value scale for Phase and Amplitude for
     * the MFM data we have.  No idea why we need 2 here.  Possibly it comes
     * from wrong Bytes/pixel handling?
     * XXX: But ZSensor is correct without the factor 2.  What is the
     * difference?*/
    //q *= 2.0;

    unitz = get_spec_abscissa_scale(hash, forcelist, scannerlist, scanlist,
                                    &zreal, &zoff, &spectype, NULL);
    if (!unitz) {
        g_object_unref(unitxy);
        g_object_unref(unitw);
        return NULL;
    }
    gwy_debug("spectype %d", spectype);
    gwy_debug("zreal %g, zoff %g", zreal, zoff);

    /* We start with x and z swapped, as in the file. */
    brick = gwy_brick_new(zres, yres, xres, 1.0, 1.0, 1.0, FALSE);
    data = gwy_brick_get_data(brick);

    /*up to now it seems that second brick is always present*/
    if (have_two_bricks) {
        *brick2 = gwy_brick_new(zres, yres, xres, 1.0, 1.0, 1.0, FALSE);
        data2 = gwy_brick_get_data(*brick2);
    }

    p = buffer + offset;
    /* All this needs axes swapped because we organise data by plane, having
     * z the outermose, not innermost index. */
    gwy_debug("coverting raw data");
    for (i = 0; i < yres; i++) {
        for (j = 0; j < xres; j++) {
            /* Approach curve */
            read_binary_data(zres, data + i*zres*xres + j*zres, p,
                             bpp, qbpp, NULL);
            p += bpp*zres;
            if (have_two_bricks) {
                /* Retract curve(?) */
                /* This needs to be reversed. */
                read_binary_data(zres, data2 + i*zres*xres + j*zres, p,
                                 bpp, qbpp, NULL);
                p += bpp*zres;
            }
        }
    }

    gwy_debug("swapping axes");
    tmpbrick = gwy_brick_new(xres, yres, zres, 1.0, 1.0, 1.0, FALSE);

    /* FIXME FIXME FIXME: When we have two bricks, one is approach and the
     * other retract -- most likely.  Not sure how to tell from the data. */
    swap_brick_x_z(brick, tmpbrick, FALSE);
    set_brick_properties(brick, unitxy, unitz, unitw,
                         xreal, yreal, zreal, q);
    if (have_two_bricks) {
        swap_brick_x_z(*brick2, tmpbrick, FALSE);
        set_brick_properties(*brick2, unitxy, unitz, unitw,
                             xreal, yreal, zreal, q);
    }

    /* XXX XXX XXX XXX No idea what this achieves. */
#if 0
    if (continuous) {
        st = 47; //ad hoc fix, this should be detected automaticallz
    } else
        st = 0;

    /*split data again with this strange shift*/
    gwy_debug("strange split");
    for (i = 0; i < yres; i++) {
        for (j = 0; j < xres; j++) {
            // Approach curves
            for (l = 0; l < zres; l++) {
                adata[(l*yres + i)*xres + j] = storage[st++];
                if (adata[(l*yres + i)*xres + j]<-0.45) 
                    adata[(l*yres + i)*xres + j] = storage[st-2]; //strange PF/QNM value?
            }
            // Retract curves
            for (l = 0; l < zres; l++) {
                rdata[((zres-l-1)*yres + i)*xres + j] = storage[st++];
            }
        }
    }
#endif

    if (continuous) {
        gwy_debug("fixing sine distortion");
        tmp = gwy_brick_get_data(tmpbrick);
        fix_sine_distortion(brick, tmp);
        if (have_two_bricks)
            fix_sine_distortion(*brick2, tmp);
    }
    g_object_unref(tmpbrick);
    g_object_unref(unitxy);
    g_object_unref(unitw);
    g_object_unref(unitz);

    return brick;
}

static void
set_brick_properties(GwyBrick *brick,
                     GwySIUnit *unitxy, GwySIUnit *unitz, GwySIUnit *unitw,
                     gdouble xreal, gdouble yreal, gdouble zreal,
                     gdouble q)
{
    gwy_serializable_clone(G_OBJECT(unitxy),
                           G_OBJECT(gwy_brick_get_si_unit_x(brick)));
    gwy_serializable_clone(G_OBJECT(unitxy),
                           G_OBJECT(gwy_brick_get_si_unit_y(brick)));
    gwy_serializable_clone(G_OBJECT(unitz),
                           G_OBJECT(gwy_brick_get_si_unit_z(brick)));
    gwy_serializable_clone(G_OBJECT(unitw),
                           G_OBJECT(gwy_brick_get_si_unit_w(brick)));
    gwy_brick_set_xreal(brick, xreal);
    gwy_brick_set_yreal(brick, yreal);
    gwy_brick_set_zreal(brick, zreal);
    gwy_brick_multiply(brick, q);
}

static void
swap_brick_x_z(GwyBrick *brick, GwyBrick *tmp, gboolean zinv)
{
    gwy_serializable_clone(G_OBJECT(brick), G_OBJECT(tmp));
    gwy_brick_transpose(tmp, brick,
                        GWY_BRICK_TRANSPOSE_ZYX, FALSE, FALSE, zinv);
}

static void
fix_sine_distortion(GwyBrick *brick, gdouble *buf)
{
    gint xres, yres, zres, n, i, l, floorv;
    gdouble *data, *destlevel, *srclevel1, *srclevel2;
    gdouble newl, rest;

    xres = gwy_brick_get_xres(brick);
    yres = gwy_brick_get_yres(brick);
    zres = gwy_brick_get_zres(brick);
    data = gwy_brick_get_data(brick);
    n = xres*yres;
    /*remove sine distortion from the data*/

    for (l = 0; l < zres; l++) {
        /* Calculate new z level. */
        newl = zres*(asin(2.0*l/zres - 1.0) + G_PI/2)/G_PI;
        floorv = (gint)floor(newl);
        rest = newl - floorv;
        if (floorv < 0) {
            floorv = 0;
            rest = 0.0;
        }
        else if (floorv >= zres-1) {
            floorv = zres-2;
            rest = 1.0;
        }
        /* Copy-interpolate the entire plane to a temporary buffer. */
        destlevel = buf + n*l;
        srclevel1 = data + n*floorv;
        srclevel2 = data + n*(floorv + 1);
        for (i = 0; i < n; i++)
            destlevel[i] = (1.0 - rest)*srclevel1[i] + rest*srclevel2[i];
    }
    gwy_assign(data, buf, n*zres);
}

static GwyDataField*
preview_from_brick(GwyBrick *brick)
{
    GwyDataField *dfield = gwy_data_field_new(10, 10, 10, 10, FALSE);

    gwy_debug("making brick preview");
    gwy_brick_mean_plane(brick, dfield, 0, 0, 0, gwy_brick_get_xres(brick),
                         gwy_brick_get_yres(brick), -1, FALSE);

    return dfield;

}

/* Find pairs (SomeData, ZSensor), use ZSensor for the z coordinate and remove
 * the ZSensor bricks. */
static gint
rebase_force_volume_data(GList *list)
{
    GList *l, *prev = NULL;
    NanoscopeData *zsdata, *ndata;
    GwyBrick *tmpbrick = NULL;
    const gchar *name;
    gint rebased = 0;

    for (l = list; l; l = g_list_next(l)) {
        ndata = (NanoscopeData*)l->data;
        /* Bricks must be consecutive. */
        if (!ndata->brick) {
            prev = NULL;
            continue;
        }
        if (!prev) {
            prev = l;
            continue;
        }
        if (!(name = get_image_data_name(ndata->hash))
            || !gwy_strequal(name, "ZSensor")) {
            gwy_debug("second brick [pair] is not ZSensor, skipping");
            continue;
        }
        zsdata = ndata;
        ndata = (NanoscopeData*)prev->data;
        if ((zsdata->brick2 && !ndata->brick2)
            || (!zsdata->brick2 && ndata->brick2)) {
            gwy_debug("two-bricks/one-brick mismatch, skipping");
            continue;
        }

        if (gwy_brick_check_compatibility(ndata->brick, zsdata->brick,
                                          GWY_DATA_COMPATIBILITY_RES)) {
            gwy_debug("brick resolution mismatch, skipping");
            continue;
        }
        if (zsdata->brick2
            && ndata->brick2
            && gwy_brick_check_compatibility(ndata->brick2, zsdata->brick2,
                                             GWY_DATA_COMPATIBILITY_RES)) {
            gwy_debug("brick 2 resolution mismatch, skipping");
            continue;
        }

        if (!tmpbrick)
            tmpbrick = gwy_brick_new_alike(ndata->brick, FALSE);

        gwy_debug("rebasing brick %s", get_image_data_name(ndata->hash));
        rebase_one_brick(ndata->brick, zsdata->brick, tmpbrick,
                         &ndata->min_field, &ndata->range_field);
        GWY_OBJECT_UNREF(zsdata->brick);

        if (ndata->brick2) {
            gwy_debug("rebasing brick2 %s", get_image_data_name(ndata->hash));
            rebase_one_brick(ndata->brick2, zsdata->brick2, tmpbrick,
                             &ndata->min_field2, &ndata->range_field2);
            GWY_OBJECT_UNREF(zsdata->brick2);
        }

        /* When we finish a pair, reset. */
        prev = NULL;
        rebased++;
    }
    GWY_OBJECT_UNREF(tmpbrick);

    return rebased;
}

static void
rebase_one_brick(GwyBrick *brick, GwyBrick *zbrick, GwyBrick *tmpbrick,
                 GwyDataField **pmin_field, GwyDataField **prange_field)
{
    GwyDataField *min_field, *range_field;
    gint xres, yres, zres, n, i, j, l, k, ij;
    gdouble min, range, z0, z, dz;
    const gdouble *zd, *md, *rd, *zline;
    gint *pos;
    gdouble *d, *t;

    xres = gwy_brick_get_xres(brick);
    yres = gwy_brick_get_yres(brick);
    zres = gwy_brick_get_zres(brick);
    if (zres < 2)
        return;

    gwy_brick_resample(tmpbrick, xres, yres, zres, GWY_INTERPOLATION_NONE);
    n = xres*yres;

    zd = gwy_brick_get_data_const(zbrick);
    /* We want increasing z values.  Flip bricks if they seem in the reverse
     * order. */
    if (zd[0] > zd[n*(zres - 1)]) {
        gwy_brick_transpose(brick, tmpbrick, GWY_BRICK_TRANSPOSE_XYZ,
                            FALSE, FALSE, TRUE);
        t = gwy_brick_get_data(tmpbrick);
        d = gwy_brick_get_data(brick);
        gwy_assign(d, t, n*zres);

        gwy_brick_transpose(zbrick, tmpbrick, GWY_BRICK_TRANSPOSE_XYZ,
                            FALSE, FALSE, TRUE);
        t = gwy_brick_get_data(tmpbrick);
        d = gwy_brick_get_data(zbrick);
        gwy_assign(d, t, n*zres);
    }
    t = gwy_brick_get_data(tmpbrick);
    d = gwy_brick_get_data(zbrick);

    min_field = gwy_data_field_new(xres, yres, 1.0, 1.0, FALSE);
    *pmin_field = min_field;
    gwy_brick_min_plane(zbrick, min_field, 0, 0, 0, xres, yres, -1, TRUE);

    range_field = gwy_data_field_new_alike(min_field, FALSE);
    *prange_field = range_field;
    gwy_brick_max_plane(zbrick, range_field, 0, 0, 0, xres, yres, -1, TRUE);
    gwy_data_field_subtract_fields(range_field, range_field, min_field);

    min = gwy_data_field_get_min(min_field);
    gwy_brick_set_zoffset(brick, min);
    range = gwy_data_field_get_max(range_field);
    gwy_brick_set_zreal(brick, range);

    pos = g_new0(gint, n);
    for (l = 0; l < zres; l++) {
        md = gwy_data_field_get_data_const(min_field);
        rd = gwy_data_field_get_data_const(range_field);
        z0 = l/(zres - 1.0)*range;
        for (i = 0; i < yres; i++) {
            for (j = 0; j < xres; j++, t++, md++, rd++) {
                ij = i*yres + j;
                k = pos[ij];
                zline = zd + ij;
                z = z0 + *md;
                while (k > 0 && z < zline[n*k])
                    k--;
                while (k < zres-1 && z >= zline[n*(k+1)])
                    k++;

                if (k == 0 && z <= zline[0])
                    *t = d[ij];
                else if (k == zres-1 && z >= zline[n*k])
                    *t = d[n*(zres - 1) + ij];
                else {
                    if (k == zres-1)
                        k--;
                    g_assert_cmpint(k, >=, 0);
                    g_assert_cmpint(k, <, zres-1);
                    dz = zline[n*(k + 1)] - zline[n*k];
                    if (G_LIKELY(dz > 0.0)) {
                        *t = ((z + zline[n*k])*d[n*(k + 1) + ij]
                              + (zline[n*(k + 1)] - z)*d[n*k + ij])/dz;
                    }
                    else {
                        *t = 0.5*(d[n*(k + 1) + ij] + d[n*k + ij]);
                    }
                }
                pos[ij] = k;
            }
        }
    }
    g_free(pos);

    t = gwy_brick_get_data(tmpbrick);
    gwy_assign(d, t, n*zres);
}

#define CHECK_AND_APPLY(op, hash, key)                     \
        if (!(val = g_hash_table_lookup((hash), (key)))) { \
            err_MISSING_FIELD(error, (key));               \
            return NULL;                                   \
        }                                                  \
        *scale op val->hard_value

static GwySIUnit*
get_physical_scale(GHashTable *hash,
                   GHashTable *scannerlist,
                   GHashTable *scanlist,
                   GHashTable *contrlist,
                   gdouble *scale,
                   GError **error)
{
    GwySIUnit *siunit, *siunit2;
    NanoscopeValue *val, *sval;
    gchar *key;
    gint q;

    /* version = 4.2 */
    if ((val = g_hash_table_lookup(hash, "Z scale"))) {
        /* Old style scales */
        gwy_debug("Old-style scale, using hard units %g %s",
                  val->hard_value, val->hard_value_units);
        siunit = gwy_si_unit_new_parse(val->hard_value_units, &q);
        *scale = val->hard_value * pow10(q);
        return siunit;

    }
    /* version >= 4.3 */
    else if ((val = g_hash_table_lookup(hash, "@4:Z scale"))
             || (val = g_hash_table_lookup(hash, "@2:Z scale"))) {
        /* Resolve reference to a soft scale */
        if (val->soft_scale) {
            gwy_debug("have soft scale (%s)", val->soft_scale);
            key = g_strdup_printf("@%s", val->soft_scale);

            if (!(sval = g_hash_table_lookup(scannerlist, key))
                && (!scanlist
                    || !(sval = g_hash_table_lookup(scanlist, key)))) {
                g_warning("`%s' not found", key);
                g_free(key);
                /* XXX */
                *scale = val->hard_value;
                return gwy_si_unit_new("");
            }

            *scale = val->hard_value*sval->hard_value;
            gwy_debug("Hard-value scale %g (%g * %g)",
                      *scale, val->hard_value, sval->hard_value);

            if (!sval->hard_value_units || !*sval->hard_value_units) {
                gwy_debug("No hard value units");
                if (gwy_strequal(val->soft_scale, "Sens. Phase"))
                    siunit = gwy_si_unit_new("deg");
                else
                    siunit = gwy_si_unit_new("V");
            }
            else {
                siunit = gwy_si_unit_new_parse(sval->hard_value_units, &q);
                if (val->hard_value_units && *val->hard_value_units) {
                    siunit2 = gwy_si_unit_new(val->hard_value_units);
                }
                else
                    siunit2 = gwy_si_unit_new("V");
                gwy_si_unit_multiply(siunit, siunit2, siunit);
                gwy_debug("Scale1 = %g V/LSB", val->hard_value);
                gwy_debug("Scale2 = %g %s",
                          sval->hard_value, sval->hard_value_units);
                *scale *= pow10(q);
#ifdef DEBUG
                gwy_debug("Total scale = %g %s/LSB",
                          *scale,
                          gwy_si_unit_get_string(siunit,
                                                 GWY_SI_UNIT_FORMAT_PLAIN));
#endif
                g_object_unref(siunit2);
            }
            g_free(key);
        }
        else {
            /* We have '@2:Z scale' but the reference to soft scale is missing,
             * the quantity is something in the hard units (seen for Potential). */
            gwy_debug("No soft scale, using hard units %g %s",
                      val->hard_value, val->hard_value_units);
            siunit = gwy_si_unit_new_parse(val->hard_value_units, &q);
            *scale = val->hard_value * pow10(q);
        }
        return siunit;
    }
    else  { /* no version */
        if (!(val = g_hash_table_lookup(hash, "Image data"))) {
             err_MISSING_FIELD(error, "Image data");
             return NULL;
        }

        if (gwy_strequal(val->hard_value_str, "Deflection")) {
            siunit = gwy_si_unit_new("m"); /* always? */
            *scale = 1e-9 * 2.0/65536.0;
            CHECK_AND_APPLY(*=, hash, "Z scale defl");
            CHECK_AND_APPLY(*=, contrlist, "In1 max");
            CHECK_AND_APPLY(*=, scannerlist, "In sensitivity");
            CHECK_AND_APPLY(/=, scanlist, "Detect sens.");
            return siunit;
        }
        else if (gwy_strequal(val->hard_value_str, "Amplitude")) {
            siunit = gwy_si_unit_new("m");
            *scale = 1e-9 * 2.0/65536.0;
            CHECK_AND_APPLY(*=, hash, "Z scale ampl");
            CHECK_AND_APPLY(*=, contrlist, "In1 max");
            CHECK_AND_APPLY(*=, scannerlist, "In sensitivity");
            CHECK_AND_APPLY(/=, scanlist, "Detect sens.");
            return siunit;
        }
        else if (gwy_strequal(val->hard_value_str, "Frequency")) {
            siunit = gwy_si_unit_new("Hz");
            *scale = 25e6/32768.0;
            CHECK_AND_APPLY(*=, hash, "Z scale freq");
            return siunit;
        }
        else if (gwy_strequal(val->hard_value_str, "Current")) {
            siunit = gwy_si_unit_new("A");
            *scale = 1e-9 * 2.0/32768.0;
            CHECK_AND_APPLY(*=, hash, "Z scale amplitude");
            CHECK_AND_APPLY(*=, contrlist, "In1 max");
            CHECK_AND_APPLY(*=, scannerlist, "In sensitivity");
            return siunit;
        }
        else if (gwy_strequal(val->hard_value_str, "Phase")) {
            siunit = gwy_si_unit_new("deg");
            *scale = 180.0/65536.0;
            CHECK_AND_APPLY(*=, hash, "Z scale phase");
            return siunit;
        }
        else if (gwy_strequal(val->hard_value_str, "Height")) {
            siunit = gwy_si_unit_new("m");
            *scale = 1e-9 * 2.0/65536.0;
            CHECK_AND_APPLY(*=, hash, "Z scale height");
            CHECK_AND_APPLY(*=, contrlist, "Z max");
            CHECK_AND_APPLY(*=, scannerlist, "Z sensitivity");
            return siunit;
        }

        return NULL;
    }
}

static GwyGraphModel*
hash_to_curve(GHashTable *hash,
              GHashTable *forcelist,
              GHashTable *scanlist,
              GHashTable *scannerlist,
              NanoscopeFileType file_type,
              gsize bufsize,
              const guchar *buffer,
              gint gxres,
              GError **error)
{
    NanoscopeValue *val;
    NanoscopeSpectraType spectype;
    GwyDataLine *dline;
    GwyGraphModel *gmodel;
    GwyGraphCurveModel *gcmodel;
    GwySIUnit *unitz, *unitx;
    gsize xres, bpp, qbpp, offset, size;
    gdouble xreal, xoff, q = 1.0;
    gdouble *data;
    gboolean size_ok, use_global, convert_to_force = FALSE;
    /* Call the curves Trace and Retrace when we do now know (often not
     * correct).  Replace the titles with something meaningful for specific
     * types. */
    const gchar *title0 = "Trace", *title1 = "Retrace";

    g_return_val_if_fail(file_type == NANOSCOPE_FILE_TYPE_FORCE_BIN
                         || file_type == NANOSCOPE_FILE_TYPE_FORCE_BIN32,
                         NULL);

    if (!require_keys(hash, error,
                      "Samps/line", "Data offset", "Data length",
                      "@4:Image Data",
                      NULL))
        return NULL;

    if (!require_keys(scanlist, error, "Scan size", NULL))
        return NULL;

    if (!(unitx = get_spec_abscissa_scale(hash, forcelist,
                                          scannerlist, scanlist,
                                          &xreal, &xoff, &spectype,
                                          error)))
        return NULL;

    val = g_hash_table_lookup(hash, "Samps/line");
    xres = (gsize)val->hard_value;

    /* Bytes/pixel determines the scaling factor, not actual raw data type.
     * This is what Bruker people say. */
    val = g_hash_table_lookup(hash, "Bytes/pixel");
    qbpp = val ? (gsize)val->hard_value : 2;

    bpp = 2;
    if (file_type == NANOSCOPE_FILE_TYPE_FORCE_BIN32)
        bpp = 4;

    /* scan size */
    offset = size = 0;
    val = g_hash_table_lookup(hash, "Data offset");
    offset = (gsize)val->hard_value;

    val = g_hash_table_lookup(hash, "Data length");
    size = (gsize)val->hard_value;

    size_ok = FALSE;
    use_global = FALSE;

    /* Try channel size and local size */
    if (!size_ok && size == 2*bpp*xres)
        size_ok = TRUE;

    if (!size_ok && size == 2*bpp*gxres) {
        size_ok = TRUE;
        use_global = TRUE;
    }

    gwy_debug("size=%lu, xres=%lu, gxres=%lu, bpp=%lu",
              (gulong)size, (gulong)xres, (gulong)gxres, (gulong)bpp);

    /* If they don't match exactly, try whether they at least fit inside */
    if (!size_ok && size > bpp*MAX(2*xres, 2*gxres)) {
        size_ok = TRUE;
        use_global = (xres < gxres);
    }

    if (!size_ok && size > bpp*MIN(2*xres, 2*gxres)) {
        size_ok = TRUE;
        use_global = (xres > gxres);
    }

    if (!size_ok) {
        err_SIZE_MISMATCH(error, size, bpp*xres, TRUE);
        return NULL;
    }

    if (use_global && gxres)
        xres = gxres;

    if (err_DIMENSION(error, xres))
        return NULL;

    if (err_SIZE_MISMATCH(error, offset + size, bufsize, FALSE))
        return NULL;

    /* Use negated positive conditions to catch NaNs */
    if (!((xreal = fabs(xreal)) > 0)) {
        g_warning("Real x size is 0.0, fixing to 1.0");
        xreal = 1.0;
    }

    val = g_hash_table_lookup(hash, "@4:Image Data");
    if (spectype == NANOSCOPE_SPECTRA_FZ
        && gwy_strequal(val->hard_value_str, "Deflection Error"))
        convert_to_force = TRUE;

    if (!(unitz = get_spec_ordinate_scale(hash, scanlist, &q, &convert_to_force,
                                          error)))
        return NULL;

    gmodel = gwy_graph_model_new();
    // TODO: Spectrum type.
    if (spectype == NANOSCOPE_SPECTRA_IV) {
        g_object_set(gmodel,
                     "title", "I-V spectrum",
                     "axis-label-bottom", "Voltage",
                     "axis-label-left", val->hard_value_str,
                     NULL);
    }
    else if (convert_to_force) {
        title0 = "Extend";
        title1 = "Retract";
        g_object_set(gmodel,
                     "title", "F-Z spectrum",
                     "axis-label-bottom", "Distance",
                     "axis-label-left", "Force",
                     NULL);
    }
    else if (spectype == NANOSCOPE_SPECTRA_FZ) {
        title0 = "Extend";
        title1 = "Retract";
        g_object_set(gmodel,
                     "title", "F-Z spectrum",
                     "axis-label-bottom", "Distance",
                     "axis-label-left", val->hard_value_str,
                     NULL);
    }

    dline = gwy_data_line_new(xres, xreal, FALSE);
    gwy_data_line_set_offset(dline, xoff);
    gwy_data_line_set_si_unit_y(dline, unitz);
    g_object_unref(unitz);
    gwy_data_line_set_si_unit_x(dline, unitx);
    g_object_unref(unitx);

    data = gwy_data_line_get_data(dline);
    if (!read_binary_data(xres, data, buffer + offset, bpp, qbpp, error)) {
        g_object_unref(dline);
        g_object_unref(gmodel);
        return NULL;
    }
    if (spectype == NANOSCOPE_SPECTRA_FZ)
        gwy_data_line_invert(dline, TRUE, FALSE);
    gwy_data_line_multiply(dline, q);
    gcmodel = gwy_graph_curve_model_new();
    gwy_graph_curve_model_set_data_from_dataline(gcmodel, dline, 0, 0);
    g_object_set(gcmodel,
                 "mode", GWY_GRAPH_CURVE_LINE,
                 "color", gwy_graph_get_preset_color(0),
                 "description", title0,
                 NULL);
    gwy_graph_model_add_curve(gmodel, gcmodel);
    g_object_unref(gcmodel);

    if (!read_binary_data(xres, data, buffer + offset + bpp*xres, bpp, qbpp,
                          error)) {
        g_object_unref(dline);
        g_object_unref(gmodel);
        return NULL;
    }
    if (spectype == NANOSCOPE_SPECTRA_FZ)
        gwy_data_line_invert(dline, TRUE, FALSE);
    gwy_data_line_multiply(dline, q);
    gcmodel = gwy_graph_curve_model_new();
    g_object_set(gcmodel,
                 "mode", GWY_GRAPH_CURVE_LINE,
                 "color", gwy_graph_get_preset_color(1),
                 "description", title1,
                 NULL);
    gwy_graph_curve_model_set_data_from_dataline(gcmodel, dline, 0, 0);
    gwy_graph_model_add_curve(gmodel, gcmodel);
    g_object_unref(gcmodel);

    gwy_graph_model_set_units_from_data_line(gmodel, dline);
    g_object_unref(dline);

    return gmodel;
}

/*
 * get HardValue from Ciao force image list/@4:Z scale -> HARD
 * get SoftScale from Ciao force image list/@4:Z scale -> SENS
 * get \@SENS in Ciao scan list -> SOFT
 * the factor is HARD*SOFT
 */
static GwySIUnit*
get_spec_ordinate_scale(GHashTable *hash,
                        GHashTable *scanlist,
                        gdouble *scale,
                        gboolean *convert_to_force,
                        GError **error)
{
    GwySIUnit *siunit, *siunit2;
    NanoscopeValue *val, *sval;
    gchar *key;
    gint q;

    if (!(val = g_hash_table_lookup(hash, "@4:Z scale"))) {
        err_MISSING_FIELD(error, "Z scale");
        return NULL;
    }

    /* Resolve reference to a soft scale */
    if (val->soft_scale) {
        gwy_debug("Soft scale %s", val->soft_scale);
        key = g_strdup_printf("@%s", val->soft_scale);
        if ((!scanlist || !(sval = g_hash_table_lookup(scanlist, key)))) {
            g_warning("`%s' not found", key);
            g_free(key);
            /* XXX */
            *scale = 2.0*val->hard_value;
            *convert_to_force = FALSE;
            return gwy_si_unit_new("");
        }

        *scale = 2.0*val->hard_value*sval->hard_value;

        gwy_debug("Hard scale units: %s", val->hard_scale_units);
        siunit = gwy_si_unit_new_parse(sval->hard_value_units, &q);
        siunit2 = gwy_si_unit_new("V");
        gwy_si_unit_multiply(siunit, siunit2, siunit);
        gwy_debug("Scale1 = %g V/LSB", val->hard_value);
        gwy_debug("Scale2 = %g %s",
                  sval->hard_value, sval->hard_value_units);
        *scale *= pow10(q);
        gwy_debug("Total scale = %g %s/LSB",
                  *scale, gwy_si_unit_get_string(siunit,
                                                 GWY_SI_UNIT_FORMAT_PLAIN));
        g_object_unref(siunit2);
        g_free(key);

        if (g_str_has_prefix(val->hard_scale_units, "log("))
            gwy_si_unit_set_from_string(siunit, "");

        if (*convert_to_force
            && (sval = g_hash_table_lookup(hash, "Spring Constant"))) {
            gwy_debug("Spring Constant: %g", sval->hard_value);
            // FIXME: Whatever.  For some reason this means Force.
            *scale *= 10.0*sval->hard_value;
            gwy_si_unit_set_from_string(siunit, "N");
        }
        else
            *convert_to_force = FALSE;

        // FIXME: Have no idea why.
        if (gwy_strequal(val->soft_scale, "Sens. ZsensSens"))
            *scale *= 5.0;
    }
    else {
        /* FIXME: Is this possible for I-V too? */
        /* We have '@4:Z scale' but the reference to soft scale is missing,
         * the quantity is something in the hard units (seen for Potential). */
        siunit = gwy_si_unit_new_parse(val->hard_value_units, &q);
        *scale = val->hard_value * pow10(q);
        *convert_to_force = FALSE;
    }

    return siunit;
}

static GwySIUnit*
get_spec_abscissa_scale(GHashTable *hash,
                        GHashTable *forcelist,
                        GHashTable *scannerlist,
                        GHashTable *scanlist,
                        gdouble *xreal,
                        gdouble *xoff,
                        NanoscopeSpectraType *spectype,
                        GError **error)
{
    GwySIUnit *siunit, *siunit2;
    NanoscopeValue *val, *rval, *sval;
    gdouble scale = 1.0;
    gchar *key, *end;
    gint q;

    if (!(val = g_hash_table_lookup(forcelist, "@4:Ramp channel"))) {
        err_MISSING_FIELD(error, "Ramp channel");
        return NULL;
    }

    if (!val->hard_value_str) {
        err_INVALID(error, "Ramp channel");
        return NULL;
    }

    if (gwy_strequal(val->hard_value_str, "DC Sample Bias"))
        *spectype = NANOSCOPE_SPECTRA_IV;
    else if (gwy_strequal(val->hard_value_str, "Z"))
        *spectype = NANOSCOPE_SPECTRA_FZ;
    else {
        err_UNSUPPORTED(error, "Ramp channel");
        return NULL;
    }

    if (*spectype == NANOSCOPE_SPECTRA_IV) {
        if (!require_keys(forcelist, error,
                          "@4:Ramp End DC Sample Bias",
                          "@4:Ramp Begin DC Sample Bias",
                          NULL))
            return NULL;
        rval = g_hash_table_lookup(forcelist, "@4:Ramp End DC Sample Bias");
        *xreal = g_ascii_strtod(rval->hard_value_str, &end);
        rval = g_hash_table_lookup(forcelist, "@4:Ramp Begin DC Sample Bias");
        *xoff = g_ascii_strtod(rval->hard_value_str, &end);
        *xreal -= *xoff;
    }
    else if (*spectype == NANOSCOPE_SPECTRA_FZ) {
        if (!require_keys(hash, error,
                          "@4:Ramp size",
                          "Samps/line",
                          NULL))
            return NULL;
        rval = g_hash_table_lookup(hash, "@4:Ramp size");
        *xreal = g_ascii_strtod(rval->hard_value_str, &end);
        *xoff = 0.0;
    }
    else {
        g_assert_not_reached();
        return NULL;
    }
    gwy_debug("Hard ramp size: %g", *xreal);

    /* Resolve reference to a soft scale */
    if (rval->soft_scale) {
        key = g_strdup_printf("@%s", rval->soft_scale);
        if (scannerlist && (sval = g_hash_table_lookup(scannerlist, key))) {
            gwy_debug("Found %s in scannerlist", key);
        }
        else if (scanlist && (sval = g_hash_table_lookup(scanlist, key))) {
            gwy_debug("Found %s in scanlist", key);
        }
        else {
            g_warning("`%s' not found", key);
            g_free(key);
            /* XXX */
            scale = rval->hard_value;
            return gwy_si_unit_new("");
        }

        scale = sval->hard_value;

        siunit = gwy_si_unit_new_parse(sval->hard_value_units, &q);
        siunit2 = gwy_si_unit_new("V");
        gwy_si_unit_multiply(siunit, siunit2, siunit);
        gwy_debug("Scale1 = %g V/LSB", rval->hard_value);
        gwy_debug("Scale2 = %g %s",
                  sval->hard_value, sval->hard_value_units);
        scale *= pow10(q);
        gwy_debug("Total scale = %g %s/LSB",
                  scale, gwy_si_unit_get_string(siunit,
                                                GWY_SI_UNIT_FORMAT_PLAIN));
        g_object_unref(siunit2);
        g_free(key);
    }
    else {
        /* FIXME: Is this possible for spectra too? */
        /* We have '@4:Z scale' but the reference to soft scale is missing,
         * the quantity is something in the hard units (seen for Potential). */
        siunit = gwy_si_unit_new_parse(rval->hard_value_units, &q);
        scale = rval->hard_value * pow10(q);
    }

    *xreal *= scale;
    *xoff *= scale;

    return siunit;
}

static gboolean
read_text_data(guint n, gdouble *data,
               gchar **buffer,
               gint bpp,
               GError **error)
{
    guint i;
    gdouble q;
    gchar *end;
    long l, min, max;

    q = pow(1.0/256.0, bpp);
    min = 10000000;
    max = -10000000;
    for (i = 0; i < n; i++) {
        /*data[i] = q*strtol(*buffer, &end, 10);*/
        l = strtol(*buffer, &end, 10);
        min = MIN(l, min);
        max = MAX(l, max);
        data[i] = q*l;
        if (end == *buffer) {
            g_set_error(error, GWY_MODULE_FILE_ERROR,
                        GWY_MODULE_FILE_ERROR_DATA,
                        _("Garbage after data sample #%u."), i);
            return FALSE;
        }
        *buffer = end;
    }
    gwy_debug("min = %ld, max = %ld", min, max);
    return TRUE;
}

static gboolean
read_binary_data(gint n, gdouble *data,
                 const guchar *buffer,
                 gint bpp,   /* actual type */
                 gint qbpp,  /* type determining the factor, may be different */
                 GError **error)
{
    static const GwyRawDataType rawtypes[] = {
        0, GWY_RAW_DATA_SINT8, GWY_RAW_DATA_SINT16, 0, GWY_RAW_DATA_SINT32,
    };

    if (bpp >= G_N_ELEMENTS(rawtypes) || !rawtypes[bpp]) {
        err_BPP(error, bpp);
        return FALSE;
    }
    gwy_convert_raw_data(buffer, n, 1,
                         rawtypes[bpp], GWY_BYTE_ORDER_LITTLE_ENDIAN,
                         data, pow(1.0/256.0, qbpp), 0.0);
    return TRUE;
}

static GHashTable*
read_hash(gchar **buffer,
          GError **error)
{
    GHashTable *hash;
    NanoscopeValue *value;
    gchar *line, *colon;

    line = gwy_str_next_line(buffer);
    if (line[0] != '\\' || line[1] != '*')
        return NULL;
    if (gwy_strequal(line, "\\*File list end")) {
        gwy_debug("FILE LIST END");
        return NULL;
    }

    hash = g_hash_table_new_full(gwy_ascii_strcase_hash,
                                 gwy_ascii_strcase_equal,
                                 NULL, g_free);
    g_hash_table_insert(hash, "#self", g_strdup(line + 2));    /* self */
    gwy_debug("hash table <%s>", line + 2);
    while ((*buffer)[0] == '\\' && (*buffer)[1] && (*buffer)[1] != '*') {
        line = gwy_str_next_line(buffer) + 1;
        if (!line || !line[0] || !line[1] || !line[2]) {
            g_set_error(error, GWY_MODULE_FILE_ERROR,
                        GWY_MODULE_FILE_ERROR_DATA,
                        _("Truncated header line."));
            goto fail;
        }
        colon = line;
        if (line[0] == '@' && g_ascii_isdigit(line[1]) && line[2] == ':')
            colon = line+3;
        colon = strchr(colon, ':');
        if (!colon || !g_ascii_isspace(colon[1])) {
            g_set_error(error, GWY_MODULE_FILE_ERROR,
                        GWY_MODULE_FILE_ERROR_DATA,
                        _("Missing colon in header line."));
            goto fail;
        }
        *colon = '\0';
        do {
            colon++;
        } while (g_ascii_isspace(*colon));
        g_strchomp(line);
        value = parse_value(line, colon);
        if (value)
            g_hash_table_insert(hash, line, value);

        while ((*buffer)[0] == '\r') {
            g_warning("Possibly split line encountered.  "
                      "Trying to synchronize.");
            line = gwy_str_next_line(buffer) + 1;
            line = gwy_str_next_line(buffer) + 1;
        }
    }

    /* Fix random stuff in Nanoscope E files */
    if ((value = g_hash_table_lookup(hash, "Samps/line"))
        && !g_hash_table_lookup(hash, "Number of lines")
        && value->hard_value_units
        && g_ascii_isdigit(value->hard_value_units[0])) {
        NanoscopeValue *val;
        val = g_new0(NanoscopeValue, 1);
        val->hard_value = g_ascii_strtod(value->hard_value_units, NULL);
        val->hard_value_str = value->hard_value_units;
        g_hash_table_insert(hash, "Number of lines", val);
    }

    return hash;

fail:
    g_hash_table_destroy(hash);
    return NULL;
}

/* General parameter line parser */
static NanoscopeValue*
parse_value(const gchar *key, gchar *line)
{
    NanoscopeValue *val;
    gchar *p, *q;
    guint len;

    val = g_new0(NanoscopeValue, 1);

    /* old-style values */
    if (key[0] != '@') {
        val->hard_value = g_ascii_strtod(line, &p);
        if (p-line > 0 && *p == ' ') {
            do {
                p++;
            } while (g_ascii_isspace(*p));
            if ((q = strchr(p, '('))) {
                *q = '\0';
                q++;
                val->hard_scale = g_ascii_strtod(q, &q);
                if (*q != ')')
                    val->hard_scale = 0.0;
            }
            val->hard_value_units = p;
        }
        val->hard_value_str = line;
        return val;
    }

    /* type */
    switch (line[0]) {
        case 'V':
        val->type = NANOSCOPE_VALUE_VALUE;
        break;

        case 'S':
        val->type = NANOSCOPE_VALUE_SELECT;
        break;

        case 'C':
        val->type = NANOSCOPE_VALUE_SCALE;
        break;

        default:
        g_warning("Cannot parse value type <%s> for key <%s>", line, key);
        g_free(val);
        return NULL;
        break;
    }

    line++;
    if (line[0] != ' ') {
        g_warning("Cannot parse value type <%s> for key <%s>", line, key);
        g_free(val);
        return NULL;
    }
    do {
        line++;
    } while (g_ascii_isspace(*line));

    /* softscale */
    if (line[0] == '[') {
        if (!(p = strchr(line, ']'))) {
            g_warning("Cannot parse soft scale <%s> for key <%s>", line, key);
            g_free(val);
            return NULL;
        }
        if (p-line-1 > 0) {
            *p = '\0';
            val->soft_scale = line+1;
        }
        line = p+1;
        if (line[0] != ' ') {
            g_warning("Cannot parse soft scale <%s> for key <%s>", line, key);
            g_free(val);
            return NULL;
        }
        do {
            line++;
        } while (g_ascii_isspace(*line));
    }

    /* hardscale (probably useless) */
    if (line[0] == '(') {
        int paren_level;
        do {
            line++;
        } while (g_ascii_isspace(*line));
        for (p = line, paren_level = 1; *p && paren_level; p++) {
            if (*p == ')')
                paren_level--;
            else if (*p == '(')
                paren_level++;
        }
        if (!*p) {
            g_warning("Cannot parse hard scale <%s> for key <%s>", line, key);
            g_free(val);
            return NULL;
        }
        p--;
        val->hard_scale = g_ascii_strtod(line, &q);
        while (g_ascii_isspace(*q))
            q++;
        if (p-q > 0) {
            *p = '\0';
            val->hard_scale_units = q;
            g_strchomp(q);
            if (g_str_has_suffix(q, "/LSB"))
                q[strlen(q) - 4] = '\0';
        }
        line = p+1;
        if (line[0] != ' ') {
            g_warning("Cannot parse hard scale <%s> for key <%s>", line, key);
            g_free(val);
            return NULL;
        }
        do {
            line++;
        } while (g_ascii_isspace(*line));
    }

    /* hard value (everything else) */
    switch (val->type) {
        case NANOSCOPE_VALUE_SELECT:
        val->hard_value_str = line;
        len = strlen(line);
        if (line[0] == '"' && line[len-1] == '"') {
            line[len-1] = '\0';
            val->hard_value_str++;
        }
        break;

        case NANOSCOPE_VALUE_SCALE:
        val->hard_value = g_ascii_strtod(line, &p);
        break;

        case NANOSCOPE_VALUE_VALUE:
        val->hard_value = g_ascii_strtod(line, &p);
        if (p-line > 0 && *p == ' ' && !strchr(p+1, ' ')) {
            do {
                p++;
            } while (g_ascii_isspace(*p));
            val->hard_value_units = p;
        }
        val->hard_value_str = line;
        break;

        default:
        g_assert_not_reached();
        break;
    }

    return val;
}

/* vim: set cin et ts=4 sw=4 cino=>1s,e0,n0,f0,{0,}0,^0,\:1s,=0,g1s,h0,t0,+1s,c3,(0,u0 : */
